import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthService, Provider } from "./auth.service";
import { UsersService } from '../user/user.service';
import { User } from '../user/schema/user.schema';
import { ConfigService } from '../config/config.service';

const token = new ConfigService().get('JWT_TOKEN');

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt')
{

    constructor(
        private readonly usersService: UsersService
    ) 
    {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: token
        });
    }

    async validate(payload, done: Function)
    {
        try
        {
            // You could add a function to the authService to verify the claims of the token:
            // i.e. does the user still have the roles that are claimed by the token
            //const validClaims = await this.authService.verifyTokenClaims(payload);
            
            //if (!validClaims)
            //    return done(new UnauthorizedException('invalid token claims'), false);
            let userDB:User;
            if(payload.provider == 'LDAP')
            userDB = await this.usersService.findOneByThirdPartyId(payload.thirdPartyId, payload.provider);
            else
            userDB = await this.usersService.findOneByThirdPartyId(payload.thirdPartyId, Provider.GOOGLE);

            const userNew = {
              "_id": userDB["_id"],
              "username": userDB.username,
              "name": userDB.name,
              "photo": userDB.photo,
              "roles": userDB.roles,
              "accessLevel": userDB.accessLevel
            }

            done(null, userNew);
        }
        catch (err)
        {
            console.log("Error JwtStrategy: ", err)
            throw new UnauthorizedException('unauthorized', err.message);
        }
    }

}
