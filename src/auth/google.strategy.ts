import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-google-oauth20";
import { AuthService, Provider } from "./auth.service";
import { UsersService } from '../user/user.service';
import { ConfigService } from '../config/config.service';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google')
{

    validAccounts = []
    constructor(
        private readonly authService: AuthService,
        private readonly usersService: UsersService,
        private readonly config: ConfigService
    )
    {
        super({
            clientID    : config.get("GOOGLE_CLIENT_ID"),
            clientSecret: config.get("GOOGLE_CLIENT_SECRET"),
            callbackURL : config.get("GOOGLE_CLIENT_CALLBACK"),
            passReqToCallback: true,
            scope: ['email', 'profile'],
            hd: 'comisiondelaverdad.co', 
        })
    }

    authorizationParams(options: any): any {
      return Object.assign(options, {
        hd: 'comisiondelaverdad.co'
      });
    }

    async validate(request: any, accessToken: string, refreshToken: string, profile, done: Function)
    {
        try
        {
         
            var mail = profile.emails[0].value;
            var userName = mail.split("@");
            var isN3 = await this.authService.isMemberGroupBuscador(userName[0],"BUSCADORN3");
            var isN4 = await this.authService.isMemberGroupBuscador(userName[0],"BUSCADORN4");
            var accesLevel = 4;
            if( isN3 || isN4){
              if(isN3){
                accesLevel = 3;
              }
              if(profile.emails[0].value.endsWith("@comisiondelaverdad.co")){
                const jwt: string = await this.authService.validateOAuthLogin(request, profile, profile.id, Provider.GOOGLE, accesLevel);           
                if (!jwt.startsWith("Error:")){

                  const userNew = {
                    jwt
                  }
                  await this.authService.loggerLogin(mail, 'GOOGLE', 'SUCCESS', userNew);

                  done(null, userNew);
                }else{
                  
                  done(null, {error: jwt});
                }
              }else{
                const userFailNew = {
                  error: "El usuario debe pertenecer a la Comisión de la Verdad."
                }
                done(null, userFailNew);
              }
            }else{
              const userFailNew = {
                error: "Usuario sin permiso de acceso en directorio activo, por favor comunicarse con la mesa de ayuda SIM al correo ayuda.sim@comisiondelaverdad.co"
              }
              done(null, userFailNew);
            }
        }
        catch(err)
        {
            console.log("Error: ", err)
            await this.authService.loggerLogin(mail, 'GOOGLE', 'FAIL', err);
            done(err, false);
        }
    }
}
