import { HttpModule, Logger, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UsersService } from '../user/user.service';
import { GoogleStrategy } from './google.strategy';
import { JwtStrategy } from './jwt.strategy';
import { User, UserSchema } from '../user/schema/user.schema';
import { UserModule } from '../user/user.module';
import { AccessRequestModule } from '../access-request/access-request.module';
import { ConfigModule } from '../config/config.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { LogsService } from 'src/logs/logs.service';
import { UtilService } from 'src/util/util.service';


@Module({
  imports: [
    UserModule,
    AccessRequestModule,
    ConfigModule,
    HttpModule,
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    UsersService,
    LogsService,
    UtilService,
    GoogleStrategy,
    JwtStrategy,
    LocalStrategy,
    Logger
  ]
})
export class AuthModule { }