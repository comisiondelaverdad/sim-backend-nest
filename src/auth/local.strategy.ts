import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import {
  Injectable,
  UnauthorizedException,
  Inject,
  Logger,
  LoggerService,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { ConfigService } from '../config/config.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(
    private authService: AuthService,
    private readonly config: ConfigService,
    @Inject(Logger) private readonly logger: LoggerService,
  ) {
    super({
      LDAP_URL: config.get('LDAP_URL'),
      usernameField: 'token',
      passwordField: 'prt',
    });
  }

  async validate(token: string, prt: string, done) {
    var username = Buffer.from(token, 'base64').toString('ascii');
    var password = Buffer.from(prt, 'base64').toString('ascii');
    var jwt = '';
    var isLoggedIn = '';
    var isMemberGroup = false;
    var isN3 = false;
    var isN4 = false;
    var isActive = false;
    var accesLevel = 4;
    isLoggedIn = await this.authService.isLoggedAD(username, password);
    
    if (isLoggedIn == '200') {
      //SE valida la información con LDAP y el usuario existe
      isN3 = await this.authService.isMemberGroupBuscador(
        username,
        'BUSCADORN3',
      );
      isN4 = await this.authService.isMemberGroupBuscador(
        username,
        'BUSCADORN4',
      );

      await this.authService.loggerLogin(username, 'LDAP', 'SUCCESS', {
        isLoggedIn,
      });

      if (isN3 || isN4) {
        isMemberGroup = true;
      }
      if (isMemberGroup) {
        jwt = await this.authService.getJWTUserExist(username); //se consulta si el usuario esta en la BD y se genera el token
        if (!jwt) {
          //el usuario no existe, se debe guardar,
          if (isN3) {
            accesLevel = 3;
          }
          let user = await this.authService.getUserLdap(username, accesLevel);
          jwt = await this.authService.getJWTUserNew(user);
          isActive = await this.authService.getStatusUserMongo(username);
          //throw new UnauthorizedException();
        } else {
          isActive = await this.authService.getStatusUserMongo(username);
        }
      }
    }
    const userNew = {
      jwt,
      isLoggedIn,
      isMemberGroup,
      isActive,
    };

    done(null, userNew);
    
  }
}
