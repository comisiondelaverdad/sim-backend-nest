import { Injectable, Request, CanActivate, ExecutionContext, HttpException, HttpStatus} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import * as _ from "lodash"

export function matchRoles(user: object, rol: string): boolean{
  if( !user["roles"].includes(rol) ){
    throw new HttpException({
      status: HttpStatus.FORBIDDEN,
      error: "El usuario ["+user["username"]+"] no tiene permisos el rol ["+rol+"]",
    }, HttpStatus.FORBIDDEN);
  }
  return true
}


export function matchRolesListBoolean(user: object, roles: string[]): boolean{
  const intersection = _.intersection(user["roles"], roles)
    if (intersection.length === 0) {
      return false
    }
  return true
}

export function matchRolesList(user: object, roles: string[]): boolean{
  const intersection = _.intersection(user["roles"], roles)
    if (intersection.length === 0) {
      throw new HttpException({
        status: HttpStatus.FORBIDDEN,
        error: `El usuario ${user["username"]} no tiene permisos ${_.toString(roles)}`,
      }, HttpStatus.FORBIDDEN);
    }
  return true
}

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}
  
  matchRoles(roles: string [], rol: string): boolean{
    console.log("RolesGuard revisando roles 1")
    return roles.includes(rol)
  }

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    
    console.log(context.getHandler())
    
    const request = context.switchToHttp().getRequest();
    console.log("RolesGuard revisando roles 2, request", roles)
    //console.log("revisando roles 3, request", request)
    const user = request.user;
    console.log("RolesGuard user ", user)
    if(user!=undefined)
      return this.matchRoles(roles, user.roles);
    else
      return true
  }
}
