import {
  Controller,
  Get,
  UseGuards,
  Res,
  Req,
  Request,
  Logger,
  Param,
  Post,
  Body,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ConfigService } from '../config/config.service';
import { AuthService } from './auth.service';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';
import { Console } from 'console';
import { response } from 'express';

@ApiBearerAuth()
@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private config: ConfigService,
    private authService: AuthService,
    private readonly logger: Logger,
  ) {}

  @Get('google')
  @UseGuards(AuthGuard('google'))
  googleLogin() {
    // initiates the Google OAuth2 login flow
  }

  @Get('google/callback')
  @UseGuards(AuthGuard('google'))
  googleLoginCallback(@Req() req, @Res() res) {
    // handles the Google OAuth2 callback
    const jwt: string = req.user.jwt;
    if (jwt)
      //Constante para retornar a el frontend
      res.redirect(
        this.config.get('AUTH_FRONTEND_REDIRECT') + '/auth?token=' + jwt,
      );
    //Constante para retornar a el frontend
    else
      res.redirect(
        this.config.get('AUTH_FRONTEND_REDIRECT') +
          '/auth/login?error=' +
          Buffer.from(req.user.error).toString('base64'),
      );
  }

  @Get('protected')
  @UseGuards(AuthGuard('jwt'))
  protectedResource() {
    return 'JWT is working!';
  } 

  @UseGuards(AuthGuard('local'))
  @Get('loginAD')
  async login(@Request() req, @Res() res) {
    console.log(req["headers"]['referer']);
    console.log( this.config.get('AUTH_FRONTEND_REDIRECT'));
    if(req["headers"]['referer']){
      if(req["headers"]['referer'].includes(this.config.get('AUTH_FRONTEND_REDIRECT'))){
        const jwt: string = req.user.jwt;
        const isLoggedIn: string = req.user.isLoggedIn;
        const isMemberGroup: boolean = req.user.isMemberGroup;
        const isActive: boolean = req.user.isActive;
          if (isLoggedIn == '200') {
            //Constante para retornar a el frontend
            if (isMemberGroup) {
              if (isActive) {
                res.redirect(
                  this.config.get('AUTH_FRONTEND_REDIRECT') + '/auth?token=' + jwt,
                );
              } else {
                res.redirect(
                  this.config.get('AUTH_FRONTEND_REDIRECT') +
                    '/auth/login?error='+ Buffer.from('Usuario inactivo en la base de datos, por favor comunicarse con la mesa de ayuda SIM al correo ayuda.sim@comisiondelaverdad.co').toString('base64'),
                );
              }
            } else {
              res.redirect(
                this.config.get('AUTH_FRONTEND_REDIRECT') +
                  '/auth/login?error='+ Buffer.from('Usuario sin permiso de acceso en directorio activo, por favor comunicarse con la mesa de ayuda SIM al correo ayuda.sim@comisiondelaverdad.co').toString('base64'),
              );
            }
          } else {
            //Constante para retornar a el frontend
            if (isLoggedIn == '532')
              res.redirect(
                this.config.get('AUTH_FRONTEND_REDIRECT') +
                  '/auth/login?error='+ Buffer.from('Contraseña expirada, para actualizar la contraseña ingresando a: https://soportetic.comisiondelaverdad.co/aprusers/').toString('base64'),
              );
            if(isLoggedIn == '701')
             res.redirect(
              this.config.get('AUTH_FRONTEND_REDIRECT') +
              '/auth/login?error='+ Buffer.from('Su cuenta ha expirado, por favor comunicarse con la mesa de ayuda SIM al correo ayuda.sim@comisiondelaverdad.co').toString('base64'),
             );
             if(isLoggedIn == '533')
             res.redirect(
              this.config.get('AUTH_FRONTEND_REDIRECT') +
              '/auth/login?error='+ Buffer.from('Su cuenta está deshabilitada, por favor comunicarse con la mesa de ayuda SIM al correo ayuda.sim@comisiondelaverdad.co').toString('base64'),
             );
            else
              res.redirect(
                this.config.get('AUTH_FRONTEND_REDIRECT') +
                  '/auth/login?error='+ Buffer.from( 'No es posible acceder, por favor valide credenciales de acceso').toString('base64'),
              );
          }
    
        }
    }else{
      req.user.isLoggedIn = false;
      res.redirect(
        this.config.get('AUTH_FRONTEND_REDIRECT') +
          '/auth/login?error='+ Buffer.from( 'Subdominio no permitido').toString('base64'),
      );
    }
    
    return req.user;
  }

  @Get('token')
  token() {
    return this.config.get('LOCALSTORE_TOKEN');
  }

  @Post('validCaptcha')
  async validCaptcha(@Body() body) {
    const response =  await this.authService.isValidCaptchav3(body);
    return response;
  }
}
