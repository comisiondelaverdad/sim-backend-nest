import {
  Injectable,
  InternalServerErrorException,
  Logger,
  LoggerService,
  Inject,
  HttpService,
} from '@nestjs/common';
import { sign } from 'jsonwebtoken';
import { UsersService } from '../user/user.service';
import { User } from '../user/schema/user.schema';
import { ConfigService } from '../config/config.service';
const ActiveDirectory = require('activedirectory2').promiseWrapper;
import { pluck } from 'rxjs/operators';

export enum Provider {
  GOOGLE = 'google',
}

@Injectable()
export class AuthService {
  private JWT_SECRET_KEY; // <- replace this with your secret key
  private JWT_EXPIRES;
  private ad;
  private SECRET_RECAPTCHA;

  constructor(
    private httpService: HttpService,
    private readonly usersService: UsersService,
    private readonly config: ConfigService,
    @Inject(Logger) private readonly logger: LoggerService,
  ) {
    const configAD = {
      url: this.config.get('LDAP_URL'),
      baseDN: this.config.get('BASE_DN'),
      username: this.config.get('USER_NAME_LDAP'),
      password: this.config.get('PASS_LDAP')
    };
    this.ad = new ActiveDirectory(configAD);
    this.JWT_SECRET_KEY = this.config.get('JWT_TOKEN');
    this.JWT_EXPIRES = this.config.get('JWT_EXPIRES');
    this.SECRET_RECAPTCHA = this.config.get('SECRET_RECAPTCHA');
  }

  async validateOAuthLogin(
    request: any,
    profile: object,
    thirdPartyId: string,
    provider: Provider,
    accessLevel: number,
  ): Promise<string> {
    try {
      let user: User = await this.usersService.findOneByUserName(profile['emails'][0]['value']);
      if (user) {
        if (user.provider == "LDAP") {
          user.social = profile;
          user.provider = provider;
          user.name = profile['displayName'];
          user.photo = profile['photos'][0]['value'];
          user.lastLogin = new Date();
          await this.usersService.update(user['_id'], user);
        }
      }

      if (!user) {
        let u = {
          social: profile,
          provider: provider,
          roles: ['user'],
          accessLevel: accessLevel,
          status: 1,
          name: profile['displayName'],
          username: profile['emails'][0]['value'],
          email: profile['emails'][0]['value'],
          photo: profile['photos'][0]['value'],
          date: new Date(),
          lastLogin: new Date(),
        };

        user = await this.usersService.registerOAuthUser(
          u,
          thirdPartyId,
          provider,
        );
      } else {
        if (user.status === 1) {
          user.social = profile;
          user.photo = profile['photos'][0]['value'];
          user.lastLogin = new Date();
          await this.usersService.update(user['_id'], user);
        } else {
          await this.loggerLogin(profile['emails'][0]['value'], 'GOOGLE', 'FAIL', {
            message: 'Error: El usuario no está autorizado a ingresar al sistema. Consulte con su coordinador.',
            user
          });
          return 'Error: El usuario no está autorizado a ingresar al sistema. Consulte con su coordinador.';
        }
      }

      const payload = {
        thirdPartyId,
        provider,
      };

      const jwt: string = sign(payload, this.JWT_SECRET_KEY, {
        expiresIn: this.JWT_EXPIRES,
      });

      return jwt;

    } catch (err) {
      await this.loggerLogin(profile['emails'][0]['value'], 'GOOGLE', 'ERROR', {
        message: 'Ha ocurrido un problema en la autenticación !',
        err
      });
      throw new InternalServerErrorException('validateOAuthLogin', err.message);
    }
  }

  async getJWTUserExist(username: string): Promise<any> {
    var thirdPartyId = '';
    var provider = 'LDAP';
    let userMongo: User = await this.usersService.findOneByUserName(
      username + '@comisiondelaverdad.co',
    );
    if (userMongo) {
      //si existe el usuario en MongoDB
      thirdPartyId = userMongo.social['id'];
      provider = userMongo.provider;
      const payload = {
        thirdPartyId,
        provider,
      };

      userMongo.lastLogin = new Date();
      await this.usersService.update(userMongo['_id'], userMongo);

      const jwt: string = await sign(payload, this.JWT_SECRET_KEY, {
        expiresIn: this.JWT_EXPIRES,
      });
      return jwt;
    }
    await this.loggerLogin(username, 'LDAP', 'WARNING', {
      message: 'El usuario no se ha encontrado en la base de datos de Mongo',
    });
    return null;
  }

  async getJWTUserNew(user: any) {
    //Crea un nuevo usuario JWT y retorna el jwt
    const thirdPartyId = user.social.id;
    const provider = user.provider;
    try {
      await this.usersService.registerOAuthUser(user, thirdPartyId, provider);
    } catch (error) {
      await this.loggerLogin(user.username, 'registerOAuthUser', 'ERROR', {
        message: 'Error al registrar nuevo usuario',
        error,
      });
    }
    const payload = {
      thirdPartyId,
      provider,
    };
    const jwt: string = await sign(payload, this.JWT_SECRET_KEY, {
      expiresIn: this.JWT_EXPIRES,
    });
    return jwt;
  }

  getUserLdap(username: string, accesLevel: any): any {
    let context = this;
    return new Promise((resolve) => {
      var provider = 'LDAP';
      try {
        this.ad.findUser(username, async function (err, userAD) {
          if (err) {
            console.log('ERROR: ' + JSON.stringify(err));
            await context.loggerLogin(username, 'LDAP', 'WARNING', {
              message: 'Error al encontrar al usuario en LDAP',
              err,
            });
          }

          if (!userAD) {
            await context.loggerLogin(username, 'LDAP', 'WARNING', {
              message: 'El usuario no se ha encontrado en LDAP',
            });
          } else {
            var profile = {
              id: userAD.sAMAccountName,
              displayName: userAD.displayName,
              name: {
                familyName: userAD.sn,
                givenNAme: userAD.givenName,
              },
              provider: provider,
            };

            let u = {
              social: profile,
              provider: provider,
              roles: ['user'],
              accessLevel: accesLevel,
              status: 1,
              name: userAD.displayName,
              username: userAD.sAMAccountName + '@comisiondelaverdad.co',
              email: userAD.mail,
              photo:
                'https://comisiondelaverdad.co/templates/cev-template/images/Logo-CV-color-vertical.svg',
              date: new Date(),
              lastLogin: new Date(),
            };
            resolve(u);
          }
        });
      } catch (error) {
        console.log('ERROR EN FIND USER: ' + error);
        this.loggerLogin(username, 'LDAP', 'ERROR', {
          error,
          message: 'ERROR EN PROCESO BUSQUEDA DE USUARIO EN LDAP',
        });
      }
    });
  }

  isValidCaptchav3(token) {
    const path = 'https://www.google.com/recaptcha/api/siteverify';
    const body = `secret=${this.SECRET_RECAPTCHA}&response=${token.token }`
    return this.httpService.post(path, body).pipe(pluck('data')).toPromise()
  }

  isLoggedAD(username: string, pass: string): any {
    let context = this;
    return new Promise((resolve) => {
      this.ad.authenticate(
        username + '@comisiondelaverdad.loc',
        pass,
        async function (err, auth) {
          if (err) {
            var error = JSON.stringify(err);
            console.log("error en autentication:" + error);
            if (err.lde_message.includes('532')) {
              //contraseña expirada
              await context.loggerLogin(username, 'LDAP', 'FAIL', {
                err,
                message: 'La contraseña ha expirado!',
              });
              resolve('532');
            }
            if (err.lde_message.includes('701')) {
              await context.loggerLogin(username, 'LDAP', 'FAIL', {
                err,
                message: 'Cuenta expirada!',
              });
              resolve('701');
            }

            if (err.lde_message.includes('533')) {
              await context.loggerLogin(username, 'LDAP', 'FAIL', {
                err,
                message: 'Cuenta deshabilitada!',
              });
              resolve('533');
            }

            if (err.lde_message.includes('52e')) {
              await context.loggerLogin(username, 'LDAP', 'FAIL', {
                err,
                message: 'Credenciales LDAP inválidas!',
              });
              resolve('52e');
            }
          }
          if (auth) {
            resolve('200'); //La autenticación es correcta
          }
        },
      );
    });
  }

  isMemberGroupBuscador(username: string, nameGroup: string): any {
    let context = this;
    return new Promise((resolve) => {
      this.ad.isUserMemberOf(username, nameGroup, async function (err, isMember) {
        if (err) {
          await context.loggerLogin(username, 'LDAP', 'WARNING', {
            err,
            message:
              'El usuario no pertenece al grupo Buscador del Directorio Activo',
          });
          console.log('ERROR: ' + JSON.stringify(err));
          resolve(false);
        }
        resolve(isMember);
      });
    });
  }

  async getStatusUserMongo(username: string) {
    try {
      let userMongo: User = await this.usersService.findOneByUserName(
        username + '@comisiondelaverdad.co',
      );
      if (userMongo) {
        if (userMongo.status == 1) return true;
      }
    } catch (error) {
      await this.loggerLogin(username, 'MONGODB', 'ERROR', {
        message: 'Ha existido un error en la búsqueda del usuario',
        error,
      });
    }

    return false;
  }

  getUsersForGroup(): any {
    return new Promise((resolve) => {
      var groupName = 'Buscador';
      this.ad.getUsersForGroup(groupName, function (err, users) {
        if (err) {
          console.log('ERROR: ' + JSON.stringify(err));
          return;
        }

        if (!users) console.log('Group: ' + groupName + ' not found.');
        else {
          console.log(JSON.stringify(users));
        }
      });
    });
  }
  public async loggerLogin(
    username,
    tipo,
    state,
    metadata = null,
  ): Promise<any> {

    const log_object = {
      'username/email': username,
      date: new Date(),
      provider: tipo,
      state: state,
      metadata: metadata,
    };
    return await this.logger.log(
      `LOGIN: ${state}, ${JSON.stringify(log_object)}`,
    );
  }
}
