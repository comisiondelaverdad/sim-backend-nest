import { IsInt, IsString, IsObject, IsBoolean, IsDate, IsMongoId, IsArray, IsNumber } from 'class-validator';
import { Document, Schema as SchemaM } from 'mongoose';

export class AccessRequestExtensionDto {

  @IsNumber()
  readonly ident: number;

  @IsMongoId()
  readonly user: string;

  @IsDate()
  readonly date: Date;

  @IsDate()
  readonly from: Date;

  @IsDate()
  readonly to: Date;
  
  @IsString()
  readonly status: string;
}

