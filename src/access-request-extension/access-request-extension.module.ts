import { Module } from '@nestjs/common';
import { getConnectionToken, MongooseModule } from '@nestjs/mongoose';
import { AccessRequestExtensionService } from './access-request-extension.service';
import { AccessRequestExtensionController } from './access-request-extension.controller';
import { AccessRequestExtensionSchema } from './schema/access-request-extension.schema';
import { Connection } from 'mongoose';
import * as AutoIncrementFactory from 'mongoose-sequence';
import { PassportModule } from '@nestjs/passport';

@Module({
    imports: [
        MongooseModule.forFeatureAsync([{
            name: 'AccessRequestExtension',
            useFactory: async (connection: Connection) => {
                const schema = AccessRequestExtensionSchema;
                const AutoIncrement = AutoIncrementFactory(connection);
                // eslint-disable-next-line @typescript-eslint/camelcase
                schema.plugin(AutoIncrement, { id: 'ident_access-request-extension', inc_field: 'ident' });
                return schema;
            },
            inject: [getConnectionToken("database")]
        }], "database"),
        PassportModule.register({ defaultStrategy: 'jwt' })
    ],
    controllers: [AccessRequestExtensionController],
    providers: [AccessRequestExtensionService],
    exports: [AccessRequestExtensionService, MongooseModule]
})
export class AccessRequestExtensionModule { }