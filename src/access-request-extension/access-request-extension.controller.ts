import { Controller, Put, Param, Body, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AccessRequestExtensionService } from './access-request-extension.service';
import { AccessRequestExtensionDto } from './dto/access-request-extension.dto' 
import { AccessRequestExtension } from './schema/access-request-extension.schema';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('access-request-extension')
@Controller('api/access-request-extension')
export class AccessRequestExtensionController {

  constructor(private readonly accessRequestExtensionService: AccessRequestExtensionService) {}

  // Create AccessRequestExtension
  @Post()
  @UseGuards(AuthGuard())
  create(@Body() are: AccessRequestExtensionDto) : Promise<AccessRequestExtension>{
    return this.accessRequestExtensionService.create(are)
  }
  
  // Update AccessRequestExtension
  @Put(':ident')
  @UseGuards(AuthGuard())
  update(@Param("ident") ident, @Body() are: AccessRequestExtensionDto) : Promise<AccessRequestExtension>{
    return this.accessRequestExtensionService.update(ident, are)
  }

}
