import { Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AccessRequestExtension } from './schema/access-request-extension.schema';

@Injectable()
export class AccessRequestExtensionService {

  constructor(@InjectModel('AccessRequestExtension') private accessRequestExtension: Model<AccessRequestExtension>) {}

  async create(post): Promise<AccessRequestExtension> {
    return this.accessRequestExtension.create(post)
  }

  async update(ident, put): Promise<AccessRequestExtension> {
    return this.accessRequestExtension.findOneAndUpdate({"ident":ident}, put,{new: true})
  }

}
