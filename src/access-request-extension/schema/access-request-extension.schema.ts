import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as SchemaM } from 'mongoose';

@Schema({collection:"access-requests-extensions"})
export class AccessRequestExtension extends Document {
  
  
  @Prop()
  ident: number;

  @Prop()
  user: SchemaM.ObjectId; //{type: 'ObjectId', ref: 'User'}

  @Prop()
  date: Date;

  @Prop()
  from: Date;

  @Prop()
  to: Date;

  @Prop()
  status: string;

}


export const AccessRequestExtensionSchema = SchemaFactory.createForClass(AccessRequestExtension);

