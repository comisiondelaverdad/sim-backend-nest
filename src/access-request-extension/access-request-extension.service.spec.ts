import { Test, TestingModule } from '@nestjs/testing';
import { AccessRequestExtensionService } from './access-request-extension.service';

describe('AccessRequestExtensionService', () => {
  let service: AccessRequestExtensionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AccessRequestExtensionService],
    }).compile();

    service = module.get<AccessRequestExtensionService>(AccessRequestExtensionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
