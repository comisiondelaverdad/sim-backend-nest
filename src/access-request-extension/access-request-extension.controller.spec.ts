import { Test, TestingModule } from '@nestjs/testing';
import { AccessRequestExtensionController } from './access-request-extension.controller';

describe('AccessRequestExtension Controller', () => {
  let controller: AccessRequestExtensionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AccessRequestExtensionController],
    }).compile();

    controller = module.get<AccessRequestExtensionController>(AccessRequestExtensionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
