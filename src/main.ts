import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ConfigService } from './config/config.service';
import { LoggingService } from './logging/logging.service';


import * as bodyParser from 'body-parser';


async function bootstrap() {
  const app = await NestFactory.create(AppModule); 
  const loggingService = app.get(LoggingService);  
  app.useLogger(await loggingService.creationLogger());  
  app.use(bodyParser.json({ limit: '20mb' }));
  app.use(bodyParser.urlencoded({ limit: '20mb', extended: true }));
  app.enableCors();


  const configService = app.get(ConfigService);

  const options = new DocumentBuilder()
    .setTitle('SIM Comisión de la Verdad')
    .setDescription(
      'Sistema de Información Misional de la Comisión de la Verdad',
    )
    .setVersion('2.0')
    .addBearerAuth()
    //.addTag('')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);
  await app.listen(configService.get('PORT'));
}
bootstrap();
