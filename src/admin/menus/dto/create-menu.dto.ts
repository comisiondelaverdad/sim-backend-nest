export class CreateMenuDto {
    _id?: String;
    readonly nombreMenu: String;  
    readonly estado: Number;
    readonly elements:  Array<Object>; 
    readonly section: Number;   
    
  }
  