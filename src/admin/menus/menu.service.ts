import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Menu } from './schema/menu.schema';
import { CreateMenuDto } from './dto/create-menu.dto';
import { EditMenuDto } from './dto/edit-menu.dto';
import { Resource } from 'src/resource/schema/resource.schema';

@Injectable()
export class MenuService {
  constructor(
    @InjectModel('Menu') private menuModel: Model<Menu>,
    @InjectModel('Resource') private resourceModel: Model<Resource>,
  ) {}

  async create(createMenuDto: CreateMenuDto): Promise<Menu> {
    createMenuDto['status'] = 1;
    const createdMenu = new this.menuModel(createMenuDto);

    if (createMenuDto.section) {
      const filter = { section: createMenuDto.section };
      const update = { section: null };
      await this.menuModel.updateMany(filter, update);
    }

    //obtener la sección de ese menú
    //si tiene seccion obtener los menus con esa sección y editarlos dejandolor sin sección
    return createdMenu.save();
  }

  async find(query): Promise<Menu[]> {
    let skip = parseInt(query['skip']);
    let limit = parseInt(query['limit']);
    delete query['skip'];
    delete query['limit'];
    return this.menuModel.find(query).skip(skip).limit(limit).exec();
  }

  async findByid(id): Promise<Menu> {
    return this.menuModel.findOne({ _id: id }).exec();
  }
  async findBySection(section): Promise<Menu> {
    return this.menuModel.findOne({ section: section, estado: 1 }).exec();
  }
  async findAllBySection(section): Promise<Menu> {
    return this.menuModel.find({ section: section, estado: 1 }).exec();
  }

  async lock(id): Promise<Menu> {
    return this.menuModel
      .findOneAndUpdate({ _id: id }, { estado: 0 }, { new: true })
      .exec();
  }

  async update(id, menu: EditMenuDto): Promise<Menu> {
    if (menu.section) {
      const filter = { section: menu.section };
      const update = { section: null };
      await this.menuModel.updateMany(filter, update);
    }
    return this.menuModel.findOneAndUpdate({ _id: id }, menu).exec();
  }

  async findOneByUserName(username: string): Promise<Menu> {
    return this.menuModel.findOne({ nombreMenu: username }).exec();
  }

  async delete(id: string): Promise<Menu> {
    return this.menuModel.deleteOne({ _id: id }).exec();
  }

  async test(): Promise<any> {
    return await this.resourceModel
      .aggregate([{ $group: { _id: '$ident', count: { $sum: 1 } } }])
      .allowDiskUse(true)
      .exec();
  }
  // async element(id: string, element: Element): Promise<Menu>{

  //   // return this.menuModel.findOneAndUpdate({ _id: id }, menu).exec();

  // }
}
