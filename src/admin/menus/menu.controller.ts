import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  Query,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { matchRoles } from '../../auth/roles.guard';
import { MenuService } from './menu.service';
import { CreateMenuDto } from './dto/create-menu.dto';
import { EditMenuDto } from './dto/edit-menu.dto';

import { Menu } from './schema/menu.schema';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('admin/menus')
@Controller('api/admin/menus')
export class MenuController {
  constructor(private readonly menuService: MenuService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  async create(@Request() req, @Body() createMenuDto: CreateMenuDto) {
    matchRoles(req.user, 'admin'); //Validación de roles del usuario
    return await this.menuService.create(createMenuDto);
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  async update(
    @Request() req,
    @Param('id') id,
    @Body() editMenuDto: EditMenuDto,
  ) {
    matchRoles(req.user, 'admin'); //Validación de roles del usuario
    await this.menuService.update(id, editMenuDto);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async find(@Request() req, @Query() query): Promise<Menu[]> {
    matchRoles(req.user, 'admin'); //Validación de roles del usuario
    return this.menuService.find(query);
    // return this.menuService.test();

  }
  //@UseGuards(AuthGuard('jwt'))
  @Get('/section/:section')
  async findOneBySection(
    @Request() req,
    @Param('section') section:  string,
    ): Promise<Menu> {
      return this.menuService.findBySection(section);
    }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  async findOne(@Request() req, @Param('id') id): Promise<Menu> {
    matchRoles(req.user, 'admin'); //Validación de roles del usuario
    return this.menuService.findByid(id);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  async lock(@Request() req, @Param('id') id): Promise<Menu> {
    matchRoles(req.user, 'admin'); //Validación de roles del usuario
    return this.menuService.lock(id);
  }
  @Get('/test/')
  async test(@Request() req): Promise<any> {
    return this.menuService.test();
  }
}
