import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Menu extends Document {
  @Prop({ type: 'string' })
  nombreMenu: string;

  @Prop({ type: 'number' })
  estado: number;

  @Prop({type:"array"})
  elements:  Array<Object>;
  
  @Prop({ type: 'number' })
  section: number;
}
export const MenuSchema = SchemaFactory.createForClass(Menu);
