import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MenuController } from './menu.controller';
import { Menu, MenuSchema } from './schema/menu.schema';
import { Resource, ResourceSchema } from '../../resource/schema/resource.schema';

import { MenuService } from './menu.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Menu', schema: MenuSchema }, { name: 'Resource', schema: ResourceSchema }], "database")],
  controllers: [MenuController],
  providers: [MenuService]
})
export class MenuModule {}
