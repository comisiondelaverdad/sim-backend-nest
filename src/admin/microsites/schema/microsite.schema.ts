import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Microsite extends Document {
  @Prop({ type: 'string' })
  name: string;
  @Prop({ type: 'string' })
  description: string;
  @Prop({type:"array"})
  elements:  Array<Object>;
  @Prop({type:"number"})
  status:  Number;  
  @Prop({type:"string"})
  section:  string; 

}
export const MicrositeSchema = SchemaFactory.createForClass(Microsite);
