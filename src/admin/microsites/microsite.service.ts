import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Microsite } from './schema/microsite.schema';
import { CreateMicrositeDto } from './dto/create-microsite.dto';
import { EditMicrositeDto } from './dto/edit-microsite.dto';

@Injectable()
export class MicrositeService {
  constructor(
    @InjectModel('Microsite') private micrositeModel: Model<Microsite>,
  ) {}

  async create(createMicrositeDto: CreateMicrositeDto): Promise<Microsite> {
    const createdMenu = new this.micrositeModel(createMicrositeDto);
    createdMenu['status'] = 1;

    if (createMicrositeDto.section) {
      const filter = { section: createMicrositeDto.section };
      const update = { section: '' };
      await this.micrositeModel.updateMany(filter, update);
    }
    return createdMenu.save();
  }

  async find(query): Promise<Microsite[]> {
    let skip = parseInt(query['skip']);
    let limit = parseInt(query['limit']);
    delete query['skip'];
    delete query['limit'];
    // query['status'] = 1;
    query.status = 1;
    return this.micrositeModel.find(query).skip(skip).limit(limit).exec();
  }

  async findByid(id): Promise<Microsite> {
    return this.micrositeModel.findOne({ _id: id }).exec();
  }
  // async findBySection(section): Promise<Microsite> {
  //   return this.micrositeModel.findOne({ section: section, estado: 1 }).exec();
  // }
  async findAllBySection(section): Promise<Microsite> {
    return this.micrositeModel.find({ section: section, estado: 1 }).exec();
  }

  async lock(id): Promise<Microsite> {
    return this.micrositeModel
      .findOneAndUpdate({ _id: id }, { status: 0 }, { new: true })
      .exec();
  }

  async update(id, microsite: EditMicrositeDto): Promise<Microsite> {
    if (microsite.section) {
      const filter = { section: microsite.section };
      const update = { section: '' };
      await this.micrositeModel.updateMany(filter, update);
    }
    return this.micrositeModel.findOneAndUpdate({ _id: id }, microsite).exec();
  }

  async findOneByUserName(username: string): Promise<Microsite> {
    return this.micrositeModel.findOne({ nombreMenu: username }).exec();
  }

  async delete(id: string): Promise<Microsite> {
    return this.micrositeModel.deleteOne({ _id: id }).exec();
  }
  async findMicrosite(body: any): Promise<Microsite> {
    return await this.micrositeModel
      .find({ name: new RegExp(body.keyWord, 'i'), status: 1 })
      .exec();
  }
  async findByName(body: any): Promise<Microsite> {
    return await this.micrositeModel
      .find({ name: body.name, status: 1 })
      .exec();
  }
  async findBySection(body: any): Promise<Microsite> {
    return await this.micrositeModel
      .find({ section: body.section, status: 1 })
      .exec();
  }

  
}
