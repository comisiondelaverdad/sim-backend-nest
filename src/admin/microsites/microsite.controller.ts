import { Controller, Get, Post, Put, Delete, Body, Param, Query, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { matchRoles } from '../../auth/roles.guard';
import { MicrositeService } from './microsite.service';
import { CreateMicrositeDto } from './dto/create-microsite.dto';
import { EditMicrositeDto } from './dto/edit-microsite.dto';

import { Microsite } from './schema/microsite.schema';


import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('admin/microsites')
@Controller('api/admin/microsites')
export class MicrositeController {

  constructor(private readonly micrositeService: MicrositeService) {}
  
  @Post()
  @UseGuards(AuthGuard('jwt'))
  async create(@Request() req, @Body() createMicrositeDto: CreateMicrositeDto) {
    matchRoles(req.user, 'admin') //Validación de roles del usuario
    return await this.micrositeService.create(createMicrositeDto);
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  async update(@Request() req, @Param("id") id, @Body() editMicrositeDto: EditMicrositeDto) {
    matchRoles(req.user, 'admin') //Validación de roles del usuario
    await this.micrositeService.update(id, editMicrositeDto);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async find(@Request() req, @Query() query): Promise<Microsite[]> {
    matchRoles(req.user, 'admin') //Validación de roles del usuario   
    return this.micrositeService.find(query);
  }
  @Get('/section/:section')
  @UseGuards(AuthGuard('jwt'))
  async findOneBySection(@Request() req, @Param('section') section): Promise<Microsite> {
    
    return this.micrositeService.findBySection(section);
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  async findOne(@Request() req, @Param('id') id): Promise<Microsite> {
    matchRoles(req.user, 'admin') //Validación de roles del usuario
    return this.micrositeService.findByid(id);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  async lock(@Request() req, @Param("id") id): Promise<Microsite> {
    matchRoles(req.user, 'admin') //Validación de roles del usuario
    return this.micrositeService.lock(id);
  }

  @Post('find')
  @UseGuards(AuthGuard('jwt'))
  async findMicrosite(@Body() body: any) {      
    return await this.micrositeService.findMicrosite(body);
  }
  @Post('findByName')
  @UseGuards(AuthGuard('jwt'))
  async findByName(@Body() body: any) {      
    return await this.micrositeService.findByName(body);
  }
  //@UseGuards(AuthGuard('jwt'))
  @Post('findBySection')
  async findBySection(@Body() body: any) {      
    return await this.micrositeService.findBySection(body);
  }

  

}
