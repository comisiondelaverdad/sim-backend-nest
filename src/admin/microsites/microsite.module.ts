import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MicrositeController } from './microsite.controller';
import { Microsite, MicrositeSchema } from './schema/microsite.schema';
import { MicrositeService } from './microsite.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Microsite', schema: MicrositeSchema }], "database")],
  controllers: [MicrositeController],
  providers: [MicrositeService]
})
export class MicrositeModule {}
