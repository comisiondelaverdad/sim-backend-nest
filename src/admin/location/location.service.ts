import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Location } from '../../location/schema/location.schema';
import { CreateLocationDto } from '../../location/dto/create-location.dto';
import { EditLocationDto } from '../../location/dto/edit-location.dto';


@Injectable()
export class LocationService {
  constructor(@InjectModel('Location') private locationModel: Model<Location>) {}

  async create(createLocationDto: CreateLocationDto): Promise<Location> {
    const createdLocation = new this.locationModel(createLocationDto);
    return createdLocation.save();
  }

  async find(query): Promise<Location[]> {
    let skip = parseInt(query["skip"])
    let limit = parseInt(query["limit"])
    delete query["skip"]
    delete query["limit"]
    return this.locationModel.find(query).select('-centroid -geometry').skip(skip).limit(limit).exec();
  }

  async findByid(id): Promise<Location> {
    return this.locationModel.findOne({"_id":id}).exec();
  }
  
  async lock(id): Promise<Location> {
    return this.locationModel.findOneAndUpdate({"_id":id}, {"status": false}).exec();
  }

  async update(id, location: EditLocationDto): Promise<Location> {
    return this.locationModel.findOneAndUpdate({"_id":id}, location).exec();
  }

}
