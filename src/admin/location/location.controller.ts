import { Controller, Get, Post, Put, Delete, Body, Param, Query, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { matchRoles } from '../../auth/roles.guard';

import { LocationService } from './location.service';
import { CreateLocationDto } from '../../location/dto/create-location.dto';
import { EditLocationDto } from '../../location/dto/edit-location.dto';
import { Location } from '../../location/schema/location.schema';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('admin/locations')
@Controller('api/admin/locations')
export class LocationController {

  constructor(private readonly locationService: LocationService) {}
  
  @Post()
  @UseGuards(AuthGuard('jwt'))
  async create(@Request() req, @Body() createLocationDto: CreateLocationDto) {
    matchRoles(req.user, 'admin') //Validación de roles del usuario
    return await this.locationService.create(createLocationDto);
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  async update(@Request() req, @Param("id") id, @Body() editLocationDto: EditLocationDto) {
    matchRoles(req.user, 'admin') //Validación de roles del usuario
    await this.locationService.update(id, editLocationDto);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async find(@Request() req, @Query() query): Promise<Location[]> {
    //matchRoles(req.user, 'admin')
    return this.locationService.find(query);
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  async findOne(@Request() req, @Param('id') id): Promise<Location> {
    //matchRoles(req.user, 'admin') //Validación de roles del usuario
    return this.locationService.findByid(id);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  async lock(@Request() req, @Param("id") id): Promise<Location> {
    matchRoles(req.user, 'admin') //Validación de roles del usuario
    return this.locationService.lock(id);
  }
}
