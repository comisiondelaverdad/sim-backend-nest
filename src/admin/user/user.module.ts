import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserController } from './user.controller';
import { User, UserSchema } from '../../user/schema/user.schema';
import { UsersService } from './user.service';
import { LogsSchema } from 'src/logs/schema/logs.schema';
import { LogsService } from 'src/logs/logs.service';
import { ConfigService } from 'src/config/config.service';
import { UtilService } from 'src/util/util.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }], "database"),
    MongooseModule.forFeature([{ name: 'Logs', schema: LogsSchema }], "mongologs"),
    HttpModule,

  ],
  controllers: [UserController],
  providers: [UsersService, LogsService, ConfigService, UtilService]
})
export class UserModule { }
