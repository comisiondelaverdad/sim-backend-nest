import { Controller, Get, Post, Put, Delete, Body, Param, Query, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { matchRoles } from '../../auth/roles.guard';

import { UsersService } from './user.service';
import { CreateUserDto } from '../../user/dto/create-user.dto';
import { EditUserDto } from '../../user/dto/edit-user.dto';
import { User } from '../../user/schema/user.schema';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('admin/users')
@Controller('api/admin/users')
export class UserController {

  constructor(private readonly usersService: UsersService) {}
  
  @Post()
  @UseGuards(AuthGuard('jwt'))
  async create(@Request() req, @Body() createUserDto: CreateUserDto) {
    matchRoles(req.user, 'admin') //Validación de roles del usuario
    return await this.usersService.create(createUserDto, req);
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  async update(@Request() req, @Param("id") id, @Body() editUserDto: EditUserDto) {
    matchRoles(req.user, 'admin') //Validación de roles del usuario
    await this.usersService.update(id, editUserDto, req);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async find(@Request() req, @Query() query): Promise<User[]> {
    matchRoles(req.user, 'admin') //Validación de roles del usuario
    console.log('entra');
    return this.usersService.find(query);
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  async findOne(@Request() req, @Param('id') id): Promise<User> {
    matchRoles(req.user, 'admin') //Validación de roles del usuario
    return this.usersService.findByid(id);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  async lock(@Request() req, @Param("id") id): Promise<User> {
    matchRoles(req.user, 'admin') //Validación de roles del usuario
    return this.usersService.lock(id);
  }
}
