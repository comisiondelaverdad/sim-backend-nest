import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from '../../user/schema/user.schema';
import { CreateUserDto } from '../../user/dto/create-user.dto';
import { EditUserDto } from '../../user/dto/edit-user.dto';
import { CreateLogDto } from 'src/logs/dto/create-log.dto';
import { LogsService } from 'src/logs/logs.service';
import { UtilService } from 'src/util/util.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel('User') private userModel: Model<User>,
    private readonly utils: UtilService,
    private readonly logs: LogsService
  ) { }

  async create(createUserDto: CreateUserDto, req): Promise<User> {
    createUserDto["accesslevel"] = 4
    const createdUser = new this.userModel(createUserDto);
    const user = await  createdUser.save();
    let logInfo: CreateLogDto = {
      user: user._id,
      username:user.username,
      from: "user",
      action: "created",
      message: "",
      metadata: {
        ident: user._id,
        roles: user.roles,
        accessLevel: user.accessLevel,
        status: user.status,
        ip: req.ip ? req.ip : (req.connection.remoteAddress ? req.connection.remoteAddress :
          (req.headers['x-forwarded-for'] ? req.headers['x-forwarded-for'] : ""))
      },
      createdAt: this.utils.dateToAAAAMMDD(Date.now()),
    }
    this.logs.create(logInfo);
    this.logs.logger("app", logInfo);
    return new Promise((resolve) => {
      resolve(createdUser)
    })
  }

  async find(query): Promise<User[]> {
    let skip = parseInt(query["skip"])
    let limit = parseInt(query["limit"])
    delete query["skip"]
    delete query["limit"]
    return this.userModel.find(query).skip(skip).limit(limit).exec();
  }

  async findByid(id): Promise<User> {
    return this.userModel.findOne({ "_id": id }).exec();
  }

  async lock(id): Promise<User> {
    return this.userModel.findOneAndUpdate({ "_id": id }, { "status": false }).exec();
  }

  async update(id, user: EditUserDto, req): Promise<User> {
    const userOld = await this.userModel.findOne({ "_id": id });
    let logInfo: CreateLogDto = {
      user: req.user._id,
      username:req.user.username,
      from: "user",
      action: "updated",
      message: "",
      metadata: {
        ident: id,
        roles: userOld.roles,
        accessLevel: userOld.accessLevel,
        status: userOld.status,
        ip: req.ip ? req.ip : (req.connection.remoteAddress ? req.connection.remoteAddress :
          (req.headers['x-forwarded-for'] ? req.headers['x-forwarded-for'] : ""))
      },
      updatedAt: this.utils.dateToAAAAMMDD(Date.now())
    }
    this.logs.create(logInfo);
    this.logs.logger("app", logInfo);
    return this.userModel.findOneAndUpdate({ "_id": id }, user).exec();
  }

  async findOneByUserName(username: string): Promise<User> {
    return this.userModel.findOne({ "username": username }).exec();
  }

}
