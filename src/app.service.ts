import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'API 2.0 SIM Comisión de la Verdad';
  }
}
