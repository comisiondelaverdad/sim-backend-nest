import { Injectable, HttpService, HttpException, HttpStatus } from '@nestjs/common';
import { Model, Types} from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { List } from './schema/list.schema';
import * as _ from "lodash";
import { ListDto } from './dto/list.dto';
import { UtilService } from 'src/util/util.service';
import { Option } from './../option/schema/option.schema'
import { Forms } from './../forms/schema/forms.schema';
import { OptionDto } from 'src/option/dto/option.dto';
import { Resource } from './../resource/schema/resource.schema';
import { ResourceGroup } from './../resource-group/schema/resource-group.schema';

@Injectable()
export class ListService {
  constructor(
    @InjectModel('List') private listModel: Model<List>,
    @InjectModel('Option') private optionModel: Model<Option>,
    @InjectModel('Forms') private formsModel: Model<Forms>,
    @InjectModel('Resource') private resourceModel: Model<Resource>,
    @InjectModel('ResourceGroup') private resourceGroupModel: Model<ResourceGroup>,
    private readonly utilService: UtilService,
  ) { }

  async create(listDto: ListDto, listOptionDto: OptionDto[], optionid): Promise<List> {
    const newOptions: OptionDto[] = await Promise.all(listOptionDto.map(async (optionDto) => {
      const option = new this.optionModel(optionDto);
      option.createdAt = this.utilService.dateToAAAAMMDD(new Date());
      option.updatedAt = null;
      option.status = 'created';
      const created = await option.save();
      return created;
    })
    )
    const optionsIds = newOptions.map((o) => (((Types.ObjectId(o._id)))))
    const createdList = new this.listModel(listDto);
    createdList.options = optionsIds;
    createdList.createdAt = this.utilService.dateToAAAAMMDD(new Date());
    createdList.updatedAt = null;
    createdList.type = optionid === '0' ? 'primary' : 'secondary'
    const created = await createdList.save();
    console.log(created);
    if (optionid !== '0') {
      let optionUpdate = await this.optionModel.findOneAndUpdate(
        { _id: optionid }, { lists: created._id }, { new: true }
      )
      console.log(optionUpdate)
    }
    return created;
  }

  async getSubLists(id) {
    return await this.listModel.find({ "_id": id }).exec();
  }


  async delete(id) {
    const listToDelete = await this.listModel.findOne({ "_id": id }).exec();
    if(listToDelete.options.length === 0){
      const optionEdit = await this.optionModel.update({ lists: listToDelete._id }, {$unset: {lists: 1}}).exec();
      return this.listModel.deleteOne({ "_id": id });
    }else {
      return { message: `No es posible eliminar, Se encontraron ${listToDelete.options.length} opciones asociadas` }
    }

  }

  async findAll(id = null, iteracion, path = null) {
    let lists = []
    let tmplists = []
    if (id == null && path === null) {
      tmplists = await this.listModel.find({ type: 'primary' }).sort({ name: 1 }).populate({ path: 'options', model: this.optionModel, }).exec();
      lists = _.orderBy(tmplists, [(l) => (l.name? l.name.toLowerCase() : '')], ['asc']);
    } else if (id !== null && path === null) {
      tmplists = await this.listModel.find({ _id: id }).sort({ name: 1 }).populate({ path: 'options', model: this.optionModel }).exec();
      lists = _.orderBy(tmplists, [(l) => (l.name? l.name.toLowerCase() : '')], ['asc']);
    } else if (path !== null) {
      tmplists = await this.listModel.find(path).sort({ name: 1 }).populate({ path: 'options', model: this.optionModel }).exec();
      lists = _.orderBy(tmplists, [(l) => (l.name? l.name.toLowerCase(): '')], ['asc']);
    }
    const newLists = await Promise.all(lists.map(async (l) => {
      const newOptions = await Promise.all(l.options.map(async (o) => {
        let objectReturn = {};
        if (typeof o.lists !== 'undefined') {
          objectReturn = {
            ...o._doc,
            ...{ lists: (await (this.findAll(o.lists, iteracion + 1, null)))[0] }
          }
        } else {
          objectReturn = { ...o._doc };
        }
        return objectReturn;
      }))
      const returnList = {
        ...l._doc,
        ...{ options: _.orderBy(newOptions, [(option) => (option.term.toLowerCase())], ['asc']) }
      }
      return returnList
    }))
    return newLists;
  }

  async findListForFrom(id = null, term = '', iteracion, path = null) {
    let lists = []
    if (id == null && path === null) {
      lists = await this.listModel
        .find({ type: 'primary' }, 'options lists')
        .sort({ name: 1 })
        .populate({ path: 'options', model: this.optionModel, })
        .exec();
    } else if (id !== null && path === null) {
      lists = await this.listModel
        .find({ _id: id }, 'options lists')
        .sort({ name: 1 })
        .populate({ path: 'options', model: this.optionModel })
        .exec();
    } else if (path !== null) {
      lists = await this.listModel
        .find({...{ type: 'primary'}, ...path}, 'options lists')
        .sort({ name: 1 })
        .populate({ path: 'options', model: this.optionModel })
        .exec();
    }
    const newLists = await Promise.all(lists.map(async (l) => {
      const newOptions = await Promise.all(l.options.map(async (o) => {
        let objectReturn = {};
        const valueSelect = term !== ''? `${term } - ${o.term}`: o.term;
        if (typeof o.lists !== 'undefined') {
          objectReturn = {
            ...o.term ? { name: o.term, value: valueSelect } : {},
            ...{ lists: (await (this.findListForFrom(o.lists, valueSelect, iteracion + 1, null)))[0] }
          }
        } else {
          objectReturn = { ...o.term ?  { name: o.term, value: valueSelect }: {} };
        }
        return objectReturn;
      }))
      const returnList = {
        ...l._doc,
        ...{ options: newOptions }
      }
      return returnList
    }))
    return newLists;
  }

  async getFlatList(idList) {
    const listaCompleta = await this.findListForFrom(idList, "", 1, null);
    
    const flatten = (arr) => {
      return arr.reduce((acc, cur) => acc.concat(Array.isArray(cur?.lists?.options) ? flatten(cur.lists.options) : cur), []);
    };
    const optionsValues =  flatten(listaCompleta[0].options).map((data)=>(data.value))
    //console.log("Listas", optionsValues.sort());
    return optionsValues.sort();
  }

  async getAlertCount(body) {
    let { origin, listDescription } = body;

    var fields_lists = [];
    var fields_destiny = [];
    var description = listDescription;
    var forms = await this.formsModel.find({}).exec();

    forms.forEach(function (doc) {
      doc.fields.forEach(function (field) {
        if ([
          "select",
          "select-multiple",
          "select-recursive",
          "select-multiple2"
        ]
          .includes(field.type) &&
          field.id === description &&
          fields_lists.indexOf(field.list) === -1) {
          fields_lists.push(field.list);
          fields_destiny.push(field.destiny);
        }
      });
    });

    // Solo se está trayendo una lista.

    let optionsValues = [];
    if (fields_lists.length > 0) {
      var lista = await this.listModel.findOne({ "description": fields_lists[0] }).exec();
      for (let i = 0; i < lista.options.length; i++) {
        var op = await this.optionModel.findOne({ _id: lista.options[i] }).exec();
        if(op){
          optionsValues.push(op.term);
        }
      }
    }

    const query = {
      "origin": {
        $eq: origin
      },
      "extra.status": {
        $ne: 'deleted'
      }
    };

    let numberOptions = 0;
    let resourcesNotValid = [];
    let resourceGroupsNotValid = [];
    let sinEspecificar = 0;

    if (fields_lists && fields_lists.length > 0 && fields_destiny && fields_destiny.length > 0 ) {

      var recursos = await this.resourceModel.find(query).exec();
      recursos.forEach((element) => {
        var docTmp = element;
        var campo = fields_destiny[0];
        campo.split(".").forEach(
          element2 => docTmp = docTmp[element2]
        );

        // Solo toma el campo si es lista

        if (docTmp && Array.isArray(docTmp)) {
          docTmp.forEach(
            (element3) => {
              if (!(optionsValues.includes(element3))) {
                numberOptions++;
                resourcesNotValid.push(element.ident);
                element3 == "Sin especificar" ? sinEspecificar++ : sinEspecificar
              }
            }
          )
        }
      });

      var resourceGroups = await this.resourceGroupModel.find(query).exec();

      resourceGroups.forEach((element) => {
        var docTmp = element;
        var campo = fields_destiny[0];
        campo.split(".").forEach(
          (element2) => {
            if(docTmp){
              docTmp = docTmp[element2];
            }
          }
        );
        if (docTmp && Array.isArray(docTmp)) {
          docTmp.forEach(
            (element3) => {
              if (!(optionsValues.includes(element3))) {
                numberOptions++;
                resourceGroupsNotValid.push(element.ident);
                element3 == "Sin especificar" ? sinEspecificar++ : sinEspecificar
              }
            }
          )
        }
      });
    }

    let resourcesDistinct = resourcesNotValid.filter((item, index) => {
      return resourcesNotValid.indexOf(item) === index;
    })
    let numberResourcesNotValid = resourcesDistinct.length;
    let resourcesGroupDistinct = resourceGroupsNotValid.filter((item, index) => {
      return resourceGroupsNotValid.indexOf(item) === index;
    })
    let numberResourcesGroupNotValid = resourcesGroupDistinct.length;

    return {
      sinEspecificar: sinEspecificar,
      numberRGroupNotValid: numberResourcesGroupNotValid,
      numberResourcesNotValid: numberResourcesNotValid,
      numberOptions: numberOptions,
      resourcesNotValid: resourcesDistinct,
      resourceGroupsNotValid: resourcesGroupDistinct,
      destiny: fields_destiny? fields_destiny[0] : null,
    };

  }

  async getAllLists() {
    return await this.findAll(null, 1, null);
  }

  async findListBy(filter) {
    const temporalList = await this.listModel.findOne(filter)
      .populate({
        path: 'options', model: this.optionModel,
        populate: { path: 'lists', model: this.listModel }
      }).exec();
    if (temporalList) {
      return temporalList;
    } else {
      return { error: "No se encontraron registros" }
    }
  }

  async updateList(body, id) {
    let options = []
    if (body.newOptions) {
      const newO: OptionDto[] = await Promise.all(body.newOptions.map(async (optionDto) => {
        const option = new this.optionModel(optionDto);
        option.createdAt = this.utilService.dateToAAAAMMDD(new Date());
        option.updatedAt = null;
        option.status = 'created';
        const created = await option.save();
        return created;
      })
      )
      const optionsIds = newO.map((o) => ((Types.ObjectId(o._id))))
      options = [...optionsIds, ...body.options.map((data) => (Types.ObjectId(data._id)))]
    }

    if (body.deleteOptions) {
      const deleteO: OptionDto[] = await Promise.all(body.deleteOptions.map(async (delO) => {
        return await this.optionModel.deleteOne({ "_id": delO });
      })
      )
      options = _.xor(options, body.deleteOptions)
    }

    const newBody = {
      ...body,
      ...{
        options: options
      }
    }
    return await this.listModel.findOneAndUpdate(
      { _id: id }, newBody, { new: true }
    )
  }

  async getOptionsListBy(filter) {
    const temporalList = await this.listModel.findOne(filter)
      .populate({
        path: 'options', model: this.optionModel,
        populate: { path: 'lists', model: this.listModel }
      }).exec();
    console.log(temporalList);
    if (temporalList) {
      return temporalList.options.map((o: OptionDto) => (o.term))
    } else {
      return { error: "No se encontraron registros" }
    }
  }

}

