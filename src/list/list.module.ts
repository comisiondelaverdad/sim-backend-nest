import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';

import { ConfigModule } from '../config/config.module';
import { ListController } from './list.controller';
import { ListService } from './list.service';
import { ListSchema } from './schema/list.schema';
import { UtilService } from 'src/util/util.service';
import { OptionModule } from 'src/option/option.module';
import { FormsModule } from 'src/forms/forms.module';
import { ResourceModule } from 'src/resource/resource.module';
import { ResourceGroupModule } from 'src/resource-group/resource-group.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'List', schema: ListSchema }], "database"),
    ConfigModule,
    ListModule,
    forwardRef(() => ResourceGroupModule),
    forwardRef(() => ResourceModule),
    forwardRef(() => FormsModule),
    forwardRef(() => OptionModule),
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [ListController],
  providers: [ ListService,UtilService],
  exports:[ ListService,MongooseModule ]
})
export class ListModule { }
