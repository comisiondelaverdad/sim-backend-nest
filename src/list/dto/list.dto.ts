export class ListDto {
  _id?: String;
  name: String;
  description: String;
  options: Array<String>;
  type: String;
  path: String;
  createdAt: String;
  updatedAt: String;
}