import {
  Controller, HttpCode, Query, Request, Post, Body, UseGuards, Delete, Get, Param, Options, Put
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ListService } from './list.service';
import { matchRolesList } from '../auth/roles.guard';


export let DESTINATION_INTERNAL_FILES = ""

export class ResponseCount {
  count: number;
  to: Date;
  from?: Date;
}
import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('list')
@Controller('api/list')
export class ListController {

  constructor(
    private listService: ListService) {
  }

  @Get('')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async getList(@Request() req, @Query() query, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor', 'tesauro']) //Validación de roles del usuario
    let newList = await this.listService.getAllLists();
    return newList;
  }

  @Get('byname/:name')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async getByName(@Request() req, @Param('name') name: string, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor','invitado', 'tesauro']) //Validación de roles del usuario
    return this.listService.findListBy({ name: name });
  }
  
  @Get('byid/:id')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async getByid(@Request() req, @Param('id') id: string, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor', 'tesauro'])  //Validación de roles del usuario
    return this.listService.findListBy({ _id: id });
  }

  @Delete(':idList')
  @UseGuards(AuthGuard())
  async delete(@Param('idList') idList: string) {
    return await this.listService.delete(idList);
  }

  @Put('')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async updateList(@Request() req, @Query() query, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor', 'tesauro']) //Validación de roles del usuario
    let newList = await this.listService.updateList(body.body, body.id);
    return newList;
  }
  @Post('')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async postList(@Request() req, @Query() query, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor', 'tesauro']) //Validación de roles del usuario
    let newList = await this.listService.create(body, body.newOptions, body.optionid);
    return newList;
  }

  @Post('alert')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async postAlert(@Request() req, @Query() query, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor', 'tesauro']) //Validación de roles del usuario
    let countAlert = await this.listService.getAlertCount(body);
    return countAlert;
  }

}
