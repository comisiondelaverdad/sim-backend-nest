import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as SchemaM } from 'mongoose';
//import * as mongoosePaginate from 'mongoose-paginate';

@Schema()
export class List extends Document {
  @Prop({type:"string"})
  name: string;

  @Prop({type:"string"})
  description: string;

  @Prop({type:"string"})
  type: string;

  @Prop({type:"string"})
  path: string;

  @Prop()
  options: SchemaM.ObjectId[];

  @Prop({ type: Date, format: 'date-time' })
  createdAt: Date;

  @Prop({ type: Date, format: 'date-time' })
  updateAt: Date;

}


export const ListSchema = SchemaFactory.createForClass(List);
//ResourceSchema.plugin(mongoosePaginate);
