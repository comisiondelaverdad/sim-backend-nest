import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RecordController } from './record.controller';
import { RecordService } from './record.service';
import { RecordSchema } from '../record/schema/record.schema';
import { UtilService } from './../util/util.service'
import { PassportModule } from '@nestjs/passport';
import { LogsService } from 'src/logs/logs.service';
import { LogsSchema } from 'src/logs/schema/logs.schema';
import { ConfigService } from 'src/config/config.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Record', schema: RecordSchema }], "database"),
    MongooseModule.forFeature([{ name: 'Logs', schema: LogsSchema }], "mongologs"),
    HttpModule,
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [RecordController],
  providers: [RecordService, LogsService, ConfigService, UtilService],
  exports: [RecordService, MongooseModule]
})
export class RecordModule { }