import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Record extends Document {

  @Prop({type:"string"})
  ident: string;

  @Prop({type:"string"})
  identifier: string;

  @Prop({type:"string"})
  identifier_II: string;

  @Prop({type:"string"})
  filename: string;

  @Prop({type:"string"})
  origin: string;

  @Prop({type:"string"})
  type: string;

  @Prop({type:"object"})
  extra: object;

  @Prop({type:"object"})
  metadata: object;

  @Prop({type:"object"})
  labelled: object;

  @Prop({type:"string"})
  content: string;

  @Prop({type:"object"})
  pre_process: object;

  @Prop({type:"string"})
  textExtraction:string;
  
  @Prop({type:"string"})
  subtitle: string;

}


export const RecordSchema = SchemaFactory.createForClass(Record);




