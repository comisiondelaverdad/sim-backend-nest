import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Record } from "./schema/record.schema";
import * as XLSX from "xlsx";
import * as CSV from "csvtojson";
import fs = require("fs");
import filePath = require('path');
import dirTree = require("directory-tree");
import AdmZip = require("adm-zip");
const execSync = require('child_process').execSync;
import { LogsService } from '../logs/logs.service';
import { CreateLogDto } from '../logs/dto/create-log.dto';
import { Types } from 'mongoose';
import { ConfigService } from '../config/config.service';

function objectIdWithTimestamp(timestamp) {
    /* Convert string date to Date object (otherwise assume timestamp is a date) */
    if (typeof (timestamp) === 'string') {
        timestamp = new Date(timestamp);
    }
    console.log({ timestamp });
    /* Convert date object to hex seconds since Unix epoch */
    const hexSeconds = Math.floor(timestamp / 1000).toString(16);
    /* Create an ObjectId with that hex timestamp */
    const constructedObjectId = Types.ObjectId(hexSeconds + "0000000000000000");
    return constructedObjectId;
}

@Injectable()
export class RecordService {
  constructor(
    @InjectModel('Record') private recordModel: Model<Record>,
    private readonly logs: LogsService,
    private configService: ConfigService,
  ) {}

  async conteoTemporalCatalogacionFuentesInternas(
    to: string,
    from: string,
  ): Promise<Record[]> {
    return await this.recordModel
      .count({
        _id: {
          $gt: objectIdWithTimestamp(to),
          $lt: objectIdWithTimestamp(from),
        },
      })
      .where({ origin: 'CatalogacionFuentesInternas' })
      .exec();
  }

  async readImage(filename: any, res, req) {
    filename = Buffer.from(filename, 'base64').toString('binary');

    if (fs.existsSync(filename)) res.sendFile(filename, { dotfiles: 'allow' });
    else
      res
        .status(500)
        .send('El archivo solicitado no se encuentra disponible...');
  }

  async readImageService(id: String, size: String, res, req) {
    // filename = Buffer.from(filename, 'base64').toString('binary')

    this.logs.loggerRecord(req.user, {
      idmongo: id,
      size: size,
      type: 'image',
    });

    let filename = '/rep/resizes/' + id;

    if (size) filename += '_' + size + '.jpg';
    else filename += '_large.jpg';

    if (fs.existsSync(filename)) {
      res.sendFile(filename, { dotfiles: 'allow' });
    } else
      res
        .status(500)
        .send('El archivo solicitado no se encuentra disponible...');
  }

  async readFileStream(
    hash: String,
    id: String,
    format: String,
    support: String,
    res,
    req,
  ) {
    // filename = Buffer.from(filename, 'base64').toString('binary')
    let filename = '';
    if (support.toLowerCase() === 'video') {
      let format_ = format;
      if (format === 'ogv') format_ = 'ogg';
      filename =
        '/rep/export_' + support.toLowerCase() + '/' + id + '.' + format;
      fs.stat(filename, (err, stats) => {
        if (err) {
          if (err.code === 'ENOENT') {
            return res.sendStatus(404);
          }
          res.end(err);
        }

        const videoSize = stats.size;
        let range = req.headers.range;
        if (!range) range = 'bytes=0-';

        const CHUNK_SIZE = 10 ** 6;
        const start = Number(range.replace(/\D/g, ''));
        const end = Math.min(start + CHUNK_SIZE, videoSize - 1);

        const range_ = `bytes ${start}-${end}/${videoSize}`;
        console.log(range_);
        const contentLength = end - start + 1;
        const headers = {
          'Content-Range': range_,
          'Accept-Ranges': 'bytes',
          'Content-Length': contentLength,
          'Content-Type': 'video/' + format_,
        };

        res.writeHead(206, headers);

        const readableStream = fs.createReadStream(filename, { start, end });

        this.logs.loggerRecord(hash, {
          idmongo: id,
          format: format,
          type: 'streaming',
        });
        readableStream.pipe(res);
      });
    } else if (support.toLowerCase() === 'audio') {
      filename =
        '/rep/export_' + support.toLowerCase() + '/' + id + '.' + format;

      fs.stat(filename, (err, stats) => {
        if (err) {
          if (err.code === 'ENOENT') {
            return res.sendStatus(404);
          }
          res.end(err);
        }

        const audioSize = stats.size;
        let range = req.headers.range;
        if (!range) range = 'bytes=0-';

        const CHUNK_SIZE = 10 ** 6;
        const start = Number(range.replace(/\D/g, ''));
        const end = Math.min(start + CHUNK_SIZE, audioSize - 1);

        const range_ = `bytes ${start}-${end}/${audioSize}`;
        console.log(range_);
        const contentLength = end - start + 1;
        const headers = {
          'Content-Range': range_,
          'Accept-Ranges': 'bytes',
          'Content-Length': contentLength,
          'Content-Type': 'audio/' + format,
        };

        res.writeHead(206, headers);

        const readableStream = fs.createReadStream(filename, { start, end });

        this.logs.loggerRecord(hash, {
          idmongo: id,
          format: format,
          type: 'streaming',
        });

        readableStream.pipe(res);
      });
    }
  }

  async readFile(id: String, res, req, zipName?) {
    console.log('ID a consultar: ' + id);
    return this.recordModel
      .findOne({
        _id: Types.ObjectId(id),
      })
      .exec()
      .then((record) => {
        console.log('Consultado por ID');
        try {
          let file = '';
          let ext = '';
          let name = '';
          var sended = false;

          if (zipName) {
            ext = zipName.split('.').pop().toLowerCase();
            name =
              record.filename.replace('.' + ext, '') +
              '/' +
              zipName.replace('.' + ext, '');
            
            file = zipName;
              let charReplace =  filePath.sep;
              let char = "\\";
              file = zipName.replace(char,charReplace); 
              console.log("por acá está el file: ",file)
              
            //}
            name = zipName.replace('.' + ext, '');
          } else {
            ext = record.filename.split('.').pop().toLowerCase();
            name = record.filename.replace('.' + ext, '');
            file = record.filename;
          }
          if (
            fs.existsSync(file) ||
            fs.existsSync(name) ||
            fs.existsSync(name + '_64k.mp3')
          ) {
            // XLS
            if (['xls'].indexOf(ext) !== -1) {
              if (fs.existsSync(name + '.pdf')) {
                sended = true;
                res.sendFile(name + '.pdf', { dotfiles: 'allow' });
              } else {
                if (fs.existsSync(name + '.json')) {
                  sended = true;
                  res.sendFile(name + '.json', { dotfiles: 'allow' });
                } else {
                  if (fs.existsSync(name + '.xlsx')) {
                    ext = 'xlsx';
                    file = name + '.xlsx';
                  } else {
                    let doc = file;
                    if (record.origin === 'Microdato') {
                      doc = name;
                      let cmd =
                        'lowriter --headless --norestore --convert-to xlsx "' +
                        doc +
                        '" --outdir "' +
                        filePath.dirname(doc) +
                        '"';
                      execSync(cmd);
                      ext = 'xlsx';
                      file = name + '.xlsx';
                    } else {
                      let cmd =
                        'lowriter --headless --norestore --convert-to pdf "' +
                        doc +
                        '" --outdir "' +
                        filePath.dirname(doc) +
                        '"';
                      execSync(cmd);
                      sended = true;
                      res.sendFile(name + '.pdf', { dotfiles: 'allow' });
                    }
                  }
                }
              }
            }

            // XLSX.
            if (['xlsx'].indexOf(ext) !== -1) {
              if (fs.existsSync(name + '.json')) {
                sended = true;
                res.sendFile(name + '.json', { dotfiles: 'allow' });
              } else {
                let json_data = {};
                var workbook = null;

                if (record.origin === 'Microdato') {
                  workbook = XLSX.readFile(name);
                } else {
                  workbook = XLSX.readFile(file);
                }

                if (workbook.SheetNames.length === 1) {
                  workbook.SheetNames.forEach((name) => {
                    let sheet = XLSX.utils.sheet_to_json(
                      workbook.Sheets[name],
                      { header: 1, defval: '' },
                    );
                    if (sheet.length) {
                      json_data = sheet.filter((x: any) => x.length > 0);
                    }
                  });
                  fs.writeFileSync(name + '.json', JSON.stringify(json_data));
                  sended = true;
                  res.sendFile(name + '.json', { dotfiles: 'allow' });
                } else {
                  if (record.origin === 'Microdato') {
                    sended = true;
                    res.sendFile(name, { dotfiles: 'allow' });
                  } else {
                    sended = true;
                    res.sendFile(file, { dotfiles: 'allow' });
                  }
                }
              }
            }

            // CSV
            if (['csv'].indexOf(ext) !== -1) {
              if (fs.existsSync(name + '.json')) {
                sended = true;
                res.sendFile(name + '.json', { dotfiles: 'allow' });
              } else {
                let json_csv = CSV({
                  output: 'csv',
                  noheader: true,
                  delimiter: [',', ';'],
                }).fromFile(name);
                let json_data = json_csv;
                fs.writeFileSync(name + '.json', JSON.stringify(json_data));
                sended = true;
                res.sendFile(name + '.json', { dotfiles: 'allow' });
              }
            }

            // DOC, DOCX, PPT, PPTX, ODT, ODP
            if (
              ['rtf', 'doc', 'docx', 'ppt', 'pptx', 'odt', 'odp'].indexOf(
                ext,
              ) !== -1
            ) {
              if (fs.existsSync(name + '.pdf')) {
                sended = true;
                res.sendFile(name + '.pdf', { dotfiles: 'allow' });
              } else {
                let convertir = record.origin === 'Microdato' ? name : file;
                var cmd =
                  'lowriter --headless --norestore --convert-to pdf "' +
                  convertir +
                  '" --outdir "' +
                  filePath.dirname(convertir) +
                  '"';
                execSync(cmd);
                sended = true;
                res.sendFile(name + '.pdf', { dotfiles: 'allow' });
              }
            }

            // AUDIO
            if (['wav', 'mp3', 'mp4', 'm4a', 'wma'].indexOf(ext) !== -1) {
              if (fs.existsSync(name + '_64k.mp3')) {
                sended = true;
                res.sendFile(name + '_64k.mp3', { dotfiles: 'allow' });
              } else {
                sended = true;
                res.sendFile(file, { dotfiles: 'allow' });
              }
            }

            // IMAGENES
            if (
              [
                'tiff',
                'tif',
                'gif',
                'png',
                'jpg',
                'jpeg',
                'bmp',
                'ico',
                'wmf',
                'hdr',
              ].indexOf(ext) !== -1
            ) {
              if (fs.existsSync(name + '.pdf')) {
                sended = true;
                res.sendFile(name + '.pdf', { dotfiles: 'allow' });
              } else {
                let cmd = "convert '" + file + "' '" + name + ".pdf'";
                execSync(cmd);
                sended = true;
                res.sendFile(name + '.pdf', { dotfiles: 'allow' });
              }
            }

            // Comprimidos ZIP
            if (['zip'].indexOf(ext) !== -1) {
              if (fs.existsSync(name + '.json')) {
                sended = true;
                res.sendFile(name + '.json', { dotfiles: 'allow' });
              } else {
                let zipFile = new AdmZip(file);
                zipFile.extractAllTo(name, false);
                let json_tree = (entry) => {
                  entry._id = id;
                  entry.text = entry.name;
                  entry.fileName = entry.path.replace(name + '/', '');
                  entry.isDirectory = 0;
                  if (entry.children && entry.children.length > 0) {
                    entry.isDirectory = 1;
                    entry.children.forEach((child) => {
                      json_tree(child);
                    });
                  }
                };
                let tree = dirTree(name).children;
                tree.forEach((child) => {
                  json_tree(child);
                });
                fs.writeFileSync(name + '.json', JSON.stringify(tree));
                sended = true;
                res.sendFile(name + '.json', { dotfiles: 'allow' });
              }
            }

            // TODO LO DEMÁS
            if (!sended) {
              if (record.origin === 'Microdato') {
                res.sendFile(name, { dotfiles: 'allow' });
              } else {
                // si se usa una ruta relativa se tiene que colocar en este punto la ruta completa a donde esta el desde la raiz
                const fullFilePath = this.configService.get('FILE_FULL_PATH')
                  ? `${this.configService.get('FILE_FULL_PATH')}${file}`
                  : file;
                res.sendFile(filePath.resolve(fullFilePath), { dotfiles: 'allow' });
              }
            }
          } else {
            res
              .status(500)
              .send('El archivo solicitado no se encuentra disponible...');
          }
        } catch (error) {
          // Agregar al LOG
          console.log(error);
        } finally {
          let date = new Date().toISOString().split('T').join(' ');
          let logInfo: CreateLogDto = {
            user: req.user._id,
            username: req.user.username,
            from: 'records',
            action: 'Read Records',
            message: '',
            metadata: {
              ident: req.params.id,
              file: {
                filename: record.filename,
                extension: record.filename.split('.').pop().toLowerCase(),
              },
              origin: record.origin,
              ip: req.ip
                ? req.ip
                : req.connection.remoteAddress
                ? req.connection.remoteAddress
                : req.headers['x-forwarded-for']
                ? req.headers['x-forwarded-for']
                : '',
            },
            createdAt: date,
            updatedAt: date,
          };
          this.logs.create(logInfo);
          this.logs.logger('app', logInfo);
        }
      });
  }

  async readPreProcess(id: String): Promise<Record> {
    return await this.recordModel
      .findOne({ ident: id }, { pre_process: 1 })
      .exec();
  }

  async readZipFile(body: any, res, req) {
    console.log(body)
    if(body.id.includes('-')){
      var record = await this.getInfoRecordByIdent(body.id);
      return await this.readFile(record["_id"],res,req,body.name)
    }else{
      return await this.readFile(body.id, res, req, body.name);
    }
  }
  
  async getInfoRecordByIdent(ident:any){
    try{
      const response = await this.recordModel.findOne({
        ident: ident,
      })
      .exec();
      return response;
    }catch(error){
      console.log("Error al obtener record by ident")
      return error;
    }
    
  }

  async getInfoRecordById(id: any) {
    try {
      const response = await this.recordModel
        .findById(
        id
        )
        .exec();
      // if(response._id)
      // response.idMongo = response._id
      console.log('DIEGO------------------RESPONSE')
      return response;
    } catch (error) {
      console.log('ERROR DIEGO------------------RESPONSE')

      return error;
    }
  }
}