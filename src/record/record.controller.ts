import { Body, Controller, Get, Param, Post, Query, Req, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UtilService } from 'src/util/util.service';
import { RecordService } from "./record.service"

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('records')
@Controller('api/records')
export class RecordController {
    constructor(
        private utils: UtilService,
        private readonly recordsService: RecordService
    ) { }

    @Get('file/:id')
    @UseGuards(AuthGuard())
    async readFile(@Param('id') id: String, @Res() res, @Req() req) {
        console.log("ID recibido: " + id);

        await this.recordsService.readFile(id, res, req);
    }
    @Get('image/:filename')
    @UseGuards(AuthGuard())
    async readImage(@Param('filename') filename: String, @Res() res, @Req() req) {

        await this.recordsService.readImage(filename, res, req);
    }

    // Este servicio es el nuevo servicio para las imágenes, recibe el id de mongo del record
    @Get('imageservice/:id/:size')
    async readImageService(@Param('id') id: String, @Param('size') size: String, @Res() res, @Req() req) {
        await this.recordsService.readImageService(id, size, res, req);
    }

    // Streaming archivo
    @Get('streamservice/:hash/:id/:format/:support')
    async getFileStream(@Param('hash') hash: String,@Param('id') id: String, @Param('format') format: String, @Param('support') support: String, @Res() res, @Req() req) {

        await this.recordsService.readFileStream(hash, id, format, support, res, req);
    }

    @Get('conteoTemporalCatalogacionFuentesInternas/:to/to')
    async conteoTemporalCatalogacionFuentesInternas(@Param() params, @Query() query) {
        return await this.recordsService.conteoTemporalCatalogacionFuentesInternas(params.to, query.from).then((c) => {
            return { 'count': c, 'to': params.to, 'from': query.from };
        }).catch((err) => {
            console.log("Error conteoTemporalCatalogacionFuentesInternas", { err })
        });
    }

    @Post('zip-file')
    @UseGuards(AuthGuard())
    async readZipFile(@Body() body, @Res() res, @Req() req) {
        await this.recordsService.readZipFile(body, res, req);
    }

    @Get('pre_process/:id')
    @UseGuards(AuthGuard())
    async readPreProcess(@Param('id') id: String) {
        return await this.recordsService.readPreProcess(id);
    }

    @Get('infoRecord/:id')
    @UseGuards(AuthGuard())
    async getInfoRecordById(@Param('id') id: String) {
        return await this.recordsService.getInfoRecordById(id);
    }

    
}
