import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SlugIdentService } from './slug-ident.service';
//import { ConfigModule } from '../config/config.module';
import { ResourceSchema } from '../resource/schema/resource.schema'

@Module({
  imports: [
    //ConfigModule
    MongooseModule.forFeature([{ name: 'Resource', schema: ResourceSchema }], "database"),
  ],
  providers: [SlugIdentService],
  exports: [SlugIdentService, MongooseModule] 
})
export class SlugIdentModule {}
