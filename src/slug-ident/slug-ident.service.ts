import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Resource } from '../resource/schema/resource.schema';
import slugify from 'slugify';
import * as rs from 'randomstring';

@Injectable()
export class SlugIdentService {

    constructor(
        @InjectModel('Resource') private resourceModel: Model<Resource>
    ) {}

    async getAllResources() {
        let resources = await this.resourceModel
            .find({
                "extra.status": {"$ne": "deleted"}, 
                "origin": "CatalogacionFuentesInternas", 
                "metadata.firstLevel.title": {"$exists": true}, 
                "metadata.firstLevel.accessLevel": {"$in": ["4", 4]}, 
                "metadata.simpleident": {"$exists": true}, 
                "metadata.slug": {"$exists": true}
            },
            { 'metadata.simpleident': 1, 'metadata.slug' : 1 })
            .exec();
        return resources;
    }

    checkSlug(resources : any, stringCompare : string) {
        return resources.filter(r => r.metadata.slug === stringCompare).length > 0;
    }

    async getSlug(resources : any, title : string) {
        let newSlug : string = slugify(title, { lower: true });
        let offset : number = 1;
        let slugTemp : string = newSlug;
        while(this.checkSlug(resources, newSlug)) {
            offset += 1;
            newSlug = slugTemp.concat('-', offset.toString());
        }
        return newSlug;
    }

    getRandomString() : string {
        return rs.generate({ length: 8, charset: 'alphanumeric', capitalization: 'uppercase'});
    }

    checkSimpleIdent(resources : any, stringCompare : string) {
        return resources.filter(r => r.metadata.simpleident === stringCompare).length > 0;
    }

    async getSimpleIdent(resources : any) {
        let simpleident : string = this.getRandomString();
        while(this.checkSimpleIdent(resources, simpleident)) simpleident = this.getRandomString();
        return simpleident;
    }
}