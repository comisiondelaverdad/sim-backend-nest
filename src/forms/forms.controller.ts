import {
  Controller, HttpCode, Query, Request, Post, Body, UseGuards, Delete, Get, Param
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FormsService } from './forms.service';
import { matchRolesList } from '../auth/roles.guard';
import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('forms')
@Controller('api/forms')
export class FormsController {

  constructor(private formService: FormsService) {
  }

  @Get('byname/:name')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async getByName(@Request() req, @Param('name') name, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor', 'catalogador']) //Validación de roles del usuario
    return this.formService.getForm(name);
  }

  @Get('byorigin/:origin')
  @HttpCode(200)
  //@UseGuards(AuthGuard())
  async getByOrigin(@Request() req, @Param('origin') origin, @Body() body): Promise<any> {
    //matchRolesList(req.user, ['admin', 'catalogador_gestor', 'catalogador']) //Validación de roles del usuario
    return this.formService.getFormByOrigin(origin);
  }

  @Get('')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async getall(@Request() req, @Param('name') name, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor', 'catalogador']) //Validación de roles del usuario
    return await this.formService.findAll();
  }

  @Get('statment/:name')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async getFormStatment(@Request() req, @Param('name') name): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor', 'catalogador']) //Validación de roles del usuario
    return this.formService.getFormStatment(name);
  }

  @Post('')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async newForm(@Request() req, @Query() query, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor']) //Validación de roles del usuario
    let newForm = await this.formService.create(body);
    return newForm;
  }

  @Post('updateForm')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async updateForm(@Request() req, @Query() query, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor']) //Validación de roles del usuario
    let newForm = await this.formService.updateForm(body);
    return newForm;
  }

  @Post('getPreviewByForm')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async getPreviewByForm(@Request() req, @Query() query, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor']) //Validación de roles del usuario
    let newForm = await this.formService.getPreviewByForm(body);
    return newForm;
  }

}
