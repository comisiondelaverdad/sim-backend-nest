import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { fieldDto } from '../dto/forms.dto';

@Schema()
export class Forms extends Document {
  @Prop({type:"string"})
  name: string;

  @Prop({type:"string"})
  label: string;

  @Prop({type:"string"})
  description: string;

  @Prop({type:"date"})
  createdAt: Date;

  @Prop({type:"date"})
  updatedAt: Date;

  @Prop({type:"array"})
  fields: fieldDto[];

  @Prop({type:"array"})
  origin: string[];

  @Prop({type:"string"})
  ruleField: String;

}


export const FormsSchema = SchemaFactory.createForClass(Forms);