import { Injectable, HttpService, HttpException, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Forms } from './schema/forms.schema';
import * as _ from "lodash";
import { FormsDto } from './dto/forms.dto';
import { ListService } from 'src/list/list.service';
import { UtilService } from 'src/util/util.service';
@Injectable()
export class FormsService {
  constructor(
    @InjectModel('Forms') private formsModel: Model<Forms>,
    readonly listsService: ListService,
    readonly utilService: UtilService
  ) { }

  async create(formsDto: FormsDto): Promise<Forms> {
    const createdForm = new this.formsModel(formsDto);
    createdForm.createdAt = new Date();
    createdForm.updatedAt = new Date();
    const created = await createdForm.save();
    return created;
  }

  async findAll(): Promise<Forms[]> {
    return await this.formsModel.find({}).exec();
  }

  async getPreviewByForm(form) {
    const newOptions = await Promise.all(
      form.fields.map(async (field) => {
        return await {
          ...field,
          ... (['select', 'select-recursive', 'select-multiple2', 'select-multiple2-recursive'].includes(field.type)) ?
            field.list !== 'undefined' ?
              ((await this.listsService.findListForFrom(null, '', 1, { "path": field.list }))[0])
              : {} : {}
        }
      })
    )
    return newOptions
  }

  async getForm(name) {
    const filter = { "name": name }
    const form = await this.formsModel.findOne(filter);
    const newOptions = await this.getPreviewByForm(form)
    return newOptions
  }

  async getFormByOrigin(origin) {
    const form = await this.formsModel.findOne({ "origin": origin });
    if (form) {
      form.fields = await this.getPreviewByForm(form);
      return form;
    } else {
      throw new HttpException({
        status: HttpStatus.NOT_FOUND,
        error: `No existen formularios para la gestión de registros con origen ${origin}`,
      }, HttpStatus.NOT_FOUND);
    }
  }


  async updateForm(formData: Forms) {
    const createdForm = new this.formsModel(formData);
    createdForm.updatedAt = new Date();
    return await this.formsModel.findOneAndUpdate(
      { _id: createdForm._id }, createdForm, { new: true }
    )
  }

  async getFormWithOriginStatment(name) {
    const filter = { "name": name }
    const form = await this.formsModel.findOne(filter);

    const statment = await Promise.all(
      form.fields
        .filter((field) => (!(['separator', 'file'].includes(field.type))))
        .map((field) => {
          return {
            defaultValue: field.defaultValue,
            destiny: field.destiny,
            origin: field.id,
            type: field.type,
            label: field.label,
            typeDestiny: field.typeDestiny ? field.typeDestiny : false
          }
        })
    )
    return { statment: statment, origin: form.origin }
  }

  async getFormStatment(name) {
    const filter = { "name": name }
    const form = await this.formsModel.findOne(filter);

    const statment = await Promise.all(
      form.fields
        .filter((field) => (!(['separator', 'file'].includes(field.type))))
        .map((field) => {
          return {
            defaultValue: field.defaultValue,
            destiny: field.destiny,
            origin: field.id,
            type: field.type,
            label: field.label,
            typeDestiny: field.typeDestiny ? field.typeDestiny : false
          }
        })
    )
    return statment
  }

  async getIdFormStatment(name) {
    const filter = { "name": name }
    const form = await this.formsModel.findOne(filter);
    const statment = await Promise.all(
      form.fields
        .filter((field) => (!(['separator', 'file'].includes(field.type))))
        .map((field) => {
          return {
            origin: field.id
          }
        })
    )
    return statment
  }

}

