export class fieldDto {
  _id?: String;
  id: String;
  defaultValue: any;
  label:String;
  type: String;
  col:number;
  info: String;
  pattern: String;
  authorConfig: any;
  min: any;
  max: any;
  rows: number;
  placeholder: any;
  requied: boolean;
  infoHTML:String;
  destiny: String;
  options: Array<String>;
  ruleToShow: Array<String>;
  hide: boolean;
  createdAt: Date;
  updatedAt: Date;
  state: String;
}

export class FormsDto {
  _id?: String;
  name: String;
  label: String;
  description: String;
  createdAt: String;
  updatedAt: String;
  fields: Array<fieldDto>;
  origin: Array<String>;
  ruleField: String;
}