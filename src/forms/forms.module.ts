import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';

import { ConfigModule } from '../config/config.module';
import { FormsController } from './forms.controller';
import { FormsService } from './forms.service';
import { FormsSchema } from './schema/forms.schema';
import { UtilService } from 'src/util/util.service';
import { ListModule } from 'src/list/list.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Forms', schema: FormsSchema }], "database"),
    ConfigModule,
    ListModule,
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [FormsController],
  providers: [FormsService, UtilService], 
  exports:[FormsService, MongooseModule]
})
export class FormsModule { }
