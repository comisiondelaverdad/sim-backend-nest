import { Test, TestingModule } from '@nestjs/testing';
import { MetadataStandarizationService } from './metadata-standarization.service';

describe('HomologateService', () => {
  let service: MetadataStandarizationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MetadataStandarizationService],
    }).compile();

    service = module.get<MetadataStandarizationService>(MetadataStandarizationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
