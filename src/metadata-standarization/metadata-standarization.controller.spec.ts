import { Test, TestingModule } from '@nestjs/testing';
import { MetadataStandarizationController } from './metadata-standarization.controller';

describe('MetadataStandarizationController', () => {
  let controller: MetadataStandarizationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MetadataStandarizationController],
    }).compile();

    controller = module.get<MetadataStandarizationController>(MetadataStandarizationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
