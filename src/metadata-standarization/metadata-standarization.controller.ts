import {
    Controller, Inject, UseInterceptors, UploadedFiles,
    UploadedFile, HttpCode, Get, Param, Query, Request, Post, Body, UseGuards, Put, Delete
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { MetadataStandarization } from './schema/metadata-standarization.schema';
import { MetadataStandarizationService } from './metadata-standarization.service';
import { EditMetadataStandarization } from './schema/edit-metadata-standarization.dto';



import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('metadata-standarization')
@Controller('api/metadata-standarization')
export class MetadataStandarizationController {

    constructor(
        private readonly metadataStandarizationService: MetadataStandarizationService
        ) {}

    @Get()
    @UseGuards(AuthGuard('jwt'))
    async find(@Request() req, @Query() query): Promise<MetadataStandarization[]> {
        //return "Algo aca";
        return this.metadataStandarizationService.find(query);
    }

    @Get(':id')
    @UseGuards(AuthGuard('jwt'))
    async findOne(@Request() req, @Param('id') id): Promise<MetadataStandarization> {
      return this.metadataStandarizationService.findByid(id);
    }

    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    async update(@Request() req, @Param("id") id, @Body() editTermDto: EditMetadataStandarization) {
      await this.metadataStandarizationService.update(id, editTermDto, req);
    }

}
