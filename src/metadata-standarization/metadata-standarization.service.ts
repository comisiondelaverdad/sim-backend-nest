import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { MetadataStandarization } from '../metadata-standarization/schema/metadata-standarization.schema';
import { EditMetadataStandarization } from './schema/edit-metadata-standarization.dto';
import { LogsService } from '../logs/logs.service';
import { CreateLogDto } from '../logs/dto/create-log.dto';

@Injectable()
export class MetadataStandarizationService {

    constructor(
      @InjectModel('MetadataStandarization') private metadataStandarizationModel: Model<MetadataStandarization>,
      private readonly logs: LogsService
      ) {}

    async find(query): Promise<MetadataStandarization[]> {
        let skip = parseInt(query["skip"])
        let limit = parseInt(query["limit"])
        delete query["skip"]
        delete query["limit"]
        return this.metadataStandarizationModel.find(query).skip(skip).limit(limit).exec();
      }

    async findByid(id): Promise<MetadataStandarization> {
      return this.metadataStandarizationModel.findOne({"_id":id}).exec();
    }

    async update(id, metadata: EditMetadataStandarization, req): Promise<MetadataStandarization> {
      return this.metadataStandarizationModel.findOneAndUpdate({"_id":id}, metadata).exec().then((mdta)=>{
        let date = new Date().toISOString().split('T').join(" ");
          let logInfo:CreateLogDto = {
            user: req.user._id,
            username: req.user.username,
            from: "metadata_standarization",
            action: "Edit term",
            message: "Se edita el término",
            metadata:{
              _id: id,
              destinationValueOld: mdta.destinationValue,
              destinationValueNew: metadata.destinationValue,
              sourceFieldOld: mdta.destinationField,
              sourceFieldNew: metadata.destinationField
            },
            createdAt: date,
            updatedAt: date
          }
          this.logs.create(logInfo);
          this.logs.logger("app", logInfo);
      });
    }

}
