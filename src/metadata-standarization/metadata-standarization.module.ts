import { Module, HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MetadataStandarizationController } from './metadata-standarization.controller';
import { MetadataStandarizationService } from './metadata-standarization.service';
import {  MetadataStandarizationSchema } from './schema/metadata-standarization.schema';
import { LogsService } from 'src/logs/logs.service';
import { LogsSchema } from 'src/logs/schema/logs.schema';
import { ConfigService } from 'src/config/config.service';

import { Menu, MenuSchema } from '../admin/menus/schema/menu.schema';
@Module({

      imports: [
            MongooseModule.forFeature([{ name: 'MetadataStandarization', schema: MetadataStandarizationSchema }], "database"),
            MongooseModule.forFeature([{ name: 'Logs', schema: LogsSchema }], "mongologs"),     
            HttpModule      
      ],
      controllers: [MetadataStandarizationController],
      providers: [MetadataStandarizationService, LogsService, ConfigService],
      exports: [MetadataStandarizationService, MongooseModule]    

})
export class MetadataStandarizationModule {}
