import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';


@Schema({collection:"metadata_standarization"})
export class MetadataStandarization extends Document {
  @Prop({type:"string"})
  sourceField: string;

  @Prop({type:"string"})
  destinationField: string;

  @Prop({type:"string"})
  sourceValue: string;

  @Prop({type:"string"})
  destinationValue: string;
}


export const MetadataStandarizationSchema = SchemaFactory.createForClass(MetadataStandarization);
