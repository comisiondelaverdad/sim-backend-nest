export class EditMetadataStandarization {
    _id?: String;
    readonly sourceField: String;   
    readonly destinationField: String; 
    readonly sourceValue:  String;
    readonly destinationValue:  String;

  }
