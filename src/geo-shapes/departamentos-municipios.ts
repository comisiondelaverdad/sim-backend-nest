export const countries = [
    {
        "fid": 38,
        "name": "Colombia",
        "value": "CO",
        "geoPoint": {
            "lat": 3.9221320600683556,
            "lon": -73.07548339866544
        }
    },
    {
        "fid": 192,
        "name": "Afghanistan",
        "value": "AF",
        "geoPoint": {
            "lat": 33.91670741074278,
            "lon": 66.06593956344418
        }
    },
    {
        "fid": 164,
        "name": "Albania",
        "value": "AL",
        "geoPoint": {
            "lat": 41.15475081247924,
            "lon": 20.067383464697905
        }
    },
    {
        "fid": 125,
        "name": "Algeria",
        "value": "DZ",
        "geoPoint": {
            "lat": 28.424546763145216,
            "lon": 2.6291479236152377
        }
    },
    {
        "fid": 1,
        "name": "American Samoa",
        "value": "AS",
        "geoPoint": {
            "lat": -14.304411216293671,
            "lon": -170.707839360432
        }
    },
    {
        "fid": 154,
        "name": "Andorra",
        "value": "AD",
        "geoPoint": {
            "lat": 42.54872587366529,
            "lon": 1.576778794649102
        }
    },
    {
        "fid": 96,
        "name": "Angola",
        "value": "AO",
        "geoPoint": {
            "lat": -12.353304143009144,
            "lon": 17.547472372609082
        }
    },
    {
        "fid": 28,
        "name": "Anguilla",
        "value": "AI",
        "geoPoint": {
            "lat": 18.222880131636288,
            "lon": -63.06007836382421
        }
    },
    {
        "fid": 25,
        "name": "Antarctica",
        "value": "AQ",
        "geoPoint": {
            "lat": -85.90952019044933,
            "lon": 9.197637381017737
        }
    },
    {
        "fid": 29,
        "name": "Antigua and Barbuda",
        "value": "AG",
        "geoPoint": {
            "lat": 17.280441052266532,
            "lon": -61.79125089274168
        }
    },
    {
        "fid": 15,
        "name": "Argentina",
        "value": "AR",
        "geoPoint": {
            "lat": -36.61005192514513,
            "lon": -65.40922674947238
        }
    },
    {
        "fid": 209,
        "name": "Armenia",
        "value": "AM",
        "geoPoint": {
            "lat": 40.29308994124879,
            "lon": 44.9432148327542
        }
    },
    {
        "fid": 30,
        "name": "Aruba",
        "value": "AW",
        "geoPoint": {
            "lat": 12.515642005048498,
            "lon": -69.9756473809219
        }
    },
    {
        "fid": 218,
        "name": "Australia",
        "value": "AU",
        "geoPoint": {
            "lat": -26.235261249543505,
            "lon": 134.6031934314354
        }
    },
    {
        "fid": 165,
        "name": "Austria",
        "value": "AT",
        "geoPoint": {
            "lat": 47.60314631323405,
            "lon": 14.148395500022284
        }
    },
    {
        "fid": 210,
        "name": "Azerbaijan",
        "value": "AZ",
        "geoPoint": {
            "lat": 40.317467124253525,
            "lon": 48.81870273742477
        }
    },
    {
        "fid": 31,
        "name": "Bahamas",
        "value": "BS",
        "geoPoint": {
            "lat": 24.23319227946004,
            "lon": -76.56622014950828
        }
    },
    {
        "fid": 193,
        "name": "Bahrain",
        "value": "BH",
        "geoPoint": {
            "lat": 26.022688380499975,
            "lon": 50.55959907103938
        }
    },
    {
        "fid": 233,
        "name": "Bangladesh",
        "value": "BD",
        "geoPoint": {
            "lat": 23.860007332011225,
            "lon": 90.2640035438027
        }
    },
    {
        "fid": 32,
        "name": "Barbados",
        "value": "BB",
        "geoPoint": {
            "lat": 13.178743324299544,
            "lon": -59.56196294327834
        }
    },
    {
        "fid": 182,
        "name": "Belarus",
        "value": "BY",
        "geoPoint": {
            "lat": 53.58691217957501,
            "lon": 28.055915596759174
        }
    },
    {
        "fid": 160,
        "name": "Belgium",
        "value": "BE",
        "geoPoint": {
            "lat": 50.64893860885923,
            "lon": 4.66039266787827
        }
    },
    {
        "fid": 33,
        "name": "Belize",
        "value": "BZ",
        "geoPoint": {
            "lat": 17.220983655788118,
            "lon": -88.6842928839457
        }
    },
    {
        "fid": 126,
        "name": "Benin",
        "value": "BJ",
        "geoPoint": {
            "lat": 9.65755387065223,
            "lon": 2.3437477018399457
        }
    },
    {
        "fid": 34,
        "name": "Bermuda",
        "value": "BM",
        "geoPoint": {
            "lat": 32.31610424104198,
            "lon": -64.7395959725039
        }
    },
    {
        "fid": 234,
        "name": "Bhutan",
        "value": "BT",
        "geoPoint": {
            "lat": 27.417380789329535,
            "lon": 90.42926580975684
        }
    },
    {
        "fid": 20,
        "name": "Bolivia",
        "value": "BO",
        "geoPoint": {
            "lat": -16.78265494592524,
            "lon": -64.66489329400098
        }
    },
    {
        "fid": 35,
        "name": "Bonaire",
        "value": "BQ",
        "geoPoint": {
            "lat": 12.187647648947733,
            "lon": -68.28431970437649
        }
    },
    {
        "fid": 166,
        "name": "Bosnia and Herzegovina",
        "value": "BA",
        "geoPoint": {
            "lat": 44.17761398190617,
            "lon": 17.78340505817501
        }
    },
    {
        "fid": 97,
        "name": "Botswana",
        "value": "BW",
        "geoPoint": {
            "lat": -22.229898292126983,
            "lon": 23.80968857091428
        }
    },
    {
        "fid": 94,
        "name": "Bouvet Island",
        "value": "BV",
        "geoPoint": {
            "lat": -54.42191926037115,
            "lon": 3.4125170151468893
        }
    },
    {
        "fid": 21,
        "name": "Brazil",
        "value": "BR",
        "geoPoint": {
            "lat": -11.171945708443886,
            "lon": -53.02741953420185
        }
    },
    {
        "fid": 116,
        "name": "British Indian Ocean Territory",
        "value": "IO",
        "geoPoint": {
            "lat": -7.334275066773771,
            "lon": 72.43403034273506
        }
    },
    {
        "fid": 36,
        "name": "British Virgin Islands",
        "value": "VG",
        "geoPoint": {
            "lat": 18.444598194212123,
            "lon": -64.53040541982455
        }
    },
    {
        "fid": 236,
        "name": "Brunei Darussalam",
        "value": "BN",
        "geoPoint": {
            "lat": 4.5215497130305815,
            "lon": 114.76112778431839
        }
    },
    {
        "fid": 183,
        "name": "Bulgaria",
        "value": "BG",
        "geoPoint": {
            "lat": 42.77345388837953,
            "lon": 25.23487908773101
        }
    },
    {
        "fid": 70,
        "name": "Burkina Faso",
        "value": "BF",
        "geoPoint": {
            "lat": 12.286003383714554,
            "lon": -1.736867873503585
        }
    },
    {
        "fid": 98,
        "name": "Burundi",
        "value": "BI",
        "geoPoint": {
            "lat": -3.3565729169460417,
            "lon": 29.887097818073265
        }
    },
    {
        "fid": 71,
        "name": "Cabo Verde",
        "value": "CV",
        "geoPoint": {
            "lat": 15.984378144584346,
            "lon": -23.969434588874154
        }
    },
    {
        "fid": 226,
        "name": "Cambodia",
        "value": "KH",
        "geoPoint": {
            "lat": 12.72220855396698,
            "lon": 104.9248203905358
        }
    },
    {
        "fid": 127,
        "name": "Cameroon",
        "value": "CM",
        "geoPoint": {
            "lat": 5.703826570874035,
            "lon": 12.746536529668953
        }
    },
    {
        "fid": 14,
        "name": "Canada",
        "value": "CA",
        "geoPoint": {
            "lat": 67.72444770105555,
            "lon": -97.22292996655962
        }
    },
    {
        "fid": 37,
        "name": "Cayman Islands",
        "value": "KY",
        "geoPoint": {
            "lat": 19.308667494883867,
            "lon": -81.23844347876208
        }
    },
    {
        "fid": 128,
        "name": "Central African Republic",
        "value": "CF",
        "geoPoint": {
            "lat": 6.579402537872304,
            "lon": 20.486242304653906
        }
    },
    {
        "fid": 129,
        "name": "Chad",
        "value": "TD",
        "geoPoint": {
            "lat": 15.468998102936146,
            "lon": 18.669210688879318
        }
    },
    {
        "fid": 17,
        "name": "Chile",
        "value": "CL",
        "geoPoint": {
            "lat": -40.45733821888505,
            "lon": -71.51485711581637
        }
    },
    {
        "fid": 235,
        "name": "China",
        "value": "CN",
        "geoPoint": {
            "lat": 37.55444957642278,
            "lon": 104.21084682935856
        }
    },
    {
        "fid": 214,
        "name": "Christmas Island",
        "value": "CX",
        "geoPoint": {
            "lat": -10.444121100643994,
            "lon": 105.70370008550069
        }
    },
    {
        "fid": 215,
        "name": "Cocos Islands",
        "value": "CC",
        "geoPoint": {
            "lat": -12.171251156323923,
            "lon": 96.83688786371111
        }
    },
    {
        "fid": 99,
        "name": "Comoros",
        "value": "KM",
        "geoPoint": {
            "lat": -11.893295763564895,
            "lon": 43.6763541641596
        }
    },
    {
        "fid": 100,
        "name": "Congo",
        "value": "CG",
        "geoPoint": {
            "lat": -0.8421829799314551,
            "lon": 15.223576952171758
        }
    },
    {
        "fid": 101,
        "name": "Congo DRC",
        "value": "CD",
        "geoPoint": {
            "lat": -2.9035365331925607,
            "lon": 23.658720138376147
        }
    },
    {
        "fid": 3,
        "name": "Cook Islands",
        "value": "CK",
        "geoPoint": {
            "lat": -20.945313563865156,
            "lon": -158.90663319441109
        }
    },
    {
        "fid": 39,
        "name": "Costa Rica",
        "value": "CR",
        "geoPoint": {
            "lat": 9.972132787099639,
            "lon": -84.18904311497889
        }
    },
    {
        "fid": 167,
        "name": "Croatia",
        "value": "HR",
        "geoPoint": {
            "lat": 45.06950180616979,
            "lon": 16.413023051075
        }
    },
    {
        "fid": 40,
        "name": "Cuba",
        "value": "CU",
        "geoPoint": {
            "lat": 21.6302149680696,
            "lon": -79.05074259741345
        }
    },
    {
        "fid": 41,
        "name": "Curacao",
        "value": "CW",
        "geoPoint": {
            "lat": 12.187971495683783,
            "lon": -68.96759404361846
        }
    },
    {
        "fid": 138,
        "name": "Cyprus",
        "value": "CY",
        "geoPoint": {
            "lat": 35.04670096623312,
            "lon": 33.22253120728586
        }
    },
    {
        "fid": 168,
        "name": "Czech Republic",
        "value": "CZ",
        "geoPoint": {
            "lat": 49.75178289079473,
            "lon": 15.334781574428375
        }
    },
    {
        "fid": 72,
        "name": "Côte d'Ivoire",
        "value": "CI",
        "geoPoint": {
            "lat": 7.639262250929064,
            "lon": -5.555701600440413
        }
    },
    {
        "fid": 169,
        "name": "Denmark",
        "value": "DK",
        "geoPoint": {
            "lat": 55.983349302928566,
            "lon": 10.03882521180958
        }
    },
    {
        "fid": 139,
        "name": "Djibouti",
        "value": "DJ",
        "geoPoint": {
            "lat": 11.75074074810045,
            "lon": 42.57805813238993
        }
    },
    {
        "fid": 42,
        "name": "Dominica",
        "value": "DM",
        "geoPoint": {
            "lat": 15.435683902346566,
            "lon": -61.35577401982092
        }
    },
    {
        "fid": 43,
        "name": "Dominican Republic",
        "value": "DO",
        "geoPoint": {
            "lat": 18.896471310987025,
            "lon": -70.4848990018896
        }
    },
    {
        "fid": 18,
        "name": "Ecuador",
        "value": "EC",
        "geoPoint": {
            "lat": -1.4288570718561953,
            "lon": -78.76718106728137
        }
    },
    {
        "fid": 140,
        "name": "Egypt",
        "value": "EG",
        "geoPoint": {
            "lat": 26.59281233697075,
            "lon": 29.86385041912667
        }
    },
    {
        "fid": 11,
        "name": "El Salvador",
        "value": "SV",
        "geoPoint": {
            "lat": 13.737395765628557,
            "lon": -88.86685685515167
        }
    },
    {
        "fid": 130,
        "name": "Equatorial Guinea",
        "value": "GQ",
        "geoPoint": {
            "lat": 1.7126982717432735,
            "lon": 10.341810223675399
        }
    },
    {
        "fid": 141,
        "name": "Eritrea",
        "value": "ER",
        "geoPoint": {
            "lat": 15.368256505776753,
            "lon": 38.845975605305625
        }
    },
    {
        "fid": 184,
        "name": "Estonia",
        "value": "EE",
        "geoPoint": {
            "lat": 58.684397744312996,
            "lon": 25.52675756154407
        }
    },
    {
        "fid": 112,
        "name": "Eswatini",
        "value": "SZ",
        "geoPoint": {
            "lat": -26.564614691743902,
            "lon": 31.497562498805994
        }
    },
    {
        "fid": 142,
        "name": "Ethiopia",
        "value": "ET",
        "geoPoint": {
            "lat": 8.656341085313462,
            "lon": 39.6113002718505
        }
    },
    {
        "fid": 16,
        "name": "Falkland Islands",
        "value": "FK",
        "geoPoint": {
            "lat": -51.739037378185685,
            "lon": -59.36464410233975
        }
    },
    {
        "fid": 92,
        "name": "Faroe Islands",
        "value": "FO",
        "geoPoint": {
            "lat": 62.0341116780132,
            "lon": -6.884467418781245
        }
    },
    {
        "fid": 26,
        "name": "Fiji",
        "value": "FJ",
        "geoPoint": {
            "lat": -17.457148441611483,
            "lon": 172.00470124006267
        }
    },
    {
        "fid": 185,
        "name": "Finland",
        "value": "FI",
        "geoPoint": {
            "lat": 64.9136311923823,
            "lon": 26.3022594554444
        }
    },
    {
        "fid": 156,
        "name": "France",
        "value": "FR",
        "geoPoint": {
            "lat": 46.687963780005475,
            "lon": 2.5405884666608736
        }
    },
    {
        "fid": 44,
        "name": "French Guiana",
        "value": "GF",
        "geoPoint": {
            "lat": 3.9261205427513626,
            "lon": -53.24123757279214
        }
    },
    {
        "fid": 4,
        "name": "French Polynesia",
        "value": "PF",
        "geoPoint": {
            "lat": -14.94879037670534,
            "lon": -146.4936845619513
        }
    },
    {
        "fid": 117,
        "name": "French Southern Territories",
        "value": "TF",
        "geoPoint": {
            "lat": -49.20140305134491,
            "lon": 68.89504752412677
        }
    },
    {
        "fid": 102,
        "name": "Gabon",
        "value": "GA",
        "geoPoint": {
            "lat": -0.5914365126465797,
            "lon": 11.797180326156933
        }
    },
    {
        "fid": 73,
        "name": "Gambia",
        "value": "GM",
        "geoPoint": {
            "lat": 13.45279006869968,
            "lon": -15.386486727025382
        }
    },
    {
        "fid": 186,
        "name": "Georgia",
        "value": "GE",
        "geoPoint": {
            "lat": 42.18438912550555,
            "lon": 43.508969442158296
        }
    },
    {
        "fid": 161,
        "name": "Germany",
        "value": "DE",
        "geoPoint": {
            "lat": 51.21894798094201,
            "lon": 10.40419331438696
        }
    },
    {
        "fid": 74,
        "name": "Ghana",
        "value": "GH",
        "geoPoint": {
            "lat": 7.970260294153038,
            "lon": -1.2072187603945126
        }
    },
    {
        "fid": 75,
        "name": "Gibraltar",
        "value": "GI",
        "geoPoint": {
            "lat": 36.1382169855094,
            "lon": -5.344893084307959
        }
    },
    {
        "fid": 122,
        "name": "Glorioso Islands",
        "value": "TF",
        "geoPoint": {
            "lat": -11.56622506868562,
            "lon": 47.29094808172891
        }
    },
    {
        "fid": 143,
        "name": "Greece",
        "value": "GR",
        "geoPoint": {
            "lat": 39.125111942520455,
            "lon": 22.95633740639946
        }
    },
    {
        "fid": 85,
        "name": "Greenland",
        "value": "GL",
        "geoPoint": {
            "lat": 77.30824860372704,
            "lon": -41.16373312717544
        }
    },
    {
        "fid": 45,
        "name": "Grenada",
        "value": "GD",
        "geoPoint": {
            "lat": 12.112947374290775,
            "lon": -61.67937563729767
        }
    },
    {
        "fid": 46,
        "name": "Guadeloupe",
        "value": "GP",
        "geoPoint": {
            "lat": 16.203625609274184,
            "lon": -61.536746988910906
        }
    },
    {
        "fid": 241,
        "name": "Guam",
        "value": "GU",
        "geoPoint": {
            "lat": 13.443638199062367,
            "lon": 144.77562865355696
        }
    },
    {
        "fid": 12,
        "name": "Guatemala",
        "value": "GT",
        "geoPoint": {
            "lat": 15.7105100140453,
            "lon": -90.3549968197566
        }
    },
    {
        "fid": 86,
        "name": "Guernsey",
        "value": "GG",
        "geoPoint": {
            "lat": 49.458722290621054,
            "lon": -2.5763865959435184
        }
    },
    {
        "fid": 76,
        "name": "Guinea",
        "value": "GN",
        "geoPoint": {
            "lat": 10.444756065498652,
            "lon": -10.944881813164248
        }
    },
    {
        "fid": 77,
        "name": "Guinea-Bissau",
        "value": "GW",
        "geoPoint": {
            "lat": 12.0311663291951,
            "lon": -14.965079212964012
        }
    },
    {
        "fid": 47,
        "name": "Guyana",
        "value": "GY",
        "geoPoint": {
            "lat": 4.799294567003349,
            "lon": -58.975826519860625
        }
    },
    {
        "fid": 48,
        "name": "Haiti",
        "value": "HT",
        "geoPoint": {
            "lat": 18.944262983445395,
            "lon": -72.67875434183907
        }
    },
    {
        "fid": 118,
        "name": "Heard Island and McDonald Islands",
        "value": "HM",
        "geoPoint": {
            "lat": -53.091428255143235,
            "lon": 73.49843824873369
        }
    },
    {
        "fid": 49,
        "name": "Honduras",
        "value": "HN",
        "geoPoint": {
            "lat": 14.822027758757965,
            "lon": -86.61797467739535
        }
    },
    {
        "fid": 170,
        "name": "Hungary",
        "value": "HU",
        "geoPoint": {
            "lat": 47.17835359955521,
            "lon": 19.42180204328459
        }
    },
    {
        "fid": 91,
        "name": "Iceland",
        "value": "IS",
        "geoPoint": {
            "lat": 65.02580988580715,
            "lon": -18.60336714127181
        }
    },
    {
        "fid": 194,
        "name": "India",
        "value": "IN",
        "geoPoint": {
            "lat": 23.213118265433682,
            "lon": 79.63203855943833
        }
    },
    {
        "fid": 216,
        "name": "Indonesia",
        "value": "ID",
        "geoPoint": {
            "lat": -2.2428199526122605,
            "lon": 117.30909650195498
        }
    },
    {
        "fid": 195,
        "name": "Iran",
        "value": "IR",
        "geoPoint": {
            "lat": 32.94288984559664,
            "lon": 54.11209492611236
        }
    },
    {
        "fid": 144,
        "name": "Iraq",
        "value": "IQ",
        "geoPoint": {
            "lat": 33.118103334274785,
            "lon": 43.75752524452421
        }
    },
    {
        "fid": 87,
        "name": "Ireland",
        "value": "IE",
        "geoPoint": {
            "lat": 53.20293186123096,
            "lon": -8.146935983428943
        }
    },
    {
        "fid": 88,
        "name": "Isle of Man",
        "value": "IM",
        "geoPoint": {
            "lat": 54.22935370933306,
            "lon": -4.525691370122558
        }
    },
    {
        "fid": 145,
        "name": "Israel",
        "value": "IL",
        "geoPoint": {
            "lat": 31.372036800882743,
            "lon": 34.96721528705876
        }
    },
    {
        "fid": 171,
        "name": "Italy",
        "value": "IT",
        "geoPoint": {
            "lat": 42.964046463188716,
            "lon": 12.007474846720408
        }
    },
    {
        "fid": 50,
        "name": "Jamaica",
        "value": "JM",
        "geoPoint": {
            "lat": 18.151684482855167,
            "lon": -77.31923129135433
        }
    },
    {
        "fid": 242,
        "name": "Japan",
        "value": "JP",
        "geoPoint": {
            "lat": 37.874094390734385,
            "lon": 138.18111306438965
        }
    },
    {
        "fid": 89,
        "name": "Jersey",
        "value": "JE",
        "geoPoint": {
            "lat": 49.21912959206853,
            "lon": -2.1286603074366535
        }
    },
    {
        "fid": 146,
        "name": "Jordan",
        "value": "JO",
        "geoPoint": {
            "lat": 31.27018108819578,
            "lon": 36.793594329443366
        }
    },
    {
        "fid": 106,
        "name": "Juan De Nova Island",
        "value": "TF",
        "geoPoint": {
            "lat": -17.06449222836885,
            "lon": 42.7437476957623
        }
    },
    {
        "fid": 211,
        "name": "Kazakhstan",
        "value": "KZ",
        "geoPoint": {
            "lat": 48.30229736041279,
            "lon": 66.73021694168612
        }
    },
    {
        "fid": 103,
        "name": "Kenya",
        "value": "KE",
        "geoPoint": {
            "lat": 0.5308029546874132,
            "lon": 37.857871647927084
        }
    },
    {
        "fid": 131,
        "name": "Kiribati",
        "value": "KI",
        "geoPoint": {
            "lat": 1.8345737225746628,
            "lon": -154.46042675723888
        }
    },
    {
        "fid": 196,
        "name": "Kuwait",
        "value": "KW",
        "geoPoint": {
            "lat": 29.34326809164785,
            "lon": 47.59050170756493
        }
    },
    {
        "fid": 212,
        "name": "Kyrgyzstan",
        "value": "KG",
        "geoPoint": {
            "lat": 41.48607103599583,
            "lon": 74.57164582578508
        }
    },
    {
        "fid": 227,
        "name": "Laos",
        "value": "LA",
        "geoPoint": {
            "lat": 18.539945832766875,
            "lon": 103.74479308531473
        }
    },
    {
        "fid": 187,
        "name": "Latvia",
        "value": "LV",
        "geoPoint": {
            "lat": 56.867378740812875,
            "lon": 24.925676530936485
        }
    },
    {
        "fid": 147,
        "name": "Lebanon",
        "value": "LB",
        "geoPoint": {
            "lat": 33.92318313586106,
            "lon": 35.88925123273719
        }
    },
    {
        "fid": 104,
        "name": "Lesotho",
        "value": "LS",
        "geoPoint": {
            "lat": -29.584144345234954,
            "lon": 28.24214257170606
        }
    },
    {
        "fid": 78,
        "name": "Liberia",
        "value": "LR",
        "geoPoint": {
            "lat": 6.45075757389595,
            "lon": -9.30903791414207
        }
    },
    {
        "fid": 132,
        "name": "Libya",
        "value": "LY",
        "geoPoint": {
            "lat": 27.168128517754255,
            "lon": 17.995771733499907
        }
    },
    {
        "fid": 157,
        "name": "Liechtenstein",
        "value": "LI",
        "geoPoint": {
            "lat": 47.151926728871075,
            "lon": 9.554262245110003
        }
    },
    {
        "fid": 188,
        "name": "Lithuania",
        "value": "LT",
        "geoPoint": {
            "lat": 55.349801121036556,
            "lon": 23.893138785905933
        }
    },
    {
        "fid": 162,
        "name": "Luxembourg",
        "value": "LU",
        "geoPoint": {
            "lat": 49.771557253999134,
            "lon": 6.087639867454824
        }
    },
    {
        "fid": 119,
        "name": "Madagascar",
        "value": "MG",
        "geoPoint": {
            "lat": -19.468696716341427,
            "lon": 46.686991631767256
        }
    },
    {
        "fid": 105,
        "name": "Malawi",
        "value": "MW",
        "geoPoint": {
            "lat": -13.237873899372918,
            "lon": 34.310557575292194
        }
    },
    {
        "fid": 228,
        "name": "Malaysia",
        "value": "MY",
        "geoPoint": {
            "lat": 3.796150345594541,
            "lon": 109.70883638596985
        }
    },
    {
        "fid": 197,
        "name": "Maldives",
        "value": "MV",
        "geoPoint": {
            "lat": 3.2258922102838796,
            "lon": 73.25215481541558
        }
    },
    {
        "fid": 79,
        "name": "Mali",
        "value": "ML",
        "geoPoint": {
            "lat": 17.449419810253623,
            "lon": -3.501298673775577
        }
    },
    {
        "fid": 133,
        "name": "Malta",
        "value": "MT",
        "geoPoint": {
            "lat": 35.89056203610552,
            "lon": 14.441903362403938
        }
    },
    {
        "fid": 243,
        "name": "Marshall Islands",
        "value": "MH",
        "geoPoint": {
            "lat": 7.66258317893684,
            "lon": 168.62659263642186
        }
    },
    {
        "fid": 51,
        "name": "Martinique",
        "value": "MQ",
        "geoPoint": {
            "lat": 14.652638978926351,
            "lon": -61.02132162821282
        }
    },
    {
        "fid": 80,
        "name": "Mauritania",
        "value": "MR",
        "geoPoint": {
            "lat": 20.34755733075372,
            "lon": -10.320472356345984
        }
    },
    {
        "fid": 120,
        "name": "Mauritius",
        "value": "MU",
        "geoPoint": {
            "lat": -20.25218137292123,
            "lon": 57.86970892377777
        }
    },
    {
        "fid": 121,
        "name": "Mayotte",
        "value": "YT",
        "geoPoint": {
            "lat": -12.818757629183688,
            "lon": 45.13870083971693
        }
    },
    {
        "fid": 13,
        "name": "Mexico",
        "value": "MX",
        "geoPoint": {
            "lat": 24.192884470004902,
            "lon": -102.71201375971238
        }
    },
    {
        "fid": 244,
        "name": "Micronesia",
        "value": "FM",
        "geoPoint": {
            "lat": 6.493663843048761,
            "lon": 159.40165042363577
        }
    },
    {
        "fid": 189,
        "name": "Moldova",
        "value": "MD",
        "geoPoint": {
            "lat": 47.2082036393656,
            "lon": 28.469588592066145
        }
    },
    {
        "fid": 158,
        "name": "Monaco",
        "value": "MC",
        "geoPoint": {
            "lat": 43.74798453685627,
            "lon": 7.412821864827808
        }
    },
    {
        "fid": 239,
        "name": "Mongolia",
        "value": "MN",
        "geoPoint": {
            "lat": 46.97174592356641,
            "lon": 103.04507114854118
        }
    },
    {
        "fid": 172,
        "name": "Montenegro",
        "value": "ME",
        "geoPoint": {
            "lat": 42.794020016536514,
            "lon": 19.253394765001524
        }
    },
    {
        "fid": 52,
        "name": "Montserrat",
        "value": "MS",
        "geoPoint": {
            "lat": 16.735373587625606,
            "lon": -62.186933576661616
        }
    },
    {
        "fid": 81,
        "name": "Morocco",
        "value": "MA",
        "geoPoint": {
            "lat": 29.33810478073039,
            "lon": -8.796601106681095
        }
    },
    {
        "fid": 107,
        "name": "Mozambique",
        "value": "MZ",
        "geoPoint": {
            "lat": -17.40000366191255,
            "lon": 35.517001117473754
        }
    },
    {
        "fid": 229,
        "name": "Myanmar",
        "value": "MM",
        "geoPoint": {
            "lat": 21.27991505547594,
            "lon": 96.5043126940638
        }
    },
    {
        "fid": 108,
        "name": "Namibia",
        "value": "NA",
        "geoPoint": {
            "lat": -22.24830157506925,
            "lon": 17.22563571665479
        }
    },
    {
        "fid": 219,
        "name": "Nauru",
        "value": "NR",
        "geoPoint": {
            "lat": -0.5221021980043343,
            "lon": 166.92937632678647
        }
    },
    {
        "fid": 198,
        "name": "Nepal",
        "value": "NP",
        "geoPoint": {
            "lat": 28.26547374338416,
            "lon": 83.92298727380529
        }
    },
    {
        "fid": 163,
        "name": "Netherlands",
        "value": "NL",
        "geoPoint": {
            "lat": 52.2573483702928,
            "lon": 5.615311648847263
        }
    },
    {
        "fid": 220,
        "name": "New Caledonia",
        "value": "NC",
        "geoPoint": {
            "lat": -21.319461161926938,
            "lon": 165.71788347626762
        }
    },
    {
        "fid": 95,
        "name": "New Zealand",
        "value": "NZ",
        "geoPoint": {
            "lat": -42.06706687537145,
            "lon": 171.624256888544
        }
    },
    {
        "fid": 53,
        "name": "Nicaragua",
        "value": "NI",
        "geoPoint": {
            "lat": 12.845734792429898,
            "lon": -85.03407381804207
        }
    },
    {
        "fid": 134,
        "name": "Niger",
        "value": "NE",
        "geoPoint": {
            "lat": 17.488505810003627,
            "lon": 9.425226816176425
        }
    },
    {
        "fid": 135,
        "name": "Nigeria",
        "value": "NG",
        "geoPoint": {
            "lat": 9.616919945589217,
            "lon": 8.110131558417466
        }
    },
    {
        "fid": 5,
        "name": "Niue",
        "value": "NU",
        "geoPoint": {
            "lat": -19.052327320419014,
            "lon": -169.86878342411833
        }
    },
    {
        "fid": 221,
        "name": "Norfolk Island",
        "value": "NF",
        "geoPoint": {
            "lat": -29.037659758847937,
            "lon": 167.95259704744518
        }
    },
    {
        "fid": 240,
        "name": "North Korea",
        "value": "KP",
        "geoPoint": {
            "lat": 40.17588620735749,
            "lon": 127.19884027604152
        }
    },
    {
        "fid": 178,
        "name": "North Macedonia",
        "value": "MK",
        "geoPoint": {
            "lat": 41.60261564682164,
            "lon": 21.69818860255877
        }
    },
    {
        "fid": 245,
        "name": "Northern Mariana Islands",
        "value": "MP",
        "geoPoint": {
            "lat": 15.08859186166528,
            "lon": 145.67924418357117
        }
    },
    {
        "fid": 180,
        "name": "Norway",
        "value": "NO",
        "geoPoint": {
            "lat": 65.39638313374897,
            "lon": 15.087461341032066
        }
    },
    {
        "fid": 199,
        "name": "Oman",
        "value": "OM",
        "geoPoint": {
            "lat": 20.644036871479,
            "lon": 56.124392643634806
        }
    },
    {
        "fid": 200,
        "name": "Pakistan",
        "value": "PK",
        "geoPoint": {
            "lat": 30.139823275356644,
            "lon": 69.47579577397211
        }
    },
    {
        "fid": 246,
        "name": "Palau",
        "value": "PW",
        "geoPoint": {
            "lat": 7.501909808642664,
            "lon": 134.568692810417
        }
    },
    {
        "fid": 148,
        "name": "Palestinian Territory",
        "value": "PS",
        "geoPoint": {
            "lat": 31.916233541800036,
            "lon": 35.20450762593664
        }
    },
    {
        "fid": 54,
        "name": "Panama",
        "value": "PA",
        "geoPoint": {
            "lat": 8.50836990573169,
            "lon": -80.10286461270395
        }
    },
    {
        "fid": 222,
        "name": "Papua New Guinea",
        "value": "PG",
        "geoPoint": {
            "lat": -6.488882648951087,
            "lon": 145.24386630945295
        }
    },
    {
        "fid": 22,
        "name": "Paraguay",
        "value": "PY",
        "geoPoint": {
            "lat": -23.285071528815347,
            "lon": -58.36935360687218
        }
    },
    {
        "fid": 19,
        "name": "Peru",
        "value": "PE",
        "geoPoint": {
            "lat": -9.248039207588535,
            "lon": -74.35603154062511
        }
    },
    {
        "fid": 237,
        "name": "Philippines",
        "value": "PH",
        "geoPoint": {
            "lat": 11.82039268999592,
            "lon": 122.86133913772994
        }
    },
    {
        "fid": 6,
        "name": "Pitcairn",
        "value": "PN",
        "geoPoint": {
            "lat": -24.47729151461201,
            "lon": -128.59447292201386
        }
    },
    {
        "fid": 173,
        "name": "Poland",
        "value": "PL",
        "geoPoint": {
            "lat": 52.18732637675109,
            "lon": 19.38647069623943
        }
    },
    {
        "fid": 82,
        "name": "Portugal",
        "value": "PT",
        "geoPoint": {
            "lat": 39.64870801989476,
            "lon": -8.54386873574168
        }
    },
    {
        "fid": 55,
        "name": "Puerto Rico",
        "value": "PR",
        "geoPoint": {
            "lat": 18.22152062692556,
            "lon": -66.46235667650494
        }
    },
    {
        "fid": 201,
        "name": "Qatar",
        "value": "QA",
        "geoPoint": {
            "lat": 25.317044952973536,
            "lon": 51.191270600944094
        }
    },
    {
        "fid": 190,
        "name": "Romania",
        "value": "RO",
        "geoPoint": {
            "lat": 45.87787250252665,
            "lon": 24.964479722856495
        }
    },
    {
        "fid": 247,
        "name": "Russian Federation",
        "value": "RU",
        "geoPoint": {
            "lat": 64.83505658504329,
            "lon": 97.7590042893037
        }
    },
    {
        "fid": 109,
        "name": "Rwanda",
        "value": "RW",
        "geoPoint": {
            "lat": -1.9980272650994875,
            "lon": 29.917604193675242
        }
    },
    {
        "fid": 123,
        "name": "Réunion",
        "value": "RE",
        "geoPoint": {
            "lat": -21.121830543473788,
            "lon": 55.53822477129875
        }
    },
    {
        "fid": 56,
        "name": "Saba",
        "value": "BQ",
        "geoPoint": {
            "lat": 17.632231158622446,
            "lon": -63.237536069802374
        }
    },
    {
        "fid": 57,
        "name": "Saint Barthelemy",
        "value": "BL",
        "geoPoint": {
            "lat": 17.902668124891,
            "lon": -62.82920135656884
        }
    },
    {
        "fid": 58,
        "name": "Saint Eustatius",
        "value": "BQ",
        "geoPoint": {
            "lat": 17.490906559968025,
            "lon": -62.97813852093549
        }
    },
    {
        "fid": 27,
        "name": "Saint Helena",
        "value": "SH",
        "geoPoint": {
            "lat": -15.961551964865293,
            "lon": -5.717153724097971
        }
    },
    {
        "fid": 59,
        "name": "Saint Kitts and Nevis",
        "value": "KN",
        "geoPoint": {
            "lat": 17.326203550573688,
            "lon": -62.75352732249114
        }
    },
    {
        "fid": 60,
        "name": "Saint Lucia",
        "value": "LC",
        "geoPoint": {
            "lat": 13.897921693653682,
            "lon": -60.96870850583919
        }
    },
    {
        "fid": 61,
        "name": "Saint Martin",
        "value": "MF",
        "geoPoint": {
            "lat": 18.079464022799577,
            "lon": -63.05878119368822
        }
    },
    {
        "fid": 62,
        "name": "Saint Pierre and Miquelon",
        "value": "PM",
        "geoPoint": {
            "lat": 46.93129698467056,
            "lon": -56.306167298090415
        }
    },
    {
        "fid": 63,
        "name": "Saint Vincent and the Grenadines",
        "value": "VC",
        "geoPoint": {
            "lat": 13.25483219336438,
            "lon": -61.193764230397505
        }
    },
    {
        "fid": 7,
        "name": "Samoa",
        "value": "WS",
        "geoPoint": {
            "lat": -13.758547436079123,
            "lon": -172.15923394043267
        }
    },
    {
        "fid": 174,
        "name": "San Marino",
        "value": "SM",
        "geoPoint": {
            "lat": 43.942957045617455,
            "lon": 12.46098042885708
        }
    },
    {
        "fid": 110,
        "name": "Sao Tome and Principe",
        "value": "ST",
        "geoPoint": {
            "lat": 0.45709343920267215,
            "lon": 6.7366312100019705
        }
    },
    {
        "fid": 202,
        "name": "Saudi Arabia",
        "value": "SA",
        "geoPoint": {
            "lat": 24.279704153914423,
            "lon": 44.4808236838216
        }
    },
    {
        "fid": 83,
        "name": "Senegal",
        "value": "SN",
        "geoPoint": {
            "lat": 14.375350900167819,
            "lon": -14.469721811916571
        }
    },
    {
        "fid": 175,
        "name": "Serbia",
        "value": "RS",
        "geoPoint": {
            "lat": 44.05881966302607,
            "lon": 20.79743674136548
        }
    },
    {
        "fid": 124,
        "name": "Seychelles",
        "value": "SC",
        "geoPoint": {
            "lat": -6.370941128401363,
            "lon": 52.20856457688323
        }
    },
    {
        "fid": 84,
        "name": "Sierra Leone",
        "value": "SL",
        "geoPoint": {
            "lat": 8.562356012511747,
            "lon": -11.791960520446302
        }
    },
    {
        "fid": 230,
        "name": "Singapore",
        "value": "SG",
        "geoPoint": {
            "lat": 1.3516176391421442,
            "lon": 103.80805120560008
        }
    },
    {
        "fid": 64,
        "name": "Sint Maarten",
        "value": "SX",
        "geoPoint": {
            "lat": 18.037945965458405,
            "lon": -63.06328482005177
        }
    },
    {
        "fid": 176,
        "name": "Slovakia",
        "value": "SK",
        "geoPoint": {
            "lat": 48.712467143294866,
            "lon": 19.496838041578517
        }
    },
    {
        "fid": 177,
        "name": "Slovenia",
        "value": "SI",
        "geoPoint": {
            "lat": 46.126671617070095,
            "lon": 14.82860455092845
        }
    },
    {
        "fid": 223,
        "name": "Solomon Islands",
        "value": "SB",
        "geoPoint": {
            "lat": -8.924045939817084,
            "lon": 159.63968810932727
        }
    },
    {
        "fid": 203,
        "name": "Somalia",
        "value": "SO",
        "geoPoint": {
            "lat": 6.097077519759182,
            "lon": 45.87373947770247
        }
    },
    {
        "fid": 111,
        "name": "South Africa",
        "value": "ZA",
        "geoPoint": {
            "lat": -29.125147965191623,
            "lon": 25.038803584123364
        }
    },
    {
        "fid": 24,
        "name": "South Georgia and South Sandwich Islands",
        "value": "GS",
        "geoPoint": {
            "lat": -54.51187922885602,
            "lon": -36.34356077535353
        }
    },
    {
        "fid": 238,
        "name": "South Korea",
        "value": "KR",
        "geoPoint": {
            "lat": 36.397444765527766,
            "lon": 127.83679274731364
        }
    },
    {
        "fid": 149,
        "name": "South Sudan",
        "value": "SS",
        "geoPoint": {
            "lat": 7.2900453816888175,
            "lon": 30.318168676531357
        }
    },
    {
        "fid": 248,
        "name": "Spain",
        "value": "ES",
        "geoPoint": {
            "lat": 40.46512238312025,
            "lon": -3.4884242112935406
        }
    },
    {
        "fid": 204,
        "name": "Sri Lanka",
        "value": "LK",
        "geoPoint": {
            "lat": 7.611028692786134,
            "lon": 80.70445352636062
        }
    },
    {
        "fid": 150,
        "name": "Sudan",
        "value": "SD",
        "geoPoint": {
            "lat": 16.08673312121002,
            "lon": 29.976678380510346
        }
    },
    {
        "fid": 65,
        "name": "Suriname",
        "value": "SR",
        "geoPoint": {
            "lat": 4.1283564538015405,
            "lon": -55.91178573695768
        }
    },
    {
        "fid": 93,
        "name": "Svalbard",
        "value": "SJ",
        "geoPoint": {
            "lat": 78.95756028503601,
            "lon": 18.507223598587753
        }
    },
    {
        "fid": 181,
        "name": "Sweden",
        "value": "SE",
        "geoPoint": {
            "lat": 63.41531473581379,
            "lon": 16.98930882559503
        }
    },
    {
        "fid": 159,
        "name": "Switzerland",
        "value": "CH",
        "geoPoint": {
            "lat": 46.8076420282503,
            "lon": 8.235571215631559
        }
    },
    {
        "fid": 151,
        "name": "Syria",
        "value": "SY",
        "geoPoint": {
            "lat": 35.037947118434836,
            "lon": 38.51530381093301
        }
    },
    {
        "fid": 205,
        "name": "Tajikistan",
        "value": "TJ",
        "geoPoint": {
            "lat": 38.54389025919966,
            "lon": 71.03529868664879
        }
    },
    {
        "fid": 113,
        "name": "Tanzania",
        "value": "TZ",
        "geoPoint": {
            "lat": -6.291010917407591,
            "lon": 34.82913890169515
        }
    },
    {
        "fid": 231,
        "name": "Thailand",
        "value": "TH",
        "geoPoint": {
            "lat": 15.195252775642723,
            "lon": 101.01811824191985
        }
    },
    {
        "fid": 217,
        "name": "Timor-Leste",
        "value": "TL",
        "geoPoint": {
            "lat": -8.82326700544599,
            "lon": 125.85330653709721
        }
    },
    {
        "fid": 136,
        "name": "Togo",
        "value": "TG",
        "geoPoint": {
            "lat": 8.542073908520566,
            "lon": 0.9749388648118411
        }
    },
    {
        "fid": 8,
        "name": "Tokelau",
        "value": "TK",
        "geoPoint": {
            "lat": -9.195175340458805,
            "lon": -171.85265947587487
        }
    },
    {
        "fid": 9,
        "name": "Tonga",
        "value": "TO",
        "geoPoint": {
            "lat": -20.415863714771465,
            "lon": -174.84040406624848
        }
    },
    {
        "fid": 66,
        "name": "Trinidad and Tobago",
        "value": "TT",
        "geoPoint": {
            "lat": 10.469081039636201,
            "lon": -61.25302935707671
        }
    },
    {
        "fid": 137,
        "name": "Tunisia",
        "value": "TN",
        "geoPoint": {
            "lat": 34.160923587598184,
            "lon": 9.55968941884532
        }
    },
    {
        "fid": 152,
        "name": "Turkey",
        "value": "TR",
        "geoPoint": {
            "lat": 39.10238825160671,
            "lon": 35.17702530437081
        }
    },
    {
        "fid": 206,
        "name": "Turkmenistan",
        "value": "TM",
        "geoPoint": {
            "lat": 39.27154134067209,
            "lon": 58.35217720039207
        }
    },
    {
        "fid": 67,
        "name": "Turks and Caicos Islands",
        "value": "TC",
        "geoPoint": {
            "lat": 21.836780954706622,
            "lon": -71.81306999512618
        }
    },
    {
        "fid": 224,
        "name": "Tuvalu",
        "value": "TV",
        "geoPoint": {
            "lat": -7.831294493024052,
            "lon": 178.55975413263457
        }
    },
    {
        "fid": 153,
        "name": "Uganda",
        "value": "UG",
        "geoPoint": {
            "lat": 1.2811355177717774,
            "lon": 32.38643388633621
        }
    },
    {
        "fid": 191,
        "name": "Ukraine",
        "value": "UA",
        "geoPoint": {
            "lat": 49.096963956966896,
            "lon": 31.348882085289574
        }
    },
    {
        "fid": 207,
        "name": "United Arab Emirates",
        "value": "AE",
        "geoPoint": {
            "lat": 23.9175255378765,
            "lon": 54.3393080442153
        }
    },
    {
        "fid": 90,
        "name": "United Kingdom",
        "value": "GB",
        "geoPoint": {
            "lat": 54.35689451339836,
            "lon": -2.9519301572761765
        }
    },
    {
        "fid": 155,
        "name": "United States",
        "value": "US",
        "geoPoint": {
            "lat": 51.19964332582137,
            "lon": -119.25836412507145
        }
    },
    {
        "fid": 2,
        "name": "United States Minor Outlying Islands",
        "value": "UM",
        "geoPoint": {
            "lat": 16.61327557498049,
            "lon": -5.326800988191774
        }
    },
    {
        "fid": 23,
        "name": "Uruguay",
        "value": "UY",
        "geoPoint": {
            "lat": -32.82168047024834,
            "lon": -56.00892370708919
        }
    },
    {
        "fid": 68,
        "name": "US Virgin Islands",
        "value": "VI",
        "geoPoint": {
            "lat": 17.905749954791652,
            "lon": -64.80714138904045
        }
    },
    {
        "fid": 213,
        "name": "Uzbekistan",
        "value": "UZ",
        "geoPoint": {
            "lat": 41.82777584004587,
            "lon": 63.08744749642731
        }
    },
    {
        "fid": 225,
        "name": "Vanuatu",
        "value": "VU",
        "geoPoint": {
            "lat": -16.272477404639456,
            "lon": 167.72393574393047
        }
    },
    {
        "fid": 179,
        "name": "Vatican City",
        "value": "VA",
        "geoPoint": {
            "lat": 41.90398806253162,
            "lon": 12.451354969627594
        }
    },
    {
        "fid": 69,
        "name": "Venezuela",
        "value": "VE",
        "geoPoint": {
            "lat": 7.141225723108773,
            "lon": -66.17503623846358
        }
    },
    {
        "fid": 232,
        "name": "Vietnam",
        "value": "VN",
        "geoPoint": {
            "lat": 16.8237416879964,
            "lon": 106.27769254952167
        }
    },
    {
        "fid": 10,
        "name": "Wallis and Futuna",
        "value": "WF",
        "geoPoint": {
            "lat": -13.789038231403616,
            "lon": -177.15288401852902
        }
    },
    {
        "fid": 208,
        "name": "Yemen",
        "value": "YE",
        "geoPoint": {
            "lat": 15.920806786251623,
            "lon": 47.60952045051823
        }
    },
    {
        "fid": 114,
        "name": "Zambia",
        "value": "ZM",
        "geoPoint": {
            "lat": -13.485644431427755,
            "lon": 27.78144518583083
        }
    },
    {
        "fid": 115,
        "name": "Zimbabwe",
        "value": "ZW",
        "geoPoint": {
            "lat": -19.023325027175453,
            "lon": 29.872167864261346
        }
    }
]

export const departaments = [
   {
      "name":"AMAZONAS",
      "value":"91",
      "text":"COLOMBIA, AMAZONAS",
      "code":"CO-91",
      "geoPoint":{
         "lat":-1.536217608999948,
         "lon":-71.50108107399996
      },
      "geoRectangle":{
         "Xmin":-73.83408107399995,
         "Xmax":-69.16808107399996,
         "Ymin":-3.869217608999948,
         "Ymax":0.7967823910000522
      },
      "municipality":[
         {
            "name":"LETICIA",
            "value":"001",
            "text":"COLOMBIA, AMAZONAS, LETICIA",
            "code":"CO-91-001",
            "geoPoint":{
               "lat":-4.208899999999971,
               "lon":-69.93810999999994
            },
            "geoRectangle":{
               "Xmin":-70.40810999999994,
               "Xmax":-69.46810999999994,
               "Ymin":-4.678899999999971,
               "Ymax":-3.7388999999999712
            }
         },
         {
            "name":"EL ENCANTO",
            "value":"263",
            "text":"COLOMBIA, AMAZONAS, EL ENCANTO",
            "code":"CO-91-263",
            "geoPoint":{
               "lat":-1.7484999999999786,
               "lon":-73.20908999999995
            },
            "geoRectangle":{
               "Xmin":-73.90508999999994,
               "Xmax":-72.51308999999995,
               "Ymin":-2.444499999999979,
               "Ymax":-1.0524999999999785
            }
         },
         {
            "name":"LA CHORRERA",
            "value":"405",
            "text":"COLOMBIA, AMAZONAS, LA CHORRERA",
            "code":"CO-91-405",
            "geoPoint":{
               "lat":-1.4426299999999515,
               "lon":-72.78828999999996
            },
            "geoRectangle":{
               "Xmin":-73.69828999999996,
               "Xmax":-71.87828999999996,
               "Ymin":-2.3526299999999516,
               "Ymax":-0.5326299999999514
            }
         },
         {
            "name":"LA PEDRERA",
            "value":"407",
            "text":"COLOMBIA, AMAZONAS, LA PEDRERA",
            "code":"CO-91-407",
            "geoPoint":{
               "lat":-1.3264499999999657,
               "lon":-69.58166999999997
            },
            "geoRectangle":{
               "Xmin":-70.26666999999998,
               "Xmax":-68.89666999999997,
               "Ymin":-2.0114499999999658,
               "Ymax":-0.6414499999999657
            }
         },
         {
            "name":"LA VICTORIA (Pacoa)",
            "value":"430",
            "text":"COLOMBIA, AMAZONAS, LA VICTORIA (Pacoa)",
            "code":"CO-91-430",
            "geoPoint":{
               "lat":0.05507000000005746,
               "lon":-71.22202999999996
            },
            "geoRectangle":{
               "Xmin":-71.23202999999997,
               "Xmax":-71.21202999999996,
               "Ymin":0.04507000000005746,
               "Ymax":0.06507000000005746
            }
         },
         {
            "name":"MIRITÍ-PARANÁ (Campoamor)",
            "value":"460",
            "text":"COLOMBIA, AMAZONAS, MIRITÍ-PARANÁ (Campoamor)",
            "code":"CO-91-460",
            "geoPoint":{
               "lat":-1.2363899999999717,
               "lon":-69.92193999999995
            },
            "geoRectangle":{
               "Xmin":-69.93193999999995,
               "Xmax":-69.91193999999994,
               "Ymin":-1.2463899999999717,
               "Ymax":-1.2263899999999717
            }
         },
         {
            "name":"PUERTO ALEGRÍA",
            "value":"530",
            "text":"COLOMBIA, AMAZONAS, PUERTO ALEGRÍA",
            "code":"CO-91-530",
            "geoPoint":{
               "lat":-1.0054399999999646,
               "lon":-74.01504999999997
            },
            "geoRectangle":{
               "Xmin":-74.64004999999997,
               "Xmax":-73.39004999999997,
               "Ymin":-1.6304399999999646,
               "Ymax":-0.3804399999999646
            }
         },
         {
            "name":"PUERTO ARICA",
            "value":"536",
            "text":"COLOMBIA, AMAZONAS, PUERTO ARICA",
            "code":"CO-91-536",
            "geoPoint":{
               "lat":-2.148469999999975,
               "lon":-71.75444999999996
            },
            "geoRectangle":{
               "Xmin":-72.41244999999996,
               "Xmax":-71.09644999999996,
               "Ymin":-2.8064699999999747,
               "Ymax":-1.4904699999999749
            }
         },
         {
            "name":"TARAPACÁ",
            "value":"798",
            "text":"COLOMBIA, AMAZONAS, TARAPACÁ",
            "code":"CO-91-798",
            "geoPoint":{
               "lat":-2.8920399999999518,
               "lon":-69.74153999999999
            },
            "geoRectangle":{
               "Xmin":-70.28053999999999,
               "Xmax":-69.20253999999998,
               "Ymin":-3.431039999999952,
               "Ymax":-2.3530399999999516
            }
         },
         {
            "name":"PUERTO NARIÑO",
            "value":"540",
            "text":"COLOMBIA, AMAZONAS, PUERTO NARIÑO",
            "code":"CO-91-540",
            "geoPoint":{
               "lat":-3.769959999999969,
               "lon":-70.38706999999994
            },
            "geoRectangle":{
               "Xmin":-70.66006999999993,
               "Xmax":-70.11406999999994,
               "Ymin":-4.042959999999969,
               "Ymax":-3.496959999999969
            }
         },
         {
            "name":"SANTANDER (Araracuara)",
            "value":"669",
            "text":"COLOMBIA, AMAZONAS, SANTANDER (Araracuara)",
            "code":"CO-91-669",
            "geoPoint":{
               "lat":-0.5832999999999515,
               "lon":-72.40829999999994
            },
            "geoRectangle":{
               "Xmin":-72.43329999999995,
               "Xmax":-72.38329999999993,
               "Ymin":-0.6082999999999515,
               "Ymax":-0.5582999999999515
            }
         }
      ]
   },
   {
      "name":"ANTIOQUIA",
      "value":"05",
      "text":"COLOMBIA, ANTIOQUIA",
      "code":"CO-05",
      "geoPoint":{
         "lat":6.923661741000046,
         "lon":-75.57002545199998
      },
      "geoRectangle":{
         "Xmin":-77.24802545199998,
         "Xmax":-73.89202545199998,
         "Ymin":5.245661741000046,
         "Ymax":8.601661741000047
      },
      "municipality":[
         {
            "name":"SANTUARIO",
            "value":"697",
            "text":"COLOMBIA, ANTIOQUIA, SANTUARIO",
            "code":"CO-05-697",
            "geoPoint":{
               "lat":6.172520041546093,
               "lon":-75.23596990166044
            },
            "geoRectangle":{
               "Xmin":-75.23696990166044,
               "Xmax":-75.23496990166043,
               "Ymin":6.171520041546093,
               "Ymax":6.173520041546094
            }
         },
         {
            "name":"SANTO DOMINGO",
            "value":"690",
            "text":"COLOMBIA, ANTIOQUIA, SANTO DOMINGO",
            "code":"CO-05-690",
            "geoPoint":{
               "lat":6.471400000000074,
               "lon":-75.16427999999996
            },
            "geoRectangle":{
               "Xmin":-75.27027999999996,
               "Xmax":-75.05827999999997,
               "Ymin":6.365400000000074,
               "Ymax":6.577400000000074
            }
         },
         {
            "name":"SANTA ROSA DE OSOS",
            "value":"686",
            "text":"COLOMBIA, ANTIOQUIA, SANTA ROSA DE OSOS",
            "code":"CO-05-686",
            "geoPoint":{
               "lat":6.647190000000023,
               "lon":-75.46024999999997
            },
            "geoRectangle":{
               "Xmin":-75.66324999999998,
               "Xmax":-75.25724999999997,
               "Ymin":6.444190000000023,
               "Ymax":6.8501900000000235
            }
         },
         {
            "name":"SANTA BÁRBARA",
            "value":"679",
            "text":"COLOMBIA, ANTIOQUIA, SANTA BÁRBARA",
            "code":"CO-05-679",
            "geoPoint":{
               "lat":5.875600000000077,
               "lon":-75.56790999999998
            },
            "geoRectangle":{
               "Xmin":-75.67090999999998,
               "Xmax":-75.46490999999999,
               "Ymin":5.772600000000077,
               "Ymax":5.9786000000000765
            }
         },
         {
            "name":"GIRALDO",
            "value":"306",
            "text":"COLOMBIA, ANTIOQUIA, GIRALDO",
            "code":"CO-05-306",
            "geoPoint":{
               "lat":6.6806700000000205,
               "lon":-75.95341999999994
            },
            "geoRectangle":{
               "Xmin":-76.02341999999993,
               "Xmax":-75.88341999999994,
               "Ymin":6.61067000000002,
               "Ymax":6.750670000000021
            }
         },
         {
            "name":"FREDONIA",
            "value":"282",
            "text":"COLOMBIA, ANTIOQUIA, FREDONIA",
            "code":"CO-05-282",
            "geoPoint":{
               "lat":5.927280000000053,
               "lon":-75.67149999999998
            },
            "geoRectangle":{
               "Xmin":-75.77549999999998,
               "Xmax":-75.56749999999998,
               "Ymin":5.823280000000053,
               "Ymax":6.031280000000053
            }
         },
         {
            "name":"ENVIGADO",
            "value":"266",
            "text":"COLOMBIA, ANTIOQUIA, ENVIGADO",
            "code":"CO-05-266",
            "geoPoint":{
               "lat":6.163950000000057,
               "lon":-75.57325999999995
            },
            "geoRectangle":{
               "Xmin":-75.61225999999995,
               "Xmax":-75.53425999999995,
               "Ymin":6.124950000000057,
               "Ymax":6.202950000000056
            }
         },
         {
            "name":"ENTRERRIOS",
            "value":"264",
            "text":"COLOMBIA, ANTIOQUIA, ENTRERRIOS",
            "code":"CO-05-264",
            "geoPoint":{
               "lat":6.566130000000044,
               "lon":-75.51722999999998
            },
            "geoRectangle":{
               "Xmin":-75.61222999999998,
               "Xmax":-75.42222999999998,
               "Ymin":6.471130000000044,
               "Ymax":6.6611300000000435
            }
         },
         {
            "name":"EL BAGRE",
            "value":"250",
            "text":"COLOMBIA, ANTIOQUIA, EL BAGRE",
            "code":"CO-05-250",
            "geoPoint":{
               "lat":7.599860000000035,
               "lon":-74.81280999999996
            },
            "geoRectangle":{
               "Xmin":-75.05580999999995,
               "Xmax":-74.56980999999996,
               "Ymin":7.356860000000035,
               "Ymax":7.8428600000000355
            }
         },
         {
            "name":"EBÉJICO",
            "value":"240",
            "text":"COLOMBIA, ANTIOQUIA, EBÉJICO",
            "code":"CO-05-240",
            "geoPoint":{
               "lat":6.326400000000035,
               "lon":-75.76652999999999
            },
            "geoRectangle":{
               "Xmin":-75.85252999999999,
               "Xmax":-75.68052999999999,
               "Ymin":6.240400000000035,
               "Ymax":6.412400000000035
            }
         },
         {
            "name":"DON MATÍAS",
            "value":"237",
            "text":"COLOMBIA, ANTIOQUIA, DON MATÍAS",
            "code":"CO-05-237",
            "geoPoint":{
               "lat":6.500000000000057,
               "lon":-75.33332999999999
            },
            "geoRectangle":{
               "Xmin":-76.33332999999999,
               "Xmax":-74.33332999999999,
               "Ymin":5.500000000000057,
               "Ymax":7.500000000000057
            }
         },
         {
            "name":"COPACABANA",
            "value":"212",
            "text":"COLOMBIA, ANTIOQUIA, COPACABANA",
            "code":"CO-05-212",
            "geoPoint":{
               "lat":6.3464100000000485,
               "lon":-75.50799999999998
            },
            "geoRectangle":{
               "Xmin":-75.56299999999999,
               "Xmax":-75.45299999999997,
               "Ymin":6.291410000000049,
               "Ymax":6.401410000000048
            }
         },
         {
            "name":"CONCORDIA",
            "value":"209",
            "text":"COLOMBIA, ANTIOQUIA, CONCORDIA",
            "code":"CO-05-209",
            "geoPoint":{
               "lat":6.046470000000056,
               "lon":-75.90702999999996
            },
            "geoRectangle":{
               "Xmin":-76.00102999999996,
               "Xmax":-75.81302999999997,
               "Ymin":5.952470000000056,
               "Ymax":6.1404700000000565
            }
         },
         {
            "name":"CONCEPCIÓN",
            "value":"206",
            "text":"COLOMBIA, ANTIOQUIA, CONCEPCIÓN",
            "code":"CO-05-206",
            "geoPoint":{
               "lat":6.39423000000005,
               "lon":-75.25811999999996
            },
            "geoRectangle":{
               "Xmin":-75.33911999999997,
               "Xmax":-75.17711999999996,
               "Ymin":6.31323000000005,
               "Ymax":6.4752300000000504
            }
         },
         {
            "name":"COCORNÁ",
            "value":"197",
            "text":"COLOMBIA, ANTIOQUIA, COCORNÁ",
            "code":"CO-05-197",
            "geoPoint":{
               "lat":6.057880000000068,
               "lon":-75.18642999999997
            },
            "geoRectangle":{
               "Xmin":-75.30642999999998,
               "Xmax":-75.06642999999997,
               "Ymin":5.937880000000068,
               "Ymax":6.177880000000068
            }
         },
         {
            "name":"CISNEROS",
            "value":"190",
            "text":"COLOMBIA, ANTIOQUIA, CISNEROS",
            "code":"CO-05-190",
            "geoPoint":{
               "lat":6.53857000000005,
               "lon":-75.08754999999996
            },
            "geoRectangle":{
               "Xmin":-75.13354999999997,
               "Xmax":-75.04154999999996,
               "Ymin":6.4925700000000495,
               "Ymax":6.58457000000005
            }
         },
         {
            "name":"CHIGORODÓ",
            "value":"172",
            "text":"COLOMBIA, ANTIOQUIA, CHIGORODÓ",
            "code":"CO-05-172",
            "geoPoint":{
               "lat":7.666270000000054,
               "lon":-76.68092999999999
            },
            "geoRectangle":{
               "Xmin":-76.84392999999999,
               "Xmax":-76.51792999999999,
               "Ymin":7.503270000000054,
               "Ymax":7.829270000000054
            }
         },
         {
            "name":"CAUCASIA",
            "value":"154",
            "text":"COLOMBIA, ANTIOQUIA, CAUCASIA",
            "code":"CO-05-154",
            "geoPoint":{
               "lat":7.988180000000057,
               "lon":-75.19782999999995
            },
            "geoRectangle":{
               "Xmin":-75.47282999999996,
               "Xmax":-74.92282999999995,
               "Ymin":7.713180000000056,
               "Ymax":8.263180000000057
            }
         },
         {
            "name":"CAROLINA",
            "value":"150",
            "text":"COLOMBIA, ANTIOQUIA, CAROLINA",
            "code":"CO-05-150",
            "geoPoint":{
               "lat":6.724570000000028,
               "lon":-75.28075999999999
            },
            "geoRectangle":{
               "Xmin":-75.36075999999998,
               "Xmax":-75.20075999999999,
               "Ymin":6.644570000000028,
               "Ymax":6.804570000000028
            }
         },
         {
            "name":"EL CARMEN DE VIBORAL",
            "value":"148",
            "text":"COLOMBIA, ANTIOQUIA, EL CARMEN DE VIBORAL",
            "code":"CO-05-148",
            "geoPoint":{
               "lat":6.080850000000055,
               "lon":-75.33462999999995
            },
            "geoRectangle":{
               "Xmin":-75.48162999999995,
               "Xmax":-75.18762999999994,
               "Ymin":5.933850000000055,
               "Ymax":6.227850000000055
            }
         },
         {
            "name":"CAREPA",
            "value":"147",
            "text":"COLOMBIA, ANTIOQUIA, CAREPA",
            "code":"CO-05-147",
            "geoPoint":{
               "lat":7.7537300000000755,
               "lon":-76.65405999999996
            },
            "geoRectangle":{
               "Xmin":-76.78705999999995,
               "Xmax":-76.52105999999996,
               "Ymin":7.6207300000000755,
               "Ymax":7.8867300000000755
            }
         },
         {
            "name":"CARAMANTA",
            "value":"145",
            "text":"COLOMBIA, ANTIOQUIA, CARAMANTA",
            "code":"CO-05-145",
            "geoPoint":{
               "lat":5.548790000000054,
               "lon":-75.64409999999998
            },
            "geoRectangle":{
               "Xmin":-75.70409999999998,
               "Xmax":-75.58409999999998,
               "Ymin":5.488790000000054,
               "Ymax":5.608790000000053
            }
         },
         {
            "name":"CARACOLÍ",
            "value":"142",
            "text":"COLOMBIA, ANTIOQUIA, CARACOLÍ",
            "code":"CO-05-142",
            "geoPoint":{
               "lat":6.409160000000043,
               "lon":-74.75692999999995
            },
            "geoRectangle":{
               "Xmin":-74.86892999999995,
               "Xmax":-74.64492999999996,
               "Ymin":6.2971600000000425,
               "Ymax":6.521160000000043
            }
         },
         {
            "name":"CAÑASGORDAS",
            "value":"138",
            "text":"COLOMBIA, ANTIOQUIA, CAÑASGORDAS",
            "code":"CO-05-138",
            "geoPoint":{
               "lat":6.750000000000057,
               "lon":-76.02468999999996
            },
            "geoRectangle":{
               "Xmin":-76.15368999999997,
               "Xmax":-75.89568999999996,
               "Ymin":6.621000000000057,
               "Ymax":6.879000000000056
            }
         },
         {
            "name":" CAMPAMENTO",
            "value":"134",
            "text":"COLOMBIA, ANTIOQUIA,  CAMPAMENTO",
            "code":"CO-05-134",
            "geoPoint":{
               "lat":6.978990000000067,
               "lon":-75.29603999999995
            },
            "geoRectangle":{
               "Xmin":-75.39703999999995,
               "Xmax":-75.19503999999995,
               "Ymin":6.877990000000067,
               "Ymax":7.079990000000067
            }
         },
         {
            "name":"CALDAS",
            "value":"129",
            "text":"COLOMBIA, ANTIOQUIA, CALDAS",
            "code":"CO-05-129",
            "geoPoint":{
               "lat":6.092580000000055,
               "lon":-75.63688999999994
            },
            "geoRectangle":{
               "Xmin":-75.69988999999994,
               "Xmax":-75.57388999999993,
               "Ymin":6.029580000000055,
               "Ymax":6.155580000000055
            }
         },
         {
            "name":"CAICEDO",
            "value":"125",
            "text":"COLOMBIA, ANTIOQUIA, CAICEDO",
            "code":"CO-05-125",
            "geoPoint":{
               "lat":6.405170000000055,
               "lon":-75.98246999999998
            },
            "geoRectangle":{
               "Xmin":-76.06746999999997,
               "Xmax":-75.89746999999998,
               "Ymin":6.320170000000055,
               "Ymax":6.490170000000055
            }
         },
         {
            "name":"SAN VICENTE",
            "value":"674",
            "text":"COLOMBIA, ANTIOQUIA, SAN VICENTE",
            "code":"CO-05-674",
            "geoPoint":{
               "lat":6.2822700000000395,
               "lon":-75.33240999999998
            },
            "geoRectangle":{
               "Xmin":-75.41840999999998,
               "Xmax":-75.24640999999998,
               "Ymin":6.196270000000039,
               "Ymax":6.36827000000004
            }
         },
         {
            "name":"SAN ROQUE",
            "value":"670",
            "text":"COLOMBIA, ANTIOQUIA, SAN ROQUE",
            "code":"CO-05-670",
            "geoPoint":{
               "lat":6.485310000000027,
               "lon":-75.01944999999995
            },
            "geoRectangle":{
               "Xmin":-75.15944999999995,
               "Xmax":-74.87944999999995,
               "Ymin":6.345310000000027,
               "Ymax":6.6253100000000265
            }
         },
         {
            "name":"SAN RAFAEL",
            "value":"667",
            "text":"COLOMBIA, ANTIOQUIA, SAN RAFAEL",
            "code":"CO-05-667",
            "geoPoint":{
               "lat":6.295220000000029,
               "lon":-75.02811999999994
            },
            "geoRectangle":{
               "Xmin":-75.15711999999995,
               "Xmax":-74.89911999999994,
               "Ymin":6.166220000000029,
               "Ymax":6.4242200000000285
            }
         },
         {
            "name":"SAN PEDRO DE URABA",
            "value":"665",
            "text":"COLOMBIA, ANTIOQUIA, SAN PEDRO DE URABA",
            "code":"CO-05-665",
            "geoPoint":{
               "lat":8.276400000000024,
               "lon":-76.37964999999997
            },
            "geoRectangle":{
               "Xmin":-76.56164999999997,
               "Xmax":-76.19764999999997,
               "Ymin":8.094400000000023,
               "Ymax":8.458400000000024
            }
         },
         {
            "name":"SAN PEDRO",
            "value":"664",
            "text":"COLOMBIA, ANTIOQUIA, SAN PEDRO",
            "code":"CO-05-664",
            "geoPoint":{
               "lat":6.464510000000075,
               "lon":-75.56001999999995
            },
            "geoRectangle":{
               "Xmin":-75.57501999999995,
               "Xmax":-75.54501999999995,
               "Ymin":6.449510000000076,
               "Ymax":6.479510000000075
            }
         },
         {
            "name":"SAN LUIS",
            "value":"660",
            "text":"COLOMBIA, ANTIOQUIA, SAN LUIS",
            "code":"CO-05-660",
            "geoPoint":{
               "lat":6.042460000000062,
               "lon":-74.99464999999998
            },
            "geoRectangle":{
               "Xmin":-75.12764999999997,
               "Xmax":-74.86164999999998,
               "Ymin":5.909460000000062,
               "Ymax":6.175460000000062
            }
         },
         {
            "name":"SAN JUAN DE URABÁ",
            "value":"659",
            "text":"COLOMBIA, ANTIOQUIA, SAN JUAN DE URABÁ",
            "code":"CO-05-659",
            "geoPoint":{
               "lat":8.760420000000067,
               "lon":-76.52946999999995
            },
            "geoRectangle":{
               "Xmin":-76.63446999999995,
               "Xmax":-76.42446999999994,
               "Ymin":8.655420000000067,
               "Ymax":8.865420000000068
            }
         },
         {
            "name":"SAN CARLOS",
            "value":"649",
            "text":"COLOMBIA, ANTIOQUIA, SAN CARLOS",
            "code":"CO-05-649",
            "geoPoint":{
               "lat":7.791770000000042,
               "lon":-74.77315999999996
            },
            "geoRectangle":{
               "Xmin":-74.79315999999996,
               "Xmax":-74.75315999999997,
               "Ymin":7.771770000000043,
               "Ymax":7.811770000000042
            }
         },
         {
            "name":"SAN ANDRÈS",
            "value":"647",
            "text":"COLOMBIA, ANTIOQUIA, SAN ANDRÈS",
            "code":"CO-05-647",
            "geoPoint":{
               "lat":6.9033300000000395,
               "lon":-75.68249999999995
            },
            "geoRectangle":{
               "Xmin":-75.69749999999995,
               "Xmax":-75.66749999999995,
               "Ymin":6.88833000000004,
               "Ymax":6.918330000000039
            }
         },
         {
            "name":"SALGAR",
            "value":"642",
            "text":"COLOMBIA, ANTIOQUIA, SALGAR",
            "code":"CO-05-642",
            "geoPoint":{
               "lat":5.962720000000047,
               "lon":-75.97856999999993
            },
            "geoRectangle":{
               "Xmin":-76.10956999999993,
               "Xmax":-75.84756999999993,
               "Ymin":5.831720000000047,
               "Ymax":6.093720000000047
            }
         },
         {
            "name":"URRAO",
            "value":"847",
            "text":"COLOMBIA, ANTIOQUIA, URRAO",
            "code":"CO-05-847",
            "geoPoint":{
               "lat":6.315240000000074,
               "lon":-76.13290999999998
            },
            "geoRectangle":{
               "Xmin":-76.43190999999999,
               "Xmax":-75.83390999999997,
               "Ymin":6.0162400000000735,
               "Ymax":6.614240000000074
            }
         },
         {
            "name":"CÁCERES",
            "value":"120",
            "text":"COLOMBIA, ANTIOQUIA, CÁCERES",
            "code":"CO-05-120",
            "geoPoint":{
               "lat":7.578220000000044,
               "lon":-75.35042999999996
            },
            "geoRectangle":{
               "Xmin":-75.62642999999996,
               "Xmax":-75.07442999999996,
               "Ymin":7.302220000000045,
               "Ymax":7.854220000000044
            }
         },
         {
            "name":"BETULIA",
            "value":"093",
            "text":"COLOMBIA, ANTIOQUIA, BETULIA",
            "code":"CO-05-093",
            "geoPoint":{
               "lat":6.11340000000007,
               "lon":-75.98434999999995
            },
            "geoRectangle":{
               "Xmin":-76.08834999999995,
               "Xmax":-75.88034999999995,
               "Ymin":6.00940000000007,
               "Ymax":6.21740000000007
            }
         },
         {
            "name":"BETANIA",
            "value":"091",
            "text":"COLOMBIA, ANTIOQUIA, BETANIA",
            "code":"CO-05-091",
            "geoPoint":{
               "lat":5.7454400000000305,
               "lon":-75.97664999999995
            },
            "geoRectangle":{
               "Xmin":-76.06564999999995,
               "Xmax":-75.88764999999995,
               "Ymin":5.65644000000003,
               "Ymax":5.834440000000031
            }
         },
         {
            "name":"BELLO",
            "value":"088",
            "text":"COLOMBIA, ANTIOQUIA, BELLO",
            "code":"CO-05-088",
            "geoPoint":{
               "lat":6.340340000000026,
               "lon":-75.55915999999996
            },
            "geoRectangle":{
               "Xmin":-75.62915999999996,
               "Xmax":-75.48915999999997,
               "Ymin":6.270340000000026,
               "Ymax":6.410340000000026
            }
         },
         {
            "name":"BELMIRA",
            "value":"086",
            "text":"COLOMBIA, ANTIOQUIA, BELMIRA",
            "code":"CO-05-086",
            "geoPoint":{
               "lat":6.606480000000033,
               "lon":-75.66716999999994
            },
            "geoRectangle":{
               "Xmin":-75.78516999999994,
               "Xmax":-75.54916999999995,
               "Ymin":6.488480000000033,
               "Ymax":6.7244800000000335
            }
         },
         {
            "name":"BARBOSA",
            "value":"079",
            "text":"COLOMBIA, ANTIOQUIA, BARBOSA",
            "code":"CO-05-079",
            "geoPoint":{
               "lat":6.436240000000055,
               "lon":-75.33156999999994
            },
            "geoRectangle":{
               "Xmin":-75.42756999999995,
               "Xmax":-75.23556999999994,
               "Ymin":6.340240000000055,
               "Ymax":6.532240000000055
            }
         },
         {
            "name":"ARMENIA",
            "value":"059",
            "text":"COLOMBIA, ANTIOQUIA, ARMENIA",
            "code":"CO-05-059",
            "geoPoint":{
               "lat":6.156850000000077,
               "lon":-75.78717999999998
            },
            "geoRectangle":{
               "Xmin":-75.85417999999997,
               "Xmax":-75.72017999999998,
               "Ymin":6.089850000000077,
               "Ymax":6.223850000000077
            }
         },
         {
            "name":"ARGELIA",
            "value":"055",
            "text":"COLOMBIA, ANTIOQUIA, ARGELIA",
            "code":"CO-05-055",
            "geoPoint":{
               "lat":5.731300000000033,
               "lon":-75.14116999999999
            },
            "geoRectangle":{
               "Xmin":-75.26116999999999,
               "Xmax":-75.02116999999998,
               "Ymin":5.611300000000033,
               "Ymax":5.851300000000033
            }
         },
         {
            "name":"ARBOLETES",
            "value":"051",
            "text":"COLOMBIA, ANTIOQUIA, ARBOLETES",
            "code":"CO-05-051",
            "geoPoint":{
               "lat":8.849750000000029,
               "lon":-76.42610999999994
            },
            "geoRectangle":{
               "Xmin":-76.61410999999994,
               "Xmax":-76.23810999999993,
               "Ymin":8.661750000000028,
               "Ymax":9.03775000000003
            }
         },
         {
            "name":"APARTADÓ",
            "value":"045",
            "text":"COLOMBIA, ANTIOQUIA, APARTADÓ",
            "code":"CO-05-045",
            "geoPoint":{
               "lat":7.882570000000044,
               "lon":-76.62717999999995
            },
            "geoRectangle":{
               "Xmin":-76.78917999999996,
               "Xmax":-76.46517999999995,
               "Ymin":7.720570000000044,
               "Ymax":8.044570000000045
            }
         },
         {
            "name":"ANZÁ",
            "value":"044",
            "text":"COLOMBIA, ANTIOQUIA, ANZÁ",
            "code":"CO-05-044",
            "geoPoint":{
               "lat":6.303220000000067,
               "lon":-75.85380999999995
            },
            "geoRectangle":{
               "Xmin":-75.86380999999996,
               "Xmax":-75.84380999999995,
               "Ymin":6.293220000000067,
               "Ymax":6.313220000000067
            }
         },
         {
            "name":"SANTA FE DE ANTIOQUIA",
            "value":"042",
            "text":"COLOMBIA, ANTIOQUIA, SANTA FE DE ANTIOQUIA",
            "code":"CO-05-042",
            "geoPoint":{
               "lat":6.5593600000000265,
               "lon":-75.82831999999996
            },
            "geoRectangle":{
               "Xmin":-75.96131999999996,
               "Xmax":-75.69531999999997,
               "Ymin":6.4263600000000265,
               "Ymax":6.6923600000000265
            }
         },
         {
            "name":"ANORÍ",
            "value":"040",
            "text":"COLOMBIA, ANTIOQUIA, ANORÍ",
            "code":"CO-05-040",
            "geoPoint":{
               "lat":7.0747800000000325,
               "lon":-75.14796999999999
            },
            "geoRectangle":{
               "Xmin":-75.39096999999998,
               "Xmax":-74.90496999999999,
               "Ymin":6.831780000000032,
               "Ymax":7.317780000000033
            }
         },
         {
            "name":"ANGOSTURA",
            "value":"038",
            "text":"COLOMBIA, ANTIOQUIA, ANGOSTURA",
            "code":"CO-05-038",
            "geoPoint":{
               "lat":6.885490000000061,
               "lon":-75.33494999999994
            },
            "geoRectangle":{
               "Xmin":-75.44794999999993,
               "Xmax":-75.22194999999994,
               "Ymin":6.772490000000061,
               "Ymax":6.998490000000062
            }
         },
         {
            "name":"ANGELÓPOLIS",
            "value":"036",
            "text":"COLOMBIA, ANTIOQUIA, ANGELÓPOLIS",
            "code":"CO-05-036",
            "geoPoint":{
               "lat":6.111360000000047,
               "lon":-75.71022999999997
            },
            "geoRectangle":{
               "Xmin":-75.76522999999997,
               "Xmax":-75.65522999999996,
               "Ymin":6.056360000000048,
               "Ymax":6.166360000000047
            }
         },
         {
            "name":"ANDES",
            "value":"034",
            "text":"COLOMBIA, ANTIOQUIA, ANDES",
            "code":"CO-05-034",
            "geoPoint":{
               "lat":5.656300000000044,
               "lon":-75.87818999999996
            },
            "geoRectangle":{
               "Xmin":-76.02118999999996,
               "Xmax":-75.73518999999996,
               "Ymin":5.5133000000000445,
               "Ymax":5.799300000000044
            }
         },
         {
            "name":"AMALFI",
            "value":"031",
            "text":"COLOMBIA, ANTIOQUIA, AMALFI",
            "code":"CO-05-031",
            "geoPoint":{
               "lat":6.907820000000072,
               "lon":-75.07403999999997
            },
            "geoRectangle":{
               "Xmin":-75.29703999999997,
               "Xmax":-74.85103999999997,
               "Ymin":6.684820000000072,
               "Ymax":7.130820000000072
            }
         },
         {
            "name":"AMAGÁ",
            "value":"030",
            "text":"COLOMBIA, ANTIOQUIA, AMAGÁ",
            "code":"CO-05-030",
            "geoPoint":{
               "lat":6.039670000000058,
               "lon":-75.70291999999995
            },
            "geoRectangle":{
               "Xmin":-75.75591999999995,
               "Xmax":-75.64991999999995,
               "Ymin":5.986670000000058,
               "Ymax":6.092670000000058
            }
         },
         {
            "name":"ALEJANDRÍA",
            "value":"021",
            "text":"COLOMBIA, ANTIOQUIA, ALEJANDRÍA",
            "code":"CO-05-021",
            "geoPoint":{
               "lat":6.3776600000000485,
               "lon":-75.14113999999995
            },
            "geoRectangle":{
               "Xmin":-75.22413999999995,
               "Xmax":-75.05813999999995,
               "Ymin":6.294660000000048,
               "Ymax":6.460660000000049
            }
         },
         {
            "name":"ABRIAQUÍ",
            "value":"004",
            "text":"COLOMBIA, ANTIOQUIA, ABRIAQUÍ",
            "code":"CO-05-004",
            "geoPoint":{
               "lat":6.632450000000063,
               "lon":-76.06434999999993
            },
            "geoRectangle":{
               "Xmin":-76.17034999999993,
               "Xmax":-75.95834999999994,
               "Ymin":6.526450000000063,
               "Ymax":6.7384500000000624
            }
         },
         {
            "name":"ABEJORRAL",
            "value":"002",
            "text":"COLOMBIA, ANTIOQUIA, ABEJORRAL",
            "code":"CO-05-002",
            "geoPoint":{
               "lat":5.789510000000064,
               "lon":-75.42725999999993
            },
            "geoRectangle":{
               "Xmin":-75.57025999999993,
               "Xmax":-75.28425999999993,
               "Ymin":5.646510000000064,
               "Ymax":5.932510000000064
            }
         },
         {
            "name":"ZARAGOZA",
            "value":"895",
            "text":"COLOMBIA, ANTIOQUIA, ZARAGOZA",
            "code":"CO-05-895",
            "geoPoint":{
               "lat":7.4887200000000576,
               "lon":-74.86748999999998
            },
            "geoRectangle":{
               "Xmin":-75.09348999999997,
               "Xmax":-74.64148999999998,
               "Ymin":7.262720000000058,
               "Ymax":7.7147200000000575
            }
         },
         {
            "name":"YONDÓ (Casabe)",
            "value":"893",
            "text":"COLOMBIA, ANTIOQUIA, YONDÓ (Casabe)",
            "code":"CO-05-893",
            "geoPoint":{
               "lat":7.02789000000007,
               "lon":-73.89170999999999
            },
            "geoRectangle":{
               "Xmin":-73.90171,
               "Xmax":-73.88170999999998,
               "Ymin":7.017890000000071,
               "Ymax":7.03789000000007
            }
         },
         {
            "name":"YOLOMBÓ",
            "value":"890",
            "text":"COLOMBIA, ANTIOQUIA, YOLOMBÓ",
            "code":"CO-05-890",
            "geoPoint":{
               "lat":6.598960000000034,
               "lon":-75.01037999999994
            },
            "geoRectangle":{
               "Xmin":-75.27137999999994,
               "Xmax":-74.74937999999995,
               "Ymin":6.337960000000034,
               "Ymax":6.859960000000034
            }
         },
         {
            "name":"YARUMAL",
            "value":"887",
            "text":"COLOMBIA, ANTIOQUIA, YARUMAL",
            "code":"CO-05-887",
            "geoPoint":{
               "lat":6.9625800000000595,
               "lon":-75.41771999999997
            },
            "geoRectangle":{
               "Xmin":-75.61471999999998,
               "Xmax":-75.22071999999997,
               "Ymin":6.765580000000059,
               "Ymax":7.15958000000006
            }
         },
         {
            "name":"YALI",
            "value":"885",
            "text":"COLOMBIA, ANTIOQUIA, YALI",
            "code":"CO-05-885",
            "geoPoint":{
               "lat":6.676060000000064,
               "lon":-74.84105999999997
            },
            "geoRectangle":{
               "Xmin":-74.97505999999997,
               "Xmax":-74.70705999999997,
               "Ymin":6.542060000000063,
               "Ymax":6.810060000000064
            }
         },
         {
            "name":"VENECIA",
            "value":"861",
            "text":"COLOMBIA, ANTIOQUIA, VENECIA",
            "code":"CO-05-861",
            "geoPoint":{
               "lat":5.964170000000024,
               "lon":-75.73479999999995
            },
            "geoRectangle":{
               "Xmin":-75.81079999999994,
               "Xmax":-75.65879999999996,
               "Ymin":5.8881700000000246,
               "Ymax":6.040170000000024
            }
         },
         {
            "name":"VEGACHÍ",
            "value":"858",
            "text":"COLOMBIA, ANTIOQUIA, VEGACHÍ",
            "code":"CO-05-858",
            "geoPoint":{
               "lat":6.773010000000056,
               "lon":-74.79841999999996
            },
            "geoRectangle":{
               "Xmin":-74.95341999999997,
               "Xmax":-74.64341999999996,
               "Ymin":6.618010000000056,
               "Ymax":6.928010000000056
            }
         },
         {
            "name":"VALPARAÍSO",
            "value":"856",
            "text":"COLOMBIA, ANTIOQUIA, VALPARAÍSO",
            "code":"CO-05-856",
            "geoPoint":{
               "lat":5.616210000000024,
               "lon":-75.62474999999995
            },
            "geoRectangle":{
               "Xmin":-75.69974999999995,
               "Xmax":-75.54974999999995,
               "Ymin":5.5412100000000235,
               "Ymax":5.691210000000024
            }
         },
         {
            "name":"VALDIVIA",
            "value":"854",
            "text":"COLOMBIA, ANTIOQUIA, VALDIVIA",
            "code":"CO-05-854",
            "geoPoint":{
               "lat":7.16435000000007,
               "lon":-75.43853999999999
            },
            "geoRectangle":{
               "Xmin":-75.59853999999999,
               "Xmax":-75.27853999999999,
               "Ymin":7.00435000000007,
               "Ymax":7.32435000000007
            }
         },
         {
            "name":"URAMITA",
            "value":"842",
            "text":"COLOMBIA, ANTIOQUIA, URAMITA",
            "code":"CO-05-842",
            "geoPoint":{
               "lat":6.902010000000075,
               "lon":-76.17620999999997
            },
            "geoRectangle":{
               "Xmin":-76.28920999999997,
               "Xmax":-76.06320999999997,
               "Ymin":6.789010000000075,
               "Ymax":7.015010000000076
            }
         },
         {
            "name":"TURBO",
            "value":"837",
            "text":"COLOMBIA, ANTIOQUIA, TURBO",
            "code":"CO-05-837",
            "geoPoint":{
               "lat":8.092930000000024,
               "lon":-76.72899999999998
            },
            "geoRectangle":{
               "Xmin":-77.16799999999998,
               "Xmax":-76.28999999999999,
               "Ymin":7.653930000000024,
               "Ymax":8.531930000000024
            }
         },
         {
            "name":"TOLEDO",
            "value":"819",
            "text":"COLOMBIA, ANTIOQUIA, TOLEDO",
            "code":"CO-05-819",
            "geoPoint":{
               "lat":7.010760000000062,
               "lon":-75.69199999999995
            },
            "geoRectangle":{
               "Xmin":-75.75899999999994,
               "Xmax":-75.62499999999996,
               "Ymin":6.943760000000061,
               "Ymax":7.077760000000062
            }
         },
         {
            "name":"TITIRIBÍ",
            "value":"809",
            "text":"COLOMBIA, ANTIOQUIA, TITIRIBÍ",
            "code":"CO-05-809",
            "geoPoint":{
               "lat":6.063100000000077,
               "lon":-75.79346999999996
            },
            "geoRectangle":{
               "Xmin":-75.86246999999996,
               "Xmax":-75.72446999999995,
               "Ymin":5.994100000000077,
               "Ymax":6.132100000000077
            }
         },
         {
            "name":"MEDELLÍN",
            "value":"001",
            "text":"COLOMBIA, ANTIOQUIA, MEDELLÍN",
            "code":"CO-05-001",
            "geoPoint":{
               "lat":6.245890000000031,
               "lon":-75.57456999999994
            },
            "geoRectangle":{
               "Xmin":-75.68656999999993,
               "Xmax":-75.46256999999994,
               "Ymin":6.133890000000031,
               "Ymax":6.357890000000031
            }
         },
         {
            "name":"CIUDAD BOLÍVAR",
            "value":"101",
            "text":"COLOMBIA, ANTIOQUIA, CIUDAD BOLÍVAR",
            "code":"CO-05-101",
            "geoPoint":{
               "lat":5.849990000000048,
               "lon":-76.02004999999997
            },
            "geoRectangle":{
               "Xmin":-76.12904999999996,
               "Xmax":-75.91104999999997,
               "Ymin":5.740990000000048,
               "Ymax":5.958990000000048
            }
         },
         {
            "name":"BRICEÑO",
            "value":"107",
            "text":"COLOMBIA, ANTIOQUIA, BRICEÑO",
            "code":"CO-05-107",
            "geoPoint":{
               "lat":7.110740000000021,
               "lon":-75.55155999999994
            },
            "geoRectangle":{
               "Xmin":-75.68055999999994,
               "Xmax":-75.42255999999993,
               "Ymin":6.981740000000022,
               "Ymax":7.239740000000021
            }
         },
         {
            "name":"BURITICÁ",
            "value":"113",
            "text":"COLOMBIA, ANTIOQUIA, BURITICÁ",
            "code":"CO-05-113",
            "geoPoint":{
               "lat":6.719100000000026,
               "lon":-75.90730999999994
            },
            "geoRectangle":{
               "Xmin":-76.03030999999994,
               "Xmax":-75.78430999999993,
               "Ymin":6.596100000000026,
               "Ymax":6.842100000000026
            }
         },
         {
            "name":"TARSO",
            "value":"792",
            "text":"COLOMBIA, ANTIOQUIA, TARSO",
            "code":"CO-05-792",
            "geoPoint":{
               "lat":5.8651400000000535,
               "lon":-75.82173999999998
            },
            "geoRectangle":{
               "Xmin":-75.88773999999998,
               "Xmax":-75.75573999999997,
               "Ymin":5.799140000000054,
               "Ymax":5.931140000000053
            }
         },
         {
            "name":"TARAZÁ",
            "value":"790",
            "text":"COLOMBIA, ANTIOQUIA, TARAZÁ",
            "code":"CO-05-790",
            "geoPoint":{
               "lat":7.582560000000058,
               "lon":-75.40037999999998
            },
            "geoRectangle":{
               "Xmin":-75.69337999999999,
               "Xmax":-75.10737999999998,
               "Ymin":7.2895600000000575,
               "Ymax":7.875560000000058
            }
         },
         {
            "name":"TÁMESIS",
            "value":"789",
            "text":"COLOMBIA, ANTIOQUIA, TÁMESIS",
            "code":"CO-05-789",
            "geoPoint":{
               "lat":5.665810000000022,
               "lon":-75.71344999999997
            },
            "geoRectangle":{
               "Xmin":-75.82244999999996,
               "Xmax":-75.60444999999997,
               "Ymin":5.556810000000022,
               "Ymax":5.774810000000022
            }
         },
         {
            "name":"SOPETRÁN",
            "value":"761",
            "text":"COLOMBIA, ANTIOQUIA, SOPETRÁN",
            "code":"CO-05-761",
            "geoPoint":{
               "lat":6.501500000000021,
               "lon":-75.74316999999996
            },
            "geoRectangle":{
               "Xmin":-75.84716999999996,
               "Xmax":-75.63916999999996,
               "Ymin":6.397500000000021,
               "Ymax":6.6055000000000215
            }
         },
         {
            "name":"SONSÓN",
            "value":"756",
            "text":"COLOMBIA, ANTIOQUIA, SONSÓN",
            "code":"CO-05-756",
            "geoPoint":{
               "lat":5.710620000000063,
               "lon":-75.31067999999993
            },
            "geoRectangle":{
               "Xmin":-75.33067999999993,
               "Xmax":-75.29067999999994,
               "Ymin":5.690620000000063,
               "Ymax":5.730620000000062
            }
         },
         {
            "name":"SEGOVIA",
            "value":"736",
            "text":"COLOMBIA, ANTIOQUIA, SEGOVIA",
            "code":"CO-05-736",
            "geoPoint":{
               "lat":7.07848000000007,
               "lon":-74.70463999999998
            },
            "geoRectangle":{
               "Xmin":-74.95163999999998,
               "Xmax":-74.45763999999998,
               "Ymin":6.83148000000007,
               "Ymax":7.32548000000007
            }
         },
         {
            "name":"SAN JOSÉ DE LA MONTAÑA",
            "value":"658",
            "text":"COLOMBIA, ANTIOQUIA, SAN JOSÉ DE LA MONTAÑA",
            "code":"CO-05-658",
            "geoPoint":{
               "lat":6.849480000000028,
               "lon":-75.68321999999995
            },
            "geoRectangle":{
               "Xmin":-75.75121999999995,
               "Xmax":-75.61521999999995,
               "Ymin":6.781480000000029,
               "Ymax":6.917480000000028
            }
         },
         {
            "name":"SAN JERÓNIMO",
            "value":"656",
            "text":"COLOMBIA, ANTIOQUIA, SAN JERÓNIMO",
            "code":"CO-05-656",
            "geoPoint":{
               "lat":6.442690000000027,
               "lon":-75.72767999999996
            },
            "geoRectangle":{
               "Xmin":-75.80467999999996,
               "Xmax":-75.65067999999997,
               "Ymin":6.365690000000027,
               "Ymax":6.519690000000027
            }
         },
         {
            "name":"SAN FRANCISCO",
            "value":"652",
            "text":"COLOMBIA, ANTIOQUIA, SAN FRANCISCO",
            "code":"CO-05-652",
            "geoPoint":{
               "lat":5.964970000000051,
               "lon":-75.10096999999996
            },
            "geoRectangle":{
               "Xmin":-75.23196999999996,
               "Xmax":-74.96996999999996,
               "Ymin":5.8339700000000505,
               "Ymax":6.095970000000051
            }
         },
         {
            "name":"SABANETA",
            "value":"631",
            "text":"COLOMBIA, ANTIOQUIA, SABANETA",
            "code":"CO-05-631",
            "geoPoint":{
               "lat":6.152600000000064,
               "lon":-75.61089999999996
            },
            "geoRectangle":{
               "Xmin":-75.63489999999996,
               "Xmax":-75.58689999999996,
               "Ymin":6.1286000000000636,
               "Ymax":6.176600000000064
            }
         },
         {
            "name":"SABANALARGA",
            "value":"628",
            "text":"COLOMBIA, ANTIOQUIA, SABANALARGA",
            "code":"CO-05-628",
            "geoPoint":{
               "lat":6.8496600000000285,
               "lon":-75.81735999999995
            },
            "geoRectangle":{
               "Xmin":-75.92235999999995,
               "Xmax":-75.71235999999995,
               "Ymin":6.744660000000028,
               "Ymax":6.954660000000029
            }
         },
         {
            "name":"RIONEGRO",
            "value":"615",
            "text":"COLOMBIA, ANTIOQUIA, RIONEGRO",
            "code":"CO-05-615",
            "geoPoint":{
               "lat":6.148960000000045,
               "lon":-75.37213999999994
            },
            "geoRectangle":{
               "Xmin":-75.46013999999994,
               "Xmax":-75.28413999999995,
               "Ymin":6.060960000000045,
               "Ymax":6.236960000000045
            }
         },
         {
            "name":"RETIRO",
            "value":"607",
            "text":"COLOMBIA, ANTIOQUIA, RETIRO",
            "code":"CO-05-607",
            "geoPoint":{
               "lat":6.083330000000046,
               "lon":-75.49999999999994
            },
            "geoRectangle":{
               "Xmin":-76.49999999999994,
               "Xmax":-74.49999999999994,
               "Ymin":5.083330000000046,
               "Ymax":7.083330000000046
            }
         },
         {
            "name":"REMEDIOS",
            "value":"604",
            "text":"COLOMBIA, ANTIOQUIA, REMEDIOS",
            "code":"CO-05-604",
            "geoPoint":{
               "lat":7.028120000000058,
               "lon":-74.69494999999995
            },
            "geoRectangle":{
               "Xmin":-74.99094999999996,
               "Xmax":-74.39894999999994,
               "Ymin":6.732120000000058,
               "Ymax":7.324120000000058
            }
         },
         {
            "name":"PUERTO TRIUNFO",
            "value":"591",
            "text":"COLOMBIA, ANTIOQUIA, PUERTO TRIUNFO",
            "code":"CO-05-591",
            "geoPoint":{
               "lat":5.872290000000021,
               "lon":-74.64079999999996
            },
            "geoRectangle":{
               "Xmin":-74.76179999999995,
               "Xmax":-74.51979999999996,
               "Ymin":5.75129000000002,
               "Ymax":5.993290000000021
            }
         },
         {
            "name":"PUERTO NARE",
            "value":"585",
            "text":"COLOMBIA, ANTIOQUIA, PUERTO NARE",
            "code":"CO-05-585",
            "geoPoint":{
               "lat":6.186150000000055,
               "lon":-74.58339999999998
            },
            "geoRectangle":{
               "Xmin":-74.75039999999998,
               "Xmax":-74.41639999999998,
               "Ymin":6.019150000000055,
               "Ymax":6.3531500000000545
            }
         },
         {
            "name":"PUERTO BERRÍO",
            "value":"579",
            "text":"COLOMBIA, ANTIOQUIA, PUERTO BERRÍO",
            "code":"CO-05-579",
            "geoPoint":{
               "lat":6.4858200000000465,
               "lon":-74.40461999999997
            },
            "geoRectangle":{
               "Xmin":-74.62761999999996,
               "Xmax":-74.18161999999997,
               "Ymin":6.262820000000047,
               "Ymax":6.708820000000046
            }
         },
         {
            "name":"PUEBLORRICO",
            "value":"576",
            "text":"COLOMBIA, ANTIOQUIA, PUEBLORRICO",
            "code":"CO-05-576",
            "geoPoint":{
               "lat":5.792010000000062,
               "lon":-75.84017999999998
            },
            "geoRectangle":{
               "Xmin":-75.90817999999997,
               "Xmax":-75.77217999999998,
               "Ymin":5.724010000000062,
               "Ymax":5.860010000000061
            }
         },
         {
            "name":"PEQUE",
            "value":"543",
            "text":"COLOMBIA, ANTIOQUIA, PEQUE",
            "code":"CO-05-543",
            "geoPoint":{
               "lat":7.021900000000073,
               "lon":-75.90935999999994
            },
            "geoRectangle":{
               "Xmin":-76.04835999999993,
               "Xmax":-75.77035999999994,
               "Ymin":6.882900000000073,
               "Ymax":7.1609000000000735
            }
         },
         {
            "name":"PEÑOL",
            "value":"541",
            "text":"COLOMBIA, ANTIOQUIA, PEÑOL",
            "code":"CO-05-541",
            "geoPoint":{
               "lat":6.220100000000059,
               "lon":-75.24433999999997
            },
            "geoRectangle":{
               "Xmin":-75.32233999999997,
               "Xmax":-75.16633999999996,
               "Ymin":6.142100000000059,
               "Ymax":6.298100000000059
            }
         },
         {
            "name":"OLAYA",
            "value":"501",
            "text":"COLOMBIA, ANTIOQUIA, OLAYA",
            "code":"CO-05-501",
            "geoPoint":{
               "lat":6.628090000000043,
               "lon":-75.81252999999998
            },
            "geoRectangle":{
               "Xmin":-75.87152999999998,
               "Xmax":-75.75352999999998,
               "Ymin":6.569090000000043,
               "Ymax":6.687090000000043
            }
         },
         {
            "name":"NECHÍ",
            "value":"495",
            "text":"COLOMBIA, ANTIOQUIA, NECHÍ",
            "code":"CO-05-495",
            "geoPoint":{
               "lat":8.09573000000006,
               "lon":-74.77514999999994
            },
            "geoRectangle":{
               "Xmin":-74.98114999999994,
               "Xmax":-74.56914999999994,
               "Ymin":7.88973000000006,
               "Ymax":8.30173000000006
            }
         },
         {
            "name":"NECOCLÍ",
            "value":"490",
            "text":"COLOMBIA, ANTIOQUIA, NECOCLÍ",
            "code":"CO-05-490",
            "geoPoint":{
               "lat":8.427410000000066,
               "lon":-76.78301999999996
            },
            "geoRectangle":{
               "Xmin":-77.02801999999997,
               "Xmax":-76.53801999999996,
               "Ymin":8.182410000000067,
               "Ymax":8.672410000000065
            }
         },
         {
            "name":"NARIÑO",
            "value":"483",
            "text":"COLOMBIA, ANTIOQUIA, NARIÑO",
            "code":"CO-05-483",
            "geoPoint":{
               "lat":5.610150000000033,
               "lon":-75.17625999999996
            },
            "geoRectangle":{
               "Xmin":-75.29825999999996,
               "Xmax":-75.05425999999996,
               "Ymin":5.488150000000033,
               "Ymax":5.732150000000033
            }
         },
         {
            "name":"MUTATÁ",
            "value":"480",
            "text":"COLOMBIA, ANTIOQUIA, MUTATÁ",
            "code":"CO-05-480",
            "geoPoint":{
               "lat":7.244800000000055,
               "lon":-76.43680999999998
            },
            "geoRectangle":{
               "Xmin":-76.67480999999998,
               "Xmax":-76.19880999999998,
               "Ymin":7.006800000000054,
               "Ymax":7.482800000000055
            }
         },
         {
            "name":"MONTEBELLO",
            "value":"467",
            "text":"COLOMBIA, ANTIOQUIA, MONTEBELLO",
            "code":"CO-05-467",
            "geoPoint":{
               "lat":5.945590000000038,
               "lon":-75.52379999999994
            },
            "geoRectangle":{
               "Xmin":-75.57779999999994,
               "Xmax":-75.46979999999994,
               "Ymin":5.891590000000038,
               "Ymax":5.999590000000039
            }
         },
         {
            "name":"MARINILLA",
            "value":"440",
            "text":"COLOMBIA, ANTIOQUIA, MARINILLA",
            "code":"CO-05-440",
            "geoPoint":{
               "lat":6.173880000000054,
               "lon":-75.33438999999998
            },
            "geoRectangle":{
               "Xmin":-75.40638999999999,
               "Xmax":-75.26238999999998,
               "Ymin":6.101880000000054,
               "Ymax":6.245880000000054
            }
         },
         {
            "name":"MACEO",
            "value":"425",
            "text":"COLOMBIA, ANTIOQUIA, MACEO",
            "code":"CO-05-425",
            "geoPoint":{
               "lat":6.551980000000071,
               "lon":-74.78776999999997
            },
            "geoRectangle":{
               "Xmin":-74.93076999999997,
               "Xmax":-74.64476999999997,
               "Ymin":6.408980000000072,
               "Ymax":6.694980000000071
            }
         },
         {
            "name":"LIBORINA",
            "value":"411",
            "text":"COLOMBIA, ANTIOQUIA, LIBORINA",
            "code":"CO-05-411",
            "geoPoint":{
               "lat":6.679020000000037,
               "lon":-75.81202999999994
            },
            "geoRectangle":{
               "Xmin":-75.89302999999994,
               "Xmax":-75.73102999999993,
               "Ymin":6.598020000000036,
               "Ymax":6.760020000000037
            }
         },
         {
            "name":"LA UNIÓN",
            "value":"400",
            "text":"COLOMBIA, ANTIOQUIA, LA UNIÓN",
            "code":"CO-05-400",
            "geoPoint":{
               "lat":5.973510000000033,
               "lon":-75.36163999999997
            },
            "geoRectangle":{
               "Xmin":-75.44763999999996,
               "Xmax":-75.27563999999997,
               "Ymin":5.887510000000033,
               "Ymax":6.059510000000033
            }
         },
         {
            "name":"LA PINTADA",
            "value":"390",
            "text":"COLOMBIA, ANTIOQUIA, LA PINTADA",
            "code":"CO-05-390",
            "geoPoint":{
               "lat":5.752100000000041,
               "lon":-75.60889999999995
            },
            "geoRectangle":{
               "Xmin":-75.64489999999995,
               "Xmax":-75.57289999999995,
               "Ymin":5.716100000000042,
               "Ymax":5.788100000000041
            }
         },
         {
            "name":"LA ESTRELLA",
            "value":"380",
            "text":"COLOMBIA, ANTIOQUIA, LA ESTRELLA",
            "code":"CO-05-380",
            "geoPoint":{
               "lat":6.157260000000065,
               "lon":-75.64350999999994
            },
            "geoRectangle":{
               "Xmin":-75.68150999999993,
               "Xmax":-75.60550999999994,
               "Ymin":6.1192600000000645,
               "Ymax":6.195260000000065
            }
         },
         {
            "name":"LA CEJA",
            "value":"376",
            "text":"COLOMBIA, ANTIOQUIA, LA CEJA",
            "code":"CO-05-376",
            "geoPoint":{
               "lat":6.029780000000073,
               "lon":-75.43017999999995
            },
            "geoRectangle":{
               "Xmin":-75.51017999999995,
               "Xmax":-75.35017999999995,
               "Ymin":5.949780000000073,
               "Ymax":6.1097800000000735
            }
         },
         {
            "name":"JERICÓ",
            "value":"368",
            "text":"COLOMBIA, ANTIOQUIA, JERICÓ",
            "code":"CO-05-368",
            "geoPoint":{
               "lat":5.7927400000000375,
               "lon":-75.78638999999998
            },
            "geoRectangle":{
               "Xmin":-75.88438999999998,
               "Xmax":-75.68838999999998,
               "Ymin":5.694740000000038,
               "Ymax":5.890740000000037
            }
         },
         {
            "name":"JARDÍN",
            "value":"364",
            "text":"COLOMBIA, ANTIOQUIA, JARDÍN",
            "code":"CO-05-364",
            "geoPoint":{
               "lat":5.598300000000052,
               "lon":-75.81859999999995
            },
            "geoRectangle":{
               "Xmin":-75.90559999999995,
               "Xmax":-75.73159999999994,
               "Ymin":5.511300000000052,
               "Ymax":5.685300000000051
            }
         },
         {
            "name":"ITAGÜÍ",
            "value":"360",
            "text":"COLOMBIA, ANTIOQUIA, ITAGÜÍ",
            "code":"CO-05-360",
            "geoPoint":{
               "lat":6.172880000000021,
               "lon":-75.61147999999997
            },
            "geoRectangle":{
               "Xmin":-75.64047999999997,
               "Xmax":-75.58247999999998,
               "Ymin":6.143880000000021,
               "Ymax":6.2018800000000205
            }
         },
         {
            "name":" HISPANIA",
            "value":"353",
            "text":"COLOMBIA, ANTIOQUIA,  HISPANIA",
            "code":"CO-05-353",
            "geoPoint":{
               "lat":5.799230000000023,
               "lon":-75.90737999999999
            },
            "geoRectangle":{
               "Xmin":-75.94937999999999,
               "Xmax":-75.86537999999999,
               "Ymin":5.757230000000023,
               "Ymax":5.841230000000023
            }
         },
         {
            "name":"HELICONIA",
            "value":"347",
            "text":"COLOMBIA, ANTIOQUIA, HELICONIA",
            "code":"CO-05-347",
            "geoPoint":{
               "lat":6.207840000000033,
               "lon":-75.73404999999997
            },
            "geoRectangle":{
               "Xmin":-75.79604999999997,
               "Xmax":-75.67204999999997,
               "Ymin":6.145840000000033,
               "Ymax":6.269840000000033
            }
         },
         {
            "name":"GUATAPÉ",
            "value":"321",
            "text":"COLOMBIA, ANTIOQUIA, GUATAPÉ",
            "code":"CO-05-321",
            "geoPoint":{
               "lat":6.2337100000000305,
               "lon":-75.16007999999994
            },
            "geoRectangle":{
               "Xmin":-75.22207999999993,
               "Xmax":-75.09807999999994,
               "Ymin":6.17171000000003,
               "Ymax":6.295710000000031
            }
         },
         {
            "name":"GUARNE",
            "value":"318",
            "text":"COLOMBIA, ANTIOQUIA, GUARNE",
            "code":"CO-05-318",
            "geoPoint":{
               "lat":6.279320000000041,
               "lon":-75.44708999999995
            },
            "geoRectangle":{
               "Xmin":-75.52108999999994,
               "Xmax":-75.37308999999995,
               "Ymin":6.205320000000041,
               "Ymax":6.353320000000041
            }
         },
         {
            "name":"GUADALUPE",
            "value":"315",
            "text":"COLOMBIA, ANTIOQUIA, GUADALUPE",
            "code":"CO-05-315",
            "geoPoint":{
               "lat":6.814360000000022,
               "lon":-75.24023999999997
            },
            "geoRectangle":{
               "Xmin":-75.30923999999997,
               "Xmax":-75.17123999999997,
               "Ymin":6.745360000000022,
               "Ymax":6.883360000000022
            }
         },
         {
            "name":"GRANADA",
            "value":"313",
            "text":"COLOMBIA, ANTIOQUIA, GRANADA",
            "code":"CO-05-313",
            "geoPoint":{
               "lat":6.141720000000021,
               "lon":-75.18342999999999
            },
            "geoRectangle":{
               "Xmin":-75.27942999999999,
               "Xmax":-75.08742999999998,
               "Ymin":6.045720000000021,
               "Ymax":6.237720000000021
            }
         },
         {
            "name":"GÓMEZ PLATA",
            "value":"310",
            "text":"COLOMBIA, ANTIOQUIA, GÓMEZ PLATA",
            "code":"CO-05-310",
            "geoPoint":{
               "lat":6.68249000000003,
               "lon":-75.21939999999995
            },
            "geoRectangle":{
               "Xmin":-75.33439999999995,
               "Xmax":-75.10439999999996,
               "Ymin":6.56749000000003,
               "Ymax":6.79749000000003
            }
         },
         {
            "name":"GIRARDOTA",
            "value":"308",
            "text":"COLOMBIA, ANTIOQUIA, GIRARDOTA",
            "code":"CO-05-308",
            "geoPoint":{
               "lat":6.377350000000035,
               "lon":-75.44397999999995
            },
            "geoRectangle":{
               "Xmin":-75.49697999999995,
               "Xmax":-75.39097999999996,
               "Ymin":6.324350000000035,
               "Ymax":6.430350000000035
            }
         },
         {
            "name":"DABEIBA",
            "value":"234",
            "text":"COLOMBIA, ANTIOQUIA, DABEIBA",
            "code":"CO-05-234",
            "geoPoint":{
               "lat":7.001910000000066,
               "lon":-76.26467999999994
            },
            "geoRectangle":{
               "Xmin":-76.55267999999994,
               "Xmax":-75.97667999999994,
               "Ymin":6.713910000000066,
               "Ymax":7.2899100000000665
            }
         },
         {
            "name":"FRONTINO",
            "value":"284",
            "text":"COLOMBIA, ANTIOQUIA, FRONTINO",
            "code":"CO-05-284",
            "geoPoint":{
               "lat":6.7818600000000515,
               "lon":-76.12917999999996
            },
            "geoRectangle":{
               "Xmin":-76.34817999999996,
               "Xmax":-75.91017999999997,
               "Ymin":6.562860000000051,
               "Ymax":7.000860000000052
            }
         },
         {
            "name":"ITUANGO",
            "value":"361",
            "text":"COLOMBIA, ANTIOQUIA, ITUANGO",
            "code":"CO-05-361",
            "geoPoint":{
               "lat":7.171140000000037,
               "lon":-75.76352999999995
            },
            "geoRectangle":{
               "Xmin":-76.09052999999994,
               "Xmax":-75.43652999999995,
               "Ymin":6.844140000000037,
               "Ymax":7.498140000000037
            }
         },
         {
            "name":"MURINDÓ",
            "value":"475",
            "text":"COLOMBIA, ANTIOQUIA, MURINDÓ",
            "code":"CO-05-475",
            "geoPoint":{
               "lat":6.981180000000052,
               "lon":-76.82077999999996
            },
            "geoRectangle":{
               "Xmin":-77.01677999999995,
               "Xmax":-76.62477999999996,
               "Ymin":6.785180000000052,
               "Ymax":7.177180000000051
            }
         },
         {
            "name":"VIGIA DEL FUERTE",
            "value":"873",
            "text":"COLOMBIA, ANTIOQUIA, VIGIA DEL FUERTE",
            "code":"CO-05-873",
            "geoPoint":{
               "lat":6.588580000000036,
               "lon":-76.89590999999996
            },
            "geoRectangle":{
               "Xmin":-77.19190999999996,
               "Xmax":-76.59990999999995,
               "Ymin":6.292580000000036,
               "Ymax":6.884580000000036
            }
         }
      ]
   },
   {
      "name":"ARAUCA",
      "value":"81",
      "text":"COLOMBIA, ARAUCA",
      "code":"CO-81",
      "geoPoint":{
         "lat":7.086930000000052,
         "lon":-70.75868999999994
      },
      "geoRectangle":{
         "Xmin":-71.27668999999995,
         "Xmax":-70.24068999999994,
         "Ymin":6.568930000000052,
         "Ymax":7.604930000000052
      },
      "municipality":[
         {
            "name":"FORTUL",
            "value":"300",
            "text":"COLOMBIA, ARAUCA, FORTUL",
            "code":"CO-81-300",
            "geoPoint":{
               "lat":6.794210000000021,
               "lon":-71.77292999999997
            },
            "geoRectangle":{
               "Xmin":-72.02792999999997,
               "Xmax":-71.51792999999998,
               "Ymin":6.539210000000021,
               "Ymax":7.049210000000021
            }
         },
         {
            "name":"ARAUCA",
            "value":"001",
            "text":"COLOMBIA, ARAUCA, ARAUCA",
            "code":"CO-81-001",
            "geoPoint":{
               "lat":7.086930000000052,
               "lon":-70.75868999999994
            },
            "geoRectangle":{
               "Xmin":-71.27668999999995,
               "Xmax":-70.24068999999994,
               "Ymin":6.568930000000052,
               "Ymax":7.604930000000052
            }
         },
         {
            "name":"PUERTO RONDÓN",
            "value":"591",
            "text":"COLOMBIA, ARAUCA, PUERTO RONDÓN",
            "code":"CO-81-591",
            "geoPoint":{
               "lat":6.279480000000035,
               "lon":-71.09941999999995
            },
            "geoRectangle":{
               "Xmin":-71.37441999999996,
               "Xmax":-70.82441999999995,
               "Ymin":6.004480000000035,
               "Ymax":6.554480000000035
            }
         },
         {
            "name":"CRAVO NORTE",
            "value":"220",
            "text":"COLOMBIA, ARAUCA, CRAVO NORTE",
            "code":"CO-81-220",
            "geoPoint":{
               "lat":6.30126000000007,
               "lon":-70.20459999999997
            },
            "geoRectangle":{
               "Xmin":-70.70759999999997,
               "Xmax":-69.70159999999997,
               "Ymin":5.79826000000007,
               "Ymax":6.80426000000007
            }
         },
         {
            "name":"ARAUQUITA",
            "value":"065",
            "text":"COLOMBIA, ARAUCA, ARAUQUITA",
            "code":"CO-81-065",
            "geoPoint":{
               "lat":7.028980000000047,
               "lon":-71.42591999999996
            },
            "geoRectangle":{
               "Xmin":-71.76691999999996,
               "Xmax":-71.08491999999997,
               "Ymin":6.687980000000047,
               "Ymax":7.369980000000047
            }
         },
         {
            "name":"SARAVENA",
            "value":"736",
            "text":"COLOMBIA, ARAUCA, SARAVENA",
            "code":"CO-81-736",
            "geoPoint":{
               "lat":6.953360000000032,
               "lon":-71.87351999999998
            },
            "geoRectangle":{
               "Xmin":-72.05351999999999,
               "Xmax":-71.69351999999998,
               "Ymin":6.773360000000032,
               "Ymax":7.133360000000032
            }
         },
         {
            "name":"TAME",
            "value":"794",
            "text":"COLOMBIA, ARAUCA, TAME",
            "code":"CO-81-794",
            "geoPoint":{
               "lat":6.46392000000003,
               "lon":-71.72769999999997
            },
            "geoRectangle":{
               "Xmin":-72.17369999999997,
               "Xmax":-71.28169999999997,
               "Ymin":6.01792000000003,
               "Ymax":6.90992000000003
            }
         }
      ]
   },
   {
      "name":"ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA",
      "value":"88",
      "text":"COLOMBIA, ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA",
      "code":"CO-88",
      "geoPoint":{
         "lat":13.385140000000035,
         "lon":-81.37346999999994
      },
      "geoRectangle":{
         "Xmin":-91.37346999999994,
         "Xmax":-71.37346999999994,
         "Ymin":3.3851400000000353,
         "Ymax":23.385140000000035
      },
      "municipality":[
         {
            "name":"SAN ANDRÉS",
            "value":"001",
            "text":"COLOMBIA, ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA, SAN ANDRÉS",
            "code":"CO-88-001",
            "geoPoint":{
               "lat":12.550000000000068,
               "lon":-81.69999999999999
            },
            "geoRectangle":{
               "Xmin":-82.69999999999999,
               "Xmax":-80.69999999999999,
               "Ymin":11.550000000000068,
               "Ymax":13.550000000000068
            }
         },
         {
            "name":"PROVIDENCIA (Santa Isabel)",
            "value":"564",
            "text":"COLOMBIA, ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA, PROVIDENCIA (Santa Isabel)",
            "code":"CO-88-564",
            "geoPoint":{
               "lat":13.350000000000023,
               "lon":-81.36666999999994
            },
            "geoRectangle":{
               "Xmin":-82.36666999999994,
               "Xmax":-80.36666999999994,
               "Ymin":12.350000000000023,
               "Ymax":14.350000000000023
            }
         }
      ]
   },
   {
      "name":"ATLÁNTICO",
      "value":"08",
      "text":"COLOMBIA, ATLÁNTICO",
      "code":"CO-08",
      "geoPoint":{
         "lat":10.682666163000022,
         "lon":-74.96381509799994
      },
      "geoRectangle":{
         "Xmin":-75.30681509799994,
         "Xmax":-74.62081509799994,
         "Ymin":10.339666163000022,
         "Ymax":11.025666163000022
      },
      "municipality":[
         {
            "name":"DISTRITO ESPECIAL, INDUSTRIAL Y PORTUARIO DE BARRANQUILLA",
            "value":"001",
            "text":"COLOMBIA, ATLÁNTICO, DISTRITO ESPECIAL, INDUSTRIAL Y PORTUARIO DE BARRANQUILLA",
            "code":"CO-08-001",
            "geoPoint":{
               "lat":10.953210036248528,
               "lon":-74.76530491596336
            },
            "geoRectangle":{
               "Xmin":-74.76630491596336,
               "Xmax":-74.76430491596335,
               "Ymin":10.952210036248529,
               "Ymax":10.954210036248528
            }
         },
         {
            "name":"BARANOA",
            "value":"078",
            "text":"COLOMBIA, ATLÁNTICO, BARANOA",
            "code":"CO-08-078",
            "geoPoint":{
               "lat":10.793610000000058,
               "lon":-74.91542999999996
            },
            "geoRectangle":{
               "Xmin":-74.98642999999996,
               "Xmax":-74.84442999999996,
               "Ymin":10.722610000000058,
               "Ymax":10.864610000000058
            }
         },
         {
            "name":" CAMPO DE LA CRUZ",
            "value":"137",
            "text":"COLOMBIA, ATLÁNTICO,  CAMPO DE LA CRUZ",
            "code":"CO-08-137",
            "geoPoint":{
               "lat":10.379750000000058,
               "lon":-74.88051999999993
            },
            "geoRectangle":{
               "Xmin":-74.95851999999994,
               "Xmax":-74.80251999999993,
               "Ymin":10.301750000000059,
               "Ymax":10.457750000000058
            }
         },
         {
            "name":"CANDELARIA",
            "value":"141",
            "text":"COLOMBIA, ATLÁNTICO, CANDELARIA",
            "code":"CO-08-141",
            "geoPoint":{
               "lat":10.460150000000056,
               "lon":-74.87923999999998
            },
            "geoRectangle":{
               "Xmin":-74.94723999999998,
               "Xmax":-74.81123999999998,
               "Ymin":10.392150000000056,
               "Ymax":10.528150000000055
            }
         },
         {
            "name":"GALAPA",
            "value":"296",
            "text":"COLOMBIA, ATLÁNTICO, GALAPA",
            "code":"CO-08-296",
            "geoPoint":{
               "lat":10.900160000000028,
               "lon":-74.88298999999995
            },
            "geoRectangle":{
               "Xmin":-74.93998999999995,
               "Xmax":-74.82598999999995,
               "Ymin":10.843160000000028,
               "Ymax":10.957160000000028
            }
         },
         {
            "name":"JUAN DE ACOSTA",
            "value":"372",
            "text":"COLOMBIA, ATLÁNTICO, JUAN DE ACOSTA",
            "code":"CO-08-372",
            "geoPoint":{
               "lat":10.829630000000066,
               "lon":-75.03403999999995
            },
            "geoRectangle":{
               "Xmin":-75.12403999999995,
               "Xmax":-74.94403999999994,
               "Ymin":10.739630000000066,
               "Ymax":10.919630000000065
            }
         },
         {
            "name":"LURUACO",
            "value":"421",
            "text":"COLOMBIA, ATLÁNTICO, LURUACO",
            "code":"CO-08-421",
            "geoPoint":{
               "lat":10.61053000000004,
               "lon":-75.14359999999994
            },
            "geoRectangle":{
               "Xmin":-75.24759999999993,
               "Xmax":-75.03959999999994,
               "Ymin":10.50653000000004,
               "Ymax":10.714530000000039
            }
         },
         {
            "name":"MALAMBO",
            "value":"433",
            "text":"COLOMBIA, ATLÁNTICO, MALAMBO",
            "code":"CO-08-433",
            "geoPoint":{
               "lat":10.86358000000007,
               "lon":-74.77626999999995
            },
            "geoRectangle":{
               "Xmin":-74.83526999999995,
               "Xmax":-74.71726999999996,
               "Ymin":10.80458000000007,
               "Ymax":10.92258000000007
            }
         },
         {
            "name":"MANATÍ",
            "value":"436",
            "text":"COLOMBIA, ATLÁNTICO, MANATÍ",
            "code":"CO-08-436",
            "geoPoint":{
               "lat":10.445520000000045,
               "lon":-74.95929999999998
            },
            "geoRectangle":{
               "Xmin":-75.05329999999998,
               "Xmax":-74.86529999999999,
               "Ymin":10.351520000000045,
               "Ymax":10.539520000000044
            }
         },
         {
            "name":"PIOJÓ",
            "value":"549",
            "text":"COLOMBIA, ATLÁNTICO, PIOJÓ",
            "code":"CO-08-549",
            "geoPoint":{
               "lat":10.748070000000041,
               "lon":-75.10838999999999
            },
            "geoRectangle":{
               "Xmin":-75.19838999999999,
               "Xmax":-75.01838999999998,
               "Ymin":10.658070000000041,
               "Ymax":10.838070000000041
            }
         },
         {
            "name":"POLONUEVO",
            "value":"558",
            "text":"COLOMBIA, ATLÁNTICO, POLONUEVO",
            "code":"CO-08-558",
            "geoPoint":{
               "lat":10.777640000000076,
               "lon":-74.85336999999998
            },
            "geoRectangle":{
               "Xmin":-74.90236999999999,
               "Xmax":-74.80436999999998,
               "Ymin":10.728640000000077,
               "Ymax":10.826640000000076
            }
         },
         {
            "name":"PONEDERA",
            "value":"560",
            "text":"COLOMBIA, ATLÁNTICO, PONEDERA",
            "code":"CO-08-560",
            "geoPoint":{
               "lat":10.642000000000053,
               "lon":-74.75083999999998
            },
            "geoRectangle":{
               "Xmin":-74.84783999999998,
               "Xmax":-74.65383999999999,
               "Ymin":10.545000000000053,
               "Ymax":10.739000000000052
            }
         },
         {
            "name":"PUERTO COLOMBIA",
            "value":"573",
            "text":"COLOMBIA, ATLÁNTICO, PUERTO COLOMBIA",
            "code":"CO-08-573",
            "geoPoint":{
               "lat":10.994360000000029,
               "lon":-74.94421999999997
            },
            "geoRectangle":{
               "Xmin":-75.00521999999998,
               "Xmax":-74.88321999999997,
               "Ymin":10.933360000000029,
               "Ymax":11.055360000000029
            }
         },
         {
            "name":"REPELÓN",
            "value":"606",
            "text":"COLOMBIA, ATLÁNTICO, REPELÓN",
            "code":"CO-08-606",
            "geoPoint":{
               "lat":10.494450000000029,
               "lon":-75.12798999999995
            },
            "geoRectangle":{
               "Xmin":-75.23398999999995,
               "Xmax":-75.02198999999996,
               "Ymin":10.388450000000029,
               "Ymax":10.600450000000029
            }
         },
         {
            "name":"PALMAR DE VARELA",
            "value":"520",
            "text":"COLOMBIA, ATLÁNTICO, PALMAR DE VARELA",
            "code":"CO-08-520",
            "geoPoint":{
               "lat":10.740480000000048,
               "lon":-74.75419999999997
            },
            "geoRectangle":{
               "Xmin":-74.80919999999998,
               "Xmax":-74.69919999999996,
               "Ymin":10.685480000000048,
               "Ymax":10.795480000000047
            }
         },
         {
            "name":"SABANAGRANDE",
            "value":"634",
            "text":"COLOMBIA, ATLÁNTICO, SABANAGRANDE",
            "code":"CO-08-634",
            "geoPoint":{
               "lat":10.788080000000036,
               "lon":-74.75774999999999
            },
            "geoRectangle":{
               "Xmin":-74.79475,
               "Xmax":-74.72074999999998,
               "Ymin":10.751080000000035,
               "Ymax":10.825080000000037
            }
         },
         {
            "name":"SABANALARGA",
            "value":"638",
            "text":"COLOMBIA, ATLÁNTICO, SABANALARGA",
            "code":"CO-08-638",
            "geoPoint":{
               "lat":10.630950000000041,
               "lon":-74.92104999999998
            },
            "geoRectangle":{
               "Xmin":-75.02904999999998,
               "Xmax":-74.81304999999998,
               "Ymin":10.52295000000004,
               "Ymax":10.738950000000042
            }
         },
         {
            "name":"SANTA LUCÍA",
            "value":"675",
            "text":"COLOMBIA, ATLÁNTICO, SANTA LUCÍA",
            "code":"CO-08-675",
            "geoPoint":{
               "lat":10.326290000000029,
               "lon":-74.96201999999994
            },
            "geoRectangle":{
               "Xmin":-75.01601999999994,
               "Xmax":-74.90801999999994,
               "Ymin":10.272290000000028,
               "Ymax":10.380290000000029
            }
         },
         {
            "name":"SANTO TOMÁS",
            "value":"685",
            "text":"COLOMBIA, ATLÁNTICO, SANTO TOMÁS",
            "code":"CO-08-685",
            "geoPoint":{
               "lat":10.757160000000056,
               "lon":-74.75451999999996
            },
            "geoRectangle":{
               "Xmin":-74.80951999999996,
               "Xmax":-74.69951999999995,
               "Ymin":10.702160000000056,
               "Ymax":10.812160000000056
            }
         },
         {
            "name":"SOLEDAD",
            "value":"758",
            "text":"COLOMBIA, ATLÁNTICO, SOLEDAD",
            "code":"CO-08-758",
            "geoPoint":{
               "lat":10.906700000000058,
               "lon":-74.79406999999998
            },
            "geoRectangle":{
               "Xmin":-74.84106999999997,
               "Xmax":-74.74706999999998,
               "Ymin":10.859700000000057,
               "Ymax":10.953700000000058
            }
         },
         {
            "name":"SUAN",
            "value":"770",
            "text":"COLOMBIA, ATLÁNTICO, SUAN",
            "code":"CO-08-770",
            "geoPoint":{
               "lat":10.334580000000074,
               "lon":-74.87998999999996
            },
            "geoRectangle":{
               "Xmin":-74.92298999999997,
               "Xmax":-74.83698999999996,
               "Ymin":10.291580000000074,
               "Ymax":10.377580000000073
            }
         },
         {
            "name":"TUBARÁ",
            "value":"832",
            "text":"COLOMBIA, ATLÁNTICO, TUBARÁ",
            "code":"CO-08-832",
            "geoPoint":{
               "lat":10.875710000000026,
               "lon":-74.97838999999993
            },
            "geoRectangle":{
               "Xmin":-75.05838999999993,
               "Xmax":-74.89838999999994,
               "Ymin":10.795710000000026,
               "Ymax":10.955710000000026
            }
         },
         {
            "name":" USIACURÍ",
            "value":"849",
            "text":"COLOMBIA, ATLÁNTICO,  USIACURÍ",
            "code":"CO-08-849",
            "geoPoint":{
               "lat":10.743630000000053,
               "lon":-74.97679999999997
            },
            "geoRectangle":{
               "Xmin":-75.03479999999998,
               "Xmax":-74.91879999999996,
               "Ymin":10.685630000000053,
               "Ymax":10.801630000000053
            }
         }
      ]
   },
   {
      "name":"BOGOTÁ, D.C.",
      "value":"11",
      "text":"COLOMBIA, BOGOTÁ, D.C.",
      "code":"CO-11",
      "geoPoint":{
         "lat":4.614960000000053,
         "lon":-74.06940999999995
      },
      "geoRectangle":{
         "Xmin":-74.46040999999995,
         "Xmax":-73.67840999999994,
         "Ymin":4.223960000000053,
         "Ymax":5.005960000000053
      },
      "municipality":[
         {
            "name":"BOGOTÁ, D.C.",
            "value":"001",
            "text":"COLOMBIA, BOGOTÁ, D.C., BOGOTÁ, D.C.",
            "code":"CO-11-001",
            "geoPoint":{
               "lat":4.614960000000053,
               "lon":-74.06940999999995
            },
            "geoRectangle":{
               "Xmin":-74.46040999999995,
               "Xmax":-73.67840999999994,
               "Ymin":4.223960000000053,
               "Ymax":5.005960000000053
            }
         }
      ]
   },
   {
      "name":"BOLÍVAR",
      "value":"13",
      "text":"COLOMBIA, BOLÍVAR",
      "code":"CO-13",
      "geoPoint":{
         "lat":1.8346400000000358,
         "lon":-76.96653999999995
      },
      "geoRectangle":{
         "Xmin":-77.15153999999995,
         "Xmax":-76.78153999999995,
         "Ymin":1.6496400000000357,
         "Ymax":2.019640000000036
      },
      "municipality":[
         {
            "name":"CANTAGALLO",
            "value":"160",
            "text":"COLOMBIA, BOLÍVAR, CANTAGALLO",
            "code":"CO-13-160",
            "geoPoint":{
               "lat":7.378300000000024,
               "lon":-73.91498999999999
            },
            "geoRectangle":{
               "Xmin":-74.11998999999999,
               "Xmax":-73.70998999999999,
               "Ymin":7.173300000000024,
               "Ymax":7.583300000000024
            }
         },
         {
            "name":"CICUCO",
            "value":"188",
            "text":"COLOMBIA, BOLÍVAR, CICUCO",
            "code":"CO-13-188",
            "geoPoint":{
               "lat":9.27633000000003,
               "lon":-74.64298999999994
            },
            "geoRectangle":{
               "Xmin":-74.71298999999993,
               "Xmax":-74.57298999999995,
               "Ymin":9.20633000000003,
               "Ymax":9.34633000000003
            }
         },
         {
            "name":"CÓRDOBA",
            "value":"212",
            "text":"COLOMBIA, BOLÍVAR, CÓRDOBA",
            "code":"CO-13-212",
            "geoPoint":{
               "lat":9.587530000000072,
               "lon":-74.82699999999994
            },
            "geoRectangle":{
               "Xmin":-74.96799999999995,
               "Xmax":-74.68599999999994,
               "Ymin":9.446530000000072,
               "Ymax":9.728530000000072
            }
         },
         {
            "name":"CLEMENCIA",
            "value":"222",
            "text":"COLOMBIA, BOLÍVAR, CLEMENCIA",
            "code":"CO-13-222",
            "geoPoint":{
               "lat":10.567450000000065,
               "lon":-75.32686999999999
            },
            "geoRectangle":{
               "Xmin":-75.38986999999999,
               "Xmax":-75.26386999999998,
               "Ymin":10.504450000000064,
               "Ymax":10.630450000000065
            }
         },
         {
            "name":"EL CARMEN DE BOLÍVAR",
            "value":"244",
            "text":"COLOMBIA, BOLÍVAR, EL CARMEN DE BOLÍVAR",
            "code":"CO-13-244",
            "geoPoint":{
               "lat":9.717040000000054,
               "lon":-75.12079999999997
            },
            "geoRectangle":{
               "Xmin":-75.31579999999997,
               "Xmax":-74.92579999999998,
               "Ymin":9.522040000000054,
               "Ymax":9.912040000000054
            }
         },
         {
            "name":"EL GUAMO",
            "value":"248",
            "text":"COLOMBIA, BOLÍVAR, EL GUAMO",
            "code":"CO-13-248",
            "geoPoint":{
               "lat":10.032530000000065,
               "lon":-74.97573999999997
            },
            "geoRectangle":{
               "Xmin":-75.08973999999998,
               "Xmax":-74.86173999999997,
               "Ymin":9.918530000000064,
               "Ymax":10.146530000000066
            }
         },
         {
            "name":"EL PEÑON",
            "value":"268",
            "text":"COLOMBIA, BOLÍVAR, EL PEÑON",
            "code":"CO-13-268",
            "geoPoint":{
               "lat":8.989210000000071,
               "lon":-73.95003999999994
            },
            "geoRectangle":{
               "Xmin":-74.06303999999994,
               "Xmax":-73.83703999999994,
               "Ymin":8.876210000000071,
               "Ymax":9.10221000000007
            }
         },
         {
            "name":"MAGANGUÉ",
            "value":"430",
            "text":"COLOMBIA, BOLÍVAR, MAGANGUÉ",
            "code":"CO-13-430",
            "geoPoint":{
               "lat":9.245360000000062,
               "lon":-74.75815999999998
            },
            "geoRectangle":{
               "Xmin":-75.04215999999998,
               "Xmax":-74.47415999999997,
               "Ymin":8.961360000000061,
               "Ymax":9.529360000000063
            }
         },
         {
            "name":"MAHATES",
            "value":"433",
            "text":"COLOMBIA, BOLÍVAR, MAHATES",
            "code":"CO-13-433",
            "geoPoint":{
               "lat":10.235590000000059,
               "lon":-75.19529999999997
            },
            "geoRectangle":{
               "Xmin":-75.31829999999998,
               "Xmax":-75.07229999999997,
               "Ymin":10.11259000000006,
               "Ymax":10.358590000000058
            }
         },
         {
            "name":"MARGARITA",
            "value":"440",
            "text":"COLOMBIA, BOLÍVAR, MARGARITA",
            "code":"CO-13-440",
            "geoPoint":{
               "lat":9.153520000000071,
               "lon":-74.28641999999996
            },
            "geoRectangle":{
               "Xmin":-74.40141999999996,
               "Xmax":-74.17141999999997,
               "Ymin":9.038520000000071,
               "Ymax":9.268520000000072
            }
         },
         {
            "name":"MARIA LA BAJA",
            "value":"442",
            "text":"COLOMBIA, BOLÍVAR, MARIA LA BAJA",
            "code":"CO-13-442",
            "geoPoint":{
               "lat":9.982860000000073,
               "lon":-75.30102999999997
            },
            "geoRectangle":{
               "Xmin":-75.42702999999997,
               "Xmax":-75.17502999999996,
               "Ymin":9.856860000000074,
               "Ymax":10.108860000000073
            }
         },
         {
            "name":"MONTECRISTO",
            "value":"458",
            "text":"COLOMBIA, BOLÍVAR, MONTECRISTO",
            "code":"CO-13-458",
            "geoPoint":{
               "lat":8.295950000000062,
               "lon":-74.47268999999994
            },
            "geoRectangle":{
               "Xmin":-74.82968999999994,
               "Xmax":-74.11568999999994,
               "Ymin":7.9389500000000615,
               "Ymax":8.652950000000061
            }
         },
         {
            "name":"MOMPÓS",
            "value":"468",
            "text":"COLOMBIA, BOLÍVAR, MOMPÓS",
            "code":"CO-13-468",
            "geoPoint":{
               "lat":9.235580000000027,
               "lon":-74.42604999999998
            },
            "geoRectangle":{
               "Xmin":-74.58504999999998,
               "Xmax":-74.26704999999997,
               "Ymin":9.076580000000027,
               "Ymax":9.394580000000028
            }
         },
         {
            "name":"MORALES",
            "value":"473",
            "text":"COLOMBIA, BOLÍVAR, MORALES",
            "code":"CO-13-473",
            "geoPoint":{
               "lat":8.275540000000035,
               "lon":-73.86820999999998
            },
            "geoRectangle":{
               "Xmin":-74.10220999999997,
               "Xmax":-73.63420999999998,
               "Ymin":8.041540000000035,
               "Ymax":8.509540000000035
            }
         },
         {
            "name":" NOROSI",
            "value":"490",
            "text":"COLOMBIA, BOLÍVAR,  NOROSI",
            "code":"CO-13-490",
            "geoPoint":{
               "lat":8.528100000000052,
               "lon":-74.03779999999995
            },
            "geoRectangle":{
               "Xmin":-74.04779999999995,
               "Xmax":-74.02779999999994,
               "Ymin":8.518100000000052,
               "Ymax":8.538100000000052
            }
         },
         {
            "name":"PINILLOS",
            "value":"549",
            "text":"COLOMBIA, BOLÍVAR, PINILLOS",
            "code":"CO-13-549",
            "geoPoint":{
               "lat":8.915050000000065,
               "lon":-74.46239999999995
            },
            "geoRectangle":{
               "Xmin":-74.66739999999994,
               "Xmax":-74.25739999999995,
               "Ymin":8.710050000000065,
               "Ymax":9.120050000000065
            }
         },
         {
            "name":"REGIDOR",
            "value":"580",
            "text":"COLOMBIA, BOLÍVAR, REGIDOR",
            "code":"CO-13-580",
            "geoPoint":{
               "lat":8.666080000000022,
               "lon":-73.82173999999998
            },
            "geoRectangle":{
               "Xmin":-73.92173999999997,
               "Xmax":-73.72173999999998,
               "Ymin":8.566080000000023,
               "Ymax":8.766080000000022
            }
         },
         {
            "name":"RIOVIEJO",
            "value":"600",
            "text":"COLOMBIA, BOLÍVAR, RIOVIEJO",
            "code":"CO-13-600",
            "geoPoint":{
               "lat":7.032420000000059,
               "lon":-71.28007999999994
            },
            "geoRectangle":{
               "Xmin":-71.29007999999995,
               "Xmax":-71.27007999999994,
               "Ymin":7.022420000000059,
               "Ymax":7.0424200000000585
            }
         },
         {
            "name":"HATILLO DE LOBA",
            "value":"300",
            "text":"COLOMBIA, BOLÍVAR, HATILLO DE LOBA",
            "code":"CO-13-300",
            "geoPoint":{
               "lat":8.95742000000007,
               "lon":-74.07886999999994
            },
            "geoRectangle":{
               "Xmin":-74.21686999999994,
               "Xmax":-73.94086999999993,
               "Ymin":8.81942000000007,
               "Ymax":9.09542000000007
            }
         },
         {
            "name":"TURBACO",
            "value":"836",
            "text":"COLOMBIA, BOLÍVAR, TURBACO",
            "code":"CO-13-836",
            "geoPoint":{
               "lat":10.333260000000053,
               "lon":-75.41256999999996
            },
            "geoRectangle":{
               "Xmin":-75.49356999999996,
               "Xmax":-75.33156999999996,
               "Ymin":10.252260000000053,
               "Ymax":10.414260000000052
            }
         },
         {
            "name":"TURBANA",
            "value":"838",
            "text":"COLOMBIA, BOLÍVAR, TURBANA",
            "code":"CO-13-838",
            "geoPoint":{
               "lat":10.273420000000044,
               "lon":-75.44299999999998
            },
            "geoRectangle":{
               "Xmin":-75.52099999999999,
               "Xmax":-75.36499999999998,
               "Ymin":10.195420000000045,
               "Ymax":10.351420000000044
            }
         },
         {
            "name":"VILLANUEVA",
            "value":"873",
            "text":"COLOMBIA, BOLÍVAR, VILLANUEVA",
            "code":"CO-13-873",
            "geoPoint":{
               "lat":10.444720000000075,
               "lon":-75.27256999999997
            },
            "geoRectangle":{
               "Xmin":-75.35456999999997,
               "Xmax":-75.19056999999998,
               "Ymin":10.362720000000074,
               "Ymax":10.526720000000076
            }
         },
         {
            "name":"ZAMBRANO",
            "value":"894",
            "text":"COLOMBIA, BOLÍVAR, ZAMBRANO",
            "code":"CO-13-894",
            "geoPoint":{
               "lat":9.74777000000006,
               "lon":-74.81567999999999
            },
            "geoRectangle":{
               "Xmin":-74.91567999999998,
               "Xmax":-74.71567999999999,
               "Ymin":9.64777000000006,
               "Ymax":9.84777000000006
            }
         },
         {
            "name":"CARTAGENA DE INDIAS",
            "value":"001",
            "text":"COLOMBIA, BOLÍVAR, CARTAGENA DE INDIAS",
            "code":"CO-13-001",
            "geoPoint":{
               "lat":10.425050000000056,
               "lon":-75.53836999999999
            },
            "geoRectangle":{
               "Xmin":-75.91636999999999,
               "Xmax":-75.16036999999999,
               "Ymin":10.047050000000056,
               "Ymax":10.803050000000056
            }
         },
         {
            "name":"ACHÍ",
            "value":"006",
            "text":"COLOMBIA, BOLÍVAR, ACHÍ",
            "code":"CO-13-006",
            "geoPoint":{
               "lat":8.569150000000036,
               "lon":-74.55595999999997
            },
            "geoRectangle":{
               "Xmin":-74.73595999999998,
               "Xmax":-74.37595999999996,
               "Ymin":8.389150000000036,
               "Ymax":8.749150000000036
            }
         },
         {
            "name":"ALTOS DEL ROSARIO",
            "value":"030",
            "text":"COLOMBIA, BOLÍVAR, ALTOS DEL ROSARIO",
            "code":"CO-13-030",
            "geoPoint":{
               "lat":8.791370000000029,
               "lon":-74.16276999999997
            },
            "geoRectangle":{
               "Xmin":-74.27176999999996,
               "Xmax":-74.05376999999997,
               "Ymin":8.682370000000029,
               "Ymax":8.900370000000029
            }
         },
         {
            "name":"ARENAL",
            "value":"042",
            "text":"COLOMBIA, BOLÍVAR, ARENAL",
            "code":"CO-13-042",
            "geoPoint":{
               "lat":8.458950000000073,
               "lon":-73.94201999999996
            },
            "geoRectangle":{
               "Xmin":-74.14101999999995,
               "Xmax":-73.74301999999996,
               "Ymin":8.259950000000073,
               "Ymax":8.657950000000072
            }
         },
         {
            "name":"ARJONA",
            "value":"052",
            "text":"COLOMBIA, BOLÍVAR, ARJONA",
            "code":"CO-13-052",
            "geoPoint":{
               "lat":10.261490000000038,
               "lon":-75.34635999999995
            },
            "geoRectangle":{
               "Xmin":-75.50435999999995,
               "Xmax":-75.18835999999995,
               "Ymin":10.103490000000038,
               "Ymax":10.419490000000037
            }
         },
         {
            "name":"ARROYOHONDO",
            "value":"062",
            "text":"COLOMBIA, BOLÍVAR, ARROYOHONDO",
            "code":"CO-13-062",
            "geoPoint":{
               "lat":10.250800000000027,
               "lon":-75.01898999999997
            },
            "geoRectangle":{
               "Xmin":-75.10098999999997,
               "Xmax":-74.93698999999998,
               "Ymin":10.168800000000026,
               "Ymax":10.332800000000027
            }
         },
         {
            "name":"BARRANCO DE LOBA",
            "value":"074",
            "text":"COLOMBIA, BOLÍVAR, BARRANCO DE LOBA",
            "code":"CO-13-074",
            "geoPoint":{
               "lat":8.94569000000007,
               "lon":-74.10608999999994
            },
            "geoRectangle":{
               "Xmin":-74.26208999999994,
               "Xmax":-73.95008999999993,
               "Ymin":8.78969000000007,
               "Ymax":9.10169000000007
            }
         },
         {
            "name":"CALAMAR",
            "value":"140",
            "text":"COLOMBIA, BOLÍVAR, CALAMAR",
            "code":"CO-13-140",
            "geoPoint":{
               "lat":10.254000000000076,
               "lon":-74.91445999999996
            },
            "geoRectangle":{
               "Xmin":-75.02745999999996,
               "Xmax":-74.80145999999996,
               "Ymin":10.141000000000076,
               "Ymax":10.367000000000075
            }
         },
         {
            "name":"SAN CRISTÓBAL",
            "value":"620",
            "text":"COLOMBIA, BOLÍVAR, SAN CRISTÓBAL",
            "code":"CO-13-620",
            "geoPoint":{
               "lat":10.394550000000038,
               "lon":-75.06569999999994
            },
            "geoRectangle":{
               "Xmin":-75.10669999999993,
               "Xmax":-75.02469999999994,
               "Ymin":10.353550000000038,
               "Ymax":10.435550000000038
            }
         },
         {
            "name":"SAN ESTANISLAO",
            "value":"647",
            "text":"COLOMBIA, BOLÍVAR, SAN ESTANISLAO",
            "code":"CO-13-647",
            "geoPoint":{
               "lat":10.407190000000071,
               "lon":-75.18784999999997
            },
            "geoRectangle":{
               "Xmin":-75.27784999999997,
               "Xmax":-75.09784999999997,
               "Ymin":10.317190000000071,
               "Ymax":10.49719000000007
            }
         },
         {
            "name":"SAN FERNANDO",
            "value":"650",
            "text":"COLOMBIA, BOLÍVAR, SAN FERNANDO",
            "code":"CO-13-650",
            "geoPoint":{
               "lat":9.209510000000023,
               "lon":-74.31490999999994
            },
            "geoRectangle":{
               "Xmin":-74.43190999999995,
               "Xmax":-74.19790999999994,
               "Ymin":9.092510000000022,
               "Ymax":9.326510000000024
            }
         },
         {
            "name":"SAN JACINTO",
            "value":"654",
            "text":"COLOMBIA, BOLÍVAR, SAN JACINTO",
            "code":"CO-13-654",
            "geoPoint":{
               "lat":9.829720000000066,
               "lon":-75.12076999999994
            },
            "geoRectangle":{
               "Xmin":-75.25976999999993,
               "Xmax":-74.98176999999994,
               "Ymin":9.690720000000066,
               "Ymax":9.968720000000065
            }
         },
         {
            "name":"SAN JACINTO DEL CAUCA",
            "value":"655",
            "text":"COLOMBIA, BOLÍVAR, SAN JACINTO DEL CAUCA",
            "code":"CO-13-655",
            "geoPoint":{
               "lat":8.25076000000007,
               "lon":-74.72049999999996
            },
            "geoRectangle":{
               "Xmin":-74.90249999999996,
               "Xmax":-74.53849999999996,
               "Ymin":8.06876000000007,
               "Ymax":8.432760000000071
            }
         },
         {
            "name":"SAN JUAN NEPOMUCENO",
            "value":"657",
            "text":"COLOMBIA, BOLÍVAR, SAN JUAN NEPOMUCENO",
            "code":"CO-13-657",
            "geoPoint":{
               "lat":9.953580000000045,
               "lon":-75.08366999999998
            },
            "geoRectangle":{
               "Xmin":-75.25066999999999,
               "Xmax":-74.91666999999998,
               "Ymin":9.786580000000045,
               "Ymax":10.120580000000045
            }
         },
         {
            "name":" SAN MARTÍN DE LOBA",
            "value":"667",
            "text":"COLOMBIA, BOLÍVAR,  SAN MARTÍN DE LOBA",
            "code":"CO-13-667",
            "geoPoint":{
               "lat":8.939630000000022,
               "lon":-74.03771999999998
            },
            "geoRectangle":{
               "Xmin":-74.18571999999998,
               "Xmax":-73.88971999999998,
               "Ymin":8.791630000000023,
               "Ymax":9.087630000000022
            }
         },
         {
            "name":"SAN PABLO",
            "value":"670",
            "text":"COLOMBIA, BOLÍVAR, SAN PABLO",
            "code":"CO-13-670",
            "geoPoint":{
               "lat":7.4787600000000225,
               "lon":-73.92156999999997
            },
            "geoRectangle":{
               "Xmin":-74.25056999999997,
               "Xmax":-73.59256999999998,
               "Ymin":7.149760000000023,
               "Ymax":7.807760000000022
            }
         },
         {
            "name":"SANTA CATALINA",
            "value":"673",
            "text":"COLOMBIA, BOLÍVAR, SANTA CATALINA",
            "code":"CO-13-673",
            "geoPoint":{
               "lat":10.605330000000038,
               "lon":-75.28843999999998
            },
            "geoRectangle":{
               "Xmin":-75.39443999999997,
               "Xmax":-75.18243999999999,
               "Ymin":10.499330000000038,
               "Ymax":10.711330000000038
            }
         },
         {
            "name":"SANTA ROSA",
            "value":"683",
            "text":"COLOMBIA, BOLÍVAR, SANTA ROSA",
            "code":"CO-13-683",
            "geoPoint":{
               "lat":10.445440000000076,
               "lon":-75.36947999999995
            },
            "geoRectangle":{
               "Xmin":-75.44147999999996,
               "Xmax":-75.29747999999995,
               "Ymin":10.373440000000077,
               "Ymax":10.517440000000075
            }
         },
         {
            "name":"SANTA ROSA DEL SUR",
            "value":"688",
            "text":"COLOMBIA, BOLÍVAR, SANTA ROSA DEL SUR",
            "code":"CO-13-688",
            "geoPoint":{
               "lat":7.962440000000072,
               "lon":-74.05157999999994
            },
            "geoRectangle":{
               "Xmin":-74.40857999999994,
               "Xmax":-73.69457999999995,
               "Ymin":7.605440000000072,
               "Ymax":8.319440000000071
            }
         },
         {
            "name":"SIMITÍ",
            "value":"744",
            "text":"COLOMBIA, BOLÍVAR, SIMITÍ",
            "code":"CO-13-744",
            "geoPoint":{
               "lat":7.957380000000057,
               "lon":-73.94551999999999
            },
            "geoRectangle":{
               "Xmin":-74.20551999999999,
               "Xmax":-73.68551999999998,
               "Ymin":7.697380000000058,
               "Ymax":8.217380000000057
            }
         },
         {
            "name":"SOPLAVIENTO",
            "value":"760",
            "text":"COLOMBIA, BOLÍVAR, SOPLAVIENTO",
            "code":"CO-13-760",
            "geoPoint":{
               "lat":10.391200000000026,
               "lon":-75.13645999999994
            },
            "geoRectangle":{
               "Xmin":-75.20045999999994,
               "Xmax":-75.07245999999995,
               "Ymin":10.327200000000026,
               "Ymax":10.455200000000026
            }
         },
         {
            "name":"TALAIGUA NUEVO",
            "value":"780",
            "text":"COLOMBIA, BOLÍVAR, TALAIGUA NUEVO",
            "code":"CO-13-780",
            "geoPoint":{
               "lat":9.303480000000036,
               "lon":-74.56691999999998
            },
            "geoRectangle":{
               "Xmin":-74.68191999999998,
               "Xmax":-74.45191999999999,
               "Ymin":9.188480000000036,
               "Ymax":9.418480000000036
            }
         },
         {
            "name":"TIQUISO",
            "value":"810",
            "text":"COLOMBIA, BOLÍVAR, TIQUISO",
            "code":"CO-13-810",
            "geoPoint":{
               "lat":1.8346400000000358,
               "lon":-76.96653999999995
            },
            "geoRectangle":{
               "Xmin":-77.15153999999995,
               "Xmax":-76.78153999999995,
               "Ymin":1.6496400000000357,
               "Ymax":2.019640000000036
            }
         }
      ]
   },
   {
      "name":"BOYACÁ",
      "value":"15",
      "text":"COLOMBIA, BOYACÁ",
      "code":"CO-15",
      "geoPoint":{
         "lat":5.773693625000021,
         "lon":-73.10336594199998
      },
      "geoRectangle":{
         "Xmin":-74.37936594199998,
         "Xmax":-71.82736594199999,
         "Ymin":4.4976936250000215,
         "Ymax":7.049693625000021
      },
      "municipality":[
         {
            "name":"LABRANZA GRANDE",
            "value":"377",
            "text":"COLOMBIA, BOYACÁ, LABRANZA GRANDE",
            "code":"CO-15-377",
            "geoPoint":{
               "lat":5.563120000000026,
               "lon":-72.57742999999994
            },
            "geoRectangle":{
               "Xmin":-72.72942999999994,
               "Xmax":-72.42542999999993,
               "Ymin":5.411120000000026,
               "Ymax":5.715120000000026
            }
         },
         {
            "name":"TUNJA",
            "value":"001",
            "text":"COLOMBIA, BOYACÁ, TUNJA",
            "code":"CO-15-001",
            "geoPoint":{
               "lat":5.534420000000068,
               "lon":-73.36149999999998
            },
            "geoRectangle":{
               "Xmin":-73.43349999999998,
               "Xmax":-73.28949999999998,
               "Ymin":5.462420000000068,
               "Ymax":5.606420000000068
            }
         },
         {
            "name":"ALMEIDA",
            "value":"022",
            "text":"COLOMBIA, BOYACÁ, ALMEIDA",
            "code":"CO-15-022",
            "geoPoint":{
               "lat":4.970750000000066,
               "lon":-73.37888999999996
            },
            "geoRectangle":{
               "Xmin":-73.42888999999995,
               "Xmax":-73.32888999999996,
               "Ymin":4.920750000000067,
               "Ymax":5.020750000000066
            }
         },
         {
            "name":"AQUITANIA",
            "value":"047",
            "text":"COLOMBIA, BOYACÁ, AQUITANIA",
            "code":"CO-15-047",
            "geoPoint":{
               "lat":5.518420000000049,
               "lon":-72.88377999999994
            },
            "geoRectangle":{
               "Xmin":-73.08677999999995,
               "Xmax":-72.68077999999994,
               "Ymin":5.315420000000048,
               "Ymax":5.721420000000049
            }
         },
         {
            "name":"ARCABUCO",
            "value":"051",
            "text":"COLOMBIA, BOYACÁ, ARCABUCO",
            "code":"CO-15-051",
            "geoPoint":{
               "lat":5.754760000000033,
               "lon":-73.43688999999995
            },
            "geoRectangle":{
               "Xmin":-73.52888999999995,
               "Xmax":-73.34488999999995,
               "Ymin":5.662760000000033,
               "Ymax":5.846760000000033
            }
         },
         {
            "name":" BELÉN",
            "value":"087",
            "text":"COLOMBIA, BOYACÁ,  BELÉN",
            "code":"CO-15-087",
            "geoPoint":{
               "lat":5.990830000000074,
               "lon":-72.90977999999996
            },
            "geoRectangle":{
               "Xmin":-72.99777999999995,
               "Xmax":-72.82177999999996,
               "Ymin":5.9028300000000735,
               "Ymax":6.078830000000074
            }
         },
         {
            "name":"BERBEO",
            "value":"090",
            "text":"COLOMBIA, BOYACÁ, BERBEO",
            "code":"CO-15-090",
            "geoPoint":{
               "lat":5.22643000000005,
               "lon":-73.12658999999996
            },
            "geoRectangle":{
               "Xmin":-73.18458999999997,
               "Xmax":-73.06858999999996,
               "Ymin":5.1684300000000505,
               "Ymax":5.28443000000005
            }
         },
         {
            "name":"BETÉITIVA",
            "value":"092",
            "text":"COLOMBIA, BOYACÁ, BETÉITIVA",
            "code":"CO-15-092",
            "geoPoint":{
               "lat":5.910830000000033,
               "lon":-72.80875999999995
            },
            "geoRectangle":{
               "Xmin":-72.87375999999995,
               "Xmax":-72.74375999999995,
               "Ymin":5.845830000000032,
               "Ymax":5.975830000000033
            }
         },
         {
            "name":"BOAVITA",
            "value":"097",
            "text":"COLOMBIA, BOYACÁ, BOAVITA",
            "code":"CO-15-097",
            "geoPoint":{
               "lat":6.330500000000029,
               "lon":-72.58525999999995
            },
            "geoRectangle":{
               "Xmin":-72.66625999999995,
               "Xmax":-72.50425999999995,
               "Ymin":6.249500000000029,
               "Ymax":6.4115000000000295
            }
         },
         {
            "name":"BOYACÁ",
            "value":"104",
            "text":"COLOMBIA, BOYACÁ, BOYACÁ",
            "code":"CO-15-104",
            "geoPoint":{
               "lat":5.773693625000021,
               "lon":-73.10336594199998
            },
            "geoRectangle":{
               "Xmin":-74.37936594199998,
               "Xmax":-71.82736594199999,
               "Ymin":4.4976936250000215,
               "Ymax":7.049693625000021
            }
         },
         {
            "name":"BRICEÑO",
            "value":"106",
            "text":"COLOMBIA, BOYACÁ, BRICEÑO",
            "code":"CO-15-106",
            "geoPoint":{
               "lat":5.690590000000043,
               "lon":-73.92355999999995
            },
            "geoRectangle":{
               "Xmin":-73.96555999999995,
               "Xmax":-73.88155999999995,
               "Ymin":5.648590000000043,
               "Ymax":5.732590000000043
            }
         },
         {
            "name":"BUENAVISTA",
            "value":"109",
            "text":"COLOMBIA, BOYACÁ, BUENAVISTA",
            "code":"CO-15-109",
            "geoPoint":{
               "lat":5.511790000000076,
               "lon":-73.94186999999994
            },
            "geoRectangle":{
               "Xmin":-74.00086999999994,
               "Xmax":-73.88286999999994,
               "Ymin":5.452790000000076,
               "Ymax":5.570790000000076
            }
         },
         {
            "name":"BUSBANZÁ",
            "value":"114",
            "text":"COLOMBIA, BOYACÁ, BUSBANZÁ",
            "code":"CO-15-114",
            "geoPoint":{
               "lat":5.916670000000067,
               "lon":-72.84999999999997
            },
            "geoRectangle":{
               "Xmin":-73.84999999999997,
               "Xmax":-71.84999999999997,
               "Ymin":4.916670000000067,
               "Ymax":6.916670000000067
            }
         },
         {
            "name":"CALDAS",
            "value":"131",
            "text":"COLOMBIA, BOYACÁ, CALDAS",
            "code":"CO-15-131",
            "geoPoint":{
               "lat":5.55391000000003,
               "lon":-73.86528999999996
            },
            "geoRectangle":{
               "Xmin":-73.91428999999997,
               "Xmax":-73.81628999999995,
               "Ymin":5.50491000000003,
               "Ymax":5.602910000000031
            }
         },
         {
            "name":"CAMPOHERMOSO",
            "value":"135",
            "text":"COLOMBIA, BOYACÁ, CAMPOHERMOSO",
            "code":"CO-15-135",
            "geoPoint":{
               "lat":5.030750000000069,
               "lon":-73.10373999999996
            },
            "geoRectangle":{
               "Xmin":-73.21473999999996,
               "Xmax":-72.99273999999996,
               "Ymin":4.919750000000069,
               "Ymax":5.141750000000068
            }
         },
         {
            "name":"LA VICTORIA",
            "value":"401",
            "text":"COLOMBIA, BOYACÁ, LA VICTORIA",
            "code":"CO-15-401",
            "geoPoint":{
               "lat":5.523400000000038,
               "lon":-74.23432999999994
            },
            "geoRectangle":{
               "Xmin":-74.26932999999994,
               "Xmax":-74.19932999999995,
               "Ymin":5.488400000000038,
               "Ymax":5.558400000000038
            }
         },
         {
            "name":"LA UVITA",
            "value":"403",
            "text":"COLOMBIA, BOYACÁ, LA UVITA",
            "code":"CO-15-403",
            "geoPoint":{
               "lat":6.317070000000058,
               "lon":-72.55996999999996
            },
            "geoRectangle":{
               "Xmin":-72.64196999999996,
               "Xmax":-72.47796999999997,
               "Ymin":6.235070000000058,
               "Ymax":6.399070000000058
            }
         },
         {
            "name":"CERINZA",
            "value":"162",
            "text":"COLOMBIA, BOYACÁ, CERINZA",
            "code":"CO-15-162",
            "geoPoint":{
               "lat":5.955750000000023,
               "lon":-72.94801999999999
            },
            "geoRectangle":{
               "Xmin":-72.99601999999999,
               "Xmax":-72.90001999999998,
               "Ymin":5.907750000000023,
               "Ymax":6.003750000000023
            }
         },
         {
            "name":"CHINAVITA",
            "value":"172",
            "text":"COLOMBIA, BOYACÁ, CHINAVITA",
            "code":"CO-15-172",
            "geoPoint":{
               "lat":5.167490000000043,
               "lon":-73.36786999999998
            },
            "geoRectangle":{
               "Xmin":-73.43786999999998,
               "Xmax":-73.29786999999999,
               "Ymin":5.097490000000043,
               "Ymax":5.237490000000044
            }
         },
         {
            "name":"CHIQUINQUIRÁ",
            "value":"176",
            "text":"COLOMBIA, BOYACÁ, CHIQUINQUIRÁ",
            "code":"CO-15-176",
            "geoPoint":{
               "lat":5.616590000000031,
               "lon":-73.81649999999996
            },
            "geoRectangle":{
               "Xmin":-73.90849999999996,
               "Xmax":-73.72449999999996,
               "Ymin":5.524590000000031,
               "Ymax":5.70859000000003
            }
         },
         {
            "name":"CHITA",
            "value":"183",
            "text":"COLOMBIA, BOYACÁ, CHITA",
            "code":"CO-15-183",
            "geoPoint":{
               "lat":6.186810000000037,
               "lon":-72.47294999999997
            },
            "geoRectangle":{
               "Xmin":-72.65294999999998,
               "Xmax":-72.29294999999996,
               "Ymin":6.006810000000037,
               "Ymax":6.3668100000000365
            }
         },
         {
            "name":"PESCA",
            "value":"542",
            "text":"COLOMBIA, BOYACÁ, PESCA",
            "code":"CO-15-542",
            "geoPoint":{
               "lat":5.559100000000058,
               "lon":-73.05064999999996
            },
            "geoRectangle":{
               "Xmin":-73.16664999999996,
               "Xmax":-72.93464999999996,
               "Ymin":5.443100000000058,
               "Ymax":5.675100000000057
            }
         },
         {
            "name":"PISBA",
            "value":"550",
            "text":"COLOMBIA, BOYACÁ, PISBA",
            "code":"CO-15-550",
            "geoPoint":{
               "lat":5.727400000000046,
               "lon":-72.48644999999993
            },
            "geoRectangle":{
               "Xmin":-72.59544999999993,
               "Xmax":-72.37744999999994,
               "Ymin":5.618400000000046,
               "Ymax":5.8364000000000456
            }
         },
         {
            "name":"PUERTO BOYACÁ",
            "value":"572",
            "text":"COLOMBIA, BOYACÁ, PUERTO BOYACÁ",
            "code":"CO-15-572",
            "geoPoint":{
               "lat":5.977080000000058,
               "lon":-74.58899999999994
            },
            "geoRectangle":{
               "Xmin":-74.83299999999994,
               "Xmax":-74.34499999999994,
               "Ymin":5.733080000000058,
               "Ymax":6.2210800000000575
            }
         },
         {
            "name":"QUÍPAMA",
            "value":"580",
            "text":"COLOMBIA, BOYACÁ, QUÍPAMA",
            "code":"CO-15-580",
            "geoPoint":{
               "lat":5.519390000000044,
               "lon":-74.17872999999997
            },
            "geoRectangle":{
               "Xmin":-74.27072999999997,
               "Xmax":-74.08672999999997,
               "Ymin":5.427390000000044,
               "Ymax":5.611390000000044
            }
         },
         {
            "name":"SOCOTÁ",
            "value":"755",
            "text":"COLOMBIA, BOYACÁ, SOCOTÁ",
            "code":"CO-15-755",
            "geoPoint":{
               "lat":6.0418900000000235,
               "lon":-72.63652999999994
            },
            "geoRectangle":{
               "Xmin":-72.79752999999994,
               "Xmax":-72.47552999999994,
               "Ymin":5.880890000000024,
               "Ymax":6.202890000000023
            }
         },
         {
            "name":"SOCHA",
            "value":"757",
            "text":"COLOMBIA, BOYACÁ, SOCHA",
            "code":"CO-15-757",
            "geoPoint":{
               "lat":5.997720000000072,
               "lon":-72.69158999999996
            },
            "geoRectangle":{
               "Xmin":-72.76858999999996,
               "Xmax":-72.61458999999996,
               "Ymin":5.920720000000072,
               "Ymax":6.074720000000072
            }
         },
         {
            "name":"SOGAMOSO",
            "value":"759",
            "text":"COLOMBIA, BOYACÁ, SOGAMOSO",
            "code":"CO-15-759",
            "geoPoint":{
               "lat":5.714180000000056,
               "lon":-72.92856999999998
            },
            "geoRectangle":{
               "Xmin":-73.02456999999998,
               "Xmax":-72.83256999999998,
               "Ymin":5.618180000000056,
               "Ymax":5.810180000000056
            }
         },
         {
            "name":" SOMONDOCO",
            "value":"761",
            "text":"COLOMBIA, BOYACÁ,  SOMONDOCO",
            "code":"CO-15-761",
            "geoPoint":{
               "lat":4.985300000000052,
               "lon":-73.43231999999995
            },
            "geoRectangle":{
               "Xmin":-73.48431999999995,
               "Xmax":-73.38031999999994,
               "Ymin":4.933300000000052,
               "Ymax":5.037300000000052
            }
         },
         {
            "name":"CHISCAS",
            "value":"180",
            "text":"COLOMBIA, BOYACÁ, CHISCAS",
            "code":"CO-15-180",
            "geoPoint":{
               "lat":6.553260000000023,
               "lon":-72.50020999999998
            },
            "geoRectangle":{
               "Xmin":-72.65020999999999,
               "Xmax":-72.35020999999998,
               "Ymin":6.403260000000023,
               "Ymax":6.703260000000023
            }
         },
         {
            "name":"CUBARÁ",
            "value":"223",
            "text":"COLOMBIA, BOYACÁ, CUBARÁ",
            "code":"CO-15-223",
            "geoPoint":{
               "lat":7.00111000000004,
               "lon":-72.10885999999994
            },
            "geoRectangle":{
               "Xmin":-72.33585999999994,
               "Xmax":-71.88185999999993,
               "Ymin":6.774110000000039,
               "Ymax":7.22811000000004
            }
         },
         {
            "name":"GÜICÁN",
            "value":"332",
            "text":"COLOMBIA, BOYACÁ, GÜICÁN",
            "code":"CO-15-332",
            "geoPoint":{
               "lat":6.46276000000006,
               "lon":-72.41221999999993
            },
            "geoRectangle":{
               "Xmin":-72.59721999999994,
               "Xmax":-72.22721999999993,
               "Ymin":6.27776000000006,
               "Ymax":6.647760000000059
            }
         },
         {
            "name":"CHITARAQUE",
            "value":"185",
            "text":"COLOMBIA, BOYACÁ, CHITARAQUE",
            "code":"CO-15-185",
            "geoPoint":{
               "lat":6.0277600000000575,
               "lon":-73.44757999999996
            },
            "geoRectangle":{
               "Xmin":-73.52857999999996,
               "Xmax":-73.36657999999996,
               "Ymin":5.946760000000057,
               "Ymax":6.108760000000058
            }
         },
         {
            "name":"CHIVATÁ",
            "value":"187",
            "text":"COLOMBIA, BOYACÁ, CHIVATÁ",
            "code":"CO-15-187",
            "geoPoint":{
               "lat":5.558350000000075,
               "lon":-73.28287999999998
            },
            "geoRectangle":{
               "Xmin":-73.33787999999998,
               "Xmax":-73.22787999999997,
               "Ymin":5.503350000000076,
               "Ymax":5.613350000000075
            }
         },
         {
            "name":"CIÉNEGA",
            "value":"189",
            "text":"COLOMBIA, BOYACÁ, CIÉNEGA",
            "code":"CO-15-189",
            "geoPoint":{
               "lat":5.408770000000061,
               "lon":-73.29616999999996
            },
            "geoRectangle":{
               "Xmin":-73.34916999999996,
               "Xmax":-73.24316999999996,
               "Ymin":5.355770000000061,
               "Ymax":5.461770000000061
            }
         },
         {
            "name":"COMBITA",
            "value":"204",
            "text":"COLOMBIA, BOYACÁ, COMBITA",
            "code":"CO-15-204",
            "geoPoint":{
               "lat":5.633330000000058,
               "lon":-73.31666999999999
            },
            "geoRectangle":{
               "Xmin":-73.33166999999999,
               "Xmax":-73.30166999999999,
               "Ymin":5.618330000000058,
               "Ymax":5.648330000000057
            }
         },
         {
            "name":"COPER",
            "value":"212",
            "text":"COLOMBIA, BOYACÁ, COPER",
            "code":"CO-15-212",
            "geoPoint":{
               "lat":5.474540000000047,
               "lon":-74.04472999999996
            },
            "geoRectangle":{
               "Xmin":-74.12472999999996,
               "Xmax":-73.96472999999996,
               "Ymin":5.394540000000047,
               "Ymax":5.554540000000047
            }
         },
         {
            "name":"CORRALES",
            "value":"215",
            "text":"COLOMBIA, BOYACÁ, CORRALES",
            "code":"CO-15-215",
            "geoPoint":{
               "lat":5.829420000000027,
               "lon":-72.84462999999994
            },
            "geoRectangle":{
               "Xmin":-72.89762999999994,
               "Xmax":-72.79162999999994,
               "Ymin":5.776420000000027,
               "Ymax":5.882420000000027
            }
         },
         {
            "name":"LA CAPILLA",
            "value":"380",
            "text":"COLOMBIA, BOYACÁ, LA CAPILLA",
            "code":"CO-15-380",
            "geoPoint":{
               "lat":5.096380000000067,
               "lon":-73.44454999999994
            },
            "geoRectangle":{
               "Xmin":-73.48754999999994,
               "Xmax":-73.40154999999993,
               "Ymin":5.053380000000067,
               "Ymax":5.139380000000068
            }
         },
         {
            "name":" CUÌTIVA",
            "value":"226",
            "text":"COLOMBIA, BOYACÁ,  CUÌTIVA",
            "code":"CO-15-226",
            "geoPoint":{
               "lat":5.5806400000000735,
               "lon":-72.96627999999998
            },
            "geoRectangle":{
               "Xmin":-73.00827999999998,
               "Xmax":-72.92427999999998,
               "Ymin":5.538640000000074,
               "Ymax":5.622640000000073
            }
         },
         {
            "name":"CHÍQUIZA",
            "value":"232",
            "text":"COLOMBIA, BOYACÁ, CHÍQUIZA",
            "code":"CO-15-232",
            "geoPoint":{
               "lat":5.6395500000000425,
               "lon":-73.44976999999994
            },
            "geoRectangle":{
               "Xmin":-73.52176999999995,
               "Xmax":-73.37776999999994,
               "Ymin":5.567550000000042,
               "Ymax":5.7115500000000425
            }
         },
         {
            "name":"UMBITA",
            "value":"842",
            "text":"COLOMBIA, BOYACÁ, UMBITA",
            "code":"CO-15-842",
            "geoPoint":{
               "lat":5.221180000000061,
               "lon":-73.45725999999996
            },
            "geoRectangle":{
               "Xmin":-73.52925999999997,
               "Xmax":-73.38525999999996,
               "Ymin":5.149180000000061,
               "Ymax":5.293180000000061
            }
         },
         {
            "name":"VENTAQUEMADA",
            "value":"861",
            "text":"COLOMBIA, BOYACÁ, VENTAQUEMADA",
            "code":"CO-15-861",
            "geoPoint":{
               "lat":5.3665600000000495,
               "lon":-73.52166999999997
            },
            "geoRectangle":{
               "Xmin":-73.61066999999997,
               "Xmax":-73.43266999999997,
               "Ymin":5.277560000000049,
               "Ymax":5.45556000000005
            }
         },
         {
            "name":"VIRACACHÁ",
            "value":"879",
            "text":"COLOMBIA, BOYACÁ, VIRACACHÁ",
            "code":"CO-15-879",
            "geoPoint":{
               "lat":5.4353700000000345,
               "lon":-73.29621999999995
            },
            "geoRectangle":{
               "Xmin":-73.34321999999995,
               "Xmax":-73.24921999999995,
               "Ymin":5.388370000000035,
               "Ymax":5.482370000000034
            }
         },
         {
            "name":"ZETAQUIRÁ",
            "value":"897",
            "text":"COLOMBIA, BOYACÁ, ZETAQUIRÁ",
            "code":"CO-15-897",
            "geoPoint":{
               "lat":5.350000000000023,
               "lon":-73.16666999999995
            },
            "geoRectangle":{
               "Xmin":-74.16666999999995,
               "Xmax":-72.16666999999995,
               "Ymin":4.350000000000023,
               "Ymax":6.350000000000023
            }
         },
         {
            "name":"MARIPÍ",
            "value":"442",
            "text":"COLOMBIA, BOYACÁ, MARIPÍ",
            "code":"CO-15-442",
            "geoPoint":{
               "lat":5.549620000000061,
               "lon":-74.00571999999994
            },
            "geoRectangle":{
               "Xmin":-74.08771999999993,
               "Xmax":-73.92371999999995,
               "Ymin":5.467620000000061,
               "Ymax":5.631620000000061
            }
         },
         {
            "name":"MIRAFLORES",
            "value":"455",
            "text":"COLOMBIA, BOYACÁ, MIRAFLORES",
            "code":"CO-15-455",
            "geoPoint":{
               "lat":5.1959000000000515,
               "lon":-73.14518999999996
            },
            "geoRectangle":{
               "Xmin":-73.23018999999995,
               "Xmax":-73.06018999999996,
               "Ymin":5.1109000000000515,
               "Ymax":5.280900000000051
            }
         },
         {
            "name":"MONGUA",
            "value":"464",
            "text":"COLOMBIA, BOYACÁ, MONGUA",
            "code":"CO-15-464",
            "geoPoint":{
               "lat":5.7548700000000395,
               "lon":-72.79900999999995
            },
            "geoRectangle":{
               "Xmin":-72.91400999999995,
               "Xmax":-72.68400999999996,
               "Ymin":5.639870000000039,
               "Ymax":5.86987000000004
            }
         },
         {
            "name":"COVARACHIA",
            "value":"218",
            "text":"COLOMBIA, BOYACÁ, COVARACHIA",
            "code":"CO-15-218",
            "geoPoint":{
               "lat":6.532395042726954,
               "lon":-72.719819924198
            },
            "geoRectangle":{
               "Xmin":-72.72081992419801,
               "Xmax":-72.718819924198,
               "Ymin":6.531395042726953,
               "Ymax":6.533395042726954
            }
         },
         {
            "name":"MONIQUIRA",
            "value":"469",
            "text":"COLOMBIA, BOYACÁ, MONIQUIRA",
            "code":"CO-15-469",
            "geoPoint":{
               "lat":5.64247000000006,
               "lon":-73.56592999999998
            },
            "geoRectangle":{
               "Xmin":-73.57592999999999,
               "Xmax":-73.55592999999998,
               "Ymin":5.63247000000006,
               "Ymax":5.65247000000006
            }
         },
         {
            "name":"MOTAVITA",
            "value":"476",
            "text":"COLOMBIA, BOYACÁ, MOTAVITA",
            "code":"CO-15-476",
            "geoPoint":{
               "lat":5.5767000000000735,
               "lon":-73.36815999999999
            },
            "geoRectangle":{
               "Xmin":-73.41015999999999,
               "Xmax":-73.32615999999999,
               "Ymin":5.534700000000074,
               "Ymax":5.618700000000073
            }
         },
         {
            "name":"MUZO",
            "value":"480",
            "text":"COLOMBIA, BOYACÁ, MUZO",
            "code":"CO-15-480",
            "geoPoint":{
               "lat":5.533980000000042,
               "lon":-74.10313999999994
            },
            "geoRectangle":{
               "Xmin":-74.17913999999993,
               "Xmax":-74.02713999999995,
               "Ymin":5.457980000000043,
               "Ymax":5.609980000000042
            }
         },
         {
            "name":"NOBSA",
            "value":"491",
            "text":"COLOMBIA, BOYACÁ, NOBSA",
            "code":"CO-15-491",
            "geoPoint":{
               "lat":5.769860000000051,
               "lon":-72.93908999999996
            },
            "geoRectangle":{
               "Xmin":-72.98308999999996,
               "Xmax":-72.89508999999997,
               "Ymin":5.7258600000000515,
               "Ymax":5.813860000000051
            }
         },
         {
            "name":"CUCAITA",
            "value":"224",
            "text":"COLOMBIA, BOYACÁ, CUCAITA",
            "code":"CO-15-224",
            "geoPoint":{
               "lat":5.542430000000024,
               "lon":-73.45295999999996
            },
            "geoRectangle":{
               "Xmin":-73.49295999999997,
               "Xmax":-73.41295999999996,
               "Ymin":5.502430000000024,
               "Ymax":5.582430000000024
            }
         },
         {
            "name":"CHIVOR",
            "value":"236",
            "text":"COLOMBIA, BOYACÁ, CHIVOR",
            "code":"CO-15-236",
            "geoPoint":{
               "lat":4.8889200000000415,
               "lon":-73.36895999999996
            },
            "geoRectangle":{
               "Xmin":-73.44095999999996,
               "Xmax":-73.29695999999996,
               "Ymin":4.816920000000041,
               "Ymax":4.9609200000000415
            }
         },
         {
            "name":"DUITAMA",
            "value":"238",
            "text":"COLOMBIA, BOYACÁ, DUITAMA",
            "code":"CO-15-238",
            "geoPoint":{
               "lat":5.837600000000066,
               "lon":-73.02242999999999
            },
            "geoRectangle":{
               "Xmin":-73.14342999999998,
               "Xmax":-72.90142999999999,
               "Ymin":5.716600000000065,
               "Ymax":5.958600000000066
            }
         },
         {
            "name":"EL COCUY",
            "value":"244",
            "text":"COLOMBIA, BOYACÁ, EL COCUY",
            "code":"CO-15-244",
            "geoPoint":{
               "lat":6.407340000000033,
               "lon":-72.44563999999997
            },
            "geoRectangle":{
               "Xmin":-72.53863999999997,
               "Xmax":-72.35263999999997,
               "Ymin":6.314340000000033,
               "Ymax":6.500340000000033
            }
         },
         {
            "name":"EL ESPINO",
            "value":"248",
            "text":"COLOMBIA, BOYACÁ, EL ESPINO",
            "code":"CO-15-248",
            "geoPoint":{
               "lat":6.483670000000075,
               "lon":-72.49646999999999
            },
            "geoRectangle":{
               "Xmin":-72.54746999999999,
               "Xmax":-72.44546999999999,
               "Ymin":6.4326700000000745,
               "Ymax":6.534670000000075
            }
         },
         {
            "name":"FIRAVITOBA",
            "value":"272",
            "text":"COLOMBIA, BOYACÁ, FIRAVITOBA",
            "code":"CO-15-272",
            "geoPoint":{
               "lat":5.669490000000053,
               "lon":-72.99331999999998
            },
            "geoRectangle":{
               "Xmin":-73.05631999999999,
               "Xmax":-72.93031999999998,
               "Ymin":5.606490000000053,
               "Ymax":5.732490000000053
            }
         },
         {
            "name":"TASCO",
            "value":"790",
            "text":"COLOMBIA, BOYACÁ, TASCO",
            "code":"CO-15-790",
            "geoPoint":{
               "lat":5.909110000000055,
               "lon":-72.78097999999994
            },
            "geoRectangle":{
               "Xmin":-72.87897999999994,
               "Xmax":-72.68297999999994,
               "Ymin":5.811110000000055,
               "Ymax":6.007110000000055
            }
         },
         {
            "name":"TENZA",
            "value":"798",
            "text":"COLOMBIA, BOYACÁ, TENZA",
            "code":"CO-15-798",
            "geoPoint":{
               "lat":5.076690000000042,
               "lon":-73.42070999999999
            },
            "geoRectangle":{
               "Xmin":-73.46570999999999,
               "Xmax":-73.37570999999998,
               "Ymin":5.031690000000042,
               "Ymax":5.121690000000042
            }
         },
         {
            "name":"TIBANA",
            "value":"804",
            "text":"COLOMBIA, BOYACÁ, TIBANA",
            "code":"CO-15-804",
            "geoPoint":{
               "lat":5.317200000000071,
               "lon":-73.39694999999995
            },
            "geoRectangle":{
               "Xmin":-73.46494999999994,
               "Xmax":-73.32894999999995,
               "Ymin":5.249200000000071,
               "Ymax":5.38520000000007
            }
         },
         {
            "name":"TIBASOSA",
            "value":"806",
            "text":"COLOMBIA, BOYACÁ, TIBASOSA",
            "code":"CO-15-806",
            "geoPoint":{
               "lat":5.748870000000068,
               "lon":-72.99679999999995
            },
            "geoRectangle":{
               "Xmin":-73.05879999999995,
               "Xmax":-72.93479999999995,
               "Ymin":5.686870000000067,
               "Ymax":5.810870000000068
            }
         },
         {
            "name":"TINJACÁ",
            "value":"808",
            "text":"COLOMBIA, BOYACÁ, TINJACÁ",
            "code":"CO-15-808",
            "geoPoint":{
               "lat":5.579980000000035,
               "lon":-73.64743999999996
            },
            "geoRectangle":{
               "Xmin":-73.69943999999997,
               "Xmax":-73.59543999999995,
               "Ymin":5.527980000000035,
               "Ymax":5.631980000000034
            }
         },
         {
            "name":"FLORESTA",
            "value":"276",
            "text":"COLOMBIA, BOYACÁ, FLORESTA",
            "code":"CO-15-276",
            "geoPoint":{
               "lat":5.85955000000007,
               "lon":-72.91864999999996
            },
            "geoRectangle":{
               "Xmin":-72.97764999999995,
               "Xmax":-72.85964999999996,
               "Ymin":5.80055000000007,
               "Ymax":5.91855000000007
            }
         },
         {
            "name":"GACHANTIVÁ",
            "value":"293",
            "text":"COLOMBIA, BOYACÁ, GACHANTIVÁ",
            "code":"CO-15-293",
            "geoPoint":{
               "lat":5.751770000000022,
               "lon":-73.54920999999996
            },
            "geoRectangle":{
               "Xmin":-73.60420999999997,
               "Xmax":-73.49420999999995,
               "Ymin":5.696770000000022,
               "Ymax":5.8067700000000215
            }
         },
         {
            "name":"GAMEZA",
            "value":"296",
            "text":"COLOMBIA, BOYACÁ, GAMEZA",
            "code":"CO-15-296",
            "geoPoint":{
               "lat":5.802700000000073,
               "lon":-72.80488999999994
            },
            "geoRectangle":{
               "Xmin":-72.87988999999995,
               "Xmax":-72.72988999999994,
               "Ymin":5.727700000000072,
               "Ymax":5.877700000000073
            }
         },
         {
            "name":"GARAGOA",
            "value":"299",
            "text":"COLOMBIA, BOYACÁ, GARAGOA",
            "code":"CO-15-299",
            "geoPoint":{
               "lat":5.0832600000000525,
               "lon":-73.36420999999996
            },
            "geoRectangle":{
               "Xmin":-73.44220999999996,
               "Xmax":-73.28620999999995,
               "Ymin":5.005260000000052,
               "Ymax":5.161260000000053
            }
         },
         {
            "name":"GUACAMAYAS",
            "value":"317",
            "text":"COLOMBIA, BOYACÁ, GUACAMAYAS",
            "code":"CO-15-317",
            "geoPoint":{
               "lat":6.45998000000003,
               "lon":-72.50087999999994
            },
            "geoRectangle":{
               "Xmin":-72.54487999999994,
               "Xmax":-72.45687999999994,
               "Ymin":6.41598000000003,
               "Ymax":6.50398000000003
            }
         },
         {
            "name":"TOGÜÍ",
            "value":"816",
            "text":"COLOMBIA, BOYACÁ, TOGÜÍ",
            "code":"CO-15-816",
            "geoPoint":{
               "lat":5.937510000000032,
               "lon":-73.51324999999997
            },
            "geoRectangle":{
               "Xmin":-73.56924999999997,
               "Xmax":-73.45724999999997,
               "Ymin":5.8815100000000315,
               "Ymax":5.993510000000032
            }
         },
         {
            "name":"GUATEQUE",
            "value":"322",
            "text":"COLOMBIA, BOYACÁ, GUATEQUE",
            "code":"CO-15-322",
            "geoPoint":{
               "lat":5.007010000000037,
               "lon":-73.47310999999996
            },
            "geoRectangle":{
               "Xmin":-73.51210999999996,
               "Xmax":-73.43410999999996,
               "Ymin":4.968010000000037,
               "Ymax":5.046010000000036
            }
         },
         {
            "name":"GUAYATÁ",
            "value":"325",
            "text":"COLOMBIA, BOYACÁ, GUAYATÁ",
            "code":"CO-15-325",
            "geoPoint":{
               "lat":4.964680000000044,
               "lon":-73.49048999999997
            },
            "geoRectangle":{
               "Xmin":-73.54948999999996,
               "Xmax":-73.43148999999997,
               "Ymin":4.905680000000044,
               "Ymax":5.023680000000044
            }
         },
         {
            "name":"IZA",
            "value":"362",
            "text":"COLOMBIA, BOYACÁ, IZA",
            "code":"CO-15-362",
            "geoPoint":{
               "lat":5.612040000000036,
               "lon":-72.98052999999999
            },
            "geoRectangle":{
               "Xmin":-73.01652999999999,
               "Xmax":-72.94452999999999,
               "Ymin":5.576040000000036,
               "Ymax":5.6480400000000355
            }
         },
         {
            "name":"JENESANO",
            "value":"367",
            "text":"COLOMBIA, BOYACÁ, JENESANO",
            "code":"CO-15-367",
            "geoPoint":{
               "lat":5.385190000000023,
               "lon":-73.36456999999996
            },
            "geoRectangle":{
               "Xmin":-73.41956999999996,
               "Xmax":-73.30956999999995,
               "Ymin":5.330190000000023,
               "Ymax":5.4401900000000225
            }
         },
         {
            "name":"JERICÓ",
            "value":"368",
            "text":"COLOMBIA, BOYACÁ, JERICÓ",
            "code":"CO-15-368",
            "geoPoint":{
               "lat":6.14721000000003,
               "lon":-72.57057999999995
            },
            "geoRectangle":{
               "Xmin":-72.64057999999994,
               "Xmax":-72.50057999999996,
               "Ymin":6.077210000000029,
               "Ymax":6.21721000000003
            }
         },
         {
            "name":"VILLA DE LEYVA",
            "value":"407",
            "text":"COLOMBIA, BOYACÁ, VILLA DE LEYVA",
            "code":"CO-15-407",
            "geoPoint":{
               "lat":5.633450000000039,
               "lon":-73.52396999999996
            },
            "geoRectangle":{
               "Xmin":-73.59596999999997,
               "Xmax":-73.45196999999996,
               "Ymin":5.561450000000039,
               "Ymax":5.705450000000039
            }
         },
         {
            "name":"MACANAL",
            "value":"425",
            "text":"COLOMBIA, BOYACÁ, MACANAL",
            "code":"CO-15-425",
            "geoPoint":{
               "lat":4.971970000000056,
               "lon":-73.31964999999997
            },
            "geoRectangle":{
               "Xmin":-73.40464999999996,
               "Xmax":-73.23464999999997,
               "Ymin":4.886970000000056,
               "Ymax":5.056970000000056
            }
         },
         {
            "name":"MONGUÌ",
            "value":"466",
            "text":"COLOMBIA, BOYACÁ, MONGUÌ",
            "code":"CO-15-466",
            "geoPoint":{
               "lat":5.722880000000032,
               "lon":-72.84886999999998
            },
            "geoRectangle":{
               "Xmin":-72.89786999999998,
               "Xmax":-72.79986999999997,
               "Ymin":5.673880000000032,
               "Ymax":5.771880000000032
            }
         },
         {
            "name":"NUEVO COLÓN",
            "value":"494",
            "text":"COLOMBIA, BOYACÁ, NUEVO COLÓN",
            "code":"CO-15-494",
            "geoPoint":{
               "lat":5.355130000000031,
               "lon":-73.45755999999994
            },
            "geoRectangle":{
               "Xmin":-73.50555999999995,
               "Xmax":-73.40955999999994,
               "Ymin":5.307130000000031,
               "Ymax":5.403130000000031
            }
         },
         {
            "name":"OICATA",
            "value":"500",
            "text":"COLOMBIA, BOYACÁ, OICATA",
            "code":"CO-15-500",
            "geoPoint":{
               "lat":5.594940000000065,
               "lon":-73.30833999999999
            },
            "geoRectangle":{
               "Xmin":-73.35633999999999,
               "Xmax":-73.26033999999999,
               "Ymin":5.546940000000065,
               "Ymax":5.642940000000065
            }
         },
         {
            "name":"OTANCHE",
            "value":"507",
            "text":"COLOMBIA, BOYACÁ, OTANCHE",
            "code":"CO-15-507",
            "geoPoint":{
               "lat":5.657560000000046,
               "lon":-74.18151999999998
            },
            "geoRectangle":{
               "Xmin":-74.32351999999997,
               "Xmax":-74.03951999999998,
               "Ymin":5.515560000000046,
               "Ymax":5.799560000000047
            }
         },
         {
            "name":"PACHAVITA",
            "value":"511",
            "text":"COLOMBIA, BOYACÁ, PACHAVITA",
            "code":"CO-15-511",
            "geoPoint":{
               "lat":5.139480000000049,
               "lon":-73.39723999999995
            },
            "geoRectangle":{
               "Xmin":-73.44923999999996,
               "Xmax":-73.34523999999995,
               "Ymin":5.087480000000049,
               "Ymax":5.191480000000048
            }
         },
         {
            "name":"PÁEZ",
            "value":"514",
            "text":"COLOMBIA, BOYACÁ, PÁEZ",
            "code":"CO-15-514",
            "geoPoint":{
               "lat":5.101540000000057,
               "lon":-73.05069999999995
            },
            "geoRectangle":{
               "Xmin":-73.16269999999994,
               "Xmax":-72.93869999999995,
               "Ymin":4.989540000000057,
               "Ymax":5.213540000000057
            }
         },
         {
            "name":"PAIPA",
            "value":"516",
            "text":"COLOMBIA, BOYACÁ, PAIPA",
            "code":"CO-15-516",
            "geoPoint":{
               "lat":5.780910000000063,
               "lon":-73.11766999999998
            },
            "geoRectangle":{
               "Xmin":-73.25166999999998,
               "Xmax":-72.98366999999998,
               "Ymin":5.646910000000062,
               "Ymax":5.914910000000063
            }
         },
         {
            "name":"PAJARITO",
            "value":"518",
            "text":"COLOMBIA, BOYACÁ, PAJARITO",
            "code":"CO-15-518",
            "geoPoint":{
               "lat":5.2931200000000445,
               "lon":-72.70307999999994
            },
            "geoRectangle":{
               "Xmin":-72.81907999999994,
               "Xmax":-72.58707999999994,
               "Ymin":5.177120000000045,
               "Ymax":5.409120000000044
            }
         },
         {
            "name":"PANQUEBA",
            "value":"522",
            "text":"COLOMBIA, BOYACÁ, PANQUEBA",
            "code":"CO-15-522",
            "geoPoint":{
               "lat":6.443670000000054,
               "lon":-72.45932999999997
            },
            "geoRectangle":{
               "Xmin":-72.50132999999997,
               "Xmax":-72.41732999999996,
               "Ymin":6.401670000000054,
               "Ymax":6.485670000000054
            }
         },
         {
            "name":"PAUNA",
            "value":"531",
            "text":"COLOMBIA, BOYACÁ, PAUNA",
            "code":"CO-15-531",
            "geoPoint":{
               "lat":5.655500000000075,
               "lon":-73.97980999999999
            },
            "geoRectangle":{
               "Xmin":-74.09680999999999,
               "Xmax":-73.86280999999998,
               "Ymin":5.538500000000075,
               "Ymax":5.772500000000075
            }
         },
         {
            "name":"PAYA",
            "value":"533",
            "text":"COLOMBIA, BOYACÁ, PAYA",
            "code":"CO-15-533",
            "geoPoint":{
               "lat":5.624990000000025,
               "lon":-72.42377999999997
            },
            "geoRectangle":{
               "Xmin":-72.57177999999996,
               "Xmax":-72.27577999999997,
               "Ymin":5.476990000000026,
               "Ymax":5.772990000000025
            }
         },
         {
            "name":"PAZ DE RÍO",
            "value":"537",
            "text":"COLOMBIA, BOYACÁ, PAZ DE RÍO",
            "code":"CO-15-537",
            "geoPoint":{
               "lat":5.9851900000000455,
               "lon":-72.74983999999995
            },
            "geoRectangle":{
               "Xmin":-72.81483999999995,
               "Xmax":-72.68483999999995,
               "Ymin":5.920190000000045,
               "Ymax":6.050190000000046
            }
         },
         {
            "name":"RAMIRIQUÍ",
            "value":"599",
            "text":"COLOMBIA, BOYACÁ, RAMIRIQUÍ",
            "code":"CO-15-599",
            "geoPoint":{
               "lat":5.400210000000072,
               "lon":-73.33542999999997
            },
            "geoRectangle":{
               "Xmin":-73.42442999999997,
               "Xmax":-73.24642999999998,
               "Ymin":5.311210000000072,
               "Ymax":5.489210000000073
            }
         },
         {
            "name":"RÁQUIRA",
            "value":"600",
            "text":"COLOMBIA, BOYACÁ, RÁQUIRA",
            "code":"CO-15-600",
            "geoPoint":{
               "lat":5.538870000000031,
               "lon":-73.63216999999997
            },
            "geoRectangle":{
               "Xmin":-73.72316999999997,
               "Xmax":-73.54116999999998,
               "Ymin":5.447870000000031,
               "Ymax":5.6298700000000315
            }
         },
         {
            "name":"RONDÓN",
            "value":"621",
            "text":"COLOMBIA, BOYACÁ, RONDÓN",
            "code":"CO-15-621",
            "geoPoint":{
               "lat":5.35659000000004,
               "lon":-73.20874999999995
            },
            "geoRectangle":{
               "Xmin":-73.28574999999995,
               "Xmax":-73.13174999999995,
               "Ymin":5.27959000000004,
               "Ymax":5.43359000000004
            }
         },
         {
            "name":"SABOYÁ",
            "value":"632",
            "text":"COLOMBIA, BOYACÁ, SABOYÁ",
            "code":"CO-15-632",
            "geoPoint":{
               "lat":5.6988000000000625,
               "lon":-73.76656999999994
            },
            "geoRectangle":{
               "Xmin":-73.86456999999994,
               "Xmax":-73.66856999999995,
               "Ymin":5.600800000000063,
               "Ymax":5.796800000000062
            }
         },
         {
            "name":"SÁCHICA",
            "value":"638",
            "text":"COLOMBIA, BOYACÁ, SÁCHICA",
            "code":"CO-15-638",
            "geoPoint":{
               "lat":5.583510000000047,
               "lon":-73.54259999999994
            },
            "geoRectangle":{
               "Xmin":-73.59059999999994,
               "Xmax":-73.49459999999993,
               "Ymin":5.535510000000047,
               "Ymax":5.631510000000047
            }
         },
         {
            "name":"SAMACÁ",
            "value":"646",
            "text":"COLOMBIA, BOYACÁ, SAMACÁ",
            "code":"CO-15-646",
            "geoPoint":{
               "lat":5.494120000000066,
               "lon":-73.48435999999998
            },
            "geoRectangle":{
               "Xmin":-73.57335999999998,
               "Xmax":-73.39535999999998,
               "Ymin":5.405120000000066,
               "Ymax":5.583120000000067
            }
         },
         {
            "name":"SAN EDUARDO",
            "value":"660",
            "text":"COLOMBIA, BOYACÁ, SAN EDUARDO",
            "code":"CO-15-660",
            "geoPoint":{
               "lat":5.223860000000059,
               "lon":-73.07701999999995
            },
            "geoRectangle":{
               "Xmin":-73.14201999999995,
               "Xmax":-73.01201999999995,
               "Ymin":5.158860000000058,
               "Ymax":5.288860000000059
            }
         },
         {
            "name":"SAN JOSÉ DE PARE",
            "value":"664",
            "text":"COLOMBIA, BOYACÁ, SAN JOSÉ DE PARE",
            "code":"CO-15-664",
            "geoPoint":{
               "lat":6.0170400000000654,
               "lon":-73.54809999999998
            },
            "geoRectangle":{
               "Xmin":-73.60009999999998,
               "Xmax":-73.49609999999997,
               "Ymin":5.965040000000066,
               "Ymax":6.069040000000065
            }
         },
         {
            "name":"SAN LUIS DE GACENO",
            "value":"667",
            "text":"COLOMBIA, BOYACÁ, SAN LUIS DE GACENO",
            "code":"CO-15-667",
            "geoPoint":{
               "lat":4.819500000000062,
               "lon":-73.16885999999994
            },
            "geoRectangle":{
               "Xmin":-73.30785999999993,
               "Xmax":-73.02985999999994,
               "Ymin":4.680500000000062,
               "Ymax":4.958500000000062
            }
         },
         {
            "name":"SAN MATEO",
            "value":"673",
            "text":"COLOMBIA, BOYACÁ, SAN MATEO",
            "code":"CO-15-673",
            "geoPoint":{
               "lat":6.4018500000000245,
               "lon":-72.55548999999996
            },
            "geoRectangle":{
               "Xmin":-72.62748999999997,
               "Xmax":-72.48348999999996,
               "Ymin":6.329850000000024,
               "Ymax":6.4738500000000245
            }
         },
         {
            "name":"SAN MIGUEL DE SEMA",
            "value":"676",
            "text":"COLOMBIA, BOYACÁ, SAN MIGUEL DE SEMA",
            "code":"CO-15-676",
            "geoPoint":{
               "lat":5.5184300000000235,
               "lon":-73.72242999999997
            },
            "geoRectangle":{
               "Xmin":-73.77742999999998,
               "Xmax":-73.66742999999997,
               "Ymin":5.463430000000024,
               "Ymax":5.573430000000023
            }
         },
         {
            "name":"SAN PABLO DE BORBUR",
            "value":"681",
            "text":"COLOMBIA, BOYACÁ, SAN PABLO DE BORBUR",
            "code":"CO-15-681",
            "geoPoint":{
               "lat":5.650830000000042,
               "lon":-74.06960999999995
            },
            "geoRectangle":{
               "Xmin":-74.14460999999996,
               "Xmax":-73.99460999999995,
               "Ymin":5.575830000000042,
               "Ymax":5.725830000000042
            }
         },
         {
            "name":"SANTANA",
            "value":"686",
            "text":"COLOMBIA, BOYACÁ, SANTANA",
            "code":"CO-15-686",
            "geoPoint":{
               "lat":6.0575300000000425,
               "lon":-73.48089999999996
            },
            "geoRectangle":{
               "Xmin":-73.53089999999996,
               "Xmax":-73.43089999999997,
               "Ymin":6.007530000000043,
               "Ymax":6.107530000000042
            }
         },
         {
            "name":"SANTA MARÍA",
            "value":"690",
            "text":"COLOMBIA, BOYACÁ, SANTA MARÍA",
            "code":"CO-15-690",
            "geoPoint":{
               "lat":4.859110000000044,
               "lon":-73.26287999999994
            },
            "geoRectangle":{
               "Xmin":-73.37787999999993,
               "Xmax":-73.14787999999994,
               "Ymin":4.744110000000044,
               "Ymax":4.974110000000044
            }
         },
         {
            "name":"SANTA ROSA DE VITERBO",
            "value":"693",
            "text":"COLOMBIA, BOYACÁ, SANTA ROSA DE VITERBO",
            "code":"CO-15-693",
            "geoPoint":{
               "lat":5.875140000000044,
               "lon":-72.98301999999995
            },
            "geoRectangle":{
               "Xmin":-73.05901999999995,
               "Xmax":-72.90701999999996,
               "Ymin":5.799140000000045,
               "Ymax":5.951140000000044
            }
         },
         {
            "name":"SANTA SOFÍA",
            "value":"696",
            "text":"COLOMBIA, BOYACÁ, SANTA SOFÍA",
            "code":"CO-15-696",
            "geoPoint":{
               "lat":5.713420000000042,
               "lon":-73.60259999999994
            },
            "geoRectangle":{
               "Xmin":-73.65259999999994,
               "Xmax":-73.55259999999994,
               "Ymin":5.663420000000042,
               "Ymax":5.763420000000042
            }
         },
         {
            "name":"SATIVANORTE",
            "value":"720",
            "text":"COLOMBIA, BOYACÁ, SATIVANORTE",
            "code":"CO-15-720",
            "geoPoint":{
               "lat":6.131460000000061,
               "lon":-72.70892999999995
            },
            "geoRectangle":{
               "Xmin":-72.78892999999995,
               "Xmax":-72.62892999999995,
               "Ymin":6.051460000000061,
               "Ymax":6.211460000000061
            }
         },
         {
            "name":"SATIVASUR",
            "value":"723",
            "text":"COLOMBIA, BOYACÁ, SATIVASUR",
            "code":"CO-15-723",
            "geoPoint":{
               "lat":6.092260000000067,
               "lon":-72.71272999999997
            },
            "geoRectangle":{
               "Xmin":-72.77072999999997,
               "Xmax":-72.65472999999996,
               "Ymin":6.034260000000067,
               "Ymax":6.150260000000067
            }
         },
         {
            "name":"SIACHOQUE",
            "value":"740",
            "text":"COLOMBIA, BOYACÁ, SIACHOQUE",
            "code":"CO-15-740",
            "geoPoint":{
               "lat":5.514460000000042,
               "lon":-73.24565999999999
            },
            "geoRectangle":{
               "Xmin":-73.32365999999999,
               "Xmax":-73.16765999999998,
               "Ymin":5.436460000000042,
               "Ymax":5.592460000000043
            }
         },
         {
            "name":"SOATÁ",
            "value":"753",
            "text":"COLOMBIA, BOYACÁ, SOATÁ",
            "code":"CO-15-753",
            "geoPoint":{
               "lat":6.332680000000039,
               "lon":-72.68280999999996
            },
            "geoRectangle":{
               "Xmin":-72.74380999999997,
               "Xmax":-72.62180999999995,
               "Ymin":6.271680000000039,
               "Ymax":6.393680000000039
            }
         },
         {
            "name":"SORA",
            "value":"762",
            "text":"COLOMBIA, BOYACÁ, SORA",
            "code":"CO-15-762",
            "geoPoint":{
               "lat":5.566450000000032,
               "lon":-73.45059999999995
            },
            "geoRectangle":{
               "Xmin":-73.49459999999995,
               "Xmax":-73.40659999999995,
               "Ymin":5.522450000000032,
               "Ymax":5.610450000000031
            }
         },
         {
            "name":"SOTAQUIRÁ",
            "value":"763",
            "text":"COLOMBIA, BOYACÁ, SOTAQUIRÁ",
            "code":"CO-15-763",
            "geoPoint":{
               "lat":5.7646100000000615,
               "lon":-73.24736999999999
            },
            "geoRectangle":{
               "Xmin":-73.35237,
               "Xmax":-73.14236999999999,
               "Ymin":5.659610000000061,
               "Ymax":5.869610000000062
            }
         },
         {
            "name":"SORACÁ",
            "value":"764",
            "text":"COLOMBIA, BOYACÁ, SORACÁ",
            "code":"CO-15-764",
            "geoPoint":{
               "lat":5.501330000000053,
               "lon":-73.33249999999998
            },
            "geoRectangle":{
               "Xmin":-73.38049999999998,
               "Xmax":-73.28449999999998,
               "Ymin":5.453330000000053,
               "Ymax":5.549330000000053
            }
         },
         {
            "name":"SUSACÓN",
            "value":"774",
            "text":"COLOMBIA, BOYACÁ, SUSACÓN",
            "code":"CO-15-774",
            "geoPoint":{
               "lat":6.230730000000051,
               "lon":-72.69013999999999
            },
            "geoRectangle":{
               "Xmin":-72.77613999999998,
               "Xmax":-72.60413999999999,
               "Ymin":6.144730000000051,
               "Ymax":6.316730000000051
            }
         },
         {
            "name":"SUTAMARCHÀN",
            "value":"776",
            "text":"COLOMBIA, BOYACÁ, SUTAMARCHÀN",
            "code":"CO-15-776",
            "geoPoint":{
               "lat":5.620210000000043,
               "lon":-73.62009999999998
            },
            "geoRectangle":{
               "Xmin":-73.68109999999999,
               "Xmax":-73.55909999999997,
               "Ymin":5.559210000000043,
               "Ymax":5.681210000000043
            }
         },
         {
            "name":" SUTATENZA",
            "value":"778",
            "text":"COLOMBIA, BOYACÁ,  SUTATENZA",
            "code":"CO-15-778",
            "geoPoint":{
               "lat":5.023630000000026,
               "lon":-73.45039999999995
            },
            "geoRectangle":{
               "Xmin":-73.48839999999994,
               "Xmax":-73.41239999999995,
               "Ymin":4.985630000000025,
               "Ymax":5.061630000000026
            }
         },
         {
            "name":"TIPACOQUE",
            "value":"810",
            "text":"COLOMBIA, BOYACÁ, TIPACOQUE",
            "code":"CO-15-810",
            "geoPoint":{
               "lat":6.421170000000075,
               "lon":-72.69200999999998
            },
            "geoRectangle":{
               "Xmin":-72.74000999999998,
               "Xmax":-72.64400999999998,
               "Ymin":6.373170000000075,
               "Ymax":6.469170000000075
            }
         },
         {
            "name":"TOCA",
            "value":"814",
            "text":"COLOMBIA, BOYACÁ, TOCA",
            "code":"CO-15-814",
            "geoPoint":{
               "lat":5.565010000000029,
               "lon":-73.18416999999994
            },
            "geoRectangle":{
               "Xmin":-73.26316999999993,
               "Xmax":-73.10516999999994,
               "Ymin":5.48601000000003,
               "Ymax":5.644010000000029
            }
         },
         {
            "name":"TÓPAGA",
            "value":"820",
            "text":"COLOMBIA, BOYACÁ, TÓPAGA",
            "code":"CO-15-820",
            "geoPoint":{
               "lat":5.7687100000000555,
               "lon":-72.83271999999994
            },
            "geoRectangle":{
               "Xmin":-72.86971999999994,
               "Xmax":-72.79571999999993,
               "Ymin":5.731710000000056,
               "Ymax":5.805710000000055
            }
         },
         {
            "name":"TOTA",
            "value":"822",
            "text":"COLOMBIA, BOYACÁ, TOTA",
            "code":"CO-15-822",
            "geoPoint":{
               "lat":5.560680000000048,
               "lon":-72.98604999999998
            },
            "geoRectangle":{
               "Xmin":-73.08104999999998,
               "Xmax":-72.89104999999998,
               "Ymin":5.465680000000048,
               "Ymax":5.655680000000047
            }
         },
         {
            "name":"TUNUNGUÁ",
            "value":"832",
            "text":"COLOMBIA, BOYACÁ, TUNUNGUÁ",
            "code":"CO-15-832",
            "geoPoint":{
               "lat":5.730140000000063,
               "lon":-73.93319999999994
            },
            "geoRectangle":{
               "Xmin":-73.97419999999994,
               "Xmax":-73.89219999999995,
               "Ymin":5.689140000000062,
               "Ymax":5.771140000000063
            }
         },
         {
            "name":"TURMEQUÉ",
            "value":"835",
            "text":"COLOMBIA, BOYACÁ, TURMEQUÉ",
            "code":"CO-15-835",
            "geoPoint":{
               "lat":5.324290000000076,
               "lon":-73.49031999999994
            },
            "geoRectangle":{
               "Xmin":-73.54831999999995,
               "Xmax":-73.43231999999993,
               "Ymin":5.266290000000076,
               "Ymax":5.382290000000076
            }
         },
         {
            "name":"TUTA",
            "value":"837",
            "text":"COLOMBIA, BOYACÁ, TUTA",
            "code":"CO-15-837",
            "geoPoint":{
               "lat":5.689770000000067,
               "lon":-73.22754999999995
            },
            "geoRectangle":{
               "Xmin":-73.30654999999994,
               "Xmax":-73.14854999999996,
               "Ymin":5.610770000000067,
               "Ymax":5.768770000000067
            }
         },
         {
            "name":"TUTAZÁ",
            "value":"839",
            "text":"COLOMBIA, BOYACÁ, TUTAZÁ",
            "code":"CO-15-839",
            "geoPoint":{
               "lat":6.101460000000031,
               "lon":-72.86654999999996
            },
            "geoRectangle":{
               "Xmin":-72.93354999999995,
               "Xmax":-72.79954999999997,
               "Ymin":6.034460000000031,
               "Ymax":6.168460000000032
            }
         }
      ]
   },
   {
      "name":"CALDAS",
      "value":"17",
      "text":"COLOMBIA, CALDAS",
      "code":"CO-17",
      "geoPoint":{
         "lat":6.092580000000055,
         "lon":-75.63688999999994
      },
      "geoRectangle":{
         "Xmin":-75.69988999999994,
         "Xmax":-75.57388999999993,
         "Ymin":6.029580000000055,
         "Ymax":6.155580000000055
      },
      "municipality":[
         {
            "name":"RIOSUCIO",
            "value":"614",
            "text":"COLOMBIA, CALDAS, RIOSUCIO",
            "code":"CO-17-614",
            "geoPoint":{
               "lat":5.4214900000000625,
               "lon":-75.70308999999997
            },
            "geoRectangle":{
               "Xmin":-75.83208999999998,
               "Xmax":-75.57408999999997,
               "Ymin":5.292490000000063,
               "Ymax":5.550490000000062
            }
         },
         {
            "name":"ANSERMA",
            "value":"042",
            "text":"COLOMBIA, CALDAS, ANSERMA",
            "code":"CO-17-042",
            "geoPoint":{
               "lat":5.233070000000055,
               "lon":-75.78604999999999
            },
            "geoRectangle":{
               "Xmin":-75.87904999999999,
               "Xmax":-75.69304999999999,
               "Ymin":5.140070000000055,
               "Ymax":5.326070000000055
            }
         },
         {
            "name":"NORCASIA",
            "value":"495",
            "text":"COLOMBIA, CALDAS, NORCASIA",
            "code":"CO-17-495",
            "geoPoint":{
               "lat":5.575300000000027,
               "lon":-74.88837999999998
            },
            "geoRectangle":{
               "Xmin":-75.00037999999998,
               "Xmax":-74.77637999999999,
               "Ymin":5.463300000000027,
               "Ymax":5.687300000000027
            }
         },
         {
            "name":"VITERBO",
            "value":"877",
            "text":"COLOMBIA, CALDAS, VITERBO",
            "code":"CO-17-877",
            "geoPoint":{
               "lat":5.061730000000068,
               "lon":-75.87170999999995
            },
            "geoRectangle":{
               "Xmin":-75.94070999999995,
               "Xmax":-75.80270999999995,
               "Ymin":4.992730000000068,
               "Ymax":5.130730000000068
            }
         },
         {
            "name":"VILLAMARIA",
            "value":"873",
            "text":"COLOMBIA, CALDAS, VILLAMARIA",
            "code":"CO-17-873",
            "geoPoint":{
               "lat":5.044650000000047,
               "lon":-75.51498999999995
            },
            "geoRectangle":{
               "Xmin":-75.64498999999995,
               "Xmax":-75.38498999999996,
               "Ymin":4.914650000000047,
               "Ymax":5.174650000000047
            }
         },
         {
            "name":"VICTORIA",
            "value":"867",
            "text":"COLOMBIA, CALDAS, VICTORIA",
            "code":"CO-17-867",
            "geoPoint":{
               "lat":5.316760000000045,
               "lon":-74.91167999999993
            },
            "geoRectangle":{
               "Xmin":-75.05667999999993,
               "Xmax":-74.76667999999994,
               "Ymin":5.171760000000045,
               "Ymax":5.461760000000044
            }
         },
         {
            "name":"SUPIA",
            "value":"777",
            "text":"COLOMBIA, CALDAS, SUPIA",
            "code":"CO-17-777",
            "geoPoint":{
               "lat":5.452200000000062,
               "lon":-75.65011999999996
            },
            "geoRectangle":{
               "Xmin":-75.71511999999996,
               "Xmax":-75.58511999999996,
               "Ymin":5.387200000000061,
               "Ymax":5.517200000000062
            }
         },
         {
            "name":"SAN JOSÉ",
            "value":"665",
            "text":"COLOMBIA, CALDAS, SAN JOSÉ",
            "code":"CO-17-665",
            "geoPoint":{
               "lat":5.084970000000055,
               "lon":-75.79050999999998
            },
            "geoRectangle":{
               "Xmin":-75.84550999999999,
               "Xmax":-75.73550999999998,
               "Ymin":5.029970000000056,
               "Ymax":5.139970000000055
            }
         },
         {
            "name":"SAMANA",
            "value":"662",
            "text":"COLOMBIA, CALDAS, SAMANA",
            "code":"CO-17-662",
            "geoPoint":{
               "lat":5.412580000000048,
               "lon":-74.99218999999994
            },
            "geoRectangle":{
               "Xmin":-75.00218999999994,
               "Xmax":-74.98218999999993,
               "Ymin":5.402580000000048,
               "Ymax":5.422580000000048
            }
         },
         {
            "name":"SALAMINA",
            "value":"653",
            "text":"COLOMBIA, CALDAS, SALAMINA",
            "code":"CO-17-653",
            "geoPoint":{
               "lat":5.403210000000058,
               "lon":-75.48674999999997
            },
            "geoRectangle":{
               "Xmin":-75.61074999999997,
               "Xmax":-75.36274999999998,
               "Ymin":5.279210000000059,
               "Ymax":5.527210000000058
            }
         },
         {
            "name":"RISARALDA",
            "value":"616",
            "text":"COLOMBIA, CALDAS, RISARALDA",
            "code":"CO-17-616",
            "geoPoint":{
               "lat":5.164790000000039,
               "lon":-75.76682999999997
            },
            "geoRectangle":{
               "Xmin":-75.83482999999997,
               "Xmax":-75.69882999999997,
               "Ymin":5.0967900000000395,
               "Ymax":5.232790000000039
            }
         },
         {
            "name":"PENSILVANIA",
            "value":"541",
            "text":"COLOMBIA, CALDAS, PENSILVANIA",
            "code":"CO-17-541",
            "geoPoint":{
               "lat":5.384160000000065,
               "lon":-75.16073999999998
            },
            "geoRectangle":{
               "Xmin":-75.30473999999998,
               "Xmax":-75.01673999999997,
               "Ymin":5.240160000000065,
               "Ymax":5.5281600000000655
            }
         },
         {
            "name":"PALESTINA",
            "value":"524",
            "text":"COLOMBIA, CALDAS, PALESTINA",
            "code":"CO-17-524",
            "geoPoint":{
               "lat":5.020400000000052,
               "lon":-75.62290999999993
            },
            "geoRectangle":{
               "Xmin":-75.68890999999994,
               "Xmax":-75.55690999999993,
               "Ymin":4.954400000000052,
               "Ymax":5.086400000000052
            }
         },
         {
            "name":"PÁCORA",
            "value":"513",
            "text":"COLOMBIA, CALDAS, PÁCORA",
            "code":"CO-17-513",
            "geoPoint":{
               "lat":5.526550000000043,
               "lon":-75.45949999999993
            },
            "geoRectangle":{
               "Xmin":-75.55049999999993,
               "Xmax":-75.36849999999994,
               "Ymin":5.435550000000043,
               "Ymax":5.617550000000043
            }
         },
         {
            "name":"MANIZALES",
            "value":"001",
            "text":"COLOMBIA, CALDAS, MANIZALES",
            "code":"CO-17-001",
            "geoPoint":{
               "lat":5.064650000000029,
               "lon":-75.50754999999998
            },
            "geoRectangle":{
               "Xmin":-75.64154999999998,
               "Xmax":-75.37354999999998,
               "Ymin":4.930650000000028,
               "Ymax":5.198650000000029
            }
         },
         {
            "name":"AGUADAS",
            "value":"013",
            "text":"COLOMBIA, CALDAS, AGUADAS",
            "code":"CO-17-013",
            "geoPoint":{
               "lat":5.612170000000049,
               "lon":-75.45651999999995
            },
            "geoRectangle":{
               "Xmin":-75.60751999999995,
               "Xmax":-75.30551999999996,
               "Ymin":5.461170000000049,
               "Ymax":5.7631700000000485
            }
         },
         {
            "name":"NEIRA",
            "value":"486",
            "text":"COLOMBIA, CALDAS, NEIRA",
            "code":"CO-17-486",
            "geoPoint":{
               "lat":5.166500000000042,
               "lon":-75.51954999999998
            },
            "geoRectangle":{
               "Xmin":-75.65854999999998,
               "Xmax":-75.38054999999999,
               "Ymin":5.027500000000042,
               "Ymax":5.305500000000042
            }
         },
         {
            "name":"MARULANDA",
            "value":"446",
            "text":"COLOMBIA, CALDAS, MARULANDA",
            "code":"CO-17-446",
            "geoPoint":{
               "lat":5.284690000000069,
               "lon":-75.26004999999998
            },
            "geoRectangle":{
               "Xmin":-75.38704999999997,
               "Xmax":-75.13304999999998,
               "Ymin":5.157690000000069,
               "Ymax":5.4116900000000685
            }
         },
         {
            "name":"MARQUETALIA",
            "value":"444",
            "text":"COLOMBIA, CALDAS, MARQUETALIA",
            "code":"CO-17-444",
            "geoPoint":{
               "lat":5.2966500000000565,
               "lon":-75.05355999999995
            },
            "geoRectangle":{
               "Xmin":-75.11255999999995,
               "Xmax":-74.99455999999995,
               "Ymin":5.237650000000056,
               "Ymax":5.355650000000057
            }
         },
         {
            "name":"MARMATO",
            "value":"442",
            "text":"COLOMBIA, CALDAS, MARMATO",
            "code":"CO-17-442",
            "geoPoint":{
               "lat":5.474900000000048,
               "lon":-75.59925999999996
            },
            "geoRectangle":{
               "Xmin":-75.63725999999996,
               "Xmax":-75.56125999999996,
               "Ymin":5.436900000000048,
               "Ymax":5.512900000000048
            }
         },
         {
            "name":"MANZANARES",
            "value":"433",
            "text":"COLOMBIA, CALDAS, MANZANARES",
            "code":"CO-17-433",
            "geoPoint":{
               "lat":5.253700000000038,
               "lon":-75.15323999999998
            },
            "geoRectangle":{
               "Xmin":-75.23823999999998,
               "Xmax":-75.06823999999999,
               "Ymin":5.168700000000038,
               "Ymax":5.3387000000000375
            }
         },
         {
            "name":"LA MERCED",
            "value":"388",
            "text":"COLOMBIA, CALDAS, LA MERCED",
            "code":"CO-17-388",
            "geoPoint":{
               "lat":5.399160000000052,
               "lon":-75.54734999999994
            },
            "geoRectangle":{
               "Xmin":-75.60234999999994,
               "Xmax":-75.49234999999993,
               "Ymin":5.344160000000052,
               "Ymax":5.454160000000051
            }
         },
         {
            "name":"LA DORADA",
            "value":"380",
            "text":"COLOMBIA, CALDAS, LA DORADA",
            "code":"CO-17-380",
            "geoPoint":{
               "lat":5.454600000000028,
               "lon":-74.66289999999998
            },
            "geoRectangle":{
               "Xmin":-74.83389999999999,
               "Xmax":-74.49189999999997,
               "Ymin":5.283600000000027,
               "Ymax":5.625600000000028
            }
         },
         {
            "name":"FILADELFIA",
            "value":"272",
            "text":"COLOMBIA, CALDAS, FILADELFIA",
            "code":"CO-17-272",
            "geoPoint":{
               "lat":5.295650000000023,
               "lon":-75.56280999999996
            },
            "geoRectangle":{
               "Xmin":-75.64380999999996,
               "Xmax":-75.48180999999995,
               "Ymin":5.214650000000023,
               "Ymax":5.376650000000024
            }
         },
         {
            "name":"CHINCHINÁ",
            "value":"174",
            "text":"COLOMBIA, CALDAS, CHINCHINÁ",
            "code":"CO-17-174",
            "geoPoint":{
               "lat":4.984290000000044,
               "lon":-75.60743999999994
            },
            "geoRectangle":{
               "Xmin":-75.68743999999994,
               "Xmax":-75.52743999999994,
               "Ymin":4.904290000000044,
               "Ymax":5.064290000000044
            }
         },
         {
            "name":"BELALCÁZAR",
            "value":"088",
            "text":"COLOMBIA, CALDAS, BELALCÁZAR",
            "code":"CO-17-088",
            "geoPoint":{
               "lat":4.994750000000067,
               "lon":-75.81252999999998
            },
            "geoRectangle":{
               "Xmin":-75.88052999999998,
               "Xmax":-75.74452999999998,
               "Ymin":4.926750000000068,
               "Ymax":5.062750000000067
            }
         },
         {
            "name":"ARANZAZU",
            "value":"050",
            "text":"COLOMBIA, CALDAS, ARANZAZU",
            "code":"CO-17-050",
            "geoPoint":{
               "lat":5.271100000000047,
               "lon":-75.49057999999997
            },
            "geoRectangle":{
               "Xmin":-75.56957999999996,
               "Xmax":-75.41157999999997,
               "Ymin":5.192100000000047,
               "Ymax":5.3501000000000465
            }
         }
      ]
   },
   {
      "name":"CAQUETÁ",
      "value":"18",
      "text":"COLOMBIA, CAQUETÁ",
      "code":"CO-18",
      "geoPoint":{
         "lat":0.8016346990000329,
         "lon":-73.96272982099998
      },
      "geoRectangle":{
         "Xmin":-76.12772982099999,
         "Xmax":-71.79772982099998,
         "Ymin":-1.3633653009999671,
         "Ymax":2.966634699000033
      },
      "municipality":[
         {
            "name":"SAN JOSÉ DEL FRAGUA",
            "value":"610",
            "text":"COLOMBIA, CAQUETÁ, SAN JOSÉ DEL FRAGUA",
            "code":"CO-18-610",
            "geoPoint":{
               "lat":1.3320300000000316,
               "lon":-75.97441999999995
            },
            "geoRectangle":{
               "Xmin":-76.19441999999995,
               "Xmax":-75.75441999999995,
               "Ymin":1.1120300000000316,
               "Ymax":1.5520300000000316
            }
         },
         {
            "name":" VALPARAÍSO",
            "value":"860",
            "text":"COLOMBIA, CAQUETÁ,  VALPARAÍSO",
            "code":"CO-18-860",
            "geoPoint":{
               "lat":1.196230000000071,
               "lon":-75.70575999999994
            },
            "geoRectangle":{
               "Xmin":-75.97575999999994,
               "Xmax":-75.43575999999995,
               "Ymin":0.9262300000000709,
               "Ymax":1.466230000000071
            }
         },
         {
            "name":"FLORENCIA",
            "value":"001",
            "text":"COLOMBIA, CAQUETÁ, FLORENCIA",
            "code":"CO-18-001",
            "geoPoint":{
               "lat":1.611720000000048,
               "lon":-75.61049999999994
            },
            "geoRectangle":{
               "Xmin":-75.92149999999995,
               "Xmax":-75.29949999999994,
               "Ymin":1.300720000000048,
               "Ymax":1.922720000000048
            }
         },
         {
            "name":" ALBANIA",
            "value":"029",
            "text":"COLOMBIA, CAQUETÁ,  ALBANIA",
            "code":"CO-18-029",
            "geoPoint":{
               "lat":1.3287300000000641,
               "lon":-75.87818999999996
            },
            "geoRectangle":{
               "Xmin":-76.01318999999997,
               "Xmax":-75.74318999999996,
               "Ymin":1.1937300000000641,
               "Ymax":1.4637300000000641
            }
         },
         {
            "name":"BELÉN DE LOS ANDAQUÍES",
            "value":"094",
            "text":"COLOMBIA, CAQUETÁ, BELÉN DE LOS ANDAQUÍES",
            "code":"CO-18-094",
            "geoPoint":{
               "lat":1.4182800000000384,
               "lon":-75.87753999999995
            },
            "geoRectangle":{
               "Xmin":-75.89253999999995,
               "Xmax":-75.86253999999995,
               "Ymin":1.4032800000000385,
               "Ymax":1.4332800000000383
            }
         },
         {
            "name":"EL DONCELLO",
            "value":"247",
            "text":"COLOMBIA, CAQUETÁ, EL DONCELLO",
            "code":"CO-18-247",
            "geoPoint":{
               "lat":1.6808200000000397,
               "lon":-75.28489999999994
            },
            "geoRectangle":{
               "Xmin":-75.61789999999993,
               "Xmax":-74.95189999999994,
               "Ymin":1.3478200000000398,
               "Ymax":2.01382000000004
            }
         },
         {
            "name":"EL PAUJIL",
            "value":"256",
            "text":"COLOMBIA, CAQUETÁ, EL PAUJIL",
            "code":"CO-18-256",
            "geoPoint":{
               "lat":1.5722200000000726,
               "lon":-75.32617999999997
            },
            "geoRectangle":{
               "Xmin":-75.66917999999997,
               "Xmax":-74.98317999999996,
               "Ymin":1.2292200000000726,
               "Ymax":1.9152200000000725
            }
         },
         {
            "name":"LA MONTAÑITA",
            "value":"410",
            "text":"COLOMBIA, CAQUETÁ, LA MONTAÑITA",
            "code":"CO-18-410",
            "geoPoint":{
               "lat":1.4797700000000304,
               "lon":-75.43626999999998
            },
            "geoRectangle":{
               "Xmin":-75.89126999999998,
               "Xmax":-74.98126999999998,
               "Ymin":1.0247700000000304,
               "Ymax":1.9347700000000305
            }
         },
         {
            "name":"MILÁN",
            "value":"460",
            "text":"COLOMBIA, CAQUETÁ, MILÁN",
            "code":"CO-18-460",
            "geoPoint":{
               "lat":1.2906600000000594,
               "lon":-75.50523999999996
            },
            "geoRectangle":{
               "Xmin":-75.72323999999996,
               "Xmax":-75.28723999999995,
               "Ymin":1.0726600000000595,
               "Ymax":1.5086600000000594
            }
         },
         {
            "name":"MORELIA",
            "value":"479",
            "text":"COLOMBIA, CAQUETÁ, MORELIA",
            "code":"CO-18-479",
            "geoPoint":{
               "lat":1.4874900000000366,
               "lon":-75.72464999999994
            },
            "geoRectangle":{
               "Xmin":-75.86164999999994,
               "Xmax":-75.58764999999994,
               "Ymin":1.3504900000000366,
               "Ymax":1.6244900000000366
            }
         },
         {
            "name":"CURILLO",
            "value":"205",
            "text":"COLOMBIA, CAQUETÁ, CURILLO",
            "code":"CO-18-205",
            "geoPoint":{
               "lat":1.0339100000000485,
               "lon":-75.91923999999995
            },
            "geoRectangle":{
               "Xmin":-76.09623999999995,
               "Xmax":-75.74223999999994,
               "Ymin":0.8569100000000485,
               "Ymax":1.2109100000000486
            }
         },
         {
            "name":"PUERTO RICO",
            "value":"592",
            "text":"COLOMBIA, CAQUETÁ, PUERTO RICO",
            "code":"CO-18-592",
            "geoPoint":{
               "lat":1.9098000000000752,
               "lon":-75.15844999999996
            },
            "geoRectangle":{
               "Xmin":-75.57644999999997,
               "Xmax":-74.74044999999995,
               "Ymin":1.4918000000000753,
               "Ymax":2.3278000000000754
            }
         },
         {
            "name":"CARTAGENA DEL CHAIRÁ",
            "value":"150",
            "text":"COLOMBIA, CAQUETÁ, CARTAGENA DEL CHAIRÁ",
            "code":"CO-18-150",
            "geoPoint":{
               "lat":1.3349700000000553,
               "lon":-74.84275999999994
            },
            "geoRectangle":{
               "Xmin":-75.71775999999994,
               "Xmax":-73.96775999999994,
               "Ymin":0.4599700000000553,
               "Ymax":2.2099700000000553
            }
         },
         {
            "name":"SAN VICENTE DEL CAGUÁN",
            "value":"753",
            "text":"COLOMBIA, CAQUETÁ, SAN VICENTE DEL CAGUÁN",
            "code":"CO-18-753",
            "geoPoint":{
               "lat":2.1130300000000375,
               "lon":-74.77026999999998
            },
            "geoRectangle":{
               "Xmin":-75.85226999999998,
               "Xmax":-73.68826999999999,
               "Ymin":1.0310300000000374,
               "Ymax":3.1950300000000373
            }
         },
         {
            "name":"SOLANO",
            "value":"756",
            "text":"COLOMBIA, CAQUETÁ, SOLANO",
            "code":"CO-18-756",
            "geoPoint":{
               "lat":0.6986500000000433,
               "lon":-75.25428999999997
            },
            "geoRectangle":{
               "Xmin":-76.76228999999996,
               "Xmax":-73.74628999999997,
               "Ymin":-0.8093499999999567,
               "Ymax":2.2066500000000433
            }
         },
         {
            "name":"SOLITA",
            "value":"785",
            "text":"COLOMBIA, CAQUETÁ, SOLITA",
            "code":"CO-18-785",
            "geoPoint":{
               "lat":0.8751200000000381,
               "lon":-75.61958999999996
            },
            "geoRectangle":{
               "Xmin":-75.79758999999996,
               "Xmax":-75.44158999999996,
               "Ymin":0.6971200000000382,
               "Ymax":1.053120000000038
            }
         }
      ]
   },
   {
      "name":"CASANARE",
      "value":"85",
      "text":"COLOMBIA, CASANARE",
      "code":"CO-85",
      "geoPoint":{
         "lat":5.404039426000054,
         "lon":-71.60126416599996
      },
      "geoRectangle":{
         "Xmin":-72.91526416599996,
         "Xmax":-70.28726416599997,
         "Ymin":4.090039426000054,
         "Ymax":6.718039426000054
      },
      "municipality":[
         {
            "name":"YOPAL",
            "value":"001",
            "text":"COLOMBIA, CASANARE, YOPAL",
            "code":"CO-85-001",
            "geoPoint":{
               "lat":5.335280000000068,
               "lon":-72.39371999999997
            },
            "geoRectangle":{
               "Xmin":-72.71971999999997,
               "Xmax":-72.06771999999998,
               "Ymin":5.009280000000069,
               "Ymax":5.661280000000068
            }
         },
         {
            "name":"AGUAZUL",
            "value":"010",
            "text":"COLOMBIA, CASANARE, AGUAZUL",
            "code":"CO-85-010",
            "geoPoint":{
               "lat":5.170090000000073,
               "lon":-72.54954999999995
            },
            "geoRectangle":{
               "Xmin":-72.78954999999995,
               "Xmax":-72.30954999999996,
               "Ymin":4.930090000000073,
               "Ymax":5.410090000000073
            }
         },
         {
            "name":"CHAMEZA",
            "value":"015",
            "text":"COLOMBIA, CASANARE, CHAMEZA",
            "code":"CO-85-015",
            "geoPoint":{
               "lat":5.213260000000048,
               "lon":-72.86893999999995
            },
            "geoRectangle":{
               "Xmin":-72.98993999999995,
               "Xmax":-72.74793999999996,
               "Ymin":5.0922600000000475,
               "Ymax":5.334260000000048
            }
         },
         {
            "name":"LA SALINA",
            "value":"136",
            "text":"COLOMBIA, CASANARE, LA SALINA",
            "code":"CO-85-136",
            "geoPoint":{
               "lat":6.127930000000049,
               "lon":-72.33423999999997
            },
            "geoRectangle":{
               "Xmin":-72.42523999999996,
               "Xmax":-72.24323999999997,
               "Ymin":6.036930000000049,
               "Ymax":6.218930000000049
            }
         },
         {
            "name":"MANÍ",
            "value":"139",
            "text":"COLOMBIA, CASANARE, MANÍ",
            "code":"CO-85-139",
            "geoPoint":{
               "lat":4.823790000000031,
               "lon":-72.28532999999999
            },
            "geoRectangle":{
               "Xmin":-72.68332999999998,
               "Xmax":-71.88732999999999,
               "Ymin":4.425790000000031,
               "Ymax":5.221790000000031
            }
         },
         {
            "name":"MONTERREY",
            "value":"162",
            "text":"COLOMBIA, CASANARE, MONTERREY",
            "code":"CO-85-162",
            "geoPoint":{
               "lat":4.8745400000000245,
               "lon":-72.88789999999995
            },
            "geoRectangle":{
               "Xmin":-73.06089999999995,
               "Xmax":-72.71489999999994,
               "Ymin":4.7015400000000245,
               "Ymax":5.047540000000025
            }
         },
         {
            "name":"NUNCHIA",
            "value":"225",
            "text":"COLOMBIA, CASANARE, NUNCHIA",
            "code":"CO-85-225",
            "geoPoint":{
               "lat":5.666670000000067,
               "lon":-72.19999999999999
            },
            "geoRectangle":{
               "Xmin":-72.24,
               "Xmax":-72.15999999999998,
               "Ymin":5.626670000000067,
               "Ymax":5.706670000000067
            }
         },
         {
            "name":"OROCUÉ",
            "value":"230",
            "text":"COLOMBIA, CASANARE, OROCUÉ",
            "code":"CO-85-230",
            "geoPoint":{
               "lat":4.78982000000002,
               "lon":-71.33863999999994
            },
            "geoRectangle":{
               "Xmin":-71.79063999999994,
               "Xmax":-70.88663999999994,
               "Ymin":4.33782000000002,
               "Ymax":5.24182000000002
            }
         },
         {
            "name":"PORE",
            "value":"263",
            "text":"COLOMBIA, CASANARE, PORE",
            "code":"CO-85-263",
            "geoPoint":{
               "lat":5.7287600000000225,
               "lon":-71.99372999999997
            },
            "geoRectangle":{
               "Xmin":-72.17172999999997,
               "Xmax":-71.81572999999997,
               "Ymin":5.550760000000023,
               "Ymax":5.906760000000022
            }
         },
         {
            "name":"RECETOR",
            "value":"279",
            "text":"COLOMBIA, CASANARE, RECETOR",
            "code":"CO-85-279",
            "geoPoint":{
               "lat":5.228550000000041,
               "lon":-72.76083999999997
            },
            "geoRectangle":{
               "Xmin":-72.84883999999997,
               "Xmax":-72.67283999999998,
               "Ymin":5.140550000000041,
               "Ymax":5.316550000000041
            }
         },
         {
            "name":"SABANALARGA",
            "value":"300",
            "text":"COLOMBIA, CASANARE, SABANALARGA",
            "code":"CO-85-300",
            "geoPoint":{
               "lat":4.854350000000068,
               "lon":-73.04046999999997
            },
            "geoRectangle":{
               "Xmin":-73.18546999999997,
               "Xmax":-72.89546999999997,
               "Ymin":4.709350000000068,
               "Ymax":4.999350000000067
            }
         },
         {
            "name":"SACAMA",
            "value":"315",
            "text":"COLOMBIA, CASANARE, SACAMA",
            "code":"CO-85-315",
            "geoPoint":{
               "lat":6.060830000000067,
               "lon":-72.21249999999998
            },
            "geoRectangle":{
               "Xmin":-73.21249999999998,
               "Xmax":-71.21249999999998,
               "Ymin":5.060830000000067,
               "Ymax":7.060830000000067
            }
         },
         {
            "name":"SAN LUIS DE PALENQUE",
            "value":"325",
            "text":"COLOMBIA, CASANARE, SAN LUIS DE PALENQUE",
            "code":"CO-85-325",
            "geoPoint":{
               "lat":5.422440000000051,
               "lon":-71.73142999999999
            },
            "geoRectangle":{
               "Xmin":-72.17342999999998,
               "Xmax":-71.28943,
               "Ymin":4.980440000000051,
               "Ymax":5.864440000000052
            }
         },
         {
            "name":"TAMARA",
            "value":"400",
            "text":"COLOMBIA, CASANARE, TAMARA",
            "code":"CO-85-400",
            "geoPoint":{
               "lat":5.829720000000066,
               "lon":-72.16332999999997
            },
            "geoRectangle":{
               "Xmin":-72.17832999999997,
               "Xmax":-72.14832999999997,
               "Ymin":5.814720000000066,
               "Ymax":5.844720000000065
            }
         },
         {
            "name":"TAURAMENA",
            "value":"410",
            "text":"COLOMBIA, CASANARE, TAURAMENA",
            "code":"CO-85-410",
            "geoPoint":{
               "lat":5.017770000000041,
               "lon":-72.75026999999994
            },
            "geoRectangle":{
               "Xmin":-73.13926999999994,
               "Xmax":-72.36126999999995,
               "Ymin":4.628770000000041,
               "Ymax":5.406770000000042
            }
         },
         {
            "name":"TRINIDAD",
            "value":"430",
            "text":"COLOMBIA, CASANARE, TRINIDAD",
            "code":"CO-85-430",
            "geoPoint":{
               "lat":5.4082200000000284,
               "lon":-71.66157999999996
            },
            "geoRectangle":{
               "Xmin":-72.09157999999996,
               "Xmax":-71.23157999999995,
               "Ymin":4.978220000000029,
               "Ymax":5.838220000000028
            }
         },
         {
            "name":"VILLANUEVA",
            "value":"440",
            "text":"COLOMBIA, CASANARE, VILLANUEVA",
            "code":"CO-85-440",
            "geoPoint":{
               "lat":4.609850000000051,
               "lon":-72.92752999999993
            },
            "geoRectangle":{
               "Xmin":-73.13552999999993,
               "Xmax":-72.71952999999993,
               "Ymin":4.401850000000051,
               "Ymax":4.8178500000000515
            }
         },
         {
            "name":"HATO COROZAL",
            "value":"125",
            "text":"COLOMBIA, CASANARE, HATO COROZAL",
            "code":"CO-85-125",
            "geoPoint":{
               "lat":6.156300000000044,
               "lon":-71.76688999999993
            },
            "geoRectangle":{
               "Xmin":-72.43288999999993,
               "Xmax":-71.10088999999994,
               "Ymin":5.490300000000044,
               "Ymax":6.822300000000045
            }
         },
         {
            "name":"PAZ DE ARIPORO",
            "value":"250",
            "text":"COLOMBIA, CASANARE, PAZ DE ARIPORO",
            "code":"CO-85-250",
            "geoPoint":{
               "lat":5.880660000000034,
               "lon":-71.89284999999995
            },
            "geoRectangle":{
               "Xmin":-72.62684999999995,
               "Xmax":-71.15884999999996,
               "Ymin":5.146660000000034,
               "Ymax":6.614660000000034
            }
         }
      ]
   },
   {
      "name":"CAUCA",
      "value":"19",
      "text":"COLOMBIA, CAUCA",
      "code":"CO-19",
      "geoPoint":{
         "lat":2.3954891190000467,
         "lon":-76.82991232199998
      },
      "geoRectangle":{
         "Xmin":-78.03391232199998,
         "Xmax":-75.62591232199999,
         "Ymin":1.1914891190000467,
         "Ymax":3.5994891190000464
      },
      "municipality":[
         {
            "name":"EL TAMBO",
            "value":"256",
            "text":"COLOMBIA, CAUCA, EL TAMBO",
            "code":"CO-19-256",
            "geoPoint":{
               "lat":2.4503100000000586,
               "lon":-76.80930999999998
            },
            "geoRectangle":{
               "Xmin":-77.11230999999998,
               "Xmax":-76.50630999999998,
               "Ymin":2.1473100000000587,
               "Ymax":2.7533100000000585
            }
         },
         {
            "name":"POPAYÁN",
            "value":"001",
            "text":"COLOMBIA, CAUCA, POPAYÁN",
            "code":"CO-19-001",
            "geoPoint":{
               "lat":2.444210000000055,
               "lon":-76.61409999999995
            },
            "geoRectangle":{
               "Xmin":-76.77709999999995,
               "Xmax":-76.45109999999995,
               "Ymin":2.2812100000000552,
               "Ymax":2.607210000000055
            }
         },
         {
            "name":"ALMAGUER",
            "value":"022",
            "text":"COLOMBIA, CAUCA, ALMAGUER",
            "code":"CO-19-022",
            "geoPoint":{
               "lat":1.9135500000000434,
               "lon":-76.85586999999998
            },
            "geoRectangle":{
               "Xmin":-76.94686999999998,
               "Xmax":-76.76486999999999,
               "Ymin":1.8225500000000434,
               "Ymax":2.0045500000000436
            }
         },
         {
            "name":"ARGELIA",
            "value":"050",
            "text":"COLOMBIA, CAUCA, ARGELIA",
            "code":"CO-19-050",
            "geoPoint":{
               "lat":2.4662400000000275,
               "lon":-77.23783999999995
            },
            "geoRectangle":{
               "Xmin":-77.38783999999995,
               "Xmax":-77.08783999999994,
               "Ymin":2.3162400000000276,
               "Ymax":2.6162400000000274
            }
         },
         {
            "name":"BALBOA",
            "value":"075",
            "text":"COLOMBIA, CAUCA, BALBOA",
            "code":"CO-19-075",
            "geoPoint":{
               "lat":2.0407300000000532,
               "lon":-77.21606999999995
            },
            "geoRectangle":{
               "Xmin":-77.34006999999994,
               "Xmax":-77.09206999999995,
               "Ymin":1.9167300000000531,
               "Ymax":2.1647300000000533
            }
         },
         {
            "name":"BOLÍVAR",
            "value":"100",
            "text":"COLOMBIA, CAUCA, BOLÍVAR",
            "code":"CO-19-100",
            "geoPoint":{
               "lat":1.8346400000000358,
               "lon":-76.96653999999995
            },
            "geoRectangle":{
               "Xmin":-77.15153999999995,
               "Xmax":-76.78153999999995,
               "Ymin":1.6496400000000357,
               "Ymax":2.019640000000036
            }
         },
         {
            "name":"BUENOS AIRES",
            "value":"110",
            "text":"COLOMBIA, CAUCA, BUENOS AIRES",
            "code":"CO-19-110",
            "geoPoint":{
               "lat":3.0163100000000327,
               "lon":-76.64235999999994
            },
            "geoRectangle":{
               "Xmin":-76.77735999999994,
               "Xmax":-76.50735999999993,
               "Ymin":2.881310000000033,
               "Ymax":3.1513100000000325
            }
         },
         {
            "name":"CAJIBÍO",
            "value":"130",
            "text":"COLOMBIA, CAUCA, CAJIBÍO",
            "code":"CO-19-130",
            "geoPoint":{
               "lat":2.6219500000000266,
               "lon":-76.57040999999998
            },
            "geoRectangle":{
               "Xmin":-76.72140999999998,
               "Xmax":-76.41940999999998,
               "Ymin":2.470950000000027,
               "Ymax":2.7729500000000264
            }
         },
         {
            "name":"CALDONO",
            "value":"137",
            "text":"COLOMBIA, CAUCA, CALDONO",
            "code":"CO-19-137",
            "geoPoint":{
               "lat":2.7975000000000705,
               "lon":-76.48235999999997
            },
            "geoRectangle":{
               "Xmin":-76.60435999999997,
               "Xmax":-76.36035999999997,
               "Ymin":2.6755000000000706,
               "Ymax":2.9195000000000704
            }
         },
         {
            "name":"CALOTO",
            "value":"142",
            "text":"COLOMBIA, CAUCA, CALOTO",
            "code":"CO-19-142",
            "geoPoint":{
               "lat":3.0354200000000446,
               "lon":-76.40789999999998
            },
            "geoRectangle":{
               "Xmin":-76.52689999999998,
               "Xmax":-76.28889999999998,
               "Ymin":2.9164200000000444,
               "Ymax":3.154420000000045
            }
         },
         {
            "name":"CORINTO",
            "value":"212",
            "text":"COLOMBIA, CAUCA, CORINTO",
            "code":"CO-19-212",
            "geoPoint":{
               "lat":3.1748000000000616,
               "lon":-76.25967999999995
            },
            "geoRectangle":{
               "Xmin":-76.36767999999995,
               "Xmax":-76.15167999999994,
               "Ymin":3.0668000000000615,
               "Ymax":3.2828000000000617
            }
         },
         {
            "name":"FLORENCIA",
            "value":"290",
            "text":"COLOMBIA, CAUCA, FLORENCIA",
            "code":"CO-19-290",
            "geoPoint":{
               "lat":1.6823000000000548,
               "lon":-77.07212999999996
            },
            "geoRectangle":{
               "Xmin":-77.12312999999996,
               "Xmax":-77.02112999999996,
               "Ymin":1.6313000000000548,
               "Ymax":1.7333000000000547
            }
         },
         {
            "name":"GUACHENÉ",
            "value":"300",
            "text":"COLOMBIA, CAUCA, GUACHENÉ",
            "code":"CO-19-300",
            "geoPoint":{
               "lat":3.133030000000076,
               "lon":-76.39173999999997
            },
            "geoRectangle":{
               "Xmin":-76.45073999999997,
               "Xmax":-76.33273999999997,
               "Ymin":3.074030000000076,
               "Ymax":3.1920300000000763
            }
         },
         {
            "name":"INZA",
            "value":"355",
            "text":"COLOMBIA, CAUCA, INZA",
            "code":"CO-19-355",
            "geoPoint":{
               "lat":2.5496100000000297,
               "lon":-76.06355999999994
            },
            "geoRectangle":{
               "Xmin":-76.24655999999995,
               "Xmax":-75.88055999999993,
               "Ymin":2.36661000000003,
               "Ymax":2.7326100000000295
            }
         },
         {
            "name":"JAMBALÓ",
            "value":"364",
            "text":"COLOMBIA, CAUCA, JAMBALÓ",
            "code":"CO-19-364",
            "geoPoint":{
               "lat":2.776520000000062,
               "lon":-76.32504999999998
            },
            "geoRectangle":{
               "Xmin":-76.42604999999998,
               "Xmax":-76.22404999999998,
               "Ymin":2.675520000000062,
               "Ymax":2.877520000000062
            }
         },
         {
            "name":"LA SIERRA",
            "value":"392",
            "text":"COLOMBIA, CAUCA, LA SIERRA",
            "code":"CO-19-392",
            "geoPoint":{
               "lat":2.1776700000000346,
               "lon":-76.76571999999999
            },
            "geoRectangle":{
               "Xmin":-76.85671999999998,
               "Xmax":-76.67472,
               "Ymin":2.0866700000000344,
               "Ymax":2.268670000000035
            }
         },
         {
            "name":"LA VEGA",
            "value":"397",
            "text":"COLOMBIA, CAUCA, LA VEGA",
            "code":"CO-19-397",
            "geoPoint":{
               "lat":2.0015300000000593,
               "lon":-76.77853999999996
            },
            "geoRectangle":{
               "Xmin":-76.91653999999997,
               "Xmax":-76.64053999999996,
               "Ymin":1.8635300000000594,
               "Ymax":2.139530000000059
            }
         },
         {
            "name":"MERCADERES",
            "value":"450",
            "text":"COLOMBIA, CAUCA, MERCADERES",
            "code":"CO-19-450",
            "geoPoint":{
               "lat":1.7974500000000262,
               "lon":-77.16417999999999
            },
            "geoRectangle":{
               "Xmin":-77.31718,
               "Xmax":-77.01117999999998,
               "Ymin":1.6444500000000262,
               "Ymax":1.9504500000000262
            }
         },
         {
            "name":"MIRANDA",
            "value":"455",
            "text":"COLOMBIA, CAUCA, MIRANDA",
            "code":"CO-19-455",
            "geoPoint":{
               "lat":3.249940000000038,
               "lon":-76.22844999999995
            },
            "geoRectangle":{
               "Xmin":-76.34044999999995,
               "Xmax":-76.11644999999996,
               "Ymin":3.1379400000000377,
               "Ymax":3.361940000000038
            }
         },
         {
            "name":"MORALES",
            "value":"473",
            "text":"COLOMBIA, CAUCA, MORALES",
            "code":"CO-19-473",
            "geoPoint":{
               "lat":2.7549300000000585,
               "lon":-76.62898999999999
            },
            "geoRectangle":{
               "Xmin":-76.78199,
               "Xmax":-76.47598999999998,
               "Ymin":2.6019300000000585,
               "Ymax":2.9079300000000585
            }
         },
         {
            "name":"PADILLA",
            "value":"513",
            "text":"COLOMBIA, CAUCA, PADILLA",
            "code":"CO-19-513",
            "geoPoint":{
               "lat":3.2224800000000755,
               "lon":-76.31311999999997
            },
            "geoRectangle":{
               "Xmin":-76.36311999999997,
               "Xmax":-76.26311999999997,
               "Ymin":3.1724800000000757,
               "Ymax":3.2724800000000753
            }
         },
         {
            "name":"PÁEZ",
            "value":"517",
            "text":"COLOMBIA, CAUCA, PÁEZ",
            "code":"CO-19-517",
            "geoPoint":{
               "lat":2.6506500000000415,
               "lon":-75.97469999999998
            },
            "geoRectangle":{
               "Xmin":-76.25669999999998,
               "Xmax":-75.69269999999999,
               "Ymin":2.3686500000000414,
               "Ymax":2.9326500000000415
            }
         },
         {
            "name":"PATÍA",
            "value":"532",
            "text":"COLOMBIA, CAUCA, PATÍA",
            "code":"CO-19-532",
            "geoPoint":{
               "lat":2.1254800000000387,
               "lon":-76.97585999999995
            },
            "geoRectangle":{
               "Xmin":-77.15085999999995,
               "Xmax":-76.80085999999996,
               "Ymin":1.9504800000000386,
               "Ymax":2.3004800000000385
            }
         },
         {
            "name":"PIAMONTE",
            "value":"533",
            "text":"COLOMBIA, CAUCA, PIAMONTE",
            "code":"CO-19-533",
            "geoPoint":{
               "lat":1.1182400000000712,
               "lon":-76.32614999999998
            },
            "geoRectangle":{
               "Xmin":-76.60314999999999,
               "Xmax":-76.04914999999998,
               "Ymin":0.8412400000000712,
               "Ymax":1.3952400000000713
            }
         },
         {
            "name":"PIENDAMÓ",
            "value":"548",
            "text":"COLOMBIA, CAUCA, PIENDAMÓ",
            "code":"CO-19-548",
            "geoPoint":{
               "lat":2.63969000000003,
               "lon":-76.52889999999996
            },
            "geoRectangle":{
               "Xmin":-76.63289999999996,
               "Xmax":-76.42489999999997,
               "Ymin":2.53569000000003,
               "Ymax":2.74369000000003
            }
         },
         {
            "name":"PUERTO TEJADA",
            "value":"573",
            "text":"COLOMBIA, CAUCA, PUERTO TEJADA",
            "code":"CO-19-573",
            "geoPoint":{
               "lat":3.230090000000075,
               "lon":-76.41597999999993
            },
            "geoRectangle":{
               "Xmin":-76.48297999999993,
               "Xmax":-76.34897999999994,
               "Ymin":3.163090000000075,
               "Ymax":3.2970900000000753
            }
         },
         {
            "name":"PURACÉ (COCONUCO)",
            "value":"585",
            "text":"COLOMBIA, CAUCA, PURACÉ (COCONUCO)",
            "code":"CO-19-585",
            "geoPoint":{
               "lat":2.3425300000000675,
               "lon":-76.49618999999996
            },
            "geoRectangle":{
               "Xmin":-76.50618999999996,
               "Xmax":-76.48618999999995,
               "Ymin":2.3325300000000677,
               "Ymax":2.3525300000000673
            }
         },
         {
            "name":"ROSAS",
            "value":"622",
            "text":"COLOMBIA, CAUCA, ROSAS",
            "code":"CO-19-622",
            "geoPoint":{
               "lat":2.2612100000000623,
               "lon":-76.74025999999998
            },
            "geoRectangle":{
               "Xmin":-76.82225999999997,
               "Xmax":-76.65825999999998,
               "Ymin":2.1792100000000625,
               "Ymax":2.343210000000062
            }
         },
         {
            "name":"SAN SEBASTIAN",
            "value":"693",
            "text":"COLOMBIA, CAUCA, SAN SEBASTIAN",
            "code":"CO-19-693",
            "geoPoint":{
               "lat":1.8388000000000488,
               "lon":-76.76981999999998
            },
            "geoRectangle":{
               "Xmin":-76.91081999999999,
               "Xmax":-76.62881999999998,
               "Ymin":1.6978000000000488,
               "Ymax":1.9798000000000489
            }
         },
         {
            "name":"SANTANDER DE QUILICHAO",
            "value":"698",
            "text":"COLOMBIA, CAUCA, SANTANDER DE QUILICHAO",
            "code":"CO-19-698",
            "geoPoint":{
               "lat":3.011120000000062,
               "lon":-76.48438999999996
            },
            "geoRectangle":{
               "Xmin":-76.62138999999996,
               "Xmax":-76.34738999999996,
               "Ymin":2.874120000000062,
               "Ymax":3.148120000000062
            }
         },
         {
            "name":"SANTA ROSA",
            "value":"701",
            "text":"COLOMBIA, CAUCA, SANTA ROSA",
            "code":"CO-19-701",
            "geoPoint":{
               "lat":1.7446300000000292,
               "lon":-76.58908999999994
            },
            "geoRectangle":{
               "Xmin":-76.93708999999994,
               "Xmax":-76.24108999999994,
               "Ymin":1.3966300000000291,
               "Ymax":2.092630000000029
            }
         },
         {
            "name":"SILVIA",
            "value":"743",
            "text":"COLOMBIA, CAUCA, SILVIA",
            "code":"CO-19-743",
            "geoPoint":{
               "lat":2.611190000000022,
               "lon":-76.38158999999996
            },
            "geoRectangle":{
               "Xmin":-76.53558999999996,
               "Xmax":-76.22758999999996,
               "Ymin":2.457190000000022,
               "Ymax":2.765190000000022
            }
         },
         {
            "name":"SOTARÁ (Paispamba)",
            "value":"760",
            "text":"COLOMBIA, CAUCA, SOTARÁ (Paispamba)",
            "code":"CO-19-760",
            "geoPoint":{
               "lat":2.4170650483188467,
               "lon":-76.62357490352522
            },
            "geoRectangle":{
               "Xmin":-76.62457490352523,
               "Xmax":-76.62257490352522,
               "Ymin":2.416065048318847,
               "Ymax":2.4180650483188466
            }
         },
         {
            "name":"SUÁREZ",
            "value":"780",
            "text":"COLOMBIA, CAUCA, SUÁREZ",
            "code":"CO-19-780",
            "geoPoint":{
               "lat":2.9534400000000574,
               "lon":-76.69266999999996
            },
            "geoRectangle":{
               "Xmin":-76.81866999999997,
               "Xmax":-76.56666999999996,
               "Ymin":2.8274400000000575,
               "Ymax":3.0794400000000572
            }
         },
         {
            "name":"SUCRE",
            "value":"785",
            "text":"COLOMBIA, CAUCA, SUCRE",
            "code":"CO-19-785",
            "geoPoint":{
               "lat":2.0383600000000683,
               "lon":-76.92578999999995
            },
            "geoRectangle":{
               "Xmin":-77.00678999999995,
               "Xmax":-76.84478999999995,
               "Ymin":1.9573600000000684,
               "Ymax":2.1193600000000683
            }
         },
         {
            "name":"TIMBÍO",
            "value":"807",
            "text":"COLOMBIA, CAUCA, TIMBÍO",
            "code":"CO-19-807",
            "geoPoint":{
               "lat":2.3518400000000383,
               "lon":-76.68160999999998
            },
            "geoRectangle":{
               "Xmin":-76.78260999999998,
               "Xmax":-76.58060999999998,
               "Ymin":2.2508400000000384,
               "Ymax":2.4528400000000383
            }
         },
         {
            "name":"TORIBÍO",
            "value":"821",
            "text":"COLOMBIA, CAUCA, TORIBÍO",
            "code":"CO-19-821",
            "geoPoint":{
               "lat":2.954810000000066,
               "lon":-76.26838999999995
            },
            "geoRectangle":{
               "Xmin":-76.28338999999995,
               "Xmax":-76.25338999999995,
               "Ymin":2.9398100000000658,
               "Ymax":2.969810000000066
            }
         },
         {
            "name":"TOTORÓ",
            "value":"824",
            "text":"COLOMBIA, CAUCA, TOTORÓ",
            "code":"CO-19-824",
            "geoPoint":{
               "lat":2.5117000000000758,
               "lon":-76.40210999999994
            },
            "geoRectangle":{
               "Xmin":-76.55210999999994,
               "Xmax":-76.25210999999993,
               "Ymin":2.361700000000076,
               "Ymax":2.6617000000000757
            }
         },
         {
            "name":"VILLA RICA",
            "value":"845",
            "text":"COLOMBIA, CAUCA, VILLA RICA",
            "code":"CO-19-845",
            "geoPoint":{
               "lat":3.1748600000000238,
               "lon":-76.46129999999994
            },
            "geoRectangle":{
               "Xmin":-76.52129999999994,
               "Xmax":-76.40129999999994,
               "Ymin":3.1148600000000237,
               "Ymax":3.234860000000024
            }
         },
         {
            "name":"GUAPI",
            "value":"318",
            "text":"COLOMBIA, CAUCA, GUAPI",
            "code":"CO-19-318",
            "geoPoint":{
               "lat":2.571140000000071,
               "lon":-77.88769999999994
            },
            "geoRectangle":{
               "Xmin":-78.32669999999993,
               "Xmax":-77.44869999999995,
               "Ymin":2.1321400000000708,
               "Ymax":3.010140000000071
            }
         },
         {
            "name":"LÓPEZ",
            "value":"418",
            "text":"COLOMBIA, CAUCA, LÓPEZ",
            "code":"CO-19-418",
            "geoPoint":{
               "lat":2.84662000000003,
               "lon":-77.24721999999997
            },
            "geoRectangle":{
               "Xmin":-77.62721999999997,
               "Xmax":-76.86721999999997,
               "Ymin":2.46662000000003,
               "Ymax":3.22662000000003
            }
         },
         {
            "name":"TIMBIQUÍ",
            "value":"809",
            "text":"COLOMBIA, CAUCA, TIMBIQUÍ",
            "code":"CO-19-809",
            "geoPoint":{
               "lat":2.774970000000053,
               "lon":-77.66504999999995
            },
            "geoRectangle":{
               "Xmin":-77.95204999999996,
               "Xmax":-77.37804999999994,
               "Ymin":2.487970000000053,
               "Ymax":3.061970000000053
            }
         }
      ]
   },
   {
      "name":"CESAR",
      "value":"20",
      "text":"COLOMBIA, CESAR",
      "code":"CO-20",
      "geoPoint":{
         "lat":9.547851846000071,
         "lon":-73.51372619799997
      },
      "geoRectangle":{
         "Xmin":-74.62572619799997,
         "Xmax":-72.40172619799998,
         "Ymin":8.435851846000071,
         "Ymax":10.659851846000072
      },
      "municipality":[
         {
            "name":"VALLEDUPAR",
            "value":"001",
            "text":"COLOMBIA, CESAR, VALLEDUPAR",
            "code":"CO-20-001",
            "geoPoint":{
               "lat":10.464780000000076,
               "lon":-73.26060999999999
            },
            "geoRectangle":{
               "Xmin":-73.71360999999999,
               "Xmax":-72.80760999999998,
               "Ymin":10.011780000000076,
               "Ymax":10.917780000000075
            }
         },
         {
            "name":"AGUSTÍN CODAZZI",
            "value":"013",
            "text":"COLOMBIA, CESAR, AGUSTÍN CODAZZI",
            "code":"CO-20-013",
            "geoPoint":{
               "lat":10.036200000000065,
               "lon":-73.23651999999998
            },
            "geoRectangle":{
               "Xmin":-73.55451999999998,
               "Xmax":-72.91851999999999,
               "Ymin":9.718200000000065,
               "Ymax":10.354200000000064
            }
         },
         {
            "name":"BECERRILL",
            "value":"045",
            "text":"COLOMBIA, CESAR, BECERRILL",
            "code":"CO-20-045",
            "geoPoint":{
               "lat":9.702930000000038,
               "lon":-73.27863999999994
            },
            "geoRectangle":{
               "Xmin":-73.54263999999993,
               "Xmax":-73.01463999999994,
               "Ymin":9.438930000000038,
               "Ymax":9.966930000000037
            }
         },
         {
            "name":"LA PAZ",
            "value":"621",
            "text":"COLOMBIA, CESAR, LA PAZ",
            "code":"CO-20-621",
            "geoPoint":{
               "lat":10.390590000000032,
               "lon":-73.16940999999997
            },
            "geoRectangle":{
               "Xmin":-73.53140999999997,
               "Xmax":-72.80740999999998,
               "Ymin":10.028590000000031,
               "Ymax":10.752590000000032
            }
         },
         {
            "name":" AGUACHICA",
            "value":"011",
            "text":"COLOMBIA, CESAR,  AGUACHICA",
            "code":"CO-20-011",
            "geoPoint":{
               "lat":8.311010000000067,
               "lon":-73.62340999999998
            },
            "geoRectangle":{
               "Xmin":-73.82940999999998,
               "Xmax":-73.41740999999998,
               "Ymin":8.105010000000068,
               "Ymax":8.517010000000067
            }
         },
         {
            "name":"ASTREA",
            "value":"032",
            "text":"COLOMBIA, CESAR, ASTREA",
            "code":"CO-20-032",
            "geoPoint":{
               "lat":9.496870000000058,
               "lon":-73.97491999999994
            },
            "geoRectangle":{
               "Xmin":-74.11691999999994,
               "Xmax":-73.83291999999994,
               "Ymin":9.354870000000059,
               "Ymax":9.638870000000058
            }
         },
         {
            "name":"BOSCONIA",
            "value":"060",
            "text":"COLOMBIA, CESAR, BOSCONIA",
            "code":"CO-20-060",
            "geoPoint":{
               "lat":9.974110000000053,
               "lon":-73.88699999999994
            },
            "geoRectangle":{
               "Xmin":-74.03699999999995,
               "Xmax":-73.73699999999994,
               "Ymin":9.824110000000053,
               "Ymax":10.124110000000053
            }
         },
         {
            "name":"CHIMICHAGUA",
            "value":"175",
            "text":"COLOMBIA, CESAR, CHIMICHAGUA",
            "code":"CO-20-175",
            "geoPoint":{
               "lat":9.258070000000032,
               "lon":-73.81335999999999
            },
            "geoRectangle":{
               "Xmin":-74.09736,
               "Xmax":-73.52935999999998,
               "Ymin":8.974070000000031,
               "Ymax":9.542070000000033
            }
         },
         {
            "name":"CHIRIGUANÁ",
            "value":"178",
            "text":"COLOMBIA, CESAR, CHIRIGUANÁ",
            "code":"CO-20-178",
            "geoPoint":{
               "lat":9.36193000000003,
               "lon":-73.60228999999998
            },
            "geoRectangle":{
               "Xmin":-73.84528999999998,
               "Xmax":-73.35928999999999,
               "Ymin":9.118930000000029,
               "Ymax":9.60493000000003
            }
         },
         {
            "name":"CURUMANÍ",
            "value":"228",
            "text":"COLOMBIA, CESAR, CURUMANÍ",
            "code":"CO-20-228",
            "geoPoint":{
               "lat":9.201510000000042,
               "lon":-73.54112999999995
            },
            "geoRectangle":{
               "Xmin":-73.73812999999996,
               "Xmax":-73.34412999999995,
               "Ymin":9.004510000000042,
               "Ymax":9.39851000000004
            }
         },
         {
            "name":"EL COPEY",
            "value":"238",
            "text":"COLOMBIA, CESAR, EL COPEY",
            "code":"CO-20-238",
            "geoPoint":{
               "lat":10.152400000000057,
               "lon":-73.95467999999994
            },
            "geoRectangle":{
               "Xmin":-74.14467999999994,
               "Xmax":-73.76467999999994,
               "Ymin":9.962400000000057,
               "Ymax":10.342400000000056
            }
         },
         {
            "name":"EL PASO",
            "value":"250",
            "text":"COLOMBIA, CESAR, EL PASO",
            "code":"CO-20-250",
            "geoPoint":{
               "lat":9.662400000000048,
               "lon":-73.74673999999999
            },
            "geoRectangle":{
               "Xmin":-73.93274,
               "Xmax":-73.56073999999998,
               "Ymin":9.476400000000048,
               "Ymax":9.848400000000048
            }
         },
         {
            "name":"GAMARRA",
            "value":"295",
            "text":"COLOMBIA, CESAR, GAMARRA",
            "code":"CO-20-295",
            "geoPoint":{
               "lat":8.323630000000037,
               "lon":-73.74244999999996
            },
            "geoRectangle":{
               "Xmin":-73.86844999999997,
               "Xmax":-73.61644999999996,
               "Ymin":8.197630000000037,
               "Ymax":8.449630000000036
            }
         },
         {
            "name":"LA JAGUA DE IBIRICO",
            "value":"400",
            "text":"COLOMBIA, CESAR, LA JAGUA DE IBIRICO",
            "code":"CO-20-400",
            "geoPoint":{
               "lat":9.561460000000068,
               "lon":-73.33319999999998
            },
            "geoRectangle":{
               "Xmin":-73.53119999999997,
               "Xmax":-73.13519999999998,
               "Ymin":9.363460000000067,
               "Ymax":9.759460000000068
            }
         },
         {
            "name":"MANAURE BALCÓN DEL CESAR",
            "value":"443",
            "text":"COLOMBIA, CESAR, MANAURE BALCÓN DEL CESAR",
            "code":"CO-20-443",
            "geoPoint":{
               "lat":10.392780000000073,
               "lon":-73.03249999999997
            },
            "geoRectangle":{
               "Xmin":-73.04749999999997,
               "Xmax":-73.01749999999997,
               "Ymin":10.377780000000072,
               "Ymax":10.407780000000074
            }
         },
         {
            "name":"PAILITAS",
            "value":"517",
            "text":"COLOMBIA, CESAR, PAILITAS",
            "code":"CO-20-517",
            "geoPoint":{
               "lat":8.956410000000062,
               "lon":-73.62157999999994
            },
            "geoRectangle":{
               "Xmin":-73.74857999999993,
               "Xmax":-73.49457999999994,
               "Ymin":8.829410000000061,
               "Ymax":9.083410000000063
            }
         },
         {
            "name":"PELAYA",
            "value":"550",
            "text":"COLOMBIA, CESAR, PELAYA",
            "code":"CO-20-550",
            "geoPoint":{
               "lat":8.690380000000062,
               "lon":-73.66382999999996
            },
            "geoRectangle":{
               "Xmin":-73.81982999999997,
               "Xmax":-73.50782999999996,
               "Ymin":8.534380000000061,
               "Ymax":8.846380000000062
            }
         },
         {
            "name":"RÍO DE ORO",
            "value":"614",
            "text":"COLOMBIA, CESAR, RÍO DE ORO",
            "code":"CO-20-614",
            "geoPoint":{
               "lat":8.292800000000057,
               "lon":-73.38549999999998
            },
            "geoRectangle":{
               "Xmin":-73.54049999999998,
               "Xmax":-73.23049999999998,
               "Ymin":8.137800000000057,
               "Ymax":8.447800000000056
            }
         },
         {
            "name":"SAN ALBERTO",
            "value":"710",
            "text":"COLOMBIA, CESAR, SAN ALBERTO",
            "code":"CO-20-710",
            "geoPoint":{
               "lat":7.760710000000074,
               "lon":-73.38921999999997
            },
            "geoRectangle":{
               "Xmin":-73.57321999999996,
               "Xmax":-73.20521999999997,
               "Ymin":7.576710000000074,
               "Ymax":7.944710000000074
            }
         },
         {
            "name":"SAN DIEGO",
            "value":"750",
            "text":"COLOMBIA, CESAR, SAN DIEGO",
            "code":"CO-20-750",
            "geoPoint":{
               "lat":10.33391000000006,
               "lon":-73.17940999999996
            },
            "geoRectangle":{
               "Xmin":-73.41640999999996,
               "Xmax":-72.94240999999997,
               "Ymin":10.09691000000006,
               "Ymax":10.57091000000006
            }
         },
         {
            "name":"SAN MARTÍN",
            "value":"770",
            "text":"COLOMBIA, CESAR, SAN MARTÍN",
            "code":"CO-20-770",
            "geoPoint":{
               "lat":8.002190000000041,
               "lon":-73.51163999999994
            },
            "geoRectangle":{
               "Xmin":-73.71763999999995,
               "Xmax":-73.30563999999994,
               "Ymin":7.796190000000041,
               "Ymax":8.208190000000041
            }
         },
         {
            "name":"TAMALAMEQUE",
            "value":"787",
            "text":"COLOMBIA, CESAR, TAMALAMEQUE",
            "code":"CO-20-787",
            "geoPoint":{
               "lat":8.86009000000007,
               "lon":-73.81317999999999
            },
            "geoRectangle":{
               "Xmin":-73.94417999999999,
               "Xmax":-73.68217999999999,
               "Ymin":8.72909000000007,
               "Ymax":8.99109000000007
            }
         },
         {
            "name":"PUEBLO BELLO",
            "value":"570",
            "text":"COLOMBIA, CESAR, PUEBLO BELLO",
            "code":"CO-20-570",
            "geoPoint":{
               "lat":10.416980000000024,
               "lon":-73.58435999999995
            },
            "geoRectangle":{
               "Xmin":-73.77635999999994,
               "Xmax":-73.39235999999995,
               "Ymin":10.224980000000023,
               "Ymax":10.608980000000024
            }
         },
         {
            "name":"GONZALEZ",
            "value":"310",
            "text":"COLOMBIA, CESAR, GONZALEZ",
            "code":"CO-20-310",
            "geoPoint":{
               "lat":10.466030000000046,
               "lon":-73.24621999999994
            },
            "geoRectangle":{
               "Xmin":-73.25121999999993,
               "Xmax":-73.24121999999994,
               "Ymin":10.461030000000045,
               "Ymax":10.471030000000047
            }
         },
         {
            "name":"LA GLORIA",
            "value":"383",
            "text":"COLOMBIA, CESAR, LA GLORIA",
            "code":"CO-20-383",
            "geoPoint":{
               "lat":8.61868000000004,
               "lon":-73.80264999999997
            },
            "geoRectangle":{
               "Xmin":-73.81764999999997,
               "Xmax":-73.78764999999997,
               "Ymin":8.60368000000004,
               "Ymax":8.63368000000004
            }
         }
      ]
   },
   {
      "name":"CHOCÓ",
      "value":"27",
      "text":"COLOMBIA, CHOCÓ",
      "code":"CO-27",
      "geoPoint":{
         "lat":5.950973989000033,
         "lon":-76.94501833699996
      },
      "geoRectangle":{
         "Xmin":-78.58301833699997,
         "Xmax":-75.30701833699996,
         "Ymin":4.312973989000033,
         "Ymax":7.588973989000033
      },
      "municipality":[
         {
            "name":"RIOSUCIO",
            "value":"615",
            "text":"COLOMBIA, CHOCÓ, RIOSUCIO",
            "code":"CO-27-615",
            "geoPoint":{
               "lat":7.438190000000077,
               "lon":-77.11432999999994
            },
            "geoRectangle":{
               "Xmin":-77.73232999999993,
               "Xmax":-76.49632999999994,
               "Ymin":6.820190000000077,
               "Ymax":8.056190000000077
            }
         },
         {
            "name":"ACANDÍ",
            "value":"006",
            "text":"COLOMBIA, CHOCÓ, ACANDÍ",
            "code":"CO-27-006",
            "geoPoint":{
               "lat":8.50941000000006,
               "lon":-77.28090999999995
            },
            "geoRectangle":{
               "Xmin":-77.47690999999995,
               "Xmax":-77.08490999999995,
               "Ymin":8.31341000000006,
               "Ymax":8.70541000000006
            }
         },
         {
            "name":"ALTO BAUDÓ (Pie de Pato)",
            "value":"025",
            "text":"COLOMBIA, CHOCÓ, ALTO BAUDÓ (Pie de Pato)",
            "code":"CO-27-025",
            "geoPoint":{
               "lat":4.95192000000003,
               "lon":-77.40303999999998
            },
            "geoRectangle":{
               "Xmin":-97.40303999999998,
               "Xmax":-57.403039999999976,
               "Ymin":-15.04807999999997,
               "Ymax":24.95192000000003
            }
         },
         {
            "name":"ATRATO (Yuto)",
            "value":"050",
            "text":"COLOMBIA, CHOCÓ, ATRATO (Yuto)",
            "code":"CO-27-050",
            "geoPoint":{
               "lat":5.53394000000003,
               "lon":-76.63305999999994
            },
            "geoRectangle":{
               "Xmin":-76.64305999999995,
               "Xmax":-76.62305999999994,
               "Ymin":5.52394000000003,
               "Ymax":5.543940000000029
            }
         },
         {
            "name":"BAGADÓ",
            "value":"073",
            "text":"COLOMBIA, CHOCÓ, BAGADÓ",
            "code":"CO-27-073",
            "geoPoint":{
               "lat":5.411000000000058,
               "lon":-76.41593999999998
            },
            "geoRectangle":{
               "Xmin":-76.60993999999998,
               "Xmax":-76.22193999999998,
               "Ymin":5.217000000000058,
               "Ymax":5.605000000000058
            }
         },
         {
            "name":"BAHÍA SOLANO (Mutis)",
            "value":"075",
            "text":"COLOMBIA, CHOCÓ, BAHÍA SOLANO (Mutis)",
            "code":"CO-27-075",
            "geoPoint":{
               "lat":6.2032500000000255,
               "lon":-77.39614999999998
            },
            "geoRectangle":{
               "Xmin":-77.40314999999998,
               "Xmax":-77.38914999999997,
               "Ymin":6.196250000000026,
               "Ymax":6.210250000000025
            }
         },
         {
            "name":"BAJO BAUDÓ (Pizarro)",
            "value":"077",
            "text":"COLOMBIA, CHOCÓ, BAJO BAUDÓ (Pizarro)",
            "code":"CO-27-077",
            "geoPoint":{
               "lat":4.899460000000033,
               "lon":-77.24228999999997
            },
            "geoRectangle":{
               "Xmin":-77.24728999999996,
               "Xmax":-77.23728999999997,
               "Ymin":4.894460000000033,
               "Ymax":4.904460000000033
            }
         },
         {
            "name":"BOJAYÁ (Bellavista)",
            "value":"099",
            "text":"COLOMBIA, CHOCÓ, BOJAYÁ (Bellavista)",
            "code":"CO-27-099",
            "geoPoint":{
               "lat":6.5565200000000345,
               "lon":-76.88429999999994
            },
            "geoRectangle":{
               "Xmin":-76.89429999999994,
               "Xmax":-76.87429999999993,
               "Ymin":6.546520000000035,
               "Ymax":6.566520000000034
            }
         },
         {
            "name":"EL CANTÓN DEL SAN PABLO (Managrú)",
            "value":"135",
            "text":"COLOMBIA, CHOCÓ, EL CANTÓN DEL SAN PABLO (Managrú)",
            "code":"CO-27-135",
            "geoPoint":{
               "lat":5.336530000000039,
               "lon":-76.72755999999998
            },
            "geoRectangle":{
               "Xmin":-76.73755999999999,
               "Xmax":-76.71755999999998,
               "Ymin":5.326530000000039,
               "Ymax":5.346530000000039
            }
         },
         {
            "name":"CARMEN DEL DARIÉN  (Curbaradó)",
            "value":"150",
            "text":"COLOMBIA, CHOCÓ, CARMEN DEL DARIÉN  (Curbaradó)",
            "code":"CO-27-150",
            "geoPoint":{
               "lat":7.183330000000069,
               "lon":-76.88332999999994
            },
            "geoRectangle":{
               "Xmin":-77.88332999999994,
               "Xmax":-75.88332999999994,
               "Ymin":6.183330000000069,
               "Ymax":8.183330000000069
            }
         },
         {
            "name":"CÉRTEGUI",
            "value":"160",
            "text":"COLOMBIA, CHOCÓ, CÉRTEGUI",
            "code":"CO-27-160",
            "geoPoint":{
               "lat":5.373730000000023,
               "lon":-76.61308999999994
            },
            "geoRectangle":{
               "Xmin":-76.73108999999994,
               "Xmax":-76.49508999999995,
               "Ymin":5.255730000000023,
               "Ymax":5.4917300000000235
            }
         },
         {
            "name":"CONDOTO",
            "value":"205",
            "text":"COLOMBIA, CHOCÓ, CONDOTO",
            "code":"CO-27-205",
            "geoPoint":{
               "lat":5.088680000000068,
               "lon":-76.65246999999994
            },
            "geoRectangle":{
               "Xmin":-76.87746999999993,
               "Xmax":-76.42746999999994,
               "Ymin":4.863680000000068,
               "Ymax":5.313680000000067
            }
         },
         {
            "name":"EL CARMEN",
            "value":"245",
            "text":"COLOMBIA, CHOCÓ, EL CARMEN",
            "code":"CO-27-245",
            "geoPoint":{
               "lat":5.887780000000021,
               "lon":-75.16416999999996
            },
            "geoRectangle":{
               "Xmin":-75.17916999999996,
               "Xmax":-75.14916999999996,
               "Ymin":5.872780000000021,
               "Ymax":5.90278000000002
            }
         },
         {
            "name":"EL LITORAL DEL SAN JUÁN (Docordó)",
            "value":"250",
            "text":"COLOMBIA, CHOCÓ, EL LITORAL DEL SAN JUÁN (Docordó)",
            "code":"CO-27-250",
            "geoPoint":{
               "lat":null,
               "lon":null
            }
         },
         {
            "name":"ISTMINA",
            "value":"361",
            "text":"COLOMBIA, CHOCÓ, ISTMINA",
            "code":"CO-27-361",
            "geoPoint":{
               "lat":5.15211000000005,
               "lon":-76.68577999999997
            },
            "geoRectangle":{
               "Xmin":-77.06877999999996,
               "Xmax":-76.30277999999997,
               "Ymin":4.76911000000005,
               "Ymax":5.53511000000005
            }
         },
         {
            "name":"JURADÓ",
            "value":"372",
            "text":"COLOMBIA, CHOCÓ, JURADÓ",
            "code":"CO-27-372",
            "geoPoint":{
               "lat":7.106150000000071,
               "lon":-77.76531999999997
            },
            "geoRectangle":{
               "Xmin":-78.10331999999997,
               "Xmax":-77.42731999999998,
               "Ymin":6.7681500000000705,
               "Ymax":7.444150000000071
            }
         },
         {
            "name":"LLORÓ",
            "value":"413",
            "text":"COLOMBIA, CHOCÓ, LLORÓ",
            "code":"CO-27-413",
            "geoPoint":{
               "lat":5.503750000000025,
               "lon":-76.53101999999996
            },
            "geoRectangle":{
               "Xmin":-76.72301999999995,
               "Xmax":-76.33901999999996,
               "Ymin":5.311750000000025,
               "Ymax":5.695750000000025
            }
         },
         {
            "name":"MEDIO ATRATO (Beté)",
            "value":"425",
            "text":"COLOMBIA, CHOCÓ, MEDIO ATRATO (Beté)",
            "code":"CO-27-425",
            "geoPoint":{
               "lat":5.9941800000000285,
               "lon":-76.78182999999996
            },
            "geoRectangle":{
               "Xmin":-76.79182999999996,
               "Xmax":-76.77182999999995,
               "Ymin":5.984180000000029,
               "Ymax":6.004180000000028
            }
         },
         {
            "name":"MEDIO BAUDÓ(Boca de Pepé)",
            "value":"430",
            "text":"COLOMBIA, CHOCÓ, MEDIO BAUDÓ(Boca de Pepé)",
            "code":"CO-27-430",
            "geoPoint":{
               "lat":4.95192000000003,
               "lon":-77.40303999999998
            },
            "geoRectangle":{
               "Xmin":-97.40303999999998,
               "Xmax":-57.403039999999976,
               "Ymin":-15.04807999999997,
               "Ymax":24.95192000000003
            }
         },
         {
            "name":"MEDIO SAN JUAN (Andagoya)",
            "value":"450",
            "text":"COLOMBIA, CHOCÓ, MEDIO SAN JUAN (Andagoya)",
            "code":"CO-27-450",
            "geoPoint":{
               "lat":5.09453000000002,
               "lon":-76.69412999999997
            },
            "geoRectangle":{
               "Xmin":-76.70412999999998,
               "Xmax":-76.68412999999997,
               "Ymin":5.08453000000002,
               "Ymax":5.10453000000002
            }
         },
         {
            "name":"NÓVITA",
            "value":"491",
            "text":"COLOMBIA, CHOCÓ, NÓVITA",
            "code":"CO-27-491",
            "geoPoint":{
               "lat":4.95497000000006,
               "lon":-76.60602999999998
            },
            "geoRectangle":{
               "Xmin":-76.80502999999997,
               "Xmax":-76.40702999999998,
               "Ymin":4.75597000000006,
               "Ymax":5.15397000000006
            }
         },
         {
            "name":"NUQUÍ",
            "value":"495",
            "text":"COLOMBIA, CHOCÓ, NUQUÍ",
            "code":"CO-27-495",
            "geoPoint":{
               "lat":5.7074100000000385,
               "lon":-77.26791999999995
            },
            "geoRectangle":{
               "Xmin":-77.49291999999994,
               "Xmax":-77.04291999999995,
               "Ymin":5.482410000000039,
               "Ymax":5.932410000000038
            }
         },
         {
            "name":"RIO IRÓ (Santa Rita)",
            "value":"580",
            "text":"COLOMBIA, CHOCÓ, RIO IRÓ (Santa Rita)",
            "code":"CO-27-580",
            "geoPoint":{
               "lat":5.184880000000021,
               "lon":-76.47150999999997
            },
            "geoRectangle":{
               "Xmin":-76.48150999999997,
               "Xmax":-76.46150999999996,
               "Ymin":5.174880000000021,
               "Ymax":5.194880000000021
            }
         },
         {
            "name":"RIO QUITO (Paimadó)",
            "value":"600",
            "text":"COLOMBIA, CHOCÓ, RIO QUITO (Paimadó)",
            "code":"CO-27-600",
            "geoPoint":{
               "lat":5.482610000000022,
               "lon":-76.73885999999999
            },
            "geoRectangle":{
               "Xmin":-76.74886,
               "Xmax":-76.72885999999998,
               "Ymin":5.472610000000023,
               "Ymax":5.492610000000022
            }
         },
         {
            "name":"SAN JOSÉ DEL PALMAR",
            "value":"660",
            "text":"COLOMBIA, CHOCÓ, SAN JOSÉ DEL PALMAR",
            "code":"CO-27-660",
            "geoPoint":{
               "lat":4.895490000000052,
               "lon":-76.23465999999996
            },
            "geoRectangle":{
               "Xmin":-76.43165999999997,
               "Xmax":-76.03765999999996,
               "Ymin":4.698490000000052,
               "Ymax":5.092490000000052
            }
         },
         {
            "name":"SIPÍ",
            "value":"745",
            "text":"COLOMBIA, CHOCÓ, SIPÍ",
            "code":"CO-27-745",
            "geoPoint":{
               "lat":4.625560000000064,
               "lon":-76.48811999999998
            },
            "geoRectangle":{
               "Xmin":-76.75911999999998,
               "Xmax":-76.21711999999998,
               "Ymin":4.354560000000064,
               "Ymax":4.896560000000064
            }
         },
         {
            "name":"TADÓ",
            "value":"787",
            "text":"COLOMBIA, CHOCÓ, TADÓ",
            "code":"CO-27-787",
            "geoPoint":{
               "lat":5.263860000000022,
               "lon":-76.56154999999995
            },
            "geoRectangle":{
               "Xmin":-76.70054999999995,
               "Xmax":-76.42254999999996,
               "Ymin":5.124860000000022,
               "Ymax":5.402860000000023
            }
         },
         {
            "name":"UNGUÍA",
            "value":"800",
            "text":"COLOMBIA, CHOCÓ, UNGUÍA",
            "code":"CO-27-800",
            "geoPoint":{
               "lat":8.042160000000024,
               "lon":-77.09251999999998
            },
            "geoRectangle":{
               "Xmin":-77.31151999999997,
               "Xmax":-76.87351999999998,
               "Ymin":7.823160000000024,
               "Ymax":8.261160000000023
            }
         },
         {
            "name":"UNIÓN PANAMERICANA ( Animas)",
            "value":"810",
            "text":"COLOMBIA, CHOCÓ, UNIÓN PANAMERICANA ( Animas)",
            "code":"CO-27-810",
            "geoPoint":{
               "lat":5.279830000000061,
               "lon":-76.62905999999998
            },
            "geoRectangle":{
               "Xmin":-76.63905999999999,
               "Xmax":-76.61905999999998,
               "Ymin":5.269830000000061,
               "Ymax":5.289830000000061
            }
         },
         {
            "name":"QUIBDÓ",
            "value":"001",
            "text":"COLOMBIA, CHOCÓ, QUIBDÓ",
            "code":"CO-27-001",
            "geoPoint":{
               "lat":5.7099000000000615,
               "lon":-76.62020999999999
            },
            "geoRectangle":{
               "Xmin":-77.04220999999998,
               "Xmax":-76.19820999999999,
               "Ymin":5.287900000000062,
               "Ymax":6.131900000000061
            }
         }
      ]
   },
   {
      "name":"CÓRDOBA",
      "value":"23",
      "text":"COLOMBIA, CÓRDOBA",
      "code":"CO-23",
      "geoPoint":{
         "lat":8.358956946000035,
         "lon":-75.79601036999998
      },
      "geoRectangle":{
         "Xmin":-76.75401036999997,
         "Xmax":-74.83801036999998,
         "Ymin":7.400956946000035,
         "Ymax":9.316956946000035
      },
      "municipality":[
         {
            "name":"MONTELÍBANO",
            "value":"466",
            "text":"COLOMBIA, CÓRDOBA, MONTELÍBANO",
            "code":"CO-23-466",
            "geoPoint":{
               "lat":7.977800000000059,
               "lon":-75.41865999999999
            },
            "geoRectangle":{
               "Xmin":-75.80165999999998,
               "Xmax":-75.03566,
               "Ymin":7.594800000000059,
               "Ymax":8.360800000000058
            }
         },
         {
            "name":"PUERTO LIBERTADOR",
            "value":"580",
            "text":"COLOMBIA, CÓRDOBA, PUERTO LIBERTADOR",
            "code":"CO-23-580",
            "geoPoint":{
               "lat":7.889100000000042,
               "lon":-75.66988999999995
            },
            "geoRectangle":{
               "Xmin":-75.96188999999995,
               "Xmax":-75.37788999999995,
               "Ymin":7.597100000000042,
               "Ymax":8.181100000000042
            }
         },
         {
            "name":"TIERRALTA",
            "value":"807",
            "text":"COLOMBIA, CÓRDOBA, TIERRALTA",
            "code":"CO-23-807",
            "geoPoint":{
               "lat":8.172620000000052,
               "lon":-76.05953999999997
            },
            "geoRectangle":{
               "Xmin":-76.49453999999997,
               "Xmax":-75.62453999999997,
               "Ymin":7.737620000000052,
               "Ymax":8.607620000000052
            }
         },
         {
            "name":"COTORRA",
            "value":"300",
            "text":"COLOMBIA, CÓRDOBA, COTORRA",
            "code":"CO-23-300",
            "geoPoint":{
               "lat":9.03824000000003,
               "lon":-75.79467999999997
            },
            "geoRectangle":{
               "Xmin":-75.85467999999997,
               "Xmax":-75.73467999999997,
               "Ymin":8.97824000000003,
               "Ymax":9.09824000000003
            }
         },
         {
            "name":"LA APARTADA",
            "value":"350",
            "text":"COLOMBIA, CÓRDOBA, LA APARTADA",
            "code":"CO-23-350",
            "geoPoint":{
               "lat":8.046860000000038,
               "lon":-75.33489999999995
            },
            "geoRectangle":{
               "Xmin":-75.43989999999995,
               "Xmax":-75.22989999999994,
               "Ymin":7.9418600000000374,
               "Ymax":8.151860000000038
            }
         },
         {
            "name":"LORICA",
            "value":"417",
            "text":"COLOMBIA, CÓRDOBA, LORICA",
            "code":"CO-23-417",
            "geoPoint":{
               "lat":9.243250000000046,
               "lon":-75.80540999999994
            },
            "geoRectangle":{
               "Xmin":-76.00240999999994,
               "Xmax":-75.60840999999994,
               "Ymin":9.046250000000047,
               "Ymax":9.440250000000045
            }
         },
         {
            "name":" LOS CÓRDOBAS",
            "value":"419",
            "text":"COLOMBIA, CÓRDOBA,  LOS CÓRDOBAS",
            "code":"CO-23-419",
            "geoPoint":{
               "lat":8.89659000000006,
               "lon":-76.35378999999995
            },
            "geoRectangle":{
               "Xmin":-76.49278999999994,
               "Xmax":-76.21478999999995,
               "Ymin":8.75759000000006,
               "Ymax":9.03559000000006
            }
         },
         {
            "name":"MOMIL",
            "value":"464",
            "text":"COLOMBIA, CÓRDOBA, MOMIL",
            "code":"CO-23-464",
            "geoPoint":{
               "lat":9.23849000000007,
               "lon":-75.67662999999999
            },
            "geoRectangle":{
               "Xmin":-75.75962999999999,
               "Xmax":-75.59362999999999,
               "Ymin":9.15549000000007,
               "Ymax":9.32149000000007
            }
         },
         {
            "name":"MOÑITOS",
            "value":"500",
            "text":"COLOMBIA, CÓRDOBA, MOÑITOS",
            "code":"CO-23-500",
            "geoPoint":{
               "lat":9.244320000000073,
               "lon":-76.13292999999999
            },
            "geoRectangle":{
               "Xmin":-76.22892999999999,
               "Xmax":-76.03692999999998,
               "Ymin":9.148320000000073,
               "Ymax":9.340320000000073
            }
         },
         {
            "name":"PLANETA RICA",
            "value":"555",
            "text":"COLOMBIA, CÓRDOBA, PLANETA RICA",
            "code":"CO-23-555",
            "geoPoint":{
               "lat":8.408440000000041,
               "lon":-75.58417999999995
            },
            "geoRectangle":{
               "Xmin":-75.82517999999995,
               "Xmax":-75.34317999999995,
               "Ymin":8.167440000000042,
               "Ymax":8.649440000000041
            }
         },
         {
            "name":"PUEBLO NUEVO",
            "value":"570",
            "text":"COLOMBIA, CÓRDOBA, PUEBLO NUEVO",
            "code":"CO-23-570",
            "geoPoint":{
               "lat":8.505310000000065,
               "lon":-75.50847999999996
            },
            "geoRectangle":{
               "Xmin":-75.70647999999996,
               "Xmax":-75.31047999999997,
               "Ymin":8.307310000000065,
               "Ymax":8.703310000000066
            }
         },
         {
            "name":"PUERTO ESCONDIDO",
            "value":"574",
            "text":"COLOMBIA, CÓRDOBA, PUERTO ESCONDIDO",
            "code":"CO-23-574",
            "geoPoint":{
               "lat":9.018150000000048,
               "lon":-76.26267999999999
            },
            "geoRectangle":{
               "Xmin":-76.39067999999999,
               "Xmax":-76.13467999999999,
               "Ymin":8.890150000000048,
               "Ymax":9.146150000000048
            }
         },
         {
            "name":"PURÍSIMA",
            "value":"586",
            "text":"COLOMBIA, CÓRDOBA, PURÍSIMA",
            "code":"CO-23-586",
            "geoPoint":{
               "lat":9.236570000000029,
               "lon":-75.72189999999995
            },
            "geoRectangle":{
               "Xmin":-75.73689999999995,
               "Xmax":-75.70689999999995,
               "Ymin":9.221570000000028,
               "Ymax":9.25157000000003
            }
         },
         {
            "name":"SAHAGÚN",
            "value":"660",
            "text":"COLOMBIA, CÓRDOBA, SAHAGÚN",
            "code":"CO-23-660",
            "geoPoint":{
               "lat":8.948500000000024,
               "lon":-75.44509999999997
            },
            "geoRectangle":{
               "Xmin":-75.63309999999997,
               "Xmax":-75.25709999999997,
               "Ymin":8.760500000000023,
               "Ymax":9.136500000000025
            }
         },
         {
            "name":"SAN ANDRES DE SOTAVENTO",
            "value":"670",
            "text":"COLOMBIA, CÓRDOBA, SAN ANDRES DE SOTAVENTO",
            "code":"CO-23-670",
            "geoPoint":{
               "lat":9.144760000000076,
               "lon":-75.50876999999997
            },
            "geoRectangle":{
               "Xmin":-75.51876999999998,
               "Xmax":-75.49876999999996,
               "Ymin":9.134760000000076,
               "Ymax":9.154760000000076
            }
         },
         {
            "name":"SAN ANTERO",
            "value":"672",
            "text":"COLOMBIA, CÓRDOBA, SAN ANTERO",
            "code":"CO-23-672",
            "geoPoint":{
               "lat":9.374360000000024,
               "lon":-75.76087999999999
            },
            "geoRectangle":{
               "Xmin":-75.84487999999999,
               "Xmax":-75.67687999999998,
               "Ymin":9.290360000000025,
               "Ymax":9.458360000000024
            }
         },
         {
            "name":"SAN BERNARDO DEL VIENTO",
            "value":"675",
            "text":"COLOMBIA, CÓRDOBA, SAN BERNARDO DEL VIENTO",
            "code":"CO-23-675",
            "geoPoint":{
               "lat":9.354490000000055,
               "lon":-75.95328999999998
            },
            "geoRectangle":{
               "Xmin":-76.09828999999998,
               "Xmax":-75.80828999999999,
               "Ymin":9.209490000000056,
               "Ymax":9.499490000000055
            }
         },
         {
            "name":"SAN CARLOS",
            "value":"678",
            "text":"COLOMBIA, CÓRDOBA, SAN CARLOS",
            "code":"CO-23-678",
            "geoPoint":{
               "lat":8.79970000000003,
               "lon":-75.69778999999994
            },
            "geoRectangle":{
               "Xmin":-75.83778999999994,
               "Xmax":-75.55778999999994,
               "Ymin":8.65970000000003,
               "Ymax":8.93970000000003
            }
         },
         {
            "name":"SAN JOSÉ DE URÉ",
            "value":"682",
            "text":"COLOMBIA, CÓRDOBA, SAN JOSÉ DE URÉ",
            "code":"CO-23-682",
            "geoPoint":{
               "lat":7.788300000000049,
               "lon":-75.53484999999995
            },
            "geoRectangle":{
               "Xmin":-75.69084999999995,
               "Xmax":-75.37884999999994,
               "Ymin":7.63230000000005,
               "Ymax":7.944300000000049
            }
         },
         {
            "name":"SAN PELAYO",
            "value":"686",
            "text":"COLOMBIA, CÓRDOBA, SAN PELAYO",
            "code":"CO-23-686",
            "geoPoint":{
               "lat":8.968350000000044,
               "lon":-75.83498999999995
            },
            "geoRectangle":{
               "Xmin":-75.97998999999994,
               "Xmax":-75.68998999999995,
               "Ymin":8.823350000000044,
               "Ymax":9.113350000000043
            }
         },
         {
            "name":"TUCHIN",
            "value":"815",
            "text":"COLOMBIA, CÓRDOBA, TUCHIN",
            "code":"CO-23-815",
            "geoPoint":{
               "lat":8.816670000000045,
               "lon":-76.19749999999993
            },
            "geoRectangle":{
               "Xmin":-76.20749999999994,
               "Xmax":-76.18749999999993,
               "Ymin":8.806670000000045,
               "Ymax":8.826670000000044
            }
         },
         {
            "name":"VALENCIA",
            "value":"855",
            "text":"COLOMBIA, CÓRDOBA, VALENCIA",
            "code":"CO-23-855",
            "geoPoint":{
               "lat":8.259660000000054,
               "lon":-76.14566999999994
            },
            "geoRectangle":{
               "Xmin":-76.34866999999994,
               "Xmax":-75.94266999999994,
               "Ymin":8.056660000000054,
               "Ymax":8.462660000000053
            }
         },
         {
            "name":"MONTERÍA",
            "value":"001",
            "text":"COLOMBIA, CÓRDOBA, MONTERÍA",
            "code":"CO-23-001",
            "geoPoint":{
               "lat":8.754060000000038,
               "lon":-75.87680999999998
            },
            "geoRectangle":{
               "Xmin":-76.19780999999998,
               "Xmax":-75.55580999999998,
               "Ymin":8.433060000000038,
               "Ymax":9.075060000000038
            }
         },
         {
            "name":"AYAPEL",
            "value":"068",
            "text":"COLOMBIA, CÓRDOBA, AYAPEL",
            "code":"CO-23-068",
            "geoPoint":{
               "lat":8.313400000000058,
               "lon":-75.14596999999998
            },
            "geoRectangle":{
               "Xmin":-75.39496999999997,
               "Xmax":-74.89696999999998,
               "Ymin":8.064400000000058,
               "Ymax":8.562400000000059
            }
         },
         {
            "name":"BUENAVISTA",
            "value":"079",
            "text":"COLOMBIA, CÓRDOBA, BUENAVISTA",
            "code":"CO-23-079",
            "geoPoint":{
               "lat":8.222330000000056,
               "lon":-75.48265999999995
            },
            "geoRectangle":{
               "Xmin":-75.66265999999996,
               "Xmax":-75.30265999999995,
               "Ymin":8.042330000000057,
               "Ymax":8.402330000000056
            }
         },
         {
            "name":"CANALETE",
            "value":"090",
            "text":"COLOMBIA, CÓRDOBA, CANALETE",
            "code":"CO-23-090",
            "geoPoint":{
               "lat":8.786090000000058,
               "lon":-76.24251999999996
            },
            "geoRectangle":{
               "Xmin":-76.36251999999996,
               "Xmax":-76.12251999999995,
               "Ymin":8.66609000000006,
               "Ymax":8.906090000000058
            }
         },
         {
            "name":"CERETÉ",
            "value":"162",
            "text":"COLOMBIA, CÓRDOBA, CERETÉ",
            "code":"CO-23-162",
            "geoPoint":{
               "lat":8.886540000000025,
               "lon":-75.79149999999998
            },
            "geoRectangle":{
               "Xmin":-75.92649999999999,
               "Xmax":-75.65649999999998,
               "Ymin":8.751540000000025,
               "Ymax":9.021540000000025
            }
         },
         {
            "name":"CHIMÁ",
            "value":"168",
            "text":"COLOMBIA, CÓRDOBA, CHIMÁ",
            "code":"CO-23-168",
            "geoPoint":{
               "lat":9.149500000000046,
               "lon":-75.62783999999994
            },
            "geoRectangle":{
               "Xmin":-75.73983999999993,
               "Xmax":-75.51583999999994,
               "Ymin":9.037500000000046,
               "Ymax":9.261500000000046
            }
         },
         {
            "name":"CHINÚ",
            "value":"182",
            "text":"COLOMBIA, CÓRDOBA, CHINÚ",
            "code":"CO-23-182",
            "geoPoint":{
               "lat":9.10790000000003,
               "lon":-75.39746999999994
            },
            "geoRectangle":{
               "Xmin":-75.56246999999995,
               "Xmax":-75.23246999999994,
               "Ymin":8.94290000000003,
               "Ymax":9.272900000000028
            }
         },
         {
            "name":"CIENAGA DE ORO",
            "value":"189",
            "text":"COLOMBIA, CÓRDOBA, CIENAGA DE ORO",
            "code":"CO-23-189",
            "geoPoint":{
               "lat":8.871670000000051,
               "lon":-75.61899999999997
            },
            "geoRectangle":{
               "Xmin":-75.79599999999998,
               "Xmax":-75.44199999999996,
               "Ymin":8.694670000000052,
               "Ymax":9.048670000000051
            }
         }
      ]
   },
   {
      "name":"CUNDINAMARCA",
      "value":"25",
      "text":"COLOMBIA, CUNDINAMARCA",
      "code":"CO-25",
      "geoPoint":{
         "lat":4.8222912340000335,
         "lon":-74.09560190799994
      },
      "geoRectangle":{
         "Xmin":-75.08260190799993,
         "Xmax":-73.10860190799994,
         "Ymin":3.8352912340000334,
         "Ymax":5.8092912340000336
      },
      "municipality":[
         {
            "name":"NARIÑO",
            "value":"483",
            "text":"COLOMBIA, CUNDINAMARCA, NARIÑO",
            "code":"CO-25-483",
            "geoPoint":{
               "lat":4.399540000000059,
               "lon":-74.82370999999995
            },
            "geoRectangle":{
               "Xmin":-74.89170999999995,
               "Xmax":-74.75570999999995,
               "Ymin":4.331540000000059,
               "Ymax":4.467540000000058
            }
         },
         {
            "name":" PACHO",
            "value":"513",
            "text":"COLOMBIA, CUNDINAMARCA,  PACHO",
            "code":"CO-25-513",
            "geoPoint":{
               "lat":5.130090000000052,
               "lon":-74.15804999999995
            },
            "geoRectangle":{
               "Xmin":-74.28704999999995,
               "Xmax":-74.02904999999994,
               "Ymin":5.001090000000053,
               "Ymax":5.259090000000052
            }
         },
         {
            "name":"NOCAIMA",
            "value":"491",
            "text":"COLOMBIA, CUNDINAMARCA, NOCAIMA",
            "code":"CO-25-491",
            "geoPoint":{
               "lat":5.07018000000005,
               "lon":-74.37767999999994
            },
            "geoRectangle":{
               "Xmin":-74.42767999999994,
               "Xmax":-74.32767999999994,
               "Ymin":5.0201800000000505,
               "Ymax":5.12018000000005
            }
         },
         {
            "name":"NIMAIMA",
            "value":"489",
            "text":"COLOMBIA, CUNDINAMARCA, NIMAIMA",
            "code":"CO-25-489",
            "geoPoint":{
               "lat":5.125430000000051,
               "lon":-74.38592999999997
            },
            "geoRectangle":{
               "Xmin":-74.44892999999998,
               "Xmax":-74.32292999999997,
               "Ymin":5.0624300000000515,
               "Ymax":5.188430000000051
            }
         },
         {
            "name":"NILO",
            "value":"488",
            "text":"COLOMBIA, CUNDINAMARCA, NILO",
            "code":"CO-25-488",
            "geoPoint":{
               "lat":4.30678000000006,
               "lon":-74.61999999999995
            },
            "geoRectangle":{
               "Xmin":-74.72199999999995,
               "Xmax":-74.51799999999994,
               "Ymin":4.20478000000006,
               "Ymax":4.4087800000000605
            }
         },
         {
            "name":"NEMOCÓN",
            "value":"486",
            "text":"COLOMBIA, CUNDINAMARCA, NEMOCÓN",
            "code":"CO-25-486",
            "geoPoint":{
               "lat":5.067690000000027,
               "lon":-73.87879999999996
            },
            "geoRectangle":{
               "Xmin":-73.93879999999996,
               "Xmax":-73.81879999999995,
               "Ymin":5.007690000000028,
               "Ymax":5.127690000000027
            }
         },
         {
            "name":"PARATEBUENO",
            "value":"530",
            "text":"COLOMBIA, CUNDINAMARCA, PARATEBUENO",
            "code":"CO-25-530",
            "geoPoint":{
               "lat":4.374650000000031,
               "lon":-73.21159999999998
            },
            "geoRectangle":{
               "Xmin":-73.43959999999997,
               "Xmax":-72.98359999999998,
               "Ymin":4.146650000000031,
               "Ymax":4.602650000000031
            }
         },
         {
            "name":"PUERTO SALGAR",
            "value":"572",
            "text":"COLOMBIA, CUNDINAMARCA, PUERTO SALGAR",
            "code":"CO-25-572",
            "geoPoint":{
               "lat":5.46494000000007,
               "lon":-74.65832999999998
            },
            "geoRectangle":{
               "Xmin":-74.81032999999998,
               "Xmax":-74.50632999999998,
               "Ymin":5.3129400000000695,
               "Ymax":5.61694000000007
            }
         },
         {
            "name":"PULÍ",
            "value":"580",
            "text":"COLOMBIA, CUNDINAMARCA, PULÍ",
            "code":"CO-25-580",
            "geoPoint":{
               "lat":4.682350000000042,
               "lon":-74.71490999999997
            },
            "geoRectangle":{
               "Xmin":-74.79490999999997,
               "Xmax":-74.63490999999998,
               "Ymin":4.602350000000042,
               "Ymax":4.762350000000042
            }
         },
         {
            "name":"QUEBRADANEGRA",
            "value":"592",
            "text":"COLOMBIA, CUNDINAMARCA, QUEBRADANEGRA",
            "code":"CO-25-592",
            "geoPoint":{
               "lat":5.117720000000077,
               "lon":-74.47934999999995
            },
            "geoRectangle":{
               "Xmin":-74.52634999999995,
               "Xmax":-74.43234999999996,
               "Ymin":5.070720000000077,
               "Ymax":5.164720000000076
            }
         },
         {
            "name":"QUETAME",
            "value":"594",
            "text":"COLOMBIA, CUNDINAMARCA, QUETAME",
            "code":"CO-25-594",
            "geoPoint":{
               "lat":4.330200000000048,
               "lon":-73.86329999999998
            },
            "geoRectangle":{
               "Xmin":-73.93229999999998,
               "Xmax":-73.79429999999998,
               "Ymin":4.261200000000048,
               "Ymax":4.3992000000000475
            }
         },
         {
            "name":"QUIPILE",
            "value":"596",
            "text":"COLOMBIA, CUNDINAMARCA, QUIPILE",
            "code":"CO-25-596",
            "geoPoint":{
               "lat":4.743790000000047,
               "lon":-74.53372999999993
            },
            "geoRectangle":{
               "Xmin":-74.60572999999994,
               "Xmax":-74.46172999999993,
               "Ymin":4.671790000000047,
               "Ymax":4.815790000000047
            }
         },
         {
            "name":"PAIME",
            "value":"518",
            "text":"COLOMBIA, CUNDINAMARCA, PAIME",
            "code":"CO-25-518",
            "geoPoint":{
               "lat":5.370130000000074,
               "lon":-74.15203999999994
            },
            "geoRectangle":{
               "Xmin":-74.23103999999994,
               "Xmax":-74.07303999999995,
               "Ymin":5.2911300000000745,
               "Ymax":5.449130000000074
            }
         },
         {
            "name":"APULO",
            "value":"599",
            "text":"COLOMBIA, CUNDINAMARCA, APULO",
            "code":"CO-25-599",
            "geoPoint":{
               "lat":4.521850000000029,
               "lon":-74.59231999999997
            },
            "geoRectangle":{
               "Xmin":-74.66831999999997,
               "Xmax":-74.51631999999998,
               "Ymin":4.445850000000029,
               "Ymax":4.597850000000029
            }
         },
         {
            "name":"RICAURTE",
            "value":"612",
            "text":"COLOMBIA, CUNDINAMARCA, RICAURTE",
            "code":"CO-25-612",
            "geoPoint":{
               "lat":4.279540000000054,
               "lon":-74.76479999999998
            },
            "geoRectangle":{
               "Xmin":-74.83379999999998,
               "Xmax":-74.69579999999998,
               "Ymin":4.210540000000054,
               "Ymax":4.348540000000054
            }
         },
         {
            "name":"SAN ANTONIO DEL TEQUENDAMA",
            "value":"645",
            "text":"COLOMBIA, CUNDINAMARCA, SAN ANTONIO DEL TEQUENDAMA",
            "code":"CO-25-645",
            "geoPoint":{
               "lat":4.616170000000068,
               "lon":-74.35215999999997
            },
            "geoRectangle":{
               "Xmin":-74.40215999999997,
               "Xmax":-74.30215999999997,
               "Ymin":4.566170000000068,
               "Ymax":4.666170000000068
            }
         },
         {
            "name":"SAN CAYETANO",
            "value":"653",
            "text":"COLOMBIA, CUNDINAMARCA, SAN CAYETANO",
            "code":"CO-25-653",
            "geoPoint":{
               "lat":5.332390000000032,
               "lon":-74.02558999999997
            },
            "geoRectangle":{
               "Xmin":-74.13358999999997,
               "Xmax":-73.91758999999996,
               "Ymin":5.2243900000000325,
               "Ymax":5.440390000000032
            }
         },
         {
            "name":"SAN FRANCISCO",
            "value":"658",
            "text":"COLOMBIA, CUNDINAMARCA, SAN FRANCISCO",
            "code":"CO-25-658",
            "geoPoint":{
               "lat":4.974230000000034,
               "lon":-74.28888999999998
            },
            "geoRectangle":{
               "Xmin":-74.35888999999997,
               "Xmax":-74.21888999999999,
               "Ymin":4.904230000000034,
               "Ymax":5.044230000000034
            }
         },
         {
            "name":"SAN JUAN DE RIO SECO",
            "value":"662",
            "text":"COLOMBIA, CUNDINAMARCA, SAN JUAN DE RIO SECO",
            "code":"CO-25-662",
            "geoPoint":{
               "lat":4.851110000000062,
               "lon":-74.12610999999998
            },
            "geoRectangle":{
               "Xmin":-74.13610999999999,
               "Xmax":-74.11610999999998,
               "Ymin":4.841110000000063,
               "Ymax":4.861110000000062
            }
         },
         {
            "name":"SASAIMA",
            "value":"718",
            "text":"COLOMBIA, CUNDINAMARCA, SASAIMA",
            "code":"CO-25-718",
            "geoPoint":{
               "lat":4.96225000000004,
               "lon":-74.43418999999994
            },
            "geoRectangle":{
               "Xmin":-74.50318999999995,
               "Xmax":-74.36518999999994,
               "Ymin":4.89325000000004,
               "Ymax":5.03125000000004
            }
         },
         {
            "name":"SESQUILÉ",
            "value":"736",
            "text":"COLOMBIA, CUNDINAMARCA, SESQUILÉ",
            "code":"CO-25-736",
            "geoPoint":{
               "lat":5.044800000000066,
               "lon":-73.79730999999998
            },
            "geoRectangle":{
               "Xmin":-73.87230999999998,
               "Xmax":-73.72230999999998,
               "Ymin":4.969800000000066,
               "Ymax":5.119800000000066
            }
         },
         {
            "name":"UBALÁ",
            "value":"839",
            "text":"COLOMBIA, CUNDINAMARCA, UBALÁ",
            "code":"CO-25-839",
            "geoPoint":{
               "lat":4.745840000000044,
               "lon":-73.53515999999996
            },
            "geoRectangle":{
               "Xmin":-73.69615999999996,
               "Xmax":-73.37415999999996,
               "Ymin":4.584840000000044,
               "Ymax":4.906840000000043
            }
         },
         {
            "name":"SIBATÉ",
            "value":"740",
            "text":"COLOMBIA, CUNDINAMARCA, SIBATÉ",
            "code":"CO-25-740",
            "geoPoint":{
               "lat":4.489150000000052,
               "lon":-74.25995999999998
            },
            "geoRectangle":{
               "Xmin":-74.32595999999998,
               "Xmax":-74.19395999999998,
               "Ymin":4.423150000000052,
               "Ymax":4.555150000000052
            }
         },
         {
            "name":"SIMIJACA",
            "value":"745",
            "text":"COLOMBIA, CUNDINAMARCA, SIMIJACA",
            "code":"CO-25-745",
            "geoPoint":{
               "lat":5.503600000000063,
               "lon":-73.85097999999994
            },
            "geoRectangle":{
               "Xmin":-73.91197999999994,
               "Xmax":-73.78997999999993,
               "Ymin":5.442600000000063,
               "Ymax":5.564600000000063
            }
         },
         {
            "name":"SOACHA",
            "value":"754",
            "text":"COLOMBIA, CUNDINAMARCA, SOACHA",
            "code":"CO-25-754",
            "geoPoint":{
               "lat":4.575920000000053,
               "lon":-74.22286999999994
            },
            "geoRectangle":{
               "Xmin":-74.31886999999995,
               "Xmax":-74.12686999999994,
               "Ymin":4.479920000000053,
               "Ymax":4.671920000000053
            }
         },
         {
            "name":"BOJACÁ",
            "value":"099",
            "text":"COLOMBIA, CUNDINAMARCA, BOJACÁ",
            "code":"CO-25-099",
            "geoPoint":{
               "lat":4.733630000000062,
               "lon":-74.34118999999998
            },
            "geoRectangle":{
               "Xmin":-74.41218999999998,
               "Xmax":-74.27018999999999,
               "Ymin":4.662630000000062,
               "Ymax":4.804630000000062
            }
         },
         {
            "name":"BITUIMA",
            "value":"095",
            "text":"COLOMBIA, CUNDINAMARCA, BITUIMA",
            "code":"CO-25-095",
            "geoPoint":{
               "lat":4.87226000000004,
               "lon":-74.53936999999996
            },
            "geoRectangle":{
               "Xmin":-74.59436999999997,
               "Xmax":-74.48436999999996,
               "Ymin":4.81726000000004,
               "Ymax":4.9272600000000395
            }
         },
         {
            "name":"BELTRÁN",
            "value":"086",
            "text":"COLOMBIA, CUNDINAMARCA, BELTRÁN",
            "code":"CO-25-086",
            "geoPoint":{
               "lat":4.799590000000023,
               "lon":-74.74077999999997
            },
            "geoRectangle":{
               "Xmin":-74.83977999999998,
               "Xmax":-74.64177999999997,
               "Ymin":4.700590000000023,
               "Ymax":4.8985900000000235
            }
         },
         {
            "name":"ANOLAIMA",
            "value":"040",
            "text":"COLOMBIA, CUNDINAMARCA, ANOLAIMA",
            "code":"CO-25-040",
            "geoPoint":{
               "lat":4.7605100000000675,
               "lon":-74.46315999999996
            },
            "geoRectangle":{
               "Xmin":-74.52715999999995,
               "Xmax":-74.39915999999997,
               "Ymin":4.6965100000000675,
               "Ymax":4.824510000000068
            }
         },
         {
            "name":"ANAPOIMA",
            "value":"035",
            "text":"COLOMBIA, CUNDINAMARCA, ANAPOIMA",
            "code":"CO-25-035",
            "geoPoint":{
               "lat":4.570230000000038,
               "lon":-74.52708999999999
            },
            "geoRectangle":{
               "Xmin":-74.60108999999999,
               "Xmax":-74.45308999999999,
               "Ymin":4.496230000000038,
               "Ymax":4.644230000000038
            }
         },
         {
            "name":"CAPARRAPI",
            "value":"148",
            "text":"COLOMBIA, CUNDINAMARCA, CAPARRAPI",
            "code":"CO-25-148",
            "geoPoint":{
               "lat":5.411690059309677,
               "lon":-74.4636999417393
            },
            "geoRectangle":{
               "Xmin":-74.46469994173931,
               "Xmax":-74.4626999417393,
               "Ymin":5.410690059309677,
               "Ymax":5.412690059309678
            }
         },
         {
            "name":"CÁQUEZA",
            "value":"151",
            "text":"COLOMBIA, CUNDINAMARCA, CÁQUEZA",
            "code":"CO-25-151",
            "geoPoint":{
               "lat":4.40569000000005,
               "lon":-73.94682999999998
            },
            "geoRectangle":{
               "Xmin":-73.96182999999998,
               "Xmax":-73.93182999999998,
               "Ymin":4.39069000000005,
               "Ymax":4.420690000000049
            }
         },
         {
            "name":" CARMEN DE CARUPA",
            "value":"154",
            "text":"COLOMBIA, CUNDINAMARCA,  CARMEN DE CARUPA",
            "code":"CO-25-154",
            "geoPoint":{
               "lat":5.349280000000022,
               "lon":-73.90114999999997
            },
            "geoRectangle":{
               "Xmin":-74.01014999999997,
               "Xmax":-73.79214999999998,
               "Ymin":5.240280000000022,
               "Ymax":5.458280000000022
            }
         },
         {
            "name":"CHAGUANÍ",
            "value":"168",
            "text":"COLOMBIA, CUNDINAMARCA, CHAGUANÍ",
            "code":"CO-25-168",
            "geoPoint":{
               "lat":4.9490200000000755,
               "lon":-74.59388999999999
            },
            "geoRectangle":{
               "Xmin":-74.67188999999999,
               "Xmax":-74.51588999999998,
               "Ymin":4.871020000000075,
               "Ymax":5.027020000000076
            }
         },
         {
            "name":"CHIA",
            "value":"175",
            "text":"COLOMBIA, CUNDINAMARCA, CHIA",
            "code":"CO-25-175",
            "geoPoint":{
               "lat":4.86641000000003,
               "lon":-74.03818999999999
            },
            "geoRectangle":{
               "Xmin":-74.04318999999998,
               "Xmax":-74.03318999999999,
               "Ymin":4.86141000000003,
               "Ymax":4.87141000000003
            }
         },
         {
            "name":"CHIPAQUE",
            "value":"178",
            "text":"COLOMBIA, CUNDINAMARCA, CHIPAQUE",
            "code":"CO-25-178",
            "geoPoint":{
               "lat":4.4423700000000395,
               "lon":-74.04408999999998
            },
            "geoRectangle":{
               "Xmin":-74.13108999999999,
               "Xmax":-73.95708999999998,
               "Ymin":4.35537000000004,
               "Ymax":4.529370000000039
            }
         },
         {
            "name":"ALBÁN",
            "value":"019",
            "text":"COLOMBIA, CUNDINAMARCA, ALBÁN",
            "code":"CO-25-019",
            "geoPoint":{
               "lat":4.879550000000052,
               "lon":-74.43775999999997
            },
            "geoRectangle":{
               "Xmin":-74.48975999999998,
               "Xmax":-74.38575999999996,
               "Ymin":4.827550000000052,
               "Ymax":4.931550000000051
            }
         },
         {
            "name":"CAJICÁ",
            "value":"126",
            "text":"COLOMBIA, CUNDINAMARCA, CAJICÁ",
            "code":"CO-25-126",
            "geoPoint":{
               "lat":4.920590000000061,
               "lon":-74.02533999999997
            },
            "geoRectangle":{
               "Xmin":-74.07233999999997,
               "Xmax":-73.97833999999997,
               "Ymin":4.873590000000061,
               "Ymax":4.967590000000061
            }
         },
         {
            "name":"AGUA DE DIOS",
            "value":"001",
            "text":"COLOMBIA, CUNDINAMARCA, AGUA DE DIOS",
            "code":"CO-25-001",
            "geoPoint":{
               "lat":4.376920000000041,
               "lon":-74.66816999999998
            },
            "geoRectangle":{
               "Xmin":-74.72516999999998,
               "Xmax":-74.61116999999997,
               "Ymin":4.319920000000041,
               "Ymax":4.433920000000041
            }
         },
         {
            "name":"CACHIPAY",
            "value":"123",
            "text":"COLOMBIA, CUNDINAMARCA, CACHIPAY",
            "code":"CO-25-123",
            "geoPoint":{
               "lat":4.731120000000033,
               "lon":-74.43501999999995
            },
            "geoRectangle":{
               "Xmin":-74.48801999999995,
               "Xmax":-74.38201999999995,
               "Ymin":4.678120000000033,
               "Ymax":4.784120000000033
            }
         },
         {
            "name":"CHOACHÍ",
            "value":"181",
            "text":"COLOMBIA, CUNDINAMARCA, CHOACHÍ",
            "code":"CO-25-181",
            "geoPoint":{
               "lat":4.528210000000058,
               "lon":-73.92336999999998
            },
            "geoRectangle":{
               "Xmin":-74.01536999999998,
               "Xmax":-73.83136999999998,
               "Ymin":4.436210000000059,
               "Ymax":4.620210000000058
            }
         },
         {
            "name":"CHOCONTÁ",
            "value":"183",
            "text":"COLOMBIA, CUNDINAMARCA, CHOCONTÁ",
            "code":"CO-25-183",
            "geoPoint":{
               "lat":5.147140000000036,
               "lon":-73.68664999999999
            },
            "geoRectangle":{
               "Xmin":-73.81564999999999,
               "Xmax":-73.55764999999998,
               "Ymin":5.018140000000036,
               "Ymax":5.276140000000035
            }
         },
         {
            "name":"COGUA",
            "value":"200",
            "text":"COLOMBIA, CUNDINAMARCA, COGUA",
            "code":"CO-25-200",
            "geoPoint":{
               "lat":5.060550000000035,
               "lon":-73.97928999999993
            },
            "geoRectangle":{
               "Xmin":-74.05028999999993,
               "Xmax":-73.90828999999994,
               "Ymin":4.989550000000035,
               "Ymax":5.1315500000000345
            }
         },
         {
            "name":"COTA",
            "value":"214",
            "text":"COLOMBIA, CUNDINAMARCA, COTA",
            "code":"CO-25-214",
            "geoPoint":{
               "lat":4.809310000000039,
               "lon":-74.10225999999994
            },
            "geoRectangle":{
               "Xmin":-74.15525999999994,
               "Xmax":-74.04925999999995,
               "Ymin":4.756310000000039,
               "Ymax":4.862310000000039
            }
         },
         {
            "name":" CUCUNUBÁ",
            "value":"224",
            "text":"COLOMBIA, CUNDINAMARCA,  CUCUNUBÁ",
            "code":"CO-25-224",
            "geoPoint":{
               "lat":5.248370000000023,
               "lon":-73.76760999999993
            },
            "geoRectangle":{
               "Xmin":-73.84160999999993,
               "Xmax":-73.69360999999994,
               "Ymin":5.174370000000023,
               "Ymax":5.3223700000000225
            }
         },
         {
            "name":"EL COLEGIO",
            "value":"245",
            "text":"COLOMBIA, CUNDINAMARCA, EL COLEGIO",
            "code":"CO-25-245",
            "geoPoint":{
               "lat":4.58086000000003,
               "lon":-74.44327999999996
            },
            "geoRectangle":{
               "Xmin":-74.50927999999996,
               "Xmax":-74.37727999999996,
               "Ymin":4.51486000000003,
               "Ymax":4.6468600000000295
            }
         },
         {
            "name":"EL PEÑON",
            "value":"258",
            "text":"COLOMBIA, CUNDINAMARCA, EL PEÑON",
            "code":"CO-25-258",
            "geoPoint":{
               "lat":5.248760000000061,
               "lon":-74.29010999999997
            },
            "geoRectangle":{
               "Xmin":-74.35710999999996,
               "Xmax":-74.22310999999998,
               "Ymin":5.181760000000061,
               "Ymax":5.315760000000061
            }
         },
         {
            "name":"EL ROSAL",
            "value":"260",
            "text":"COLOMBIA, CUNDINAMARCA, EL ROSAL",
            "code":"CO-25-260",
            "geoPoint":{
               "lat":4.852470000000039,
               "lon":-74.26152999999994
            },
            "geoRectangle":{
               "Xmin":-74.31852999999994,
               "Xmax":-74.20452999999993,
               "Ymin":4.795470000000039,
               "Ymax":4.90947000000004
            }
         },
         {
            "name":"FACATATIVÁ",
            "value":"269",
            "text":"COLOMBIA, CUNDINAMARCA, FACATATIVÁ",
            "code":"CO-25-269",
            "geoPoint":{
               "lat":4.8093900000000644,
               "lon":-74.35443999999995
            },
            "geoRectangle":{
               "Xmin":-74.42843999999995,
               "Xmax":-74.28043999999996,
               "Ymin":4.735390000000065,
               "Ymax":4.883390000000064
            }
         },
         {
            "name":"FÓMEQUE",
            "value":"279",
            "text":"COLOMBIA, CUNDINAMARCA, FÓMEQUE",
            "code":"CO-25-279",
            "geoPoint":{
               "lat":4.487970000000075,
               "lon":-73.89748999999995
            },
            "geoRectangle":{
               "Xmin":-73.91248999999995,
               "Xmax":-73.88248999999995,
               "Ymin":4.472970000000076,
               "Ymax":4.502970000000075
            }
         },
         {
            "name":"FOSCA",
            "value":"281",
            "text":"COLOMBIA, CUNDINAMARCA, FOSCA",
            "code":"CO-25-281",
            "geoPoint":{
               "lat":4.33901000000003,
               "lon":-73.93916999999993
            },
            "geoRectangle":{
               "Xmin":-74.00316999999993,
               "Xmax":-73.87516999999994,
               "Ymin":4.27501000000003,
               "Ymax":4.40301000000003
            }
         },
         {
            "name":" FUNZA",
            "value":"286",
            "text":"COLOMBIA, CUNDINAMARCA,  FUNZA",
            "code":"CO-25-286",
            "geoPoint":{
               "lat":4.71461000000005,
               "lon":-74.20715999999999
            },
            "geoRectangle":{
               "Xmin":-74.25715999999998,
               "Xmax":-74.15715999999999,
               "Ymin":4.66461000000005,
               "Ymax":4.76461000000005
            }
         },
         {
            "name":"TAUSA",
            "value":"793",
            "text":"COLOMBIA, CUNDINAMARCA, TAUSA",
            "code":"CO-25-793",
            "geoPoint":{
               "lat":5.196110000000033,
               "lon":-73.88664999999997
            },
            "geoRectangle":{
               "Xmin":-73.97364999999998,
               "Xmax":-73.79964999999997,
               "Ymin":5.109110000000033,
               "Ymax":5.283110000000033
            }
         },
         {
            "name":"TABIO",
            "value":"785",
            "text":"COLOMBIA, CUNDINAMARCA, TABIO",
            "code":"CO-25-785",
            "geoPoint":{
               "lat":4.917700000000025,
               "lon":-74.09675999999996
            },
            "geoRectangle":{
               "Xmin":-74.15075999999996,
               "Xmax":-74.04275999999996,
               "Ymin":4.863700000000025,
               "Ymax":4.971700000000025
            }
         },
         {
            "name":"UBAQUE",
            "value":"841",
            "text":"COLOMBIA, CUNDINAMARCA, UBAQUE",
            "code":"CO-25-841",
            "geoPoint":{
               "lat":4.48404000000005,
               "lon":-73.93401999999998
            },
            "geoRectangle":{
               "Xmin":-74.00101999999997,
               "Xmax":-73.86701999999998,
               "Ymin":4.41704000000005,
               "Ymax":4.55104000000005
            }
         },
         {
            "name":"SUSA",
            "value":"779",
            "text":"COLOMBIA, CUNDINAMARCA, SUSA",
            "code":"CO-25-779",
            "geoPoint":{
               "lat":5.454080000000033,
               "lon":-73.81387999999998
            },
            "geoRectangle":{
               "Xmin":-73.89287999999998,
               "Xmax":-73.73487999999999,
               "Ymin":5.375080000000033,
               "Ymax":5.533080000000033
            }
         },
         {
            "name":"TOCANCIPÁ",
            "value":"817",
            "text":"COLOMBIA, CUNDINAMARCA, TOCANCIPÁ",
            "code":"CO-25-817",
            "geoPoint":{
               "lat":4.96470000000005,
               "lon":-73.91221999999993
            },
            "geoRectangle":{
               "Xmin":-73.97021999999994,
               "Xmax":-73.85421999999993,
               "Ymin":4.9067000000000505,
               "Ymax":5.02270000000005
            }
         },
         {
            "name":"SUPATÁ",
            "value":"777",
            "text":"COLOMBIA, CUNDINAMARCA, SUPATÁ",
            "code":"CO-25-777",
            "geoPoint":{
               "lat":5.062280000000044,
               "lon":-74.23610999999994
            },
            "geoRectangle":{
               "Xmin":-74.30310999999993,
               "Xmax":-74.16910999999995,
               "Ymin":4.995280000000044,
               "Ymax":5.129280000000044
            }
         },
         {
            "name":"TOCAIMA",
            "value":"815",
            "text":"COLOMBIA, CUNDINAMARCA, TOCAIMA",
            "code":"CO-25-815",
            "geoPoint":{
               "lat":4.458550000000059,
               "lon":-74.63632999999999
            },
            "geoRectangle":{
               "Xmin":-74.75132999999998,
               "Xmax":-74.52132999999999,
               "Ymin":4.343550000000059,
               "Ymax":4.5735500000000595
            }
         },
         {
            "name":"SUESCA",
            "value":"772",
            "text":"COLOMBIA, CUNDINAMARCA, SUESCA",
            "code":"CO-25-772",
            "geoPoint":{
               "lat":5.103870000000029,
               "lon":-73.79871999999995
            },
            "geoRectangle":{
               "Xmin":-73.89771999999995,
               "Xmax":-73.69971999999994,
               "Ymin":5.004870000000029,
               "Ymax":5.202870000000029
            }
         },
         {
            "name":"ZIPAQUIRA",
            "value":"899",
            "text":"COLOMBIA, CUNDINAMARCA, ZIPAQUIRA",
            "code":"CO-25-899",
            "geoPoint":{
               "lat":5.025470000000041,
               "lon":-73.97484999999995
            },
            "geoRectangle":{
               "Xmin":-73.97984999999994,
               "Xmax":-73.96984999999995,
               "Ymin":5.020470000000041,
               "Ymax":5.030470000000041
            }
         },
         {
            "name":"SOPO",
            "value":"758",
            "text":"COLOMBIA, CUNDINAMARCA, SOPO",
            "code":"CO-25-758",
            "geoPoint":{
               "lat":4.906680000000051,
               "lon":-73.94191999999998
            },
            "geoRectangle":{
               "Xmin":-74.00591999999997,
               "Xmax":-73.87791999999999,
               "Ymin":4.842680000000051,
               "Ymax":4.970680000000051
            }
         },
         {
            "name":"TENJO",
            "value":"799",
            "text":"COLOMBIA, CUNDINAMARCA, TENJO",
            "code":"CO-25-799",
            "geoPoint":{
               "lat":4.8721400000000585,
               "lon":-74.14467999999994
            },
            "geoRectangle":{
               "Xmin":-74.21867999999994,
               "Xmax":-74.07067999999994,
               "Ymin":4.798140000000059,
               "Ymax":4.946140000000058
            }
         },
         {
            "name":"UNE",
            "value":"845",
            "text":"COLOMBIA, CUNDINAMARCA, UNE",
            "code":"CO-25-845",
            "geoPoint":{
               "lat":4.403050000000064,
               "lon":-74.02521999999993
            },
            "geoRectangle":{
               "Xmin":-74.12921999999993,
               "Xmax":-73.92121999999993,
               "Ymin":4.299050000000064,
               "Ymax":4.507050000000064
            }
         },
         {
            "name":"SUTATAUSA",
            "value":"781",
            "text":"COLOMBIA, CUNDINAMARCA, SUTATAUSA",
            "code":"CO-25-781",
            "geoPoint":{
               "lat":5.247480000000053,
               "lon":-73.85333999999995
            },
            "geoRectangle":{
               "Xmin":-73.90433999999995,
               "Xmax":-73.80233999999994,
               "Ymin":5.196480000000053,
               "Ymax":5.298480000000053
            }
         },
         {
            "name":"TOPAIPÍ",
            "value":"823",
            "text":"COLOMBIA, CUNDINAMARCA, TOPAIPÍ",
            "code":"CO-25-823",
            "geoPoint":{
               "lat":5.335910000000069,
               "lon":-74.30231999999995
            },
            "geoRectangle":{
               "Xmin":-74.37831999999995,
               "Xmax":-74.22631999999996,
               "Ymin":5.25991000000007,
               "Ymax":5.411910000000069
            }
         },
         {
            "name":"TIBIRITA",
            "value":"807",
            "text":"COLOMBIA, CUNDINAMARCA, TIBIRITA",
            "code":"CO-25-807",
            "geoPoint":{
               "lat":5.052380000000028,
               "lon":-73.50546999999995
            },
            "geoRectangle":{
               "Xmin":-73.55146999999995,
               "Xmax":-73.45946999999994,
               "Ymin":5.006380000000028,
               "Ymax":5.098380000000028
            }
         },
         {
            "name":"VERGARA",
            "value":"862",
            "text":"COLOMBIA, CUNDINAMARCA, VERGARA",
            "code":"CO-25-862",
            "geoPoint":{
               "lat":5.11901000000006,
               "lon":-74.34532999999993
            },
            "geoRectangle":{
               "Xmin":-74.42132999999993,
               "Xmax":-74.26932999999994,
               "Ymin":5.04301000000006,
               "Ymax":5.195010000000059
            }
         },
         {
            "name":"VIANÍ",
            "value":"867",
            "text":"COLOMBIA, CUNDINAMARCA, VIANÍ",
            "code":"CO-25-867",
            "geoPoint":{
               "lat":4.873860000000036,
               "lon":-74.56234999999998
            },
            "geoRectangle":{
               "Xmin":-74.62434999999998,
               "Xmax":-74.50034999999998,
               "Ymin":4.811860000000036,
               "Ymax":4.935860000000036
            }
         },
         {
            "name":"VILLA GÓMEZ",
            "value":"871",
            "text":"COLOMBIA, CUNDINAMARCA, VILLA GÓMEZ",
            "code":"CO-25-871",
            "geoPoint":{
               "lat":5.273490000000038,
               "lon":-74.19563999999997
            },
            "geoRectangle":{
               "Xmin":-74.25663999999998,
               "Xmax":-74.13463999999996,
               "Ymin":5.212490000000038,
               "Ymax":5.334490000000038
            }
         },
         {
            "name":"VILLAPINZÓN",
            "value":"873",
            "text":"COLOMBIA, CUNDINAMARCA, VILLAPINZÓN",
            "code":"CO-25-873",
            "geoPoint":{
               "lat":5.216160000000059,
               "lon":-73.59358999999995
            },
            "geoRectangle":{
               "Xmin":-73.70058999999995,
               "Xmax":-73.48658999999995,
               "Ymin":5.109160000000059,
               "Ymax":5.323160000000059
            }
         },
         {
            "name":"VILLETA",
            "value":"875",
            "text":"COLOMBIA, CUNDINAMARCA, VILLETA",
            "code":"CO-25-875",
            "geoPoint":{
               "lat":5.008170000000064,
               "lon":-74.47117999999995
            },
            "geoRectangle":{
               "Xmin":-74.53817999999994,
               "Xmax":-74.40417999999995,
               "Ymin":4.941170000000064,
               "Ymax":5.075170000000064
            }
         },
         {
            "name":"VIOTÁ",
            "value":"878",
            "text":"COLOMBIA, CUNDINAMARCA, VIOTÁ",
            "code":"CO-25-878",
            "geoPoint":{
               "lat":4.4391200000000595,
               "lon":-74.52185999999995
            },
            "geoRectangle":{
               "Xmin":-74.60885999999995,
               "Xmax":-74.43485999999994,
               "Ymin":4.35212000000006,
               "Ymax":4.526120000000059
            }
         },
         {
            "name":"SUBACHOQUE",
            "value":"769",
            "text":"COLOMBIA, CUNDINAMARCA, SUBACHOQUE",
            "code":"CO-25-769",
            "geoPoint":{
               "lat":4.923510000000022,
               "lon":-74.17232999999999
            },
            "geoRectangle":{
               "Xmin":-74.26432999999999,
               "Xmax":-74.08032999999999,
               "Ymin":4.831510000000022,
               "Ymax":5.015510000000021
            }
         },
         {
            "name":"TIBACUY",
            "value":"805",
            "text":"COLOMBIA, CUNDINAMARCA, TIBACUY",
            "code":"CO-25-805",
            "geoPoint":{
               "lat":4.347270000000037,
               "lon":-74.45239999999995
            },
            "geoRectangle":{
               "Xmin":-74.51739999999995,
               "Xmax":-74.38739999999996,
               "Ymin":4.282270000000037,
               "Ymax":4.412270000000038
            }
         },
         {
            "name":"ÚTICA",
            "value":"851",
            "text":"COLOMBIA, CUNDINAMARCA, ÚTICA",
            "code":"CO-25-851",
            "geoPoint":{
               "lat":5.187960000000032,
               "lon":-74.47995999999995
            },
            "geoRectangle":{
               "Xmin":-74.53995999999995,
               "Xmax":-74.41995999999995,
               "Ymin":5.127960000000033,
               "Ymax":5.247960000000032
            }
         },
         {
            "name":"ZIPACÓN",
            "value":"898",
            "text":"COLOMBIA, CUNDINAMARCA, ZIPACÓN",
            "code":"CO-25-898",
            "geoPoint":{
               "lat":4.759140000000059,
               "lon":-74.38044999999994
            },
            "geoRectangle":{
               "Xmin":-74.42944999999995,
               "Xmax":-74.33144999999993,
               "Ymin":4.710140000000059,
               "Ymax":4.808140000000059
            }
         },
         {
            "name":"GUASCA",
            "value":"322",
            "text":"COLOMBIA, CUNDINAMARCA, GUASCA",
            "code":"CO-25-322",
            "geoPoint":{
               "lat":4.866620000000069,
               "lon":-73.87732999999997
            },
            "geoRectangle":{
               "Xmin":-73.99332999999997,
               "Xmax":-73.76132999999997,
               "Ymin":4.750620000000069,
               "Ymax":4.982620000000068
            }
         },
         {
            "name":"GUATAQUÍ",
            "value":"324",
            "text":"COLOMBIA, CUNDINAMARCA, GUATAQUÍ",
            "code":"CO-25-324",
            "geoPoint":{
               "lat":4.517360000000053,
               "lon":-74.78965999999997
            },
            "geoRectangle":{
               "Xmin":-74.85365999999996,
               "Xmax":-74.72565999999998,
               "Ymin":4.453360000000053,
               "Ymax":4.581360000000053
            }
         },
         {
            "name":" GUATAVITA",
            "value":"326",
            "text":"COLOMBIA, CUNDINAMARCA,  GUATAVITA",
            "code":"CO-25-326",
            "geoPoint":{
               "lat":4.934440000000052,
               "lon":-73.83133999999995
            },
            "geoRectangle":{
               "Xmin":-73.93133999999995,
               "Xmax":-73.73133999999996,
               "Ymin":4.834440000000052,
               "Ymax":5.0344400000000515
            }
         },
         {
            "name":"TENA",
            "value":"797",
            "text":"COLOMBIA, CUNDINAMARCA, TENA",
            "code":"CO-25-797",
            "geoPoint":{
               "lat":4.655970000000025,
               "lon":-74.38988999999998
            },
            "geoRectangle":{
               "Xmin":-74.44188999999999,
               "Xmax":-74.33788999999997,
               "Ymin":4.603970000000025,
               "Ymax":4.707970000000024
            }
         },
         {
            "name":"UBATE",
            "value":"843",
            "text":"COLOMBIA, CUNDINAMARCA, UBATE",
            "code":"CO-25-843",
            "geoPoint":{
               "lat":4.576770000000067,
               "lon":-74.21705999999995
            },
            "geoRectangle":{
               "Xmin":-74.22705999999995,
               "Xmax":-74.20705999999994,
               "Ymin":4.5667700000000675,
               "Ymax":4.586770000000067
            }
         },
         {
            "name":"YACOPÍ",
            "value":"885",
            "text":"COLOMBIA, CUNDINAMARCA, YACOPÍ",
            "code":"CO-25-885",
            "geoPoint":{
               "lat":5.458210000000065,
               "lon":-74.33760999999998
            },
            "geoRectangle":{
               "Xmin":-74.54160999999998,
               "Xmax":-74.13360999999999,
               "Ymin":5.254210000000065,
               "Ymax":5.662210000000065
            }
         },
         {
            "name":"GUAYABAL DE SÍQUIMA",
            "value":"328",
            "text":"COLOMBIA, CUNDINAMARCA, GUAYABAL DE SÍQUIMA",
            "code":"CO-25-328",
            "geoPoint":{
               "lat":4.877390000000048,
               "lon":-74.46743999999995
            },
            "geoRectangle":{
               "Xmin":-74.48243999999995,
               "Xmax":-74.45243999999995,
               "Ymin":4.862390000000048,
               "Ymax":4.892390000000048
            }
         },
         {
            "name":"GUAYABETAL",
            "value":"335",
            "text":"COLOMBIA, CUNDINAMARCA, GUAYABETAL",
            "code":"CO-25-335",
            "geoPoint":{
               "lat":4.21568000000002,
               "lon":-73.81476999999995
            },
            "geoRectangle":{
               "Xmin":-73.91476999999995,
               "Xmax":-73.71476999999996,
               "Ymin":4.115680000000021,
               "Ymax":4.31568000000002
            }
         },
         {
            "name":"JERUSALÉN",
            "value":"368",
            "text":"COLOMBIA, CUNDINAMARCA, JERUSALÉN",
            "code":"CO-25-368",
            "geoPoint":{
               "lat":4.563400000000058,
               "lon":-74.69545999999997
            },
            "geoRectangle":{
               "Xmin":-74.78945999999996,
               "Xmax":-74.60145999999997,
               "Ymin":4.469400000000058,
               "Ymax":4.657400000000059
            }
         },
         {
            "name":"GUACHETÁ",
            "value":"317",
            "text":"COLOMBIA, CUNDINAMARCA, GUACHETÁ",
            "code":"CO-25-317",
            "geoPoint":{
               "lat":5.384750000000054,
               "lon":-73.68667999999997
            },
            "geoRectangle":{
               "Xmin":-73.76767999999997,
               "Xmax":-73.60567999999996,
               "Ymin":5.303750000000053,
               "Ymax":5.465750000000054
            }
         },
         {
            "name":"JUNÍN",
            "value":"372",
            "text":"COLOMBIA, CUNDINAMARCA, JUNÍN",
            "code":"CO-25-372",
            "geoPoint":{
               "lat":4.790870000000041,
               "lon":-73.66238999999996
            },
            "geoRectangle":{
               "Xmin":-73.78238999999996,
               "Xmax":-73.54238999999995,
               "Ymin":4.670870000000041,
               "Ymax":4.910870000000041
            }
         },
         {
            "name":"LA CALERA",
            "value":"377",
            "text":"COLOMBIA, CUNDINAMARCA, LA CALERA",
            "code":"CO-25-377",
            "geoPoint":{
               "lat":4.721750000000043,
               "lon":-73.96788999999995
            },
            "geoRectangle":{
               "Xmin":-74.08388999999995,
               "Xmax":-73.85188999999995,
               "Ymin":4.605750000000043,
               "Ymax":4.837750000000042
            }
         },
         {
            "name":"LA MESA",
            "value":"386",
            "text":"COLOMBIA, CUNDINAMARCA, LA MESA",
            "code":"CO-25-386",
            "geoPoint":{
               "lat":4.629320000000064,
               "lon":-74.46624999999995
            },
            "geoRectangle":{
               "Xmin":-74.54324999999994,
               "Xmax":-74.38924999999995,
               "Ymin":4.552320000000064,
               "Ymax":4.706320000000064
            }
         },
         {
            "name":"LA PALMA",
            "value":"394",
            "text":"COLOMBIA, CUNDINAMARCA, LA PALMA",
            "code":"CO-25-394",
            "geoPoint":{
               "lat":5.360370000000046,
               "lon":-74.39028999999994
            },
            "geoRectangle":{
               "Xmin":-74.47728999999994,
               "Xmax":-74.30328999999993,
               "Ymin":5.273370000000046,
               "Ymax":5.447370000000046
            }
         },
         {
            "name":"LA PEÑA",
            "value":"398",
            "text":"COLOMBIA, CUNDINAMARCA, LA PEÑA",
            "code":"CO-25-398",
            "geoPoint":{
               "lat":5.19876000000005,
               "lon":-74.39367999999996
            },
            "geoRectangle":{
               "Xmin":-74.45967999999996,
               "Xmax":-74.32767999999996,
               "Ymin":5.13276000000005,
               "Ymax":5.26476000000005
            }
         },
         {
            "name":"LA VEGA",
            "value":"402",
            "text":"COLOMBIA, CUNDINAMARCA, LA VEGA",
            "code":"CO-25-402",
            "geoPoint":{
               "lat":4.994600000000048,
               "lon":-74.33782999999994
            },
            "geoRectangle":{
               "Xmin":-74.41382999999993,
               "Xmax":-74.26182999999995,
               "Ymin":4.9186000000000485,
               "Ymax":5.070600000000048
            }
         },
         {
            "name":"GRANADA",
            "value":"312",
            "text":"COLOMBIA, CUNDINAMARCA, GRANADA",
            "code":"CO-25-312",
            "geoPoint":{
               "lat":4.519270000000063,
               "lon":-74.35128999999995
            },
            "geoRectangle":{
               "Xmin":-74.39728999999996,
               "Xmax":-74.30528999999994,
               "Ymin":4.4732700000000625,
               "Ymax":4.565270000000063
            }
         },
         {
            "name":"GIRARDOT",
            "value":"307",
            "text":"COLOMBIA, CUNDINAMARCA, GIRARDOT",
            "code":"CO-25-307",
            "geoPoint":{
               "lat":4.296790000000044,
               "lon":-74.80550999999997
            },
            "geoRectangle":{
               "Xmin":-74.88450999999996,
               "Xmax":-74.72650999999998,
               "Ymin":4.217790000000044,
               "Ymax":4.375790000000044
            }
         },
         {
            "name":"GAMA",
            "value":"299",
            "text":"COLOMBIA, CUNDINAMARCA, GAMA",
            "code":"CO-25-299",
            "geoPoint":{
               "lat":4.7629100000000335,
               "lon":-73.61082999999996
            },
            "geoRectangle":{
               "Xmin":-73.67482999999996,
               "Xmax":-73.54682999999997,
               "Ymin":4.6989100000000334,
               "Ymax":4.826910000000034
            }
         },
         {
            "name":"GACHETÁ",
            "value":"297",
            "text":"COLOMBIA, CUNDINAMARCA, GACHETÁ",
            "code":"CO-25-297",
            "geoPoint":{
               "lat":4.8176500000000715,
               "lon":-73.63613999999995
            },
            "geoRectangle":{
               "Xmin":-73.73613999999995,
               "Xmax":-73.53613999999996,
               "Ymin":4.717650000000072,
               "Ymax":4.917650000000071
            }
         },
         {
            "name":" LENGUAZAQUE",
            "value":"407",
            "text":"COLOMBIA, CUNDINAMARCA,  LENGUAZAQUE",
            "code":"CO-25-407",
            "geoPoint":{
               "lat":5.306560000000047,
               "lon":-73.71172999999999
            },
            "geoRectangle":{
               "Xmin":-73.79272999999999,
               "Xmax":-73.63072999999999,
               "Ymin":5.225560000000047,
               "Ymax":5.387560000000048
            }
         },
         {
            "name":"MACHETÁ",
            "value":"426",
            "text":"COLOMBIA, CUNDINAMARCA, MACHETÁ",
            "code":"CO-25-426",
            "geoPoint":{
               "lat":5.081540000000075,
               "lon":-73.60760999999997
            },
            "geoRectangle":{
               "Xmin":-73.62260999999997,
               "Xmax":-73.59260999999996,
               "Ymin":5.066540000000075,
               "Ymax":5.096540000000075
            }
         },
         {
            "name":"MADRID",
            "value":"430",
            "text":"COLOMBIA, CUNDINAMARCA, MADRID",
            "code":"CO-25-430",
            "geoPoint":{
               "lat":4.734090000000037,
               "lon":-74.26496999999995
            },
            "geoRectangle":{
               "Xmin":-74.34696999999994,
               "Xmax":-74.18296999999995,
               "Ymin":4.652090000000038,
               "Ymax":4.816090000000037
            }
         },
         {
            "name":"GACHANCIPÁ",
            "value":"295",
            "text":"COLOMBIA, CUNDINAMARCA, GACHANCIPÁ",
            "code":"CO-25-295",
            "geoPoint":{
               "lat":4.992600000000039,
               "lon":-73.87127999999996
            },
            "geoRectangle":{
               "Xmin":-73.91127999999996,
               "Xmax":-73.83127999999995,
               "Ymin":4.9526000000000385,
               "Ymax":5.032600000000039
            }
         },
         {
            "name":"GACHALÁ",
            "value":"293",
            "text":"COLOMBIA, CUNDINAMARCA, GACHALÁ",
            "code":"CO-25-293",
            "geoPoint":{
               "lat":4.692440000000033,
               "lon":-73.52041999999994
            },
            "geoRectangle":{
               "Xmin":-73.53541999999995,
               "Xmax":-73.50541999999994,
               "Ymin":4.677440000000034,
               "Ymax":4.707440000000033
            }
         },
         {
            "name":" FUQUENE",
            "value":"288",
            "text":"COLOMBIA, CUNDINAMARCA,  FUQUENE",
            "code":"CO-25-288",
            "geoPoint":{
               "lat":5.404430000000048,
               "lon":-73.79645999999997
            },
            "geoRectangle":{
               "Xmin":-73.86145999999997,
               "Xmax":-73.73145999999997,
               "Ymin":5.339430000000047,
               "Ymax":5.469430000000048
            }
         },
         {
            "name":"GUADUAS",
            "value":"320",
            "text":"COLOMBIA, CUNDINAMARCA, GUADUAS",
            "code":"CO-25-320",
            "geoPoint":{
               "lat":5.067010000000039,
               "lon":-74.59771999999998
            },
            "geoRectangle":{
               "Xmin":-74.77771999999999,
               "Xmax":-74.41771999999997,
               "Ymin":4.887010000000039,
               "Ymax":5.247010000000039
            }
         },
         {
            "name":"MANTA",
            "value":"436",
            "text":"COLOMBIA, CUNDINAMARCA, MANTA",
            "code":"CO-25-436",
            "geoPoint":{
               "lat":5.009270000000072,
               "lon":-73.54053999999996
            },
            "geoRectangle":{
               "Xmin":-73.60753999999996,
               "Xmax":-73.47353999999997,
               "Ymin":4.942270000000072,
               "Ymax":5.076270000000072
            }
         },
         {
            "name":"MEDINA",
            "value":"438",
            "text":"COLOMBIA, CUNDINAMARCA, MEDINA",
            "code":"CO-25-438",
            "geoPoint":{
               "lat":4.509200000000021,
               "lon":-73.35096999999996
            },
            "geoRectangle":{
               "Xmin":-73.55496999999995,
               "Xmax":-73.14696999999997,
               "Ymin":4.3052000000000215,
               "Ymax":4.713200000000021
            }
         },
         {
            "name":"MOSQUERA",
            "value":"473",
            "text":"COLOMBIA, CUNDINAMARCA, MOSQUERA",
            "code":"CO-25-473",
            "geoPoint":{
               "lat":4.7129900000000475,
               "lon":-74.23847999999998
            },
            "geoRectangle":{
               "Xmin":-74.30247999999997,
               "Xmax":-74.17447999999999,
               "Ymin":4.6489900000000475,
               "Ymax":4.776990000000048
            }
         },
         {
            "name":"FUSAGASUGÁ",
            "value":"290",
            "text":"COLOMBIA, CUNDINAMARCA, FUSAGASUGÁ",
            "code":"CO-25-290",
            "geoPoint":{
               "lat":4.326760000000036,
               "lon":-74.40867999999995
            },
            "geoRectangle":{
               "Xmin":-74.52567999999995,
               "Xmax":-74.29167999999994,
               "Ymin":4.209760000000036,
               "Ymax":4.443760000000036
            }
         },
         {
            "name":"CABRERA",
            "value":"120",
            "text":"COLOMBIA, CUNDINAMARCA, CABRERA",
            "code":"CO-25-120",
            "geoPoint":{
               "lat":3.9850600000000327,
               "lon":-74.48431999999997
            },
            "geoRectangle":{
               "Xmin":-74.60931999999997,
               "Xmax":-74.35931999999997,
               "Ymin":3.8600600000000327,
               "Ymax":4.110060000000033
            }
         },
         {
            "name":"VENECIA",
            "value":"506",
            "text":"COLOMBIA, CUNDINAMARCA, VENECIA",
            "code":"CO-25-506",
            "geoPoint":{
               "lat":4.089710000000025,
               "lon":-74.47801999999996
            },
            "geoRectangle":{
               "Xmin":-74.54201999999995,
               "Xmax":-74.41401999999997,
               "Ymin":4.025710000000025,
               "Ymax":4.153710000000025
            }
         },
         {
            "name":"PASCA",
            "value":"535",
            "text":"COLOMBIA, CUNDINAMARCA, PASCA",
            "code":"CO-25-535",
            "geoPoint":{
               "lat":4.308940000000064,
               "lon":-74.30263999999994
            },
            "geoRectangle":{
               "Xmin":-74.39163999999994,
               "Xmax":-74.21363999999994,
               "Ymin":4.219940000000063,
               "Ymax":4.397940000000064
            }
         },
         {
            "name":"PANDI",
            "value":"524",
            "text":"COLOMBIA, CUNDINAMARCA, PANDI",
            "code":"CO-25-524",
            "geoPoint":{
               "lat":4.1907900000000495,
               "lon":-74.48735999999997
            },
            "geoRectangle":{
               "Xmin":-74.54335999999996,
               "Xmax":-74.43135999999997,
               "Ymin":4.1347900000000495,
               "Ymax":4.24679000000005
            }
         },
         {
            "name":"SAN BERNARDO",
            "value":"649",
            "text":"COLOMBIA, CUNDINAMARCA, SAN BERNARDO",
            "code":"CO-25-649",
            "geoPoint":{
               "lat":4.179680000000076,
               "lon":-74.42313999999999
            },
            "geoRectangle":{
               "Xmin":-74.52814,
               "Xmax":-74.31813999999999,
               "Ymin":4.074680000000075,
               "Ymax":4.284680000000076
            }
         },
         {
            "name":"ARBELÁEZ",
            "value":"053",
            "text":"COLOMBIA, CUNDINAMARCA, ARBELÁEZ",
            "code":"CO-25-053",
            "geoPoint":{
               "lat":4.2750800000000595,
               "lon":-74.41245999999995
            },
            "geoRectangle":{
               "Xmin":-74.51745999999996,
               "Xmax":-74.30745999999995,
               "Ymin":4.170080000000059,
               "Ymax":4.38008000000006
            }
         },
         {
            "name":"GUTIERREZ",
            "value":"339",
            "text":"COLOMBIA, CUNDINAMARCA, GUTIERREZ",
            "code":"CO-25-339",
            "geoPoint":{
               "lat":4.2535100000000625,
               "lon":-74.00386999999995
            },
            "geoRectangle":{
               "Xmin":-74.14186999999995,
               "Xmax":-73.86586999999994,
               "Ymin":4.115510000000063,
               "Ymax":4.391510000000062
            }
         },
         {
            "name":"SILVANIA",
            "value":"743",
            "text":"COLOMBIA, CUNDINAMARCA, SILVANIA",
            "code":"CO-25-743",
            "geoPoint":{
               "lat":4.403530000000046,
               "lon":-74.38388999999995
            },
            "geoRectangle":{
               "Xmin":-74.45888999999995,
               "Xmax":-74.30888999999995,
               "Ymin":4.328530000000046,
               "Ymax":4.478530000000046
            }
         }
      ]
   },
   {
      "name":"GUAINÍA",
      "value":"94",
      "text":"COLOMBIA, GUAINÍA",
      "code":"CO-94",
      "geoPoint":{
         "lat":2.728843578000067,
         "lon":-68.81701066999995
      },
      "geoRectangle":{
         "Xmin":-70.55301066999995,
         "Xmax":-67.08101066999994,
         "Ymin":0.9928435780000668,
         "Ymax":4.464843578000067
      },
      "municipality":[
         {
            "name":"INÍRIDA",
            "value":"001",
            "text":"COLOMBIA, GUAINÍA, INÍRIDA",
            "code":"CO-94-001",
            "geoPoint":{
               "lat":3.8711900000000696,
               "lon":-67.92249999999996
            },
            "geoRectangle":{
               "Xmin":-68.80749999999996,
               "Xmax":-67.03749999999995,
               "Ymin":2.98619000000007,
               "Ymax":4.756190000000069
            }
         },
         {
            "name":"BARRANCO MINA",
            "value":"343",
            "text":"COLOMBIA, GUAINÍA, BARRANCO MINA",
            "code":"CO-94-343",
            "geoPoint":{
               "lat":3.4941800000000285,
               "lon":-69.81452999999993
            },
            "geoRectangle":{
               "Xmin":-70.46952999999993,
               "Xmax":-69.15952999999993,
               "Ymin":2.8391800000000282,
               "Ymax":4.149180000000029
            }
         },
         {
            "name":"MAPIRIPANA",
            "value":"663",
            "text":"COLOMBIA, GUAINÍA, MAPIRIPANA",
            "code":"CO-94-663",
            "geoPoint":{
               "lat":2.8121600000000626,
               "lon":-70.47642999999994
            },
            "geoRectangle":{
               "Xmin":-71.01742999999993,
               "Xmax":-69.93542999999994,
               "Ymin":2.2711600000000627,
               "Ymax":3.3531600000000625
            }
         },
         {
            "name":"SAN FELIPE",
            "value":"883",
            "text":"COLOMBIA, GUAINÍA, SAN FELIPE",
            "code":"CO-94-883",
            "geoPoint":{
               "lat":1.913220000000024,
               "lon":-67.06768999999997
            },
            "geoRectangle":{
               "Xmin":-67.46868999999997,
               "Xmax":-66.66668999999997,
               "Ymin":1.5122200000000239,
               "Ymax":2.3142200000000237
            }
         },
         {
            "name":"PUERTO COLOMBIA",
            "value":"884",
            "text":"COLOMBIA, GUAINÍA, PUERTO COLOMBIA",
            "code":"CO-94-884",
            "geoPoint":{
               "lat":2.7191800000000512,
               "lon":-67.56644999999997
            },
            "geoRectangle":{
               "Xmin":-68.37444999999998,
               "Xmax":-66.75844999999997,
               "Ymin":1.9111800000000512,
               "Ymax":3.527180000000051
            }
         },
         {
            "name":"LA GUADALUPE",
            "value":"885",
            "text":"COLOMBIA, GUAINÍA, LA GUADALUPE",
            "code":"CO-94-885",
            "geoPoint":{
               "lat":1.2468300000000454,
               "lon":-66.86423999999994
            },
            "geoRectangle":{
               "Xmin":-67.07923999999994,
               "Xmax":-66.64923999999993,
               "Ymin":1.0318300000000453,
               "Ymax":1.4618300000000455
            }
         },
         {
            "name":"CACAHUAL",
            "value":"886",
            "text":"COLOMBIA, GUAINÍA, CACAHUAL",
            "code":"CO-94-886",
            "geoPoint":{
               "lat":3.525600000000054,
               "lon":-67.41272999999995
            },
            "geoRectangle":{
               "Xmin":-67.71372999999996,
               "Xmax":-67.11172999999995,
               "Ymin":3.224600000000054,
               "Ymax":3.826600000000054
            }
         },
         {
            "name":"PANÁ-PANÁ (Campo Alegre)",
            "value":"887",
            "text":"COLOMBIA, GUAINÍA, PANÁ-PANÁ (Campo Alegre)",
            "code":"CO-94-887",
            "geoPoint":{
               "lat":1.867650000000026,
               "lon":-69.00900999999993
            },
            "geoRectangle":{
               "Xmin":-69.69500999999994,
               "Xmax":-68.32300999999993,
               "Ymin":1.181650000000026,
               "Ymax":2.553650000000026
            }
         },
         {
            "name":"MORICHAL (Morichal Nuevo)",
            "value":"888",
            "text":"COLOMBIA, GUAINÍA, MORICHAL (Morichal Nuevo)",
            "code":"CO-94-888",
            "geoPoint":{
               "lat":2.2629400000000714,
               "lon":-69.91863999999998
            },
            "geoRectangle":{
               "Xmin":-70.49563999999998,
               "Xmax":-69.34163999999998,
               "Ymin":1.6859400000000715,
               "Ymax":2.8399400000000714
            }
         }
      ]
   },
   {
      "name":"GUAVIARE",
      "value":"95",
      "text":"COLOMBIA, GUAVIARE",
      "code":"CO-95",
      "geoPoint":{
         "lat":1.9260543910000365,
         "lon":-72.12750026199996
      },
      "geoRectangle":{
         "Xmin":-73.61050026199996,
         "Xmax":-70.64450026199995,
         "Ymin":0.4430543910000364,
         "Ymax":3.4090543910000366
      },
      "municipality":[
         {
            "name":"SAN JOSÉ DEL GUAVIARE",
            "value":"001",
            "text":"COLOMBIA, GUAVIARE, SAN JOSÉ DEL GUAVIARE",
            "code":"CO-95-001",
            "geoPoint":{
               "lat":2.5687200000000416,
               "lon":-72.64089999999999
            },
            "geoRectangle":{
               "Xmin":-73.64389999999999,
               "Xmax":-71.63789999999999,
               "Ymin":1.5657200000000415,
               "Ymax":3.5717200000000418
            }
         },
         {
            "name":"MIRAFLORES",
            "value":"200",
            "text":"COLOMBIA, GUAVIARE, MIRAFLORES",
            "code":"CO-95-200",
            "geoPoint":{
               "lat":1.3356000000000563,
               "lon":-71.95045999999996
            },
            "geoRectangle":{
               "Xmin":-72.62045999999997,
               "Xmax":-71.28045999999996,
               "Ymin":0.6656000000000563,
               "Ymax":2.0056000000000562
            }
         },
         {
            "name":"CALAMAR",
            "value":"015",
            "text":"COLOMBIA, GUAVIARE, CALAMAR",
            "code":"CO-95-015",
            "geoPoint":{
               "lat":1.9590400000000727,
               "lon":-72.65564999999998
            },
            "geoRectangle":{
               "Xmin":-73.32164999999998,
               "Xmax":-71.98964999999998,
               "Ymin":1.2930400000000728,
               "Ymax":2.6250400000000726
            }
         },
         {
            "name":"EL RETORNO",
            "value":"025",
            "text":"COLOMBIA, GUAVIARE, EL RETORNO",
            "code":"CO-95-025",
            "geoPoint":{
               "lat":2.331860000000063,
               "lon":-72.62699999999995
            },
            "geoRectangle":{
               "Xmin":-73.56499999999996,
               "Xmax":-71.68899999999995,
               "Ymin":1.3938600000000627,
               "Ymax":3.269860000000063
            }
         }
      ]
   },
   {
      "name":"HUILA",
      "value":"41",
      "text":"COLOMBIA, HUILA",
      "code":"CO-41",
      "geoPoint":{
         "lat":2.5564175980000527,
         "lon":-75.59508255999998
      },
      "geoRectangle":{
         "Xmin":-76.71508255999998,
         "Xmax":-74.47508255999998,
         "Ymin":1.4364175980000526,
         "Ymax":3.676417598000053
      },
      "municipality":[
         {
            "name":"NEIVA",
            "value":"001",
            "text":"COLOMBIA, HUILA, NEIVA",
            "code":"CO-41-001",
            "geoPoint":{
               "lat":2.9278600000000665,
               "lon":-75.27896999999996
            },
            "geoRectangle":{
               "Xmin":-75.58396999999997,
               "Xmax":-74.97396999999995,
               "Ymin":2.6228600000000664,
               "Ymax":3.2328600000000667
            }
         },
         {
            "name":"GIGANTE",
            "value":"306",
            "text":"COLOMBIA, HUILA, GIGANTE",
            "code":"CO-41-306",
            "geoPoint":{
               "lat":2.387770000000046,
               "lon":-75.54791999999998
            },
            "geoRectangle":{
               "Xmin":-75.69891999999997,
               "Xmax":-75.39691999999998,
               "Ymin":2.236770000000046,
               "Ymax":2.5387700000000457
            }
         },
         {
            "name":"GUADALUPE",
            "value":"319",
            "text":"COLOMBIA, HUILA, GUADALUPE",
            "code":"CO-41-319",
            "geoPoint":{
               "lat":2.0251100000000406,
               "lon":-75.75631999999996
            },
            "geoRectangle":{
               "Xmin":-75.86031999999996,
               "Xmax":-75.65231999999996,
               "Ymin":1.9211100000000405,
               "Ymax":2.1291100000000407
            }
         },
         {
            "name":"HOBO",
            "value":"349",
            "text":"COLOMBIA, HUILA, HOBO",
            "code":"CO-41-349",
            "geoPoint":{
               "lat":2.580470000000048,
               "lon":-75.44815999999997
            },
            "geoRectangle":{
               "Xmin":-75.53815999999998,
               "Xmax":-75.35815999999997,
               "Ymin":2.490470000000048,
               "Ymax":2.670470000000048
            }
         },
         {
            "name":"ÍQUIRA",
            "value":"357",
            "text":"COLOMBIA, HUILA, ÍQUIRA",
            "code":"CO-41-357",
            "geoPoint":{
               "lat":2.6486700000000383,
               "lon":-75.63456999999994
            },
            "geoRectangle":{
               "Xmin":-75.64956999999994,
               "Xmax":-75.61956999999994,
               "Ymin":2.633670000000038,
               "Ymax":2.6636700000000384
            }
         },
         {
            "name":"ISNOS",
            "value":"359",
            "text":"COLOMBIA, HUILA, ISNOS",
            "code":"CO-41-359",
            "geoPoint":{
               "lat":1.9317400000000475,
               "lon":-76.21605999999997
            },
            "geoRectangle":{
               "Xmin":-76.40405999999997,
               "Xmax":-76.02805999999997,
               "Ymin":1.7437400000000476,
               "Ymax":2.1197400000000477
            }
         },
         {
            "name":"LA ARGENTINA",
            "value":"378",
            "text":"COLOMBIA, HUILA, LA ARGENTINA",
            "code":"CO-41-378",
            "geoPoint":{
               "lat":2.199260000000038,
               "lon":-75.97965999999997
            },
            "geoRectangle":{
               "Xmin":-76.13865999999997,
               "Xmax":-75.82065999999996,
               "Ymin":2.040260000000038,
               "Ymax":2.3582600000000378
            }
         },
         {
            "name":"LA PLATA",
            "value":"396",
            "text":"COLOMBIA, HUILA, LA PLATA",
            "code":"CO-41-396",
            "geoPoint":{
               "lat":2.389590000000055,
               "lon":-75.89107999999999
            },
            "geoRectangle":{
               "Xmin":-76.07207999999999,
               "Xmax":-75.71007999999999,
               "Ymin":2.208590000000055,
               "Ymax":2.570590000000055
            }
         },
         {
            "name":"NÁTAGA",
            "value":"483",
            "text":"COLOMBIA, HUILA, NÁTAGA",
            "code":"CO-41-483",
            "geoPoint":{
               "lat":2.543730000000039,
               "lon":-75.80809999999997
            },
            "geoRectangle":{
               "Xmin":-75.88909999999997,
               "Xmax":-75.72709999999996,
               "Ymin":2.462730000000039,
               "Ymax":2.624730000000039
            }
         },
         {
            "name":"PALERMO",
            "value":"524",
            "text":"COLOMBIA, HUILA, PALERMO",
            "code":"CO-41-524",
            "geoPoint":{
               "lat":2.888180000000034,
               "lon":-75.43424999999996
            },
            "geoRectangle":{
               "Xmin":-75.61324999999997,
               "Xmax":-75.25524999999996,
               "Ymin":2.709180000000034,
               "Ymax":3.0671800000000338
            }
         },
         {
            "name":"TELLO",
            "value":"799",
            "text":"COLOMBIA, HUILA, TELLO",
            "code":"CO-41-799",
            "geoPoint":{
               "lat":3.0666400000000635,
               "lon":-75.13863999999995
            },
            "geoRectangle":{
               "Xmin":-75.29363999999995,
               "Xmax":-74.98363999999995,
               "Ymin":2.9116400000000637,
               "Ymax":3.2216400000000633
            }
         },
         {
            "name":"TERUEL",
            "value":"801",
            "text":"COLOMBIA, HUILA, TERUEL",
            "code":"CO-41-801",
            "geoPoint":{
               "lat":2.7428500000000327,
               "lon":-75.56730999999996
            },
            "geoRectangle":{
               "Xmin":-75.65730999999997,
               "Xmax":-75.47730999999996,
               "Ymin":2.652850000000033,
               "Ymax":2.8328500000000325
            }
         },
         {
            "name":"TIMANÁ",
            "value":"807",
            "text":"COLOMBIA, HUILA, TIMANÁ",
            "code":"CO-41-807",
            "geoPoint":{
               "lat":1.9738100000000713,
               "lon":-75.93233999999995
            },
            "geoRectangle":{
               "Xmin":-76.02933999999995,
               "Xmax":-75.83533999999996,
               "Ymin":1.8768100000000714,
               "Ymax":2.0708100000000713
            }
         },
         {
            "name":"VILLAVIEJA",
            "value":"872",
            "text":"COLOMBIA, HUILA, VILLAVIEJA",
            "code":"CO-41-872",
            "geoPoint":{
               "lat":3.217990000000043,
               "lon":-75.21721999999994
            },
            "geoRectangle":{
               "Xmin":-75.36321999999994,
               "Xmax":-75.07121999999994,
               "Ymin":3.071990000000043,
               "Ymax":3.363990000000043
            }
         },
         {
            "name":"OPORAPA",
            "value":"503",
            "text":"COLOMBIA, HUILA, OPORAPA",
            "code":"CO-41-503",
            "geoPoint":{
               "lat":2.0270700000000375,
               "lon":-75.99288999999999
            },
            "geoRectangle":{
               "Xmin":-76.07088999999999,
               "Xmax":-75.91488999999999,
               "Ymin":1.9490700000000374,
               "Ymax":2.1050700000000373
            }
         },
         {
            "name":"PAICOL",
            "value":"518",
            "text":"COLOMBIA, HUILA, PAICOL",
            "code":"CO-41-518",
            "geoPoint":{
               "lat":2.4494600000000446,
               "lon":-75.77414999999996
            },
            "geoRectangle":{
               "Xmin":-75.88414999999996,
               "Xmax":-75.66414999999996,
               "Ymin":2.3394600000000447,
               "Ymax":2.5594600000000445
            }
         },
         {
            "name":"PALESTINA",
            "value":"530",
            "text":"COLOMBIA, HUILA, PALESTINA",
            "code":"CO-41-530",
            "geoPoint":{
               "lat":1.7232600000000389,
               "lon":-76.13300999999996
            },
            "geoRectangle":{
               "Xmin":-76.22800999999995,
               "Xmax":-76.03800999999996,
               "Ymin":1.628260000000039,
               "Ymax":1.8182600000000388
            }
         },
         {
            "name":"PITAL",
            "value":"548",
            "text":"COLOMBIA, HUILA, PITAL",
            "code":"CO-41-548",
            "geoPoint":{
               "lat":2.2668500000000336,
               "lon":-75.80615999999998
            },
            "geoRectangle":{
               "Xmin":-75.90715999999998,
               "Xmax":-75.70515999999998,
               "Ymin":2.1658500000000336,
               "Ymax":2.3678500000000335
            }
         },
         {
            "name":"PITALITO",
            "value":"551",
            "text":"COLOMBIA, HUILA, PITALITO",
            "code":"CO-41-551",
            "geoPoint":{
               "lat":1.8508500000000367,
               "lon":-76.04590999999994
            },
            "geoRectangle":{
               "Xmin":-76.21490999999993,
               "Xmax":-75.87690999999994,
               "Ymin":1.6818500000000367,
               "Ymax":2.019850000000037
            }
         },
         {
            "name":" RIVERA",
            "value":"615",
            "text":"COLOMBIA, HUILA,  RIVERA",
            "code":"CO-41-615",
            "geoPoint":{
               "lat":2.778820000000053,
               "lon":-75.25818999999996
            },
            "geoRectangle":{
               "Xmin":-75.36818999999996,
               "Xmax":-75.14818999999996,
               "Ymin":2.668820000000053,
               "Ymax":2.888820000000053
            }
         },
         {
            "name":"SALADOBLANCO",
            "value":"660",
            "text":"COLOMBIA, HUILA, SALADOBLANCO",
            "code":"CO-41-660",
            "geoPoint":{
               "lat":1.9919800000000691,
               "lon":-76.04598999999996
            },
            "geoRectangle":{
               "Xmin":-76.21798999999996,
               "Xmax":-75.87398999999996,
               "Ymin":1.8199800000000692,
               "Ymax":2.1639800000000693
            }
         },
         {
            "name":"SAN AGUSTÍN",
            "value":"668",
            "text":"COLOMBIA, HUILA, SAN AGUSTÍN",
            "code":"CO-41-668",
            "geoPoint":{
               "lat":1.8822300000000496,
               "lon":-76.27283999999997
            },
            "geoRectangle":{
               "Xmin":-76.55383999999998,
               "Xmax":-75.99183999999997,
               "Ymin":1.6012300000000494,
               "Ymax":2.1632300000000497
            }
         },
         {
            "name":"SANTA MARÍA",
            "value":"676",
            "text":"COLOMBIA, HUILA, SANTA MARÍA",
            "code":"CO-41-676",
            "geoPoint":{
               "lat":2.937910000000045,
               "lon":-75.58647999999994
            },
            "geoRectangle":{
               "Xmin":-75.71147999999994,
               "Xmax":-75.46147999999994,
               "Ymin":2.812910000000045,
               "Ymax":3.062910000000045
            }
         },
         {
            "name":"SUAZA",
            "value":"770",
            "text":"COLOMBIA, HUILA, SUAZA",
            "code":"CO-41-770",
            "geoPoint":{
               "lat":1.9769500000000448,
               "lon":-75.79546999999997
            },
            "geoRectangle":{
               "Xmin":-75.93046999999997,
               "Xmax":-75.66046999999996,
               "Ymin":1.8419500000000448,
               "Ymax":2.1119500000000446
            }
         },
         {
            "name":"TARQUI",
            "value":"791",
            "text":"COLOMBIA, HUILA, TARQUI",
            "code":"CO-41-791",
            "geoPoint":{
               "lat":2.113510000000076,
               "lon":-75.82494999999994
            },
            "geoRectangle":{
               "Xmin":-75.93294999999995,
               "Xmax":-75.71694999999994,
               "Ymin":2.005510000000076,
               "Ymax":2.2215100000000763
            }
         },
         {
            "name":"TESALIA",
            "value":"797",
            "text":"COLOMBIA, HUILA, TESALIA",
            "code":"CO-41-797",
            "geoPoint":{
               "lat":2.486630000000048,
               "lon":-75.73095999999998
            },
            "geoRectangle":{
               "Xmin":-75.85295999999998,
               "Xmax":-75.60895999999998,
               "Ymin":2.364630000000048,
               "Ymax":2.6086300000000477
            }
         },
         {
            "name":"YAGUARÁ",
            "value":"885",
            "text":"COLOMBIA, HUILA, YAGUARÁ",
            "code":"CO-41-885",
            "geoPoint":{
               "lat":2.6639900000000694,
               "lon":-75.51892999999995
            },
            "geoRectangle":{
               "Xmin":-75.62892999999995,
               "Xmax":-75.40892999999996,
               "Ymin":2.5539900000000695,
               "Ymax":2.7739900000000692
            }
         },
         {
            "name":"GARZÓN",
            "value":"298",
            "text":"COLOMBIA, HUILA, GARZÓN",
            "code":"CO-41-298",
            "geoPoint":{
               "lat":2.1973200000000475,
               "lon":-75.62804999999997
            },
            "geoRectangle":{
               "Xmin":-75.78404999999998,
               "Xmax":-75.47204999999997,
               "Ymin":2.0413200000000473,
               "Ymax":2.3533200000000476
            }
         },
         {
            "name":"ELÍAS",
            "value":"244",
            "text":"COLOMBIA, HUILA, ELÍAS",
            "code":"CO-41-244",
            "geoPoint":{
               "lat":2.0133600000000342,
               "lon":-75.94001999999995
            },
            "geoRectangle":{
               "Xmin":-76.02901999999995,
               "Xmax":-75.85101999999995,
               "Ymin":1.9243600000000343,
               "Ymax":2.102360000000034
            }
         },
         {
            "name":"COLOMBIA",
            "value":"206",
            "text":"COLOMBIA, HUILA, COLOMBIA",
            "code":"CO-41-206",
            "geoPoint":{
               "lat":3.376630000000034,
               "lon":-74.80237999999997
            },
            "geoRectangle":{
               "Xmin":-75.11037999999998,
               "Xmax":-74.49437999999996,
               "Ymin":3.0686300000000344,
               "Ymax":3.684630000000034
            }
         },
         {
            "name":"CAMPOALEGRE",
            "value":"132",
            "text":"COLOMBIA, HUILA, CAMPOALEGRE",
            "code":"CO-41-132",
            "geoPoint":{
               "lat":2.684470000000033,
               "lon":-75.32559999999995
            },
            "geoRectangle":{
               "Xmin":-75.46159999999995,
               "Xmax":-75.18959999999996,
               "Ymin":2.548470000000033,
               "Ymax":2.820470000000033
            }
         },
         {
            "name":"BARAYA",
            "value":"078",
            "text":"COLOMBIA, HUILA, BARAYA",
            "code":"CO-41-078",
            "geoPoint":{
               "lat":3.153190000000052,
               "lon":-75.05534999999998
            },
            "geoRectangle":{
               "Xmin":-75.22734999999997,
               "Xmax":-74.88334999999998,
               "Ymin":2.9811900000000517,
               "Ymax":3.325190000000052
            }
         },
         {
            "name":"ALTAMIRA",
            "value":"026",
            "text":"COLOMBIA, HUILA, ALTAMIRA",
            "code":"CO-41-026",
            "geoPoint":{
               "lat":2.063660000000027,
               "lon":-75.78731999999997
            },
            "geoRectangle":{
               "Xmin":-75.88231999999996,
               "Xmax":-75.69231999999997,
               "Ymin":1.9686600000000272,
               "Ymax":2.1586600000000273
            }
         },
         {
            "name":"ALGECIRAS",
            "value":"020",
            "text":"COLOMBIA, HUILA, ALGECIRAS",
            "code":"CO-41-020",
            "geoPoint":{
               "lat":2.522700000000043,
               "lon":-75.31626999999997
            },
            "geoRectangle":{
               "Xmin":-75.51826999999997,
               "Xmax":-75.11426999999998,
               "Ymin":2.320700000000043,
               "Ymax":2.724700000000043
            }
         },
         {
            "name":"AIPE",
            "value":"016",
            "text":"COLOMBIA, HUILA, AIPE",
            "code":"CO-41-016",
            "geoPoint":{
               "lat":3.2224900000000503,
               "lon":-75.23738999999995
            },
            "geoRectangle":{
               "Xmin":-75.43538999999994,
               "Xmax":-75.03938999999995,
               "Ymin":3.0244900000000503,
               "Ymax":3.42049000000005
            }
         },
         {
            "name":"AGRADO",
            "value":"013",
            "text":"COLOMBIA, HUILA, AGRADO",
            "code":"CO-41-013",
            "geoPoint":{
               "lat":2.25899000000004,
               "lon":-75.77212999999995
            },
            "geoRectangle":{
               "Xmin":-75.86112999999995,
               "Xmax":-75.68312999999995,
               "Ymin":2.16999000000004,
               "Ymax":2.3479900000000398
            }
         },
         {
            "name":"ACEVEDO",
            "value":"006",
            "text":"COLOMBIA, HUILA, ACEVEDO",
            "code":"CO-41-006",
            "geoPoint":{
               "lat":1.8050800000000322,
               "lon":-75.89055999999994
            },
            "geoRectangle":{
               "Xmin":-76.04555999999994,
               "Xmax":-75.73555999999994,
               "Ymin":1.6500800000000322,
               "Ymax":1.9600800000000322
            }
         }
      ]
   },
   {
      "name":"LA GUAJIRA",
      "value":"44",
      "text":"COLOMBIA, LA GUAJIRA",
      "code":"CO-44",
      "geoPoint":{
         "lat":11.476500482000063,
         "lon":-72.42929045199998
      },
      "geoRectangle":{
         "Xmin":-73.58229045199998,
         "Xmax":-71.27629045199997,
         "Ymin":10.323500482000062,
         "Ymax":12.629500482000063
      },
      "municipality":[
         {
            "name":"RIOHACHA",
            "value":"001",
            "text":"COLOMBIA, LA GUAJIRA, RIOHACHA",
            "code":"CO-44-001",
            "geoPoint":{
               "lat":11.544900000000041,
               "lon":-72.90634999999997
            },
            "geoRectangle":{
               "Xmin":-73.26534999999997,
               "Xmax":-72.54734999999998,
               "Ymin":11.185900000000041,
               "Ymax":11.903900000000041
            }
         },
         {
            "name":"ALBANIA",
            "value":"035",
            "text":"COLOMBIA, LA GUAJIRA, ALBANIA",
            "code":"CO-44-035",
            "geoPoint":{
               "lat":11.163820000000044,
               "lon":-72.59124999999995
            },
            "geoRectangle":{
               "Xmin":-72.73524999999995,
               "Xmax":-72.44724999999994,
               "Ymin":11.019820000000044,
               "Ymax":11.307820000000044
            }
         },
         {
            "name":"DIBULLA",
            "value":"090",
            "text":"COLOMBIA, LA GUAJIRA, DIBULLA",
            "code":"CO-44-090",
            "geoPoint":{
               "lat":11.272270000000049,
               "lon":-73.30794999999995
            },
            "geoRectangle":{
               "Xmin":-73.56094999999995,
               "Xmax":-73.05494999999995,
               "Ymin":11.019270000000049,
               "Ymax":11.525270000000049
            }
         },
         {
            "name":"SAN JUAN DEL CESAR",
            "value":"650",
            "text":"COLOMBIA, LA GUAJIRA, SAN JUAN DEL CESAR",
            "code":"CO-44-650",
            "geoPoint":{
               "lat":10.773790000000076,
               "lon":-73.00108999999998
            },
            "geoRectangle":{
               "Xmin":-73.29108999999998,
               "Xmax":-72.71108999999997,
               "Ymin":10.483790000000077,
               "Ymax":11.063790000000076
            }
         },
         {
            "name":"URIBIA",
            "value":"847",
            "text":"COLOMBIA, LA GUAJIRA, URIBIA",
            "code":"CO-44-847",
            "geoPoint":{
               "lat":11.71604000000002,
               "lon":-72.26535999999999
            },
            "geoRectangle":{
               "Xmin":-72.81535999999998,
               "Xmax":-71.71535999999999,
               "Ymin":11.16604000000002,
               "Ymax":12.266040000000022
            }
         },
         {
            "name":"MAICAO",
            "value":"430",
            "text":"COLOMBIA, LA GUAJIRA, MAICAO",
            "code":"CO-44-430",
            "geoPoint":{
               "lat":11.38083000000006,
               "lon":-72.23830999999996
            },
            "geoRectangle":{
               "Xmin":-72.55430999999996,
               "Xmax":-71.92230999999995,
               "Ymin":11.06483000000006,
               "Ymax":11.69683000000006
            }
         },
         {
            "name":"MANAURE",
            "value":"560",
            "text":"COLOMBIA, LA GUAJIRA, MANAURE",
            "code":"CO-44-560",
            "geoPoint":{
               "lat":11.774360000000058,
               "lon":-72.44247999999999
            },
            "geoRectangle":{
               "Xmin":-72.70047999999998,
               "Xmax":-72.18448,
               "Ymin":11.51636000000006,
               "Ymax":12.032360000000057
            }
         },
         {
            "name":"BARRANCAS",
            "value":"078",
            "text":"COLOMBIA, LA GUAJIRA, BARRANCAS",
            "code":"CO-44-078",
            "geoPoint":{
               "lat":10.958870000000047,
               "lon":-72.79167999999999
            },
            "geoRectangle":{
               "Xmin":-72.97667999999999,
               "Xmax":-72.60667999999998,
               "Ymin":10.773870000000047,
               "Ymax":11.143870000000048
            }
         },
         {
            "name":"DISTRACCIÓN",
            "value":"098",
            "text":"COLOMBIA, LA GUAJIRA, DISTRACCIÓN",
            "code":"CO-44-098",
            "geoPoint":{
               "lat":10.894940000000076,
               "lon":-72.88491999999997
            },
            "geoRectangle":{
               "Xmin":-72.98291999999996,
               "Xmax":-72.78691999999997,
               "Ymin":10.796940000000076,
               "Ymax":10.992940000000077
            }
         },
         {
            "name":"EL MOLINO",
            "value":"110",
            "text":"COLOMBIA, LA GUAJIRA, EL MOLINO",
            "code":"CO-44-110",
            "geoPoint":{
               "lat":10.652940000000058,
               "lon":-72.92429999999996
            },
            "geoRectangle":{
               "Xmin":-73.02029999999996,
               "Xmax":-72.82829999999996,
               "Ymin":10.556940000000058,
               "Ymax":10.748940000000058
            }
         },
         {
            "name":"FONSECA",
            "value":"279",
            "text":"COLOMBIA, LA GUAJIRA, FONSECA",
            "code":"CO-44-279",
            "geoPoint":{
               "lat":10.88695000000007,
               "lon":-72.84611999999998
            },
            "geoRectangle":{
               "Xmin":-73.00411999999999,
               "Xmax":-72.68811999999998,
               "Ymin":10.72895000000007,
               "Ymax":11.04495000000007
            }
         },
         {
            "name":"HATONUEVO",
            "value":"378",
            "text":"COLOMBIA, LA GUAJIRA, HATONUEVO",
            "code":"CO-44-378",
            "geoPoint":{
               "lat":11.069140000000061,
               "lon":-72.76091999999994
            },
            "geoRectangle":{
               "Xmin":-72.88791999999994,
               "Xmax":-72.63391999999995,
               "Ymin":10.94214000000006,
               "Ymax":11.196140000000062
            }
         },
         {
            "name":"LA JAGUA DEL PILAR",
            "value":"420",
            "text":"COLOMBIA, LA GUAJIRA, LA JAGUA DEL PILAR",
            "code":"CO-44-420",
            "geoPoint":{
               "lat":10.51093000000003,
               "lon":-73.07151999999996
            },
            "geoRectangle":{
               "Xmin":-73.18251999999997,
               "Xmax":-72.96051999999996,
               "Ymin":10.39993000000003,
               "Ymax":10.621930000000031
            }
         },
         {
            "name":"URUMITA",
            "value":"855",
            "text":"COLOMBIA, LA GUAJIRA, URUMITA",
            "code":"CO-44-855",
            "geoPoint":{
               "lat":10.563240000000064,
               "lon":-73.01268999999996
            },
            "geoRectangle":{
               "Xmin":-73.13368999999996,
               "Xmax":-72.89168999999997,
               "Ymin":10.442240000000064,
               "Ymax":10.684240000000065
            }
         },
         {
            "name":"VILLANUEVA",
            "value":"874",
            "text":"COLOMBIA, LA GUAJIRA, VILLANUEVA",
            "code":"CO-44-874",
            "geoPoint":{
               "lat":10.60713000000004,
               "lon":-72.98133999999999
            },
            "geoRectangle":{
               "Xmin":-73.09733999999999,
               "Xmax":-72.86533999999999,
               "Ymin":10.491130000000041,
               "Ymax":10.72313000000004
            }
         }
      ]
   },
   {
      "name":"MAGDALENA",
      "value":"47",
      "text":"COLOMBIA, MAGDALENA",
      "code":"CO-47",
      "geoPoint":{
         "lat":10.235360033000063,
         "lon":-74.25308468199995
      },
      "geoRectangle":{
         "Xmin":-75.20108468199994,
         "Xmax":-73.30508468199996,
         "Ymin":9.287360033000063,
         "Ymax":11.183360033000064
      },
      "municipality":[
         {
            "name":"SANTA MARTA",
            "value":"001",
            "text":"COLOMBIA, MAGDALENA, SANTA MARTA",
            "code":"CO-47-001",
            "geoPoint":{
               "lat":11.226560000000063,
               "lon":-74.19873999999999
            },
            "geoRectangle":{
               "Xmin":-74.49873999999998,
               "Xmax":-73.89873999999999,
               "Ymin":10.926560000000062,
               "Ymax":11.526560000000064
            }
         },
         {
            "name":"ARACATACA",
            "value":"053",
            "text":"COLOMBIA, MAGDALENA, ARACATACA",
            "code":"CO-47-053",
            "geoPoint":{
               "lat":10.589860000000044,
               "lon":-74.17722999999995
            },
            "geoRectangle":{
               "Xmin":-74.46922999999995,
               "Xmax":-73.88522999999995,
               "Ymin":10.297860000000044,
               "Ymax":10.881860000000044
            }
         },
         {
            "name":"CIÉNAGA",
            "value":"189",
            "text":"COLOMBIA, MAGDALENA, CIÉNAGA",
            "code":"CO-47-189",
            "geoPoint":{
               "lat":11.005430000000047,
               "lon":-74.24622999999997
            },
            "geoRectangle":{
               "Xmin":-74.50122999999996,
               "Xmax":-73.99122999999997,
               "Ymin":10.750430000000046,
               "Ymax":11.260430000000047
            }
         },
         {
            "name":"FUNDACIÓN",
            "value":"288",
            "text":"COLOMBIA, MAGDALENA, FUNDACIÓN",
            "code":"CO-47-288",
            "geoPoint":{
               "lat":10.517660000000035,
               "lon":-74.19622999999996
            },
            "geoRectangle":{
               "Xmin":-74.45422999999995,
               "Xmax":-73.93822999999996,
               "Ymin":10.259660000000036,
               "Ymax":10.775660000000034
            }
         },
         {
            "name":"PEDRAZA",
            "value":"541",
            "text":"COLOMBIA, MAGDALENA, PEDRAZA",
            "code":"CO-47-541",
            "geoPoint":{
               "lat":10.188220000000058,
               "lon":-74.91565999999995
            },
            "geoRectangle":{
               "Xmin":-75.01265999999994,
               "Xmax":-74.81865999999995,
               "Ymin":10.091220000000058,
               "Ymax":10.285220000000058
            }
         },
         {
            "name":"ALGARROBO",
            "value":"030",
            "text":"COLOMBIA, MAGDALENA, ALGARROBO",
            "code":"CO-47-030",
            "geoPoint":{
               "lat":10.189860000000067,
               "lon":-74.06092999999998
            },
            "geoRectangle":{
               "Xmin":-74.22392999999998,
               "Xmax":-73.89792999999999,
               "Ymin":10.026860000000067,
               "Ymax":10.352860000000067
            }
         },
         {
            "name":"ARIGUANÍ",
            "value":"058",
            "text":"COLOMBIA, MAGDALENA, ARIGUANÍ",
            "code":"CO-47-058",
            "geoPoint":{
               "lat":9.854900000000043,
               "lon":-74.23035999999996
            },
            "geoRectangle":{
               "Xmin":-74.44435999999996,
               "Xmax":-74.01635999999996,
               "Ymin":9.640900000000043,
               "Ymax":10.068900000000044
            }
         },
         {
            "name":"CERRO DE SAN ANTONIO",
            "value":"161",
            "text":"COLOMBIA, MAGDALENA, CERRO DE SAN ANTONIO",
            "code":"CO-47-161",
            "geoPoint":{
               "lat":10.32585000000006,
               "lon":-74.86932999999993
            },
            "geoRectangle":{
               "Xmin":-74.88432999999993,
               "Xmax":-74.85432999999993,
               "Ymin":10.310850000000059,
               "Ymax":10.34085000000006
            }
         },
         {
            "name":"CHIVOLO",
            "value":"170",
            "text":"COLOMBIA, MAGDALENA, CHIVOLO",
            "code":"CO-47-170",
            "geoPoint":{
               "lat":10.027160000000038,
               "lon":-74.62369999999999
            },
            "geoRectangle":{
               "Xmin":-74.76769999999999,
               "Xmax":-74.47969999999998,
               "Ymin":9.883160000000037,
               "Ymax":10.171160000000038
            }
         },
         {
            "name":"SANTA BÁRBARA DE PINTO",
            "value":"720",
            "text":"COLOMBIA, MAGDALENA, SANTA BÁRBARA DE PINTO",
            "code":"CO-47-720",
            "geoPoint":{
               "lat":9.434620000000052,
               "lon":-74.70382999999998
            },
            "geoRectangle":{
               "Xmin":-74.84382999999998,
               "Xmax":-74.56382999999998,
               "Ymin":9.294620000000052,
               "Ymax":9.574620000000053
            }
         },
         {
            "name":"SITIONUEVO",
            "value":"745",
            "text":"COLOMBIA, MAGDALENA, SITIONUEVO",
            "code":"CO-47-745",
            "geoPoint":{
               "lat":10.776470000000074,
               "lon":-74.72036999999995
            },
            "geoRectangle":{
               "Xmin":-74.91736999999995,
               "Xmax":-74.52336999999994,
               "Ymin":10.579470000000075,
               "Ymax":10.973470000000074
            }
         },
         {
            "name":"TENERIFE",
            "value":"798",
            "text":"COLOMBIA, MAGDALENA, TENERIFE",
            "code":"CO-47-798",
            "geoPoint":{
               "lat":9.899410000000046,
               "lon":-74.86014999999998
            },
            "geoRectangle":{
               "Xmin":-74.99214999999998,
               "Xmax":-74.72814999999997,
               "Ymin":9.767410000000046,
               "Ymax":10.031410000000045
            }
         },
         {
            "name":"ZAPAYÁN",
            "value":"960",
            "text":"COLOMBIA, MAGDALENA, ZAPAYÁN",
            "code":"CO-47-960",
            "geoPoint":{
               "lat":10.166430000000048,
               "lon":-74.71427999999997
            },
            "geoRectangle":{
               "Xmin":-74.84327999999998,
               "Xmax":-74.58527999999997,
               "Ymin":10.037430000000048,
               "Ymax":10.295430000000048
            }
         },
         {
            "name":"ZONA BANANERA",
            "value":"980",
            "text":"COLOMBIA, MAGDALENA, ZONA BANANERA",
            "code":"CO-47-980",
            "geoPoint":{
               "lat":10.750680000000045,
               "lon":-74.15597999999994
            },
            "geoRectangle":{
               "Xmin":-74.28597999999994,
               "Xmax":-74.02597999999995,
               "Ymin":10.620680000000045,
               "Ymax":10.880680000000046
            }
         },
         {
            "name":"CONCORDIA",
            "value":"205",
            "text":"COLOMBIA, MAGDALENA, CONCORDIA",
            "code":"CO-47-205",
            "geoPoint":{
               "lat":10.25828000000007,
               "lon":-74.83305999999999
            },
            "geoRectangle":{
               "Xmin":-74.91105999999999,
               "Xmax":-74.75505999999999,
               "Ymin":10.18028000000007,
               "Ymax":10.33628000000007
            }
         },
         {
            "name":"EL BANCO",
            "value":"245",
            "text":"COLOMBIA, MAGDALENA, EL BANCO",
            "code":"CO-47-245",
            "geoPoint":{
               "lat":9.00314000000003,
               "lon":-73.97377999999998
            },
            "geoRectangle":{
               "Xmin":-74.16277999999997,
               "Xmax":-73.78477999999998,
               "Ymin":8.81414000000003,
               "Ymax":9.19214000000003
            }
         },
         {
            "name":"EL PIÑÓN",
            "value":"258",
            "text":"COLOMBIA, MAGDALENA, EL PIÑÓN",
            "code":"CO-47-258",
            "geoPoint":{
               "lat":10.404440000000022,
               "lon":-74.82355999999999
            },
            "geoRectangle":{
               "Xmin":-74.98055999999998,
               "Xmax":-74.66655999999999,
               "Ymin":10.247440000000022,
               "Ymax":10.561440000000022
            }
         },
         {
            "name":"EL RETÉN",
            "value":"268",
            "text":"COLOMBIA, MAGDALENA, EL RETÉN",
            "code":"CO-47-268",
            "geoPoint":{
               "lat":10.610610000000065,
               "lon":-74.26805999999993
            },
            "geoRectangle":{
               "Xmin":-74.39105999999994,
               "Xmax":-74.14505999999993,
               "Ymin":10.487610000000066,
               "Ymax":10.733610000000064
            }
         },
         {
            "name":"GUAMAL",
            "value":"318",
            "text":"COLOMBIA, MAGDALENA, GUAMAL",
            "code":"CO-47-318",
            "geoPoint":{
               "lat":9.144720000000063,
               "lon":-74.22471999999993
            },
            "geoRectangle":{
               "Xmin":-74.37971999999993,
               "Xmax":-74.06971999999993,
               "Ymin":8.989720000000064,
               "Ymax":9.299720000000063
            }
         },
         {
            "name":"NUEVA GRANADA",
            "value":"460",
            "text":"COLOMBIA, MAGDALENA, NUEVA GRANADA",
            "code":"CO-47-460",
            "geoPoint":{
               "lat":9.803310000000067,
               "lon":-74.39299999999997
            },
            "geoRectangle":{
               "Xmin":-74.56799999999997,
               "Xmax":-74.21799999999998,
               "Ymin":9.628310000000067,
               "Ymax":9.978310000000068
            }
         },
         {
            "name":"PIJIÑO DEL CARMEN",
            "value":"545",
            "text":"COLOMBIA, MAGDALENA, PIJIÑO DEL CARMEN",
            "code":"CO-47-545",
            "geoPoint":{
               "lat":9.330490000000054,
               "lon":-74.45349999999996
            },
            "geoRectangle":{
               "Xmin":-74.73649999999996,
               "Xmax":-74.17049999999996,
               "Ymin":9.047490000000055,
               "Ymax":9.613490000000054
            }
         },
         {
            "name":"PIVIJAY",
            "value":"551",
            "text":"COLOMBIA, MAGDALENA, PIVIJAY",
            "code":"CO-47-551",
            "geoPoint":{
               "lat":10.461560000000077,
               "lon":-74.61520999999993
            },
            "geoRectangle":{
               "Xmin":-74.85720999999994,
               "Xmax":-74.37320999999993,
               "Ymin":10.219560000000076,
               "Ymax":10.703560000000078
            }
         },
         {
            "name":"PLATO",
            "value":"555",
            "text":"COLOMBIA, MAGDALENA, PLATO",
            "code":"CO-47-555",
            "geoPoint":{
               "lat":9.792110000000037,
               "lon":-74.78355999999997
            },
            "geoRectangle":{
               "Xmin":-75.04055999999997,
               "Xmax":-74.52655999999996,
               "Ymin":9.535110000000037,
               "Ymax":10.049110000000036
            }
         },
         {
            "name":"PUEBLO VIEJO",
            "value":"570",
            "text":"COLOMBIA, MAGDALENA, PUEBLO VIEJO",
            "code":"CO-47-570",
            "geoPoint":{
               "lat":10.983330000000024,
               "lon":-74.33332999999999
            },
            "geoRectangle":{
               "Xmin":-75.33332999999999,
               "Xmax":-73.33332999999999,
               "Ymin":9.983330000000024,
               "Ymax":11.983330000000024
            }
         },
         {
            "name":"REMOLINO",
            "value":"605",
            "text":"COLOMBIA, MAGDALENA, REMOLINO",
            "code":"CO-47-605",
            "geoPoint":{
               "lat":10.702540000000056,
               "lon":-74.71613999999994
            },
            "geoRectangle":{
               "Xmin":-74.84913999999993,
               "Xmax":-74.58313999999994,
               "Ymin":10.569540000000057,
               "Ymax":10.835540000000055
            }
         },
         {
            "name":"SABANAS DE SAN ANGEL",
            "value":"660",
            "text":"COLOMBIA, MAGDALENA, SABANAS DE SAN ANGEL",
            "code":"CO-47-660",
            "geoPoint":{
               "lat":10.031900000000064,
               "lon":-74.21454999999997
            },
            "geoRectangle":{
               "Xmin":-74.41854999999997,
               "Xmax":-74.01054999999998,
               "Ymin":9.827900000000064,
               "Ymax":10.235900000000065
            }
         },
         {
            "name":"SALAMINA",
            "value":"675",
            "text":"COLOMBIA, MAGDALENA, SALAMINA",
            "code":"CO-47-675",
            "geoPoint":{
               "lat":10.491210000000024,
               "lon":-74.79363999999998
            },
            "geoRectangle":{
               "Xmin":-74.86863999999998,
               "Xmax":-74.71863999999998,
               "Ymin":10.416210000000024,
               "Ymax":10.566210000000023
            }
         },
         {
            "name":"SAN SEBASTIAN DE BUENAVISTA",
            "value":"692",
            "text":"COLOMBIA, MAGDALENA, SAN SEBASTIAN DE BUENAVISTA",
            "code":"CO-47-692",
            "geoPoint":{
               "lat":9.239370000000065,
               "lon":-74.35144999999994
            },
            "geoRectangle":{
               "Xmin":-74.52944999999994,
               "Xmax":-74.17344999999995,
               "Ymin":9.061370000000064,
               "Ymax":9.417370000000066
            }
         },
         {
            "name":"SAN ZENÓN",
            "value":"703",
            "text":"COLOMBIA, MAGDALENA, SAN ZENÓN",
            "code":"CO-47-703",
            "geoPoint":{
               "lat":9.244320000000073,
               "lon":-74.49905999999999
            },
            "geoRectangle":{
               "Xmin":-74.64505999999999,
               "Xmax":-74.35305999999999,
               "Ymin":9.098320000000072,
               "Ymax":9.390320000000074
            }
         },
         {
            "name":"SANTA ANA",
            "value":"707",
            "text":"COLOMBIA, MAGDALENA, SANTA ANA",
            "code":"CO-47-707",
            "geoPoint":{
               "lat":9.322670000000073,
               "lon":-74.57092999999998
            },
            "geoRectangle":{
               "Xmin":-74.91192999999997,
               "Xmax":-74.22992999999998,
               "Ymin":8.981670000000074,
               "Ymax":9.663670000000073
            }
         }
      ]
   },
   {
      "name":"META",
      "value":"50",
      "text":"COLOMBIA, META",
      "code":"CO-50",
      "geoPoint":{
         "lat":3.3448408760000348,
         "lon":-72.95559189599999
      },
      "geoRectangle":{
         "Xmin":-74.73859189599999,
         "Xmax":-71.17259189599999,
         "Ymin":1.5618408760000346,
         "Ymax":5.127840876000035
      },
      "municipality":[
         {
            "name":"LA MACARENA",
            "value":"350",
            "text":"COLOMBIA, META, LA MACARENA",
            "code":"CO-50-350",
            "geoPoint":{
               "lat":2.1823700000000485,
               "lon":-73.78481999999997
            },
            "geoRectangle":{
               "Xmin":-74.34881999999996,
               "Xmax":-73.22081999999997,
               "Ymin":1.6183700000000485,
               "Ymax":2.7463700000000486
            }
         },
         {
            "name":"VILLAVICENCIO",
            "value":"001",
            "text":"COLOMBIA, META, VILLAVICENCIO",
            "code":"CO-50-001",
            "geoPoint":{
               "lat":4.132350000000031,
               "lon":-73.62195999999994
            },
            "geoRectangle":{
               "Xmin":-73.86195999999994,
               "Xmax":-73.38195999999995,
               "Ymin":3.8923500000000306,
               "Ymax":4.372350000000031
            }
         },
         {
            "name":"ACACÍAS",
            "value":"006",
            "text":"COLOMBIA, META, ACACÍAS",
            "code":"CO-50-006",
            "geoPoint":{
               "lat":3.9911600000000362,
               "lon":-73.76387999999997
            },
            "geoRectangle":{
               "Xmin":-74.01087999999997,
               "Xmax":-73.51687999999997,
               "Ymin":3.7441600000000363,
               "Ymax":4.238160000000036
            }
         },
         {
            "name":"BARRANCA DE UPIA",
            "value":"110",
            "text":"COLOMBIA, META, BARRANCA DE UPIA",
            "code":"CO-50-110",
            "geoPoint":{
               "lat":4.5668600000000765,
               "lon":-72.96298999999993
            },
            "geoRectangle":{
               "Xmin":-73.12198999999994,
               "Xmax":-72.80398999999993,
               "Ymin":4.407860000000077,
               "Ymax":4.725860000000076
            }
         },
         {
            "name":"CABUYARO",
            "value":"124",
            "text":"COLOMBIA, META, CABUYARO",
            "code":"CO-50-124",
            "geoPoint":{
               "lat":4.287210000000073,
               "lon":-72.79203999999999
            },
            "geoRectangle":{
               "Xmin":-72.96903999999999,
               "Xmax":-72.61503999999998,
               "Ymin":4.110210000000073,
               "Ymax":4.464210000000072
            }
         },
         {
            "name":"CASTILLA LA NUEVA",
            "value":"150",
            "text":"COLOMBIA, META, CASTILLA LA NUEVA",
            "code":"CO-50-150",
            "geoPoint":{
               "lat":3.826850000000036,
               "lon":-73.68682999999999
            },
            "geoRectangle":{
               "Xmin":-73.83682999999999,
               "Xmax":-73.53682999999998,
               "Ymin":3.676850000000036,
               "Ymax":3.9768500000000357
            }
         },
         {
            "name":"CUMARAL",
            "value":"226",
            "text":"COLOMBIA, META, CUMARAL",
            "code":"CO-50-226",
            "geoPoint":{
               "lat":4.271290000000022,
               "lon":-73.48728999999997
            },
            "geoRectangle":{
               "Xmin":-73.67928999999997,
               "Xmax":-73.29528999999998,
               "Ymin":4.079290000000022,
               "Ymax":4.463290000000022
            }
         },
         {
            "name":"EL CALVARIO",
            "value":"245",
            "text":"COLOMBIA, META, EL CALVARIO",
            "code":"CO-50-245",
            "geoPoint":{
               "lat":4.352290000000039,
               "lon":-73.71403999999995
            },
            "geoRectangle":{
               "Xmin":-73.81303999999996,
               "Xmax":-73.61503999999995,
               "Ymin":4.253290000000039,
               "Ymax":4.451290000000039
            }
         },
         {
            "name":"PUERTO GAITÁN",
            "value":"568",
            "text":"COLOMBIA, META, PUERTO GAITÁN",
            "code":"CO-50-568",
            "geoPoint":{
               "lat":4.312610000000063,
               "lon":-72.08190999999994
            },
            "geoRectangle":{
               "Xmin":-72.82490999999993,
               "Xmax":-71.33890999999994,
               "Ymin":3.5696100000000635,
               "Ymax":5.055610000000064
            }
         },
         {
            "name":"MAPIRIPÁN",
            "value":"325",
            "text":"COLOMBIA, META, MAPIRIPÁN",
            "code":"CO-50-325",
            "geoPoint":{
               "lat":2.8918500000000336,
               "lon":-72.13320999999996
            },
            "geoRectangle":{
               "Xmin":-72.78120999999996,
               "Xmax":-71.48520999999997,
               "Ymin":2.2438500000000334,
               "Ymax":3.5398500000000337
            }
         },
         {
            "name":"EL DORADO",
            "value":"270",
            "text":"COLOMBIA, META, EL DORADO",
            "code":"CO-50-270",
            "geoPoint":{
               "lat":3.7393600000000333,
               "lon":-73.83551999999997
            },
            "geoRectangle":{
               "Xmin":-73.89951999999997,
               "Xmax":-73.77151999999998,
               "Ymin":3.6753600000000333,
               "Ymax":3.8033600000000334
            }
         },
         {
            "name":"FUENTE DE ORO",
            "value":"287",
            "text":"COLOMBIA, META, FUENTE DE ORO",
            "code":"CO-50-287",
            "geoPoint":{
               "lat":3.4632700000000227,
               "lon":-73.61870999999996
            },
            "geoRectangle":{
               "Xmin":-73.78270999999997,
               "Xmax":-73.45470999999996,
               "Ymin":3.2992700000000226,
               "Ymax":3.627270000000023
            }
         },
         {
            "name":"GRANADA",
            "value":"313",
            "text":"COLOMBIA, META, GRANADA",
            "code":"CO-50-313",
            "geoPoint":{
               "lat":3.546880000000044,
               "lon":-73.70738999999998
            },
            "geoRectangle":{
               "Xmin":-73.83838999999998,
               "Xmax":-73.57638999999998,
               "Ymin":3.415880000000044,
               "Ymax":3.6778800000000444
            }
         },
         {
            "name":"GUAMAL",
            "value":"318",
            "text":"COLOMBIA, META, GUAMAL",
            "code":"CO-50-318",
            "geoPoint":{
               "lat":3.8810100000000602,
               "lon":-73.76867999999996
            },
            "geoRectangle":{
               "Xmin":-73.96367999999995,
               "Xmax":-73.57367999999997,
               "Ymin":3.6860100000000604,
               "Ymax":4.0760100000000605
            }
         },
         {
            "name":"MESETAS",
            "value":"330",
            "text":"COLOMBIA, META, MESETAS",
            "code":"CO-50-330",
            "geoPoint":{
               "lat":3.3849800000000414,
               "lon":-74.04281999999995
            },
            "geoRectangle":{
               "Xmin":-74.39881999999994,
               "Xmax":-73.68681999999995,
               "Ymin":3.0289800000000415,
               "Ymax":3.7409800000000413
            }
         },
         {
            "name":"LEJANÍAS",
            "value":"400",
            "text":"COLOMBIA, META, LEJANÍAS",
            "code":"CO-50-400",
            "geoPoint":{
               "lat":3.5260900000000674,
               "lon":-74.02197999999999
            },
            "geoRectangle":{
               "Xmin":-74.22397999999998,
               "Xmax":-73.81997999999999,
               "Ymin":3.3240900000000675,
               "Ymax":3.7280900000000674
            }
         },
         {
            "name":"PUERTO CONCORDIA",
            "value":"450",
            "text":"COLOMBIA, META, PUERTO CONCORDIA",
            "code":"CO-50-450",
            "geoPoint":{
               "lat":2.622850000000028,
               "lon":-72.76014999999995
            },
            "geoRectangle":{
               "Xmin":-73.00214999999996,
               "Xmax":-72.51814999999995,
               "Ymin":2.380850000000028,
               "Ymax":2.864850000000028
            }
         },
         {
            "name":"PUERTO LÓPEZ",
            "value":"573",
            "text":"COLOMBIA, META, PUERTO LÓPEZ",
            "code":"CO-50-573",
            "geoPoint":{
               "lat":4.086480000000051,
               "lon":-72.95977999999997
            },
            "geoRectangle":{
               "Xmin":-73.45977999999997,
               "Xmax":-72.45977999999997,
               "Ymin":3.5864800000000514,
               "Ymax":4.586480000000051
            }
         },
         {
            "name":"PUERTO LLERAS",
            "value":"577",
            "text":"COLOMBIA, META, PUERTO LLERAS",
            "code":"CO-50-577",
            "geoPoint":{
               "lat":3.269170000000031,
               "lon":-73.37383999999997
            },
            "geoRectangle":{
               "Xmin":-73.69283999999998,
               "Xmax":-73.05483999999997,
               "Ymin":2.950170000000031,
               "Ymax":3.588170000000031
            }
         },
         {
            "name":"PUERTO RICO",
            "value":"590",
            "text":"COLOMBIA, META, PUERTO RICO",
            "code":"CO-50-590",
            "geoPoint":{
               "lat":2.938690000000065,
               "lon":-73.20694999999995
            },
            "geoRectangle":{
               "Xmin":-73.54494999999994,
               "Xmax":-72.86894999999996,
               "Ymin":2.600690000000065,
               "Ymax":3.276690000000065
            }
         },
         {
            "name":"RESTREPO",
            "value":"606",
            "text":"COLOMBIA, META, RESTREPO",
            "code":"CO-50-606",
            "geoPoint":{
               "lat":4.260370000000023,
               "lon":-73.56543999999997
            },
            "geoRectangle":{
               "Xmin":-73.77843999999996,
               "Xmax":-73.35243999999997,
               "Ymin":4.047370000000023,
               "Ymax":4.473370000000023
            }
         },
         {
            "name":"SAN CARLOS DE GUAROA",
            "value":"680",
            "text":"COLOMBIA, META, SAN CARLOS DE GUAROA",
            "code":"CO-50-680",
            "geoPoint":{
               "lat":3.7115700000000516,
               "lon":-73.24270999999999
            },
            "geoRectangle":{
               "Xmin":-73.40471,
               "Xmax":-73.08070999999998,
               "Ymin":3.5495700000000516,
               "Ymax":3.8735700000000515
            }
         },
         {
            "name":"SAN JUAN DE ARAMA",
            "value":"683",
            "text":"COLOMBIA, META, SAN JUAN DE ARAMA",
            "code":"CO-50-683",
            "geoPoint":{
               "lat":3.373610000000042,
               "lon":-73.87570999999997
            },
            "geoRectangle":{
               "Xmin":-74.12570999999997,
               "Xmax":-73.62570999999997,
               "Ymin":3.123610000000042,
               "Ymax":3.623610000000042
            }
         },
         {
            "name":"SAN JUANITO",
            "value":"686",
            "text":"COLOMBIA, META, SAN JUANITO",
            "code":"CO-50-686",
            "geoPoint":{
               "lat":4.4578200000000265,
               "lon":-73.67651999999998
            },
            "geoRectangle":{
               "Xmin":-73.77251999999999,
               "Xmax":-73.58051999999998,
               "Ymin":4.3618200000000265,
               "Ymax":4.553820000000027
            }
         },
         {
            "name":"SAN MARTÍN",
            "value":"689",
            "text":"COLOMBIA, META, SAN MARTÍN",
            "code":"CO-50-689",
            "geoPoint":{
               "lat":3.6982400000000553,
               "lon":-73.69779999999997
            },
            "geoRectangle":{
               "Xmin":-74.23179999999998,
               "Xmax":-73.16379999999997,
               "Ymin":3.1642400000000555,
               "Ymax":4.232240000000055
            }
         },
         {
            "name":"VISTAHERMOSA",
            "value":"711",
            "text":"COLOMBIA, META, VISTAHERMOSA",
            "code":"CO-50-711",
            "geoPoint":{
               "lat":3.126320000000021,
               "lon":-73.75137999999998
            },
            "geoRectangle":{
               "Xmin":-74.24837999999998,
               "Xmax":-73.25437999999998,
               "Ymin":2.629320000000021,
               "Ymax":3.623320000000021
            }
         },
         {
            "name":"CUBARRAL",
            "value":"223",
            "text":"COLOMBIA, META, CUBARRAL",
            "code":"CO-50-223",
            "geoPoint":{
               "lat":3.7934400000000323,
               "lon":-73.83856999999995
            },
            "geoRectangle":{
               "Xmin":-74.09856999999995,
               "Xmax":-73.57856999999994,
               "Ymin":3.5334400000000326,
               "Ymax":4.053440000000032
            }
         },
         {
            "name":"EL CASTILLO",
            "value":"251",
            "text":"COLOMBIA, META, EL CASTILLO",
            "code":"CO-50-251",
            "geoPoint":{
               "lat":3.563510000000065,
               "lon":-73.79541999999998
            },
            "geoRectangle":{
               "Xmin":-73.93641999999998,
               "Xmax":-73.65441999999997,
               "Ymin":3.422510000000065,
               "Ymax":3.704510000000065
            }
         },
         {
            "name":"URIBE",
            "value":"370",
            "text":"COLOMBIA, META, URIBE",
            "code":"CO-50-370",
            "geoPoint":{
               "lat":3.240570000000048,
               "lon":-74.35459999999995
            },
            "geoRectangle":{
               "Xmin":-74.93959999999994,
               "Xmax":-73.76959999999995,
               "Ymin":2.655570000000048,
               "Ymax":3.825570000000048
            }
         }
      ]
   },
   {
      "name":"NARIÑO",
      "value":"52",
      "text":"COLOMBIA, NARIÑO",
      "code":"CO-52",
      "geoPoint":{
         "lat":1.5482626760000358,
         "lon":-77.85752325199996
      },
      "geoRectangle":{
         "Xmin":-78.96952325199996,
         "Xmax":-76.74552325199997,
         "Ymin":0.43626267600003565,
         "Ymax":2.660262676000036
      },
      "municipality":[
         {
            "name":"CUMBAL",
            "value":"227",
            "text":"COLOMBIA, NARIÑO, CUMBAL",
            "code":"CO-52-227",
            "geoPoint":{
               "lat":0.9059600000000501,
               "lon":-77.78943999999996
            },
            "geoRectangle":{
               "Xmin":-77.96443999999995,
               "Xmax":-77.61443999999996,
               "Ymin":0.73096000000005,
               "Ymax":1.08096000000005
            }
         },
         {
            "name":"CUMBITARA",
            "value":"233",
            "text":"COLOMBIA, NARIÑO, CUMBITARA",
            "code":"CO-52-233",
            "geoPoint":{
               "lat":1.6472500000000423,
               "lon":-77.57851999999997
            },
            "geoRectangle":{
               "Xmin":-77.72051999999996,
               "Xmax":-77.43651999999997,
               "Ymin":1.5052500000000424,
               "Ymax":1.7892500000000422
            }
         },
         {
            "name":"EL ROSARIO",
            "value":"256",
            "text":"COLOMBIA, NARIÑO, EL ROSARIO",
            "code":"CO-52-256",
            "geoPoint":{
               "lat":1.7418700000000626,
               "lon":-77.33541999999994
            },
            "geoRectangle":{
               "Xmin":-77.50941999999995,
               "Xmax":-77.16141999999994,
               "Ymin":1.5678700000000627,
               "Ymax":1.9158700000000626
            }
         },
         {
            "name":"LA LLANADA",
            "value":"385",
            "text":"COLOMBIA, NARIÑO, LA LLANADA",
            "code":"CO-52-385",
            "geoPoint":{
               "lat":1.472840000000076,
               "lon":-77.57999999999998
            },
            "geoRectangle":{
               "Xmin":-77.72199999999998,
               "Xmax":-77.43799999999999,
               "Ymin":1.3308400000000762,
               "Ymax":1.614840000000076
            }
         },
         {
            "name":"LOS ANDES",
            "value":"418",
            "text":"COLOMBIA, NARIÑO, LOS ANDES",
            "code":"CO-52-418",
            "geoPoint":{
               "lat":1.2128400000000283,
               "lon":-77.29378999999994
            },
            "geoRectangle":{
               "Xmin":-77.30378999999995,
               "Xmax":-77.28378999999994,
               "Ymin":1.2028400000000283,
               "Ymax":1.2228400000000283
            }
         },
         {
            "name":"POLICARPA",
            "value":"540",
            "text":"COLOMBIA, NARIÑO, POLICARPA",
            "code":"CO-52-540",
            "geoPoint":{
               "lat":1.5996500000000538,
               "lon":-77.46632999999997
            },
            "geoRectangle":{
               "Xmin":-77.60932999999997,
               "Xmax":-77.32332999999997,
               "Ymin":1.4566500000000537,
               "Ymax":1.7426500000000538
            }
         },
         {
            "name":"RICAURTE",
            "value":"612",
            "text":"COLOMBIA, NARIÑO, RICAURTE",
            "code":"CO-52-612",
            "geoPoint":{
               "lat":1.210360000000037,
               "lon":-77.99398999999994
            },
            "geoRectangle":{
               "Xmin":-78.27598999999994,
               "Xmax":-77.71198999999994,
               "Ymin":0.9283600000000369,
               "Ymax":1.492360000000037
            }
         },
         {
            "name":"BARBACOAS",
            "value":"079",
            "text":"COLOMBIA, NARIÑO, BARBACOAS",
            "code":"CO-52-079",
            "geoPoint":{
               "lat":1.6729500000000712,
               "lon":-78.13856999999996
            },
            "geoRectangle":{
               "Xmin":-78.53756999999996,
               "Xmax":-77.73956999999996,
               "Ymin":1.2739500000000712,
               "Ymax":2.0719500000000712
            }
         },
         {
            "name":"EL CHARCO",
            "value":"250",
            "text":"COLOMBIA, NARIÑO, EL CHARCO",
            "code":"CO-52-250",
            "geoPoint":{
               "lat":2.4771800000000326,
               "lon":-78.11099999999999
            },
            "geoRectangle":{
               "Xmin":-78.52799999999999,
               "Xmax":-77.69399999999999,
               "Ymin":2.0601800000000328,
               "Ymax":2.8941800000000324
            }
         },
         {
            "name":"LA TOLA",
            "value":"390",
            "text":"COLOMBIA, NARIÑO, LA TOLA",
            "code":"CO-52-390",
            "geoPoint":{
               "lat":2.3996100000000524,
               "lon":-78.18971999999997
            },
            "geoRectangle":{
               "Xmin":-78.37971999999996,
               "Xmax":-77.99971999999997,
               "Ymin":2.2096100000000525,
               "Ymax":2.5896100000000524
            }
         },
         {
            "name":"MAGÜÍ (Payán)",
            "value":"427",
            "text":"COLOMBIA, NARIÑO, MAGÜÍ (Payán)",
            "code":"CO-52-427",
            "geoPoint":{
               "lat":1.7653000000000247,
               "lon":-78.18235999999996
            },
            "geoRectangle":{
               "Xmin":-78.43335999999996,
               "Xmax":-77.93135999999996,
               "Ymin":1.5143000000000248,
               "Ymax":2.0163000000000246
            }
         },
         {
            "name":"MOSQUERA",
            "value":"473",
            "text":"COLOMBIA, NARIÑO, MOSQUERA",
            "code":"CO-52-473",
            "geoPoint":{
               "lat":2.509110000000021,
               "lon":-78.45144999999997
            },
            "geoRectangle":{
               "Xmin":-78.60044999999997,
               "Xmax":-78.30244999999996,
               "Ymin":2.360110000000021,
               "Ymax":2.658110000000021
            }
         },
         {
            "name":"SANDONÁ",
            "value":"683",
            "text":"COLOMBIA, NARIÑO, SANDONÁ",
            "code":"CO-52-683",
            "geoPoint":{
               "lat":1.2849900000000503,
               "lon":-77.47230999999994
            },
            "geoRectangle":{
               "Xmin":-77.54230999999993,
               "Xmax":-77.40230999999994,
               "Ymin":1.2149900000000502,
               "Ymax":1.3549900000000503
            }
         },
         {
            "name":"SAN BERNARDO",
            "value":"685",
            "text":"COLOMBIA, NARIÑO, SAN BERNARDO",
            "code":"CO-52-685",
            "geoPoint":{
               "lat":1.5162800000000516,
               "lon":-77.04527999999993
            },
            "geoRectangle":{
               "Xmin":-77.10127999999993,
               "Xmax":-76.98927999999994,
               "Ymin":1.4602800000000515,
               "Ymax":1.5722800000000516
            }
         },
         {
            "name":"SAN LORENZO",
            "value":"687",
            "text":"COLOMBIA, NARIÑO, SAN LORENZO",
            "code":"CO-52-687",
            "geoPoint":{
               "lat":1.5033100000000559,
               "lon":-77.21576999999996
            },
            "geoRectangle":{
               "Xmin":-77.31076999999996,
               "Xmax":-77.12076999999996,
               "Ymin":1.408310000000056,
               "Ymax":1.5983100000000559
            }
         },
         {
            "name":"POTOSÍ",
            "value":"560",
            "text":"COLOMBIA, NARIÑO, POTOSÍ",
            "code":"CO-52-560",
            "geoPoint":{
               "lat":0.807200000000023,
               "lon":-77.57233999999994
            },
            "geoRectangle":{
               "Xmin":-77.72433999999994,
               "Xmax":-77.42033999999994,
               "Ymin":0.655200000000023,
               "Ymax":0.959200000000023
            }
         },
         {
            "name":"SAN PABLO",
            "value":"693",
            "text":"COLOMBIA, NARIÑO, SAN PABLO",
            "code":"CO-52-693",
            "geoPoint":{
               "lat":1.669080000000065,
               "lon":-77.01108999999997
            },
            "geoRectangle":{
               "Xmin":-77.08008999999997,
               "Xmax":-76.94208999999996,
               "Ymin":1.600080000000065,
               "Ymax":1.738080000000065
            }
         },
         {
            "name":"PROVIDENCIA",
            "value":"565",
            "text":"COLOMBIA, NARIÑO, PROVIDENCIA",
            "code":"CO-52-565",
            "geoPoint":{
               "lat":1.238800000000026,
               "lon":-77.59679999999997
            },
            "geoRectangle":{
               "Xmin":-77.64179999999998,
               "Xmax":-77.55179999999997,
               "Ymin":1.1938000000000262,
               "Ymax":1.283800000000026
            }
         },
         {
            "name":"SAN PEDRO DE CARTAGO",
            "value":"694",
            "text":"COLOMBIA, NARIÑO, SAN PEDRO DE CARTAGO",
            "code":"CO-52-694",
            "geoPoint":{
               "lat":1.5522100000000592,
               "lon":-77.11993999999999
            },
            "geoRectangle":{
               "Xmin":-77.16793999999999,
               "Xmax":-77.07193999999998,
               "Ymin":1.504210000000059,
               "Ymax":1.6002100000000592
            }
         },
         {
            "name":"PUERRES",
            "value":"573",
            "text":"COLOMBIA, NARIÑO, PUERRES",
            "code":"CO-52-573",
            "geoPoint":{
               "lat":0.8844300000000658,
               "lon":-77.50380999999999
            },
            "geoRectangle":{
               "Xmin":-77.66581,
               "Xmax":-77.34180999999998,
               "Ymin":0.7224300000000657,
               "Ymax":1.0464300000000657
            }
         },
         {
            "name":"SANTA CRUZ (Guachavés)",
            "value":"699",
            "text":"COLOMBIA, NARIÑO, SANTA CRUZ (Guachavés)",
            "code":"CO-52-699",
            "geoPoint":{
               "lat":1.2242900000000532,
               "lon":-77.67999999999995
            },
            "geoRectangle":{
               "Xmin":-77.68999999999996,
               "Xmax":-77.66999999999994,
               "Ymin":1.2142900000000532,
               "Ymax":1.2342900000000532
            }
         },
         {
            "name":"PUPIALES",
            "value":"585",
            "text":"COLOMBIA, NARIÑO, PUPIALES",
            "code":"CO-52-585",
            "geoPoint":{
               "lat":0.870930000000044,
               "lon":-77.64039999999994
            },
            "geoRectangle":{
               "Xmin":-77.70839999999994,
               "Xmax":-77.57239999999994,
               "Ymin":0.8029300000000439,
               "Ymax":0.938930000000044
            }
         },
         {
            "name":"SAMANIEGO",
            "value":"678",
            "text":"COLOMBIA, NARIÑO, SAMANIEGO",
            "code":"CO-52-678",
            "geoPoint":{
               "lat":1.3374300000000403,
               "lon":-77.59256999999997
            },
            "geoRectangle":{
               "Xmin":-77.78856999999996,
               "Xmax":-77.39656999999997,
               "Ymin":1.1414300000000404,
               "Ymax":1.5334300000000403
            }
         },
         {
            "name":"CONSACÁ",
            "value":"207",
            "text":"COLOMBIA, NARIÑO, CONSACÁ",
            "code":"CO-52-207",
            "geoPoint":{
               "lat":1.2154400000000578,
               "lon":-77.46954999999997
            },
            "geoRectangle":{
               "Xmin":-77.48454999999997,
               "Xmax":-77.45454999999997,
               "Ymin":1.200440000000058,
               "Ymax":1.2304400000000577
            }
         },
         {
            "name":"CONTADERO",
            "value":"210",
            "text":"COLOMBIA, NARIÑO, CONTADERO",
            "code":"CO-52-210",
            "geoPoint":{
               "lat":0.909370000000024,
               "lon":-77.54821999999996
            },
            "geoRectangle":{
               "Xmin":-77.59021999999996,
               "Xmax":-77.50621999999996,
               "Ymin":0.867370000000024,
               "Ymax":0.951370000000024
            }
         },
         {
            "name":"CÓRDOBA",
            "value":"215",
            "text":"COLOMBIA, NARIÑO, CÓRDOBA",
            "code":"CO-52-215",
            "geoPoint":{
               "lat":0.8532700000000659,
               "lon":-77.51826999999997
            },
            "geoRectangle":{
               "Xmin":-77.66326999999997,
               "Xmax":-77.37326999999998,
               "Ymin":0.7082700000000659,
               "Ymax":0.9982700000000659
            }
         },
         {
            "name":"PASTO",
            "value":"001",
            "text":"COLOMBIA, NARIÑO, PASTO",
            "code":"CO-52-001",
            "geoPoint":{
               "lat":1.2095000000000482,
               "lon":-77.27667999999994
            },
            "geoRectangle":{
               "Xmin":-77.50367999999995,
               "Xmax":-77.04967999999994,
               "Ymin":0.9825000000000482,
               "Ymax":1.4365000000000483
            }
         },
         {
            "name":"ALBÁN (San José)",
            "value":"019",
            "text":"COLOMBIA, NARIÑO, ALBÁN (San José)",
            "code":"CO-52-019",
            "geoPoint":{
               "lat":1.4738600000000588,
               "lon":-77.08141999999998
            },
            "geoRectangle":{
               "Xmin":-77.09141999999999,
               "Xmax":-77.07141999999997,
               "Ymin":1.4638600000000588,
               "Ymax":1.4838600000000588
            }
         },
         {
            "name":"ALDANA",
            "value":"022",
            "text":"COLOMBIA, NARIÑO, ALDANA",
            "code":"CO-52-022",
            "geoPoint":{
               "lat":0.8827300000000378,
               "lon":-77.70030999999994
            },
            "geoRectangle":{
               "Xmin":-77.75330999999994,
               "Xmax":-77.64730999999995,
               "Ymin":0.8297300000000377,
               "Ymax":0.9357300000000378
            }
         },
         {
            "name":"CUASPUD",
            "value":"224",
            "text":"COLOMBIA, NARIÑO, CUASPUD",
            "code":"CO-52-224",
            "geoPoint":{
               "lat":0.8629000000000246,
               "lon":-77.72747999999996
            },
            "geoRectangle":{
               "Xmin":-77.78447999999996,
               "Xmax":-77.67047999999996,
               "Ymin":0.8059000000000246,
               "Ymax":0.9199000000000247
            }
         },
         {
            "name":"ANCUYA",
            "value":"036",
            "text":"COLOMBIA, NARIÑO, ANCUYA",
            "code":"CO-52-036",
            "geoPoint":{
               "lat":1.2703000000000202,
               "lon":-77.51574999999997
            },
            "geoRectangle":{
               "Xmin":-77.53074999999997,
               "Xmax":-77.50074999999997,
               "Ymin":1.2553000000000203,
               "Ymax":1.28530000000002
            }
         },
         {
            "name":"ARBOLEDA",
            "value":"051",
            "text":"COLOMBIA, NARIÑO, ARBOLEDA",
            "code":"CO-52-051",
            "geoPoint":{
               "lat":1.5032000000000494,
               "lon":-77.13486999999998
            },
            "geoRectangle":{
               "Xmin":-77.18386999999998,
               "Xmax":-77.08586999999997,
               "Ymin":1.4542000000000495,
               "Ymax":1.5522000000000493
            }
         },
         {
            "name":"OLAYA HERRERA (Bocas de Satinga)",
            "value":"490",
            "text":"COLOMBIA, NARIÑO, OLAYA HERRERA (Bocas de Satinga)",
            "code":"CO-52-490",
            "geoPoint":{
               "lat":2.3481400000000576,
               "lon":-78.32570999999996
            },
            "geoRectangle":{
               "Xmin":-78.33570999999996,
               "Xmax":-78.31570999999995,
               "Ymin":2.338140000000058,
               "Ymax":2.3581400000000574
            }
         },
         {
            "name":"FRANCISCO PIZARRO (Salahonda)",
            "value":"520",
            "text":"COLOMBIA, NARIÑO, FRANCISCO PIZARRO (Salahonda)",
            "code":"CO-52-520",
            "geoPoint":{
               "lat":2.039920000000052,
               "lon":-78.65827999999993
            },
            "geoRectangle":{
               "Xmin":-78.81927999999994,
               "Xmax":-78.49727999999993,
               "Ymin":1.8789200000000519,
               "Ymax":2.200920000000052
            }
         },
         {
            "name":"ROBERTO PAYÁN (San José)",
            "value":"621",
            "text":"COLOMBIA, NARIÑO, ROBERTO PAYÁN (San José)",
            "code":"CO-52-621",
            "geoPoint":{
               "lat":1.6960300000000643,
               "lon":-78.24501999999995
            },
            "geoRectangle":{
               "Xmin":-78.49101999999995,
               "Xmax":-77.99901999999996,
               "Ymin":1.4500300000000643,
               "Ymax":1.9420300000000643
            }
         },
         {
            "name":"SANTA BÁRBARA (Iscuandé)",
            "value":"696",
            "text":"COLOMBIA, NARIÑO, SANTA BÁRBARA (Iscuandé)",
            "code":"CO-52-696",
            "geoPoint":{
               "lat":2.4500700000000393,
               "lon":-77.97935999999999
            },
            "geoRectangle":{
               "Xmin":-77.99435999999999,
               "Xmax":-77.96435999999999,
               "Ymin":2.435070000000039,
               "Ymax":2.4650700000000394
            }
         },
         {
            "name":"TUMACO",
            "value":"835",
            "text":"COLOMBIA, NARIÑO, TUMACO",
            "code":"CO-52-835",
            "geoPoint":{
               "lat":1.8039600000000746,
               "lon":-78.77996999999993
            },
            "geoRectangle":{
               "Xmin":-78.82196999999994,
               "Xmax":-78.73796999999993,
               "Ymin":1.7619600000000746,
               "Ymax":1.8459600000000747
            }
         },
         {
            "name":"TAMINANGO",
            "value":"786",
            "text":"COLOMBIA, NARIÑO, TAMINANGO",
            "code":"CO-52-786",
            "geoPoint":{
               "lat":1.5701800000000503,
               "lon":-77.28050999999994
            },
            "geoRectangle":{
               "Xmin":-77.36250999999993,
               "Xmax":-77.19850999999994,
               "Ymin":1.4881800000000502,
               "Ymax":1.6521800000000504
            }
         },
         {
            "name":"TANGUA",
            "value":"788",
            "text":"COLOMBIA, NARIÑO, TANGUA",
            "code":"CO-52-788",
            "geoPoint":{
               "lat":1.0949100000000271,
               "lon":-77.39424999999994
            },
            "geoRectangle":{
               "Xmin":-77.49124999999994,
               "Xmax":-77.29724999999995,
               "Ymin":0.9979100000000272,
               "Ymax":1.1919100000000271
            }
         },
         {
            "name":"CHACHAGÜÍ",
            "value":"240",
            "text":"COLOMBIA, NARIÑO, CHACHAGÜÍ",
            "code":"CO-52-240",
            "geoPoint":{
               "lat":1.3610500000000343,
               "lon":-77.28343999999998
            },
            "geoRectangle":{
               "Xmin":-77.36743999999999,
               "Xmax":-77.19943999999998,
               "Ymin":1.2770500000000342,
               "Ymax":1.4450500000000344
            }
         },
         {
            "name":"EL PEÑOL",
            "value":"254",
            "text":"COLOMBIA, NARIÑO, EL PEÑOL",
            "code":"CO-52-254",
            "geoPoint":{
               "lat":1.4527200000000562,
               "lon":-77.44018999999997
            },
            "geoRectangle":{
               "Xmin":-77.50718999999997,
               "Xmax":-77.37318999999998,
               "Ymin":1.3857200000000562,
               "Ymax":1.5197200000000561
            }
         },
         {
            "name":"EL TABLÓN DE GÓMEZ",
            "value":"258",
            "text":"COLOMBIA, NARIÑO, EL TABLÓN DE GÓMEZ",
            "code":"CO-52-258",
            "geoPoint":{
               "lat":1.4293500000000563,
               "lon":-77.08160999999996
            },
            "geoRectangle":{
               "Xmin":-77.18160999999995,
               "Xmax":-76.98160999999996,
               "Ymin":1.3293500000000562,
               "Ymax":1.5293500000000564
            }
         },
         {
            "name":"TUQUERRES",
            "value":"838",
            "text":"COLOMBIA, NARIÑO, TUQUERRES",
            "code":"CO-52-838",
            "geoPoint":{
               "lat":1.0863800000000197,
               "lon":-77.61806999999999
            },
            "geoRectangle":{
               "Xmin":-77.71706999999999,
               "Xmax":-77.51906999999999,
               "Ymin":0.9873800000000197,
               "Ymax":1.1853800000000196
            }
         },
         {
            "name":"LINARES",
            "value":"411",
            "text":"COLOMBIA, NARIÑO, LINARES",
            "code":"CO-52-411",
            "geoPoint":{
               "lat":1.3505700000000616,
               "lon":-77.52344999999997
            },
            "geoRectangle":{
               "Xmin":-77.59844999999997,
               "Xmax":-77.44844999999997,
               "Ymin":1.2755700000000616,
               "Ymax":1.4255700000000615
            }
         },
         {
            "name":"YACUANQUER",
            "value":"885",
            "text":"COLOMBIA, NARIÑO, YACUANQUER",
            "code":"CO-52-885",
            "geoPoint":{
               "lat":1.1157100000000355,
               "lon":-77.40166999999997
            },
            "geoRectangle":{
               "Xmin":-77.46566999999996,
               "Xmax":-77.33766999999997,
               "Ymin":1.0517100000000354,
               "Ymax":1.1797100000000356
            }
         },
         {
            "name":"MALLAMA (Piedrancha)",
            "value":"435",
            "text":"COLOMBIA, NARIÑO, MALLAMA (Piedrancha)",
            "code":"CO-52-435",
            "geoPoint":{
               "lat":1.1405700000000252,
               "lon":-77.86410999999998
            },
            "geoRectangle":{
               "Xmin":-78.03510999999999,
               "Xmax":-77.69310999999998,
               "Ymin":0.9695700000000251,
               "Ymax":1.3115700000000252
            }
         },
         {
            "name":"LA CRUZ",
            "value":"378",
            "text":"COLOMBIA, NARIÑO, LA CRUZ",
            "code":"CO-52-378",
            "geoPoint":{
               "lat":1.6001100000000292,
               "lon":-76.97012999999998
            },
            "geoRectangle":{
               "Xmin":-77.05612999999998,
               "Xmax":-76.88412999999998,
               "Ymin":1.5141100000000292,
               "Ymax":1.6861100000000293
            }
         },
         {
            "name":"LA FLORIDA",
            "value":"381",
            "text":"COLOMBIA, NARIÑO, LA FLORIDA",
            "code":"CO-52-381",
            "geoPoint":{
               "lat":1.298970000000054,
               "lon":-77.40590999999995
            },
            "geoRectangle":{
               "Xmin":-77.49990999999994,
               "Xmax":-77.31190999999995,
               "Ymin":1.2049700000000538,
               "Ymax":1.392970000000054
            }
         },
         {
            "name":"EL TAMBO",
            "value":"260",
            "text":"COLOMBIA, NARIÑO, EL TAMBO",
            "code":"CO-52-260",
            "geoPoint":{
               "lat":1.4073300000000586,
               "lon":-77.39174999999994
            },
            "geoRectangle":{
               "Xmin":-77.48974999999994,
               "Xmax":-77.29374999999995,
               "Ymin":1.3093300000000585,
               "Ymax":1.5053300000000587
            }
         },
         {
            "name":"FUNES",
            "value":"287",
            "text":"COLOMBIA, NARIÑO, FUNES",
            "code":"CO-52-287",
            "geoPoint":{
               "lat":1.0009000000000583,
               "lon":-77.44755999999995
            },
            "geoRectangle":{
               "Xmin":-77.61555999999996,
               "Xmax":-77.27955999999995,
               "Ymin":0.8329000000000583,
               "Ymax":1.1689000000000582
            }
         },
         {
            "name":"GUACHUCAL",
            "value":"317",
            "text":"COLOMBIA, NARIÑO, GUACHUCAL",
            "code":"CO-52-317",
            "geoPoint":{
               "lat":0.9586900000000469,
               "lon":-77.73394999999994
            },
            "geoRectangle":{
               "Xmin":-77.82094999999994,
               "Xmax":-77.64694999999993,
               "Ymin":0.8716900000000469,
               "Ymax":1.0456900000000469
            }
         },
         {
            "name":"LA UNIÓN",
            "value":"399",
            "text":"COLOMBIA, NARIÑO, LA UNIÓN",
            "code":"CO-52-399",
            "geoPoint":{
               "lat":1.60283000000004,
               "lon":-77.13116999999994
            },
            "geoRectangle":{
               "Xmin":-77.21616999999993,
               "Xmax":-77.04616999999995,
               "Ymin":1.51783000000004,
               "Ymax":1.68783000000004
            }
         },
         {
            "name":"GUAITARILLA",
            "value":"320",
            "text":"COLOMBIA, NARIÑO, GUAITARILLA",
            "code":"CO-52-320",
            "geoPoint":{
               "lat":1.1295200000000705,
               "lon":-77.55092999999994
            },
            "geoRectangle":{
               "Xmin":-77.61192999999994,
               "Xmax":-77.48992999999993,
               "Ymin":1.0685200000000705,
               "Ymax":1.1905200000000704
            }
         },
         {
            "name":"LEIVA",
            "value":"405",
            "text":"COLOMBIA, NARIÑO, LEIVA",
            "code":"CO-52-405",
            "geoPoint":{
               "lat":1.9347400000000334,
               "lon":-77.30570999999998
            },
            "geoRectangle":{
               "Xmin":-77.44270999999998,
               "Xmax":-77.16870999999998,
               "Ymin":1.7977400000000334,
               "Ymax":2.0717400000000334
            }
         },
         {
            "name":"BELEN",
            "value":"083",
            "text":"COLOMBIA, NARIÑO, BELEN",
            "code":"CO-52-083",
            "geoPoint":{
               "lat":1.5956600000000662,
               "lon":-77.01536999999996
            },
            "geoRectangle":{
               "Xmin":-77.05636999999996,
               "Xmax":-76.97436999999996,
               "Ymin":1.5546600000000663,
               "Ymax":1.6366600000000662
            }
         },
         {
            "name":"BUESACO",
            "value":"110",
            "text":"COLOMBIA, NARIÑO, BUESACO",
            "code":"CO-52-110",
            "geoPoint":{
               "lat":1.3821400000000494,
               "lon":-77.15673999999996
            },
            "geoRectangle":{
               "Xmin":-77.31173999999996,
               "Xmax":-77.00173999999996,
               "Ymin":1.2271400000000494,
               "Ymax":1.5371400000000495
            }
         },
         {
            "name":"COLON",
            "value":"203",
            "text":"COLOMBIA, NARIÑO, COLON",
            "code":"CO-52-203",
            "geoPoint":{
               "lat":1.3255450554776687,
               "lon":-77.02131490084403
            },
            "geoRectangle":{
               "Xmin":-77.02231490084404,
               "Xmax":-77.02031490084403,
               "Ymin":1.3245450554776688,
               "Ymax":1.3265450554776685
            }
         },
         {
            "name":"NARIÑO",
            "value":"480",
            "text":"COLOMBIA, NARIÑO, NARIÑO",
            "code":"CO-52-480",
            "geoPoint":{
               "lat":1.5482626760000358,
               "lon":-77.85752325199996
            },
            "geoRectangle":{
               "Xmin":-78.96952325199996,
               "Xmax":-76.74552325199997,
               "Ymin":0.43626267600003565,
               "Ymax":2.660262676000036
            }
         },
         {
            "name":"OSPINA",
            "value":"506",
            "text":"COLOMBIA, NARIÑO, OSPINA",
            "code":"CO-52-506",
            "geoPoint":{
               "lat":1.0586000000000695,
               "lon":-77.56592999999998
            },
            "geoRectangle":{
               "Xmin":-77.61292999999998,
               "Xmax":-77.51892999999998,
               "Ymin":1.0116000000000696,
               "Ymax":1.1056000000000694
            }
         },
         {
            "name":"SAPUYES",
            "value":"720",
            "text":"COLOMBIA, NARIÑO, SAPUYES",
            "code":"CO-52-720",
            "geoPoint":{
               "lat":1.0380000000000678,
               "lon":-77.62089999999995
            },
            "geoRectangle":{
               "Xmin":-77.69489999999995,
               "Xmax":-77.54689999999995,
               "Ymin":0.9640000000000678,
               "Ymax":1.1120000000000678
            }
         },
         {
            "name":"GUALMATÁN",
            "value":"323",
            "text":"COLOMBIA, NARIÑO, GUALMATÁN",
            "code":"CO-52-323",
            "geoPoint":{
               "lat":0.9211200000000304,
               "lon":-77.56751999999994
            },
            "geoRectangle":{
               "Xmin":-77.60251999999994,
               "Xmax":-77.53251999999995,
               "Ymin":0.8861200000000303,
               "Ymax":0.9561200000000304
            }
         },
         {
            "name":"ILES",
            "value":"352",
            "text":"COLOMBIA, NARIÑO, ILES",
            "code":"CO-52-352",
            "geoPoint":{
               "lat":0.9702900000000341,
               "lon":-77.52122999999995
            },
            "geoRectangle":{
               "Xmin":-77.57522999999995,
               "Xmax":-77.46722999999994,
               "Ymin":0.916290000000034,
               "Ymax":1.0242900000000341
            }
         },
         {
            "name":"IMUES",
            "value":"354",
            "text":"COLOMBIA, NARIÑO, IMUES",
            "code":"CO-52-354",
            "geoPoint":{
               "lat":1.055590000000052,
               "lon":-77.49589999999995
            },
            "geoRectangle":{
               "Xmin":-77.54989999999995,
               "Xmax":-77.44189999999995,
               "Ymin":1.001590000000052,
               "Ymax":1.109590000000052
            }
         },
         {
            "name":"IPIALES",
            "value":"356",
            "text":"COLOMBIA, NARIÑO, IPIALES",
            "code":"CO-52-356",
            "geoPoint":{
               "lat":0.8274900000000684,
               "lon":-77.63546999999994
            },
            "geoRectangle":{
               "Xmin":-77.92446999999994,
               "Xmax":-77.34646999999994,
               "Ymin":0.5384900000000685,
               "Ymax":1.1164900000000684
            }
         }
      ]
   },
   {
      "name":"NORTE DE SANTANDER",
      "value":"54",
      "text":"COLOMBIA, NORTE DE SANTANDER",
      "code":"CO-54",
      "geoPoint":{
         "lat":8.095474954000053,
         "lon":-72.88368275199997
      },
      "geoRectangle":{
         "Xmin":-73.88568275199997,
         "Xmax":-71.88168275199997,
         "Ymin":7.093474954000054,
         "Ymax":9.097474954000054
      },
      "municipality":[
         {
            "name":"CÚCUTA",
            "value":"001",
            "text":"COLOMBIA, NORTE DE SANTANDER, CÚCUTA",
            "code":"CO-54-001",
            "geoPoint":{
               "lat":7.909660000000031,
               "lon":-72.50409999999994
            },
            "geoRectangle":{
               "Xmin":-72.74609999999994,
               "Xmax":-72.26209999999993,
               "Ymin":7.667660000000031,
               "Ymax":8.151660000000032
            }
         },
         {
            "name":"ABREGO",
            "value":"003",
            "text":"COLOMBIA, NORTE DE SANTANDER, ABREGO",
            "code":"CO-54-003",
            "geoPoint":{
               "lat":8.08095000000003,
               "lon":-73.22051999999996
            },
            "geoRectangle":{
               "Xmin":-73.44851999999996,
               "Xmax":-72.99251999999997,
               "Ymin":7.85295000000003,
               "Ymax":8.30895000000003
            }
         },
         {
            "name":"ARBOLEDAS",
            "value":"051",
            "text":"COLOMBIA, NORTE DE SANTANDER, ARBOLEDAS",
            "code":"CO-54-051",
            "geoPoint":{
               "lat":7.643000000000029,
               "lon":-72.79951999999997
            },
            "geoRectangle":{
               "Xmin":-72.94451999999997,
               "Xmax":-72.65451999999998,
               "Ymin":7.4980000000000295,
               "Ymax":7.788000000000029
            }
         },
         {
            "name":"BOCHALEMA",
            "value":"099",
            "text":"COLOMBIA, NORTE DE SANTANDER, BOCHALEMA",
            "code":"CO-54-099",
            "geoPoint":{
               "lat":7.610940000000028,
               "lon":-72.64754999999997
            },
            "geoRectangle":{
               "Xmin":-72.74954999999997,
               "Xmax":-72.54554999999996,
               "Ymin":7.5089400000000275,
               "Ymax":7.712940000000028
            }
         },
         {
            "name":"BUCARASICA",
            "value":"109",
            "text":"COLOMBIA, NORTE DE SANTANDER, BUCARASICA",
            "code":"CO-54-109",
            "geoPoint":{
               "lat":8.040990000000022,
               "lon":-72.86786999999998
            },
            "geoRectangle":{
               "Xmin":-72.96286999999998,
               "Xmax":-72.77286999999998,
               "Ymin":7.945990000000022,
               "Ymax":8.135990000000023
            }
         },
         {
            "name":"CÁCOTA",
            "value":"125",
            "text":"COLOMBIA, NORTE DE SANTANDER, CÁCOTA",
            "code":"CO-54-125",
            "geoPoint":{
               "lat":7.268610000000024,
               "lon":-72.64161999999999
            },
            "geoRectangle":{
               "Xmin":-72.71461999999998,
               "Xmax":-72.56862,
               "Ymin":7.195610000000023,
               "Ymax":7.341610000000024
            }
         },
         {
            "name":"CÁCHIRA",
            "value":"128",
            "text":"COLOMBIA, NORTE DE SANTANDER, CÁCHIRA",
            "code":"CO-54-128",
            "geoPoint":{
               "lat":7.741040000000055,
               "lon":-73.04829999999998
            },
            "geoRectangle":{
               "Xmin":-73.06329999999998,
               "Xmax":-73.03329999999998,
               "Ymin":7.726040000000055,
               "Ymax":7.756040000000055
            }
         },
         {
            "name":"CHINÁCOTA",
            "value":"172",
            "text":"COLOMBIA, NORTE DE SANTANDER, CHINÁCOTA",
            "code":"CO-54-172",
            "geoPoint":{
               "lat":7.604250000000036,
               "lon":-72.60113999999999
            },
            "geoRectangle":{
               "Xmin":-72.69113999999999,
               "Xmax":-72.51113999999998,
               "Ymin":7.514250000000036,
               "Ymax":7.694250000000036
            }
         },
         {
            "name":"CUCUTILLA",
            "value":"223",
            "text":"COLOMBIA, NORTE DE SANTANDER, CUCUTILLA",
            "code":"CO-54-223",
            "geoPoint":{
               "lat":7.539160000000038,
               "lon":-72.77281999999997
            },
            "geoRectangle":{
               "Xmin":-72.89581999999997,
               "Xmax":-72.64981999999996,
               "Ymin":7.416160000000038,
               "Ymax":7.662160000000038
            }
         },
         {
            "name":"DURANIA",
            "value":"239",
            "text":"COLOMBIA, NORTE DE SANTANDER, DURANIA",
            "code":"CO-54-239",
            "geoPoint":{
               "lat":7.714510000000075,
               "lon":-72.65824999999995
            },
            "geoRectangle":{
               "Xmin":-72.74224999999996,
               "Xmax":-72.57424999999995,
               "Ymin":7.630510000000076,
               "Ymax":7.798510000000075
            }
         },
         {
            "name":"EL ZULIA",
            "value":"261",
            "text":"COLOMBIA, NORTE DE SANTANDER, EL ZULIA",
            "code":"CO-54-261",
            "geoPoint":{
               "lat":7.938220000000058,
               "lon":-72.60512999999997
            },
            "geoRectangle":{
               "Xmin":-72.75912999999997,
               "Xmax":-72.45112999999998,
               "Ymin":7.784220000000058,
               "Ymax":8.092220000000058
            }
         },
         {
            "name":"GRAMALOTE",
            "value":"313",
            "text":"COLOMBIA, NORTE DE SANTANDER, GRAMALOTE",
            "code":"CO-54-313",
            "geoPoint":{
               "lat":7.888600000000054,
               "lon":-72.79778999999996
            },
            "geoRectangle":{
               "Xmin":-72.88378999999996,
               "Xmax":-72.71178999999997,
               "Ymin":7.802600000000053,
               "Ymax":7.974600000000054
            }
         },
         {
            "name":"HACARÍ",
            "value":"344",
            "text":"COLOMBIA, NORTE DE SANTANDER, HACARÍ",
            "code":"CO-54-344",
            "geoPoint":{
               "lat":8.321470000000033,
               "lon":-73.14559999999994
            },
            "geoRectangle":{
               "Xmin":-73.26259999999995,
               "Xmax":-73.02859999999994,
               "Ymin":8.204470000000033,
               "Ymax":8.438470000000034
            }
         },
         {
            "name":"HERRÁN",
            "value":"347",
            "text":"COLOMBIA, NORTE DE SANTANDER, HERRÁN",
            "code":"CO-54-347",
            "geoPoint":{
               "lat":7.5067000000000235,
               "lon":-72.48334999999997
            },
            "geoRectangle":{
               "Xmin":-72.55934999999997,
               "Xmax":-72.40734999999998,
               "Ymin":7.430700000000024,
               "Ymax":7.582700000000023
            }
         },
         {
            "name":"LABATECA",
            "value":"377",
            "text":"COLOMBIA, NORTE DE SANTANDER, LABATECA",
            "code":"CO-54-377",
            "geoPoint":{
               "lat":7.2995200000000295,
               "lon":-72.49492999999995
            },
            "geoRectangle":{
               "Xmin":-72.59392999999996,
               "Xmax":-72.39592999999995,
               "Ymin":7.200520000000029,
               "Ymax":7.39852000000003
            }
         },
         {
            "name":"LA ESPERANZA",
            "value":"385",
            "text":"COLOMBIA, NORTE DE SANTANDER, LA ESPERANZA",
            "code":"CO-54-385",
            "geoPoint":{
               "lat":7.6381900000000655,
               "lon":-73.32812999999999
            },
            "geoRectangle":{
               "Xmin":-73.52112999999999,
               "Xmax":-73.13512999999999,
               "Ymin":7.445190000000066,
               "Ymax":7.831190000000065
            }
         },
         {
            "name":"LA PLAYA",
            "value":"398",
            "text":"COLOMBIA, NORTE DE SANTANDER, LA PLAYA",
            "code":"CO-54-398",
            "geoPoint":{
               "lat":8.211560000000077,
               "lon":-73.23853999999994
            },
            "geoRectangle":{
               "Xmin":-73.34653999999995,
               "Xmax":-73.13053999999994,
               "Ymin":8.103560000000076,
               "Ymax":8.319560000000077
            }
         },
         {
            "name":"LOS PATIOS",
            "value":"405",
            "text":"COLOMBIA, NORTE DE SANTANDER, LOS PATIOS",
            "code":"CO-54-405",
            "geoPoint":{
               "lat":7.8342700000000605,
               "lon":-72.50638999999995
            },
            "geoRectangle":{
               "Xmin":-72.59238999999995,
               "Xmax":-72.42038999999995,
               "Ymin":7.74827000000006,
               "Ymax":7.920270000000061
            }
         },
         {
            "name":"LOURDES",
            "value":"418",
            "text":"COLOMBIA, NORTE DE SANTANDER, LOURDES",
            "code":"CO-54-418",
            "geoPoint":{
               "lat":7.945360000000051,
               "lon":-72.83248999999995
            },
            "geoRectangle":{
               "Xmin":-72.89048999999996,
               "Xmax":-72.77448999999994,
               "Ymin":7.887360000000051,
               "Ymax":8.00336000000005
            }
         },
         {
            "name":"MUTISCUA",
            "value":"480",
            "text":"COLOMBIA, NORTE DE SANTANDER, MUTISCUA",
            "code":"CO-54-480",
            "geoPoint":{
               "lat":7.30066000000005,
               "lon":-72.74714999999998
            },
            "geoRectangle":{
               "Xmin":-72.81614999999998,
               "Xmax":-72.67814999999997,
               "Ymin":7.23166000000005,
               "Ymax":7.36966000000005
            }
         },
         {
            "name":"OCAÑA",
            "value":"498",
            "text":"COLOMBIA, NORTE DE SANTANDER, OCAÑA",
            "code":"CO-54-498",
            "geoPoint":{
               "lat":8.241960000000063,
               "lon":-73.35467999999997
            },
            "geoRectangle":{
               "Xmin":-73.53267999999997,
               "Xmax":-73.17667999999998,
               "Ymin":8.063960000000062,
               "Ymax":8.419960000000064
            }
         },
         {
            "name":"PAMPLONA",
            "value":"518",
            "text":"COLOMBIA, NORTE DE SANTANDER, PAMPLONA",
            "code":"CO-54-518",
            "geoPoint":{
               "lat":7.3770900000000665,
               "lon":-72.64723999999995
            },
            "geoRectangle":{
               "Xmin":-72.77023999999996,
               "Xmax":-72.52423999999995,
               "Ymin":7.254090000000066,
               "Ymax":7.500090000000067
            }
         },
         {
            "name":"PAMPLONITA",
            "value":"520",
            "text":"COLOMBIA, NORTE DE SANTANDER, PAMPLONITA",
            "code":"CO-54-520",
            "geoPoint":{
               "lat":7.436650000000043,
               "lon":-72.63791999999995
            },
            "geoRectangle":{
               "Xmin":-72.72191999999995,
               "Xmax":-72.55391999999995,
               "Ymin":7.352650000000043,
               "Ymax":7.5206500000000425
            }
         },
         {
            "name":"CHITAGÀ",
            "value":"174",
            "text":"COLOMBIA, NORTE DE SANTANDER, CHITAGÀ",
            "code":"CO-54-174",
            "geoPoint":{
               "lat":7.1389200000000415,
               "lon":-72.66421999999994
            },
            "geoRectangle":{
               "Xmin":-72.87921999999995,
               "Xmax":-72.44921999999994,
               "Ymin":6.923920000000042,
               "Ymax":7.353920000000041
            }
         },
         {
            "name":"CONVENCIÓN",
            "value":"206",
            "text":"COLOMBIA, NORTE DE SANTANDER, CONVENCIÓN",
            "code":"CO-54-206",
            "geoPoint":{
               "lat":8.468680000000063,
               "lon":-73.33749999999998
            },
            "geoRectangle":{
               "Xmin":-73.67149999999998,
               "Xmax":-73.00349999999997,
               "Ymin":8.134680000000063,
               "Ymax":8.802680000000063
            }
         },
         {
            "name":"EL CARMEN",
            "value":"245",
            "text":"COLOMBIA, NORTE DE SANTANDER, EL CARMEN",
            "code":"CO-54-245",
            "geoPoint":{
               "lat":8.511740000000032,
               "lon":-73.44702999999998
            },
            "geoRectangle":{
               "Xmin":-73.81302999999998,
               "Xmax":-73.08102999999998,
               "Ymin":8.145740000000032,
               "Ymax":8.877740000000031
            }
         },
         {
            "name":"EL TARRA",
            "value":"250",
            "text":"COLOMBIA, NORTE DE SANTANDER, EL TARRA",
            "code":"CO-54-250",
            "geoPoint":{
               "lat":8.576550000000054,
               "lon":-73.09439999999995
            },
            "geoRectangle":{
               "Xmin":-73.24439999999996,
               "Xmax":-72.94439999999994,
               "Ymin":8.426550000000054,
               "Ymax":8.726550000000055
            }
         },
         {
            "name":"TEORAMA",
            "value":"800",
            "text":"COLOMBIA, NORTE DE SANTANDER, TEORAMA",
            "code":"CO-54-800",
            "geoPoint":{
               "lat":8.43748000000005,
               "lon":-73.28676999999993
            },
            "geoRectangle":{
               "Xmin":-73.58076999999993,
               "Xmax":-72.99276999999994,
               "Ymin":8.14348000000005,
               "Ymax":8.731480000000051
            }
         },
         {
            "name":"TIBÚ",
            "value":"810",
            "text":"COLOMBIA, NORTE DE SANTANDER, TIBÚ",
            "code":"CO-54-810",
            "geoPoint":{
               "lat":8.637200000000064,
               "lon":-72.73334999999997
            },
            "geoRectangle":{
               "Xmin":-73.10534999999997,
               "Xmax":-72.36134999999997,
               "Ymin":8.265200000000064,
               "Ymax":9.009200000000064
            }
         },
         {
            "name":"TOLEDO",
            "value":"820",
            "text":"COLOMBIA, NORTE DE SANTANDER, TOLEDO",
            "code":"CO-54-820",
            "geoPoint":{
               "lat":7.310170000000028,
               "lon":-72.48359999999997
            },
            "geoRectangle":{
               "Xmin":-72.74759999999996,
               "Xmax":-72.21959999999997,
               "Ymin":7.046170000000028,
               "Ymax":7.574170000000028
            }
         },
         {
            "name":"PUERTO SANTANDER",
            "value":"553",
            "text":"COLOMBIA, NORTE DE SANTANDER, PUERTO SANTANDER",
            "code":"CO-54-553",
            "geoPoint":{
               "lat":8.360960000000034,
               "lon":-72.40938999999997
            },
            "geoRectangle":{
               "Xmin":-72.45438999999998,
               "Xmax":-72.36438999999997,
               "Ymin":8.315960000000034,
               "Ymax":8.405960000000034
            }
         },
         {
            "name":"RAGONVALIA",
            "value":"599",
            "text":"COLOMBIA, NORTE DE SANTANDER, RAGONVALIA",
            "code":"CO-54-599",
            "geoPoint":{
               "lat":7.578050000000076,
               "lon":-72.47504999999995
            },
            "geoRectangle":{
               "Xmin":-72.52804999999995,
               "Xmax":-72.42204999999996,
               "Ymin":7.525050000000076,
               "Ymax":7.631050000000076
            }
         },
         {
            "name":"SALAZAR",
            "value":"660",
            "text":"COLOMBIA, NORTE DE SANTANDER, SALAZAR",
            "code":"CO-54-660",
            "geoPoint":{
               "lat":7.773670000000038,
               "lon":-72.81167999999997
            },
            "geoRectangle":{
               "Xmin":-72.94667999999997,
               "Xmax":-72.67667999999996,
               "Ymin":7.6386700000000385,
               "Ymax":7.908670000000038
            }
         },
         {
            "name":"SAN CALIXTO",
            "value":"670",
            "text":"COLOMBIA, NORTE DE SANTANDER, SAN CALIXTO",
            "code":"CO-54-670",
            "geoPoint":{
               "lat":8.401980000000037,
               "lon":-73.20658999999995
            },
            "geoRectangle":{
               "Xmin":-73.35858999999995,
               "Xmax":-73.05458999999995,
               "Ymin":8.249980000000038,
               "Ymax":8.553980000000037
            }
         },
         {
            "name":"SAN CAYETANO",
            "value":"673",
            "text":"COLOMBIA, NORTE DE SANTANDER, SAN CAYETANO",
            "code":"CO-54-673",
            "geoPoint":{
               "lat":7.878790000000038,
               "lon":-72.62642999999997
            },
            "geoRectangle":{
               "Xmin":-72.70242999999996,
               "Xmax":-72.55042999999998,
               "Ymin":7.802790000000038,
               "Ymax":7.954790000000037
            }
         },
         {
            "name":"SANTIAGO",
            "value":"680",
            "text":"COLOMBIA, NORTE DE SANTANDER, SANTIAGO",
            "code":"CO-54-680",
            "geoPoint":{
               "lat":7.865060000000028,
               "lon":-72.71656999999993
            },
            "geoRectangle":{
               "Xmin":-72.79656999999993,
               "Xmax":-72.63656999999994,
               "Ymin":7.785060000000028,
               "Ymax":7.945060000000028
            }
         },
         {
            "name":"SARDINATA",
            "value":"720",
            "text":"COLOMBIA, NORTE DE SANTANDER, SARDINATA",
            "code":"CO-54-720",
            "geoPoint":{
               "lat":8.082790000000045,
               "lon":-72.79984999999994
            },
            "geoRectangle":{
               "Xmin":-73.03184999999993,
               "Xmax":-72.56784999999994,
               "Ymin":7.850790000000045,
               "Ymax":8.314790000000045
            }
         },
         {
            "name":"SILOS",
            "value":"743",
            "text":"COLOMBIA, NORTE DE SANTANDER, SILOS",
            "code":"CO-54-743",
            "geoPoint":{
               "lat":7.204390000000046,
               "lon":-72.75737999999996
            },
            "geoRectangle":{
               "Xmin":-72.88237999999996,
               "Xmax":-72.63237999999996,
               "Ymin":7.079390000000046,
               "Ymax":7.329390000000046
            }
         },
         {
            "name":"VILLA CARO",
            "value":"871",
            "text":"COLOMBIA, NORTE DE SANTANDER, VILLA CARO",
            "code":"CO-54-871",
            "geoPoint":{
               "lat":7.91444000000007,
               "lon":-72.97131999999993
            },
            "geoRectangle":{
               "Xmin":-73.10931999999994,
               "Xmax":-72.83331999999993,
               "Ymin":7.77644000000007,
               "Ymax":8.05244000000007
            }
         },
         {
            "name":"VILLA DEL ROSARIO",
            "value":"874",
            "text":"COLOMBIA, NORTE DE SANTANDER, VILLA DEL ROSARIO",
            "code":"CO-54-874",
            "geoPoint":{
               "lat":7.838410000000067,
               "lon":-72.47420999999997
            },
            "geoRectangle":{
               "Xmin":-72.55820999999997,
               "Xmax":-72.39020999999997,
               "Ymin":7.7544100000000675,
               "Ymax":7.922410000000067
            }
         }
      ]
   },
   {
      "name":"PUTUMAYO",
      "value":"86",
      "text":"COLOMBIA, PUTUMAYO",
      "code":"CO-86",
      "geoPoint":{
         "lat":0.45174732600003153,
         "lon":-75.85115201799994
      },
      "geoRectangle":{
         "Xmin":-77.20515201799994,
         "Xmax":-74.49715201799994,
         "Ymin":-0.9022526739999686,
         "Ymax":1.8057473260000316
      },
      "municipality":[
         {
            "name":"PUERTO ASÍS",
            "value":"568",
            "text":"COLOMBIA, PUTUMAYO, PUERTO ASÍS",
            "code":"CO-86-568",
            "geoPoint":{
               "lat":0.5031700000000683,
               "lon":-76.49945999999994
            },
            "geoRectangle":{
               "Xmin":-76.86445999999994,
               "Xmax":-76.13445999999995,
               "Ymin":0.1381700000000683,
               "Ymax":0.8681700000000683
            }
         },
         {
            "name":"PUERTO GUZMÁN",
            "value":"571",
            "text":"COLOMBIA, PUTUMAYO, PUERTO GUZMÁN",
            "code":"CO-86-571",
            "geoPoint":{
               "lat":0.9629200000000537,
               "lon":-76.40772999999996
            },
            "geoRectangle":{
               "Xmin":-76.87872999999996,
               "Xmax":-75.93672999999995,
               "Ymin":0.4919200000000537,
               "Ymax":1.4339200000000538
            }
         },
         {
            "name":"PUERTO LEGUÍZAMO",
            "value":"573",
            "text":"COLOMBIA, PUTUMAYO, PUERTO LEGUÍZAMO",
            "code":"CO-86-573",
            "geoPoint":{
               "lat":-0.19400999999993473,
               "lon":-74.78138999999999
            },
            "geoRectangle":{
               "Xmin":-75.68139,
               "Xmax":-73.88138999999998,
               "Ymin":-1.0940099999999346,
               "Ymax":0.7059900000000653
            }
         },
         {
            "name":"MOCOA",
            "value":"001",
            "text":"COLOMBIA, PUTUMAYO, MOCOA",
            "code":"CO-86-001",
            "geoPoint":{
               "lat":1.1511300000000233,
               "lon":-76.64841999999999
            },
            "geoRectangle":{
               "Xmin":-76.96141999999999,
               "Xmax":-76.33541999999998,
               "Ymin":0.8381300000000234,
               "Ymax":1.4641300000000232
            }
         },
         {
            "name":"SANTIAGO",
            "value":"760",
            "text":"COLOMBIA, PUTUMAYO, SANTIAGO",
            "code":"CO-86-760",
            "geoPoint":{
               "lat":1.1475200000000427,
               "lon":-77.00214999999997
            },
            "geoRectangle":{
               "Xmin":-77.17614999999998,
               "Xmax":-76.82814999999997,
               "Ymin":0.9735200000000427,
               "Ymax":1.3215200000000427
            }
         },
         {
            "name":"VALLE DEL GUAMUEZ",
            "value":"865",
            "text":"COLOMBIA, PUTUMAYO, VALLE DEL GUAMUEZ",
            "code":"CO-86-865",
            "geoPoint":{
               "lat":0.42303000000003976,
               "lon":-76.90540999999996
            },
            "geoRectangle":{
               "Xmin":-77.10240999999996,
               "Xmax":-76.70840999999996,
               "Ymin":0.22603000000003975,
               "Ymax":0.6200300000000398
            }
         },
         {
            "name":"VILLAGARZÓN",
            "value":"885",
            "text":"COLOMBIA, PUTUMAYO, VILLAGARZÓN",
            "code":"CO-86-885",
            "geoPoint":{
               "lat":1.0295900000000415,
               "lon":-76.61726999999996
            },
            "geoRectangle":{
               "Xmin":-76.84726999999997,
               "Xmax":-76.38726999999996,
               "Ymin":0.7995900000000415,
               "Ymax":1.2595900000000415
            }
         },
         {
            "name":"COLÓN",
            "value":"219",
            "text":"COLOMBIA, PUTUMAYO, COLÓN",
            "code":"CO-86-219",
            "geoPoint":{
               "lat":1.1908600000000433,
               "lon":-76.97244999999998
            },
            "geoRectangle":{
               "Xmin":-77.03244999999998,
               "Xmax":-76.91244999999998,
               "Ymin":1.1308600000000433,
               "Ymax":1.2508600000000434
            }
         },
         {
            "name":"ORITO",
            "value":"320",
            "text":"COLOMBIA, PUTUMAYO, ORITO",
            "code":"CO-86-320",
            "geoPoint":{
               "lat":0.6676500000000374,
               "lon":-76.87098999999995
            },
            "geoRectangle":{
               "Xmin":-77.15998999999995,
               "Xmax":-76.58198999999995,
               "Ymin":0.3786500000000374,
               "Ymax":0.9566500000000373
            }
         },
         {
            "name":"SIBUNDOY",
            "value":"749",
            "text":"COLOMBIA, PUTUMAYO, SIBUNDOY",
            "code":"CO-86-749",
            "geoPoint":{
               "lat":1.2017900000000736,
               "lon":-76.91687999999994
            },
            "geoRectangle":{
               "Xmin":-76.98387999999993,
               "Xmax":-76.84987999999994,
               "Ymin":1.1347900000000737,
               "Ymax":1.2687900000000736
            }
         },
         {
            "name":"SAN FRANCISCO",
            "value":"755",
            "text":"COLOMBIA, PUTUMAYO, SAN FRANCISCO",
            "code":"CO-86-755",
            "geoPoint":{
               "lat":1.1750600000000304,
               "lon":-76.87829999999997
            },
            "geoRectangle":{
               "Xmin":-76.98729999999996,
               "Xmax":-76.76929999999997,
               "Ymin":1.0660600000000304,
               "Ymax":1.2840600000000304
            }
         },
         {
            "name":"SAN MIGUEL (La Dorada)",
            "value":"757",
            "text":"COLOMBIA, PUTUMAYO, SAN MIGUEL (La Dorada)",
            "code":"CO-86-757",
            "geoPoint":{
               "lat":-12.074239965097156,
               "lon":-77.0803149077484
            },
            "geoRectangle":{
               "Xmin":-77.0813149077484,
               "Xmax":-77.0793149077484,
               "Ymin":-12.075239965097156,
               "Ymax":-12.073239965097157
            }
         },
         {
            "name":"PUERTO CAICEDO",
            "value":"569",
            "text":"COLOMBIA, PUTUMAYO, PUERTO CAICEDO",
            "code":"CO-86-569",
            "geoPoint":{
               "lat":0.6862900000000423,
               "lon":-76.60445999999996
            },
            "geoRectangle":{
               "Xmin":-76.78245999999996,
               "Xmax":-76.42645999999996,
               "Ymin":0.5082900000000423,
               "Ymax":0.8642900000000422
            }
         }
      ]
   },
   {
      "name":"QUINDIO",
      "value":"63",
      "text":"COLOMBIA, QUINDIO",
      "code":"CO-63",
      "geoPoint":{
         "lat":4.455735037000068,
         "lon":-75.69010064299994
      },
      "geoRectangle":{
         "Xmin":-75.98010064299994,
         "Xmax":-75.40010064299993,
         "Ymin":4.165735037000068,
         "Ymax":4.745735037000068
      },
      "municipality":[
         {
            "name":"ARMENIA",
            "value":"001",
            "text":"COLOMBIA, QUINDIO, ARMENIA",
            "code":"CO-63-001",
            "geoPoint":{
               "lat":4.530200000000036,
               "lon":-75.68832999999995
            },
            "geoRectangle":{
               "Xmin":-75.77832999999995,
               "Xmax":-75.59832999999995,
               "Ymin":4.440200000000036,
               "Ymax":4.620200000000036
            }
         },
         {
            "name":"BUENAVISTA",
            "value":"111",
            "text":"COLOMBIA, QUINDIO, BUENAVISTA",
            "code":"CO-63-111",
            "geoPoint":{
               "lat":4.359770000000026,
               "lon":-75.73883999999998
            },
            "geoRectangle":{
               "Xmin":-75.77383999999998,
               "Xmax":-75.70383999999999,
               "Ymin":4.324770000000026,
               "Ymax":4.394770000000026
            }
         },
         {
            "name":"CALARCA",
            "value":"130",
            "text":"COLOMBIA, QUINDIO, CALARCA",
            "code":"CO-63-130",
            "geoPoint":{
               "lat":4.5302200000000425,
               "lon":-75.64106999999996
            },
            "geoRectangle":{
               "Xmin":-75.76206999999995,
               "Xmax":-75.52006999999996,
               "Ymin":4.409220000000042,
               "Ymax":4.651220000000043
            }
         },
         {
            "name":"CIRCASIA",
            "value":"190",
            "text":"COLOMBIA, QUINDIO, CIRCASIA",
            "code":"CO-63-190",
            "geoPoint":{
               "lat":4.620460000000037,
               "lon":-75.63322999999997
            },
            "geoRectangle":{
               "Xmin":-75.70122999999997,
               "Xmax":-75.56522999999997,
               "Ymin":4.552460000000037,
               "Ymax":4.6884600000000365
            }
         },
         {
            "name":"CÓRDOBA",
            "value":"212",
            "text":"COLOMBIA, QUINDIO, CÓRDOBA",
            "code":"CO-63-212",
            "geoPoint":{
               "lat":4.390170000000069,
               "lon":-75.68599999999998
            },
            "geoRectangle":{
               "Xmin":-75.74299999999998,
               "Xmax":-75.62899999999998,
               "Ymin":4.333170000000068,
               "Ymax":4.447170000000069
            }
         },
         {
            "name":"FILANDIA",
            "value":"272",
            "text":"COLOMBIA, QUINDIO, FILANDIA",
            "code":"CO-63-272",
            "geoPoint":{
               "lat":4.674650000000042,
               "lon":-75.65887999999995
            },
            "geoRectangle":{
               "Xmin":-75.73187999999995,
               "Xmax":-75.58587999999996,
               "Ymin":4.601650000000042,
               "Ymax":4.747650000000043
            }
         },
         {
            "name":"GÉNOVA",
            "value":"302",
            "text":"COLOMBIA, QUINDIO, GÉNOVA",
            "code":"CO-63-302",
            "geoPoint":{
               "lat":4.206030000000055,
               "lon":-75.79063999999994
            },
            "geoRectangle":{
               "Xmin":-75.89563999999994,
               "Xmax":-75.68563999999994,
               "Ymin":4.101030000000055,
               "Ymax":4.311030000000056
            }
         },
         {
            "name":"LA TEBAIDA",
            "value":"401",
            "text":"COLOMBIA, QUINDIO, LA TEBAIDA",
            "code":"CO-63-401",
            "geoPoint":{
               "lat":4.453010000000063,
               "lon":-75.78883999999994
            },
            "geoRectangle":{
               "Xmin":-75.85083999999993,
               "Xmax":-75.72683999999994,
               "Ymin":4.391010000000063,
               "Ymax":4.515010000000063
            }
         },
         {
            "name":"MONTENEGRO",
            "value":"470",
            "text":"COLOMBIA, QUINDIO, MONTENEGRO",
            "code":"CO-63-470",
            "geoPoint":{
               "lat":4.566410000000076,
               "lon":-75.75060999999994
            },
            "geoRectangle":{
               "Xmin":-75.82460999999994,
               "Xmax":-75.67660999999994,
               "Ymin":4.492410000000076,
               "Ymax":4.640410000000076
            }
         },
         {
            "name":"PIJAO",
            "value":"548",
            "text":"COLOMBIA, QUINDIO, PIJAO",
            "code":"CO-63-548",
            "geoPoint":{
               "lat":4.346390000000042,
               "lon":-75.70076999999998
            },
            "geoRectangle":{
               "Xmin":-75.79276999999998,
               "Xmax":-75.60876999999998,
               "Ymin":4.2543900000000425,
               "Ymax":4.438390000000042
            }
         },
         {
            "name":"QUIMBAYA",
            "value":"594",
            "text":"COLOMBIA, QUINDIO, QUIMBAYA",
            "code":"CO-63-594",
            "geoPoint":{
               "lat":4.623490000000061,
               "lon":-75.76364999999998
            },
            "geoRectangle":{
               "Xmin":-75.83264999999999,
               "Xmax":-75.69464999999998,
               "Ymin":4.554490000000061,
               "Ymax":4.692490000000061
            }
         },
         {
            "name":"SALENTO",
            "value":"690",
            "text":"COLOMBIA, QUINDIO, SALENTO",
            "code":"CO-63-690",
            "geoPoint":{
               "lat":4.637410000000045,
               "lon":-75.56950999999998
            },
            "geoRectangle":{
               "Xmin":-75.69450999999998,
               "Xmax":-75.44450999999998,
               "Ymin":4.512410000000045,
               "Ymax":4.762410000000045
            }
         }
      ]
   },
   {
      "name":"RISARALDA",
      "value":"66",
      "text":"COLOMBIA, RISARALDA",
      "code":"CO-66",
      "geoPoint":{
         "lat":5.267686775000072,
         "lon":-76.08783249999993
      },
      "geoRectangle":{
         "Xmin":-76.55783249999993,
         "Xmax":-75.61783249999993,
         "Ymin":4.797686775000073,
         "Ymax":5.737686775000072
      },
      "municipality":[
         {
            "name":"MISTRATÓ",
            "value":"456",
            "text":"COLOMBIA, RISARALDA, MISTRATÓ",
            "code":"CO-66-456",
            "geoPoint":{
               "lat":5.297090000000026,
               "lon":-75.88290999999998
            },
            "geoRectangle":{
               "Xmin":-76.03490999999998,
               "Xmax":-75.73090999999998,
               "Ymin":5.1450900000000255,
               "Ymax":5.449090000000026
            }
         },
         {
            "name":"PUEBLO RICO",
            "value":"572",
            "text":"COLOMBIA, RISARALDA, PUEBLO RICO",
            "code":"CO-66-572",
            "geoPoint":{
               "lat":5.222680000000025,
               "lon":-76.02982999999995
            },
            "geoRectangle":{
               "Xmin":-76.21082999999994,
               "Xmax":-75.84882999999995,
               "Ymin":5.041680000000025,
               "Ymax":5.403680000000025
            }
         },
         {
            "name":"DOSQUEBRADAS",
            "value":"170",
            "text":"COLOMBIA, RISARALDA, DOSQUEBRADAS",
            "code":"CO-66-170",
            "geoPoint":{
               "lat":4.8322200000000635,
               "lon":-75.67666999999994
            },
            "geoRectangle":{
               "Xmin":-75.72466999999995,
               "Xmax":-75.62866999999994,
               "Ymin":4.784220000000063,
               "Ymax":4.8802200000000635
            }
         },
         {
            "name":"GUÁTICA",
            "value":"318",
            "text":"COLOMBIA, RISARALDA, GUÁTICA",
            "code":"CO-66-318",
            "geoPoint":{
               "lat":5.315540000000055,
               "lon":-75.79809999999998
            },
            "geoRectangle":{
               "Xmin":-75.85609999999998,
               "Xmax":-75.74009999999997,
               "Ymin":5.257540000000056,
               "Ymax":5.373540000000055
            }
         },
         {
            "name":"LA CELIA",
            "value":"383",
            "text":"COLOMBIA, RISARALDA, LA CELIA",
            "code":"CO-66-383",
            "geoPoint":{
               "lat":5.004840000000058,
               "lon":-76.00365999999997
            },
            "geoRectangle":{
               "Xmin":-76.06765999999996,
               "Xmax":-75.93965999999998,
               "Ymin":4.940840000000058,
               "Ymax":5.068840000000058
            }
         },
         {
            "name":"PEREIRA",
            "value":"001",
            "text":"COLOMBIA, RISARALDA, PEREIRA",
            "code":"CO-66-001",
            "geoPoint":{
               "lat":4.805060000000026,
               "lon":-75.69369999999998
            },
            "geoRectangle":{
               "Xmin":-75.89369999999998,
               "Xmax":-75.49369999999998,
               "Ymin":4.605060000000026,
               "Ymax":5.005060000000026
            }
         },
         {
            "name":"APÍA",
            "value":"045",
            "text":"COLOMBIA, RISARALDA, APÍA",
            "code":"CO-66-045",
            "geoPoint":{
               "lat":5.107420000000047,
               "lon":-75.94298999999995
            },
            "geoRectangle":{
               "Xmin":-76.03398999999995,
               "Xmax":-75.85198999999996,
               "Ymin":5.016420000000047,
               "Ymax":5.198420000000048
            }
         },
         {
            "name":"BALBOA",
            "value":"075",
            "text":"COLOMBIA, RISARALDA, BALBOA",
            "code":"CO-66-075",
            "geoPoint":{
               "lat":4.950550000000021,
               "lon":-75.95835999999997
            },
            "geoRectangle":{
               "Xmin":-76.02035999999997,
               "Xmax":-75.89635999999997,
               "Ymin":4.888550000000021,
               "Ymax":5.012550000000021
            }
         },
         {
            "name":"BELÉN DE UMBRÍA",
            "value":"088",
            "text":"COLOMBIA, RISARALDA, BELÉN DE UMBRÍA",
            "code":"CO-66-088",
            "geoPoint":{
               "lat":5.202210000000036,
               "lon":-75.86728999999997
            },
            "geoRectangle":{
               "Xmin":-75.94228999999997,
               "Xmax":-75.79228999999997,
               "Ymin":5.127210000000036,
               "Ymax":5.277210000000037
            }
         },
         {
            "name":"LA VIRGINIA",
            "value":"400",
            "text":"COLOMBIA, RISARALDA, LA VIRGINIA",
            "code":"CO-66-400",
            "geoPoint":{
               "lat":4.901540000000068,
               "lon":-75.87957999999998
            },
            "geoRectangle":{
               "Xmin":-75.91457999999997,
               "Xmax":-75.84457999999998,
               "Ymin":4.866540000000068,
               "Ymax":4.936540000000068
            }
         },
         {
            "name":"MARSELLA",
            "value":"440",
            "text":"COLOMBIA, RISARALDA, MARSELLA",
            "code":"CO-66-440",
            "geoPoint":{
               "lat":4.935410000000047,
               "lon":-75.73846999999995
            },
            "geoRectangle":{
               "Xmin":-75.81546999999995,
               "Xmax":-75.66146999999995,
               "Ymin":4.858410000000047,
               "Ymax":5.012410000000047
            }
         },
         {
            "name":"QUINCHIA",
            "value":"594",
            "text":"COLOMBIA, RISARALDA, QUINCHIA",
            "code":"CO-66-594",
            "geoPoint":{
               "lat":5.340290000000039,
               "lon":-75.73015999999996
            },
            "geoRectangle":{
               "Xmin":-75.79815999999995,
               "Xmax":-75.66215999999996,
               "Ymin":5.272290000000039,
               "Ymax":5.408290000000038
            }
         },
         {
            "name":"SANTA ROSA DE CABAL",
            "value":"682",
            "text":"COLOMBIA, RISARALDA, SANTA ROSA DE CABAL",
            "code":"CO-66-682",
            "geoPoint":{
               "lat":4.87104000000005,
               "lon":-75.62394999999998
            },
            "geoRectangle":{
               "Xmin":-75.77994999999999,
               "Xmax":-75.46794999999997,
               "Ymin":4.715040000000051,
               "Ymax":5.02704000000005
            }
         },
         {
            "name":"SANTUARIO",
            "value":"687",
            "text":"COLOMBIA, RISARALDA, SANTUARIO",
            "code":"CO-66-687",
            "geoPoint":{
               "lat":5.073620000000062,
               "lon":-75.96338999999995
            },
            "geoRectangle":{
               "Xmin":-76.08238999999995,
               "Xmax":-75.84438999999995,
               "Ymin":4.954620000000062,
               "Ymax":5.192620000000062
            }
         }
      ]
   },
   {
      "name":"SANTANDER",
      "value":"68",
      "text":"COLOMBIA, SANTANDER",
      "code":"CO-68",
      "geoPoint":{
         "lat":6.693945282000072,
         "lon":-73.48431737799996
      },
      "geoRectangle":{
         "Xmin":-74.60631737799996,
         "Xmax":-72.36231737799996,
         "Ymin":5.5719452820000726,
         "Ymax":7.815945282000072
      },
      "municipality":[
         {
            "name":"BUCARAMANGA",
            "value":"001",
            "text":"COLOMBIA, SANTANDER, BUCARAMANGA",
            "code":"CO-68-001",
            "geoPoint":{
               "lat":7.11710000000005,
               "lon":-73.12926999999996
            },
            "geoRectangle":{
               "Xmin":-73.20926999999996,
               "Xmax":-73.04926999999996,
               "Ymin":7.03710000000005,
               "Ymax":7.1971000000000505
            }
         },
         {
            "name":"AGUADA",
            "value":"013",
            "text":"COLOMBIA, SANTANDER, AGUADA",
            "code":"CO-68-013",
            "geoPoint":{
               "lat":6.1623800000000415,
               "lon":-73.52237999999994
            },
            "geoRectangle":{
               "Xmin":-73.58337999999995,
               "Xmax":-73.46137999999993,
               "Ymin":6.1013800000000415,
               "Ymax":6.223380000000041
            }
         },
         {
            "name":"ALBANIA",
            "value":"020",
            "text":"COLOMBIA, SANTANDER, ALBANIA",
            "code":"CO-68-020",
            "geoPoint":{
               "lat":5.757560000000069,
               "lon":-73.91449999999998
            },
            "geoRectangle":{
               "Xmin":-74.01249999999997,
               "Xmax":-73.81649999999998,
               "Ymin":5.659560000000069,
               "Ymax":5.855560000000069
            }
         },
         {
            "name":"ARATOCA",
            "value":"051",
            "text":"COLOMBIA, SANTANDER, ARATOCA",
            "code":"CO-68-051",
            "geoPoint":{
               "lat":6.694600000000037,
               "lon":-73.01834999999994
            },
            "geoRectangle":{
               "Xmin":-73.10534999999994,
               "Xmax":-72.93134999999994,
               "Ymin":6.607600000000037,
               "Ymax":6.7816000000000365
            }
         },
         {
            "name":"BARBOSA",
            "value":"077",
            "text":"COLOMBIA, SANTANDER, BARBOSA",
            "code":"CO-68-077",
            "geoPoint":{
               "lat":5.930520000000058,
               "lon":-73.61605999999995
            },
            "geoRectangle":{
               "Xmin":-73.65705999999994,
               "Xmax":-73.57505999999995,
               "Ymin":5.889520000000058,
               "Ymax":5.971520000000059
            }
         },
         {
            "name":"CABRERA",
            "value":"121",
            "text":"COLOMBIA, SANTANDER, CABRERA",
            "code":"CO-68-121",
            "geoPoint":{
               "lat":6.5941700000000765,
               "lon":-73.24372999999997
            },
            "geoRectangle":{
               "Xmin":-73.29272999999998,
               "Xmax":-73.19472999999996,
               "Ymin":6.545170000000076,
               "Ymax":6.643170000000077
            }
         },
         {
            "name":"CALIFORNIA",
            "value":"132",
            "text":"COLOMBIA, SANTANDER, CALIFORNIA",
            "code":"CO-68-132",
            "geoPoint":{
               "lat":7.347210000000075,
               "lon":-72.94582999999994
            },
            "geoRectangle":{
               "Xmin":-73.00182999999994,
               "Xmax":-72.88982999999995,
               "Ymin":7.291210000000075,
               "Ymax":7.403210000000075
            }
         },
         {
            "name":"CAPITANEJO",
            "value":"147",
            "text":"COLOMBIA, SANTANDER, CAPITANEJO",
            "code":"CO-68-147",
            "geoPoint":{
               "lat":6.532090000000039,
               "lon":-72.69875999999994
            },
            "geoRectangle":{
               "Xmin":-72.76775999999994,
               "Xmax":-72.62975999999993,
               "Ymin":6.463090000000039,
               "Ymax":6.601090000000039
            }
         },
         {
            "name":"CARCASÍ",
            "value":"152",
            "text":"COLOMBIA, SANTANDER, CARCASÍ",
            "code":"CO-68-152",
            "geoPoint":{
               "lat":6.6272200000000225,
               "lon":-72.62631999999996
            },
            "geoRectangle":{
               "Xmin":-72.72131999999996,
               "Xmax":-72.53131999999997,
               "Ymin":6.532220000000023,
               "Ymax":6.722220000000022
            }
         },
         {
            "name":"BARICHARA",
            "value":"079",
            "text":"COLOMBIA, SANTANDER, BARICHARA",
            "code":"CO-68-079",
            "geoPoint":{
               "lat":6.636210000000062,
               "lon":-73.22254999999996
            },
            "geoRectangle":{
               "Xmin":-73.29654999999995,
               "Xmax":-73.14854999999996,
               "Ymin":6.5622100000000625,
               "Ymax":6.710210000000062
            }
         },
         {
            "name":"BARRANCABERMEJA",
            "value":"081",
            "text":"COLOMBIA, SANTANDER, BARRANCABERMEJA",
            "code":"CO-68-081",
            "geoPoint":{
               "lat":7.0619100000000685,
               "lon":-73.85037999999997
            },
            "geoRectangle":{
               "Xmin":-74.10437999999998,
               "Xmax":-73.59637999999997,
               "Ymin":6.807910000000069,
               "Ymax":7.315910000000068
            }
         },
         {
            "name":"BETULIA",
            "value":"092",
            "text":"COLOMBIA, SANTANDER, BETULIA",
            "code":"CO-68-092",
            "geoPoint":{
               "lat":6.899790000000053,
               "lon":-73.28346999999997
            },
            "geoRectangle":{
               "Xmin":-73.43746999999996,
               "Xmax":-73.12946999999997,
               "Ymin":6.745790000000053,
               "Ymax":7.053790000000053
            }
         },
         {
            "name":"BOLIVAR",
            "value":"101",
            "text":"COLOMBIA, SANTANDER, BOLIVAR",
            "code":"CO-68-101",
            "geoPoint":{
               "lat":10.402400000000057,
               "lon":-75.52151999999995
            },
            "geoRectangle":{
               "Xmin":-75.52651999999995,
               "Xmax":-75.51651999999996,
               "Ymin":10.397400000000056,
               "Ymax":10.407400000000058
            }
         },
         {
            "name":"ZAPATOCA",
            "value":"895",
            "text":"COLOMBIA, SANTANDER, ZAPATOCA",
            "code":"CO-68-895",
            "geoPoint":{
               "lat":6.8139600000000655,
               "lon":-73.26923999999997
            },
            "geoRectangle":{
               "Xmin":-73.41623999999997,
               "Xmax":-73.12223999999996,
               "Ymin":6.666960000000065,
               "Ymax":6.960960000000066
            }
         },
         {
            "name":"CEPITÁ",
            "value":"160",
            "text":"COLOMBIA, SANTANDER, CEPITÁ",
            "code":"CO-68-160",
            "geoPoint":{
               "lat":6.754440000000045,
               "lon":-72.97253999999998
            },
            "geoRectangle":{
               "Xmin":-73.03453999999998,
               "Xmax":-72.91053999999998,
               "Ymin":6.692440000000045,
               "Ymax":6.816440000000045
            }
         },
         {
            "name":"CERRITO",
            "value":"162",
            "text":"COLOMBIA, SANTANDER, CERRITO",
            "code":"CO-68-162",
            "geoPoint":{
               "lat":6.843630000000076,
               "lon":-72.69452999999999
            },
            "geoRectangle":{
               "Xmin":-72.80252999999999,
               "Xmax":-72.58652999999998,
               "Ymin":6.735630000000076,
               "Ymax":6.951630000000075
            }
         },
         {
            "name":"CHARALA",
            "value":"167",
            "text":"COLOMBIA, SANTANDER, CHARALA",
            "code":"CO-68-167",
            "geoPoint":{
               "lat":6.188415042678088,
               "lon":-73.18100493549416
            },
            "geoRectangle":{
               "Xmin":-73.18200493549416,
               "Xmax":-73.18000493549415,
               "Ymin":6.187415042678087,
               "Ymax":6.189415042678088
            }
         },
         {
            "name":"CHARTA",
            "value":"169",
            "text":"COLOMBIA, SANTANDER, CHARTA",
            "code":"CO-68-169",
            "geoPoint":{
               "lat":7.281090000000063,
               "lon":-72.96735999999999
            },
            "geoRectangle":{
               "Xmin":-73.04935999999998,
               "Xmax":-72.88535999999999,
               "Ymin":7.199090000000063,
               "Ymax":7.363090000000063
            }
         },
         {
            "name":"CHIMA",
            "value":"176",
            "text":"COLOMBIA, SANTANDER, CHIMA",
            "code":"CO-68-176",
            "geoPoint":{
               "lat":6.344290000000058,
               "lon":-73.37358999999998
            },
            "geoRectangle":{
               "Xmin":-73.45458999999998,
               "Xmax":-73.29258999999998,
               "Ymin":6.263290000000057,
               "Ymax":6.425290000000058
            }
         },
         {
            "name":"CHIPATÁ",
            "value":"179",
            "text":"COLOMBIA, SANTANDER, CHIPATÁ",
            "code":"CO-68-179",
            "geoPoint":{
               "lat":6.062330000000031,
               "lon":-73.63733999999994
            },
            "geoRectangle":{
               "Xmin":-73.69533999999994,
               "Xmax":-73.57933999999993,
               "Ymin":6.0043300000000315,
               "Ymax":6.120330000000031
            }
         },
         {
            "name":"CIMITARRA",
            "value":"190",
            "text":"COLOMBIA, SANTANDER, CIMITARRA",
            "code":"CO-68-190",
            "geoPoint":{
               "lat":6.313850000000059,
               "lon":-73.95016999999996
            },
            "geoRectangle":{
               "Xmin":-74.30616999999995,
               "Xmax":-73.59416999999996,
               "Ymin":5.957850000000059,
               "Ymax":6.669850000000059
            }
         },
         {
            "name":"CONCEPCIÓN",
            "value":"207",
            "text":"COLOMBIA, SANTANDER, CONCEPCIÓN",
            "code":"CO-68-207",
            "geoPoint":{
               "lat":6.769200000000069,
               "lon":-72.69478999999995
            },
            "geoRectangle":{
               "Xmin":-72.81878999999995,
               "Xmax":-72.57078999999996,
               "Ymin":6.645200000000069,
               "Ymax":6.893200000000069
            }
         },
         {
            "name":"CONFINES",
            "value":"209",
            "text":"COLOMBIA, SANTANDER, CONFINES",
            "code":"CO-68-209",
            "geoPoint":{
               "lat":6.355920000000026,
               "lon":-73.24036999999998
            },
            "geoRectangle":{
               "Xmin":-73.29536999999999,
               "Xmax":-73.18536999999998,
               "Ymin":6.300920000000026,
               "Ymax":6.410920000000026
            }
         },
         {
            "name":"RIONEGRO",
            "value":"615",
            "text":"COLOMBIA, SANTANDER, RIONEGRO",
            "code":"CO-68-615",
            "geoPoint":{
               "lat":7.264670000000024,
               "lon":-73.14787999999999
            },
            "geoRectangle":{
               "Xmin":-73.45487999999999,
               "Xmax":-72.84087999999998,
               "Ymin":6.957670000000023,
               "Ymax":7.571670000000024
            }
         },
         {
            "name":"SABANA DE TORRES",
            "value":"655",
            "text":"COLOMBIA, SANTANDER, SABANA DE TORRES",
            "code":"CO-68-655",
            "geoPoint":{
               "lat":7.391390000000058,
               "lon":-73.49421999999998
            },
            "geoRectangle":{
               "Xmin":-73.73721999999998,
               "Xmax":-73.25121999999999,
               "Ymin":7.148390000000058,
               "Ymax":7.634390000000058
            }
         },
         {
            "name":" SAN ANDRES",
            "value":"669",
            "text":"COLOMBIA, SANTANDER,  SAN ANDRES",
            "code":"CO-68-669",
            "geoPoint":{
               "lat":6.029790000000048,
               "lon":-73.36896999999993
            },
            "geoRectangle":{
               "Xmin":-73.37896999999994,
               "Xmax":-73.35896999999993,
               "Ymin":6.019790000000048,
               "Ymax":6.039790000000048
            }
         },
         {
            "name":"SAN BENITO",
            "value":"673",
            "text":"COLOMBIA, SANTANDER, SAN BENITO",
            "code":"CO-68-673",
            "geoPoint":{
               "lat":6.1253900000000385,
               "lon":-73.50942999999995
            },
            "geoRectangle":{
               "Xmin":-73.56642999999995,
               "Xmax":-73.45242999999995,
               "Ymin":6.068390000000038,
               "Ymax":6.182390000000039
            }
         },
         {
            "name":"SAN GIL",
            "value":"679",
            "text":"COLOMBIA, SANTANDER, SAN GIL",
            "code":"CO-68-679",
            "geoPoint":{
               "lat":6.552340000000072,
               "lon":-73.13149999999996
            },
            "geoRectangle":{
               "Xmin":-73.22049999999996,
               "Xmax":-73.04249999999996,
               "Ymin":6.463340000000072,
               "Ymax":6.641340000000072
            }
         },
         {
            "name":"SAN JOAQUÍN",
            "value":"682",
            "text":"COLOMBIA, SANTANDER, SAN JOAQUÍN",
            "code":"CO-68-682",
            "geoPoint":{
               "lat":6.427450000000022,
               "lon":-72.86796999999996
            },
            "geoRectangle":{
               "Xmin":-72.94396999999995,
               "Xmax":-72.79196999999996,
               "Ymin":6.351450000000022,
               "Ymax":6.503450000000021
            }
         },
         {
            "name":"SAN JOSÉ DE MIRANDA",
            "value":"684",
            "text":"COLOMBIA, SANTANDER, SAN JOSÉ DE MIRANDA",
            "code":"CO-68-684",
            "geoPoint":{
               "lat":6.658780000000036,
               "lon":-72.73352999999997
            },
            "geoRectangle":{
               "Xmin":-72.78352999999997,
               "Xmax":-72.68352999999998,
               "Ymin":6.608780000000036,
               "Ymax":6.7087800000000355
            }
         },
         {
            "name":"SAN MIGUEL",
            "value":"686",
            "text":"COLOMBIA, SANTANDER, SAN MIGUEL",
            "code":"CO-68-686",
            "geoPoint":{
               "lat":6.575860000000034,
               "lon":-72.64557999999994
            },
            "geoRectangle":{
               "Xmin":-72.69957999999994,
               "Xmax":-72.59157999999994,
               "Ymin":6.521860000000034,
               "Ymax":6.6298600000000345
            }
         },
         {
            "name":"SAN VICENTE DE CHUCURI",
            "value":"689",
            "text":"COLOMBIA, SANTANDER, SAN VICENTE DE CHUCURI",
            "code":"CO-68-689",
            "geoPoint":{
               "lat":6.87996000000004,
               "lon":-73.41006999999996
            },
            "geoRectangle":{
               "Xmin":-73.63006999999996,
               "Xmax":-73.19006999999996,
               "Ymin":6.65996000000004,
               "Ymax":7.099960000000039
            }
         },
         {
            "name":"SANTA BARBARA",
            "value":"705",
            "text":"COLOMBIA, SANTANDER, SANTA BARBARA",
            "code":"CO-68-705",
            "geoPoint":{
               "lat":6.3155700000000365,
               "lon":-73.12815999999998
            },
            "geoRectangle":{
               "Xmin":-73.13815999999998,
               "Xmax":-73.11815999999997,
               "Ymin":6.305570000000037,
               "Ymax":6.325570000000036
            }
         },
         {
            "name":"SANTA HELENA DEL OPÓN",
            "value":"720",
            "text":"COLOMBIA, SANTANDER, SANTA HELENA DEL OPÓN",
            "code":"CO-68-720",
            "geoPoint":{
               "lat":6.339270000000056,
               "lon":-73.61635999999999
            },
            "geoRectangle":{
               "Xmin":-73.72935999999999,
               "Xmax":-73.50335999999999,
               "Ymin":6.2262700000000555,
               "Ymax":6.452270000000056
            }
         },
         {
            "name":"SIMACOTA",
            "value":"745",
            "text":"COLOMBIA, SANTANDER, SIMACOTA",
            "code":"CO-68-745",
            "geoPoint":{
               "lat":6.4426200000000335,
               "lon":-73.33694999999994
            },
            "geoRectangle":{
               "Xmin":-73.63194999999995,
               "Xmax":-73.04194999999994,
               "Ymin":6.147620000000034,
               "Ymax":6.7376200000000335
            }
         },
         {
            "name":"SOCORRO",
            "value":"755",
            "text":"COLOMBIA, SANTANDER, SOCORRO",
            "code":"CO-68-755",
            "geoPoint":{
               "lat":6.469220000000064,
               "lon":-73.26063999999997
            },
            "geoRectangle":{
               "Xmin":-73.32563999999996,
               "Xmax":-73.19563999999997,
               "Ymin":6.4042200000000635,
               "Ymax":6.534220000000064
            }
         },
         {
            "name":"SUAITA",
            "value":"770",
            "text":"COLOMBIA, SANTANDER, SUAITA",
            "code":"CO-68-770",
            "geoPoint":{
               "lat":6.10352000000006,
               "lon":-73.44129999999996
            },
            "geoRectangle":{
               "Xmin":-73.54829999999995,
               "Xmax":-73.33429999999996,
               "Ymin":5.99652000000006,
               "Ymax":6.21052000000006
            }
         },
         {
            "name":"SUCRE",
            "value":"773",
            "text":"COLOMBIA, SANTANDER, SUCRE",
            "code":"CO-68-773",
            "geoPoint":{
               "lat":5.917940000000044,
               "lon":-73.79173999999995
            },
            "geoRectangle":{
               "Xmin":-73.96473999999995,
               "Xmax":-73.61873999999995,
               "Ymin":5.744940000000044,
               "Ymax":6.090940000000044
            }
         },
         {
            "name":"SURATÁ",
            "value":"780",
            "text":"COLOMBIA, SANTANDER, SURATÁ",
            "code":"CO-68-780",
            "geoPoint":{
               "lat":7.366390000000024,
               "lon":-72.98336999999998
            },
            "geoRectangle":{
               "Xmin":-73.12536999999998,
               "Xmax":-72.84136999999998,
               "Ymin":7.224390000000024,
               "Ymax":7.508390000000024
            }
         },
         {
            "name":"TONA",
            "value":"820",
            "text":"COLOMBIA, SANTANDER, TONA",
            "code":"CO-68-820",
            "geoPoint":{
               "lat":7.202530000000024,
               "lon":-72.96589999999998
            },
            "geoRectangle":{
               "Xmin":-73.07189999999997,
               "Xmax":-72.85989999999998,
               "Ymin":7.096530000000024,
               "Ymax":7.308530000000024
            }
         },
         {
            "name":"VALLE DE SAN JOSE",
            "value":"855",
            "text":"COLOMBIA, SANTANDER, VALLE DE SAN JOSE",
            "code":"CO-68-855",
            "geoPoint":{
               "lat":6.44798000000003,
               "lon":-73.14352999999994
            },
            "geoRectangle":{
               "Xmin":-73.19152999999994,
               "Xmax":-73.09552999999994,
               "Ymin":6.3999800000000295,
               "Ymax":6.49598000000003
            }
         },
         {
            "name":"VÉLEZ",
            "value":"861",
            "text":"COLOMBIA, SANTANDER, VÉLEZ",
            "code":"CO-68-861",
            "geoPoint":{
               "lat":6.012270000000058,
               "lon":-73.67350999999996
            },
            "geoRectangle":{
               "Xmin":-73.87950999999997,
               "Xmax":-73.46750999999996,
               "Ymin":5.806270000000057,
               "Ymax":6.218270000000058
            }
         },
         {
            "name":"VETAS",
            "value":"867",
            "text":"COLOMBIA, SANTANDER, VETAS",
            "code":"CO-68-867",
            "geoPoint":{
               "lat":7.309950000000072,
               "lon":-72.87088999999997
            },
            "geoRectangle":{
               "Xmin":-72.92688999999997,
               "Xmax":-72.81488999999998,
               "Ymin":7.253950000000072,
               "Ymax":7.365950000000072
            }
         },
         {
            "name":"VILLANUEVA",
            "value":"872",
            "text":"COLOMBIA, SANTANDER, VILLANUEVA",
            "code":"CO-68-872",
            "geoPoint":{
               "lat":6.671230000000037,
               "lon":-73.17459999999994
            },
            "geoRectangle":{
               "Xmin":-73.24459999999993,
               "Xmax":-73.10459999999995,
               "Ymin":6.601230000000037,
               "Ymax":6.741230000000037
            }
         },
         {
            "name":"JORDÁN SUBE",
            "value":"370",
            "text":"COLOMBIA, SANTANDER, JORDÁN SUBE",
            "code":"CO-68-370",
            "geoPoint":{
               "lat":6.731750000000034,
               "lon":-73.09689999999995
            },
            "geoRectangle":{
               "Xmin":-73.13889999999995,
               "Xmax":-73.05489999999995,
               "Ymin":6.689750000000034,
               "Ymax":6.7737500000000335
            }
         },
         {
            "name":"LA BELLEZA",
            "value":"377",
            "text":"COLOMBIA, SANTANDER, LA BELLEZA",
            "code":"CO-68-377",
            "geoPoint":{
               "lat":5.8587400000000684,
               "lon":-73.96599999999995
            },
            "geoRectangle":{
               "Xmin":-74.08899999999996,
               "Xmax":-73.84299999999995,
               "Ymin":5.735740000000068,
               "Ymax":5.981740000000069
            }
         },
         {
            "name":"LANDÁZURI",
            "value":"385",
            "text":"COLOMBIA, SANTANDER, LANDÁZURI",
            "code":"CO-68-385",
            "geoPoint":{
               "lat":6.218660000000057,
               "lon":-73.81122999999997
            },
            "geoRectangle":{
               "Xmin":-74.01422999999997,
               "Xmax":-73.60822999999996,
               "Ymin":6.015660000000056,
               "Ymax":6.421660000000057
            }
         },
         {
            "name":"LA PAZ",
            "value":"397",
            "text":"COLOMBIA, SANTANDER, LA PAZ",
            "code":"CO-68-397",
            "geoPoint":{
               "lat":6.178930000000037,
               "lon":-73.59028999999998
            },
            "geoRectangle":{
               "Xmin":-73.70928999999998,
               "Xmax":-73.47128999999998,
               "Ymin":6.059930000000037,
               "Ymax":6.297930000000036
            }
         },
         {
            "name":"LEBRÍJA",
            "value":"406",
            "text":"COLOMBIA, SANTANDER, LEBRÍJA",
            "code":"CO-68-406",
            "geoPoint":{
               "lat":7.113630000000057,
               "lon":-73.21819999999997
            },
            "geoRectangle":{
               "Xmin":-73.38619999999997,
               "Xmax":-73.05019999999996,
               "Ymin":6.945630000000057,
               "Ymax":7.281630000000058
            }
         },
         {
            "name":"LOS SANTOS",
            "value":"418",
            "text":"COLOMBIA, SANTANDER, LOS SANTOS",
            "code":"CO-68-418",
            "geoPoint":{
               "lat":6.7561300000000415,
               "lon":-73.10388999999998
            },
            "geoRectangle":{
               "Xmin":-73.20088999999997,
               "Xmax":-73.00688999999998,
               "Ymin":6.659130000000041,
               "Ymax":6.853130000000042
            }
         },
         {
            "name":"MACARAVITA",
            "value":"425",
            "text":"COLOMBIA, SANTANDER, MACARAVITA",
            "code":"CO-68-425",
            "geoPoint":{
               "lat":6.506230000000073,
               "lon":-72.59235999999999
            },
            "geoRectangle":{
               "Xmin":-72.65935999999998,
               "Xmax":-72.52535999999999,
               "Ymin":6.439230000000073,
               "Ymax":6.573230000000073
            }
         },
         {
            "name":"MÁLAGA",
            "value":"432",
            "text":"COLOMBIA, SANTANDER, MÁLAGA",
            "code":"CO-68-432",
            "geoPoint":{
               "lat":6.698080000000061,
               "lon":-72.73300999999998
            },
            "geoRectangle":{
               "Xmin":-72.77700999999998,
               "Xmax":-72.68900999999998,
               "Ymin":6.654080000000062,
               "Ymax":6.742080000000061
            }
         },
         {
            "name":"MATANZA",
            "value":"444",
            "text":"COLOMBIA, SANTANDER, MATANZA",
            "code":"CO-68-444",
            "geoPoint":{
               "lat":7.3235200000000304,
               "lon":-73.01580999999999
            },
            "geoRectangle":{
               "Xmin":-73.11680999999999,
               "Xmax":-72.91480999999999,
               "Ymin":7.2225200000000305,
               "Ymax":7.42452000000003
            }
         },
         {
            "name":"MOGOTES",
            "value":"464",
            "text":"COLOMBIA, SANTANDER, MOGOTES",
            "code":"CO-68-464",
            "geoPoint":{
               "lat":6.476640000000032,
               "lon":-72.97082999999998
            },
            "geoRectangle":{
               "Xmin":-73.10682999999997,
               "Xmax":-72.83482999999998,
               "Ymin":6.340640000000032,
               "Ymax":6.612640000000032
            }
         },
         {
            "name":"MOLAGAVITA",
            "value":"468",
            "text":"COLOMBIA, SANTANDER, MOLAGAVITA",
            "code":"CO-68-468",
            "geoPoint":{
               "lat":6.673410000000047,
               "lon":-72.80945999999994
            },
            "geoRectangle":{
               "Xmin":-72.89245999999994,
               "Xmax":-72.72645999999995,
               "Ymin":6.5904100000000465,
               "Ymax":6.756410000000047
            }
         },
         {
            "name":"OCAMONTE",
            "value":"498",
            "text":"COLOMBIA, SANTANDER, OCAMONTE",
            "code":"CO-68-498",
            "geoPoint":{
               "lat":6.340450000000033,
               "lon":-73.12213999999994
            },
            "geoRectangle":{
               "Xmin":-73.17413999999995,
               "Xmax":-73.07013999999994,
               "Ymin":6.288450000000033,
               "Ymax":6.392450000000032
            }
         },
         {
            "name":"OIBA",
            "value":"500",
            "text":"COLOMBIA, SANTANDER, OIBA",
            "code":"CO-68-500",
            "geoPoint":{
               "lat":6.264410000000055,
               "lon":-73.30092999999994
            },
            "geoRectangle":{
               "Xmin":-73.39592999999994,
               "Xmax":-73.20592999999994,
               "Ymin":6.169410000000055,
               "Ymax":6.359410000000055
            }
         },
         {
            "name":"ONZAGA",
            "value":"502",
            "text":"COLOMBIA, SANTANDER, ONZAGA",
            "code":"CO-68-502",
            "geoPoint":{
               "lat":6.344100000000026,
               "lon":-72.81699999999995
            },
            "geoRectangle":{
               "Xmin":-72.96799999999995,
               "Xmax":-72.66599999999995,
               "Ymin":6.193100000000026,
               "Ymax":6.495100000000026
            }
         },
         {
            "name":"PALMAR",
            "value":"522",
            "text":"COLOMBIA, SANTANDER, PALMAR",
            "code":"CO-68-522",
            "geoPoint":{
               "lat":6.538110000000074,
               "lon":-73.29070999999993
            },
            "geoRectangle":{
               "Xmin":-73.32270999999993,
               "Xmax":-73.25870999999994,
               "Ymin":6.506110000000074,
               "Ymax":6.570110000000074
            }
         },
         {
            "name":"PALMAS DEL SOCORRO",
            "value":"524",
            "text":"COLOMBIA, SANTANDER, PALMAS DEL SOCORRO",
            "code":"CO-68-524",
            "geoPoint":{
               "lat":6.406360000000063,
               "lon":-73.28803999999997
            },
            "geoRectangle":{
               "Xmin":-73.33303999999997,
               "Xmax":-73.24303999999997,
               "Ymin":6.361360000000063,
               "Ymax":6.451360000000063
            }
         },
         {
            "name":"PÁRAMO",
            "value":"533",
            "text":"COLOMBIA, SANTANDER, PÁRAMO",
            "code":"CO-68-533",
            "geoPoint":{
               "lat":6.416000000000054,
               "lon":-73.17059999999998
            },
            "geoRectangle":{
               "Xmin":-73.23659999999998,
               "Xmax":-73.10459999999998,
               "Ymin":6.350000000000054,
               "Ymax":6.4820000000000535
            }
         },
         {
            "name":"PIEDECUESTA",
            "value":"547",
            "text":"COLOMBIA, SANTANDER, PIEDECUESTA",
            "code":"CO-68-547",
            "geoPoint":{
               "lat":6.98905000000002,
               "lon":-73.04360999999994
            },
            "geoRectangle":{
               "Xmin":-73.17760999999994,
               "Xmax":-72.90960999999994,
               "Ymin":6.85505000000002,
               "Ymax":7.1230500000000205
            }
         },
         {
            "name":"PINCHOTE",
            "value":"549",
            "text":"COLOMBIA, SANTANDER, PINCHOTE",
            "code":"CO-68-549",
            "geoPoint":{
               "lat":6.531950000000052,
               "lon":-73.17265999999995
            },
            "geoRectangle":{
               "Xmin":-73.21765999999995,
               "Xmax":-73.12765999999995,
               "Ymin":6.486950000000052,
               "Ymax":6.576950000000052
            }
         },
         {
            "name":"PUENTE NACIONAL",
            "value":"572",
            "text":"COLOMBIA, SANTANDER, PUENTE NACIONAL",
            "code":"CO-68-572",
            "geoPoint":{
               "lat":5.877320000000054,
               "lon":-73.67955999999998
            },
            "geoRectangle":{
               "Xmin":-73.77755999999998,
               "Xmax":-73.58155999999998,
               "Ymin":5.779320000000054,
               "Ymax":5.975320000000054
            }
         },
         {
            "name":"PUERTO PARRA",
            "value":"573",
            "text":"COLOMBIA, SANTANDER, PUERTO PARRA",
            "code":"CO-68-573",
            "geoPoint":{
               "lat":6.65062000000006,
               "lon":-74.05660999999998
            },
            "geoRectangle":{
               "Xmin":-74.24160999999998,
               "Xmax":-73.87160999999998,
               "Ymin":6.465620000000061,
               "Ymax":6.83562000000006
            }
         },
         {
            "name":"PUERTO WILCHES",
            "value":"575",
            "text":"COLOMBIA, SANTANDER, PUERTO WILCHES",
            "code":"CO-68-575",
            "geoPoint":{
               "lat":7.348840000000052,
               "lon":-73.89662999999996
            },
            "geoRectangle":{
               "Xmin":-74.22762999999996,
               "Xmax":-73.56562999999996,
               "Ymin":7.017840000000052,
               "Ymax":7.679840000000053
            }
         },
         {
            "name":"CONTRATACIÓN",
            "value":"211",
            "text":"COLOMBIA, SANTANDER, CONTRATACIÓN",
            "code":"CO-68-211",
            "geoPoint":{
               "lat":6.246420000000057,
               "lon":-73.49574999999999
            },
            "geoRectangle":{
               "Xmin":-73.57674999999999,
               "Xmax":-73.41474999999998,
               "Ymin":6.165420000000057,
               "Ymax":6.327420000000058
            }
         },
         {
            "name":"COROMORO",
            "value":"217",
            "text":"COLOMBIA, SANTANDER, COROMORO",
            "code":"CO-68-217",
            "geoPoint":{
               "lat":6.2948200000000725,
               "lon":-73.04055999999997
            },
            "geoRectangle":{
               "Xmin":-73.18055999999997,
               "Xmax":-72.90055999999997,
               "Ymin":6.154820000000073,
               "Ymax":6.434820000000072
            }
         },
         {
            "name":"CURITÍ",
            "value":"229",
            "text":"COLOMBIA, SANTANDER, CURITÍ",
            "code":"CO-68-229",
            "geoPoint":{
               "lat":6.606390000000033,
               "lon":-73.07044999999994
            },
            "geoRectangle":{
               "Xmin":-73.17244999999994,
               "Xmax":-72.96844999999993,
               "Ymin":6.504390000000033,
               "Ymax":6.708390000000033
            }
         },
         {
            "name":"EL CARMEN DE CHUCURI",
            "value":"235",
            "text":"COLOMBIA, SANTANDER, EL CARMEN DE CHUCURI",
            "code":"CO-68-235",
            "geoPoint":{
               "lat":6.697870000000023,
               "lon":-73.51176999999996
            },
            "geoRectangle":{
               "Xmin":-73.68376999999995,
               "Xmax":-73.33976999999996,
               "Ymin":6.525870000000023,
               "Ymax":6.869870000000023
            }
         },
         {
            "name":"EL GUACAMAYO",
            "value":"245",
            "text":"COLOMBIA, SANTANDER, EL GUACAMAYO",
            "code":"CO-68-245",
            "geoPoint":{
               "lat":6.290170000000046,
               "lon":-73.47453999999993
            },
            "geoRectangle":{
               "Xmin":-73.55853999999994,
               "Xmax":-73.39053999999993,
               "Ymin":6.206170000000046,
               "Ymax":6.374170000000046
            }
         },
         {
            "name":"EL PEÑÓN",
            "value":"250",
            "text":"COLOMBIA, SANTANDER, EL PEÑÓN",
            "code":"CO-68-250",
            "geoPoint":{
               "lat":6.0664200000000505,
               "lon":-73.84946999999994
            },
            "geoRectangle":{
               "Xmin":-73.97646999999994,
               "Xmax":-73.72246999999994,
               "Ymin":5.939420000000051,
               "Ymax":6.19342000000005
            }
         },
         {
            "name":"EL PLAYÓN",
            "value":"255",
            "text":"COLOMBIA, SANTANDER, EL PLAYÓN",
            "code":"CO-68-255",
            "geoPoint":{
               "lat":7.47082000000006,
               "lon":-73.20333999999997
            },
            "geoRectangle":{
               "Xmin":-73.32833999999997,
               "Xmax":-73.07833999999997,
               "Ymin":7.34582000000006,
               "Ymax":7.59582000000006
            }
         },
         {
            "name":" ENCINO",
            "value":"264",
            "text":"COLOMBIA, SANTANDER,  ENCINO",
            "code":"CO-68-264",
            "geoPoint":{
               "lat":6.1372800000000325,
               "lon":-73.09846999999996
            },
            "geoRectangle":{
               "Xmin":-73.22246999999996,
               "Xmax":-72.97446999999997,
               "Ymin":6.013280000000033,
               "Ymax":6.261280000000032
            }
         },
         {
            "name":"ENCISO",
            "value":"266",
            "text":"COLOMBIA, SANTANDER, ENCISO",
            "code":"CO-68-266",
            "geoPoint":{
               "lat":6.669200000000046,
               "lon":-72.69944999999996
            },
            "geoRectangle":{
               "Xmin":-72.76044999999996,
               "Xmax":-72.63844999999995,
               "Ymin":6.608200000000046,
               "Ymax":6.730200000000046
            }
         },
         {
            "name":"FLORIÁN",
            "value":"271",
            "text":"COLOMBIA, SANTANDER, FLORIÁN",
            "code":"CO-68-271",
            "geoPoint":{
               "lat":5.802130000000034,
               "lon":-73.97183999999999
            },
            "geoRectangle":{
               "Xmin":-74.06883999999998,
               "Xmax":-73.87483999999999,
               "Ymin":5.705130000000033,
               "Ymax":5.899130000000034
            }
         },
         {
            "name":"FLORIDABLANCA",
            "value":"276",
            "text":"COLOMBIA, SANTANDER, FLORIDABLANCA",
            "code":"CO-68-276",
            "geoPoint":{
               "lat":7.067180000000064,
               "lon":-73.10010999999997
            },
            "geoRectangle":{
               "Xmin":-73.16610999999997,
               "Xmax":-73.03410999999997,
               "Ymin":7.001180000000065,
               "Ymax":7.133180000000064
            }
         },
         {
            "name":"GALÁN",
            "value":"296",
            "text":"COLOMBIA, SANTANDER, GALÁN",
            "code":"CO-68-296",
            "geoPoint":{
               "lat":6.638140000000021,
               "lon":-73.28664999999995
            },
            "geoRectangle":{
               "Xmin":-73.37764999999995,
               "Xmax":-73.19564999999996,
               "Ymin":6.547140000000021,
               "Ymax":6.729140000000021
            }
         },
         {
            "name":"GÁMBITA",
            "value":"298",
            "text":"COLOMBIA, SANTANDER, GÁMBITA",
            "code":"CO-68-298",
            "geoPoint":{
               "lat":6.000000000000057,
               "lon":-73.24999999999994
            },
            "geoRectangle":{
               "Xmin":-74.24999999999994,
               "Xmax":-72.24999999999994,
               "Ymin":5.000000000000057,
               "Ymax":7.000000000000057
            }
         },
         {
            "name":"GIRÓN",
            "value":"307",
            "text":"COLOMBIA, SANTANDER, GIRÓN",
            "code":"CO-68-307",
            "geoPoint":{
               "lat":7.074680000000058,
               "lon":-73.17369999999994
            },
            "geoRectangle":{
               "Xmin":-73.36869999999993,
               "Xmax":-72.97869999999995,
               "Ymin":6.879680000000057,
               "Ymax":7.269680000000058
            }
         },
         {
            "name":"GUACA",
            "value":"318",
            "text":"COLOMBIA, SANTANDER, GUACA",
            "code":"CO-68-318",
            "geoPoint":{
               "lat":6.875880000000052,
               "lon":-72.85540999999995
            },
            "geoRectangle":{
               "Xmin":-72.97640999999994,
               "Xmax":-72.73440999999995,
               "Ymin":6.7548800000000515,
               "Ymax":6.996880000000052
            }
         },
         {
            "name":"GUADALUPE",
            "value":"320",
            "text":"COLOMBIA, SANTANDER, GUADALUPE",
            "code":"CO-68-320",
            "geoPoint":{
               "lat":6.247090000000071,
               "lon":-73.41798999999997
            },
            "geoRectangle":{
               "Xmin":-73.49398999999997,
               "Xmax":-73.34198999999998,
               "Ymin":6.1710900000000715,
               "Ymax":6.323090000000071
            }
         },
         {
            "name":"GUAPOTÁ",
            "value":"322",
            "text":"COLOMBIA, SANTANDER, GUAPOTÁ",
            "code":"CO-68-322",
            "geoPoint":{
               "lat":6.307250000000067,
               "lon":-73.32122999999996
            },
            "geoRectangle":{
               "Xmin":-73.37222999999996,
               "Xmax":-73.27022999999996,
               "Ymin":6.256250000000067,
               "Ymax":6.3582500000000675
            }
         },
         {
            "name":"GUAVATÁ",
            "value":"324",
            "text":"COLOMBIA, SANTANDER, GUAVATÁ",
            "code":"CO-68-324",
            "geoPoint":{
               "lat":5.955100000000073,
               "lon":-73.70103999999998
            },
            "geoRectangle":{
               "Xmin":-73.75403999999997,
               "Xmax":-73.64803999999998,
               "Ymin":5.902100000000073,
               "Ymax":6.008100000000073
            }
         },
         {
            "name":"GÜEPSA",
            "value":"327",
            "text":"COLOMBIA, SANTANDER, GÜEPSA",
            "code":"CO-68-327",
            "geoPoint":{
               "lat":6.025810000000035,
               "lon":-73.57329999999996
            },
            "geoRectangle":{
               "Xmin":-73.61629999999997,
               "Xmax":-73.53029999999995,
               "Ymin":5.982810000000035,
               "Ymax":6.068810000000036
            }
         },
         {
            "name":"HATO",
            "value":"344",
            "text":"COLOMBIA, SANTANDER, HATO",
            "code":"CO-68-344",
            "geoPoint":{
               "lat":6.542370000000062,
               "lon":-73.30775999999997
            },
            "geoRectangle":{
               "Xmin":-73.38175999999997,
               "Xmax":-73.23375999999998,
               "Ymin":6.468370000000062,
               "Ymax":6.616370000000062
            }
         },
         {
            "name":"JESÚS MARÍA",
            "value":"368",
            "text":"COLOMBIA, SANTANDER, JESÚS MARÍA",
            "code":"CO-68-368",
            "geoPoint":{
               "lat":5.87558000000007,
               "lon":-73.78232999999994
            },
            "geoRectangle":{
               "Xmin":-73.86232999999994,
               "Xmax":-73.70232999999995,
               "Ymin":5.79558000000007,
               "Ymax":5.9555800000000705
            }
         }
      ]
   },
   {
      "name":"SUCRE",
      "value":"70",
      "text":"COLOMBIA, SUCRE",
      "code":"CO-70",
      "geoPoint":{
         "lat":8.81236000000007,
         "lon":-74.72035999999997
      },
      "geoRectangle":{
         "Xmin":-74.92835999999997,
         "Xmax":-74.51235999999997,
         "Ymin":8.604360000000069,
         "Ymax":9.02036000000007
      },
      "municipality":[
         {
            "name":"SAMPUÉS",
            "value":"670",
            "text":"COLOMBIA, SUCRE, SAMPUÉS",
            "code":"CO-70-670",
            "geoPoint":{
               "lat":9.183700000000044,
               "lon":-75.37877999999995
            },
            "geoRectangle":{
               "Xmin":-75.48577999999995,
               "Xmax":-75.27177999999995,
               "Ymin":9.076700000000045,
               "Ymax":9.290700000000044
            }
         },
         {
            "name":"SAN BENITO ABAD",
            "value":"678",
            "text":"COLOMBIA, SUCRE, SAN BENITO ABAD",
            "code":"CO-70-678",
            "geoPoint":{
               "lat":8.929880000000026,
               "lon":-75.02916999999997
            },
            "geoRectangle":{
               "Xmin":-75.30716999999997,
               "Xmax":-74.75116999999996,
               "Ymin":8.651880000000025,
               "Ymax":9.207880000000026
            }
         },
         {
            "name":"SAN JUAN DE BETULIA",
            "value":"702",
            "text":"COLOMBIA, SUCRE, SAN JUAN DE BETULIA",
            "code":"CO-70-702",
            "geoPoint":{
               "lat":9.273640000000057,
               "lon":-75.24300999999997
            },
            "geoRectangle":{
               "Xmin":-75.33500999999997,
               "Xmax":-75.15100999999997,
               "Ymin":9.181640000000057,
               "Ymax":9.365640000000058
            }
         },
         {
            "name":"SAN MARCOS",
            "value":"708",
            "text":"COLOMBIA, SUCRE, SAN MARCOS",
            "code":"CO-70-708",
            "geoPoint":{
               "lat":8.663580000000024,
               "lon":-75.13153999999997
            },
            "geoRectangle":{
               "Xmin":-75.31153999999998,
               "Xmax":-74.95153999999997,
               "Ymin":8.483580000000025,
               "Ymax":8.843580000000024
            }
         },
         {
            "name":"SAN ONOFRE",
            "value":"713",
            "text":"COLOMBIA, SUCRE, SAN ONOFRE",
            "code":"CO-70-713",
            "geoPoint":{
               "lat":9.736980000000074,
               "lon":-75.52498999999995
            },
            "geoRectangle":{
               "Xmin":-75.74998999999994,
               "Xmax":-75.29998999999995,
               "Ymin":9.511980000000074,
               "Ymax":9.961980000000073
            }
         },
         {
            "name":"SAN PEDRO",
            "value":"717",
            "text":"COLOMBIA, SUCRE, SAN PEDRO",
            "code":"CO-70-717",
            "geoPoint":{
               "lat":9.394310000000075,
               "lon":-75.06385999999998
            },
            "geoRectangle":{
               "Xmin":-75.14585999999997,
               "Xmax":-74.98185999999998,
               "Ymin":9.312310000000075,
               "Ymax":9.476310000000076
            }
         },
         {
            "name":"SAN LUIS DE SINCÉ",
            "value":"742",
            "text":"COLOMBIA, SUCRE, SAN LUIS DE SINCÉ",
            "code":"CO-70-742",
            "geoPoint":{
               "lat":9.245070000000055,
               "lon":-75.14788999999996
            },
            "geoRectangle":{
               "Xmin":-75.29788999999997,
               "Xmax":-74.99788999999996,
               "Ymin":9.095070000000055,
               "Ymax":9.395070000000056
            }
         },
         {
            "name":"SUCRE",
            "value":"771",
            "text":"COLOMBIA, SUCRE, SUCRE",
            "code":"CO-70-771",
            "geoPoint":{
               "lat":8.81236000000007,
               "lon":-74.72035999999997
            },
            "geoRectangle":{
               "Xmin":-74.92835999999997,
               "Xmax":-74.51235999999997,
               "Ymin":8.604360000000069,
               "Ymax":9.02036000000007
            }
         },
         {
            "name":"SANTIAGO DE TOLÚ",
            "value":"820",
            "text":"COLOMBIA, SUCRE, SANTIAGO DE TOLÚ",
            "code":"CO-70-820",
            "geoPoint":{
               "lat":9.52815000000004,
               "lon":-75.58040999999997
            },
            "geoRectangle":{
               "Xmin":-75.69940999999997,
               "Xmax":-75.46140999999997,
               "Ymin":9.40915000000004,
               "Ymax":9.647150000000039
            }
         },
         {
            "name":"TOLÚ VIEJO",
            "value":"823",
            "text":"COLOMBIA, SUCRE, TOLÚ VIEJO",
            "code":"CO-70-823",
            "geoPoint":{
               "lat":9.451000000000022,
               "lon":-75.44104999999996
            },
            "geoRectangle":{
               "Xmin":-75.54604999999997,
               "Xmax":-75.33604999999996,
               "Ymin":9.346000000000021,
               "Ymax":9.556000000000022
            }
         },
         {
            "name":"SINCELEJO",
            "value":"001",
            "text":"COLOMBIA, SUCRE, SINCELEJO",
            "code":"CO-70-001",
            "geoPoint":{
               "lat":9.299440000000061,
               "lon":-75.39464999999996
            },
            "geoRectangle":{
               "Xmin":-75.50364999999995,
               "Xmax":-75.28564999999996,
               "Ymin":9.190440000000061,
               "Ymax":9.408440000000061
            }
         },
         {
            "name":"BUENAVISTA",
            "value":"110",
            "text":"COLOMBIA, SUCRE, BUENAVISTA",
            "code":"CO-70-110",
            "geoPoint":{
               "lat":9.32036000000005,
               "lon":-74.97124999999994
            },
            "geoRectangle":{
               "Xmin":-75.06124999999994,
               "Xmax":-74.88124999999994,
               "Ymin":9.23036000000005,
               "Ymax":9.41036000000005
            }
         },
         {
            "name":"CAIMITO",
            "value":"124",
            "text":"COLOMBIA, SUCRE, CAIMITO",
            "code":"CO-70-124",
            "geoPoint":{
               "lat":8.790650000000028,
               "lon":-75.11687999999998
            },
            "geoRectangle":{
               "Xmin":-75.26287999999998,
               "Xmax":-74.97087999999998,
               "Ymin":8.644650000000027,
               "Ymax":8.936650000000029
            }
         },
         {
            "name":"RICAURTE (COLOSO)",
            "value":"204",
            "text":"COLOMBIA, SUCRE, RICAURTE (COLOSO)",
            "code":"CO-70-204",
            "geoPoint":{
               "lat":9.491900000000044,
               "lon":-75.35213999999996
            },
            "geoRectangle":{
               "Xmin":-75.43513999999996,
               "Xmax":-75.26913999999996,
               "Ymin":9.408900000000044,
               "Ymax":9.574900000000044
            }
         },
         {
            "name":"COROZAL",
            "value":"215",
            "text":"COLOMBIA, SUCRE, COROZAL",
            "code":"CO-70-215",
            "geoPoint":{
               "lat":9.319470000000024,
               "lon":-75.29289999999997
            },
            "geoRectangle":{
               "Xmin":-75.42589999999997,
               "Xmax":-75.15989999999998,
               "Ymin":9.186470000000025,
               "Ymax":9.452470000000023
            }
         },
         {
            "name":"COVEÑAS",
            "value":"221",
            "text":"COLOMBIA, SUCRE, COVEÑAS",
            "code":"CO-70-221",
            "geoPoint":{
               "lat":9.403880000000072,
               "lon":-75.68526999999995
            },
            "geoRectangle":{
               "Xmin":-75.74026999999995,
               "Xmax":-75.63026999999994,
               "Ymin":9.348880000000072,
               "Ymax":9.458880000000072
            }
         },
         {
            "name":"CHALÁN",
            "value":"230",
            "text":"COLOMBIA, SUCRE, CHALÁN",
            "code":"CO-70-230",
            "geoPoint":{
               "lat":9.542920000000038,
               "lon":-75.31294999999994
            },
            "geoRectangle":{
               "Xmin":-75.37794999999994,
               "Xmax":-75.24794999999995,
               "Ymin":9.477920000000038,
               "Ymax":9.607920000000037
            }
         },
         {
            "name":"EL ROBLE",
            "value":"233",
            "text":"COLOMBIA, SUCRE, EL ROBLE",
            "code":"CO-70-233",
            "geoPoint":{
               "lat":9.10328000000004,
               "lon":-75.19523999999996
            },
            "geoRectangle":{
               "Xmin":-75.30123999999995,
               "Xmax":-75.08923999999996,
               "Ymin":8.99728000000004,
               "Ymax":9.20928000000004
            }
         },
         {
            "name":"GALERAS",
            "value":"235",
            "text":"COLOMBIA, SUCRE, GALERAS",
            "code":"CO-70-235",
            "geoPoint":{
               "lat":9.160540000000026,
               "lon":-75.04832999999996
            },
            "geoRectangle":{
               "Xmin":-75.15432999999996,
               "Xmax":-74.94232999999997,
               "Ymin":9.054540000000026,
               "Ymax":9.266540000000026
            }
         },
         {
            "name":"GUARANDA",
            "value":"265",
            "text":"COLOMBIA, SUCRE, GUARANDA",
            "code":"CO-70-265",
            "geoPoint":{
               "lat":8.468900000000076,
               "lon":-74.53644999999995
            },
            "geoRectangle":{
               "Xmin":-74.67344999999995,
               "Xmax":-74.39944999999994,
               "Ymin":8.331900000000076,
               "Ymax":8.605900000000076
            }
         },
         {
            "name":"LA UNIÓN",
            "value":"400",
            "text":"COLOMBIA, SUCRE, LA UNIÓN",
            "code":"CO-70-400",
            "geoPoint":{
               "lat":8.854560000000049,
               "lon":-75.28105999999997
            },
            "geoRectangle":{
               "Xmin":-75.38405999999996,
               "Xmax":-75.17805999999997,
               "Ymin":8.75156000000005,
               "Ymax":8.957560000000049
            }
         },
         {
            "name":"LOS PALMITOS",
            "value":"418",
            "text":"COLOMBIA, SUCRE, LOS PALMITOS",
            "code":"CO-70-418",
            "geoPoint":{
               "lat":9.381560000000036,
               "lon":-75.27062999999998
            },
            "geoRectangle":{
               "Xmin":-75.36662999999999,
               "Xmax":-75.17462999999998,
               "Ymin":9.285560000000036,
               "Ymax":9.477560000000036
            }
         },
         {
            "name":"MAJAGUAL",
            "value":"429",
            "text":"COLOMBIA, SUCRE, MAJAGUAL",
            "code":"CO-70-429",
            "geoPoint":{
               "lat":8.540430000000072,
               "lon":-74.62972999999994
            },
            "geoRectangle":{
               "Xmin":-74.81372999999994,
               "Xmax":-74.44572999999994,
               "Ymin":8.356430000000072,
               "Ymax":8.724430000000071
            }
         },
         {
            "name":"MORROA",
            "value":"473",
            "text":"COLOMBIA, SUCRE, MORROA",
            "code":"CO-70-473",
            "geoPoint":{
               "lat":9.335300000000075,
               "lon":-75.30538999999999
            },
            "geoRectangle":{
               "Xmin":-75.38438999999998,
               "Xmax":-75.22639,
               "Ymin":9.256300000000074,
               "Ymax":9.414300000000075
            }
         },
         {
            "name":"OVEJAS",
            "value":"508",
            "text":"COLOMBIA, SUCRE, OVEJAS",
            "code":"CO-70-508",
            "geoPoint":{
               "lat":9.525850000000048,
               "lon":-75.22860999999995
            },
            "geoRectangle":{
               "Xmin":-75.36960999999995,
               "Xmax":-75.08760999999994,
               "Ymin":9.384850000000048,
               "Ymax":9.666850000000048
            }
         },
         {
            "name":"PALMITO",
            "value":"523",
            "text":"COLOMBIA, SUCRE, PALMITO",
            "code":"CO-70-523",
            "geoPoint":{
               "lat":9.333280000000059,
               "lon":-75.54186999999996
            },
            "geoRectangle":{
               "Xmin":-75.61686999999996,
               "Xmax":-75.46686999999996,
               "Ymin":9.25828000000006,
               "Ymax":9.408280000000058
            }
         }
      ]
   },
   {
      "name":"TOLIMA",
      "value":"73",
      "text":"COLOMBIA, TOLIMA",
      "code":"CO-73",
      "geoPoint":{
         "lat":4.039664307000066,
         "lon":-75.25429016899994
      },
      "geoRectangle":{
         "Xmin":-76.27429016899994,
         "Xmax":-74.23429016899995,
         "Ymin":3.0196643070000664,
         "Ymax":5.059664307000066
      },
      "municipality":[
         {
            "name":"ICONONZO",
            "value":"352",
            "text":"COLOMBIA, TOLIMA, ICONONZO",
            "code":"CO-73-352",
            "geoPoint":{
               "lat":4.177250000000072,
               "lon":-74.53217999999998
            },
            "geoRectangle":{
               "Xmin":-74.64317999999999,
               "Xmax":-74.42117999999998,
               "Ymin":4.066250000000072,
               "Ymax":4.288250000000072
            }
         },
         {
            "name":"IBAGUÉ",
            "value":"001",
            "text":"COLOMBIA, TOLIMA, IBAGUÉ",
            "code":"CO-73-001",
            "geoPoint":{
               "lat":4.4350200000000655,
               "lon":-75.21947999999998
            },
            "geoRectangle":{
               "Xmin":-75.46847999999997,
               "Xmax":-74.97047999999998,
               "Ymin":4.186020000000066,
               "Ymax":4.684020000000065
            }
         },
         {
            "name":"ALPUJARRA",
            "value":"024",
            "text":"COLOMBIA, TOLIMA, ALPUJARRA",
            "code":"CO-73-024",
            "geoPoint":{
               "lat":3.3913400000000706,
               "lon":-74.93228999999997
            },
            "geoRectangle":{
               "Xmin":-75.05728999999997,
               "Xmax":-74.80728999999997,
               "Ymin":3.2663400000000706,
               "Ymax":3.5163400000000706
            }
         },
         {
            "name":"ALVARADO",
            "value":"026",
            "text":"COLOMBIA, TOLIMA, ALVARADO",
            "code":"CO-73-026",
            "geoPoint":{
               "lat":4.567590000000052,
               "lon":-74.95321999999999
            },
            "geoRectangle":{
               "Xmin":-75.07021999999999,
               "Xmax":-74.83621999999998,
               "Ymin":4.4505900000000524,
               "Ymax":4.684590000000052
            }
         },
         {
            "name":"AMBALEMA",
            "value":"030",
            "text":"COLOMBIA, TOLIMA, AMBALEMA",
            "code":"CO-73-030",
            "geoPoint":{
               "lat":4.7833400000000665,
               "lon":-74.76378999999997
            },
            "geoRectangle":{
               "Xmin":-74.86278999999998,
               "Xmax":-74.66478999999997,
               "Ymin":4.684340000000066,
               "Ymax":4.882340000000067
            }
         },
         {
            "name":" ANZOÁTEGUI",
            "value":"043",
            "text":"COLOMBIA, TOLIMA,  ANZOÁTEGUI",
            "code":"CO-73-043",
            "geoPoint":{
               "lat":4.631040000000041,
               "lon":-75.09473999999994
            },
            "geoRectangle":{
               "Xmin":-75.23473999999995,
               "Xmax":-74.95473999999994,
               "Ymin":4.491040000000042,
               "Ymax":4.771040000000041
            }
         },
         {
            "name":"ARMERO",
            "value":"055",
            "text":"COLOMBIA, TOLIMA, ARMERO",
            "code":"CO-73-055",
            "geoPoint":{
               "lat":5.083330000000046,
               "lon":-74.83332999999999
            },
            "geoRectangle":{
               "Xmin":-75.83332999999999,
               "Xmax":-73.83332999999999,
               "Ymin":4.083330000000046,
               "Ymax":6.083330000000046
            }
         },
         {
            "name":"ATACO",
            "value":"067",
            "text":"COLOMBIA, TOLIMA, ATACO",
            "code":"CO-73-067",
            "geoPoint":{
               "lat":3.5912000000000717,
               "lon":-75.38138999999995
            },
            "geoRectangle":{
               "Xmin":-75.61938999999995,
               "Xmax":-75.14338999999995,
               "Ymin":3.3532000000000717,
               "Ymax":3.8292000000000717
            }
         },
         {
            "name":"CAJAMARCA",
            "value":"124",
            "text":"COLOMBIA, TOLIMA, CAJAMARCA",
            "code":"CO-73-124",
            "geoPoint":{
               "lat":4.4416400000000635,
               "lon":-75.42689999999999
            },
            "geoRectangle":{
               "Xmin":-75.57589999999999,
               "Xmax":-75.27789999999999,
               "Ymin":4.2926400000000635,
               "Ymax":4.590640000000064
            }
         },
         {
            "name":"CARMEN DE APICALA",
            "value":"148",
            "text":"COLOMBIA, TOLIMA, CARMEN DE APICALA",
            "code":"CO-73-148",
            "geoPoint":{
               "lat":4.170475038313526,
               "lon":-74.7165199344589
            },
            "geoRectangle":{
               "Xmin":-74.7175199344589,
               "Xmax":-74.7155199344589,
               "Ymin":4.169475038313526,
               "Ymax":4.171475038313527
            }
         },
         {
            "name":"CASABIANCA",
            "value":"152",
            "text":"COLOMBIA, TOLIMA, CASABIANCA",
            "code":"CO-73-152",
            "geoPoint":{
               "lat":5.0791600000000585,
               "lon":-75.12053999999995
            },
            "geoRectangle":{
               "Xmin":-75.25453999999995,
               "Xmax":-74.98653999999995,
               "Ymin":4.945160000000058,
               "Ymax":5.213160000000059
            }
         },
         {
            "name":"CHAPARRAL",
            "value":"168",
            "text":"COLOMBIA, TOLIMA, CHAPARRAL",
            "code":"CO-73-168",
            "geoPoint":{
               "lat":3.724260000000072,
               "lon":-75.48481999999996
            },
            "geoRectangle":{
               "Xmin":-75.79181999999996,
               "Xmax":-75.17781999999995,
               "Ymin":3.417260000000072,
               "Ymax":4.0312600000000725
            }
         },
         {
            "name":"COELLO",
            "value":"200",
            "text":"COLOMBIA, TOLIMA, COELLO",
            "code":"CO-73-200",
            "geoPoint":{
               "lat":4.2873800000000415,
               "lon":-74.89860999999996
            },
            "geoRectangle":{
               "Xmin":-75.03560999999996,
               "Xmax":-74.76160999999996,
               "Ymin":4.150380000000041,
               "Ymax":4.424380000000042
            }
         },
         {
            "name":"COYAIMA",
            "value":"217",
            "text":"COLOMBIA, TOLIMA, COYAIMA",
            "code":"CO-73-217",
            "geoPoint":{
               "lat":3.798430000000053,
               "lon":-75.19451999999995
            },
            "geoRectangle":{
               "Xmin":-75.37151999999996,
               "Xmax":-75.01751999999995,
               "Ymin":3.621430000000053,
               "Ymax":3.975430000000053
            }
         },
         {
            "name":"DOLORES",
            "value":"236",
            "text":"COLOMBIA, TOLIMA, DOLORES",
            "code":"CO-73-236",
            "geoPoint":{
               "lat":3.5387100000000373,
               "lon":-74.89648999999997
            },
            "geoRectangle":{
               "Xmin":-75.11748999999998,
               "Xmax":-74.67548999999997,
               "Ymin":3.3177100000000372,
               "Ymax":3.7597100000000374
            }
         },
         {
            "name":"ESPINAL",
            "value":"268",
            "text":"COLOMBIA, TOLIMA, ESPINAL",
            "code":"CO-73-268",
            "geoPoint":{
               "lat":4.152470000000051,
               "lon":-74.88575999999995
            },
            "geoRectangle":{
               "Xmin":-74.97675999999994,
               "Xmax":-74.79475999999995,
               "Ymin":4.0614700000000505,
               "Ymax":4.243470000000051
            }
         },
         {
            "name":"FALAN",
            "value":"270",
            "text":"COLOMBIA, TOLIMA, FALAN",
            "code":"CO-73-270",
            "geoPoint":{
               "lat":5.123990000000049,
               "lon":-74.95171999999997
            },
            "geoRectangle":{
               "Xmin":-75.04071999999996,
               "Xmax":-74.86271999999997,
               "Ymin":5.0349900000000485,
               "Ymax":5.212990000000049
            }
         },
         {
            "name":"CUNDAY",
            "value":"226",
            "text":"COLOMBIA, TOLIMA, CUNDAY",
            "code":"CO-73-226",
            "geoPoint":{
               "lat":4.061370000000068,
               "lon":-74.69239999999996
            },
            "geoRectangle":{
               "Xmin":-74.84839999999997,
               "Xmax":-74.53639999999996,
               "Ymin":3.9053700000000675,
               "Ymax":4.217370000000067
            }
         },
         {
            "name":"NATAGAIMA",
            "value":"483",
            "text":"COLOMBIA, TOLIMA, NATAGAIMA",
            "code":"CO-73-483",
            "geoPoint":{
               "lat":3.6224300000000653,
               "lon":-75.09670999999997
            },
            "geoRectangle":{
               "Xmin":-75.27270999999998,
               "Xmax":-74.92070999999997,
               "Ymin":3.446430000000065,
               "Ymax":3.7984300000000655
            }
         },
         {
            "name":"ORTEGA",
            "value":"504",
            "text":"COLOMBIA, TOLIMA, ORTEGA",
            "code":"CO-73-504",
            "geoPoint":{
               "lat":3.935760000000073,
               "lon":-75.22048999999998
            },
            "geoRectangle":{
               "Xmin":-75.40648999999999,
               "Xmax":-75.03448999999998,
               "Ymin":3.749760000000073,
               "Ymax":4.121760000000073
            }
         },
         {
            "name":"PALOCABILDO",
            "value":"520",
            "text":"COLOMBIA, TOLIMA, PALOCABILDO",
            "code":"CO-73-520",
            "geoPoint":{
               "lat":5.121120000000076,
               "lon":-75.02283999999997
            },
            "geoRectangle":{
               "Xmin":-75.06883999999998,
               "Xmax":-74.97683999999997,
               "Ymin":5.075120000000076,
               "Ymax":5.167120000000076
            }
         },
         {
            "name":"PIEDRAS",
            "value":"547",
            "text":"COLOMBIA, TOLIMA, PIEDRAS",
            "code":"CO-73-547",
            "geoPoint":{
               "lat":4.543840000000046,
               "lon":-74.87810999999994
            },
            "geoRectangle":{
               "Xmin":-75.01910999999994,
               "Xmax":-74.73710999999993,
               "Ymin":4.402840000000046,
               "Ymax":4.684840000000046
            }
         },
         {
            "name":"PLANADAS",
            "value":"555",
            "text":"COLOMBIA, TOLIMA, PLANADAS",
            "code":"CO-73-555",
            "geoPoint":{
               "lat":3.1962100000000646,
               "lon":-75.64500999999996
            },
            "geoRectangle":{
               "Xmin":-75.88900999999996,
               "Xmax":-75.40100999999996,
               "Ymin":2.952210000000065,
               "Ymax":3.4402100000000644
            }
         },
         {
            "name":"PRADO",
            "value":"563",
            "text":"COLOMBIA, TOLIMA, PRADO",
            "code":"CO-73-563",
            "geoPoint":{
               "lat":3.7515300000000593,
               "lon":-74.92879999999997
            },
            "geoRectangle":{
               "Xmin":-75.05979999999997,
               "Xmax":-74.79779999999997,
               "Ymin":3.620530000000059,
               "Ymax":3.8825300000000595
            }
         },
         {
            "name":"PURIFICACIÓN",
            "value":"585",
            "text":"COLOMBIA, TOLIMA, PURIFICACIÓN",
            "code":"CO-73-585",
            "geoPoint":{
               "lat":3.8532800000000407,
               "lon":-74.93446999999998
            },
            "geoRectangle":{
               "Xmin":-75.08046999999998,
               "Xmax":-74.78846999999998,
               "Ymin":3.7072800000000408,
               "Ymax":3.9992800000000406
            }
         },
         {
            "name":"RIOBLANCO",
            "value":"616",
            "text":"COLOMBIA, TOLIMA, RIOBLANCO",
            "code":"CO-73-616",
            "geoPoint":{
               "lat":3.5300700000000234,
               "lon":-75.64554999999996
            },
            "geoRectangle":{
               "Xmin":-75.92654999999996,
               "Xmax":-75.36454999999995,
               "Ymin":3.249070000000023,
               "Ymax":3.8110700000000235
            }
         },
         {
            "name":"RONCESVALLES",
            "value":"622",
            "text":"COLOMBIA, TOLIMA, RONCESVALLES",
            "code":"CO-73-622",
            "geoPoint":{
               "lat":4.0106700000000615,
               "lon":-75.60505999999998
            },
            "geoRectangle":{
               "Xmin":-75.78305999999998,
               "Xmax":-75.42705999999998,
               "Ymin":3.8326700000000615,
               "Ymax":4.188670000000061
            }
         },
         {
            "name":"ROVIRA",
            "value":"624",
            "text":"COLOMBIA, TOLIMA, ROVIRA",
            "code":"CO-73-624",
            "geoPoint":{
               "lat":4.238680000000045,
               "lon":-75.23962999999998
            },
            "geoRectangle":{
               "Xmin":-75.42362999999997,
               "Xmax":-75.05562999999998,
               "Ymin":4.054680000000045,
               "Ymax":4.422680000000045
            }
         },
         {
            "name":"SALDAÑA",
            "value":"671",
            "text":"COLOMBIA, TOLIMA, SALDAÑA",
            "code":"CO-73-671",
            "geoPoint":{
               "lat":3.9284200000000737,
               "lon":-75.01556999999997
            },
            "geoRectangle":{
               "Xmin":-75.11156999999997,
               "Xmax":-74.91956999999996,
               "Ymin":3.8324200000000737,
               "Ymax":4.024420000000074
            }
         },
         {
            "name":"SAN ANTONIO",
            "value":"675",
            "text":"COLOMBIA, TOLIMA, SAN ANTONIO",
            "code":"CO-73-675",
            "geoPoint":{
               "lat":3.915650000000028,
               "lon":-75.47800999999998
            },
            "geoRectangle":{
               "Xmin":-75.61300999999999,
               "Xmax":-75.34300999999998,
               "Ymin":3.780650000000028,
               "Ymax":4.050650000000028
            }
         },
         {
            "name":"SAN LUIS",
            "value":"678",
            "text":"COLOMBIA, TOLIMA, SAN LUIS",
            "code":"CO-73-678",
            "geoPoint":{
               "lat":4.132930000000044,
               "lon":-75.09459999999996
            },
            "geoRectangle":{
               "Xmin":-75.24959999999996,
               "Xmax":-74.93959999999996,
               "Ymin":3.9779300000000446,
               "Ymax":4.287930000000045
            }
         },
         {
            "name":"SANTA ISABEL",
            "value":"686",
            "text":"COLOMBIA, TOLIMA, SANTA ISABEL",
            "code":"CO-73-686",
            "geoPoint":{
               "lat":4.7143500000000245,
               "lon":-75.09786999999994
            },
            "geoRectangle":{
               "Xmin":-75.23186999999994,
               "Xmax":-74.96386999999994,
               "Ymin":4.580350000000024,
               "Ymax":4.848350000000025
            }
         },
         {
            "name":"SUARÉZ",
            "value":"770",
            "text":"COLOMBIA, TOLIMA, SUARÉZ",
            "code":"CO-73-770",
            "geoPoint":{
               "lat":4.0491700000000606,
               "lon":-74.83164999999997
            },
            "geoRectangle":{
               "Xmin":-74.95764999999997,
               "Xmax":-74.70564999999996,
               "Ymin":3.9231700000000607,
               "Ymax":4.175170000000061
            }
         },
         {
            "name":"VALLE DE SAN JUAN",
            "value":"854",
            "text":"COLOMBIA, TOLIMA, VALLE DE SAN JUAN",
            "code":"CO-73-854",
            "geoPoint":{
               "lat":4.198820000000069,
               "lon":-75.11670999999996
            },
            "geoRectangle":{
               "Xmin":-75.20170999999995,
               "Xmax":-75.03170999999996,
               "Ymin":4.113820000000069,
               "Ymax":4.283820000000069
            }
         },
         {
            "name":" VENADILLO",
            "value":"861",
            "text":"COLOMBIA, TOLIMA,  VENADILLO",
            "code":"CO-73-861",
            "geoPoint":{
               "lat":4.719560000000058,
               "lon":-74.92815999999993
            },
            "geoRectangle":{
               "Xmin":-75.04915999999993,
               "Xmax":-74.80715999999994,
               "Ymin":4.598560000000058,
               "Ymax":4.840560000000059
            }
         },
         {
            "name":"VILLAHERMOSA",
            "value":"870",
            "text":"COLOMBIA, TOLIMA, VILLAHERMOSA",
            "code":"CO-73-870",
            "geoPoint":{
               "lat":5.030620000000056,
               "lon":-75.11599999999999
            },
            "geoRectangle":{
               "Xmin":-75.23899999999999,
               "Xmax":-74.99299999999998,
               "Ymin":4.907620000000056,
               "Ymax":5.153620000000056
            }
         },
         {
            "name":"FLANDES",
            "value":"275",
            "text":"COLOMBIA, TOLIMA, FLANDES",
            "code":"CO-73-275",
            "geoPoint":{
               "lat":4.282190000000071,
               "lon":-74.81476999999995
            },
            "geoRectangle":{
               "Xmin":-74.87476999999996,
               "Xmax":-74.75476999999995,
               "Ymin":4.222190000000071,
               "Ymax":4.342190000000071
            }
         },
         {
            "name":"FRESNO",
            "value":"283",
            "text":"COLOMBIA, TOLIMA, FRESNO",
            "code":"CO-73-283",
            "geoPoint":{
               "lat":5.154210000000035,
               "lon":-75.04079999999993
            },
            "geoRectangle":{
               "Xmin":-75.13979999999994,
               "Xmax":-74.94179999999993,
               "Ymin":5.055210000000034,
               "Ymax":5.253210000000035
            }
         },
         {
            "name":"GUAMO",
            "value":"319",
            "text":"COLOMBIA, TOLIMA, GUAMO",
            "code":"CO-73-319",
            "geoPoint":{
               "lat":4.030040000000042,
               "lon":-74.96871999999996
            },
            "geoRectangle":{
               "Xmin":-75.11371999999996,
               "Xmax":-74.82371999999997,
               "Ymin":3.8850400000000422,
               "Ymax":4.175040000000042
            }
         },
         {
            "name":"HERVEO",
            "value":"347",
            "text":"COLOMBIA, TOLIMA, HERVEO",
            "code":"CO-73-347",
            "geoPoint":{
               "lat":5.080000000000041,
               "lon":-75.17530999999997
            },
            "geoRectangle":{
               "Xmin":-75.30130999999997,
               "Xmax":-75.04930999999996,
               "Ymin":4.954000000000041,
               "Ymax":5.206000000000041
            }
         },
         {
            "name":"HONDA",
            "value":"349",
            "text":"COLOMBIA, TOLIMA, HONDA",
            "code":"CO-73-349",
            "geoPoint":{
               "lat":5.207560000000058,
               "lon":-74.73684999999995
            },
            "geoRectangle":{
               "Xmin":-74.84984999999995,
               "Xmax":-74.62384999999995,
               "Ymin":5.094560000000057,
               "Ymax":5.320560000000058
            }
         },
         {
            "name":"LÉRIDA",
            "value":"408",
            "text":"COLOMBIA, TOLIMA, LÉRIDA",
            "code":"CO-73-408",
            "geoPoint":{
               "lat":4.859300000000076,
               "lon":-74.91064999999998
            },
            "geoRectangle":{
               "Xmin":-75.01164999999997,
               "Xmax":-74.80964999999998,
               "Ymin":4.758300000000076,
               "Ymax":4.960300000000076
            }
         },
         {
            "name":"LIBANO",
            "value":"411",
            "text":"COLOMBIA, TOLIMA, LIBANO",
            "code":"CO-73-411",
            "geoPoint":{
               "lat":4.916840000000036,
               "lon":-75.05127999999996
            },
            "geoRectangle":{
               "Xmin":-75.05627999999996,
               "Xmax":-75.04627999999997,
               "Ymin":4.911840000000036,
               "Ymax":4.921840000000036
            }
         },
         {
            "name":"SAN SEBASTIÁN DE MARIQUITA",
            "value":"443",
            "text":"COLOMBIA, TOLIMA, SAN SEBASTIÁN DE MARIQUITA",
            "code":"CO-73-443",
            "geoPoint":{
               "lat":5.192190000000039,
               "lon":-74.89150999999998
            },
            "geoRectangle":{
               "Xmin":-74.98850999999998,
               "Xmax":-74.79450999999999,
               "Ymin":5.095190000000039,
               "Ymax":5.28919000000004
            }
         },
         {
            "name":"MELGAR",
            "value":"449",
            "text":"COLOMBIA, TOLIMA, MELGAR",
            "code":"CO-73-449",
            "geoPoint":{
               "lat":4.205870000000061,
               "lon":-74.64290999999997
            },
            "geoRectangle":{
               "Xmin":-74.73690999999997,
               "Xmax":-74.54890999999998,
               "Ymin":4.111870000000061,
               "Ymax":4.299870000000062
            }
         },
         {
            "name":"MURILLO",
            "value":"461",
            "text":"COLOMBIA, TOLIMA, MURILLO",
            "code":"CO-73-461",
            "geoPoint":{
               "lat":4.873600000000067,
               "lon":-75.17180999999994
            },
            "geoRectangle":{
               "Xmin":-75.29480999999994,
               "Xmax":-75.04880999999993,
               "Ymin":4.750600000000067,
               "Ymax":4.996600000000067
            }
         },
         {
            "name":"VILLARRICA",
            "value":"873",
            "text":"COLOMBIA, TOLIMA, VILLARRICA",
            "code":"CO-73-873",
            "geoPoint":{
               "lat":3.9359600000000228,
               "lon":-74.60109999999997
            },
            "geoRectangle":{
               "Xmin":-74.72109999999998,
               "Xmax":-74.48109999999997,
               "Ymin":3.8159600000000227,
               "Ymax":4.055960000000023
            }
         }
      ]
   },
   {
      "name":"VALLE DEL CAUCA",
      "value":"76",
      "text":"COLOMBIA, VALLE DEL CAUCA",
      "code":"CO-76",
      "geoPoint":{
         "lat":3.8493070940000393,
         "lon":-76.52261190799999
      },
      "geoRectangle":{
         "Xmin":-78.49161190799998,
         "Xmax":-74.553611908,
         "Ymin":1.8803070940000393,
         "Ymax":5.81830709400004
      },
      "municipality":[
         {
            "name":"BOLÍVAR",
            "value":"100",
            "text":"COLOMBIA, VALLE DEL CAUCA, BOLÍVAR",
            "code":"CO-76-100",
            "geoPoint":{
               "lat":4.33910000000003,
               "lon":-76.18404999999996
            },
            "geoRectangle":{
               "Xmin":-76.35404999999996,
               "Xmax":-76.01404999999995,
               "Ymin":4.1691000000000304,
               "Ymax":4.50910000000003
            }
         },
         {
            "name":"DAGUA",
            "value":"233",
            "text":"COLOMBIA, VALLE DEL CAUCA, DAGUA",
            "code":"CO-76-233",
            "geoPoint":{
               "lat":3.6582500000000664,
               "lon":-76.69168999999994
            },
            "geoRectangle":{
               "Xmin":-76.88168999999994,
               "Xmax":-76.50168999999994,
               "Ymin":3.4682500000000664,
               "Ymax":3.8482500000000663
            }
         },
         {
            "name":"PRADERA",
            "value":"563",
            "text":"COLOMBIA, VALLE DEL CAUCA, PRADERA",
            "code":"CO-76-563",
            "geoPoint":{
               "lat":3.4194100000000276,
               "lon":-76.24211999999994
            },
            "geoRectangle":{
               "Xmin":-76.35211999999994,
               "Xmax":-76.13211999999994,
               "Ymin":3.3094100000000277,
               "Ymax":3.5294100000000275
            }
         },
         {
            "name":"RESTREPO",
            "value":"606",
            "text":"COLOMBIA, VALLE DEL CAUCA, RESTREPO",
            "code":"CO-76-606",
            "geoPoint":{
               "lat":3.8222600000000284,
               "lon":-76.52221999999995
            },
            "geoRectangle":{
               "Xmin":-76.60121999999994,
               "Xmax":-76.44321999999995,
               "Ymin":3.7432600000000282,
               "Ymax":3.9012600000000286
            }
         },
         {
            "name":"RIOFRÍO",
            "value":"616",
            "text":"COLOMBIA, VALLE DEL CAUCA, RIOFRÍO",
            "code":"CO-76-616",
            "geoPoint":{
               "lat":4.156260000000032,
               "lon":-76.28750999999994
            },
            "geoRectangle":{
               "Xmin":-76.39850999999994,
               "Xmax":-76.17650999999994,
               "Ymin":4.045260000000032,
               "Ymax":4.267260000000031
            }
         },
         {
            "name":"ROLDANILLO",
            "value":"622",
            "text":"COLOMBIA, VALLE DEL CAUCA, ROLDANILLO",
            "code":"CO-76-622",
            "geoPoint":{
               "lat":4.413280000000043,
               "lon":-76.15270999999996
            },
            "geoRectangle":{
               "Xmin":-76.23970999999996,
               "Xmax":-76.06570999999995,
               "Ymin":4.326280000000043,
               "Ymax":4.500280000000043
            }
         },
         {
            "name":"SAN PEDRO",
            "value":"670",
            "text":"COLOMBIA, VALLE DEL CAUCA, SAN PEDRO",
            "code":"CO-76-670",
            "geoPoint":{
               "lat":3.9938400000000343,
               "lon":-76.22670999999997
            },
            "geoRectangle":{
               "Xmin":-76.31670999999997,
               "Xmax":-76.13670999999997,
               "Ymin":3.9038400000000344,
               "Ymax":4.083840000000034
            }
         },
         {
            "name":"SEVILLA",
            "value":"736",
            "text":"COLOMBIA, VALLE DEL CAUCA, SEVILLA",
            "code":"CO-76-736",
            "geoPoint":{
               "lat":4.2663800000000265,
               "lon":-75.93262999999996
            },
            "geoRectangle":{
               "Xmin":-76.13462999999996,
               "Xmax":-75.73062999999996,
               "Ymin":4.0643800000000265,
               "Ymax":4.468380000000026
            }
         },
         {
            "name":"TORO",
            "value":"823",
            "text":"COLOMBIA, VALLE DEL CAUCA, TORO",
            "code":"CO-76-823",
            "geoPoint":{
               "lat":4.608570000000043,
               "lon":-76.07599999999996
            },
            "geoRectangle":{
               "Xmin":-76.15699999999997,
               "Xmax":-75.99499999999996,
               "Ymin":4.5275700000000425,
               "Ymax":4.689570000000043
            }
         },
         {
            "name":"TRUJILLO",
            "value":"828",
            "text":"COLOMBIA, VALLE DEL CAUCA, TRUJILLO",
            "code":"CO-76-828",
            "geoPoint":{
               "lat":4.21198000000004,
               "lon":-76.31913999999995
            },
            "geoRectangle":{
               "Xmin":-76.42713999999995,
               "Xmax":-76.21113999999994,
               "Ymin":4.10398000000004,
               "Ymax":4.319980000000039
            }
         },
         {
            "name":"TULUÁ",
            "value":"834",
            "text":"COLOMBIA, VALLE DEL CAUCA, TULUÁ",
            "code":"CO-76-834",
            "geoPoint":{
               "lat":4.085990000000038,
               "lon":-76.19633999999996
            },
            "geoRectangle":{
               "Xmin":-76.39233999999996,
               "Xmax":-76.00033999999997,
               "Ymin":3.889990000000038,
               "Ymax":4.281990000000038
            }
         },
         {
            "name":"ULLOA",
            "value":"845",
            "text":"COLOMBIA, VALLE DEL CAUCA, ULLOA",
            "code":"CO-76-845",
            "geoPoint":{
               "lat":4.7026200000000244,
               "lon":-75.73732999999999
            },
            "geoRectangle":{
               "Xmin":-75.79032999999998,
               "Xmax":-75.68432999999999,
               "Ymin":4.6496200000000245,
               "Ymax":4.755620000000024
            }
         },
         {
            "name":"VERSALLES",
            "value":"863",
            "text":"COLOMBIA, VALLE DEL CAUCA, VERSALLES",
            "code":"CO-76-863",
            "geoPoint":{
               "lat":4.5746200000000385,
               "lon":-76.19851999999997
            },
            "geoRectangle":{
               "Xmin":-76.29451999999998,
               "Xmax":-76.10251999999997,
               "Ymin":4.4786200000000385,
               "Ymax":4.670620000000039
            }
         },
         {
            "name":"VIJES",
            "value":"869",
            "text":"COLOMBIA, VALLE DEL CAUCA, VIJES",
            "code":"CO-76-869",
            "geoPoint":{
               "lat":3.6978200000000356,
               "lon":-76.43856999999997
            },
            "geoRectangle":{
               "Xmin":-76.50956999999997,
               "Xmax":-76.36756999999997,
               "Ymin":3.6268200000000355,
               "Ymax":3.768820000000036
            }
         },
         {
            "name":"YOTOCO",
            "value":"890",
            "text":"COLOMBIA, VALLE DEL CAUCA, YOTOCO",
            "code":"CO-76-890",
            "geoPoint":{
               "lat":3.8615800000000604,
               "lon":-76.38353999999998
            },
            "geoRectangle":{
               "Xmin":-76.51653999999998,
               "Xmax":-76.25053999999999,
               "Ymin":3.7285800000000604,
               "Ymax":3.9945800000000604
            }
         },
         {
            "name":"YUMBO",
            "value":"892",
            "text":"COLOMBIA, VALLE DEL CAUCA, YUMBO",
            "code":"CO-76-892",
            "geoPoint":{
               "lat":3.577570000000037,
               "lon":-76.48618999999997
            },
            "geoRectangle":{
               "Xmin":-76.58018999999996,
               "Xmax":-76.39218999999997,
               "Ymin":3.483570000000037,
               "Ymax":3.671570000000037
            }
         },
         {
            "name":"ZARZAL",
            "value":"895",
            "text":"COLOMBIA, VALLE DEL CAUCA, ZARZAL",
            "code":"CO-76-895",
            "geoPoint":{
               "lat":4.396830000000023,
               "lon":-76.07027999999997
            },
            "geoRectangle":{
               "Xmin":-76.20027999999996,
               "Xmax":-75.94027999999997,
               "Ymin":4.266830000000023,
               "Ymax":4.526830000000023
            }
         },
         {
            "name":"LA VICTORIA",
            "value":"403",
            "text":"COLOMBIA, VALLE DEL CAUCA, LA VICTORIA",
            "code":"CO-76-403",
            "geoPoint":{
               "lat":4.523550000000057,
               "lon":-76.03787999999997
            },
            "geoRectangle":{
               "Xmin":-76.13287999999997,
               "Xmax":-75.94287999999997,
               "Ymin":4.428550000000057,
               "Ymax":4.618550000000057
            }
         },
         {
            "name":"LA UNIÓN",
            "value":"400",
            "text":"COLOMBIA, VALLE DEL CAUCA, LA UNIÓN",
            "code":"CO-76-400",
            "geoPoint":{
               "lat":4.534940000000063,
               "lon":-76.10341999999997
            },
            "geoRectangle":{
               "Xmin":-76.16741999999996,
               "Xmax":-76.03941999999998,
               "Ymin":4.470940000000063,
               "Ymax":4.598940000000063
            }
         },
         {
            "name":"LA CUMBRE",
            "value":"377",
            "text":"COLOMBIA, VALLE DEL CAUCA, LA CUMBRE",
            "code":"CO-76-377",
            "geoPoint":{
               "lat":3.6499800000000278,
               "lon":-76.56909999999993
            },
            "geoRectangle":{
               "Xmin":-76.66109999999993,
               "Xmax":-76.47709999999994,
               "Ymin":3.5579800000000277,
               "Ymax":3.741980000000028
            }
         },
         {
            "name":" JAMUNDÍ",
            "value":"364",
            "text":"COLOMBIA, VALLE DEL CAUCA,  JAMUNDÍ",
            "code":"CO-76-364",
            "geoPoint":{
               "lat":3.2620900000000574,
               "lon":-76.54108999999994
            },
            "geoRectangle":{
               "Xmin":-76.69008999999994,
               "Xmax":-76.39208999999994,
               "Ymin":3.1130900000000574,
               "Ymax":3.4110900000000575
            }
         },
         {
            "name":"BUENAVENTURA",
            "value":"109",
            "text":"COLOMBIA, VALLE DEL CAUCA, BUENAVENTURA",
            "code":"CO-76-109",
            "geoPoint":{
               "lat":3.88301000000007,
               "lon":-77.04738999999995
            },
            "geoRectangle":{
               "Xmin":-78.57238999999996,
               "Xmax":-75.52238999999994,
               "Ymin":2.3580100000000694,
               "Ymax":5.40801000000007
            }
         },
         {
            "name":"CALI",
            "value":"001",
            "text":"COLOMBIA, VALLE DEL CAUCA, CALI",
            "code":"CO-76-001",
            "geoPoint":{
               "lat":3.4340200000000323,
               "lon":-76.52644999999995
            },
            "geoRectangle":{
               "Xmin":-76.65844999999996,
               "Xmax":-76.39444999999995,
               "Ymin":3.302020000000032,
               "Ymax":3.5660200000000324
            }
         },
         {
            "name":" ANDALUCÍA",
            "value":"036",
            "text":"COLOMBIA, VALLE DEL CAUCA,  ANDALUCÍA",
            "code":"CO-76-036",
            "geoPoint":{
               "lat":4.173590000000047,
               "lon":-76.16475999999994
            },
            "geoRectangle":{
               "Xmin":-76.24775999999994,
               "Xmax":-76.08175999999995,
               "Ymin":4.090590000000047,
               "Ymax":4.256590000000047
            }
         },
         {
            "name":"ANSERMANUEVO",
            "value":"041",
            "text":"COLOMBIA, VALLE DEL CAUCA, ANSERMANUEVO",
            "code":"CO-76-041",
            "geoPoint":{
               "lat":4.795240000000035,
               "lon":-75.99400999999995
            },
            "geoRectangle":{
               "Xmin":-76.10500999999995,
               "Xmax":-75.88300999999994,
               "Ymin":4.6842400000000355,
               "Ymax":4.906240000000035
            }
         },
         {
            "name":"ARGELIA",
            "value":"054",
            "text":"COLOMBIA, VALLE DEL CAUCA, ARGELIA",
            "code":"CO-76-054",
            "geoPoint":{
               "lat":4.726560000000063,
               "lon":-76.12176999999997
            },
            "geoRectangle":{
               "Xmin":-76.18176999999997,
               "Xmax":-76.06176999999997,
               "Ymin":4.6665600000000635,
               "Ymax":4.786560000000063
            }
         },
         {
            "name":"BUGA",
            "value":"111",
            "text":"COLOMBIA, VALLE DEL CAUCA, BUGA",
            "code":"CO-76-111",
            "geoPoint":{
               "lat":3.900890000000061,
               "lon":-76.29782999999998
            },
            "geoRectangle":{
               "Xmin":-76.31382999999998,
               "Xmax":-76.28182999999997,
               "Ymin":3.884890000000061,
               "Ymax":3.916890000000061
            }
         },
         {
            "name":"BUGALAGRANDE",
            "value":"113",
            "text":"COLOMBIA, VALLE DEL CAUCA, BUGALAGRANDE",
            "code":"CO-76-113",
            "geoPoint":{
               "lat":4.209880000000055,
               "lon":-76.15635999999995
            },
            "geoRectangle":{
               "Xmin":-76.28535999999995,
               "Xmax":-76.02735999999994,
               "Ymin":4.080880000000056,
               "Ymax":4.338880000000055
            }
         },
         {
            "name":"CAICEDONIA",
            "value":"122",
            "text":"COLOMBIA, VALLE DEL CAUCA, CAICEDONIA",
            "code":"CO-76-122",
            "geoPoint":{
               "lat":4.330340000000035,
               "lon":-75.82647999999995
            },
            "geoRectangle":{
               "Xmin":-75.90547999999994,
               "Xmax":-75.74747999999995,
               "Ymin":4.251340000000035,
               "Ymax":4.409340000000035
            }
         },
         {
            "name":"CALIMA",
            "value":"126",
            "text":"COLOMBIA, VALLE DEL CAUCA, CALIMA",
            "code":"CO-76-126",
            "geoPoint":{
               "lat":3.9309800000000337,
               "lon":-76.48410999999999
            },
            "geoRectangle":{
               "Xmin":-76.66810999999998,
               "Xmax":-76.30010999999999,
               "Ymin":3.7469800000000335,
               "Ymax":4.114980000000034
            }
         },
         {
            "name":"CANDELARIA",
            "value":"130",
            "text":"COLOMBIA, VALLE DEL CAUCA, CANDELARIA",
            "code":"CO-76-130",
            "geoPoint":{
               "lat":3.407380000000046,
               "lon":-76.34880999999996
            },
            "geoRectangle":{
               "Xmin":-76.44380999999996,
               "Xmax":-76.25380999999996,
               "Ymin":3.312380000000046,
               "Ymax":3.5023800000000462
            }
         },
         {
            "name":"CARTAGO",
            "value":"147",
            "text":"COLOMBIA, VALLE DEL CAUCA, CARTAGO",
            "code":"CO-76-147",
            "geoPoint":{
               "lat":4.751330000000053,
               "lon":-75.91324999999995
            },
            "geoRectangle":{
               "Xmin":-76.01024999999994,
               "Xmax":-75.81624999999995,
               "Ymin":4.654330000000052,
               "Ymax":4.848330000000053
            }
         },
         {
            "name":"EL ÁGUILA",
            "value":"243",
            "text":"COLOMBIA, VALLE DEL CAUCA, EL ÁGUILA",
            "code":"CO-76-243",
            "geoPoint":{
               "lat":4.908350000000041,
               "lon":-76.04209999999995
            },
            "geoRectangle":{
               "Xmin":-76.12809999999995,
               "Xmax":-75.95609999999995,
               "Ymin":4.822350000000041,
               "Ymax":4.994350000000042
            }
         },
         {
            "name":"EL CAIRO",
            "value":"246",
            "text":"COLOMBIA, VALLE DEL CAUCA, EL CAIRO",
            "code":"CO-76-246",
            "geoPoint":{
               "lat":4.760850000000062,
               "lon":-76.22066999999998
            },
            "geoRectangle":{
               "Xmin":-76.30666999999998,
               "Xmax":-76.13466999999999,
               "Ymin":4.6748500000000615,
               "Ymax":4.846850000000062
            }
         },
         {
            "name":"EL CERRITO",
            "value":"248",
            "text":"COLOMBIA, VALLE DEL CAUCA, EL CERRITO",
            "code":"CO-76-248",
            "geoPoint":{
               "lat":3.686670000000049,
               "lon":-76.31397999999996
            },
            "geoRectangle":{
               "Xmin":-76.47397999999995,
               "Xmax":-76.15397999999996,
               "Ymin":3.526670000000049,
               "Ymax":3.8466700000000493
            }
         },
         {
            "name":"EL DOVIO",
            "value":"250",
            "text":"COLOMBIA, VALLE DEL CAUCA, EL DOVIO",
            "code":"CO-76-250",
            "geoPoint":{
               "lat":4.509290000000021,
               "lon":-76.23704999999995
            },
            "geoRectangle":{
               "Xmin":-76.32804999999995,
               "Xmax":-76.14604999999996,
               "Ymin":4.418290000000021,
               "Ymax":4.6002900000000215
            }
         },
         {
            "name":"FLORIDA",
            "value":"275",
            "text":"COLOMBIA, VALLE DEL CAUCA, FLORIDA",
            "code":"CO-76-275",
            "geoPoint":{
               "lat":3.321480000000065,
               "lon":-76.23499999999996
            },
            "geoRectangle":{
               "Xmin":-76.35699999999996,
               "Xmax":-76.11299999999996,
               "Ymin":3.199480000000065,
               "Ymax":3.443480000000065
            }
         },
         {
            "name":"GINEBRA",
            "value":"306",
            "text":"COLOMBIA, VALLE DEL CAUCA, GINEBRA",
            "code":"CO-76-306",
            "geoPoint":{
               "lat":3.7251700000000483,
               "lon":-76.26676999999995
            },
            "geoRectangle":{
               "Xmin":-76.36976999999995,
               "Xmax":-76.16376999999996,
               "Ymin":3.622170000000048,
               "Ymax":3.8281700000000485
            }
         },
         {
            "name":"GUACARÍ",
            "value":"318",
            "text":"COLOMBIA, VALLE DEL CAUCA, GUACARÍ",
            "code":"CO-76-318",
            "geoPoint":{
               "lat":3.7642400000000293,
               "lon":-76.33292999999998
            },
            "geoRectangle":{
               "Xmin":-76.42492999999997,
               "Xmax":-76.24092999999998,
               "Ymin":3.6722400000000293,
               "Ymax":3.8562400000000294
            }
         },
         {
            "name":"ALCALÁ",
            "value":"020",
            "text":"COLOMBIA, VALLE DEL CAUCA, ALCALÁ",
            "code":"CO-76-020",
            "geoPoint":{
               "lat":4.675020000000075,
               "lon":-75.78262999999998
            },
            "geoRectangle":{
               "Xmin":-75.83862999999998,
               "Xmax":-75.72662999999999,
               "Ymin":4.6190200000000745,
               "Ymax":4.731020000000075
            }
         },
         {
            "name":"OBANDO",
            "value":"497",
            "text":"COLOMBIA, VALLE DEL CAUCA, OBANDO",
            "code":"CO-76-497",
            "geoPoint":{
               "lat":4.576970000000074,
               "lon":-75.97366999999997
            },
            "geoRectangle":{
               "Xmin":-76.05366999999997,
               "Xmax":-75.89366999999997,
               "Ymin":4.496970000000074,
               "Ymax":4.656970000000074
            }
         },
         {
            "name":"PALMIRA",
            "value":"520",
            "text":"COLOMBIA, VALLE DEL CAUCA, PALMIRA",
            "code":"CO-76-520",
            "geoPoint":{
               "lat":3.542120000000068,
               "lon":-76.27825999999999
            },
            "geoRectangle":{
               "Xmin":-76.48025999999999,
               "Xmax":-76.07625999999999,
               "Ymin":3.340120000000068,
               "Ymax":3.744120000000068
            }
         }
      ]
   },
   {
      "name":"VAUPÉS",
      "value":"97",
      "text":"COLOMBIA, VAUPÉS",
      "code":"CO-97",
      "geoPoint":{
         "lat":0.6466890840000588,
         "lon":-70.55981138499999
      },
      "geoRectangle":{
         "Xmin":-72.11681138499999,
         "Xmax":-69.00281138499999,
         "Ymin":-0.9103109159999412,
         "Ymax":2.2036890840000587
      },
      "municipality":[
         {
            "name":"CARURÚ",
            "value":"161",
            "text":"COLOMBIA, VAUPÉS, CARURÚ",
            "code":"CO-97-161",
            "geoPoint":{
               "lat":1.0140200000000732,
               "lon":-71.29623999999995
            },
            "geoRectangle":{
               "Xmin":-71.30623999999996,
               "Xmax":-71.28623999999995,
               "Ymin":1.0040200000000732,
               "Ymax":1.0240200000000732
            }
         },
         {
            "name":"MITÚ",
            "value":"001",
            "text":"COLOMBIA, VAUPÉS, MITÚ",
            "code":"CO-97-001",
            "geoPoint":{
               "lat":1.2587100000000646,
               "lon":-70.23695999999995
            },
            "geoRectangle":{
               "Xmin":-70.95095999999995,
               "Xmax":-69.52295999999996,
               "Ymin":0.5447100000000646,
               "Ymax":1.9727100000000646
            }
         },
         {
            "name":"PACOA (Cor. Departamental)",
            "value":"511",
            "text":"COLOMBIA, VAUPÉS, PACOA (Cor. Departamental)",
            "code":"CO-97-511",
            "geoPoint":{
               "lat":0.021580000000028576,
               "lon":-71.00378999999998
            },
            "geoRectangle":{
               "Xmin":-71.85078999999998,
               "Xmax":-70.15678999999999,
               "Ymin":-0.8254199999999714,
               "Ymax":0.8685800000000286
            }
         },
         {
            "name":"TARAIRA",
            "value":"666",
            "text":"COLOMBIA, VAUPÉS, TARAIRA",
            "code":"CO-97-666",
            "geoPoint":{
               "lat":-0.5644299999999589,
               "lon":-69.63407999999998
            },
            "geoRectangle":{
               "Xmin":-70.14107999999999,
               "Xmax":-69.12707999999998,
               "Ymin":-1.071429999999959,
               "Ymax":-0.0574299999999589
            }
         },
         {
            "name":"PAPUNAUA(Cor. Departamental)",
            "value":"777",
            "text":"COLOMBIA, VAUPÉS, PAPUNAUA(Cor. Departamental)",
            "code":"CO-97-777",
            "geoPoint":{
               "lat":1.907380000000046,
               "lon":-70.76195999999999
            },
            "geoRectangle":{
               "Xmin":-71.33895999999999,
               "Xmax":-70.18495999999999,
               "Ymin":1.330380000000046,
               "Ymax":2.484380000000046
            }
         },
         {
            "name":"YAVARATÉ  (Cor. Departamental)",
            "value":"889",
            "text":"COLOMBIA, VAUPÉS, YAVARATÉ  (Cor. Departamental)",
            "code":"CO-97-889",
            "geoPoint":{
               "lat":0.6093800000000442,
               "lon":-69.20322999999996
            },
            "geoRectangle":{
               "Xmin":-69.60222999999996,
               "Xmax":-68.80422999999996,
               "Ymin":0.2103800000000442,
               "Ymax":1.0083800000000442
            }
         }
      ]
   },
   {
      "name":"VICHADA",
      "value":"99",
      "text":"COLOMBIA, VICHADA",
      "code":"CO-99",
      "geoPoint":{
         "lat":4.710178727000027,
         "lon":-69.41965949499996
      },
      "geoRectangle":{
         "Xmin":-71.23465949499996,
         "Xmax":-67.60465949499996,
         "Ymin":2.8951787270000273,
         "Ymax":6.525178727000027
      },
      "municipality":[
         {
            "name":"SANTA ROSALÍA",
            "value":"624",
            "text":"COLOMBIA, VICHADA, SANTA ROSALÍA",
            "code":"CO-99-624",
            "geoPoint":{
               "lat":5.141950000000065,
               "lon":-70.85837999999995
            },
            "geoRectangle":{
               "Xmin":-71.14737999999996,
               "Xmax":-70.56937999999995,
               "Ymin":4.852950000000066,
               "Ymax":5.430950000000065
            }
         },
         {
            "name":"CUMARIBO",
            "value":"773",
            "text":"COLOMBIA, VICHADA, CUMARIBO",
            "code":"CO-99-773",
            "geoPoint":{
               "lat":4.446040000000039,
               "lon":-69.79598999999996
            },
            "geoRectangle":{
               "Xmin":-71.34898999999996,
               "Xmax":-68.24298999999996,
               "Ymin":2.893040000000039,
               "Ymax":5.999040000000039
            }
         },
         {
            "name":"PUERTO CARREÑO",
            "value":"001",
            "text":"COLOMBIA, VICHADA, PUERTO CARREÑO",
            "code":"CO-99-001",
            "geoPoint":{
               "lat":6.187730000000045,
               "lon":-67.48381999999998
            },
            "geoRectangle":{
               "Xmin":-68.08581999999998,
               "Xmax":-66.88181999999998,
               "Ymin":5.585730000000044,
               "Ymax":6.789730000000045
            }
         },
         {
            "name":"LA PRIMAVERA",
            "value":"524",
            "text":"COLOMBIA, VICHADA, LA PRIMAVERA",
            "code":"CO-99-524",
            "geoPoint":{
               "lat":5.489530000000059,
               "lon":-70.41428999999994
            },
            "geoRectangle":{
               "Xmin":-71.24628999999993,
               "Xmax":-69.58228999999994,
               "Ymin":4.657530000000059,
               "Ymax":6.321530000000059
            }
         }
      ]
   }
]