export const departamentos = [{
        "codigo": "47",
        "nombre": "MAGDALENA",
        "id": "j2-Hy3oBVXS07smaYJey"
    },
    {
        "codigo": "50",
        "nombre": "META",
        "id": "qm-Hy3oBVXS07smaepep"
    },
    {
        "codigo": "15",
        "nombre": "BOYACÁ",
        "id": "q2-Hy3oBVXS07smaepep"
    },
    {
        "codigo": "41",
        "nombre": "HUILA",
        "id": "pm-Hy3oBVXS07smadpcJ"
    },
    {
        "codigo": "18",
        "nombre": "CAQUETÁ",
        "id": "p2-Hy3oBVXS07smadpcJ"
    },
    {
        "codigo": "13",
        "nombre": "BOLÍVAR",
        "id": "gW-Hy3oBVXS07smaWpdL"
    },
    {
        "codigo": "20",
        "nombre": "CESAR",
        "id": "km-Hy3oBVXS07smaZZdg"
    },
    {
        "codigo": "05",
        "nombre": "ANTIOQUIA",
        "id": "bG-Hy3oBVXS07smaRJfD"
    },
    {
        "codigo": "19",
        "nombre": "CAUCA",
        "id": "lG-Hy3oBVXS07smabpdG"
    },
    {
        "codigo": "44",
        "nombre": "LA GUAJIRA",
        "id": "k2-Hy3oBVXS07smaapck"
    },
    {
        "codigo": "76",
        "nombre": "VALLE DEL CAUCA",
        "id": "pW-Hy3oBVXS07smacZfl"
    },
    {
        "codigo": "70",
        "nombre": "SUCRE",
        "id": "f2-Hy3oBVXS07smaVpce"
    },
    {
        "codigo": "08",
        "nombre": "ATLÁNTICO",
        "id": "gG-Hy3oBVXS07smaVpce"
    },
    {
        "codigo": "23",
        "nombre": "CÓRDOBA",
        "id": "e2-Hy3oBVXS07smaTpe-"
    },
    {
        "codigo": "27",
        "nombre": "CHOCÓ",
        "id": "fG-Hy3oBVXS07smaTpe-"
    },
    {
        "codigo": "99",
        "nombre": "VICHADA",
        "id": "1G-Hy3oBVXS07smanpdy"
    },
    {
        "codigo": "91",
        "nombre": "AMAZONAS",
        "id": "1W-Hy3oBVXS07smanpdy"
    },
    {
        "codigo": "54",
        "nombre": "NORTE DE SANTANDER",
        "id": "wG-Hy3oBVXS07smajZf9"
    },
    {
        "codigo": "68",
        "nombre": "SANTANDER",
        "id": "wW-Hy3oBVXS07smajZf9"
    },
    {
        "codigo": "95",
        "nombre": "GUAVIARE",
        "id": "2G-Hy3oBVXS07smappdh"
    },
    {
        "codigo": "25",
        "nombre": "CUNDINAMARCA",
        "id": "rG-Hy3oBVXS07smafpfE"
    },
    {
        "codigo": "66",
        "nombre": "RISARALDA",
        "id": "vG-Hy3oBVXS07smahpcm"
    },
    {
        "codigo": "73",
        "nombre": "TOLIMA",
        "id": "vW-Hy3oBVXS07smahpcm"
    },
    {
        "codigo": "52",
        "nombre": "NARIÑO",
        "id": "vm-Hy3oBVXS07smaiZfS"
    },
    {
        "codigo": "94",
        "nombre": "GUAINÍA",
        "id": "02-Hy3oBVXS07smamZcS"
    },
    {
        "codigo": "17",
        "nombre": "CALDAS",
        "id": "rW-Hy3oBVXS07smagpeC"
    },
    {
        "codigo": "63",
        "nombre": "QUINDIO",
        "id": "rm-Hy3oBVXS07smagpeC"
    },
    {
        "codigo": "86",
        "nombre": "PUTUMAYO",
        "id": "0m-Hy3oBVXS07smamZcS"
    },
    {
        "codigo": "85",
        "nombre": "CASANARE",
        "id": "wm-Hy3oBVXS07smak5ee"
    },
    {
        "codigo": "97",
        "nombre": "VAUPÉS",
        "id": "w2-Hy3oBVXS07smak5ee"
    },
    {
        "codigo": "88",
        "nombre": "ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA",
        "id": "2W-Hy3oBVXS07smaqpeL"
    },
    {
        "codigo": "81",
        "nombre": "ARAUCA",
        "id": "2m-Hy3oBVXS07smaqpeL"
    },
    {
        "codigo": "11",
        "nombre": "BOGOTÁ, D.C.",
        "id": "52-Hy3oBVXS07smarpe7"
    }
]