export const municipios = [{
    "dpto": "27",
    "codigo": "27745",
    "nombre": "SIPÍ",
    "id": "TG-Ey3oBVXS07smau5HB"
},
{
    "dpto": "27",
    "codigo": "27006",
    "nombre": "ACANDÍ",
    "id": "Im-Ey3oBVXS07smappGM"
},
{
    "dpto": "27",
    "codigo": "27800",
    "nombre": "UNGUÍA",
    "id": "Tm-Ey3oBVXS07smau5HB"
},
{
    "dpto": "27",
    "codigo": "27050",
    "nombre": "ATRATO",
    "id": "JG-Ey3oBVXS07smaqZHp"
},
{
    "dpto": "70",
    "codigo": "70001",
    "nombre": "SINCELEJO",
    "id": "UG-Ey3oBVXS07smau5HB"
},
{
    "dpto": "27",
    "codigo": "27075",
    "nombre": "BAHÍA SOLANO",
    "id": "Jm-Ey3oBVXS07smaqZHq"
},
{
    "dpto": "70",
    "codigo": "70124",
    "nombre": "CAIMITO",
    "id": "Um-Ey3oBVXS07smau5HB"
},
{
    "dpto": "27",
    "codigo": "27099",
    "nombre": "BOJAYÁ",
    "id": "KG-Ey3oBVXS07smarZFI"
},
{
    "dpto": "70",
    "codigo": "70215",
    "nombre": "COROZAL",
    "id": "VG-Ey3oBVXS07smav5Eb"
},
{
    "dpto": "27",
    "codigo": "27150",
    "nombre": "CARMEN DEL DARIÉN",
    "id": "Km-Ey3oBVXS07smarZFI"
},
{
    "dpto": "70",
    "codigo": "70230",
    "nombre": "CHALÁN",
    "id": "Vm-Ey3oBVXS07smav5Eb"
},
{
    "dpto": "27",
    "codigo": "27205",
    "nombre": "CONDOTO",
    "id": "LG-Ey3oBVXS07smarZFI"
},
{
    "dpto": "70",
    "codigo": "70235",
    "nombre": "GALERAS",
    "id": "WG-Ey3oBVXS07smav5Eb"
},
{
    "dpto": "23",
    "codigo": "23807",
    "nombre": "TIERRALTA",
    "id": "HW-Ey3oBVXS07smaopHY"
},
{
    "dpto": "70",
    "codigo": "70400",
    "nombre": "LA UNIÓN",
    "id": "Wm-Ey3oBVXS07smav5Eb"
},
{
    "dpto": "23",
    "codigo": "23672",
    "nombre": "SAN ANTERO",
    "id": "FG-Ey3oBVXS07sman5EK"
},
{
    "dpto": "70",
    "codigo": "70429",
    "nombre": "MAJAGUAL",
    "id": "XG-Ey3oBVXS07smav5Eb"
},
{
    "dpto": "23",
    "codigo": "23678",
    "nombre": "SAN CARLOS",
    "id": "Fm-Ey3oBVXS07sman5EK"
},
{
    "dpto": "23",
    "codigo": "23580",
    "nombre": "PUERTO LIBERTADOR",
    "id": "HG-Ey3oBVXS07smaopHY"
},
{
    "dpto": "23",
    "codigo": "23686",
    "nombre": "SAN PELAYO",
    "id": "GG-Ey3oBVXS07sman5EK"
},
{
    "dpto": "27",
    "codigo": "27615",
    "nombre": "RIOSUCIO",
    "id": "Hm-Ey3oBVXS07smaopHY"
},
{
    "dpto": "23",
    "codigo": "23855",
    "nombre": "VALENCIA",
    "id": "Gm-Ey3oBVXS07sman5EK"
},
{
    "dpto": "23",
    "codigo": "23675",
    "nombre": "SAN BERNARDO DEL VIENTO",
    "id": "FW-Ey3oBVXS07sman5EK"
},
{
    "dpto": "27",
    "codigo": "27450",
    "nombre": "MEDIO SAN JUAN",
    "id": "RG-Ey3oBVXS07smauJFk"
},
{
    "dpto": "23",
    "codigo": "23682",
    "nombre": "SAN JOSÉ DE URÉ",
    "id": "F2-Ey3oBVXS07sman5EK"
},
{
    "dpto": "27",
    "codigo": "27495",
    "nombre": "NUQUÍ",
    "id": "Rm-Ey3oBVXS07smauJFk"
},
{
    "dpto": "23",
    "codigo": "23815",
    "nombre": "TUCHÍN",
    "id": "GW-Ey3oBVXS07sman5EK"
},
{
    "dpto": "27",
    "codigo": "27600",
    "nombre": "RÍO QUITO",
    "id": "SG-Ey3oBVXS07smauJFk"
},
{
    "dpto": "23",
    "codigo": "23466",
    "nombre": "MONTELÍBANO",
    "id": "G2-Ey3oBVXS07sman5EK"
},
{
    "dpto": "05",
    "codigo": "05893",
    "nombre": "YONDÓ",
    "id": "72-Ey3oBVXS07smakZAS"
},
{
    "dpto": "27",
    "codigo": "27491",
    "nombre": "NÓVITA",
    "id": "RW-Ey3oBVXS07smauJFk"
},
{
    "dpto": "23",
    "codigo": "23001",
    "nombre": "MONTERÍA",
    "id": "8W-Ey3oBVXS07smakZAS"
},
{
    "dpto": "27",
    "codigo": "27580",
    "nombre": "RÍO IRÓ",
    "id": "R2-Ey3oBVXS07smauJFk"
},
{
    "dpto": "23",
    "codigo": "23079",
    "nombre": "BUENAVISTA",
    "id": "82-Ey3oBVXS07smakZAS"
},
{
    "dpto": "27",
    "codigo": "27660",
    "nombre": "SAN JOSÉ DEL PALMAR",
    "id": "SW-Ey3oBVXS07smauJFk"
},
{
    "dpto": "27",
    "codigo": "27413",
    "nombre": "LLORÓ",
    "id": "QW-Ey3oBVXS07smas5Hv"
},
{
    "dpto": "05",
    "codigo": "05895",
    "nombre": "ZARAGOZA",
    "id": "8G-Ey3oBVXS07smakZAS"
},
{
    "dpto": "27",
    "codigo": "27430",
    "nombre": "MEDIO BAUDÓ",
    "id": "Q2-Ey3oBVXS07smas5Hv"
},
{
    "dpto": "23",
    "codigo": "23068",
    "nombre": "AYAPEL",
    "id": "8m-Ey3oBVXS07smakZAS"
},
{
    "dpto": "23",
    "codigo": "23555",
    "nombre": "PLANETA RICA",
    "id": "AW-Ey3oBVXS07smam5Er"
},
{
    "dpto": "27",
    "codigo": "27372",
    "nombre": "JURADÓ",
    "id": "QG-Ey3oBVXS07smas5Hv"
},
{
    "dpto": "23",
    "codigo": "23574",
    "nombre": "PUERTO ESCONDIDO",
    "id": "A2-Ey3oBVXS07smam5Er"
},
{
    "dpto": "27",
    "codigo": "27425",
    "nombre": "MEDIO ATRATO",
    "id": "Qm-Ey3oBVXS07smas5Hv"
},
{
    "dpto": "23",
    "codigo": "23660",
    "nombre": "SAHAGÚN",
    "id": "BW-Ey3oBVXS07smam5Er"
},
{
    "dpto": "23",
    "codigo": "23500",
    "nombre": "MOÑITOS",
    "id": "AG-Ey3oBVXS07smam5Er"
},
{
    "dpto": "23",
    "codigo": "23090",
    "nombre": "CANALETE",
    "id": "9m-Ey3oBVXS07smal5CI"
},
{
    "dpto": "23",
    "codigo": "23570",
    "nombre": "PUEBLO NUEVO",
    "id": "Am-Ey3oBVXS07smam5Er"
},
{
    "dpto": "23",
    "codigo": "23168",
    "nombre": "CHIMÁ",
    "id": "-G-Ey3oBVXS07smal5CI"
},
{
    "dpto": "23",
    "codigo": "23586",
    "nombre": "PURÍSIMA DE LA CONCEPCIÓN",
    "id": "BG-Ey3oBVXS07smam5Er"
},
{
    "dpto": "23",
    "codigo": "23189",
    "nombre": "CIÉNAGA DE ORO",
    "id": "-m-Ey3oBVXS07smal5CI"
},
{
    "dpto": "23",
    "codigo": "23670",
    "nombre": "SAN ANDRÉS DE SOTAVENTO",
    "id": "Bm-Ey3oBVXS07smam5Er"
},
{
    "dpto": "23",
    "codigo": "23350",
    "nombre": "LA APARTADA",
    "id": "_G-Ey3oBVXS07smal5CI"
},
{
    "dpto": "23",
    "codigo": "23162",
    "nombre": "CERETÉ",
    "id": "92-Ey3oBVXS07smal5CI"
},
{
    "dpto": "23",
    "codigo": "23419",
    "nombre": "LOS CÓRDOBAS",
    "id": "_m-Ey3oBVXS07smal5CI"
},
{
    "dpto": "23",
    "codigo": "23182",
    "nombre": "CHINÚ",
    "id": "-W-Ey3oBVXS07smal5CI"
},
{
    "dpto": "27",
    "codigo": "27245",
    "nombre": "EL CARMEN DE ATRATO",
    "id": "LW-Ey3oBVXS07smasJGK"
},
{
    "dpto": "23",
    "codigo": "23300",
    "nombre": "COTORRA",
    "id": "-2-Ey3oBVXS07smal5CI"
},
{
    "dpto": "27",
    "codigo": "27361",
    "nombre": "ISTMINA",
    "id": "L2-Ey3oBVXS07smasJGK"
},
{
    "dpto": "23",
    "codigo": "23417",
    "nombre": "LORICA",
    "id": "_W-Ey3oBVXS07smal5CI"
},
{
    "dpto": "27",
    "codigo": "27787",
    "nombre": "TADÓ",
    "id": "TW-Ey3oBVXS07smau5HB"
},
{
    "dpto": "23",
    "codigo": "23464",
    "nombre": "MOMIL",
    "id": "_2-Ey3oBVXS07smal5CI"
},
{
    "dpto": "27",
    "codigo": "27810",
    "nombre": "UNIÓN PANAMERICANA",
    "id": "T2-Ey3oBVXS07smau5HB"
},
{
    "dpto": "27",
    "codigo": "27250",
    "nombre": "EL LITORAL DEL SAN JUAN",
    "id": "Lm-Ey3oBVXS07smasJGK"
},
{
    "dpto": "70",
    "codigo": "70110",
    "nombre": "BUENAVISTA",
    "id": "UW-Ey3oBVXS07smau5HB"
},
{
    "dpto": "27",
    "codigo": "27099",
    "nombre": "BOJAYÁ",
    "id": "KG-Ey3oBVXS07smarZFI"
},
{
    "dpto": "70",
    "codigo": "70204",
    "nombre": "COLOSÓ",
    "id": "U2-Ey3oBVXS07smau5HB"
},
{
    "dpto": "27",
    "codigo": "27150",
    "nombre": "CARMEN DEL DARIÉN",
    "id": "Km-Ey3oBVXS07smarZFI"
},
{
    "dpto": "70",
    "codigo": "70221",
    "nombre": "COVEÑAS",
    "id": "VW-Ey3oBVXS07smav5Eb"
},
{
    "dpto": "27",
    "codigo": "27205",
    "nombre": "CONDOTO",
    "id": "LG-Ey3oBVXS07smarZFI"
},
{
    "dpto": "70",
    "codigo": "70233",
    "nombre": "EL ROBLE",
    "id": "V2-Ey3oBVXS07smav5Eb"
},
{
    "dpto": "27",
    "codigo": "27006",
    "nombre": "ACANDÍ",
    "id": "Im-Ey3oBVXS07smappGM"
},
{
    "dpto": "70",
    "codigo": "70265",
    "nombre": "GUARANDA",
    "id": "WW-Ey3oBVXS07smav5Eb"
},
{
    "dpto": "27",
    "codigo": "27050",
    "nombre": "ATRATO",
    "id": "JG-Ey3oBVXS07smaqZHp"
},
{
    "dpto": "70",
    "codigo": "70418",
    "nombre": "LOS PALMITOS",
    "id": "W2-Ey3oBVXS07smav5Eb"
},
{
    "dpto": "27",
    "codigo": "27075",
    "nombre": "BAHÍA SOLANO",
    "id": "Jm-Ey3oBVXS07smaqZHq"
},
{
    "dpto": "70",
    "codigo": "70508",
    "nombre": "OVEJAS",
    "id": "XW-Ey3oBVXS07smav5Eb"
},
{
    "dpto": "08",
    "codigo": "08549",
    "nombre": "PIOJÓ",
    "id": "gW-Ey3oBVXS07smaypE-"
},
{
    "dpto": "08",
    "codigo": "08558",
    "nombre": "POLONUEVO",
    "id": "gm-Ey3oBVXS07smaypE-"
},
{
    "dpto": "08",
    "codigo": "08560",
    "nombre": "PONEDERA",
    "id": "g2-Ey3oBVXS07smaypE-"
},
{
    "dpto": "08",
    "codigo": "08573",
    "nombre": "PUERTO COLOMBIA",
    "id": "hG-Ey3oBVXS07smaypE-"
},
{
    "dpto": "08",
    "codigo": "08606",
    "nombre": "REPELÓN",
    "id": "hW-Ey3oBVXS07smaypE-"
},
{
    "dpto": "08",
    "codigo": "08634",
    "nombre": "SABANAGRANDE",
    "id": "hm-Ey3oBVXS07smaypE-"
},
{
    "dpto": "08",
    "codigo": "08638",
    "nombre": "SABANALARGA",
    "id": "h2-Ey3oBVXS07smaypE-"
},
{
    "dpto": "08",
    "codigo": "08675",
    "nombre": "SANTA LUCÍA",
    "id": "iG-Ey3oBVXS07smaypE-"
},
{
    "dpto": "08",
    "codigo": "08685",
    "nombre": "SANTO TOMÁS",
    "id": "iW-Ey3oBVXS07smaypE-"
},
{
    "dpto": "08",
    "codigo": "08758",
    "nombre": "SOLEDAD",
    "id": "im-Ey3oBVXS07smaypE-"
},
{
    "dpto": "08",
    "codigo": "08770",
    "nombre": "SUAN",
    "id": "i2-Ey3oBVXS07smaypE-"
},
{
    "dpto": "08",
    "codigo": "08832",
    "nombre": "TUBARÁ",
    "id": "jG-Ey3oBVXS07smaypE-"
},
{
    "dpto": "08",
    "codigo": "08849",
    "nombre": "USIACURÍ",
    "id": "jW-Ey3oBVXS07smaypE-"
},
{
    "dpto": "13",
    "codigo": "13001",
    "nombre": "CARTAGENA DE INDIAS",
    "id": "jm-Ey3oBVXS07smaypE-"
},
{
    "dpto": "13",
    "codigo": "13160",
    "nombre": "CANTAGALLO",
    "id": "pW-Ey3oBVXS07sma2pFW"
},
{
    "dpto": "13",
    "codigo": "13188",
    "nombre": "CICUCO",
    "id": "pm-Ey3oBVXS07sma2pFW"
},
{
    "dpto": "13",
    "codigo": "13212",
    "nombre": "CÓRDOBA",
    "id": "p2-Ey3oBVXS07sma2pFW"
},
{
    "dpto": "13",
    "codigo": "13222",
    "nombre": "CLEMENCIA",
    "id": "qG-Ey3oBVXS07sma2pFW"
},
{
    "dpto": "13",
    "codigo": "13244",
    "nombre": "EL CARMEN DE BOLÍVAR",
    "id": "qW-Ey3oBVXS07sma2pFW"
},
{
    "dpto": "13",
    "codigo": "13248",
    "nombre": "EL GUAMO",
    "id": "qm-Ey3oBVXS07sma2pFW"
},
{
    "dpto": "13",
    "codigo": "13268",
    "nombre": "EL PEÑÓN",
    "id": "q2-Ey3oBVXS07sma2pFW"
},
{
    "dpto": "13",
    "codigo": "13433",
    "nombre": "MAHATES",
    "id": "rG-Ey3oBVXS07sma2pFW"
},
{
    "dpto": "47",
    "codigo": "47161",
    "nombre": "CERRO DE SAN ANTONIO",
    "id": "3G-Ey3oBVXS07sma75HI"
},
{
    "dpto": "47",
    "codigo": "47170",
    "nombre": "CHIVOLO",
    "id": "3W-Ey3oBVXS07sma75HI"
},
{
    "dpto": "47",
    "codigo": "47205",
    "nombre": "CONCORDIA",
    "id": "3m-Ey3oBVXS07sma75HI"
},
{
    "dpto": "47",
    "codigo": "47258",
    "nombre": "EL PIÑÓN",
    "id": "32-Ey3oBVXS07sma75HI"
},
{
    "dpto": "47",
    "codigo": "47268",
    "nombre": "EL RETÉN",
    "id": "4G-Ey3oBVXS07sma75HI"
},
{
    "dpto": "47",
    "codigo": "47318",
    "nombre": "GUAMAL",
    "id": "4W-Ey3oBVXS07sma75HI"
},
{
    "dpto": "47",
    "codigo": "47460",
    "nombre": "NUEVA GRANADA",
    "id": "4m-Ey3oBVXS07sma75HI"
},
{
    "dpto": "47",
    "codigo": "47545",
    "nombre": "PIJIÑO DEL CARMEN",
    "id": "42-Ey3oBVXS07sma75HI"
},
{
    "dpto": "13",
    "codigo": "13440",
    "nombre": "MARGARITA",
    "id": "rW-Ey3oBVXS07sma3ZG8"
},
{
    "dpto": "13",
    "codigo": "13442",
    "nombre": "MARÍA LA BAJA",
    "id": "rm-Ey3oBVXS07sma3ZG8"
},
{
    "dpto": "13",
    "codigo": "13458",
    "nombre": "MONTECRISTO",
    "id": "r2-Ey3oBVXS07sma3ZG8"
},
{
    "dpto": "13",
    "codigo": "13468",
    "nombre": "MOMPÓS",
    "id": "sG-Ey3oBVXS07sma3ZG8"
},
{
    "dpto": "13",
    "codigo": "13473",
    "nombre": "MORALES",
    "id": "sW-Ey3oBVXS07sma3ZG8"
},
{
    "dpto": "13",
    "codigo": "13490",
    "nombre": "NOROSÍ",
    "id": "sm-Ey3oBVXS07sma3ZG8"
},
{
    "dpto": "13",
    "codigo": "13549",
    "nombre": "PINILLOS",
    "id": "s2-Ey3oBVXS07sma3ZG8"
},
{
    "dpto": "13",
    "codigo": "13744",
    "nombre": "SIMITÍ",
    "id": "wm-Ey3oBVXS07sma55Hq"
},
{
    "dpto": "13",
    "codigo": "13760",
    "nombre": "SOPLAVIENTO",
    "id": "w2-Ey3oBVXS07sma55Hq"
},
{
    "dpto": "13",
    "codigo": "13780",
    "nombre": "TALAIGUA NUEVO",
    "id": "xG-Ey3oBVXS07sma55Hq"
},
{
    "dpto": "13",
    "codigo": "13810",
    "nombre": "TIQUISIO",
    "id": "xW-Ey3oBVXS07sma55Hq"
},
{
    "dpto": "13",
    "codigo": "13836",
    "nombre": "TURBACO",
    "id": "xm-Ey3oBVXS07sma55Hq"
},
{
    "dpto": "13",
    "codigo": "13838",
    "nombre": "TURBANÁ",
    "id": "x2-Ey3oBVXS07sma55Hq"
},
{
    "dpto": "13",
    "codigo": "13873",
    "nombre": "VILLANUEVA",
    "id": "yG-Ey3oBVXS07sma55Hq"
},
{
    "dpto": "13",
    "codigo": "13894",
    "nombre": "ZAMBRANO",
    "id": "yW-Ey3oBVXS07sma55Hq"
},
{
    "dpto": "13",
    "codigo": "13300",
    "nombre": "HATILLO DE LOBA",
    "id": "ym-Ey3oBVXS07sma55Hq"
},
{
    "dpto": "13",
    "codigo": "13650",
    "nombre": "SAN FERNANDO",
    "id": "12-Ey3oBVXS07sma7JEq"
},
{
    "dpto": "13",
    "codigo": "13430",
    "nombre": "MAGANGUÉ",
    "id": "2G-Ey3oBVXS07sma7JEq"
},
{
    "dpto": "47",
    "codigo": "47541",
    "nombre": "PEDRAZA",
    "id": "2W-Ey3oBVXS07sma7JEq"
},
{
    "dpto": "47",
    "codigo": "47030",
    "nombre": "ALGARROBO",
    "id": "2m-Ey3oBVXS07sma7JEq"
},
{
    "dpto": "47",
    "codigo": "47058",
    "nombre": "ARIGUANÍ",
    "id": "22-Ey3oBVXS07sma7JEq"
},
{
    "dpto": "70",
    "codigo": "70523",
    "nombre": "PALMITO",
    "id": "Xm-Ey3oBVXS07smawpGW"
},
{
    "dpto": "70",
    "codigo": "70670",
    "nombre": "SAMPUÉS",
    "id": "X2-Ey3oBVXS07smawpGW"
},
{
    "dpto": "70",
    "codigo": "70678",
    "nombre": "SAN BENITO ABAD",
    "id": "YG-Ey3oBVXS07smawpGW"
},
{
    "dpto": "70",
    "codigo": "70702",
    "nombre": "SAN JUAN DE BETULIA",
    "id": "YW-Ey3oBVXS07smawpGW"
},
{
    "dpto": "70",
    "codigo": "70708",
    "nombre": "SAN MARCOS",
    "id": "Ym-Ey3oBVXS07smawpGW"
},
{
    "dpto": "70",
    "codigo": "70713",
    "nombre": "SAN ONOFRE",
    "id": "Y2-Ey3oBVXS07smawpGW"
},
{
    "dpto": "70",
    "codigo": "70717",
    "nombre": "SAN PEDRO",
    "id": "ZG-Ey3oBVXS07smawpGW"
},
{
    "dpto": "70",
    "codigo": "70742",
    "nombre": "SAN LUIS DE SINCÉ",
    "id": "ZW-Ey3oBVXS07smawpGW"
},
{
    "dpto": "70",
    "codigo": "70771",
    "nombre": "SUCRE",
    "id": "Zm-Ey3oBVXS07smawpGW"
},
{
    "dpto": "70",
    "codigo": "70820",
    "nombre": "SANTIAGO DE TOLÚ",
    "id": "Z2-Ey3oBVXS07smawpGW"
},
{
    "dpto": "13",
    "codigo": "13006",
    "nombre": "ACHÍ",
    "id": "kG-Ey3oBVXS07sma0pHp"
},
{
    "dpto": "13",
    "codigo": "13030",
    "nombre": "ALTOS DEL ROSARIO",
    "id": "kW-Ey3oBVXS07sma0pHp"
},
{
    "dpto": "13",
    "codigo": "13042",
    "nombre": "ARENAL",
    "id": "km-Ey3oBVXS07sma0pHp"
},
{
    "dpto": "13",
    "codigo": "13052",
    "nombre": "ARJONA",
    "id": "k2-Ey3oBVXS07sma0pHp"
},
{
    "dpto": "13",
    "codigo": "13062",
    "nombre": "ARROYOHONDO",
    "id": "lG-Ey3oBVXS07sma0pHp"
},
{
    "dpto": "13",
    "codigo": "13074",
    "nombre": "BARRANCO DE LOBA",
    "id": "lW-Ey3oBVXS07sma0pHp"
},
{
    "dpto": "13",
    "codigo": "13140",
    "nombre": "CALAMAR",
    "id": "lm-Ey3oBVXS07sma0pHp"
},
{
    "dpto": "70",
    "codigo": "70823",
    "nombre": "SAN JOSÉ DE TOLUVIEJO",
    "id": "dW-Ey3oBVXS07smaxZGZ"
},
{
    "dpto": "70",
    "codigo": "70473",
    "nombre": "MORROA",
    "id": "dm-Ey3oBVXS07smaxZGZ"
},
{
    "dpto": "08",
    "codigo": "08001",
    "nombre": "BARRANQUILLA",
    "id": "d2-Ey3oBVXS07smaxZGZ"
},
{
    "dpto": "08",
    "codigo": "08078",
    "nombre": "BARANOA",
    "id": "eG-Ey3oBVXS07smaxZGZ"
},
{
    "dpto": "08",
    "codigo": "08137",
    "nombre": "CAMPO DE LA CRUZ",
    "id": "eW-Ey3oBVXS07smaxZGZ"
},
{
    "dpto": "08",
    "codigo": "08141",
    "nombre": "CANDELARIA",
    "id": "em-Ey3oBVXS07smaxZGZ"
},
{
    "dpto": "08",
    "codigo": "08296",
    "nombre": "GALAPA",
    "id": "e2-Ey3oBVXS07smaxZGZ"
},
{
    "dpto": "08",
    "codigo": "08372",
    "nombre": "JUAN DE ACOSTA",
    "id": "fG-Ey3oBVXS07smaxZGZ"
},
{
    "dpto": "08",
    "codigo": "08421",
    "nombre": "LURUACO",
    "id": "fW-Ey3oBVXS07smaxZGZ"
},
{
    "dpto": "08",
    "codigo": "08433",
    "nombre": "MALAMBO",
    "id": "fm-Ey3oBVXS07smaxZGZ"
},
{
    "dpto": "08",
    "codigo": "08436",
    "nombre": "MANATÍ",
    "id": "f2-Ey3oBVXS07smaxZGZ"
},
{
    "dpto": "08",
    "codigo": "08520",
    "nombre": "PALMAR DE VARELA",
    "id": "gG-Ey3oBVXS07smaxZGZ"
},
{
    "dpto": "20",
    "codigo": "20060",
    "nombre": "BOSCONIA",
    "id": "y2-Fy3oBVXS07smazJNa"
},
{
    "dpto": "13",
    "codigo": "13600",
    "nombre": "RÍO VIEJO",
    "id": "t2-Ey3oBVXS07sma4ZEz"
},
{
    "dpto": "20",
    "codigo": "20178",
    "nombre": "CHIRIGUANÁ",
    "id": "zW-Fy3oBVXS07smazJNa"
},
{
    "dpto": "13",
    "codigo": "13647",
    "nombre": "SAN ESTANISLAO",
    "id": "uW-Ey3oBVXS07sma4ZEz"
},
{
    "dpto": "20",
    "codigo": "20238",
    "nombre": "EL COPEY",
    "id": "z2-Fy3oBVXS07smaz5Pb"
},
{
    "dpto": "13",
    "codigo": "13655",
    "nombre": "SAN JACINTO DEL CAUCA",
    "id": "u2-Ey3oBVXS07sma4ZEz"
},
{
    "dpto": "20",
    "codigo": "20295",
    "nombre": "GAMARRA",
    "id": "0W-Fy3oBVXS07smaz5Pb"
},
{
    "dpto": "13",
    "codigo": "13667",
    "nombre": "SAN MARTÍN DE LOBA",
    "id": "vW-Ey3oBVXS07sma5JGw"
},
{
    "dpto": "20",
    "codigo": "20383",
    "nombre": "LA GLORIA",
    "id": "02-Fy3oBVXS07smaz5Pb"
},
{
    "dpto": "13",
    "codigo": "13673",
    "nombre": "SANTA CATALINA",
    "id": "v2-Ey3oBVXS07sma5JGw"
},
{
    "dpto": "13",
    "codigo": "13580",
    "nombre": "REGIDOR",
    "id": "tm-Ey3oBVXS07sma4ZEz"
},
{
    "dpto": "13",
    "codigo": "13688",
    "nombre": "SANTA ROSA DEL SUR",
    "id": "wW-Ey3oBVXS07sma5JGw"
},
{
    "dpto": "13",
    "codigo": "13620",
    "nombre": "SAN CRISTÓBAL",
    "id": "uG-Ey3oBVXS07sma4ZEz"
},
{
    "dpto": "47",
    "codigo": "47555",
    "nombre": "PLATO",
    "id": "5W-Ey3oBVXS07sma85Ew"
},
{
    "dpto": "13",
    "codigo": "13654",
    "nombre": "SAN JACINTO",
    "id": "um-Ey3oBVXS07sma4ZEz"
},
{
    "dpto": "47",
    "codigo": "47605",
    "nombre": "REMOLINO",
    "id": "6G-Ey3oBVXS07sma9pG_"
},
{
    "dpto": "13",
    "codigo": "13657",
    "nombre": "SAN JUAN NEPOMUCENO",
    "id": "vG-Ey3oBVXS07sma4ZEz"
},
{
    "dpto": "47",
    "codigo": "47675",
    "nombre": "SALAMINA",
    "id": "6m-Ey3oBVXS07sma9pG_"
},
{
    "dpto": "13",
    "codigo": "13670",
    "nombre": "SAN PABLO",
    "id": "vm-Ey3oBVXS07sma5JGw"
},
{
    "dpto": "47",
    "codigo": "47703",
    "nombre": "SAN ZENÓN",
    "id": "7G-Ey3oBVXS07sma9pG_"
},
{
    "dpto": "13",
    "codigo": "13683",
    "nombre": "SANTA ROSA",
    "id": "wG-Ey3oBVXS07sma5JGw"
},
{
    "dpto": "05",
    "codigo": "05002",
    "nombre": "ABEJORRAL",
    "id": "7m-Ey3oBVXS07sma-pEB"
},
{
    "dpto": "25",
    "codigo": "25867",
    "nombre": "VIANÍ",
    "id": "fm-Gy3oBVXS07smaDpRx"
},
{
    "dpto": "05",
    "codigo": "05021",
    "nombre": "ALEJANDRÍA",
    "id": "8G-Ey3oBVXS07sma-pEB"
},
{
    "dpto": "25",
    "codigo": "25873",
    "nombre": "VILLAPINZÓN",
    "id": "gG-Gy3oBVXS07smaDpRx"
},
{
    "dpto": "05",
    "codigo": "05031",
    "nombre": "AMALFI",
    "id": "8m-Ey3oBVXS07sma-pEB"
},
{
    "dpto": "25",
    "codigo": "25878",
    "nombre": "VIOTÁ",
    "id": "gm-Gy3oBVXS07smaDpRx"
},
{
    "dpto": "05",
    "codigo": "05036",
    "nombre": "ANGELÓPOLIS",
    "id": "9G-Ey3oBVXS07sma-pEB"
},
{
    "dpto": "25",
    "codigo": "25898",
    "nombre": "ZIPACÓN",
    "id": "hG-Gy3oBVXS07smaDpRx"
},
{
    "dpto": "05",
    "codigo": "05040",
    "nombre": "ANORÍ",
    "id": "BG-Fy3oBVXS07smaAJKR"
},
{
    "dpto": "15",
    "codigo": "15764",
    "nombre": "SORACÁ",
    "id": "Om-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "05",
    "codigo": "05044",
    "nombre": "ANZÁ",
    "id": "Bm-Fy3oBVXS07smaAJKR"
},
{
    "dpto": "15",
    "codigo": "15776",
    "nombre": "SUTAMARCHÁN",
    "id": "PG-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "05",
    "codigo": "05051",
    "nombre": "ARBOLETES",
    "id": "CG-Fy3oBVXS07smaAJKR"
},
{
    "dpto": "15",
    "codigo": "15790",
    "nombre": "TASCO",
    "id": "Pm-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "05",
    "codigo": "05059",
    "nombre": "ARMENIA",
    "id": "Cm-Fy3oBVXS07smaA5LP"
},
{
    "dpto": "15",
    "codigo": "15804",
    "nombre": "TIBANÁ",
    "id": "QG-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "05",
    "codigo": "05086",
    "nombre": "BELMIRA",
    "id": "DG-Fy3oBVXS07smaA5LP"
},
{
    "dpto": "15",
    "codigo": "15808",
    "nombre": "TINJACÁ",
    "id": "Qm-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "05",
    "codigo": "05091",
    "nombre": "BETANIA",
    "id": "Dm-Fy3oBVXS07smaA5LP"
},
{
    "dpto": "15",
    "codigo": "15814",
    "nombre": "TOCA",
    "id": "RG-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "05",
    "codigo": "05101",
    "nombre": "CIUDAD BOLÍVAR",
    "id": "EG-Fy3oBVXS07smaB5Ik"
},
{
    "dpto": "15",
    "codigo": "15820",
    "nombre": "TÓPAGA",
    "id": "Rm-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "05",
    "codigo": "05113",
    "nombre": "BURITICÁ",
    "id": "Em-Fy3oBVXS07smaB5Ik"
},
{
    "dpto": "15",
    "codigo": "15832",
    "nombre": "TUNUNGUÁ",
    "id": "SG-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "05",
    "codigo": "05125",
    "nombre": "CAICEDO",
    "id": "FG-Fy3oBVXS07smaB5Ik"
},
{
    "dpto": "15",
    "codigo": "15837",
    "nombre": "TUTA",
    "id": "Sm-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "05",
    "codigo": "05134",
    "nombre": "CAMPAMENTO",
    "id": "Fm-Fy3oBVXS07smaB5Ik"
},
{
    "dpto": "15",
    "codigo": "15646",
    "nombre": "SAMACÁ",
    "id": "KW-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "05",
    "codigo": "05142",
    "nombre": "CARACOLÍ",
    "id": "GG-Fy3oBVXS07smaB5Ik"
},
{
    "dpto": "15",
    "codigo": "15664",
    "nombre": "SAN JOSÉ DE PARE",
    "id": "K2-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "05",
    "codigo": "05147",
    "nombre": "CAREPA",
    "id": "HG-Fy3oBVXS07smaC5I6"
},
{
    "dpto": "15",
    "codigo": "15673",
    "nombre": "SAN MATEO",
    "id": "LW-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "05",
    "codigo": "05150",
    "nombre": "CAROLINA",
    "id": "Hm-Fy3oBVXS07smaC5I6"
},
{
    "dpto": "15",
    "codigo": "15681",
    "nombre": "SAN PABLO DE BORBUR",
    "id": "L2-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "05",
    "codigo": "05172",
    "nombre": "CHIGORODÓ",
    "id": "IG-Fy3oBVXS07smaC5I6"
},
{
    "dpto": "15",
    "codigo": "15690",
    "nombre": "SANTA MARÍA",
    "id": "MW-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "05",
    "codigo": "05197",
    "nombre": "COCORNÁ",
    "id": "Im-Fy3oBVXS07smaDpLJ"
},
{
    "dpto": "15",
    "codigo": "15696",
    "nombre": "SANTA SOFÍA",
    "id": "M2-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "05",
    "codigo": "05209",
    "nombre": "CONCORDIA",
    "id": "JG-Fy3oBVXS07smaDpLJ"
},
{
    "dpto": "15",
    "codigo": "15723",
    "nombre": "SATIVASUR",
    "id": "NW-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "05",
    "codigo": "05234",
    "nombre": "DABEIBA",
    "id": "Jm-Fy3oBVXS07smaDpLJ"
},
{
    "dpto": "15",
    "codigo": "15753",
    "nombre": "SOATÁ",
    "id": "N2-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "05",
    "codigo": "05240",
    "nombre": "EBÉJICO",
    "id": "KG-Fy3oBVXS07smaEpIV"
},
{
    "dpto": "15",
    "codigo": "15763",
    "nombre": "SOTAQUIRÁ",
    "id": "OW-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "05",
    "codigo": "05264",
    "nombre": "ENTRERRÍOS",
    "id": "Km-Fy3oBVXS07smaEpIV"
},
{
    "dpto": "25",
    "codigo": "25839",
    "nombre": "UBALÁ",
    "id": "eG-Gy3oBVXS07smaCZSk"
},
{
    "dpto": "05",
    "codigo": "05282",
    "nombre": "FREDONIA",
    "id": "LG-Fy3oBVXS07smaEpIV"
},
{
    "dpto": "25",
    "codigo": "25843",
    "nombre": "VILLA DE SAN DIEGO DE UBATÉ",
    "id": "em-Gy3oBVXS07smaCZSk"
},
{
    "dpto": "05",
    "codigo": "05306",
    "nombre": "GIRALDO",
    "id": "Lm-Fy3oBVXS07smaEpIV"
},
{
    "dpto": "25",
    "codigo": "25851",
    "nombre": "ÚTICA",
    "id": "fG-Gy3oBVXS07smaCZSk"
},
{
    "dpto": "05",
    "codigo": "05310",
    "nombre": "GÓMEZ PLATA",
    "id": "PW-Fy3oBVXS07smaGZJZ"
},
{
    "dpto": "25",
    "codigo": "25001",
    "nombre": "AGUA DE DIOS",
    "id": "k2-Gy3oBVXS07smaE5Q3"
},
{
    "dpto": "05",
    "codigo": "05315",
    "nombre": "GUADALUPE",
    "id": "P2-Fy3oBVXS07smaGZJZ"
},
{
    "dpto": "25",
    "codigo": "25754",
    "nombre": "SOACHA",
    "id": "lW-Gy3oBVXS07smaE5Q3"
},
{
    "dpto": "05",
    "codigo": "05321",
    "nombre": "GUATAPÉ",
    "id": "QW-Fy3oBVXS07smaGZJZ"
},
{
    "dpto": "25",
    "codigo": "25260",
    "nombre": "EL ROSAL",
    "id": "l2-Gy3oBVXS07smaE5Q3"
},
{
    "dpto": "05",
    "codigo": "05353",
    "nombre": "HISPANIA",
    "id": "Q2-Fy3oBVXS07smaGZJZ"
},
{
    "dpto": "25",
    "codigo": "25740",
    "nombre": "SIBATÉ",
    "id": "mW-Gy3oBVXS07smaE5Q3"
},
{
    "dpto": "05",
    "codigo": "05361",
    "nombre": "ITUANGO",
    "id": "Rm-Fy3oBVXS07smaHZIL"
},
{
    "dpto": "25",
    "codigo": "25662",
    "nombre": "SAN JUAN DE RIOSECO",
    "id": "m2-Gy3oBVXS07smaE5Q3"
},
{
    "dpto": "05",
    "codigo": "05368",
    "nombre": "JERICÓ",
    "id": "SG-Fy3oBVXS07smaHZIL"
},
{
    "dpto": "15",
    "codigo": "15516",
    "nombre": "PAIPA",
    "id": "HG-Fy3oBVXS07sma8ZRa"
},
{
    "dpto": "05",
    "codigo": "05380",
    "nombre": "LA ESTRELLA",
    "id": "Sm-Fy3oBVXS07smaHZIL"
},
{
    "dpto": "15",
    "codigo": "15522",
    "nombre": "PANQUEBA",
    "id": "Hm-Fy3oBVXS07sma8ZRa"
},
{
    "dpto": "05",
    "codigo": "05400",
    "nombre": "LA UNIÓN",
    "id": "TG-Fy3oBVXS07smaHZIL"
},
{
    "dpto": "15",
    "codigo": "15533",
    "nombre": "PAYA",
    "id": "IG-Fy3oBVXS07sma8ZRa"
},
{
    "dpto": "05",
    "codigo": "05425",
    "nombre": "MACEO",
    "id": "Tm-Fy3oBVXS07smaHZIL"
},
{
    "dpto": "15",
    "codigo": "15599",
    "nombre": "RAMIRIQUÍ",
    "id": "Im-Fy3oBVXS07sma8ZRa"
},
{
    "dpto": "05",
    "codigo": "05467",
    "nombre": "MONTEBELLO",
    "id": "UG-Fy3oBVXS07smaJJJS"
},
{
    "dpto": "15",
    "codigo": "15621",
    "nombre": "RONDÓN",
    "id": "JG-Fy3oBVXS07sma8ZRa"
},
{
    "dpto": "05",
    "codigo": "05480",
    "nombre": "MUTATÁ",
    "id": "Um-Fy3oBVXS07smaJJJS"
},
{
    "dpto": "15",
    "codigo": "15638",
    "nombre": "SÁCHICA",
    "id": "Jm-Fy3oBVXS07sma8ZRa"
},
{
    "dpto": "05",
    "codigo": "05490",
    "nombre": "NECOCLÍ",
    "id": "VG-Fy3oBVXS07smaJJJS"
},
{
    "dpto": "25",
    "codigo": "25779",
    "nombre": "SUSA",
    "id": "bW-Gy3oBVXS07smaBpQz"
},
{
    "dpto": "05",
    "codigo": "05501",
    "nombre": "OLAYA",
    "id": "Y2-Fy3oBVXS07smaKJK5"
},
{
    "dpto": "25",
    "codigo": "25785",
    "nombre": "TABIO",
    "id": "b2-Gy3oBVXS07smaBpQz"
},
{
    "dpto": "05",
    "codigo": "05543",
    "nombre": "PEQUE",
    "id": "ZW-Fy3oBVXS07smaKJK5"
},
{
    "dpto": "25",
    "codigo": "25797",
    "nombre": "TENA",
    "id": "cW-Gy3oBVXS07smaBpQz"
},
{
    "dpto": "05",
    "codigo": "05579",
    "nombre": "PUERTO BERRÍO",
    "id": "Z2-Fy3oBVXS07smaKJK5"
},
{
    "dpto": "25",
    "codigo": "25805",
    "nombre": "TIBACUY",
    "id": "c2-Gy3oBVXS07smaBpQz"
},
{
    "dpto": "05",
    "codigo": "05591",
    "nombre": "PUERTO TRIUNFO",
    "id": "aW-Fy3oBVXS07smaKJK5"
},
{
    "dpto": "25",
    "codigo": "25815",
    "nombre": "TOCAIMA",
    "id": "dW-Gy3oBVXS07smaBpQz"
},
{
    "dpto": "05",
    "codigo": "05607",
    "nombre": "RETIRO",
    "id": "bW-Fy3oBVXS07smaL5Kn"
},
{
    "dpto": "25",
    "codigo": "25095",
    "nombre": "BITUIMA",
    "id": "YG-Gy3oBVXS07smaApQO"
},
{
    "dpto": "05",
    "codigo": "05628",
    "nombre": "SABANALARGA",
    "id": "b2-Fy3oBVXS07smaL5Kn"
},
{
    "dpto": "25",
    "codigo": "25120",
    "nombre": "CABRERA",
    "id": "Ym-Gy3oBVXS07smaApQO"
},
{
    "dpto": "05",
    "codigo": "05642",
    "nombre": "SALGAR",
    "id": "cW-Fy3oBVXS07smaL5Kn"
},
{
    "dpto": "25",
    "codigo": "25718",
    "nombre": "SASAIMA",
    "id": "ZG-Gy3oBVXS07smaApQO"
},
{
    "dpto": "05",
    "codigo": "05649",
    "nombre": "SAN CARLOS",
    "id": "c2-Fy3oBVXS07smaL5Kn"
},
{
    "dpto": "25",
    "codigo": "25743",
    "nombre": "SILVANIA",
    "id": "Zm-Gy3oBVXS07smaApQO"
},
{
    "dpto": "05",
    "codigo": "05656",
    "nombre": "SAN JERÓNIMO",
    "id": "dW-Fy3oBVXS07smaL5Kn"
},
{
    "dpto": "25",
    "codigo": "25758",
    "nombre": "SOPÓ",
    "id": "aG-Gy3oBVXS07smaApQO"
},
{
    "dpto": "05",
    "codigo": "05659",
    "nombre": "SAN JUAN DE URABÁ",
    "id": "d2-Fy3oBVXS07smaM5Ju"
},
{
    "dpto": "25",
    "codigo": "25772",
    "nombre": "SUESCA",
    "id": "am-Gy3oBVXS07smaApQO"
},
{
    "dpto": "05",
    "codigo": "05664",
    "nombre": "SAN PEDRO DE LOS MILAGROS",
    "id": "eW-Fy3oBVXS07smaM5Ju"
},
{
    "dpto": "44",
    "codigo": "44560",
    "nombre": "MANAURE",
    "id": "C2-Fy3oBVXS07sma65QP"
},
{
    "dpto": "05",
    "codigo": "05667",
    "nombre": "SAN RAFAEL",
    "id": "e2-Fy3oBVXS07smaM5Ju"
},
{
    "dpto": "19",
    "codigo": "19022",
    "nombre": "ALMAGUER",
    "id": "DW-Fy3oBVXS07sma65QP"
},
{
    "dpto": "05",
    "codigo": "05674",
    "nombre": "SAN VICENTE FERRER",
    "id": "fW-Fy3oBVXS07smaM5Ju"
},
{
    "dpto": "19",
    "codigo": "19075",
    "nombre": "BALBOA",
    "id": "D2-Fy3oBVXS07sma65QP"
},
{
    "dpto": "05",
    "codigo": "05686",
    "nombre": "SANTA ROSA DE OSOS",
    "id": "f2-Fy3oBVXS07smaNpL2"
},
{
    "dpto": "19",
    "codigo": "19110",
    "nombre": "BUENOS AIRES",
    "id": "EW-Fy3oBVXS07sma7pRE"
},
{
    "dpto": "05",
    "codigo": "05697",
    "nombre": "EL SANTUARIO",
    "id": "gW-Fy3oBVXS07smaNpL2"
},
{
    "dpto": "19",
    "codigo": "19137",
    "nombre": "CALDONO",
    "id": "E2-Fy3oBVXS07sma7pRE"
},
{
    "dpto": "05",
    "codigo": "05756",
    "nombre": "SONSÓN",
    "id": "g2-Fy3oBVXS07smaNpL2"
},
{
    "dpto": "15",
    "codigo": "15480",
    "nombre": "MUZO",
    "id": "FW-Fy3oBVXS07sma7pRE"
},
{
    "dpto": "05",
    "codigo": "05789",
    "nombre": "TÁMESIS",
    "id": "km-Fy3oBVXS07smaOpLe"
},
{
    "dpto": "15",
    "codigo": "15494",
    "nombre": "NUEVO COLÓN",
    "id": "F2-Fy3oBVXS07sma7pRE"
},
{
    "dpto": "05",
    "codigo": "05792",
    "nombre": "TARSO",
    "id": "lG-Fy3oBVXS07smaOpLe"
},
{
    "dpto": "15",
    "codigo": "15507",
    "nombre": "OTANCHE",
    "id": "GW-Fy3oBVXS07sma7pRE"
},
{
    "dpto": "05",
    "codigo": "05819",
    "nombre": "TOLEDO",
    "id": "lm-Fy3oBVXS07smaOpLe"
},
{
    "dpto": "47",
    "codigo": "47605",
    "nombre": "REMOLINO",
    "id": "6G-Ey3oBVXS07sma9pG_"
},
{
    "dpto": "05",
    "codigo": "05842",
    "nombre": "URAMITA",
    "id": "mG-Fy3oBVXS07smaP5If"
},
{
    "dpto": "47",
    "codigo": "47675",
    "nombre": "SALAMINA",
    "id": "6m-Ey3oBVXS07sma9pG_"
},
{
    "dpto": "05",
    "codigo": "05854",
    "nombre": "VALDIVIA",
    "id": "mm-Fy3oBVXS07smaP5If"
},
{
    "dpto": "47",
    "codigo": "47703",
    "nombre": "SAN ZENÓN",
    "id": "7G-Ey3oBVXS07sma9pG_"
},
{
    "dpto": "05",
    "codigo": "05858",
    "nombre": "VEGACHÍ",
    "id": "nG-Fy3oBVXS07smaP5If"
},
{
    "dpto": "05",
    "codigo": "05002",
    "nombre": "ABEJORRAL",
    "id": "7m-Ey3oBVXS07sma-pEB"
},
{
    "dpto": "05",
    "codigo": "05873",
    "nombre": "VIGÍA DEL FUERTE",
    "id": "n2-Fy3oBVXS07smaQ5Jr"
},
{
    "dpto": "05",
    "codigo": "05021",
    "nombre": "ALEJANDRÍA",
    "id": "8G-Ey3oBVXS07sma-pEB"
},
{
    "dpto": "05",
    "codigo": "05887",
    "nombre": "YARUMAL",
    "id": "oW-Fy3oBVXS07smaQ5Jr"
},
{
    "dpto": "05",
    "codigo": "05031",
    "nombre": "AMALFI",
    "id": "8m-Ey3oBVXS07sma-pEB"
},
{
    "dpto": "19",
    "codigo": "19142",
    "nombre": "CALOTO",
    "id": "o2-Fy3oBVXS07smaRpLF"
},
{
    "dpto": "05",
    "codigo": "05036",
    "nombre": "ANGELÓPOLIS",
    "id": "9G-Ey3oBVXS07sma-pEB"
},
{
    "dpto": "19",
    "codigo": "19290",
    "nombre": "FLORENCIA",
    "id": "pW-Fy3oBVXS07smaRpLF"
},
{
    "dpto": "44",
    "codigo": "44090",
    "nombre": "DIBULLA",
    "id": "-m-Fy3oBVXS07sma55NR"
},
{
    "dpto": "19",
    "codigo": "19355",
    "nombre": "INZÁ",
    "id": "p2-Fy3oBVXS07smaRpLF"
},
{
    "dpto": "44",
    "codigo": "44847",
    "nombre": "URIBIA",
    "id": "_G-Fy3oBVXS07sma55NR"
},
{
    "dpto": "19",
    "codigo": "19392",
    "nombre": "LA SIERRA",
    "id": "qW-Fy3oBVXS07smaRpLF"
},
{
    "dpto": "44",
    "codigo": "44110",
    "nombre": "EL MOLINO",
    "id": "8m-Fy3oBVXS07sma45Na"
},
{
    "dpto": "19",
    "codigo": "19450",
    "nombre": "MERCADERES",
    "id": "q2-Fy3oBVXS07smaRpLF"
},
{
    "dpto": "44",
    "codigo": "44378",
    "nombre": "HATONUEVO",
    "id": "9G-Fy3oBVXS07sma45Nb"
},
{
    "dpto": "19",
    "codigo": "19473",
    "nombre": "MORALES",
    "id": "rW-Fy3oBVXS07smaSpJ7"
},
{
    "dpto": "44",
    "codigo": "44855",
    "nombre": "URUMITA",
    "id": "9m-Fy3oBVXS07sma45Nb"
},
{
    "dpto": "19",
    "codigo": "19517",
    "nombre": "PÁEZ",
    "id": "r2-Fy3oBVXS07smaSpJ7"
},
{
    "dpto": "44",
    "codigo": "44001",
    "nombre": "RIOHACHA",
    "id": "-G-Fy3oBVXS07sma45Nb"
},
{
    "dpto": "19",
    "codigo": "19533",
    "nombre": "PIAMONTE",
    "id": "sW-Fy3oBVXS07smaSpJ7"
},
{
    "dpto": "20",
    "codigo": "20517",
    "nombre": "PAILITAS",
    "id": "1m-Fy3oBVXS07sma05OG"
},
{
    "dpto": "19",
    "codigo": "19573",
    "nombre": "PUERTO TEJADA",
    "id": "wW-Fy3oBVXS07smaTpIN"
},
{
    "dpto": "20",
    "codigo": "20614",
    "nombre": "RÍO DE ORO",
    "id": "2G-Fy3oBVXS07sma05OG"
},
{
    "dpto": "19",
    "codigo": "19622",
    "nombre": "ROSAS",
    "id": "w2-Fy3oBVXS07smaTpIN"
},
{
    "dpto": "20",
    "codigo": "20750",
    "nombre": "SAN DIEGO",
    "id": "2m-Fy3oBVXS07sma05OG"
},
{
    "dpto": "19",
    "codigo": "19698",
    "nombre": "SANTANDER DE QUILICHAO",
    "id": "xW-Fy3oBVXS07smaTpIN"
},
{
    "dpto": "47",
    "codigo": "47245",
    "nombre": "EL BANCO",
    "id": "x2-Fy3oBVXS07smayZMa"
},
{
    "dpto": "19",
    "codigo": "19743",
    "nombre": "SILVIA",
    "id": "x2-Fy3oBVXS07smaTpIN"
},
{
    "dpto": "20",
    "codigo": "20570",
    "nombre": "PUEBLO BELLO",
    "id": "7W-Fy3oBVXS07sma35PN"
},
{
    "dpto": "19",
    "codigo": "19780",
    "nombre": "SUÁREZ",
    "id": "yW-Fy3oBVXS07smaUZKM"
},
{
    "dpto": "20",
    "codigo": "20011",
    "nombre": "AGUACHICA",
    "id": "72-Fy3oBVXS07sma35PN"
},
{
    "dpto": "19",
    "codigo": "19807",
    "nombre": "TIMBÍO",
    "id": "y2-Fy3oBVXS07smaUZKM"
},
{
    "dpto": "20",
    "codigo": "20787",
    "nombre": "TAMALAMEQUE",
    "id": "52-Fy3oBVXS07sma15MY"
},
{
    "dpto": "19",
    "codigo": "19824",
    "nombre": "TOTORÓ",
    "id": "zW-Fy3oBVXS07smaUZKM"
},
{
    "dpto": "20",
    "codigo": "20013",
    "nombre": "AGUSTÍN CODAZZI",
    "id": "6W-Fy3oBVXS07sma15MY"
},
{
    "dpto": "19",
    "codigo": "19318",
    "nombre": "GUAPI",
    "id": "z2-Fy3oBVXS07smaVZKx"
},
{
    "dpto": "20",
    "codigo": "20621",
    "nombre": "LA PAZ",
    "id": "62-Fy3oBVXS07sma2pNQ"
},
{
    "dpto": "19",
    "codigo": "19809",
    "nombre": "TIMBIQUÍ",
    "id": "0W-Fy3oBVXS07smaVZKx"
},
{
    "dpto": "47",
    "codigo": "47555",
    "nombre": "PLATO",
    "id": "5W-Ey3oBVXS07sma85Ew"
},
{
    "dpto": "76",
    "codigo": "76001",
    "nombre": "CALI",
    "id": "02-Fy3oBVXS07smaVZKx"
},
{
    "dpto": "05",
    "codigo": "05038",
    "nombre": "ANGOSTURA",
    "id": "A2-Fy3oBVXS07smaAJKR"
},
{
    "dpto": "76",
    "codigo": "76041",
    "nombre": "ANSERMANUEVO",
    "id": "1W-Fy3oBVXS07smaVZKx"
},
{
    "dpto": "05",
    "codigo": "05042",
    "nombre": "SANTA FÉ DE ANTIOQUIA",
    "id": "BW-Fy3oBVXS07smaAJKR"
},
{
    "dpto": "76",
    "codigo": "76111",
    "nombre": "GUADALAJARA DE BUGA",
    "id": "12-Fy3oBVXS07smaVZKx"
},
{
    "dpto": "05",
    "codigo": "05045",
    "nombre": "APARTADÓ",
    "id": "B2-Fy3oBVXS07smaAJKR"
},
{
    "dpto": "76",
    "codigo": "76122",
    "nombre": "CAICEDONIA",
    "id": "3m-Fy3oBVXS07smaWZIg"
},
{
    "dpto": "05",
    "codigo": "05055",
    "nombre": "ARGELIA",
    "id": "CW-Fy3oBVXS07smaAJKR"
},
{
    "dpto": "76",
    "codigo": "76130",
    "nombre": "CANDELARIA",
    "id": "4G-Fy3oBVXS07smaWZIg"
},
{
    "dpto": "05",
    "codigo": "05079",
    "nombre": "BARBOSA",
    "id": "C2-Fy3oBVXS07smaA5LP"
},
{
    "dpto": "76",
    "codigo": "76243",
    "nombre": "EL ÁGUILA",
    "id": "4m-Fy3oBVXS07smaWZIg"
},
{
    "dpto": "05",
    "codigo": "05088",
    "nombre": "BELLO",
    "id": "DW-Fy3oBVXS07smaA5LP"
},
{
    "dpto": "76",
    "codigo": "76248",
    "nombre": "EL CERRITO",
    "id": "5G-Fy3oBVXS07smaWZIg"
},
{
    "dpto": "05",
    "codigo": "05093",
    "nombre": "BETULIA",
    "id": "D2-Fy3oBVXS07smaA5LP"
},
{
    "dpto": "76",
    "codigo": "76275",
    "nombre": "FLORIDA",
    "id": "5m-Fy3oBVXS07smaWZIg"
},
{
    "dpto": "05",
    "codigo": "05107",
    "nombre": "BRICEÑO",
    "id": "EW-Fy3oBVXS07smaB5Ik"
},
{
    "dpto": "76",
    "codigo": "76318",
    "nombre": "GUACARÍ",
    "id": "6G-Fy3oBVXS07smaXJJP"
},
{
    "dpto": "05",
    "codigo": "05120",
    "nombre": "CÁCERES",
    "id": "E2-Fy3oBVXS07smaB5Ik"
},
{
    "dpto": "76",
    "codigo": "76497",
    "nombre": "OBANDO",
    "id": "6m-Fy3oBVXS07smaXJJP"
},
{
    "dpto": "05",
    "codigo": "05129",
    "nombre": "CALDAS",
    "id": "FW-Fy3oBVXS07smaB5Ik"
},
{
    "dpto": "76",
    "codigo": "76563",
    "nombre": "PRADERA",
    "id": "7G-Fy3oBVXS07smaXJJP"
},
{
    "dpto": "05",
    "codigo": "05138",
    "nombre": "CAÑASGORDAS",
    "id": "F2-Fy3oBVXS07smaB5Ik"
},
{
    "dpto": "76",
    "codigo": "76616",
    "nombre": "RIOFRÍO",
    "id": "7m-Fy3oBVXS07smaXJJP"
},
{
    "dpto": "05",
    "codigo": "05145",
    "nombre": "CARAMANTA",
    "id": "G2-Fy3oBVXS07smaC5I6"
},
{
    "dpto": "76",
    "codigo": "76670",
    "nombre": "SAN PEDRO",
    "id": "8G-Fy3oBVXS07smaXJJP"
},
{
    "dpto": "05",
    "codigo": "05148",
    "nombre": "EL CARMEN DE VIBORAL",
    "id": "HW-Fy3oBVXS07smaC5I6"
},
{
    "dpto": "76",
    "codigo": "76823",
    "nombre": "TORO",
    "id": "AW-Fy3oBVXS07smaYZN6"
},
{
    "dpto": "05",
    "codigo": "05154",
    "nombre": "CAUCASIA",
    "id": "H2-Fy3oBVXS07smaC5I6"
},
{
    "dpto": "76",
    "codigo": "76834",
    "nombre": "TULUÁ",
    "id": "A2-Fy3oBVXS07smaYZN6"
},
{
    "dpto": "05",
    "codigo": "05190",
    "nombre": "CISNEROS",
    "id": "IW-Fy3oBVXS07smaDpLJ"
},
{
    "dpto": "18",
    "codigo": "18785",
    "nombre": "SOLITA",
    "id": "BW-Fy3oBVXS07smaZpNN"
},
{
    "dpto": "05",
    "codigo": "05206",
    "nombre": "CONCEPCIÓN",
    "id": "I2-Fy3oBVXS07smaDpLJ"
},
{
    "dpto": "18",
    "codigo": "18592",
    "nombre": "PUERTO RICO",
    "id": "B2-Fy3oBVXS07smaZpNN"
},
{
    "dpto": "05",
    "codigo": "05212",
    "nombre": "COPACABANA",
    "id": "JW-Fy3oBVXS07smaDpLJ"
},
{
    "dpto": "50",
    "codigo": "50006",
    "nombre": "ACACÍAS",
    "id": "CW-Fy3oBVXS07smaZpNN"
},
{
    "dpto": "05",
    "codigo": "05237",
    "nombre": "DONMATÍAS",
    "id": "J2-Fy3oBVXS07smaDpLJ"
},
{
    "dpto": "50",
    "codigo": "50124",
    "nombre": "CABUYARO",
    "id": "DG-Fy3oBVXS07smaapPR"
},
{
    "dpto": "05",
    "codigo": "05250",
    "nombre": "EL BAGRE",
    "id": "KW-Fy3oBVXS07smaEpIV"
},
{
    "dpto": "50",
    "codigo": "50223",
    "nombre": "CUBARRAL",
    "id": "Dm-Fy3oBVXS07smaapPR"
},
{
    "dpto": "05",
    "codigo": "05266",
    "nombre": "ENVIGADO",
    "id": "K2-Fy3oBVXS07smaEpIV"
},
{
    "dpto": "50",
    "codigo": "50245",
    "nombre": "EL CALVARIO",
    "id": "EG-Fy3oBVXS07smaapPR"
},
{
    "dpto": "05",
    "codigo": "05284",
    "nombre": "FRONTINO",
    "id": "LW-Fy3oBVXS07smaEpIV"
},
{
    "dpto": "50",
    "codigo": "50270",
    "nombre": "EL DORADO",
    "id": "Em-Fy3oBVXS07smabpNG"
},
{
    "dpto": "05",
    "codigo": "05308",
    "nombre": "GIRARDOTA",
    "id": "PG-Fy3oBVXS07smaGZJZ"
},
{
    "dpto": "50",
    "codigo": "50313",
    "nombre": "GRANADA",
    "id": "FG-Fy3oBVXS07smabpNG"
},
{
    "dpto": "05",
    "codigo": "05313",
    "nombre": "GRANADA",
    "id": "Pm-Fy3oBVXS07smaGZJZ"
},
{
    "dpto": "50",
    "codigo": "50330",
    "nombre": "MESETAS",
    "id": "Fm-Fy3oBVXS07smabpNG"
},
{
    "dpto": "05",
    "codigo": "05318",
    "nombre": "GUARNE",
    "id": "QG-Fy3oBVXS07smaGZJZ"
},
{
    "dpto": "50",
    "codigo": "50400",
    "nombre": "LEJANÍAS",
    "id": "GG-Fy3oBVXS07smacpPl"
},
{
    "dpto": "05",
    "codigo": "05347",
    "nombre": "HELICONIA",
    "id": "Qm-Fy3oBVXS07smaGZJZ"
},
{
    "dpto": "50",
    "codigo": "50573",
    "nombre": "PUERTO LÓPEZ",
    "id": "Gm-Fy3oBVXS07smacpPl"
},
{
    "dpto": "05",
    "codigo": "05360",
    "nombre": "ITAGÜÍ",
    "id": "RG-Fy3oBVXS07smaGZJZ"
},
{
    "dpto": "50",
    "codigo": "50590",
    "nombre": "PUERTO RICO",
    "id": "Km-Fy3oBVXS07smaeJPy"
},
{
    "dpto": "05",
    "codigo": "05364",
    "nombre": "JARDÍN",
    "id": "R2-Fy3oBVXS07smaHZIL"
},
{
    "dpto": "50",
    "codigo": "50680",
    "nombre": "SAN CARLOS DE GUAROA",
    "id": "Lm-Fy3oBVXS07smafpPJ"
},
{
    "dpto": "05",
    "codigo": "05376",
    "nombre": "LA CEJA",
    "id": "SW-Fy3oBVXS07smaHZIL"
},
{
    "dpto": "50",
    "codigo": "50686",
    "nombre": "SAN JUANITO",
    "id": "MG-Fy3oBVXS07smafpPJ"
},
{
    "dpto": "05",
    "codigo": "05390",
    "nombre": "LA PINTADA",
    "id": "S2-Fy3oBVXS07smaHZIL"
},
{
    "dpto": "50",
    "codigo": "50711",
    "nombre": "VISTAHERMOSA",
    "id": "Mm-Fy3oBVXS07smahJNK"
},
{
    "dpto": "05",
    "codigo": "05411",
    "nombre": "LIBORINA",
    "id": "TW-Fy3oBVXS07smaHZIL"
},
{
    "dpto": "50",
    "codigo": "50325",
    "nombre": "MAPIRIPÁN",
    "id": "NG-Fy3oBVXS07smaiJNH"
},
{
    "dpto": "05",
    "codigo": "05440",
    "nombre": "MARINILLA",
    "id": "T2-Fy3oBVXS07smaHZIL"
},
{
    "dpto": "15",
    "codigo": "15001",
    "nombre": "TUNJA",
    "id": "Q2-Fy3oBVXS07smai5O2"
},
{
    "dpto": "05",
    "codigo": "05475",
    "nombre": "MURINDÓ",
    "id": "UW-Fy3oBVXS07smaJJJS"
},
{
    "dpto": "15",
    "codigo": "15047",
    "nombre": "AQUITANIA",
    "id": "RW-Fy3oBVXS07smai5O2"
},
{
    "dpto": "05",
    "codigo": "05483",
    "nombre": "NARIÑO",
    "id": "U2-Fy3oBVXS07smaJJJS"
},
{
    "dpto": "15",
    "codigo": "15087",
    "nombre": "BELÉN",
    "id": "R2-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "05",
    "codigo": "05495",
    "nombre": "NECHÍ",
    "id": "VW-Fy3oBVXS07smaJJJS"
},
{
    "dpto": "15",
    "codigo": "15092",
    "nombre": "BETÉITIVA",
    "id": "SW-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "05",
    "codigo": "05541",
    "nombre": "PEÑOL",
    "id": "ZG-Fy3oBVXS07smaKJK5"
},
{
    "dpto": "15",
    "codigo": "15104",
    "nombre": "BOYACÁ",
    "id": "S2-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "05",
    "codigo": "05576",
    "nombre": "PUEBLORRICO",
    "id": "Zm-Fy3oBVXS07smaKJK5"
},
{
    "dpto": "15",
    "codigo": "15109",
    "nombre": "BUENAVISTA",
    "id": "TW-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "05",
    "codigo": "05585",
    "nombre": "PUERTO NARE",
    "id": "aG-Fy3oBVXS07smaKJK5"
},
{
    "dpto": "76",
    "codigo": "76845",
    "nombre": "ULLOA",
    "id": "T2-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "05",
    "codigo": "05604",
    "nombre": "REMEDIOS",
    "id": "am-Fy3oBVXS07smaKJK5"
},
{
    "dpto": "76",
    "codigo": "76869",
    "nombre": "VIJES",
    "id": "UW-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "05",
    "codigo": "05615",
    "nombre": "RIONEGRO",
    "id": "bm-Fy3oBVXS07smaL5Kn"
},
{
    "dpto": "76",
    "codigo": "76892",
    "nombre": "YUMBO",
    "id": "U2-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "05",
    "codigo": "05631",
    "nombre": "SABANETA",
    "id": "cG-Fy3oBVXS07smaL5Kn"
},
{
    "dpto": "76",
    "codigo": "76403",
    "nombre": "LA VICTORIA",
    "id": "V2-Fy3oBVXS07smakpO-"
},
{
    "dpto": "05",
    "codigo": "05647",
    "nombre": "SAN ANDRÉS DE CUERQUÍA",
    "id": "cm-Fy3oBVXS07smaL5Kn"
},
{
    "dpto": "76",
    "codigo": "76377",
    "nombre": "LA CUMBRE",
    "id": "WW-Fy3oBVXS07smakpO-"
},
{
    "dpto": "05",
    "codigo": "05652",
    "nombre": "SAN FRANCISCO",
    "id": "dG-Fy3oBVXS07smaL5Kn"
},
{
    "dpto": "76",
    "codigo": "76100",
    "nombre": "BOLÍVAR",
    "id": "W2-Fy3oBVXS07smakpO-"
},
{
    "dpto": "05",
    "codigo": "05658",
    "nombre": "SAN JOSÉ DE LA MONTAÑA",
    "id": "dm-Fy3oBVXS07smaM5Ju"
},
{
    "dpto": "76",
    "codigo": "76109",
    "nombre": "BUENAVENTURA",
    "id": "XW-Fy3oBVXS07smakpO-"
},
{
    "dpto": "05",
    "codigo": "05660",
    "nombre": "SAN LUIS",
    "id": "eG-Fy3oBVXS07smaM5Ju"
},
{
    "dpto": "41",
    "codigo": "41306",
    "nombre": "GIGANTE",
    "id": "X2-Fy3oBVXS07smalpNN"
},
{
    "dpto": "05",
    "codigo": "05665",
    "nombre": "SAN PEDRO DE URABÁ",
    "id": "em-Fy3oBVXS07smaM5Ju"
},
{
    "dpto": "41",
    "codigo": "41349",
    "nombre": "HOBO",
    "id": "YW-Fy3oBVXS07smalpNN"
},
{
    "dpto": "05",
    "codigo": "05670",
    "nombre": "SAN ROQUE",
    "id": "fG-Fy3oBVXS07smaM5Ju"
},
{
    "dpto": "41",
    "codigo": "41359",
    "nombre": "ISNOS",
    "id": "Y2-Fy3oBVXS07smalpNN"
},
{
    "dpto": "05",
    "codigo": "05679",
    "nombre": "SANTA BÁRBARA",
    "id": "fm-Fy3oBVXS07smaNpL2"
},
{
    "dpto": "41",
    "codigo": "41396",
    "nombre": "LA PLATA",
    "id": "ZW-Fy3oBVXS07smalpNN"
},
{
    "dpto": "05",
    "codigo": "05690",
    "nombre": "SANTO DOMINGO",
    "id": "gG-Fy3oBVXS07smaNpL2"
},
{
    "dpto": "41",
    "codigo": "41503",
    "nombre": "OPORAPA",
    "id": "Z2-Fy3oBVXS07smamZNo"
},
{
    "dpto": "05",
    "codigo": "05736",
    "nombre": "SEGOVIA",
    "id": "gm-Fy3oBVXS07smaNpL2"
},
{
    "dpto": "41",
    "codigo": "41524",
    "nombre": "PALERMO",
    "id": "aW-Fy3oBVXS07smamZNo"
},
{
    "dpto": "05",
    "codigo": "05761",
    "nombre": "SOPETRÁN",
    "id": "kW-Fy3oBVXS07smaOpLe"
},
{
    "dpto": "41",
    "codigo": "41548",
    "nombre": "PITAL",
    "id": "a2-Fy3oBVXS07smamZNo"
},
{
    "dpto": "05",
    "codigo": "05790",
    "nombre": "TARAZÁ",
    "id": "k2-Fy3oBVXS07smaOpLe"
},
{
    "dpto": "41",
    "codigo": "41615",
    "nombre": "RIVERA",
    "id": "bW-Fy3oBVXS07smamZNo"
},
{
    "dpto": "05",
    "codigo": "05809",
    "nombre": "TITIRIBÍ",
    "id": "lW-Fy3oBVXS07smaOpLe"
},
{
    "dpto": "41",
    "codigo": "41668",
    "nombre": "SAN AGUSTÍN",
    "id": "b2-Fy3oBVXS07smamZNo"
},
{
    "dpto": "05",
    "codigo": "05837",
    "nombre": "TURBO",
    "id": "l2-Fy3oBVXS07smaOpLe"
},
{
    "dpto": "41",
    "codigo": "41770",
    "nombre": "SUAZA",
    "id": "gW-Fy3oBVXS07smanJPJ"
},
{
    "dpto": "05",
    "codigo": "05847",
    "nombre": "URRAO",
    "id": "mW-Fy3oBVXS07smaP5If"
},
{
    "dpto": "41",
    "codigo": "41797",
    "nombre": "TESALIA",
    "id": "g2-Fy3oBVXS07smanJPJ"
},
{
    "dpto": "05",
    "codigo": "05856",
    "nombre": "VALPARAÍSO",
    "id": "m2-Fy3oBVXS07smaP5If"
},
{
    "dpto": "41",
    "codigo": "41801",
    "nombre": "TERUEL",
    "id": "hW-Fy3oBVXS07smanJPJ"
},
{
    "dpto": "05",
    "codigo": "05861",
    "nombre": "VENECIA",
    "id": "nm-Fy3oBVXS07smaQ5Jr"
},
{
    "dpto": "41",
    "codigo": "41872",
    "nombre": "VILLAVIEJA",
    "id": "h2-Fy3oBVXS07smanJPJ"
},
{
    "dpto": "05",
    "codigo": "05885",
    "nombre": "YALÍ",
    "id": "oG-Fy3oBVXS07smaQ5Jr"
},
{
    "dpto": "41",
    "codigo": "41298",
    "nombre": "GARZÓN",
    "id": "iW-Fy3oBVXS07smanJPJ"
},
{
    "dpto": "05",
    "codigo": "05890",
    "nombre": "YOLOMBÓ",
    "id": "om-Fy3oBVXS07smaQ5Jr"
},
{
    "dpto": "41",
    "codigo": "41206",
    "nombre": "COLOMBIA",
    "id": "i2-Fy3oBVXS07smanJPJ"
},
{
    "dpto": "19",
    "codigo": "19212",
    "nombre": "CORINTO",
    "id": "pG-Fy3oBVXS07smaRpLF"
},
{
    "dpto": "41",
    "codigo": "41078",
    "nombre": "BARAYA",
    "id": "jW-Fy3oBVXS07smaoJNj"
},
{
    "dpto": "19",
    "codigo": "19300",
    "nombre": "GUACHENÉ",
    "id": "pm-Fy3oBVXS07smaRpLF"
},
{
    "dpto": "41",
    "codigo": "41020",
    "nombre": "ALGECIRAS",
    "id": "j2-Fy3oBVXS07smaoJNj"
},
{
    "dpto": "19",
    "codigo": "19364",
    "nombre": "JAMBALÓ",
    "id": "qG-Fy3oBVXS07smaRpLF"
},
{
    "dpto": "41",
    "codigo": "41013",
    "nombre": "AGRADO",
    "id": "kW-Fy3oBVXS07smaoJNj"
},
{
    "dpto": "19",
    "codigo": "19397",
    "nombre": "LA VEGA",
    "id": "qm-Fy3oBVXS07smaRpLF"
},
{
    "dpto": "18",
    "codigo": "18001",
    "nombre": "FLORENCIA",
    "id": "k2-Fy3oBVXS07smaoJNj"
},
{
    "dpto": "19",
    "codigo": "19455",
    "nombre": "MIRANDA",
    "id": "rG-Fy3oBVXS07smaSpJ7"
},
{
    "dpto": "18",
    "codigo": "18094",
    "nombre": "BELÉN DE LOS ANDAQUÍES",
    "id": "l2-Fy3oBVXS07smaqJN2"
},
{
    "dpto": "19",
    "codigo": "19513",
    "nombre": "PADILLA",
    "id": "rm-Fy3oBVXS07smaSpJ7"
},
{
    "dpto": "18",
    "codigo": "18256",
    "nombre": "EL PAUJÍL",
    "id": "mW-Fy3oBVXS07smarJNO"
},
{
    "dpto": "19",
    "codigo": "19532",
    "nombre": "PATÍA",
    "id": "sG-Fy3oBVXS07smaSpJ7"
},
{
    "dpto": "18",
    "codigo": "18460",
    "nombre": "MILÁN",
    "id": "qG-Fy3oBVXS07smasJP3"
},
{
    "dpto": "19",
    "codigo": "19548",
    "nombre": "PIENDAMÓ - TUNÍA",
    "id": "wG-Fy3oBVXS07smaTpIN"
},
{
    "dpto": "18",
    "codigo": "18610",
    "nombre": "SAN JOSÉ DEL FRAGUA",
    "id": "qm-Fy3oBVXS07smasJP3"
},
{
    "dpto": "19",
    "codigo": "19585",
    "nombre": "PURACÉ",
    "id": "wm-Fy3oBVXS07smaTpIN"
},
{
    "dpto": "18",
    "codigo": "18150",
    "nombre": "CARTAGENA DEL CHAIRÁ",
    "id": "rW-Fy3oBVXS07smaupOg"
},
{
    "dpto": "19",
    "codigo": "19693",
    "nombre": "SAN SEBASTIÁN",
    "id": "xG-Fy3oBVXS07smaTpIN"
},
{
    "dpto": "47",
    "codigo": "47707",
    "nombre": "SANTA ANA",
    "id": "r2-Fy3oBVXS07smavpNe"
},
{
    "dpto": "19",
    "codigo": "19701",
    "nombre": "SANTA ROSA",
    "id": "xm-Fy3oBVXS07smaTpIN"
},
{
    "dpto": "47",
    "codigo": "47745",
    "nombre": "SITIONUEVO",
    "id": "sW-Fy3oBVXS07smawZO7"
},
{
    "dpto": "19",
    "codigo": "19760",
    "nombre": "SOTARÁ - PAISPAMBA",
    "id": "yG-Fy3oBVXS07smaUZKM"
},
{
    "dpto": "47",
    "codigo": "47960",
    "nombre": "ZAPAYÁN",
    "id": "s2-Fy3oBVXS07smawZO7"
},
{
    "dpto": "19",
    "codigo": "19785",
    "nombre": "SUCRE",
    "id": "ym-Fy3oBVXS07smaUZKM"
},
{
    "dpto": "47",
    "codigo": "47053",
    "nombre": "ARACATACA",
    "id": "w2-Fy3oBVXS07smaxZOS"
},
{
    "dpto": "19",
    "codigo": "19821",
    "nombre": "TORIBÍO",
    "id": "zG-Fy3oBVXS07smaUZKM"
},
{
    "dpto": "47",
    "codigo": "47288",
    "nombre": "FUNDACIÓN",
    "id": "xW-Fy3oBVXS07smaxZOS"
},
{
    "dpto": "19",
    "codigo": "19256",
    "nombre": "EL TAMBO",
    "id": "zm-Fy3oBVXS07smaUZKM"
},
{
    "dpto": "47",
    "codigo": "47245",
    "nombre": "EL BANCO",
    "id": "x2-Fy3oBVXS07smayZMa"
},
{
    "dpto": "19",
    "codigo": "19418",
    "nombre": "LÓPEZ DE MICAY",
    "id": "0G-Fy3oBVXS07smaVZKx"
},
{
    "dpto": "20",
    "codigo": "20060",
    "nombre": "BOSCONIA",
    "id": "y2-Fy3oBVXS07smazJNa"
},
{
    "dpto": "19",
    "codigo": "19845",
    "nombre": "VILLA RICA",
    "id": "0m-Fy3oBVXS07smaVZKx"
},
{
    "dpto": "20",
    "codigo": "20178",
    "nombre": "CHIRIGUANÁ",
    "id": "zW-Fy3oBVXS07smazJNa"
},
{
    "dpto": "76",
    "codigo": "76036",
    "nombre": "ANDALUCÍA",
    "id": "1G-Fy3oBVXS07smaVZKx"
},
{
    "dpto": "20",
    "codigo": "20238",
    "nombre": "EL COPEY",
    "id": "z2-Fy3oBVXS07smaz5Pb"
},
{
    "dpto": "76",
    "codigo": "76054",
    "nombre": "ARGELIA",
    "id": "1m-Fy3oBVXS07smaVZKx"
},
{
    "dpto": "20",
    "codigo": "20295",
    "nombre": "GAMARRA",
    "id": "0W-Fy3oBVXS07smaz5Pb"
},
{
    "dpto": "76",
    "codigo": "76113",
    "nombre": "BUGALAGRANDE",
    "id": "3W-Fy3oBVXS07smaWZIg"
},
{
    "dpto": "20",
    "codigo": "20383",
    "nombre": "LA GLORIA",
    "id": "02-Fy3oBVXS07smaz5Pb"
},
{
    "dpto": "76",
    "codigo": "76126",
    "nombre": "CALIMA",
    "id": "32-Fy3oBVXS07smaWZIg"
},
{
    "dpto": "20",
    "codigo": "20443",
    "nombre": "MANAURE BALCÓN DEL CESAR",
    "id": "1W-Fy3oBVXS07sma05OG"
},
{
    "dpto": "76",
    "codigo": "76147",
    "nombre": "CARTAGO",
    "id": "4W-Fy3oBVXS07smaWZIg"
},
{
    "dpto": "20",
    "codigo": "20550",
    "nombre": "PELAYA",
    "id": "12-Fy3oBVXS07sma05OG"
},
{
    "dpto": "76",
    "codigo": "76246",
    "nombre": "EL CAIRO",
    "id": "42-Fy3oBVXS07smaWZIg"
},
{
    "dpto": "20",
    "codigo": "20710",
    "nombre": "SAN ALBERTO",
    "id": "2W-Fy3oBVXS07sma05OG"
},
{
    "dpto": "76",
    "codigo": "76250",
    "nombre": "EL DOVIO",
    "id": "5W-Fy3oBVXS07smaWZIg"
},
{
    "dpto": "20",
    "codigo": "20787",
    "nombre": "TAMALAMEQUE",
    "id": "52-Fy3oBVXS07sma15MY"
},
{
    "dpto": "76",
    "codigo": "76306",
    "nombre": "GINEBRA",
    "id": "52-Fy3oBVXS07smaXJJP"
},
{
    "dpto": "20",
    "codigo": "20013",
    "nombre": "AGUSTÍN CODAZZI",
    "id": "6W-Fy3oBVXS07sma15MY"
},
{
    "dpto": "76",
    "codigo": "76020",
    "nombre": "ALCALÁ",
    "id": "6W-Fy3oBVXS07smaXJJP"
},
{
    "dpto": "20",
    "codigo": "20621",
    "nombre": "LA PAZ",
    "id": "62-Fy3oBVXS07sma2pNQ"
},
{
    "dpto": "76",
    "codigo": "76520",
    "nombre": "PALMIRA",
    "id": "62-Fy3oBVXS07smaXJJP"
},
{
    "dpto": "20",
    "codigo": "20770",
    "nombre": "SAN MARTÍN",
    "id": "7m-Fy3oBVXS07sma35PN"
},
{
    "dpto": "76",
    "codigo": "76606",
    "nombre": "RESTREPO",
    "id": "7W-Fy3oBVXS07smaXJJP"
},
{
    "dpto": "44",
    "codigo": "44078",
    "nombre": "BARRANCAS",
    "id": "8G-Fy3oBVXS07sma35PN"
},
{
    "dpto": "76",
    "codigo": "76622",
    "nombre": "ROLDANILLO",
    "id": "72-Fy3oBVXS07smaXJJP"
},
{
    "dpto": "44",
    "codigo": "44110",
    "nombre": "EL MOLINO",
    "id": "8m-Fy3oBVXS07sma45Na"
},
{
    "dpto": "76",
    "codigo": "76736",
    "nombre": "SEVILLA",
    "id": "AG-Fy3oBVXS07smaYZN6"
},
{
    "dpto": "44",
    "codigo": "44378",
    "nombre": "HATONUEVO",
    "id": "9G-Fy3oBVXS07sma45Nb"
},
{
    "dpto": "76",
    "codigo": "76828",
    "nombre": "TRUJILLO",
    "id": "Am-Fy3oBVXS07smaYZN6"
},
{
    "dpto": "44",
    "codigo": "44855",
    "nombre": "URUMITA",
    "id": "9m-Fy3oBVXS07sma45Nb"
},
{
    "dpto": "18",
    "codigo": "18756",
    "nombre": "SOLANO",
    "id": "BG-Fy3oBVXS07smaYZN6"
},
{
    "dpto": "44",
    "codigo": "44001",
    "nombre": "RIOHACHA",
    "id": "-G-Fy3oBVXS07sma45Nb"
},
{
    "dpto": "18",
    "codigo": "18205",
    "nombre": "CURILLO",
    "id": "Bm-Fy3oBVXS07smaZpNN"
},
{
    "dpto": "44",
    "codigo": "44090",
    "nombre": "DIBULLA",
    "id": "-m-Fy3oBVXS07sma55NR"
},
{
    "dpto": "50",
    "codigo": "50001",
    "nombre": "VILLAVICENCIO",
    "id": "CG-Fy3oBVXS07smaZpNN"
},
{
    "dpto": "44",
    "codigo": "44847",
    "nombre": "URIBIA",
    "id": "_G-Fy3oBVXS07sma55NR"
},
{
    "dpto": "50",
    "codigo": "50110",
    "nombre": "BARRANCA DE UPÍA",
    "id": "C2-Fy3oBVXS07smaapPR"
},
{
    "dpto": "44",
    "codigo": "44560",
    "nombre": "MANAURE",
    "id": "C2-Fy3oBVXS07sma65QP"
},
{
    "dpto": "50",
    "codigo": "50150",
    "nombre": "CASTILLA LA NUEVA",
    "id": "DW-Fy3oBVXS07smaapPR"
},
{
    "dpto": "19",
    "codigo": "19022",
    "nombre": "ALMAGUER",
    "id": "DW-Fy3oBVXS07sma65QP"
},
{
    "dpto": "50",
    "codigo": "50226",
    "nombre": "CUMARAL",
    "id": "D2-Fy3oBVXS07smaapPR"
},
{
    "dpto": "19",
    "codigo": "19075",
    "nombre": "BALBOA",
    "id": "D2-Fy3oBVXS07sma65QP"
},
{
    "dpto": "50",
    "codigo": "50251",
    "nombre": "EL CASTILLO",
    "id": "EW-Fy3oBVXS07smaapPR"
},
{
    "dpto": "19",
    "codigo": "19110",
    "nombre": "BUENOS AIRES",
    "id": "EW-Fy3oBVXS07sma7pRE"
},
{
    "dpto": "50",
    "codigo": "50287",
    "nombre": "FUENTE DE ORO",
    "id": "E2-Fy3oBVXS07smabpNG"
},
{
    "dpto": "19",
    "codigo": "19137",
    "nombre": "CALDONO",
    "id": "E2-Fy3oBVXS07sma7pRE"
},
{
    "dpto": "50",
    "codigo": "50318",
    "nombre": "GUAMAL",
    "id": "FW-Fy3oBVXS07smabpNG"
},
{
    "dpto": "15",
    "codigo": "15480",
    "nombre": "MUZO",
    "id": "FW-Fy3oBVXS07sma7pRE"
},
{
    "dpto": "50",
    "codigo": "50370",
    "nombre": "URIBE",
    "id": "F2-Fy3oBVXS07smabpNG"
},
{
    "dpto": "15",
    "codigo": "15494",
    "nombre": "NUEVO COLÓN",
    "id": "F2-Fy3oBVXS07sma7pRE"
},
{
    "dpto": "50",
    "codigo": "50450",
    "nombre": "PUERTO CONCORDIA",
    "id": "GW-Fy3oBVXS07smacpPl"
},
{
    "dpto": "15",
    "codigo": "15507",
    "nombre": "OTANCHE",
    "id": "GW-Fy3oBVXS07sma7pRE"
},
{
    "dpto": "50",
    "codigo": "50577",
    "nombre": "PUERTO LLERAS",
    "id": "KW-Fy3oBVXS07smaeJPy"
},
{
    "dpto": "15",
    "codigo": "15514",
    "nombre": "PÁEZ",
    "id": "G2-Fy3oBVXS07sma8ZRa"
},
{
    "dpto": "50",
    "codigo": "50606",
    "nombre": "RESTREPO",
    "id": "LW-Fy3oBVXS07smafpPJ"
},
{
    "dpto": "15",
    "codigo": "15518",
    "nombre": "PAJARITO",
    "id": "HW-Fy3oBVXS07sma8ZRa"
},
{
    "dpto": "50",
    "codigo": "50683",
    "nombre": "SAN JUAN DE ARAMA",
    "id": "L2-Fy3oBVXS07smafpPJ"
},
{
    "dpto": "15",
    "codigo": "15531",
    "nombre": "PAUNA",
    "id": "H2-Fy3oBVXS07sma8ZRa"
},
{
    "dpto": "50",
    "codigo": "50689",
    "nombre": "SAN MARTÍN",
    "id": "MW-Fy3oBVXS07smafpPJ"
},
{
    "dpto": "15",
    "codigo": "15537",
    "nombre": "PAZ DE RÍO",
    "id": "IW-Fy3oBVXS07sma8ZRa"
},
{
    "dpto": "50",
    "codigo": "50350",
    "nombre": "LA MACARENA",
    "id": "M2-Fy3oBVXS07smahJNK"
},
{
    "dpto": "15",
    "codigo": "15600",
    "nombre": "RÁQUIRA",
    "id": "I2-Fy3oBVXS07sma8ZRa"
},
{
    "dpto": "50",
    "codigo": "50568",
    "nombre": "PUERTO GAITÁN",
    "id": "Qm-Fy3oBVXS07smai5O2"
},
{
    "dpto": "15",
    "codigo": "15632",
    "nombre": "SABOYÁ",
    "id": "JW-Fy3oBVXS07sma8ZRa"
},
{
    "dpto": "15",
    "codigo": "15022",
    "nombre": "ALMEIDA",
    "id": "RG-Fy3oBVXS07smai5O2"
},
{
    "dpto": "15",
    "codigo": "15646",
    "nombre": "SAMACÁ",
    "id": "KW-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "15",
    "codigo": "15051",
    "nombre": "ARCABUCO",
    "id": "Rm-Fy3oBVXS07smai5O2"
},
{
    "dpto": "15",
    "codigo": "15664",
    "nombre": "SAN JOSÉ DE PARE",
    "id": "K2-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "15",
    "codigo": "15090",
    "nombre": "BERBEO",
    "id": "SG-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "15",
    "codigo": "15673",
    "nombre": "SAN MATEO",
    "id": "LW-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "15",
    "codigo": "15097",
    "nombre": "BOAVITA",
    "id": "Sm-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "15",
    "codigo": "15681",
    "nombre": "SAN PABLO DE BORBUR",
    "id": "L2-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "15",
    "codigo": "15106",
    "nombre": "BRICEÑO",
    "id": "TG-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "15",
    "codigo": "15690",
    "nombre": "SANTA MARÍA",
    "id": "MW-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "15",
    "codigo": "15114",
    "nombre": "BUSBANZÁ",
    "id": "Tm-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "15",
    "codigo": "15696",
    "nombre": "SANTA SOFÍA",
    "id": "M2-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "76",
    "codigo": "76863",
    "nombre": "VERSALLES",
    "id": "UG-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "15",
    "codigo": "15723",
    "nombre": "SATIVASUR",
    "id": "NW-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "76",
    "codigo": "76890",
    "nombre": "YOTOCO",
    "id": "Um-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "15",
    "codigo": "15753",
    "nombre": "SOATÁ",
    "id": "N2-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "76",
    "codigo": "76895",
    "nombre": "ZARZAL",
    "id": "VG-Fy3oBVXS07smaj5ND"
},
{
    "dpto": "15",
    "codigo": "15763",
    "nombre": "SOTAQUIRÁ",
    "id": "OW-Fy3oBVXS07sma9JTJ"
},
{
    "dpto": "76",
    "codigo": "76400",
    "nombre": "LA UNIÓN",
    "id": "WG-Fy3oBVXS07smakpO-"
},
{
    "dpto": "15",
    "codigo": "15774",
    "nombre": "SUSACÓN",
    "id": "O2-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "76",
    "codigo": "76364",
    "nombre": "JAMUNDÍ",
    "id": "Wm-Fy3oBVXS07smakpO-"
},
{
    "dpto": "15",
    "codigo": "15778",
    "nombre": "SUTATENZA",
    "id": "PW-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "76",
    "codigo": "76233",
    "nombre": "DAGUA",
    "id": "XG-Fy3oBVXS07smakpO-"
},
{
    "dpto": "15",
    "codigo": "15798",
    "nombre": "TENZA",
    "id": "P2-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "41",
    "codigo": "41001",
    "nombre": "NEIVA",
    "id": "Xm-Fy3oBVXS07smalpNN"
},
{
    "dpto": "15",
    "codigo": "15806",
    "nombre": "TIBASOSA",
    "id": "QW-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "41",
    "codigo": "41319",
    "nombre": "GUADALUPE",
    "id": "YG-Fy3oBVXS07smalpNN"
},
{
    "dpto": "15",
    "codigo": "15810",
    "nombre": "TIPACOQUE",
    "id": "Q2-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "41",
    "codigo": "41357",
    "nombre": "ÍQUIRA",
    "id": "Ym-Fy3oBVXS07smalpNN"
},
{
    "dpto": "15",
    "codigo": "15816",
    "nombre": "TOGÜÍ",
    "id": "RW-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "41",
    "codigo": "41378",
    "nombre": "LA ARGENTINA",
    "id": "ZG-Fy3oBVXS07smalpNN"
},
{
    "dpto": "15",
    "codigo": "15822",
    "nombre": "TOTA",
    "id": "R2-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "41",
    "codigo": "41483",
    "nombre": "NÁTAGA",
    "id": "Zm-Fy3oBVXS07smalpNN"
},
{
    "dpto": "15",
    "codigo": "15835",
    "nombre": "TURMEQUÉ",
    "id": "SW-Fy3oBVXS07sma-JQo"
},
{
    "dpto": "41",
    "codigo": "41518",
    "nombre": "PAICOL",
    "id": "aG-Fy3oBVXS07smamZNo"
},
{
    "dpto": "15",
    "codigo": "15839",
    "nombre": "TUTAZÁ",
    "id": "S2-Fy3oBVXS07sma-5SR"
},
{
    "dpto": "41",
    "codigo": "41530",
    "nombre": "PALESTINA",
    "id": "am-Fy3oBVXS07smamZNo"
},
{
    "dpto": "15",
    "codigo": "15223",
    "nombre": "CUBARÁ",
    "id": "TW-Fy3oBVXS07sma-5SR"
},
{
    "dpto": "41",
    "codigo": "41551",
    "nombre": "PITALITO",
    "id": "bG-Fy3oBVXS07smamZNo"
},
{
    "dpto": "25",
    "codigo": "25019",
    "nombre": "ALBÁN",
    "id": "T2-Fy3oBVXS07sma-5SR"
},
{
    "dpto": "41",
    "codigo": "41660",
    "nombre": "SALADOBLANCO",
    "id": "bm-Fy3oBVXS07smamZNo"
},
{
    "dpto": "25",
    "codigo": "25040",
    "nombre": "ANOLAIMA",
    "id": "UW-Fy3oBVXS07sma-5SR"
},
{
    "dpto": "41",
    "codigo": "41676",
    "nombre": "SANTA MARÍA",
    "id": "gG-Fy3oBVXS07smanJPJ"
},
{
    "dpto": "25",
    "codigo": "25086",
    "nombre": "BELTRÁN",
    "id": "U2-Fy3oBVXS07sma-5SR"
},
{
    "dpto": "41",
    "codigo": "41791",
    "nombre": "TARQUI",
    "id": "gm-Fy3oBVXS07smanJPJ"
},
{
    "dpto": "25",
    "codigo": "25099",
    "nombre": "BOJACÁ",
    "id": "YW-Gy3oBVXS07smaApQO"
},
{
    "dpto": "41",
    "codigo": "41799",
    "nombre": "TELLO",
    "id": "hG-Fy3oBVXS07smanJPJ"
},
{
    "dpto": "25",
    "codigo": "25123",
    "nombre": "CACHIPAY",
    "id": "Y2-Gy3oBVXS07smaApQO"
},
{
    "dpto": "41",
    "codigo": "41807",
    "nombre": "TIMANÁ",
    "id": "hm-Fy3oBVXS07smanJPJ"
},
{
    "dpto": "25",
    "codigo": "25736",
    "nombre": "SESQUILÉ",
    "id": "ZW-Gy3oBVXS07smaApQO"
},
{
    "dpto": "41",
    "codigo": "41885",
    "nombre": "YAGUARÁ",
    "id": "iG-Fy3oBVXS07smanJPJ"
},
{
    "dpto": "25",
    "codigo": "25745",
    "nombre": "SIMIJACA",
    "id": "Z2-Gy3oBVXS07smaApQO"
},
{
    "dpto": "41",
    "codigo": "41244",
    "nombre": "ELÍAS",
    "id": "im-Fy3oBVXS07smanJPJ"
},
{
    "dpto": "25",
    "codigo": "25769",
    "nombre": "SUBACHOQUE",
    "id": "aW-Gy3oBVXS07smaApQO"
},
{
    "dpto": "41",
    "codigo": "41132",
    "nombre": "CAMPOALEGRE",
    "id": "jG-Fy3oBVXS07smaoJNj"
},
{
    "dpto": "25",
    "codigo": "25777",
    "nombre": "SUPATÁ",
    "id": "bG-Gy3oBVXS07smaBpQz"
},
{
    "dpto": "41",
    "codigo": "41026",
    "nombre": "ALTAMIRA",
    "id": "jm-Fy3oBVXS07smaoJNj"
},
{
    "dpto": "25",
    "codigo": "25781",
    "nombre": "SUTATAUSA",
    "id": "bm-Gy3oBVXS07smaBpQz"
},
{
    "dpto": "41",
    "codigo": "41016",
    "nombre": "AIPE",
    "id": "kG-Fy3oBVXS07smaoJNj"
},
{
    "dpto": "25",
    "codigo": "25793",
    "nombre": "TAUSA",
    "id": "cG-Gy3oBVXS07smaBpQz"
},
{
    "dpto": "41",
    "codigo": "41006",
    "nombre": "ACEVEDO",
    "id": "km-Fy3oBVXS07smaoJNj"
},
{
    "dpto": "25",
    "codigo": "25799",
    "nombre": "TENJO",
    "id": "cm-Gy3oBVXS07smaBpQz"
},
{
    "dpto": "18",
    "codigo": "18029",
    "nombre": "ALBANIA",
    "id": "lm-Fy3oBVXS07smaqJN2"
},
{
    "dpto": "25",
    "codigo": "25807",
    "nombre": "TIBIRITA",
    "id": "dG-Gy3oBVXS07smaBpQz"
},
{
    "dpto": "18",
    "codigo": "18247",
    "nombre": "EL DONCELLO",
    "id": "mG-Fy3oBVXS07smaqJN2"
},
{
    "dpto": "25",
    "codigo": "25817",
    "nombre": "TOCANCIPÁ",
    "id": "dm-Gy3oBVXS07smaBpQz"
},
{
    "dpto": "18",
    "codigo": "18410",
    "nombre": "LA MONTAÑITA",
    "id": "mm-Fy3oBVXS07smarJNO"
},
{
    "dpto": "25",
    "codigo": "25839",
    "nombre": "UBALÁ",
    "id": "eG-Gy3oBVXS07smaCZSk"
},
{
    "dpto": "18",
    "codigo": "18479",
    "nombre": "MORELIA",
    "id": "qW-Fy3oBVXS07smasJP3"
},
{
    "dpto": "25",
    "codigo": "25843",
    "nombre": "VILLA DE SAN DIEGO DE UBATÉ",
    "id": "em-Gy3oBVXS07smaCZSk"
},
{
    "dpto": "18",
    "codigo": "18860",
    "nombre": "VALPARAÍSO",
    "id": "q2-Fy3oBVXS07smasJP3"
},
{
    "dpto": "25",
    "codigo": "25851",
    "nombre": "ÚTICA",
    "id": "fG-Gy3oBVXS07smaCZSk"
},
{
    "dpto": "18",
    "codigo": "18753",
    "nombre": "SAN VICENTE DEL CAGUÁN",
    "id": "rm-Fy3oBVXS07smavpNe"
},
{
    "dpto": "25",
    "codigo": "25867",
    "nombre": "VIANÍ",
    "id": "fm-Gy3oBVXS07smaDpRx"
},
{
    "dpto": "47",
    "codigo": "47720",
    "nombre": "SANTA BÁRBARA DE PINTO",
    "id": "sG-Fy3oBVXS07smawZO7"
},
{
    "dpto": "25",
    "codigo": "25873",
    "nombre": "VILLAPINZÓN",
    "id": "gG-Gy3oBVXS07smaDpRx"
},
{
    "dpto": "47",
    "codigo": "47798",
    "nombre": "TENERIFE",
    "id": "sm-Fy3oBVXS07smawZO7"
},
{
    "dpto": "25",
    "codigo": "25878",
    "nombre": "VIOTÁ",
    "id": "gm-Gy3oBVXS07smaDpRx"
},
{
    "dpto": "47",
    "codigo": "47980",
    "nombre": "ZONA BANANERA",
    "id": "tG-Fy3oBVXS07smawZO7"
},
{
    "dpto": "25",
    "codigo": "25898",
    "nombre": "ZIPACÓN",
    "id": "hG-Gy3oBVXS07smaDpRx"
},
{
    "dpto": "47",
    "codigo": "47189",
    "nombre": "CIÉNAGA",
    "id": "xG-Fy3oBVXS07smaxZOS"
},
{
    "dpto": "25",
    "codigo": "25001",
    "nombre": "AGUA DE DIOS",
    "id": "k2-Gy3oBVXS07smaE5Q3"
},
{
    "dpto": "17",
    "codigo": "17653",
    "nombre": "SALAMINA",
    "id": "wG-Gy3oBVXS07smaJpRI"
},
{
    "dpto": "25",
    "codigo": "25754",
    "nombre": "SOACHA",
    "id": "lW-Gy3oBVXS07smaE5Q3"
},
{
    "dpto": "25",
    "codigo": "25126",
    "nombre": "CAJICÁ",
    "id": "wm-Gy3oBVXS07smaJpRI"
},
{
    "dpto": "25",
    "codigo": "25260",
    "nombre": "EL ROSAL",
    "id": "l2-Gy3oBVXS07smaE5Q3"
},
{
    "dpto": "25",
    "codigo": "25151",
    "nombre": "CÁQUEZA",
    "id": "xG-Gy3oBVXS07smaJpRI"
},
{
    "dpto": "25",
    "codigo": "25740",
    "nombre": "SIBATÉ",
    "id": "mW-Gy3oBVXS07smaE5Q3"
},
{
    "dpto": "25",
    "codigo": "25168",
    "nombre": "CHAGUANÍ",
    "id": "xm-Gy3oBVXS07smaJpRI"
},
{
    "dpto": "25",
    "codigo": "25662",
    "nombre": "SAN JUAN DE RIOSECO",
    "id": "m2-Gy3oBVXS07smaE5Q3"
},
{
    "dpto": "25",
    "codigo": "25181",
    "nombre": "CHOACHÍ",
    "id": "yG-Gy3oBVXS07smaKZSo"
},
{
    "dpto": "17",
    "codigo": "17013",
    "nombre": "AGUADAS",
    "id": "n2-Gy3oBVXS07smaGpSe"
},
{
    "dpto": "25",
    "codigo": "25200",
    "nombre": "COGUA",
    "id": "ym-Gy3oBVXS07smaKZSo"
},
{
    "dpto": "17",
    "codigo": "17446",
    "nombre": "MARULANDA",
    "id": "oW-Gy3oBVXS07smaGpSe"
},
{
    "dpto": "25",
    "codigo": "25224",
    "nombre": "CUCUNUBÁ",
    "id": "zG-Gy3oBVXS07smaKZSo"
},
{
    "dpto": "17",
    "codigo": "17442",
    "nombre": "MARMATO",
    "id": "o2-Gy3oBVXS07smaGpSe"
},
{
    "dpto": "17",
    "codigo": "17877",
    "nombre": "VITERBO",
    "id": "rW-Gy3oBVXS07smaIpTa"
},
{
    "dpto": "17",
    "codigo": "17388",
    "nombre": "LA MERCED",
    "id": "pW-Gy3oBVXS07smaH5Qm"
},
{
    "dpto": "17",
    "codigo": "17867",
    "nombre": "VICTORIA",
    "id": "r2-Gy3oBVXS07smaIpTa"
},
{
    "dpto": "17",
    "codigo": "17272",
    "nombre": "FILADELFIA",
    "id": "p2-Gy3oBVXS07smaH5Qm"
},
{
    "dpto": "17",
    "codigo": "17665",
    "nombre": "SAN JOSÉ",
    "id": "sW-Gy3oBVXS07smaIpTa"
},
{
    "dpto": "17",
    "codigo": "17088",
    "nombre": "BELALCÁZAR",
    "id": "qW-Gy3oBVXS07smaH5Qm"
},
{
    "dpto": "17",
    "codigo": "17388",
    "nombre": "LA MERCED",
    "id": "pW-Gy3oBVXS07smaH5Qm"
},
{
    "dpto": "17",
    "codigo": "17042",
    "nombre": "ANSERMA",
    "id": "q2-Gy3oBVXS07smaH5Qm"
},
{
    "dpto": "17",
    "codigo": "17272",
    "nombre": "FILADELFIA",
    "id": "p2-Gy3oBVXS07smaH5Qm"
},
{
    "dpto": "17",
    "codigo": "17877",
    "nombre": "VITERBO",
    "id": "rW-Gy3oBVXS07smaIpTa"
},
{
    "dpto": "17",
    "codigo": "17088",
    "nombre": "BELALCÁZAR",
    "id": "qW-Gy3oBVXS07smaH5Qm"
},
{
    "dpto": "17",
    "codigo": "17867",
    "nombre": "VICTORIA",
    "id": "r2-Gy3oBVXS07smaIpTa"
},
{
    "dpto": "17",
    "codigo": "17042",
    "nombre": "ANSERMA",
    "id": "q2-Gy3oBVXS07smaH5Qm"
},
{
    "dpto": "17",
    "codigo": "17665",
    "nombre": "SAN JOSÉ",
    "id": "sW-Gy3oBVXS07smaIpTa"
},
{
    "dpto": "25",
    "codigo": "25281",
    "nombre": "FOSCA",
    "id": "0m-Gy3oBVXS07smaMZRN"
},
{
    "dpto": "17",
    "codigo": "17653",
    "nombre": "SALAMINA",
    "id": "wG-Gy3oBVXS07smaJpRI"
},
{
    "dpto": "25",
    "codigo": "25288",
    "nombre": "FÚQUENE",
    "id": "1G-Gy3oBVXS07smaMZRN"
},
{
    "dpto": "25",
    "codigo": "25126",
    "nombre": "CAJICÁ",
    "id": "wm-Gy3oBVXS07smaJpRI"
},
{
    "dpto": "25",
    "codigo": "25295",
    "nombre": "GACHANCIPÁ",
    "id": "1m-Gy3oBVXS07smaMZRN"
},
{
    "dpto": "25",
    "codigo": "25151",
    "nombre": "CÁQUEZA",
    "id": "xG-Gy3oBVXS07smaJpRI"
},
{
    "dpto": "25",
    "codigo": "25299",
    "nombre": "GAMA",
    "id": "2G-Gy3oBVXS07smaMZRN"
},
{
    "dpto": "25",
    "codigo": "25168",
    "nombre": "CHAGUANÍ",
    "id": "xm-Gy3oBVXS07smaJpRI"
},
{
    "dpto": "25",
    "codigo": "25317",
    "nombre": "GUACHETÁ",
    "id": "2m-Gy3oBVXS07smaMZRN"
},
{
    "dpto": "25",
    "codigo": "25181",
    "nombre": "CHOACHÍ",
    "id": "yG-Gy3oBVXS07smaKZSo"
},
{
    "dpto": "25",
    "codigo": "25322",
    "nombre": "GUASCA",
    "id": "3G-Gy3oBVXS07smaMZRN"
},
{
    "dpto": "25",
    "codigo": "25200",
    "nombre": "COGUA",
    "id": "ym-Gy3oBVXS07smaKZSo"
},
{
    "dpto": "52",
    "codigo": "52352",
    "nombre": "ILES",
    "id": "Xm-Gy3oBVXS07smat5by"
},
{
    "dpto": "25",
    "codigo": "25224",
    "nombre": "CUCUNUBÁ",
    "id": "zG-Gy3oBVXS07smaKZSo"
},
{
    "dpto": "52",
    "codigo": "52356",
    "nombre": "IPIALES",
    "id": "YG-Gy3oBVXS07smat5by"
},
{
    "dpto": "25",
    "codigo": "25258",
    "nombre": "EL PEÑÓN",
    "id": "zm-Gy3oBVXS07smaKZSo"
},
{
    "dpto": "52",
    "codigo": "52233",
    "nombre": "CUMBITARA",
    "id": "Ym-Gy3oBVXS07smat5by"
},
{
    "dpto": "25",
    "codigo": "25279",
    "nombre": "FÓMEQUE",
    "id": "0G-Gy3oBVXS07smaKZSo"
},
{
    "dpto": "68",
    "codigo": "68872",
    "nombre": "VILLANUEVA",
    "id": "ZG-Gy3oBVXS07smat5by"
},
{
    "dpto": "25",
    "codigo": "25286",
    "nombre": "FUNZA",
    "id": "02-Gy3oBVXS07smaMZRN"
},
{
    "dpto": "68",
    "codigo": "68370",
    "nombre": "JORDÁN",
    "id": "Zm-Gy3oBVXS07smat5by"
},
{
    "dpto": "25",
    "codigo": "25293",
    "nombre": "GACHALÁ",
    "id": "1W-Gy3oBVXS07smaMZRN"
},
{
    "dpto": "68",
    "codigo": "68385",
    "nombre": "LANDÁZURI",
    "id": "aG-Gy3oBVXS07smat5by"
},
{
    "dpto": "25",
    "codigo": "25297",
    "nombre": "GACHETÁ",
    "id": "12-Gy3oBVXS07smaMZRN"
},
{
    "dpto": "17",
    "codigo": "17013",
    "nombre": "AGUADAS",
    "id": "n2-Gy3oBVXS07smaGpSe"
},
{
    "dpto": "25",
    "codigo": "25312",
    "nombre": "GRANADA",
    "id": "2W-Gy3oBVXS07smaMZRN"
},
{
    "dpto": "17",
    "codigo": "17446",
    "nombre": "MARULANDA",
    "id": "oW-Gy3oBVXS07smaGpSe"
},
{
    "dpto": "25",
    "codigo": "25320",
    "nombre": "GUADUAS",
    "id": "22-Gy3oBVXS07smaMZRN"
},
{
    "dpto": "17",
    "codigo": "17442",
    "nombre": "MARMATO",
    "id": "o2-Gy3oBVXS07smaGpSe"
},
{
    "dpto": "25",
    "codigo": "25324",
    "nombre": "GUATAQUÍ",
    "id": "62-Gy3oBVXS07smaPJTT"
},
{
    "dpto": "68",
    "codigo": "68573",
    "nombre": "PUERTO PARRA",
    "id": "eW-Gy3oBVXS07smavpae"
},
{
    "dpto": "25",
    "codigo": "25328",
    "nombre": "GUAYABAL DE SÍQUIMA",
    "id": "7W-Gy3oBVXS07smaPJTT"
},
{
    "dpto": "68",
    "codigo": "68211",
    "nombre": "CONTRATACIÓN",
    "id": "e2-Gy3oBVXS07smavpae"
},
{
    "dpto": "25",
    "codigo": "25339",
    "nombre": "GUTIÉRREZ",
    "id": "72-Gy3oBVXS07smaPJTT"
},
{
    "dpto": "68",
    "codigo": "68229",
    "nombre": "CURITÍ",
    "id": "fW-Gy3oBVXS07smavpae"
},
{
    "dpto": "25",
    "codigo": "25372",
    "nombre": "JUNÍN",
    "id": "8W-Gy3oBVXS07smaPJTT"
},
{
    "dpto": "68",
    "codigo": "68245",
    "nombre": "EL GUACAMAYO",
    "id": "f2-Gy3oBVXS07smavpae"
},
{
    "dpto": "25",
    "codigo": "25386",
    "nombre": "LA MESA",
    "id": "82-Gy3oBVXS07smaQJRP"
},
{
    "dpto": "68",
    "codigo": "68264",
    "nombre": "ENCINO",
    "id": "gW-Gy3oBVXS07smavpae"
},
{
    "dpto": "25",
    "codigo": "25398",
    "nombre": "LA PEÑA",
    "id": "9W-Gy3oBVXS07smaQJRP"
},
{
    "dpto": "68",
    "codigo": "68397",
    "nombre": "LA PAZ",
    "id": "aW-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "25",
    "codigo": "25407",
    "nombre": "LENGUAZAQUE",
    "id": "92-Gy3oBVXS07smaQJRP"
},
{
    "dpto": "68",
    "codigo": "68418",
    "nombre": "LOS SANTOS",
    "id": "a2-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "25",
    "codigo": "25430",
    "nombre": "MADRID",
    "id": "-W-Gy3oBVXS07smaQJRP"
},
{
    "dpto": "68",
    "codigo": "68432",
    "nombre": "MÁLAGA",
    "id": "bW-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "25",
    "codigo": "25438",
    "nombre": "MEDINA",
    "id": "-2-Gy3oBVXS07smaQJRP"
},
{
    "dpto": "68",
    "codigo": "68464",
    "nombre": "MOGOTES",
    "id": "b2-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "25",
    "codigo": "25486",
    "nombre": "NEMOCÓN",
    "id": "AG-Gy3oBVXS07smaQ5XK"
},
{
    "dpto": "68",
    "codigo": "68498",
    "nombre": "OCAMONTE",
    "id": "cW-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "25",
    "codigo": "25489",
    "nombre": "NIMAIMA",
    "id": "Am-Gy3oBVXS07smaQ5XK"
},
{
    "dpto": "68",
    "codigo": "68502",
    "nombre": "ONZAGA",
    "id": "c2-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "25",
    "codigo": "25506",
    "nombre": "VENECIA",
    "id": "BG-Gy3oBVXS07smaQ5XK"
},
{
    "dpto": "68",
    "codigo": "68524",
    "nombre": "PALMAS DEL SOCORRO",
    "id": "dW-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "25",
    "codigo": "25518",
    "nombre": "PAIME",
    "id": "Bm-Gy3oBVXS07smaQ5XK"
},
{
    "dpto": "68",
    "codigo": "68549",
    "nombre": "PINCHOTE",
    "id": "d2-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "25",
    "codigo": "25530",
    "nombre": "PARATEBUENO",
    "id": "CG-Gy3oBVXS07smaRpXx"
},
{
    "dpto": "15",
    "codigo": "15180",
    "nombre": "CHISCAS",
    "id": "TG-Fy3oBVXS07sma-5SR"
},
{
    "dpto": "25",
    "codigo": "25572",
    "nombre": "PUERTO SALGAR",
    "id": "Cm-Gy3oBVXS07smaRpXx"
},
{
    "dpto": "15",
    "codigo": "15332",
    "nombre": "GÜICÁN DE LA SIERRA",
    "id": "Tm-Fy3oBVXS07sma-5SR"
},
{
    "dpto": "25",
    "codigo": "25592",
    "nombre": "QUEBRADANEGRA",
    "id": "DG-Gy3oBVXS07smaRpXx"
},
{
    "dpto": "25",
    "codigo": "25035",
    "nombre": "ANAPOIMA",
    "id": "UG-Fy3oBVXS07sma-5SR"
},
{
    "dpto": "25",
    "codigo": "25596",
    "nombre": "QUIPILE",
    "id": "Dm-Gy3oBVXS07smaRpXx"
},
{
    "dpto": "25",
    "codigo": "25053",
    "nombre": "ARBELÁEZ",
    "id": "Um-Fy3oBVXS07sma-5SR"
},
{
    "dpto": "25",
    "codigo": "25612",
    "nombre": "RICAURTE",
    "id": "EG-Gy3oBVXS07smaRpXx"
},
{
    "dpto": "25",
    "codigo": "25258",
    "nombre": "EL PEÑÓN",
    "id": "zm-Gy3oBVXS07smaKZSo"
},
{
    "dpto": "25",
    "codigo": "25649",
    "nombre": "SAN BERNARDO",
    "id": "Em-Gy3oBVXS07smaRpXx"
},
{
    "dpto": "25",
    "codigo": "25279",
    "nombre": "FÓMEQUE",
    "id": "0G-Gy3oBVXS07smaKZSo"
},
{
    "dpto": "25",
    "codigo": "25658",
    "nombre": "SAN FRANCISCO",
    "id": "JG-Gy3oBVXS07smaTpVP"
},
{
    "dpto": "25",
    "codigo": "25326",
    "nombre": "GUATAVITA",
    "id": "7G-Gy3oBVXS07smaPJTT"
},
{
    "dpto": "15",
    "codigo": "15135",
    "nombre": "CAMPOHERMOSO",
    "id": "Jm-Gy3oBVXS07smaTpVP"
},
{
    "dpto": "25",
    "codigo": "25335",
    "nombre": "GUAYABETAL",
    "id": "7m-Gy3oBVXS07smaPJTT"
},
{
    "dpto": "15",
    "codigo": "15172",
    "nombre": "CHINAVITA",
    "id": "KG-Gy3oBVXS07smaTpVP"
},
{
    "dpto": "25",
    "codigo": "25368",
    "nombre": "JERUSALÉN",
    "id": "8G-Gy3oBVXS07smaPJTT"
},
{
    "dpto": "15",
    "codigo": "15183",
    "nombre": "CHITA",
    "id": "Km-Gy3oBVXS07smaTpVP"
},
{
    "dpto": "25",
    "codigo": "25377",
    "nombre": "LA CALERA",
    "id": "8m-Gy3oBVXS07smaPJTT"
},
{
    "dpto": "15",
    "codigo": "15187",
    "nombre": "CHIVATÁ",
    "id": "LG-Gy3oBVXS07smaTpVP"
},
{
    "dpto": "25",
    "codigo": "25394",
    "nombre": "LA PALMA",
    "id": "9G-Gy3oBVXS07smaQJRP"
},
{
    "dpto": "15",
    "codigo": "15204",
    "nombre": "CÓMBITA",
    "id": "Lm-Gy3oBVXS07smaUZWz"
},
{
    "dpto": "25",
    "codigo": "25402",
    "nombre": "LA VEGA",
    "id": "9m-Gy3oBVXS07smaQJRP"
},
{
    "dpto": "15",
    "codigo": "15215",
    "nombre": "CORRALES",
    "id": "MG-Gy3oBVXS07smaUZWz"
},
{
    "dpto": "25",
    "codigo": "25426",
    "nombre": "MACHETÁ",
    "id": "-G-Gy3oBVXS07smaQJRP"
},
{
    "dpto": "15",
    "codigo": "15380",
    "nombre": "LA CAPILLA",
    "id": "Mm-Gy3oBVXS07smaUZWz"
},
{
    "dpto": "25",
    "codigo": "25436",
    "nombre": "MANTA",
    "id": "-m-Gy3oBVXS07smaQJRP"
},
{
    "dpto": "15",
    "codigo": "15403",
    "nombre": "LA UVITA",
    "id": "NG-Gy3oBVXS07smaUZWz"
},
{
    "dpto": "25",
    "codigo": "25483",
    "nombre": "NARIÑO",
    "id": "_2-Gy3oBVXS07smaQ5TK"
},
{
    "dpto": "15",
    "codigo": "15550",
    "nombre": "PISBA",
    "id": "Nm-Gy3oBVXS07smaUZWz"
},
{
    "dpto": "25",
    "codigo": "25488",
    "nombre": "NILO",
    "id": "AW-Gy3oBVXS07smaQ5XK"
},
{
    "dpto": "15",
    "codigo": "15580",
    "nombre": "QUÍPAMA",
    "id": "OG-Gy3oBVXS07smaUZWz"
},
{
    "dpto": "25",
    "codigo": "25491",
    "nombre": "NOCAIMA",
    "id": "A2-Gy3oBVXS07smaQ5XK"
},
{
    "dpto": "15",
    "codigo": "15757",
    "nombre": "SOCHA",
    "id": "O2-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "25",
    "codigo": "25513",
    "nombre": "PACHO",
    "id": "BW-Gy3oBVXS07smaQ5XK"
},
{
    "dpto": "15",
    "codigo": "15761",
    "nombre": "SOMONDOCO",
    "id": "PW-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "25",
    "codigo": "25524",
    "nombre": "PANDI",
    "id": "B2-Gy3oBVXS07smaRpXx"
},
{
    "dpto": "15",
    "codigo": "15861",
    "nombre": "VENTAQUEMADA",
    "id": "P2-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "25",
    "codigo": "25535",
    "nombre": "PASCA",
    "id": "CW-Gy3oBVXS07smaRpXx"
},
{
    "dpto": "15",
    "codigo": "15897",
    "nombre": "ZETAQUIRA",
    "id": "QW-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "25",
    "codigo": "25580",
    "nombre": "PULÍ",
    "id": "C2-Gy3oBVXS07smaRpXx"
},
{
    "dpto": "15",
    "codigo": "15224",
    "nombre": "CUCAITA",
    "id": "Q2-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "25",
    "codigo": "25594",
    "nombre": "QUETAME",
    "id": "DW-Gy3oBVXS07smaRpXx"
},
{
    "dpto": "15",
    "codigo": "15232",
    "nombre": "CHÍQUIZA",
    "id": "RW-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "25",
    "codigo": "25599",
    "nombre": "APULO",
    "id": "D2-Gy3oBVXS07smaRpXx"
},
{
    "dpto": "15",
    "codigo": "15238",
    "nombre": "DUITAMA",
    "id": "R2-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "25",
    "codigo": "25645",
    "nombre": "SAN ANTONIO DEL TEQUENDAMA",
    "id": "EW-Gy3oBVXS07smaRpXx"
},
{
    "dpto": "15",
    "codigo": "15248",
    "nombre": "EL ESPINO",
    "id": "SW-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "25",
    "codigo": "25653",
    "nombre": "SAN CAYETANO",
    "id": "I2-Gy3oBVXS07smaTpVP"
},
{
    "dpto": "15",
    "codigo": "15276",
    "nombre": "FLORESTA",
    "id": "S2-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "15",
    "codigo": "15131",
    "nombre": "CALDAS",
    "id": "JW-Gy3oBVXS07smaTpVP"
},
{
    "dpto": "15",
    "codigo": "15296",
    "nombre": "GÁMEZA",
    "id": "TW-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "15",
    "codigo": "15162",
    "nombre": "CERINZA",
    "id": "J2-Gy3oBVXS07smaTpVP"
},
{
    "dpto": "15",
    "codigo": "15317",
    "nombre": "GUACAMAYAS",
    "id": "T2-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "15",
    "codigo": "15176",
    "nombre": "CHIQUINQUIRÁ",
    "id": "KW-Gy3oBVXS07smaTpVP"
},
{
    "dpto": "15",
    "codigo": "15325",
    "nombre": "GUAYATÁ",
    "id": "UW-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "15",
    "codigo": "15185",
    "nombre": "CHITARAQUE",
    "id": "K2-Gy3oBVXS07smaTpVP"
},
{
    "dpto": "15",
    "codigo": "15367",
    "nombre": "JENESANO",
    "id": "U2-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "15",
    "codigo": "15189",
    "nombre": "CIÉNEGA",
    "id": "LW-Gy3oBVXS07smaTpVP"
},
{
    "dpto": "15",
    "codigo": "15407",
    "nombre": "VILLA DE LEYVA",
    "id": "VW-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "15",
    "codigo": "15212",
    "nombre": "COPER",
    "id": "L2-Gy3oBVXS07smaUZWz"
},
{
    "dpto": "15",
    "codigo": "15442",
    "nombre": "MARIPÍ",
    "id": "V2-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "15",
    "codigo": "15377",
    "nombre": "LABRANZAGRANDE",
    "id": "MW-Gy3oBVXS07smaUZWz"
},
{
    "dpto": "15",
    "codigo": "15464",
    "nombre": "MONGUA",
    "id": "WW-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "15",
    "codigo": "15401",
    "nombre": "LA VICTORIA",
    "id": "M2-Gy3oBVXS07smaUZWz"
},
{
    "dpto": "15",
    "codigo": "15469",
    "nombre": "MONIQUIRÁ",
    "id": "aW-Gy3oBVXS07smaXpXr"
},
{
    "dpto": "15",
    "codigo": "15542",
    "nombre": "PESCA",
    "id": "NW-Gy3oBVXS07smaUZWz"
},
{
    "dpto": "52",
    "codigo": "52385",
    "nombre": "LA LLANADA",
    "id": "a2-Gy3oBVXS07smaXpXr"
},
{
    "dpto": "15",
    "codigo": "15572",
    "nombre": "PUERTO BOYACÁ",
    "id": "N2-Gy3oBVXS07smaUZWz"
},
{
    "dpto": "52",
    "codigo": "52540",
    "nombre": "POLICARPA",
    "id": "bW-Gy3oBVXS07smaXpXr"
},
{
    "dpto": "15",
    "codigo": "15755",
    "nombre": "SOCOTÁ",
    "id": "OW-Gy3oBVXS07smaUZWz"
},
{
    "dpto": "52",
    "codigo": "52079",
    "nombre": "BARBACOAS",
    "id": "b2-Gy3oBVXS07smaXpXr"
},
{
    "dpto": "15",
    "codigo": "15759",
    "nombre": "SOGAMOSO",
    "id": "PG-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "52",
    "codigo": "52390",
    "nombre": "LA TOLA",
    "id": "cW-Gy3oBVXS07smaYpWb"
},
{
    "dpto": "15",
    "codigo": "15842",
    "nombre": "ÚMBITA",
    "id": "Pm-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "52",
    "codigo": "52473",
    "nombre": "MOSQUERA",
    "id": "c2-Gy3oBVXS07smaYpWb"
},
{
    "dpto": "15",
    "codigo": "15879",
    "nombre": "VIRACACHÁ",
    "id": "QG-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "52",
    "codigo": "52520",
    "nombre": "FRANCISCO PIZARRO",
    "id": "dW-Gy3oBVXS07smaZpVY"
},
{
    "dpto": "15",
    "codigo": "15218",
    "nombre": "COVARACHÍA",
    "id": "Qm-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "52",
    "codigo": "52696",
    "nombre": "SANTA BÁRBARA",
    "id": "d2-Gy3oBVXS07smaZpVY"
},
{
    "dpto": "15",
    "codigo": "15226",
    "nombre": "CUÍTIVA",
    "id": "RG-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "54",
    "codigo": "54001",
    "nombre": "SAN JOSÉ DE CÚCUTA",
    "id": "e2-Gy3oBVXS07smaapUB"
},
{
    "dpto": "15",
    "codigo": "15236",
    "nombre": "CHIVOR",
    "id": "Rm-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "54",
    "codigo": "54051",
    "nombre": "ARBOLEDAS",
    "id": "fW-Gy3oBVXS07smaapUB"
},
{
    "dpto": "15",
    "codigo": "15244",
    "nombre": "EL COCUY",
    "id": "SG-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "54",
    "codigo": "54109",
    "nombre": "BUCARASICA",
    "id": "f2-Gy3oBVXS07smaapUB"
},
{
    "dpto": "15",
    "codigo": "15272",
    "nombre": "FIRAVITOBA",
    "id": "Sm-Gy3oBVXS07smaVJW5"
},
{
    "dpto": "54",
    "codigo": "54128",
    "nombre": "CÁCHIRA",
    "id": "gW-Gy3oBVXS07smaapUB"
},
{
    "dpto": "15",
    "codigo": "15293",
    "nombre": "GACHANTIVÁ",
    "id": "TG-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "54",
    "codigo": "54223",
    "nombre": "CUCUTILLA",
    "id": "g2-Gy3oBVXS07smabZVX"
},
{
    "dpto": "15",
    "codigo": "15299",
    "nombre": "GARAGOA",
    "id": "Tm-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "54",
    "codigo": "54261",
    "nombre": "EL ZULIA",
    "id": "hW-Gy3oBVXS07smabZVX"
},
{
    "dpto": "15",
    "codigo": "15322",
    "nombre": "GUATEQUE",
    "id": "UG-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "54",
    "codigo": "54344",
    "nombre": "HACARÍ",
    "id": "h2-Gy3oBVXS07smabZVX"
},
{
    "dpto": "15",
    "codigo": "15362",
    "nombre": "IZA",
    "id": "Um-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "54",
    "codigo": "54377",
    "nombre": "LABATECA",
    "id": "iW-Gy3oBVXS07smabZVX"
},
{
    "dpto": "15",
    "codigo": "15368",
    "nombre": "JERICÓ",
    "id": "VG-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "54",
    "codigo": "54398",
    "nombre": "LA PLAYA",
    "id": "i2-Gy3oBVXS07smabZVX"
},
{
    "dpto": "15",
    "codigo": "15425",
    "nombre": "MACANAL",
    "id": "Vm-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "54",
    "codigo": "54418",
    "nombre": "LOURDES",
    "id": "jW-Gy3oBVXS07smacZVV"
},
{
    "dpto": "15",
    "codigo": "15455",
    "nombre": "MIRAFLORES",
    "id": "WG-Gy3oBVXS07smaWZV0"
},
{
    "dpto": "54",
    "codigo": "54498",
    "nombre": "OCAÑA",
    "id": "j2-Gy3oBVXS07smacZVV"
},
{
    "dpto": "15",
    "codigo": "15466",
    "nombre": "MONGUÍ",
    "id": "aG-Gy3oBVXS07smaXpXr"
},
{
    "dpto": "54",
    "codigo": "54520",
    "nombre": "PAMPLONITA",
    "id": "kW-Gy3oBVXS07smacZVV"
},
{
    "dpto": "52",
    "codigo": "52256",
    "nombre": "EL ROSARIO",
    "id": "am-Gy3oBVXS07smaXpXr"
},
{
    "dpto": "54",
    "codigo": "54599",
    "nombre": "RAGONVALIA",
    "id": "k2-Gy3oBVXS07smacZVV"
},
{
    "dpto": "52",
    "codigo": "52418",
    "nombre": "LOS ANDES",
    "id": "bG-Gy3oBVXS07smaXpXr"
},
{
    "dpto": "54",
    "codigo": "54670",
    "nombre": "SAN CALIXTO",
    "id": "lW-Gy3oBVXS07smacZVV"
},
{
    "dpto": "52",
    "codigo": "52612",
    "nombre": "RICAURTE",
    "id": "bm-Gy3oBVXS07smaXpXr"
},
{
    "dpto": "54",
    "codigo": "54680",
    "nombre": "SANTIAGO",
    "id": "pG-Gy3oBVXS07smadJW7"
},
{
    "dpto": "52",
    "codigo": "52250",
    "nombre": "EL CHARCO",
    "id": "cG-Gy3oBVXS07smaYpWb"
},
{
    "dpto": "54",
    "codigo": "54743",
    "nombre": "SILOS",
    "id": "pm-Gy3oBVXS07smadJW7"
},
{
    "dpto": "52",
    "codigo": "52427",
    "nombre": "MAGÜÍ",
    "id": "cm-Gy3oBVXS07smaYpWb"
},
{
    "dpto": "54",
    "codigo": "54874",
    "nombre": "VILLA DEL ROSARIO",
    "id": "qG-Gy3oBVXS07smadJW7"
},
{
    "dpto": "52",
    "codigo": "52490",
    "nombre": "OLAYA HERRERA",
    "id": "dG-Gy3oBVXS07smaYpWb"
},
{
    "dpto": "54",
    "codigo": "54206",
    "nombre": "CONVENCIÓN",
    "id": "qm-Gy3oBVXS07smaeJX0"
},
{
    "dpto": "52",
    "codigo": "52621",
    "nombre": "ROBERTO PAYÁN",
    "id": "dm-Gy3oBVXS07smaZpVY"
},
{
    "dpto": "54",
    "codigo": "54800",
    "nombre": "TEORAMA",
    "id": "rG-Gy3oBVXS07smaeJX0"
},
{
    "dpto": "52",
    "codigo": "52835",
    "nombre": "SAN ANDRÉS DE TUMACO",
    "id": "eG-Gy3oBVXS07smaZpVY"
},
{
    "dpto": "54",
    "codigo": "54820",
    "nombre": "TOLEDO",
    "id": "sG-Gy3oBVXS07smafJVD"
},
{
    "dpto": "54",
    "codigo": "54003",
    "nombre": "ÁBREGO",
    "id": "fG-Gy3oBVXS07smaapUB"
},
{
    "dpto": "68",
    "codigo": "68013",
    "nombre": "AGUADA",
    "id": "sm-Gy3oBVXS07smafJVD"
},
{
    "dpto": "54",
    "codigo": "54099",
    "nombre": "BOCHALEMA",
    "id": "fm-Gy3oBVXS07smaapUB"
},
{
    "dpto": "68",
    "codigo": "68051",
    "nombre": "ARATOCA",
    "id": "tG-Gy3oBVXS07smafJVD"
},
{
    "dpto": "54",
    "codigo": "54125",
    "nombre": "CÁCOTA",
    "id": "gG-Gy3oBVXS07smaapUB"
},
{
    "dpto": "17",
    "codigo": "17524",
    "nombre": "PALESTINA",
    "id": "tm-Gy3oBVXS07smafJVD"
},
{
    "dpto": "54",
    "codigo": "54172",
    "nombre": "CHINÁCOTA",
    "id": "gm-Gy3oBVXS07smabZVX"
},
{
    "dpto": "17",
    "codigo": "17614",
    "nombre": "RIOSUCIO",
    "id": "uG-Gy3oBVXS07smaf5Wd"
},
{
    "dpto": "54",
    "codigo": "54239",
    "nombre": "DURANIA",
    "id": "hG-Gy3oBVXS07smabZVX"
},
{
    "dpto": "63",
    "codigo": "63111",
    "nombre": "BUENAVISTA",
    "id": "um-Gy3oBVXS07smaf5Wd"
},
{
    "dpto": "54",
    "codigo": "54313",
    "nombre": "GRAMALOTE",
    "id": "hm-Gy3oBVXS07smabZVX"
},
{
    "dpto": "63",
    "codigo": "63190",
    "nombre": "CIRCASIA",
    "id": "vG-Gy3oBVXS07smaf5Wd"
},
{
    "dpto": "54",
    "codigo": "54347",
    "nombre": "HERRÁN",
    "id": "iG-Gy3oBVXS07smabZVX"
},
{
    "dpto": "63",
    "codigo": "63272",
    "nombre": "FILANDIA",
    "id": "vm-Gy3oBVXS07smaf5Wd"
},
{
    "dpto": "54",
    "codigo": "54385",
    "nombre": "LA ESPERANZA",
    "id": "im-Gy3oBVXS07smabZVX"
},
{
    "dpto": "63",
    "codigo": "63401",
    "nombre": "LA TEBAIDA",
    "id": "wG-Gy3oBVXS07smaf5Wd"
},
{
    "dpto": "54",
    "codigo": "54405",
    "nombre": "LOS PATIOS",
    "id": "jG-Gy3oBVXS07smacZVV"
},
{
    "dpto": "63",
    "codigo": "63548",
    "nombre": "PIJAO",
    "id": "wm-Gy3oBVXS07smaf5Wd"
},
{
    "dpto": "54",
    "codigo": "54480",
    "nombre": "MUTISCUA",
    "id": "jm-Gy3oBVXS07smacZVV"
},
{
    "dpto": "63",
    "codigo": "63690",
    "nombre": "SALENTO",
    "id": "xG-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "54",
    "codigo": "54518",
    "nombre": "PAMPLONA",
    "id": "kG-Gy3oBVXS07smacZVV"
},
{
    "dpto": "66",
    "codigo": "66045",
    "nombre": "APÍA",
    "id": "xm-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "54",
    "codigo": "54553",
    "nombre": "PUERTO SANTANDER",
    "id": "km-Gy3oBVXS07smacZVV"
},
{
    "dpto": "66",
    "codigo": "66088",
    "nombre": "BELÉN DE UMBRÍA",
    "id": "yG-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "54",
    "codigo": "54660",
    "nombre": "SALAZAR",
    "id": "lG-Gy3oBVXS07smacZVV"
},
{
    "dpto": "66",
    "codigo": "66318",
    "nombre": "GUÁTICA",
    "id": "ym-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "54",
    "codigo": "54673",
    "nombre": "SAN CAYETANO",
    "id": "o2-Gy3oBVXS07smadJW7"
},
{
    "dpto": "66",
    "codigo": "66400",
    "nombre": "LA VIRGINIA",
    "id": "zG-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "54",
    "codigo": "54720",
    "nombre": "SARDINATA",
    "id": "pW-Gy3oBVXS07smadJW7"
},
{
    "dpto": "66",
    "codigo": "66456",
    "nombre": "MISTRATÓ",
    "id": "zm-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "54",
    "codigo": "54871",
    "nombre": "VILLA CARO",
    "id": "p2-Gy3oBVXS07smadJW7"
},
{
    "dpto": "66",
    "codigo": "66594",
    "nombre": "QUINCHÍA",
    "id": "0G-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "54",
    "codigo": "54174",
    "nombre": "CHITAGÁ",
    "id": "qW-Gy3oBVXS07smadJW7"
},
{
    "dpto": "66",
    "codigo": "66687",
    "nombre": "SANTUARIO",
    "id": "4m-Gy3oBVXS07smaiJUW"
},
{
    "dpto": "54",
    "codigo": "54245",
    "nombre": "EL CARMEN",
    "id": "q2-Gy3oBVXS07smaeJX0"
},
{
    "dpto": "73",
    "codigo": "73024",
    "nombre": "ALPUJARRA",
    "id": "5G-Gy3oBVXS07smaiJUW"
},
{
    "dpto": "54",
    "codigo": "54810",
    "nombre": "TIBÚ",
    "id": "rW-Gy3oBVXS07smaeJX0"
},
{
    "dpto": "73",
    "codigo": "73030",
    "nombre": "AMBALEMA",
    "id": "5m-Gy3oBVXS07smaiJUW"
},
{
    "dpto": "54",
    "codigo": "54250",
    "nombre": "EL TARRA",
    "id": "sW-Gy3oBVXS07smafJVD"
},
{
    "dpto": "73",
    "codigo": "73055",
    "nombre": "ARMERO",
    "id": "6G-Gy3oBVXS07smaiJUW"
},
{
    "dpto": "68",
    "codigo": "68020",
    "nombre": "ALBANIA",
    "id": "s2-Gy3oBVXS07smafJVD"
},
{
    "dpto": "73",
    "codigo": "73124",
    "nombre": "CAJAMARCA",
    "id": "6m-Gy3oBVXS07smajJUm"
},
{
    "dpto": "17",
    "codigo": "17541",
    "nombre": "PENSILVANIA",
    "id": "tW-Gy3oBVXS07smafJVD"
},
{
    "dpto": "73",
    "codigo": "73152",
    "nombre": "CASABIANCA",
    "id": "7G-Gy3oBVXS07smajJUm"
},
{
    "dpto": "17",
    "codigo": "17513",
    "nombre": "PÁCORA",
    "id": "t2-Gy3oBVXS07smafJVD"
},
{
    "dpto": "73",
    "codigo": "73200",
    "nombre": "COELLO",
    "id": "7m-Gy3oBVXS07smajJUm"
},
{
    "dpto": "63",
    "codigo": "63001",
    "nombre": "ARMENIA",
    "id": "uW-Gy3oBVXS07smaf5Wd"
},
{
    "dpto": "73",
    "codigo": "73226",
    "nombre": "CUNDAY",
    "id": "8G-Gy3oBVXS07smajJUm"
},
{
    "dpto": "63",
    "codigo": "63130",
    "nombre": "CALARCÁ",
    "id": "u2-Gy3oBVXS07smaf5Wd"
},
{
    "dpto": "73",
    "codigo": "73268",
    "nombre": "ESPINAL",
    "id": "9G-Gy3oBVXS07smakJVI"
},
{
    "dpto": "63",
    "codigo": "63212",
    "nombre": "CÓRDOBA",
    "id": "vW-Gy3oBVXS07smaf5Wd"
},
{
    "dpto": "73",
    "codigo": "73275",
    "nombre": "FLANDES",
    "id": "9m-Gy3oBVXS07smalJWs"
},
{
    "dpto": "63",
    "codigo": "63302",
    "nombre": "GÉNOVA",
    "id": "v2-Gy3oBVXS07smaf5Wd"
},
{
    "dpto": "73",
    "codigo": "73319",
    "nombre": "GUAMO",
    "id": "-G-Gy3oBVXS07smalJWs"
},
{
    "dpto": "63",
    "codigo": "63470",
    "nombre": "MONTENEGRO",
    "id": "wW-Gy3oBVXS07smaf5Wd"
},
{
    "dpto": "73",
    "codigo": "73352",
    "nombre": "ICONONZO",
    "id": "-m-Gy3oBVXS07smalJWs"
},
{
    "dpto": "63",
    "codigo": "63594",
    "nombre": "QUIMBAYA",
    "id": "w2-Gy3oBVXS07smaf5Wd"
},
{
    "dpto": "73",
    "codigo": "73411",
    "nombre": "LÍBANO",
    "id": "_G-Gy3oBVXS07smalJWs"
},
{
    "dpto": "66",
    "codigo": "66001",
    "nombre": "PEREIRA",
    "id": "xW-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "73",
    "codigo": "73449",
    "nombre": "MELGAR",
    "id": "_m-Gy3oBVXS07smamJVR"
},
{
    "dpto": "66",
    "codigo": "66075",
    "nombre": "BALBOA",
    "id": "x2-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "73",
    "codigo": "73483",
    "nombre": "NATAGAIMA",
    "id": "AG-Gy3oBVXS07smamJZR"
},
{
    "dpto": "66",
    "codigo": "66170",
    "nombre": "DOSQUEBRADAS",
    "id": "yW-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "73",
    "codigo": "73520",
    "nombre": "PALOCABILDO",
    "id": "Am-Gy3oBVXS07smamJZR"
},
{
    "dpto": "66",
    "codigo": "66383",
    "nombre": "LA CELIA",
    "id": "y2-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "73",
    "codigo": "73555",
    "nombre": "PLANADAS",
    "id": "EW-Gy3oBVXS07smanJYr"
},
{
    "dpto": "66",
    "codigo": "66440",
    "nombre": "MARSELLA",
    "id": "zW-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "73",
    "codigo": "73585",
    "nombre": "PURIFICACIÓN",
    "id": "E2-Gy3oBVXS07smanJYr"
},
{
    "dpto": "66",
    "codigo": "66572",
    "nombre": "PUEBLO RICO",
    "id": "z2-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "73",
    "codigo": "73622",
    "nombre": "RONCESVALLES",
    "id": "FW-Gy3oBVXS07smanJYr"
},
{
    "dpto": "66",
    "codigo": "66682",
    "nombre": "SANTA ROSA DE CABAL",
    "id": "0W-Gy3oBVXS07smag5Xm"
},
{
    "dpto": "73",
    "codigo": "73671",
    "nombre": "SALDAÑA",
    "id": "F2-Gy3oBVXS07smanJYr"
},
{
    "dpto": "73",
    "codigo": "73001",
    "nombre": "IBAGUÉ",
    "id": "42-Gy3oBVXS07smaiJUW"
},
{
    "dpto": "73",
    "codigo": "73678",
    "nombre": "SAN LUIS",
    "id": "GW-Gy3oBVXS07sman5aQ"
},
{
    "dpto": "73",
    "codigo": "73026",
    "nombre": "ALVARADO",
    "id": "5W-Gy3oBVXS07smaiJUW"
},
{
    "dpto": "73",
    "codigo": "73770",
    "nombre": "SUÁREZ",
    "id": "G2-Gy3oBVXS07sman5aQ"
},
{
    "dpto": "73",
    "codigo": "73043",
    "nombre": "ANZOÁTEGUI",
    "id": "52-Gy3oBVXS07smaiJUW"
},
{
    "dpto": "73",
    "codigo": "73861",
    "nombre": "VENADILLO",
    "id": "HW-Gy3oBVXS07sman5aQ"
},
{
    "dpto": "73",
    "codigo": "73067",
    "nombre": "ATACO",
    "id": "6W-Gy3oBVXS07smajJUm"
},
{
    "dpto": "73",
    "codigo": "73873",
    "nombre": "VILLARRICA",
    "id": "H2-Gy3oBVXS07sman5aQ"
},
{
    "dpto": "73",
    "codigo": "73148",
    "nombre": "CARMEN DE APICALÁ",
    "id": "62-Gy3oBVXS07smajJUm"
},
{
    "dpto": "52",
    "codigo": "52683",
    "nombre": "SANDONÁ",
    "id": "Im-Gy3oBVXS07smaopbu"
},
{
    "dpto": "73",
    "codigo": "73168",
    "nombre": "CHAPARRAL",
    "id": "7W-Gy3oBVXS07smajJUm"
},
{
    "dpto": "52",
    "codigo": "52687",
    "nombre": "SAN LORENZO",
    "id": "JG-Gy3oBVXS07smaopbu"
},
{
    "dpto": "73",
    "codigo": "73217",
    "nombre": "COYAIMA",
    "id": "72-Gy3oBVXS07smajJUm"
},
{
    "dpto": "52",
    "codigo": "52693",
    "nombre": "SAN PABLO",
    "id": "Jm-Gy3oBVXS07smaopbu"
},
{
    "dpto": "73",
    "codigo": "73236",
    "nombre": "DOLORES",
    "id": "82-Gy3oBVXS07smakJVI"
},
{
    "dpto": "52",
    "codigo": "52694",
    "nombre": "SAN PEDRO DE CARTAGO",
    "id": "KG-Gy3oBVXS07smaopbu"
},
{
    "dpto": "73",
    "codigo": "73270",
    "nombre": "FALAN",
    "id": "9W-Gy3oBVXS07smakJVI"
},
{
    "dpto": "52",
    "codigo": "52699",
    "nombre": "SANTACRUZ",
    "id": "Km-Gy3oBVXS07smaopbu"
},
{
    "dpto": "73",
    "codigo": "73283",
    "nombre": "FRESNO",
    "id": "92-Gy3oBVXS07smalJWs"
},
{
    "dpto": "52",
    "codigo": "52678",
    "nombre": "SAMANIEGO",
    "id": "LG-Gy3oBVXS07smaopbu"
},
{
    "dpto": "73",
    "codigo": "73347",
    "nombre": "HERVEO",
    "id": "-W-Gy3oBVXS07smalJWs"
},
{
    "dpto": "52",
    "codigo": "52210",
    "nombre": "CONTADERO",
    "id": "Lm-Gy3oBVXS07smappYn"
},
{
    "dpto": "73",
    "codigo": "73408",
    "nombre": "LÉRIDA",
    "id": "-2-Gy3oBVXS07smalJWs"
},
{
    "dpto": "52",
    "codigo": "52001",
    "nombre": "PASTO",
    "id": "MG-Gy3oBVXS07smappYn"
},
{
    "dpto": "73",
    "codigo": "73443",
    "nombre": "SAN SEBASTIÁN DE MARIQUITA",
    "id": "_W-Gy3oBVXS07smamJVR"
},
{
    "dpto": "52",
    "codigo": "52022",
    "nombre": "ALDANA",
    "id": "Mm-Gy3oBVXS07smappYn"
},
{
    "dpto": "73",
    "codigo": "73461",
    "nombre": "MURILLO",
    "id": "_2-Gy3oBVXS07smamJVR"
},
{
    "dpto": "52",
    "codigo": "52036",
    "nombre": "ANCUYA",
    "id": "NG-Gy3oBVXS07smappYn"
},
{
    "dpto": "73",
    "codigo": "73504",
    "nombre": "ORTEGA",
    "id": "AW-Gy3oBVXS07smamJZR"
},
{
    "dpto": "52",
    "codigo": "52083",
    "nombre": "BELÉN",
    "id": "Nm-Gy3oBVXS07smappYn"
},
{
    "dpto": "73",
    "codigo": "73547",
    "nombre": "PIEDRAS",
    "id": "EG-Gy3oBVXS07smanJYr"
},
{
    "dpto": "52",
    "codigo": "52203",
    "nombre": "COLÓN",
    "id": "OG-Gy3oBVXS07smappYn"
},
{
    "dpto": "73",
    "codigo": "73563",
    "nombre": "PRADO",
    "id": "Em-Gy3oBVXS07smanJYr"
},
{
    "dpto": "52",
    "codigo": "52506",
    "nombre": "OSPINA",
    "id": "Om-Gy3oBVXS07smappYn"
},
{
    "dpto": "73",
    "codigo": "73616",
    "nombre": "RIOBLANCO",
    "id": "FG-Gy3oBVXS07smanJYr"
},
{
    "dpto": "52",
    "codigo": "52786",
    "nombre": "TAMINANGO",
    "id": "PG-Gy3oBVXS07smaqZZB"
},
{
    "dpto": "73",
    "codigo": "73624",
    "nombre": "ROVIRA",
    "id": "Fm-Gy3oBVXS07smanJYr"
},
{
    "dpto": "52",
    "codigo": "52240",
    "nombre": "CHACHAGÜÍ",
    "id": "Pm-Gy3oBVXS07smaqZZB"
},
{
    "dpto": "73",
    "codigo": "73675",
    "nombre": "SAN ANTONIO",
    "id": "GG-Gy3oBVXS07sman5aQ"
},
{
    "dpto": "52",
    "codigo": "52258",
    "nombre": "EL TABLÓN DE GÓMEZ",
    "id": "QG-Gy3oBVXS07smaqZZB"
},
{
    "dpto": "73",
    "codigo": "73686",
    "nombre": "SANTA ISABEL",
    "id": "Gm-Gy3oBVXS07sman5aQ"
},
{
    "dpto": "52",
    "codigo": "52411",
    "nombre": "LINARES",
    "id": "Qm-Gy3oBVXS07smaqZZB"
},
{
    "dpto": "73",
    "codigo": "73854",
    "nombre": "VALLE DE SAN JUAN",
    "id": "HG-Gy3oBVXS07sman5aQ"
},
{
    "dpto": "52",
    "codigo": "52435",
    "nombre": "MALLAMA",
    "id": "RG-Gy3oBVXS07smarJZc"
},
{
    "dpto": "73",
    "codigo": "73870",
    "nombre": "VILLAHERMOSA",
    "id": "Hm-Gy3oBVXS07sman5aQ"
},
{
    "dpto": "52",
    "codigo": "52381",
    "nombre": "LA FLORIDA",
    "id": "Rm-Gy3oBVXS07smarJZc"
},
{
    "dpto": "73",
    "codigo": "73349",
    "nombre": "HONDA",
    "id": "IG-Gy3oBVXS07sman5aQ"
},
{
    "dpto": "52",
    "codigo": "52287",
    "nombre": "FUNES",
    "id": "SG-Gy3oBVXS07smarJZc"
},
{
    "dpto": "52",
    "codigo": "52685",
    "nombre": "SAN BERNARDO",
    "id": "I2-Gy3oBVXS07smaopbu"
},
{
    "dpto": "52",
    "codigo": "52399",
    "nombre": "LA UNIÓN",
    "id": "Sm-Gy3oBVXS07smarJZc"
},
{
    "dpto": "52",
    "codigo": "52560",
    "nombre": "POTOSÍ",
    "id": "JW-Gy3oBVXS07smaopbu"
},
{
    "dpto": "52",
    "codigo": "52405",
    "nombre": "LEIVA",
    "id": "TG-Gy3oBVXS07smarJZc"
},
{
    "dpto": "52",
    "codigo": "52565",
    "nombre": "PROVIDENCIA",
    "id": "J2-Gy3oBVXS07smaopbu"
},
{
    "dpto": "52",
    "codigo": "52352",
    "nombre": "ILES",
    "id": "Xm-Gy3oBVXS07smat5by"
},
{
    "dpto": "52",
    "codigo": "52573",
    "nombre": "PUERRES",
    "id": "KW-Gy3oBVXS07smaopbu"
},
{
    "dpto": "52",
    "codigo": "52356",
    "nombre": "IPIALES",
    "id": "YG-Gy3oBVXS07smat5by"
},
{
    "dpto": "52",
    "codigo": "52585",
    "nombre": "PUPIALES",
    "id": "K2-Gy3oBVXS07smaopbu"
},
{
    "dpto": "52",
    "codigo": "52233",
    "nombre": "CUMBITARA",
    "id": "Ym-Gy3oBVXS07smat5by"
},
{
    "dpto": "52",
    "codigo": "52207",
    "nombre": "CONSACÁ",
    "id": "LW-Gy3oBVXS07smappYn"
},
{
    "dpto": "68",
    "codigo": "68872",
    "nombre": "VILLANUEVA",
    "id": "ZG-Gy3oBVXS07smat5by"
},
{
    "dpto": "52",
    "codigo": "52215",
    "nombre": "CÓRDOBA",
    "id": "L2-Gy3oBVXS07smappYn"
},
{
    "dpto": "68",
    "codigo": "68370",
    "nombre": "JORDÁN",
    "id": "Zm-Gy3oBVXS07smat5by"
},
{
    "dpto": "52",
    "codigo": "52019",
    "nombre": "ALBÁN",
    "id": "MW-Gy3oBVXS07smappYn"
},
{
    "dpto": "68",
    "codigo": "68385",
    "nombre": "LANDÁZURI",
    "id": "aG-Gy3oBVXS07smat5by"
},
{
    "dpto": "52",
    "codigo": "52224",
    "nombre": "CUASPUD CARLOSAMA",
    "id": "M2-Gy3oBVXS07smappYn"
},
{
    "dpto": "68",
    "codigo": "68406",
    "nombre": "LEBRIJA",
    "id": "am-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "52",
    "codigo": "52051",
    "nombre": "ARBOLEDA",
    "id": "NW-Gy3oBVXS07smappYn"
},
{
    "dpto": "68",
    "codigo": "68425",
    "nombre": "MACARAVITA",
    "id": "bG-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "52",
    "codigo": "52110",
    "nombre": "BUESACO",
    "id": "N2-Gy3oBVXS07smappYn"
},
{
    "dpto": "68",
    "codigo": "68444",
    "nombre": "MATANZA",
    "id": "bm-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "52",
    "codigo": "52480",
    "nombre": "NARIÑO",
    "id": "OW-Gy3oBVXS07smappYn"
},
{
    "dpto": "68",
    "codigo": "68468",
    "nombre": "MOLAGAVITA",
    "id": "cG-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "91",
    "codigo": "91669",
    "nombre": "PUERTO SANTANDER",
    "id": "pG-Gy3oBVXS07smayZbX"
},
{
    "dpto": "68",
    "codigo": "68500",
    "nombre": "OIBA",
    "id": "cm-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "52",
    "codigo": "52786",
    "nombre": "TAMINANGO",
    "id": "PG-Gy3oBVXS07smaqZZB"
},
{
    "dpto": "68",
    "codigo": "68522",
    "nombre": "PALMAR",
    "id": "dG-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "52",
    "codigo": "52240",
    "nombre": "CHACHAGÜÍ",
    "id": "Pm-Gy3oBVXS07smaqZZB"
},
{
    "dpto": "68",
    "codigo": "68533",
    "nombre": "PÁRAMO",
    "id": "dm-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "52",
    "codigo": "52258",
    "nombre": "EL TABLÓN DE GÓMEZ",
    "id": "QG-Gy3oBVXS07smaqZZB"
},
{
    "dpto": "68",
    "codigo": "68572",
    "nombre": "PUENTE NACIONAL",
    "id": "eG-Gy3oBVXS07smau5Y7"
},
{
    "dpto": "52",
    "codigo": "52411",
    "nombre": "LINARES",
    "id": "Qm-Gy3oBVXS07smaqZZB"
},
{
    "dpto": "68",
    "codigo": "68575",
    "nombre": "PUERTO WILCHES",
    "id": "em-Gy3oBVXS07smavpae"
},
{
    "dpto": "52",
    "codigo": "52435",
    "nombre": "MALLAMA",
    "id": "RG-Gy3oBVXS07smarJZc"
},
{
    "dpto": "68",
    "codigo": "68217",
    "nombre": "COROMORO",
    "id": "fG-Gy3oBVXS07smavpae"
},
{
    "dpto": "52",
    "codigo": "52381",
    "nombre": "LA FLORIDA",
    "id": "Rm-Gy3oBVXS07smarJZc"
},
{
    "dpto": "68",
    "codigo": "68235",
    "nombre": "EL CARMEN DE CHUCURI",
    "id": "fm-Gy3oBVXS07smavpae"
},
{
    "dpto": "52",
    "codigo": "52287",
    "nombre": "FUNES",
    "id": "SG-Gy3oBVXS07smarJZc"
},
{
    "dpto": "68",
    "codigo": "68255",
    "nombre": "EL PLAYÓN",
    "id": "gG-Gy3oBVXS07smavpae"
},
{
    "dpto": "52",
    "codigo": "52399",
    "nombre": "LA UNIÓN",
    "id": "Sm-Gy3oBVXS07smarJZc"
},
{
    "dpto": "68",
    "codigo": "68266",
    "nombre": "ENCISO",
    "id": "gm-Gy3oBVXS07smavpae"
},
{
    "dpto": "52",
    "codigo": "52405",
    "nombre": "LEIVA",
    "id": "TG-Gy3oBVXS07smarJZc"
},
{
    "dpto": "68",
    "codigo": "68296",
    "nombre": "GALÁN",
    "id": "kW-Gy3oBVXS07smaw5aV"
},
{
    "dpto": "95",
    "codigo": "95200",
    "nombre": "MIRAFLORES",
    "id": "t2-Gy3oBVXS07sma2JYR"
},
{
    "dpto": "68",
    "codigo": "68318",
    "nombre": "GUACA",
    "id": "k2-Gy3oBVXS07smaw5aV"
},
{
    "dpto": "85",
    "codigo": "85440",
    "nombre": "VILLANUEVA",
    "id": "3m-Gy3oBVXS07sma7pbV"
},
{
    "dpto": "68",
    "codigo": "68322",
    "nombre": "GUAPOTÁ",
    "id": "lW-Gy3oBVXS07smaw5aV"
},
{
    "dpto": "85",
    "codigo": "85250",
    "nombre": "PAZ DE ARIPORO",
    "id": "4G-Gy3oBVXS07sma7pbV"
},
{
    "dpto": "68",
    "codigo": "68327",
    "nombre": "GÜEPSA",
    "id": "l2-Gy3oBVXS07smaw5aV"
},
{
    "dpto": "68",
    "codigo": "68296",
    "nombre": "GALÁN",
    "id": "kW-Gy3oBVXS07smaw5aV"
},
{
    "dpto": "68",
    "codigo": "68368",
    "nombre": "JESÚS MARÍA",
    "id": "mW-Gy3oBVXS07smaw5aV"
},
{
    "dpto": "68",
    "codigo": "68318",
    "nombre": "GUACA",
    "id": "k2-Gy3oBVXS07smaw5aV"
},
{
    "dpto": "68",
    "codigo": "68276",
    "nombre": "FLORIDABLANCA",
    "id": "m2-Gy3oBVXS07smaw5aV"
},
{
    "dpto": "68",
    "codigo": "68322",
    "nombre": "GUAPOTÁ",
    "id": "lW-Gy3oBVXS07smaw5aV"
},
{
    "dpto": "68",
    "codigo": "68250",
    "nombre": "EL PEÑÓN",
    "id": "nm-Gy3oBVXS07smayZbX"
},
{
    "dpto": "68",
    "codigo": "68327",
    "nombre": "GÜEPSA",
    "id": "l2-Gy3oBVXS07smaw5aV"
},
{
    "dpto": "68",
    "codigo": "68001",
    "nombre": "BUCARAMANGA",
    "id": "oG-Gy3oBVXS07smayZbX"
},
{
    "dpto": "68",
    "codigo": "68368",
    "nombre": "JESÚS MARÍA",
    "id": "mW-Gy3oBVXS07smaw5aV"
},
{
    "dpto": "85",
    "codigo": "85010",
    "nombre": "AGUAZUL",
    "id": "om-Gy3oBVXS07smayZbX"
},
{
    "dpto": "68",
    "codigo": "68276",
    "nombre": "FLORIDABLANCA",
    "id": "m2-Gy3oBVXS07smaw5aV"
},
{
    "dpto": "91",
    "codigo": "91669",
    "nombre": "PUERTO SANTANDER",
    "id": "pG-Gy3oBVXS07smayZbX"
},
{
    "dpto": "91",
    "codigo": "91798",
    "nombre": "TARAPACÁ",
    "id": "pW-Gy3oBVXS07sma05YF"
},
{
    "dpto": "95",
    "codigo": "95001",
    "nombre": "SAN JOSÉ DEL GUAVIARE",
    "id": "pm-Gy3oBVXS07sma05YF"
},
{
    "dpto": "95",
    "codigo": "95015",
    "nombre": "CALAMAR",
    "id": "p2-Gy3oBVXS07sma05YF"
},
{
    "dpto": "95",
    "codigo": "95025",
    "nombre": "EL RETORNO",
    "id": "tm-Gy3oBVXS07sma2JYR"
},
{
    "dpto": "85",
    "codigo": "85263",
    "nombre": "PORE",
    "id": "ym-Gy3oBVXS07sma5pas"
},
{
    "dpto": "88",
    "codigo": "88001",
    "nombre": "SAN ANDRÉS",
    "id": "uG-Gy3oBVXS07sma25b2"
},
{
    "dpto": "85",
    "codigo": "85300",
    "nombre": "SABANALARGA",
    "id": "zG-Gy3oBVXS07sma5pas"
},
{
    "dpto": "81",
    "codigo": "81001",
    "nombre": "ARAUCA",
    "id": "um-Gy3oBVXS07sma25b2"
},
{
    "dpto": "85",
    "codigo": "85325",
    "nombre": "SAN LUIS DE PALENQUE",
    "id": "zm-Gy3oBVXS07sma5pas"
},
{
    "dpto": "81",
    "codigo": "81220",
    "nombre": "CRAVO NORTE",
    "id": "vm-Gy3oBVXS07sma35bd"
},
{
    "dpto": "85",
    "codigo": "85410",
    "nombre": "TAURAMENA",
    "id": "0G-Gy3oBVXS07sma5pas"
},
{
    "dpto": "81",
    "codigo": "81300",
    "nombre": "FORTUL",
    "id": "wG-Gy3oBVXS07sma35bd"
},
{
    "dpto": "85",
    "codigo": "85136",
    "nombre": "LA SALINA",
    "id": "xW-Gy3oBVXS07sma45Y1"
},
{
    "dpto": "81",
    "codigo": "81794",
    "nombre": "TAME",
    "id": "wm-Gy3oBVXS07sma35bd"
},
{
    "dpto": "85",
    "codigo": "85162",
    "nombre": "MONTERREY",
    "id": "x2-Gy3oBVXS07sma45Y1"
},
{
    "dpto": "85",
    "codigo": "85015",
    "nombre": "CHÁMEZA",
    "id": "xG-Gy3oBVXS07sma45Y1"
},
{
    "dpto": "85",
    "codigo": "85230",
    "nombre": "OROCUÉ",
    "id": "yW-Gy3oBVXS07sma45Y1"
},
{
    "dpto": "85",
    "codigo": "85139",
    "nombre": "MANÍ",
    "id": "xm-Gy3oBVXS07sma45Y1"
},
{
    "dpto": "68",
    "codigo": "68547",
    "nombre": "PIEDECUESTA",
    "id": "n2-Gy3oBVXS07smayZbX"
},
{
    "dpto": "85",
    "codigo": "85225",
    "nombre": "NUNCHÍA",
    "id": "yG-Gy3oBVXS07sma45Y1"
},
{
    "dpto": "85",
    "codigo": "85001",
    "nombre": "YOPAL",
    "id": "oW-Gy3oBVXS07smayZbX"
},
{
    "dpto": "85",
    "codigo": "85263",
    "nombre": "PORE",
    "id": "ym-Gy3oBVXS07sma5pas"
},
{
    "dpto": "91",
    "codigo": "91540",
    "nombre": "PUERTO NARIÑO",
    "id": "o2-Gy3oBVXS07smayZbX"
},
{
    "dpto": "85",
    "codigo": "85300",
    "nombre": "SABANALARGA",
    "id": "zG-Gy3oBVXS07sma5pas"
},
{
    "dpto": "88",
    "codigo": "88564",
    "nombre": "PROVIDENCIA",
    "id": "uW-Gy3oBVXS07sma25b2"
},
{
    "dpto": "85",
    "codigo": "85325",
    "nombre": "SAN LUIS DE PALENQUE",
    "id": "zm-Gy3oBVXS07sma5pas"
},
{
    "dpto": "81",
    "codigo": "81591",
    "nombre": "PUERTO RONDÓN",
    "id": "vW-Gy3oBVXS07sma35bd"
},
{
    "dpto": "85",
    "codigo": "85410",
    "nombre": "TAURAMENA",
    "id": "0G-Gy3oBVXS07sma5pas"
},
{
    "dpto": "81",
    "codigo": "81065",
    "nombre": "ARAUQUITA",
    "id": "v2-Gy3oBVXS07sma35bd"
},
{
    "dpto": "85",
    "codigo": "85440",
    "nombre": "VILLANUEVA",
    "id": "3m-Gy3oBVXS07sma7pbV"
},
{
    "dpto": "81",
    "codigo": "81736",
    "nombre": "SARAVENA",
    "id": "wW-Gy3oBVXS07sma35bd"
},
{
    "dpto": "85",
    "codigo": "85250",
    "nombre": "PAZ DE ARIPORO",
    "id": "4G-Gy3oBVXS07sma7pbV"
},
{
    "dpto": "11",
    "codigo": "11001",
    "nombre": "BOGOTÁ, D.C.",
    "id": "w2-Gy3oBVXS07sma35bd"
},
{
    "dpto": "97",
    "codigo": "97161",
    "nombre": "CARURÚ",
    "id": "42-Gy3oBVXS07sma9pZ4"
},
{
    "dpto": "97",
    "codigo": "97511",
    "nombre": "PACOA",
    "id": "5G-Gy3oBVXS07sma9pZ4"
},
{
    "dpto": "97",
    "codigo": "97666",
    "nombre": "TARAIRA",
    "id": "5W-Gy3oBVXS07sma-pYK"
},
{
    "dpto": "97",
    "codigo": "97777",
    "nombre": "PAPUNAHUA",
    "id": "9G-Gy3oBVXS07sma_pZU"
},
{
    "dpto": "97",
    "codigo": "97889",
    "nombre": "YAVARATÉ",
    "id": "9W-Gy3oBVXS07sma_pZU"
},
{
    "dpto": "86",
    "codigo": "86001",
    "nombre": "MOCOA",
    "id": "9m-Hy3oBVXS07smaApa3"
},
{
    "dpto": "86",
    "codigo": "86219",
    "nombre": "COLÓN",
    "id": "92-Hy3oBVXS07smaApa3"
},
{
    "dpto": "86",
    "codigo": "86320",
    "nombre": "ORITO",
    "id": "-G-Hy3oBVXS07smaApa3"
},
{
    "dpto": "86",
    "codigo": "86749",
    "nombre": "SIBUNDOY",
    "id": "-W-Hy3oBVXS07smaApa3"
},
{
    "dpto": "86",
    "codigo": "86755",
    "nombre": "SAN FRANCISCO",
    "id": "-m-Hy3oBVXS07smaApa3"
},
{
    "dpto": "86",
    "codigo": "86757",
    "nombre": "SAN MIGUEL",
    "id": "-2-Hy3oBVXS07smaApa3"
},
{
    "dpto": "86",
    "codigo": "86760",
    "nombre": "SANTIAGO",
    "id": "_G-Hy3oBVXS07smaApa3"
},
{
    "dpto": "86",
    "codigo": "86865",
    "nombre": "VALLE DEL GUAMUEZ",
    "id": "_W-Hy3oBVXS07smaApa3"
},
{
    "dpto": "86",
    "codigo": "86885",
    "nombre": "VILLAGARZÓN",
    "id": "_m-Hy3oBVXS07smaApa3"
},
{
    "dpto": "86",
    "codigo": "86569",
    "nombre": "PUERTO CAICEDO",
    "id": "_2-Hy3oBVXS07smaApa3"
},
{
    "dpto": "86",
    "codigo": "86568",
    "nombre": "PUERTO ASÍS",
    "id": "Am-Hy3oBVXS07smaB5fy"
},
{
    "dpto": "86",
    "codigo": "86571",
    "nombre": "PUERTO GUZMÁN",
    "id": "A2-Hy3oBVXS07smaB5fy"
},
{
    "dpto": "86",
    "codigo": "86573",
    "nombre": "PUERTO LEGUÍZAMO",
    "id": "BG-Hy3oBVXS07smaB5fy"
},
{
    "dpto": "94",
    "codigo": "94001",
    "nombre": "INÍRIDA",
    "id": "BW-Hy3oBVXS07smaB5fy"
},
{
    "dpto": "94",
    "codigo": "94343",
    "nombre": "BARRANCOMINAS",
    "id": "Bm-Hy3oBVXS07smaC5eV"
},
{
    "dpto": "94",
    "codigo": "94883",
    "nombre": "SAN FELIPE",
    "id": "B2-Hy3oBVXS07smaC5eV"
},
{
    "dpto": "94",
    "codigo": "94884",
    "nombre": "PUERTO COLOMBIA",
    "id": "FW-Hy3oBVXS07smaD5eK"
},
{
    "dpto": "94",
    "codigo": "94885",
    "nombre": "LA GUADALUPE",
    "id": "Fm-Hy3oBVXS07smaD5eK"
},
{
    "dpto": "94",
    "codigo": "94886",
    "nombre": "CACAHUAL",
    "id": "F2-Hy3oBVXS07smaD5eK"
},
{
    "dpto": "94",
    "codigo": "94887",
    "nombre": "PANA PANA",
    "id": "GG-Hy3oBVXS07smaD5eK"
},
{
    "dpto": "94",
    "codigo": "94888",
    "nombre": "MORICHAL",
    "id": "GW-Hy3oBVXS07smaFJcw"
},
{
    "dpto": "99",
    "codigo": "99624",
    "nombre": "SANTA ROSALÍA",
    "id": "Gm-Hy3oBVXS07smaFJcw"
},
{
    "dpto": "99",
    "codigo": "99001",
    "nombre": "PUERTO CARREÑO",
    "id": "HG-Hy3oBVXS07smaF5d3"
},
{
    "dpto": "99",
    "codigo": "99524",
    "nombre": "LA PRIMAVERA",
    "id": "HW-Hy3oBVXS07smaF5d3"
},
{
    "dpto": "99",
    "codigo": "99773",
    "nombre": "CUMARIBO",
    "id": "Hm-Hy3oBVXS07smaG5fu"
},
{
    "dpto": "91",
    "codigo": "91001",
    "nombre": "LETICIA",
    "id": "H2-Hy3oBVXS07smaIJcM"
},
{
    "dpto": "91",
    "codigo": "91263",
    "nombre": "EL ENCANTO",
    "id": "IG-Hy3oBVXS07smaIJcM"
},
{
    "dpto": "91",
    "codigo": "91405",
    "nombre": "LA CHORRERA",
    "id": "IW-Hy3oBVXS07smaIJcM"
},
{
    "dpto": "91",
    "codigo": "91407",
    "nombre": "LA PEDRERA",
    "id": "MG-Hy3oBVXS07smaJJfG"
},
{
    "dpto": "91",
    "codigo": "91430",
    "nombre": "LA VICTORIA",
    "id": "MW-Hy3oBVXS07smaJJfG"
},
{
    "dpto": "91",
    "codigo": "91460",
    "nombre": "MIRITÍ - PARANÁ",
    "id": "Mm-Hy3oBVXS07smaJJfG"
},
{
    "dpto": "91",
    "codigo": "91530",
    "nombre": "PUERTO ALEGRÍA",
    "id": "M2-Hy3oBVXS07smaKZeR"
},
{
    "dpto": "91",
    "codigo": "91536",
    "nombre": "PUERTO ARICA",
    "id": "NG-Hy3oBVXS07smaKZeR"
},
{
    "dpto": "68",
    "codigo": "68077",
    "nombre": "BARBOSA",
    "id": "OG-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68079",
    "nombre": "BARICHARA",
    "id": "OW-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68081",
    "nombre": "BARRANCABERMEJA",
    "id": "Om-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68092",
    "nombre": "BETULIA",
    "id": "O2-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68121",
    "nombre": "CABRERA",
    "id": "PG-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68132",
    "nombre": "CALIFORNIA",
    "id": "PW-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68147",
    "nombre": "CAPITANEJO",
    "id": "Pm-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68152",
    "nombre": "CARCASÍ",
    "id": "P2-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68160",
    "nombre": "CEPITÁ",
    "id": "QG-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68162",
    "nombre": "CERRITO",
    "id": "QW-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68167",
    "nombre": "CHARALÁ",
    "id": "Qm-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68169",
    "nombre": "CHARTA",
    "id": "Q2-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68176",
    "nombre": "CHIMA",
    "id": "RG-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68179",
    "nombre": "CHIPATÁ",
    "id": "RW-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68190",
    "nombre": "CIMITARRA",
    "id": "Rm-Hy3oBVXS07smaL5d6"
},
{
    "dpto": "68",
    "codigo": "68207",
    "nombre": "CONCEPCIÓN",
    "id": "R2-Hy3oBVXS07smaMpf-"
},
{
    "dpto": "68",
    "codigo": "68209",
    "nombre": "CONFINES",
    "id": "SG-Hy3oBVXS07smaMpf-"
},
{
    "dpto": "68",
    "codigo": "68615",
    "nombre": "RIONEGRO",
    "id": "SW-Hy3oBVXS07smaMpf-"
},
{
    "dpto": "68",
    "codigo": "68655",
    "nombre": "SABANA DE TORRES",
    "id": "Sm-Hy3oBVXS07smaMpf-"
},
{
    "dpto": "68",
    "codigo": "68669",
    "nombre": "SAN ANDRÉS",
    "id": "S2-Hy3oBVXS07smaMpf-"
},
{
    "dpto": "68",
    "codigo": "68673",
    "nombre": "SAN BENITO",
    "id": "TG-Hy3oBVXS07smaMpf-"
},
{
    "dpto": "68",
    "codigo": "68679",
    "nombre": "SAN GIL",
    "id": "TW-Hy3oBVXS07smaMpf-"
},
{
    "dpto": "68",
    "codigo": "68682",
    "nombre": "SAN JOAQUÍN",
    "id": "Tm-Hy3oBVXS07smaMpf-"
},
{
    "dpto": "68",
    "codigo": "68684",
    "nombre": "SAN JOSÉ DE MIRANDA",
    "id": "T2-Hy3oBVXS07smaMpf-"
},
{
    "dpto": "68",
    "codigo": "68686",
    "nombre": "SAN MIGUEL",
    "id": "UG-Hy3oBVXS07smaMpf-"
},
{
    "dpto": "68",
    "codigo": "68689",
    "nombre": "SAN VICENTE DE CHUCURÍ",
    "id": "UW-Hy3oBVXS07smaMpf-"
},
{
    "dpto": "68",
    "codigo": "68705",
    "nombre": "SANTA BÁRBARA",
    "id": "YW-Hy3oBVXS07smaNpei"
},
{
    "dpto": "68",
    "codigo": "68720",
    "nombre": "SANTA HELENA DEL OPÓN",
    "id": "Ym-Hy3oBVXS07smaNpei"
},
{
    "dpto": "68",
    "codigo": "68745",
    "nombre": "SIMACOTA",
    "id": "Y2-Hy3oBVXS07smaNpei"
},
{
    "dpto": "68",
    "codigo": "68755",
    "nombre": "SOCORRO",
    "id": "ZG-Hy3oBVXS07smaNpei"
},
{
    "dpto": "68",
    "codigo": "68770",
    "nombre": "SUAITA",
    "id": "ZW-Hy3oBVXS07smaNpei"
},
{
    "dpto": "68",
    "codigo": "68773",
    "nombre": "SUCRE",
    "id": "Zm-Hy3oBVXS07smaNpei"
},
{
    "dpto": "68",
    "codigo": "68780",
    "nombre": "SURATÁ",
    "id": "Z2-Hy3oBVXS07smaNpei"
},
{
    "dpto": "68",
    "codigo": "68820",
    "nombre": "TONA",
    "id": "aG-Hy3oBVXS07smaNpei"
},
{
    "dpto": "68",
    "codigo": "68855",
    "nombre": "VALLE DE SAN JOSÉ",
    "id": "aW-Hy3oBVXS07smaNpei"
},
{
    "dpto": "68",
    "codigo": "68861",
    "nombre": "VÉLEZ",
    "id": "am-Hy3oBVXS07smaNpei"
}
]