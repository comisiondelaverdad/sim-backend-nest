import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import * as XLSX from "xlsx";
import * as CSV from "csvtojson";
import fs = require("fs");
import filePath = require('path');
import tmp = require('temporary');
import chai = require('chai');
import { departaments, countries } from "./../geo-shapes/departamentos-municipios";
export const labels = {
  ident: 'Identificación',
  attachedDocument: 'Documento Adjunto',
  url: 'Url',
  documentType: 'Tipo de documento',
  documentaryGroup: 'Agrupación documental',
  title: 'Título',
  authors: 'Autor',
  collaborators: 'Colaboradores',
  rights: 'Derechos',
  accessLevel: 'Nivel de acceso interno',
  creationDate: 'Fecha de creación/publicación.',
  editionPlace: 'Lugar de creación/publicación',
  description: 'Descripción',
  topics: 'Temas',
  mandate: 'Mandato',
  objective: 'Objetivo',
  focus: 'Enfoque',
  macroprocess: 'Macroproceso',
  stage: 'Etapa',
  violenceType: 'Tipo de violencia',
  geographicCoverage: 'Cobertura espacial',
  temporalCoverage: 'Cobertura Temporal',
  milestone: 'Hitos',
  conflictActors: 'Actores armados',
  population: 'Población',
  occupation: 'Ocupaciones',
  language: 'Idioma/Lengua',
  repository: 'Archivo, biblioteca o repositorio',
  background: 'Archivo y fondo',
  journalPublication: 'Publicación de prensa',
  journalSection: 'Sección de prensa',
  editorial: 'Editorial',
  ISBN: 'ISBN',
  ISSN: 'ISSN',
  attachedRightsAuthorization: 'Autorización de derechos de uso',
  associatedResources: 'Asociar con otros recursos',
  associatedResourceGroups: 'Asociar con otros fondos',
  notes: 'Notas de catalogación',
  //Campos que no pertenecen al estándar de catalogación
  date: 'Fecha',
  keywords: 'Palabras clave',
  otherTitles: 'Otros  títulos',
  editors: 'Editores',
  creator: 'Creador',
  availabilityDate: 'Fecha disponible',
  approaches: 'Enfoques',
  filename: 'Ruta original'
}

const tesseract = require('node-tesseract');
const pdfTextExtract = require('pdf-text-extract')
const docxExtractMammoth = require('mammoth');
const WordExtractor = require("word-extractor");
var textract = require('textract');
// please install
// npm install pdf-text-extract
// npm install node-tesseract
// npm install mammoth
// npm install word-extractor
// npm install textract
@Injectable()
export class UtilService {

  readJson(path) {
    const promiseJson = new Promise((resolve, reject) => {
      fs.readFile(path, (err, data) => {
        if (err) {
          reject(err);
          throw err;
        }
        try {
          let jsonData = JSON.parse(data.toString('utf8'));
          resolve(jsonData)
        } catch (err) {
          reject(err)
        }
      });
    })
    return promiseJson
  }

  writeJson(path, json) {
    const fs = require('fs');
    let data = JSON.stringify(json);
    fs.writeFileSync(path, data);
  }

  constructor(
    private readonly mailerService: MailerService,
  ) { }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  };

  public async sendEmail(to, subject, body): Promise<void> {
    return this.mailerService
      .sendMail({
        to: to, // list of receivers
        from: 'sim@comisiondelaverdad.co', // sender address
        subject: subject, // Subject line
        text:
          'SIM - Comisión de la Verdad\n' +
          body + // plaintext body
          '\n\n--\nSistema de Información Misional.\nhttps://buscador.comisiondelaverdad.co\n',

        html:
          "<img src='cid:logo' style='width: 150px;' /><br/><br/>" +
          body + // HTML body content
          "<br/><br/>--<br/>Sistema de Información Misional.<br/><a href='https://buscador.comisiondelaverdad.co'>https://buscador.comisiondelaverdad.co</a><br/>",

        attachments: [
          {
            filename: 'logo.png',
            path: __dirname + '/../../files/logo.png',
            cid: 'logo', //same cid value as in the html img src
          },
        ],
      })
      .then();
  }

  dateToAAAAMMDD(d) {
    if (d !== null) {
      const date = new Date(d);
      const dd = (date.getUTCDate() < 10) ? `0${date.getUTCDate()}` : date.getUTCDate();
      const mm = (date.getUTCMonth() + 1) < 10 ? `0${date.getUTCMonth() + 1}` : date.getUTCMonth() + 1;
      const aaaa = date.getUTCFullYear();
      return `${aaaa}-${mm}-${dd}`
    } else {
      return d;
    }
  }

  dateDatesRange(startDate, stopDate, amount) {
    const dateArray = [];
    let currentDate = startDate;
    while (currentDate.getTime() <= stopDate.getTime()) {
      dateArray.push(new Date(currentDate));
      currentDate = new Date(currentDate.setDate(currentDate.getDate() + amount)); //number  of days to add, e.x. 15 days
    }
    return dateArray;
  }

  findDivipola(text) {
    const findDepartament = (code: string,lists :any) => {
      const d: any = lists.find(d => d.code === code);
      return d? d: false;
    }
    const places = text.split('|');
    console.log("entrada", text); 
    const options = places.map((divipola)=>{
      const place = divipola.split('-');
      let objectPlace: any  = null;
      console.log("lugar separado por -", place);
      if(place.length === 1) {
        const country: any = countries.find((c)=>(c.value === place[0]));
        if(country) {
          const { name, value, geoPoint } = country;
          objectPlace =  {name, code: value, geoPoint};
        }
      } else if(place.length === 2) {
        if (place[0] === 'CO') {
          const departament = place[0] + "-" +place[1]; 
          const d = findDepartament(departament, departaments);
          if(d) {
            const { code, text, geoPoint, geoRectangle } = d;
            objectPlace = { code: code, name: text, geoPoint: geoPoint, geoRectangle: geoRectangle}
          }
        }
      } else if(place.length === 3) {
        if (place[0] === 'CO') {
          const departament = place[0] + "-" +place[1]; 
          const d = findDepartament(departament, departaments);
          if(d) {
            const { municipality } = d;
            const munText = departament + "-" + place[2];
            const mun: any = findDepartament(munText,municipality);
            if(mun){
              const { code, text, geoPoint, geoRectangle } = mun
              objectPlace =  { code: code, name: text, geoPoint: geoPoint, geoRectangle: geoRectangle}
            }
          }
        }
      }
      return objectPlace;
    })
    if((options.filter(Boolean)).length === 0) {
      return null;
    } else {
      return options;
    }
  }

  matrixToObjects(m: any, statment) {
    return m
      .map((e, i) => {
        let row = {};
        if (i > 0) {
          for (let j = 0; j < m[0].length; j++) {
            const statmentElement = statment.filter(
              (statmentElement) => statmentElement.origin === m[0][j],
            )[0];
            let obj = null;
            const type = statmentElement
              ? statmentElement.type
                ? statmentElement.type
                : ''
              : '';

            if (
              ['select-multiple2', 'select-multiple2-tags', 'author'].includes(
                type,
              )
            ) {
              obj = e[j] ? e[j] === 'null'? 'null': e[j].split('|').map(item => item.trim()) : null;
            } else if (['select', 'select-recursive', 'text', 'pattern', 'textarea', 'simple-date'].includes(type)) {
              obj = e[j] ? e[j] : null;

            } else if (['range-date-year'].includes(type)) {
              const tempDate =
                typeof e[j] === 'string' ? e[j].split('|') : null;
              if (tempDate && JSON.stringify(tempDate) !== "[\"\"]") {
                if (tempDate.length === 1) {
                  obj = { start: this.dateToAAAAMMDD(tempDate[0]), end: '' };
                } else if (tempDate.length === 2) {
                  obj = {
                    start: this.dateToAAAAMMDD(tempDate[0]),
                    end: this.dateToAAAAMMDD(tempDate[1]),
                  };
                }
              } else {
                obj = { start: null, end: null };
              }
              obj = e[j] === 'null' ? 'null' : obj;
            }else if(['location'].includes(type)) {
              obj = e[j] === 'null' ? 'null': e[j]? this.findDivipola(e[j]): null;
            }

            row = { ...row, ...{ [m[0][j]]: obj } };
          }
        } else {
          return null;
        }
        return row;
      })
      .filter((elem) => elem !== null);
  }

  removeFile(file) {
    try {
      fs.unlinkSync(file.path);
      //file removed
    } catch (err) {
      console.error(err);
    }
  }

  public async xlsxCsvToJson(file, statment): Promise<any> {
    const path = file.path;
    const ext = file.filename.split('.').pop().toLowerCase();
    let objectPromise: any = new Promise(async (resolve) => {
      let json_data = {};
      if (['xls', 'xlsx'].includes(ext)) {
        var workbook = XLSX.readFile(path);
        workbook.SheetNames.forEach((name) => {
          let sheet = XLSX.utils.sheet_to_json(workbook.Sheets[name], {
            header: 1,
          });
          if (sheet.length) {
            json_data = sheet.filter((x: any) => x.length > 0);
            //console.log(json_data);
            resolve(this.matrixToObjects(json_data, statment));
          }
        });
      } else {
        if (['csv'].includes(ext)) {
          let json_csv = await CSV({
            output: 'csv',
            noheader: true,
            delimiter: [',', ';'],
          }).fromFile(path);
          json_data = json_csv;
          resolve(this.matrixToObjects(json_data, statment));
        }
      }
    });
    return objectPromise;
  }

  addPropertyToObject(object, propertyObject) {
    for (let property in propertyObject) {
      if (
        object.hasOwnProperty(property) &&
        propertyObject.hasOwnProperty(property)
      ) {
        const mergeChild = this.addPropertyToObject(
          object[property],
          propertyObject[property],
        );
        return { ...object, ...{ [property]: mergeChild } };
      } else {
        return { ...object, ...propertyObject };
      }
    }
  }

  objectTransformation(object, statment) {
    let newObject = null;
    for (let i = 0; i < statment.length; i++) {
      const { destiny, origin, defaultValue } = statment[i];
      if (destiny) {
        const newDestiny = `"${(destiny.split(".")).join('"."')}"`;
        const obj = JSON.parse(
          `{${newDestiny.split('.').join(':{')}:${object.hasOwnProperty(origin)
            ? JSON.stringify(object[origin])
            : JSON.stringify(defaultValue)
          }${'}'.repeat(newDestiny.split('.').length)}`,
        );
        if (newObject === null) {
          newObject = obj;
        } else {
          newObject = this.addPropertyToObject(newObject, obj);
        }
      }
    }
    return newObject;
  }

  is_array(value) {
    return Object.prototype.toString.call(value) === '[object Array]';
  }

  getMetadatePlain(object, hashAuth = null) {
    var moment = require('moment');
    let history = [""];

    if (object?.extra?.history) {
      history = this.is_array(object.extra.history) ? [object.extra.history.map(
        (h) =>
          `${h.status === "created" ? "Creado" : "Actualizado"} el ${moment(h.date)
            .local()
            .format("YYYY-MM-DD HH:mm:ss")} ${h.username ? 'por' : ''} ${h.username ? h.username : ''}`
      )
      ] : [""];
    }

    const metadata = {
      ...(object.ident ? { ident: object.ident } : {}),
      ...(object.metadata.firstLevel ? object.metadata.firstLevel : {}),
      ...(object.metadata.missionLevel
        ? object.metadata.missionLevel.humanRights
          ? object.metadata.missionLevel.humanRights
          : {}
        : {}),
      ...(object.metadata.missionLevel
        ? object.metadata.missionLevel.target
          ? object.metadata.missionLevel.target
          : {}
        : {}),
      ...(object.metadata.missionLevel
        ? object.metadata.missionLevel.source
          ? object.metadata.missionLevel.source
          : {}
        : {}),
      ...(object.origin ? { origin: object.origin } : {}),
      ...{ history: history },
      ...{ authorizationRights: hashAuth },
      ...{ status: object?.extra?.status },

    };
    return metadata;
  }

  getMetadataHtml(object) {
    const metadata = {
      ...(object.ident ? { ident: object.ident } : {}),
      ...(object.datarecords ? { filename: object.datarecords } : {}),
      ...(object.metadata.firstLevel ? object.metadata.firstLevel : {}),
      ...(object.metadata.missionLevel
        ? object.metadata.missionLevel.humanRights
          ? object.metadata.missionLevel.humanRights
          : {}
        : {}),
      ...(object.metadata.missionLevel
        ? object.metadata.missionLevel.target
          ? object.metadata.missionLevel.target
          : {}
        : {}),
      ...(object.metadata.missionLevel
        ? object.metadata.missionLevel.source
          ? object.metadata.missionLevel.source
          : {}
        : {}),
    };

    let metadataList = [];
    for (let meta in metadata) {
      if (metadata.hasOwnProperty(meta)) {
        if (metadata[meta]) {
          let label = labels[meta] ? labels[meta] : meta;
          let item = '';

          if (
            Object.prototype.toString.call(metadata[meta]) === '[object Array]'
          ) {
            let listItem = metadata[meta]
              .map((data) => {
                let itemData = '';
                if (label == 'Ruta original') {
                  itemData = !(data.filename) ? 'NO REGISTRA' : data.filename;
                }
                if (
                  Object.prototype.toString.call(data) === '[object Object]'
                ) {
                  if (data.geoPoint) {
                    itemData = 'NO REGISTRA';
                    if (data.geoPoint.lat && data.geoPoint.lon) {
                      itemData = `<a target="_blank" href="https://maps.google.com/?q=${data.geoPoint.lat ? data.geoPoint.lat : ''
                        },${data.geoPoint.lon ? data.geoPoint.lon : ''}">${data.name
                          ? data.name
                          : '' + data.originalLocation
                            ? data.originalLocation
                            : ''
                        } ${data.geoPoint.lat ? data.geoPoint.lat : ''},${data.geoPoint.lon ? data.geoPoint.lon : ''
                        } </a>`;
                    } else if (data.name) {
                      itemData = `${data.name}`;
                    }
                  }
                } else {
                  itemData = data;
                }
                return `<span>-${itemData}</span>`;
              })
              .reduce((a, b) => a + b, '');
            item = `<div class="meta-list"> 
                          <span class="form-label">${label} </span>
                          <div class="meta-value">${listItem} </div>
                        </div>`;
            metadataList.push(item);
          } else if (
            Object.prototype.toString.call(metadata[meta]) === '[object String]'
          ) {
            item = `<div class="meta-list"> 
              <span class="form-label">${label} </span>
              <span class="meta-value">${metadata[meta]} </span>
              </div>`;
            metadataList.push(item);
          } else if (
            Object.prototype.toString.call(metadata[meta]) === '[object Object]'
          ) {
            let dataObject = '';
            if (
              typeof metadata[meta].start !== 'undefined' &&
              metadata[meta].start
            ) {
              let start = metadata[meta].start
                ? this.dateToAAAAMMDD(
                  `${metadata[meta].start}`.replace('COT', ''),
                )
                : '';
              let end = metadata[meta].end
                ? this.dateToAAAAMMDD(
                  `${metadata[meta].end}`.replace('COT', ''),
                )
                : '';
              start = start ? start : 'No registra';
              end = end ? end : 'No registra';
              dataObject = !(start && end)
                ? 'NO REGISTRA'
                : `${start} -  ${end}`;
              item = `<div class="meta-list"> 
                <span class="form-label">${label} </span>
                <span class="meta-value">${dataObject} </span>
                </div>`;
              metadataList.push(item);
            }
          }
        }
      }
    }
    return metadataList.reduce((a, b) => a + b, '');
  }

  getMetadataHtmlFromStatment(object, statment) {
    const metadata = this.elementTransformationInverse(object, statment);
    let metadataList = [];
    for (let meta in metadata) {
      if (metadata.hasOwnProperty(meta)) {
        let label = labels[meta] ? labels[meta] : meta;
        let item = '';
        if (metadata[meta]) {
          if (
            Object.prototype.toString.call(metadata[meta]) === '[object Array]'
          ) {
            let listItem = metadata[meta]
              .map((data) => {
                let itemData = '';
                if (label == 'Ruta original') {
                  itemData = !(data.filename) ? 'NO REGISTRA' : data.filename;
                }
                if (
                  Object.prototype.toString.call(data) === '[object Object]'
                ) {
                  if (data.geoPoint) {
                    itemData = 'NO REGISTRA';
                    if (data.geoPoint.lat && data.geoPoint.lon) {
                      itemData = `<a target="_blank" href="https://maps.google.com/?q=${data.geoPoint.lat ? data.geoPoint.lat : ''
                        },${data.geoPoint.lon ? data.geoPoint.lon : ''}">${data.name
                          ? data.name
                          : '' + data.originalLocation
                            ? data.originalLocation
                            : ''
                        } ${data.geoPoint.lat ? data.geoPoint.lat : ''},${data.geoPoint.lon ? data.geoPoint.lon : ''
                        } </a>`;
                    } else if (data.name) {
                      itemData = `${data.name}`;
                    }
                  }
                } else {
                  itemData = data;
                }
                return `<span>-${itemData}</span>`;
              })
              .reduce((a, b) => a + b, '');
            item = `<div class="meta-list"> 
                          <span class="form-label">${label} </span>
                          <div class="meta-value">${listItem} </div>
                        </div>`;
            metadataList.push(item);
          } else if (
            Object.prototype.toString.call(metadata[meta]) === '[object String]'
          ) {
            item = `<div class="meta-list"> 
              <span class="form-label">${label} </span>
              <span class="meta-value">${metadata[meta]} </span>
              </div>`;
            metadataList.push(item);
          } else if (
            Object.prototype.toString.call(metadata[meta]) === '[object Object]'
          ) {
            let dataObject = '';
            if (
              typeof metadata[meta].start !== 'undefined' &&
              metadata[meta].start
            ) {
              const start = metadata[meta].start
                ? this.dateToAAAAMMDD(
                  `${metadata[meta].start}`.replace('COT', ''),
                )
                : '';
              const end = metadata[meta].end
                ? this.dateToAAAAMMDD(
                  `${metadata[meta].end}`.replace('COT', ''),
                )
                : '';
              dataObject = !(start && end)
                ? 'NO REGISTRA'
                : `${start} -  ${end}`;
              item = `<div class="meta-list"> 
                <span class="form-label">${label} </span>
                <span class="meta-value">${dataObject} </span>
                </div>`;
              metadataList.push(item);
            }
          }
        } else {
          item = `<div class="meta-list"> 
              <span class="form-label">${label} </span>
              <span class="meta-value"> No registra </span>
              </div>`;
          metadataList.push(item);
        }
      }
    }
    return metadataList.reduce((a, b) => a + b, '');
  }

  objectTransformationInverse(object, statment) {
    return object.map((element) => {
      let newObject = null;
      for (let i = 0; i < statment.length; i++) {
        const { destiny, origin, defaultValue } = statment[i];
        const path = `element.${destiny}`;
        let objectTrasform = defaultValue;
        if (JSON.stringify(element) == '{}') {
          objectTrasform = 'REGISTRO NO ENCONTRADO';
        } else {
          try {
            objectTrasform = eval(path) ? eval(path) : defaultValue;
          } catch (error) {
            console.error(error);
          }
        }
        newObject = {
          ...newObject,
          ...{ [origin]: objectTrasform },
        };
      }
      return newObject;
    });
  }

  elementTransformationInverse(element, statment) {
    let newObject = null;
    for (let i = 0; i < statment.length; i++) {
      const { destiny, origin, defaultValue } = statment[i];
      const path = `element.${destiny}`;
      let objectTrasform = defaultValue;
      if (JSON.stringify(element) == '{}') {
        objectTrasform = 'REGISTRO NO ENCONTRADO';
      } else {
        try {
          objectTrasform = eval(path) ? eval(path) : defaultValue;
        } catch (error) {
          console.error(error);
        }
      }
      newObject = {
        ...newObject,
        ...{ [origin]: objectTrasform },
      };
    }
    return newObject;

  }


  getText(path, ext) {
    if (ext === 'pdf') {
      return this.getPdfExtract(path);
      // return this.defaultExtract();
    } else if (ext === 'docx') {
      return this.getDocxExtract(path)
      //return this.defaultExtract();
    } else if (ext === 'doc') {
      return this.getDocExtract(path)
      //return this.defaultExtract();
    } else if (['odt', 'ott', 'txt'].includes(ext)) {
      return this.getTextract(path);
      //return this.defaultExtract();
    } else {
      return this.defaultExtract();
    }
  }

  defaultExtract() {
    return new Promise((resolve) => {
      resolve('');
    });
  }

  getTextract(filepath) { // use https://www.npmjs.com/package/textract
    const textData = new Promise((resolve, reject) => {
      textract.fromFileWithPath(filepath, ((err, text) => {
        //console.log(text)
        if (text) {
          resolve(text);
        } else {
          resolve('')
        }
      }))
    });
    return textData
  }

  getDocxExtract(filepath) {
    const textData = new Promise((resolve, reject) => {
      docxExtractMammoth.extractRawText({ path: filepath })
        .then(function (result) {
          if (result.value) {
            resolve(result.value)
          } else {
            resolve('')
          }
        })
        .done();
    });
    return textData
  }

  getDocExtract(filepath) {
    const textData = new Promise((resolve, reject) => {
      var extractor = new WordExtractor();
      var extracted = extractor.extract(filepath);
      extracted.then(function (doc) {
        const resultText = doc.getBody();
        if (resultText) {
          resolve(resultText)
        } else {
          resolve('')
        }
      });
    });
    return textData
  }

  // getPdfExtract(filePath) {
  //   const textData = new Promise((resolve, reject) => {
  //     pdfTextExtract(filePath, { splitPages: false }, (err, text) => {
  //       if (err) {
  //         resolve('');
  //         return
  //       }
  //       const data = (JSON.stringify(text)).replace(/  +/g, ' ');
  //       if (data.length < 200) {
  //         tesseract.process(filePath, (err, text) => {
  //           if (err) {
  //             console.error(err);
  //             resolve('');
  //           } else {
  //             console.log(text);
  //             resolve(text);
  //           }
  //         })
  //       } else {
  //         resolve((JSON.parse(data)).join('\\n'));
  //       }
  //     })
  //   });
  //   return textData
  // }
  getPdfExtract(filePath) {
    const textData = new Promise((resolve, reject) => {
      pdfTextExtract(filePath, { splitPages: false }, (err, text) => {
        if (err) {
          resolve('');
          return
        }
        const data = (JSON.stringify(text)).replace(/  +/g, ' ')
        resolve((JSON.parse(data)).join('\\n'));
      })
    });
    return textData
  }

  // getPdfText(path) {
  //   let dataBuffer = fs.readFileSync(`${path}`);
  //   const textData = new Promise((resolve, reject) => {
  //     pdf(dataBuffer).then(function (data) {
  //       resolve(data);
  //     })
  //       .catch(function (error) {
  //         console.log(error.message)
  //         resolve('');
  //       })
  //   })
  //   return textData;
  // }

  uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(
      /[xy]/g,
      function (c) {
        var r = (Math.random() * 16) | 0,
          v = c == 'x' ? r : (r & 0x3) | 0x8;
        return v.toString(16);
      },
    );
  }
}

