import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { UtilService } from './util.service';

//import { ConfigModule } from '../config/config.module';
//import { ConfigService } from '../config/config.service';

//https://medium.com/@alexb72/how-to-send-emails-using-a-nodemailer-gmail-and-oauth2-fe19d66451f9

const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;

const myOAuth2Client = new OAuth2("90375686739-gq9sbedm0lnlk54t08do46lp79f8h4kj.apps.googleusercontent.com","TlY0S6CH5MnqN8gPv223dxku",)
myOAuth2Client.setCredentials({refresh_token:"1//04ktyWv5nvWpNCgYIARAAGAQSNwF-L9IrL_aQpfE6-nwoYFzueAG8niSDXpu_Vvxalaooc8EaP-bUa6HdSFi1Xo_qzJJDGi1TrRs"});
const myAccessToken = myOAuth2Client.getAccessToken()


/*
@Module({
  providers: [MeService],
  controllers: [MeController],
  imports: [
    HttpModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        baseURL: configService.get('app').profileBaseUrl,
      }),
      inject: [ConfigService],
    }),
  ],
})
export class MeModule {}
*/

@Module({
  imports: [
    MailerModule.forRoot({
      transport: {
        service: "gmail",
        auth: {
          type: "OAuth2",
          user: "sim@comisiondelaverdad.co", //your gmail account you used to set the project up in google cloud console"
          clientId: "90375686739-gq9sbedm0lnlk54t08do46lp79f8h4kj.apps.googleusercontent.com",
          clientSecret: "TlY0S6CH5MnqN8gPv223dxku",
          refreshToken: "1//04ktyWv5nvWpNCgYIARAAGAQSNwF-L9IrL_aQpfE6-nwoYFzueAG8niSDXpu_Vvxalaooc8EaP-bUa6HdSFi1Xo_qzJJDGi1TrRs",
          accessToken: myAccessToken //access token variable we defined earlier
        }},
      defaults: {
        from:'"Sistema de Información Misional - Comisión de la Verdad" <sim@comisiondelaverdad.co>',
      },
      template: {
        dir: __dirname + '/templates',
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    }),
  ],
  providers: [UtilService]
})
export class UtilModule {}
