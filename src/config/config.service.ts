import * as dotenv from 'dotenv';
import * as fs from 'fs';

export class ConfigService {
  MONGODB_URI: string;
  private readonly envConfig: { [key: string]: string };

  constructor() {
    if (
      process.env.NODE_ENV === 'production' ||
      process.env.NODE_ENV === 'staging'
    ) {
      this.envConfig = {
        MONGODB_URI: process.env.MONGODB_URI
      };
    } else {
      let secureEnv = require('secure-env');
      let crypt = require('crypto');
      let env = dotenv.parse(fs.readFileSync('.env'));
      let pwd = env['PWD'];
      let skString = env['SK'];
      let arrayPwd = pwd.split(':');
      const algoritmo = 'aes-256-cbc';
      let iv = Buffer.from(arrayPwd[0], 'hex');
      let sk = Buffer.from(skString, 'hex'); 
      let pwd2 = arrayPwd[1];
      const decipher = crypt.createDecipheriv(algoritmo, sk, iv);
      let clave = decipher.update(pwd2, "hex", "utf-8");
      clave += decipher.final("utf8");
      this.envConfig = secureEnv({secret:clave});
      }
  }
  

  get(key: string): string {
    return this.envConfig[key];
  }
}


