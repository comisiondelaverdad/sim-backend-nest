import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document ,Types} from 'mongoose';
import { Piece } from './piece.schema';
import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';

@Schema()
export class Card extends Document {
  @ApiProperty({type: [Piece]})
  @Prop({ type: [{type: Object}]})
  pieces: Piece[]; 
 
  @ApiProperty()
  @Prop({type:"string"})
  order: string;

  @ApiProperty()
  @Prop({type:"string"})
  id: string;

  @ApiProperty()
  @Prop({type:Object})
  popupContent: Object;

  @ApiProperty()
  @Prop({type:Object})
  sensibleContent: Object;

}

export const CardSchema = SchemaFactory.createForClass(Card);