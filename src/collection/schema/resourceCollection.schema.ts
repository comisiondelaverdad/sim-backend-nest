import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document , Types } from 'mongoose';
import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';

@Schema()
export class ResourceCollection extends Document {
  @ApiPropertyOptional()
  @Prop( {type: Types.ObjectId})
  _id: string;

  @ApiProperty()
  @Prop({type:"string"})
  ident: string;

  @ApiPropertyOptional()
  @Prop({type:"string"})
  type: string;

  @ApiPropertyOptional()
  @Prop({type:"string"})
  title: string;  

}
export const ResourceCollectionSchema = SchemaFactory.createForClass(ResourceCollection);