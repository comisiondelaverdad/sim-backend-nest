import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';

@Schema()
export class Location extends Document {
    @ApiPropertyOptional()
    @Prop({type:"object"})
    geoPoint: object;

    @ApiPropertyOptional()
    @Prop({type:"String"})
    ident: string;  

    @ApiPropertyOptional()
    @Prop({type:"String"})
    value: string;

    @ApiPropertyOptional()
    @Prop({type:"String"})
    name: string;

    @ApiPropertyOptional()
    @Prop({type:"String"})
    description: string;

    @ApiPropertyOptional()
    @Prop({type:"String"})
    code: string;

    @ApiPropertyOptional()
    @Prop({type:"String"})
    code2: string;
    
    @ApiPropertyOptional()
    @Prop({type:"String"})
    type: string;

    @ApiPropertyOptional()
    @Prop({type:"boolean", default:false})
    resource: boolean;
}

export const FrameSchema = SchemaFactory.createForClass(Location);