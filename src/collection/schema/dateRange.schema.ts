import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document ,Types} from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

@Schema()
export class 
DateRange extends Document {  
  @ApiProperty()
  @Prop({ type: Date, format: 'date-time' })
  start: Date;

  @ApiProperty()
  @Prop({ type: Date, format: 'date-time' })
  end: Date;
}

export const DateSchema = SchemaFactory.createForClass(Date);