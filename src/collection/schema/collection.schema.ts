import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document , Types} from 'mongoose';
import { User } from 'src/user/schema/user.schema';
import { Card} from './card.schema';
import { Location} from './location.shema';
import { DateRange} from './dateRange.schema';
import { Piece} from './piece.schema';
import { Place } from './place.shema';
import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';

@Schema( { minimize: false })
export class Collection extends Document {

  @ApiProperty()
  @Prop( {type: Types.ObjectId, ref: 'User'})
  user: User;

  @ApiProperty()
  @Prop({type:String})
  title: String;

  @ApiProperty()
  @Prop({type:String})
  slug: String;
  
  @ApiProperty()
  @Prop({type:String})
  author: String;

  @ApiProperty()
  @Prop({type:String})
  type_author: String;

  @ApiPropertyOptional()
  @Prop({type:String})
  area_author: String;

  @ApiProperty()
  @Prop({type:Object})
  place_creation:  Place;

  @ApiProperty()
  @Prop({type:Object})
  cover_page:  Piece;

  @ApiProperty()
  @Prop({type:String})
  description: String;

  @ApiProperty()
  @Prop({type:String})
  category: String;

  @ApiProperty()
  @Prop({type:String})
  type: String;

  @ApiPropertyOptional()
  @Prop({type:String})
  mandate: String;

  @ApiPropertyOptional()
  @Prop({type:String})
  topic: String;

  @ApiPropertyOptional()
  @Prop({ type: [{type: String}]})
  parent: String[];
  
  @ApiProperty({type: [Card]})
  @Prop({ type: [{type: Object}]})
  cards: Card[]; 

  @ApiProperty()
  @Prop({ type: [{type: String}]})
  keywords:  String[];

  @ApiProperty()
  @Prop({ type: [{type: String}]})
  keywords_resources:  String[];
  
  @ApiProperty({type: [Location]})
  @Prop({ type: [{type: Object}]})
  geographicCoverage: Location[];

  @ApiProperty()
  @Prop({type:Object})
  temporalCoverage:  DateRange;

  @ApiPropertyOptional()
  @Prop({type:"boolean", default:false})
  publish: Boolean;

  @ApiPropertyOptional()
  @Prop({ type: [{type: Types.ObjectId, ref: 'User'}]})
  shared: User[];
 
  @ApiProperty()
  @Prop({ type: Date, format: 'date-time' })
  createdAt: Date;

  @ApiProperty()
  @Prop({ type: Date, format: 'date-time' })
  updatedAt: Date;

  @ApiProperty()
  @Prop({type:"boolean", default:false})
  indexed: Boolean;

  @ApiProperty()
  @Prop({type:"boolean", default:false})
  deleted: Boolean;
  
  @ApiProperty()
  @Prop({type:String})
  objective: String;

  @ApiProperty()
  @Prop({type:Object})
  ilustration: Object;

}

export const CollectionSchema = SchemaFactory.createForClass(Collection);