import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Location} from './location.shema';
import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';

@Schema()
export class Place extends Document {
  @ApiProperty({type: [Location]})
  @Prop({ type: [{type: Object}]})
  country: Location; 
  
  @ApiPropertyOptional({type: [Location]})
  @Prop({ type: [{type: Object}]})
  departament: Location; 
    
  @ApiPropertyOptional({type: [Location]})
  @Prop({ type: [{type: Object}]})
  municipality: Location; 
}

export const FrameSchema = SchemaFactory.createForClass(Location);