import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document , Types } from 'mongoose';
import { Record } from 'src/record/schema/record.schema';
import { ResourceCollection } from './resourceCollection.schema';
import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';

@Schema()
export class Piece extends Document {
  @ApiPropertyOptional()
  @Prop( {type: Types.ObjectId})
  _id: string;

  @ApiPropertyOptional()
  @Prop({type:"object"})
  resource: ResourceCollection;

  @ApiPropertyOptional()
  @Prop({type:"string"})
  type: string;

  @ApiPropertyOptional()
  @Prop({type:"string"})
  value: string;

  @ApiProperty()
  @Prop({type:"number"})
  order: BigInteger;

  @ApiPropertyOptional({type: [Record]})
  @Prop({ type: [{type: Object}]})
  records: Record[]; 

  @ApiPropertyOptional()
  @Prop({type:"string"})
  path: string;

  @ApiPropertyOptional()
  @Prop({type:"string"})
  alt: string;

  @ApiPropertyOptional()
  @Prop({type:"string"})
  text: string;

  @ApiPropertyOptional()
  @Prop({type:"string"})
  min: string;

  @ApiPropertyOptional()
  @Prop({type:"string"})
  max: string;


}


export const PieceSchema = SchemaFactory.createForClass(Piece);