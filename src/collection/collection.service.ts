import { Injectable, HttpService } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import * as https from 'https';
import * as _ from 'lodash';
import * as fs from 'fs';
import { Collection } from './schema/collection.schema';
import { CollectionDto } from './dto/collection.dto';
import { CreateCollectionDto } from './dto/createCollection.dto';
import { FinKeyValueMetadata } from './dto/finKeyValueMetadata.dto';
import { FindCategoryDto } from './dto/findCategory.dto';
import { AggregationName } from './dto/aggregationName.dto';
import { FindKeywordDto } from './dto/findKeyword.dto';
import { PieceDto } from './dto/piece.dto';
import { Resource } from 'src/resource/schema/resource.schema';
import { Card } from './schema/card.schema';
import { Piece } from './schema/piece.schema';
import { ResourceService } from 'src/resource/resource.service';
import { CollectionBookmarkService } from 'src/collection-bookmark/collection-bookmark.service';
import { ResourceCollection } from './schema/resourceCollection.schema';
import { User } from 'src/user/schema/user.schema';
import { ConfigService } from '../config/config.service';
const slugify = require('slugify');

@Injectable()
export class CollectionService {
  config: any = undefined;
  httpsAgent = null;
  constructor(
    private readonly resourceService: ResourceService,
    private readonly collectionBookmarkService: CollectionBookmarkService,
    private configX: ConfigService,
    private http: HttpService,
    @InjectModel('Collection') private collectionModel: Model<Collection>,
    @InjectModel('Card') private cardModel: Model<Card>,
    @InjectModel('Piece') private pieceModel: Model<Piece>,
    @InjectModel('ResourceCollection')
    private resourceCollectionModel: Model<ResourceCollection>,
    @InjectModel('User') private userModel: Model<User>,
  ) {
    let crtFile = fs.readFileSync(configX.get('ELASTIC_AUTH_CERT'));
    let agent = new https.Agent({ ca: crtFile });
    this.httpsAgent = agent;
    this.config = {
      elastic: {
        url: configX.get('ELASTIC_URL'),
        index: {
          data: configX.get('ELASTIC_INDEX_DATA'),
          env: configX.get('ELASTIC_INDEX_ENV'),
          museo: configX.get('ELASTIC_INDEX_MUSEO'),
        },
        headers: {
          auth: {
            username: configX.get('ELASTIC_AUTH_USER'),
            password: configX.get('ELASTIC_AUTH_PASS'),
          },
        },
      },
    };
  }

  async createCollection(
    collectionDto: CollectionDto,
  ): Promise<CreateCollectionDto> {
    const createdCollection = new this.collectionModel(collectionDto);
    createdCollection.createdAt = new Date(Date.now());
    createdCollection.updatedAt = createdCollection.createdAt;
    // let slug = slugify(collectionDto.title);
    let slug = slugify(collectionDto.title, {
      lower: true,
    });
    const resp = await this.getCollectionBySlug(slug);
    if (resp) slug = slug + '-1';

    createdCollection.slug = slug;
    const collectionSave = await createdCollection.save();
    const created = collectionSave == createdCollection;
    if (created) {
      const reshapingOptions = {
        virtuals: true,
        versionKey: false,
        transform: function (doc, ret) {
          delete ret._id;
          delete ret.id;
          return ret;
        },
      };
      this.indexCollection(
        collectionSave.toObject(reshapingOptions),
        collectionSave._id,
      );
    }
    const ccollection: CreateCollectionDto = {
      created: created,
      id: collectionSave._id,
      slug: collectionSave.slug,
    };
    return ccollection;
  }

  async deleteCollection(idCollection: string): Promise<Boolean> {
    let deleteCol = false;
    const reshapingOptions = {
      virtuals: true,
      versionKey: false,
      transform: function (doc, ret) {
        delete ret._id;
        delete ret.id;
        return ret;
      },
    };
    const collectionUpdate = await this.collectionModel
      .findOneAndUpdate(
        { _id: idCollection },
        { $set: { deleted: true } },
        {
          new: true,
          useFindAndModify: false,
        },
        function (err, doc) {
          if (err) {
            return false;
          } else {
            return doc;
          }
        },
      )
      .exec();

    if (collectionUpdate) {
      deleteCol = true;
      this.indexCollection(
        collectionUpdate.toObject(reshapingOptions),
        idCollection,
      );
    }

    return deleteCol;
  }

  async getCollectionById(id: string): Promise<Collection> {
    return this.collectionModel.findOne({ _id: id }).exec();
  }

  async getCollectionBySlug(slug: string): Promise<Collection> {
    return this.collectionModel.findOne({ slug: slug }).exec();
  }

  async getResumeCollectionByUser(id: string): Promise<Collection> {
    return this.collectionModel
      .find(
        { $and: [{ user: id }, { deleted: { $ne: true } }] },
        { cover_page: 1, title: 1, description: 1, _id: 1 },
      )
      .exec();
  }


  async getResumeCollectionAll(): Promise<Collection> {
    return this.collectionModel
      .find(
        { $and: [{ deleted: { $ne: true } }] },
        { cover_page: 1, title: 1, description: 1, _id: 1 },
      )
      .exec();
  }

  async getCollectionIdsByUser(id: String): Promise<string[]> {
    return this.collectionModel
      .find({ $and: [{ user: id }, { deleted: { $ne: true } }] })
      .exec()
      .then((Collections) => {
        let CollectionsId = _.flatMap(Collections, 'id');
        return _.uniq(CollectionsId).sort();
      });
  }

  async getCollectionbyUser(id: String): Promise<Collection> {
    return this.collectionModel
      .find({ $and: [{ user: id }, { deleted: { $ne: true } }] })
      .exec()
      .then();
  }

  async getAllCollectionIDs(): Promise<Collection> {
    return this.collectionModel
    .find({ $and: [{ deleted: { $ne: true } }] })
    .exec()
    .then((Collections) => {
      let CollectionsId = _.flatMap(Collections, 'id');
      return _.uniq(CollectionsId).sort();
    });
  }

  async updateCollection(
    idCollection: String,
    collection: CollectionDto,
  ): Promise<Boolean> {
    let update = false;
    const collectionModel = new this.collectionModel(collection);
    collectionModel.updatedAt = new Date(Date.now());
    const reshapingOptions = {
      virtuals: true,
      versionKey: false,
      transform: function (doc, ret) {
        delete ret._id;
        delete ret.id;
        return ret;
      },
    };
    const collObj = collectionModel.toObject(reshapingOptions);
    const collectionUpdate = await this.collectionModel
      .findOneAndUpdate(
        { _id: idCollection },
        collObj,
        {
          new: true,
          useFindAndModify: false,
        },
        function (err, doc) {
          if (err) {
            return false;
          } else {
            return doc;
          }
        },
      )
      .exec();

    if (collectionUpdate) {
      update = true;
      this.indexCollection(
        collectionUpdate.toObject(reshapingOptions),
        idCollection,
      );
    }
    return update;
  }

  async findCollection(body: FindKeywordDto): Promise<Collection> {
    return await this.collectionModel
      .find({ title: new RegExp(body.keyWord, 'i'), publish: true })
      .exec();
  }

  async findKeywords(): Promise<Object> {
    return await this.collectionModel
      .aggregate([
        { $unwind: '$keywords' },
        { $group: { _id: '$keywords', totaldocs: { $sum: 1 } } },
      ])
      .exec();
  }

  async aggregation(body: AggregationName): Promise<Object> {
    if (body.name == 'user') {
      const resAgg = await this.collectionModel
        .aggregate([
          { $unwind: '$' + body.name },
          { $group: { _id: '$' + body.name, totaldocsUser: { $sum: 1 } } },
        ])
        .exec();
      await this.userModel.populate(resAgg, { path: '_id' });

      return resAgg;
    }
    return await this.collectionModel
      .aggregate([
        { $unwind: '$' + body.name },
        { $group: { _id: '$' + body.name, totaldocs: { $sum: 1 } } },
      ])
      .exec();
  }
  async findCollectionsBykeyword(body): Promise<Collection> {
    if (body.keywords) {
      if (Array.isArray(body.keywords))
        return await this.collectionModel
          .find({ keywords: { $in: [[body.keywords[0]]] } })
          .exec();
      else
        return await this.collectionModel
          .find({ keywords: { $in: [body.keywords] } })
          .exec();
    } else {
      return await this.collectionModel
        .find({ keywords_resources: { $in: [body.keywords_resources] } })
        .exec();
    }
  }
  async findCollectionsByCategory(body: FindCategoryDto): Promise<Collection> {
    return await this.collectionModel
      .find({ category: { $in: [body.category] } })
      .exec();
  }
  async findCollectionsByMetadata(body: FinKeyValueMetadata): Promise<Object> {
    let json = {
      query: {
        bool: {
          filter: [
            {
              terms: {
                [body.key]: body.values,
              },
            },
            {
              bool: {
                must_not: {
                  term: {
                    deleted: true,
                  },
                },
              },
            },
          ],
        },
      },
      size: 1000,
      from: 0,
    };

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    let response = await this.http
      .post(
        this.config.elastic.url +
          'sim-museo-col-' +
          this.config.elastic.index.env +
          '/_search',
        json,
        h,
      )
      .toPromise()
      .then();

    return response.data;
  }

  async indexCollection(collection: any, id: String): Promise<Boolean> {
    let indexed = false;
    collection['indexed'] = true;
    let responseIndexed = await this.createIndexedCollection(collection, id);
    if (responseIndexed) {
      await this.collectionModel
        .updateOne({ _id: id }, { indexed: true })
        .exec();
      const col = await this.collectionModel
        .findOneAndUpdate(
          { _id: id },
          { indexed: true },
          {
            new: true,
            useFindAndModify: false,
          },
        )
        .exec();
      indexed = col.indexed;
    }

    return indexed;
  }

  async createIndexedCollection(
    collection: any,
    idIndex: String,
  ): Promise<boolean> {
    let change = false;
    try {
      let h = {
        auth: this.config.elastic.headers.auth,
        httpsAgent: this.httpsAgent,
      };
      let response = await this.http
        .post(
          this.config.elastic.url +
            this.config.elastic.index.museo +
            '-col-' +
            this.config.elastic.index.env +
            '/_doc/' +
            idIndex,
          collection,
          h,
        )
        .toPromise();
      let response_data = response.data;
      change =
        response_data['result'] == 'created' ||
        response_data['result'] == 'updated';
    } catch (e) {
      console.log(e);
    }
    return change;
  }

  async searchIndexedCollections(params): Promise<any> {
    let query = params;
    if (query.random) return this.searchRandomCollections(query);
    let json = {
      "_source": {
        // "includes": [
          
        // ]
      },
      "query": {
        "bool": {
          "filter": [],
          "must": [
            {"term": {
              "type": "Narrativa"
            }}
          ],
          "should": [],
          "must_not": [
            {"term": {
              "deleted": true
            }}
          ]
        }
      },
      "size": 50,
      "track_total_hits": true
    }

    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let resp: any = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.museo + '-col-' + this.config.elastic.index.env + "/_search",
      json,
      h
    ).toPromise()

    return resp.data
  }

  async searchRandomCollections(query) : Promise<any> {
    let json = {
      _source: {},
      query: {
        function_score: {
          query: {
            bool: {
              must: [
                {
                  term: {
                    type: 'Narrativa',
                  },
                },
              ],
              should: [],
              must_not: [
                {
                  term: {
                    deleted: true,
                  },
                },
              ],
            },
          },

          functions: [
            {
              random_score: {
                seed: Date.now(),
              },
            },
          ],
        },
      },
      size: query.size ? query.size : 50,
      track_total_hits: true,
    };

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };

    try {
      let resp = await this.http
        .post(
          this.config.elastic.url +
            this.config.elastic.index.museo +
            '-col-' +
            this.config.elastic.index.env +
            '/_search',
          json,
          h,
        )
        .toPromise();
      return resp.data;
    } catch (error) {
      console.log(error);
    }
  }
}
