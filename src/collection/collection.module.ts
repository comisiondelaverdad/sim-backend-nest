
import { Module, HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { CollectionBookmarkModule } from 'src/collection-bookmark/collection-bookmark.module';
import { CollectionBookmarkService } from 'src/collection-bookmark/collection-bookmark.service';
import { ResourceModule } from 'src/resource/resource.module';
import { ResourceService } from 'src/resource/resource.service';
import { CollectionController } from './collection.controller';
import { CollectionService } from './collection.service';
import { CollectionSchema } from './schema/collection.schema';
import { ResourceCollectionSchema } from './schema/resourceCollection.schema';
import { CardSchema } from './schema/card.schema';
import { PieceSchema } from './schema/piece.schema';
import { UserSchema } from 'src/user/schema/user.schema';
import { ConfigService } from '../config/config.service';
import { ConfigModule } from '../config/config.module';


@Module({

  imports: [
    MongooseModule.forFeature([
     { name: 'Collection',schema: CollectionSchema },
     { name: 'Card', schema: CardSchema },
     { name: 'Piece', schema: PieceSchema },
     { name: 'ResourceCollection', schema: ResourceCollectionSchema },
     {name: 'User', schema: UserSchema}], "database"),
     ConfigModule,
     HttpModule,
    ResourceModule,
    CollectionBookmarkModule,
    HttpModule,
    ConfigModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
  ],
  controllers: [CollectionController],
  providers: [CollectionService,ConfigService]
})
export class CollectionModule {}
