import {
  Post,
  Delete,
  Get,
  Param,
  Controller,
  Body,
  Request,
  Put,
  UseGuards,
} from '@nestjs/common';
import { CollectionDto } from './dto/collection.dto';
import { CreateCollectionDto } from './dto/createCollection.dto';
import { FindKeywordsDto } from './dto/findKeywords.dto';
import { FindKeywordDto } from './dto/findKeyword.dto';
import { FindCategoryDto } from './dto/findCategory.dto';
import { FinKeyValueMetadata } from './dto/finKeyValueMetadata.dto';
import { AggregationName } from './dto/aggregationName.dto';
import { CollectionService } from './collection.service';
import { AuthGuard } from '@nestjs/passport';
import { Collection } from './schema/collection.schema';
import { matchRolesListBoolean } from '../auth/roles.guard';

import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiParam,
  ApiTags,
  ApiBody,
} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('Collection')
@Controller('api/collection')
export class CollectionController {
  constructor(
    private readonly collectionService: CollectionService,
  ) {}

  @ApiResponse({
    status: 201,
    description: 'This collection has been create succesfully',
    type: CreateCollectionDto,
  })
  @ApiOperation({ summary: 'Create collection' })
  @Post()
  @UseGuards(AuthGuard())
  async create(@Body() collectionDto: CollectionDto) {
    const response = await this.collectionService.createCollection(
      collectionDto,
    );
    if (response) {
    }
    return response
  }

  @ApiOperation({ summary: 'Update collection' })
  @ApiResponse({
    status: 200,
    description: 'This collection has been update succesfully',
    type: Boolean,
  })
  @Put(':id')
  @ApiParam({
    name: 'id',
    required: true,
    description: 'Collection identification code',
  })
  @ApiBody({
    description: 'New collection',
    required: true,
    isArray: false,
    type: CollectionDto,
  })
  @UseGuards(AuthGuard())
  async update(@Param('id') id: string, @Body() collectionDto: CollectionDto) {
    const response = await this.collectionService.updateCollection(id, collectionDto,
    );
    if (response) {
    }
    return response
  }

  @ApiOperation({ summary: 'Delete collection by id' })
  @ApiResponse({
    status: 200,
    description: 'This collection has been delete succesfully',
    type: Boolean,
  })
  @Delete(':id')
  @ApiParam({
    name: 'id',
    required: true,
    description: 'Collection identification code',
  })
  @UseGuards(AuthGuard())
  async delete(@Param('id') id: string) {
    return await this.collectionService.deleteCollection(id);
  }

  @ApiOperation({ summary: 'Collection by id' })
  @ApiResponse({
    status: 200,
    description: 'Collection has been find',
    type: Collection,
  })
  @Get(':id')
  @ApiParam({
    name: 'id',
    required: true,
    description: 'Collection identification code',
  })
  @UseGuards(AuthGuard())
  async get(@Param('id') id: string) {
    return await this.collectionService.getCollectionById(id);
  }

  @ApiOperation({ summary: 'Collection by slug' })
  @ApiResponse({
    status: 200,
    description: 'Collection has been find',
    type: Collection,
  })
  @Get('slug/:slug')
  @ApiParam({
    name: 'slug',
    required: true,
    description: 'Collection identification code',
  })
  @UseGuards(AuthGuard())
  async getBySlug(@Param('slug') slug: string) {
    return await this.collectionService.getCollectionBySlug(slug);
  }

  @ApiOperation({ summary: 'List collections id by user' })
  @ApiResponse({
    status: 200,
    description: 'Collections id list found',
    type: String,
    isArray: true,
  })
  @Get('idsbyuser/:id')
  @ApiParam({
    name: 'id',
    required: true,
    description: 'User identification code',
  })
  @UseGuards(AuthGuard())
  async idsByUser(@Request() req,@Param('id') id: string) {
    let collections = null
  
    if(matchRolesListBoolean(req.user, ['editor_archivo']) ){
      collections = await this.collectionService.getAllCollectionIDs();
    }else{
      collections = await this.collectionService.getCollectionIdsByUser(id);
    }
    return collections
  }

  @ApiOperation({ summary: 'Summary collection by user' })
  @ApiResponse({
    status: 200,
    description: 'Summary of collection has been find',
    type: Collection,
    isArray: true,
  })
  @ApiParam({
    name: 'id',
    required: true,
    description: 'User identification code',
  })
  @Get('resume/byuser/:id')
  @UseGuards(AuthGuard())
  async resumeByuser(@Request() req,@Param('id') id) {
    let collections = null
    console.log("req.user ",req.user)
    if(matchRolesListBoolean(req.user, ['editor_archivo']) ){
      collections = await this.collectionService.getResumeCollectionAll();    
    }else{
      collections = await this.collectionService.getResumeCollectionByUser(id);
    }  
    return collections;
  }

  @ApiOperation({ summary: 'Collection by user' })
  @ApiResponse({
    status: 200,
    description: 'Collection has been find',
    type: Collection,
    isArray: true,
  })
  @ApiParam({
    name: 'id',
    required: true,
    description: 'User identification code',
  })
  @Get('byuser/:id')
  @UseGuards(AuthGuard())
  async byuser(@Param('id') id) {
    return await this.collectionService.getCollectionbyUser(id);
  }

  @ApiOperation({ summary: 'Find collection by title' })
  @ApiResponse({
    status: 201,
    description: 'Collection has been find',
    type: Collection,
    isArray: true,
  })
  @ApiBody({
    description: 'Keyword of title',
    required: true,
    isArray: false,
    type: FindKeywordDto,
  })
  @Post('find')
  @UseGuards(AuthGuard())
  async find(@Body() body: FindKeywordDto) {
    return await this.collectionService.findCollection(body);
  }

  @ApiOperation({ summary: 'Find keywords in collection' })
  @ApiResponse({
    status: 200,
    description: 'The keywords has been find',
    type: Object,
    isArray: false,
  })
  @Get('find/keywords')
  @UseGuards(AuthGuard())
  async keywords() {
    return await this.collectionService.findKeywords();
  }

  @ApiOperation({ summary: 'Find collection by keywords' })
  @ApiResponse({
    status: 201,
    description: 'Collection has been find',
    type: Collection,
    isArray: true,
  })
  @ApiBody({
    description: 'Keyword',
    required: true,
    isArray: false,
    type: FindKeywordsDto,
  })
  @Post('find/collectionsByKeywords')
  @UseGuards(AuthGuard())
  async collectionsByKeywords(@Body() body: any) {
    return await this.collectionService.findCollectionsBykeyword(body);
  }

  @ApiOperation({ summary: 'Find collection by category' })
  @ApiResponse({
    status: 201,
    description: 'Collection has been find',
    type: Collection,
    isArray: false,
  })
  @ApiBody({
    description: 'Category',
    required: true,
    isArray: true,
    type: FindCategoryDto,
  })
  @Post('find/collectionsByCategory')
  @UseGuards(AuthGuard())
  async collectionsByCategory(@Body() body: FindCategoryDto) {
    return await this.collectionService.findCollectionsByCategory(body);
  }

  @ApiOperation({ summary: 'Aggregations by field' })
  @ApiResponse({
    status: 201,
    description: 'Aggregation has been create',
    type: Object,
    isArray: false,
  })
  @ApiBody({
    description: 'Metadata pair key-value ',
    required: true,
    isArray: false,
    type: AggregationName,
  })
  @Post('aggregation')
  @UseGuards(AuthGuard())
  async aggregation(@Body() body: AggregationName) {
    return await this.collectionService.aggregation(body);
  }

  //@UseGuards(AuthGuard())
  @ApiOperation({ summary: 'Find collection by metadata' })
  @ApiResponse({
    status: 201,
    description: 'Collection has been find',
    type: Object,
    isArray: false,
  })
  @ApiBody({
    description: 'Metadata pair key-value ',
    required: true,
    isArray: false,
    type: FinKeyValueMetadata,
  })
  @Post('find/collectionsByMetadata')
  async collectionsByMetadata(@Body() body: FinKeyValueMetadata) {
    return await this.collectionService.findCollectionsByMetadata(body);
  }

  @Post('find/searchCollections')
  async searchIndexedCollections(@Body() body) {
    return this.collectionService.searchIndexedCollections(body);
  }
}
