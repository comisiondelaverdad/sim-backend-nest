import {  IsString, IsObject} from "class-validator";
import { LocationDto } from "./location.dto";
import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';

export class PlaceCreationDto { 

    @ApiProperty()
    @IsObject()
    readonly country: LocationDto;

    @ApiPropertyOptional()
    @IsObject()
    readonly departament: LocationDto;

    @ApiPropertyOptional()
    @IsObject()
    readonly municipality: LocationDto;

  }