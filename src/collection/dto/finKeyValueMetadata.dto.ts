import { IsString} from "class-validator";
import { ApiProperty } from '@nestjs/swagger';

export class  FinKeyValueMetadata { 

    @ApiProperty()
    @IsString()
    readonly key: string;
    
    @ApiProperty()
    @IsString()
    readonly values: string;

  }