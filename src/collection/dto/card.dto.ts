import {  IsArray, IsString} from "class-validator";
import { PieceDto } from "./piece.dto";
import { ApiProperty } from '@nestjs/swagger';
export class CardDto { 
    @ApiProperty()
    @IsString()
    readonly order: string;
    
    @ApiProperty()
    @IsString()
    readonly id: string;

    @ApiProperty()
    @IsArray()
    readonly pieces: Array<PieceDto>;

    @ApiProperty()
    @IsArray()
    readonly poputContent: object;

    @ApiProperty()
    @IsArray()
    readonly sensibleContent: object;
  }