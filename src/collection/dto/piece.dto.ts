import { IsMongoId, IsString , IsObject, IsArray, IsDate} from "class-validator";
import { Types } from 'mongoose';
import { ApiProperty , ApiPropertyOptional} from '@nestjs/swagger';

export class PieceDto {
  @ApiPropertyOptional()
  @IsString()
  readonly  _id: string;

  @ApiPropertyOptional()
  @IsObject()
  readonly resource: object;

  @ApiPropertyOptional()
  @IsString()
  readonly type: string;

  @ApiPropertyOptional()
  @IsString()
  readonly value: string;

  @ApiPropertyOptional()
  @IsString()
  readonly order: number;
   
  @ApiPropertyOptional()
  @IsArray()
  readonly records: Array<object>;

  @ApiPropertyOptional()
  @IsString()
  readonly path: string;
  
  @ApiPropertyOptional()
  @IsString()
  readonly alt: string;
  
  @ApiPropertyOptional()
  @IsString()
  readonly text: string;
  
  @ApiPropertyOptional()
  @IsString()
  readonly min: string;

  @ApiPropertyOptional()
  @IsString()
  readonly max: string;

  }


