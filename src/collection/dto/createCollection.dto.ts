import { IsBoolean, IsNumber, IsString} from "class-validator";
import { ApiProperty } from '@nestjs/swagger';

export class  CreateCollectionDto { 
    @ApiProperty()
    @IsBoolean()
    readonly created: boolean;
    
    @ApiProperty()
    @IsString()
    readonly id: string;

    @ApiProperty()
    @IsString()
    readonly slug: string;

  }