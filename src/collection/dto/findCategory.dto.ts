import { IsString} from "class-validator";
import { ApiProperty } from '@nestjs/swagger';

export class  FindCategoryDto { 
    @ApiProperty()
    @IsString()
    readonly category: string;
  }