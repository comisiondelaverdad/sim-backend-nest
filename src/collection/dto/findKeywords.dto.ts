import { IsString} from "class-validator";
import { ApiProperty } from '@nestjs/swagger';

export class  FindKeywordsDto { 

    @ApiProperty()
    @IsString()
    readonly keywords: string;
    
    @ApiProperty()
    @IsString()
    readonly keywords_resources: string;

  }