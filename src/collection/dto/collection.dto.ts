import { IsMongoId, IsString ,  IsArray, IsObject, IsBoolean} from "class-validator";
import { Types } from 'mongoose';
import { LocationDto } from "./location.dto";
import { DateRangeDto } from "./dateRange.dto";
import { CardDto } from "./card.dto";
import { PieceDto } from "./piece.dto";
import { PlaceCreationDto } from "./place.dto";
import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';


export class CollectionDto {
  @ApiProperty({type:"string"})
  @IsMongoId()
  readonly user: Types.ObjectId;


  @ApiProperty()
  @IsString()
  readonly author: string;

  @ApiProperty()
  @IsString()
  readonly type_author: string;

  @ApiPropertyOptional()
  @IsString()
  readonly area_author: string;

  @ApiProperty()
  @IsString()
  readonly title: string;

  @ApiProperty()
  @IsString()
  readonly slug: string;

  @ApiProperty()
  @IsString()
  readonly description: string;

  @ApiProperty()
  @IsObject()
  readonly place_creation: PlaceCreationDto;

  @ApiProperty()
  @IsObject()
  readonly cover_page: PieceDto;

  @ApiProperty()
  @IsString()
  readonly category: string;  

  @ApiPropertyOptional()
  @IsBoolean()
  readonly publish: boolean;
    
  @ApiProperty()
  @IsBoolean()
  readonly deleted: boolean;

  @ApiProperty()
  @IsString()
  readonly type: string;
  
  @ApiPropertyOptional()
  @IsString()
  readonly mandate: string;
  
  @ApiPropertyOptional()
  @IsString()
  readonly topic: string;

  @ApiProperty()
  @IsArray()
  readonly keywords: Array<string>;

  @ApiProperty()
  @IsArray()
  readonly keywords_resources: Array<string>;

  @ApiProperty()
  @IsObject()
  readonly temporalCoverage:  DateRangeDto;

  @ApiProperty()
  @IsArray()
  readonly geographicCoverage:  Array<LocationDto>; 
  
  @ApiProperty()
  @IsArray()
  readonly cards:  Array<CardDto>;

  @ApiProperty()
  @IsString()
  readonly objective: string;

  @ApiProperty()
  @IsObject()
  readonly ilustration: object;
  

  }