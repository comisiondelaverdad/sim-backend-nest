import { IsString} from "class-validator";
import { ApiProperty } from '@nestjs/swagger';

export class  AggregationName { 

    @ApiProperty()
    @IsString()
    readonly name: string;


  }