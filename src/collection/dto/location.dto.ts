import {  IsString, IsObject, IsBoolean} from "class-validator";
import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';

export class LocationDto { 
    
    @ApiPropertyOptional()
    @IsObject()
    readonly geoPoint: object;

    @ApiPropertyOptional()
    @IsString()
    readonly ident: string;

    @ApiPropertyOptional()
    @IsString()
    readonly value: string;

    @ApiPropertyOptional()
    @IsString()
    readonly name: string;
    
    @ApiPropertyOptional()
    @IsString()
    readonly code: string;

    @ApiPropertyOptional()
    @IsString()
    readonly code2: string;

    @ApiPropertyOptional()
    @IsString()
    readonly type: string;

    @ApiPropertyOptional()
    @IsString()
    readonly descripcion: string;
      
    @ApiPropertyOptional()
    @IsBoolean()
    readonly resource: boolean;

  }