import { IsString} from "class-validator";
import { ApiProperty } from '@nestjs/swagger';

export class  FindKeywordDto { 

    @ApiProperty()
    @IsString()
    readonly keyWord: string;


  }