import { IsDate} from "class-validator";
import { ApiProperty } from '@nestjs/swagger';
export class DateRangeDto { 
    @ApiProperty()
    @IsDate()
    readonly start: Date;
    
    @ApiProperty()
    @IsDate()
    readonly end: Date;

  }