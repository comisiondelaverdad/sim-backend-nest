import { Test, TestingModule } from '@nestjs/testing';
import { AccessRequestHistoryController } from './access-request-history.controller';

describe('AccessRequestHistory Controller', () => {
  let controller: AccessRequestHistoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AccessRequestHistoryController],
    }).compile();

    controller = module.get<AccessRequestHistoryController>(AccessRequestHistoryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
