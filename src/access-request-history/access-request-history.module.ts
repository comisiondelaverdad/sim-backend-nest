import { Module, forwardRef } from '@nestjs/common';
import { getConnectionToken, MongooseModule, } from '@nestjs/mongoose';
import { AccessRequestHistoryController } from './access-request-history.controller';
import { AccessRequestHistory, AccessRequestHistorySchema } from './schema/access-request-history.schema';
import { AccessRequestHistoryService } from './access-request-history.service';
import { UserModule } from '../user/user.module'
import { Connection } from 'mongoose';
import * as AutoIncrementFactory from 'mongoose-sequence'
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'AccessRequestHistory',
        useFactory: async (connection: Connection) => {
          const schema = AccessRequestHistorySchema;
          const AutoIncrement = AutoIncrementFactory(connection);
          // eslint-disable-next-line @typescript-eslint/camelcase
          schema.plugin(AutoIncrement, { id: 'ident_access-request-history', inc_field: 'ident' });
          return schema;
        },
        inject: [getConnectionToken("database")]
      }
    ], "database"),
    forwardRef(() => UserModule),
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [AccessRequestHistoryController],
  providers: [AccessRequestHistoryService],
  exports: [AccessRequestHistoryService, MongooseModule]
})
export class AccessRequestHistoryModule { }