import { Controller, Get, Post, Param, Body, UseGuards } from '@nestjs/common';
import { AccessRequestHistoryService } from './access-request-history.service';
import { AccessRequestHistory } from './schema/access-request-history.schema';
import { AccessRequestHistoryDto } from './schema/access-request-history.dto'
import { AuthGuard } from '@nestjs/passport';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('access-requests-history')
@Controller('api/access-requests-history')
export class AccessRequestHistoryController {

  constructor(private readonly accessRequestHistoryService: AccessRequestHistoryService) {}

  // List AccessRequestHistory's
  @Get()
  @UseGuards(AuthGuard())
  list() : Promise<AccessRequestHistory[]>{
    return this.accessRequestHistoryService.list()
  }

  // Create AccessRequestHistory
  @Post()
  @UseGuards(AuthGuard())
  create(@Body() arh: AccessRequestHistoryDto) : Promise<AccessRequestHistory>{
    return this.accessRequestHistoryService.create(arh)
  }

}
