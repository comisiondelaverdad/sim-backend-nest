import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as SchemaM } from 'mongoose';


@Schema({collection:"access-requests-history"})
export class AccessRequestHistory extends Document {
  
  @Prop()
  ident: Number;

  @Prop()
  user: SchemaM.ObjectId; //{type: 'ObjectId', ref: 'User'}
  
  @Prop()
  date: string;

  @Prop()
  status: string;

  @Prop()
  description: string;
  
  @Prop()
  __v: Number;
   


}

export const AccessRequestHistorySchema = SchemaFactory.createForClass(AccessRequestHistory);