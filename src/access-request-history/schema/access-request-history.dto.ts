import { IsInt, IsString, IsObject } from 'class-validator';

export class AccessRequestHistoryDto {
  @IsObject()
  readonly user: string;

  @IsString()
  readonly date: number;

  @IsString()
  readonly description: string;
}
