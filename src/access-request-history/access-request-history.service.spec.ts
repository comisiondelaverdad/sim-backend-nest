import { Test, TestingModule } from '@nestjs/testing';
import { AccessRequestHistoryService } from './access-request-history.service';

describe('AccessRequestHistoryService', () => {
  let service: AccessRequestHistoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AccessRequestHistoryService],
    }).compile();

    service = module.get<AccessRequestHistoryService>(AccessRequestHistoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
