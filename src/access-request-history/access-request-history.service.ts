import { Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AccessRequestHistory } from './schema/access-request-history.schema';
import { User } from '../user/schema/user.schema';

@Injectable()
export class AccessRequestHistoryService {

  constructor(
  	@InjectModel('AccessRequestHistory') private accessRequestHistory: Model<AccessRequestHistory>,
  	@InjectModel('User') private userModel: Model<User>	
  	) {}

  async list(): Promise<AccessRequestHistory[]> {
    return this.accessRequestHistory.find()
    .populate({
       path: 'user',
       model: this.userModel}).exec()
  }

  async create(post): Promise<AccessRequestHistory> {
    return this.accessRequestHistory.create(post)
  }

}
