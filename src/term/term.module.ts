import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TermController } from './term.controller';
import { TermService } from './term.service';
import { TermSchema } from '../term/schema/term.schema';
import { PassportModule } from '@nestjs/passport';
import { ResourceModule } from '../resource/resource.module';
import { RecordModule } from '../record/record.module';
import { ConfigModule } from '../config/config.module';
import { UtilService } from './../util/util.service'
import { SemanticModule } from 'src/semantic/semantic.module';
import { SemanticService } from 'src/semantic/semantic.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Term', schema: TermSchema }], "database"),
    ConfigModule,
    ResourceModule,
    RecordModule, 
    SemanticModule,   
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [TermController],
  providers: [TermService,UtilService, SemanticService],
  exports: [TermService, MongooseModule]
})
export class TermModule { }