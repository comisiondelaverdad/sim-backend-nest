import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Term } from '../term/schema/term.schema';
import { Resource } from '../resource/schema/resource.schema';
import { Record } from '../record/schema/record.schema';
import { Semantic } from '../semantic/schema/semantic.schema';

import { CreateTermDto } from './dto/term-semantic.dto';
import fs = require("fs");
import filePath = require('path');
import tmp = require('temporary');
import dirTree = require("directory-tree");
import AdmZip = require("adm-zip");
import { json } from 'express';
import { Console } from 'console';
import { SemanticService } from 'src/semantic/semantic.service';
//import { async } from 'rxjs';
const exec = require('child_process').exec;

@Injectable()
export class TermService {
  constructor(
    @InjectModel('Term') private TermModel: Model<Term>,
    @InjectModel('Resource') private ResourceModel: Model<Resource>,
    @InjectModel('Record') private RecordModel: Model<Record>,
    @InjectModel('Semantic') private SemanticModel: Model<Semantic>,
    private SemanticService: SemanticService,
  ) {}

  async findAll(active, from, size): Promise<Term[]> {
    return this.TermModel.find({ active: active })
      .sort()
      .skip(Number((from - 1) * size))
      .limit(Number(size))
      .exec();
  }

  async relations(): Promise<Term[]> {
    return this.TermModel.find({ active: true }).sort({ name: 1 }).exec();
  }

  async findTable(query): Promise<Term[]> {
    let skip = parseInt(query['skip']);
    let limit = parseInt(query['limit']);
    delete query['skip'];
    delete query['limit'];
    //return this.TermModel.find(query).skip(skip).limit(limit).exec();
    return this.TermModel.find(query)
      .sort({ name: 1 })
      .collation({ locale: 'en_US', strength: 1 })
      .skip(skip)
      .limit(limit)
      .exec();
  }

  async find(keyword, active, from, size): Promise<Term[]> {
    let search = new RegExp('.*' + keyword + '.*', 'i');
    return this.TermModel.find({ name: search, active: active })
      .sort()
      .skip(Number((from - 1) * size))
      .limit(Number(size))
      .exec();
  }

  async create(term: CreateTermDto): Promise<Term> {
    const create = new this.TermModel(term);
    //si existe un campo semantico, entonces en ese mismo campo semantico guardar el nuevo termino que vamos a crear
    if (term.thematic_field) {
      //obtener id de semantic
      const id_semantic = term.thematic_field;
      //obtener el id creado del termino
      const result_create = await create.save();
      const id_result = result_create._id;
      //actualizar el semantic con un push a terms con el id obtenido
      const find_semantic = await this.SemanticService.findByidNoPopulate(
        id_semantic,
      );
      find_semantic.terms.push(id_result);
      await this.SemanticService.update(id_semantic, find_semantic);
      return result_create;
    }

    return create.save();
  }

  async update(id, term: CreateTermDto): Promise<Term> {
    if (term.thematic_field) {
      //obtener id de semantic nuevo
      const id_semantic_new = term.thematic_field;
      //obtemer el id del semantic viejo thematic_field
      const old_term = await this.findByid(id);
      if (old_term.thematic_field) {
        const old_semantic_id_on_term = old_term.thematic_field;
        //si es diferente entonces al antiguo semantic borrarle el id del termino
        let find_old_semantic = await this.SemanticService.findByidNoPopulate(
          old_semantic_id_on_term,
        );
        const new_terms_on_old_semantic = find_old_semantic.terms.filter(
          (item) => item != id,
        );
        find_old_semantic.terms = new_terms_on_old_semantic;
        await this.SemanticService.update(
          old_semantic_id_on_term,
          find_old_semantic,
        );
      }

      // agregar el id del termino al nuevo semantic
      const find_new_semantic = await this.SemanticService.findByidNoPopulate(
        id_semantic_new,
      );
      find_new_semantic.terms.push(id);
      await this.SemanticService.update(id_semantic_new, find_new_semantic);
    }
    return this.TermModel.findOneAndUpdate({ _id: id }, term).exec();
  }

  async findByid(id): Promise<Term> {
    return this.TermModel.findOne({ _id: id })
      .populate({ path: 'terms', model: this.TermModel })
      .exec();
  }

  async diccionarioTermino(id): Promise<Term> {
    return this.TermModel.findOne({ _id: id })
      .populate({ path: 'terms', model: this.TermModel })
      .exec();
  }

  async diccionarioAbcFind(like): Promise<Term> {
    if (like == 'a' || like == 'A') {
      like = 'áa';
    }
    if (like == 'e' || like == 'E') {
      like = 'ée';
    }
    if (like == 'i' || like == 'I') {
      like = 'íi';
    }
    if (like == 'o' || like == 'O') {
      like = 'óo';
    }
    if (like == 'u' || like == 'U') {
      like = 'úu';
    }

    return this.TermModel.findOne({
      name: new RegExp('^[' + like + ']', 'i'),
      active: true,
    })
      .sort({ name: -1 })
      .exec();
  }

  async diccionarioAbc(like): Promise<Term> {
    if (like == 'a' || like == 'A') {
      like = 'áa';
    }
    if (like == 'e' || like == 'E') {
      like = 'ée';
    }
    if (like == 'i' || like == 'I') {
      like = 'íi';
    }
    if (like == 'o' || like == 'O') {
      like = 'óo';
    }
    if (like == 'u' || like == 'U') {
      like = 'úu';
    }

    return this.TermModel.find({
      name: new RegExp('^[' + like + ']', 'i'),
      active: true,
    })
      .sort({ name: -1 })
      .exec();
  }

  async diccionarioLike(like): Promise<Term> {
    console.log(like);
    return this.TermModel.find({
      name: { $regex: new RegExp('^' + like, 'i') },
      active: true,
    })
      .sort({ name: -1 })
      .exec();
  }

  async findByidResource(id): Promise<Resource> {
    return this.ResourceModel.findOne({ _id: id }).exec();
  }

  async createRecord(newRec): Promise<Record> {
    const record = await this.RecordModel.create(newRec);
    return await record.save();
  }

  async updateRecord(id, subtitle): Promise<Record> {
    let record = await this.RecordModel.findOne({ _id: id }).exec();
    record.subtitle = subtitle;
    record.save();
    return record;
  }

  async updateRecordAudio(id, filename): Promise<Record> {
    let record = await this.RecordModel.findOne({ _id: id }).exec();
    record.filename = filename;
    record.save();
    return record;
  }

  async updateResourceDiccionario(id, record): Promise<Resource> {
    let resource = await this.ResourceModel.findOne({ _id: id }).exec();
    console.log(typeof resource.records);

    var find = resource.records.find(function (element) {
      return element === JSON.stringify(record).replace(/['"]+/g, '');
    });

    if (typeof find === 'undefined') {
      resource.records.push(JSON.stringify(record).replace(/['"]+/g, ''));
    }
    resource.save();
    return resource;
  }

  async findByidRecord(id): Promise<Record> {
    return this.RecordModel.findOne({ _id: id }).exec();
  }
}
