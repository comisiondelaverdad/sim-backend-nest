import { Body, Controller, Get, Param, Post, Put, Request, UploadedFiles, Query, Res, UseGuards, UseInterceptors, Response } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FilesInterceptor } from '@nestjs/platform-express';
import { TermService } from "./term.service"
import { CreateTermDto } from './dto/term-semantic.dto';
import { matchRoles } from 'src/auth/roles.guard';
import { Term } from './schema/term.schema';
import { Record } from '../record/schema/record.schema';
import { ConfigService } from 'src/config/config.service';
import { diskStorage } from 'multer';
import { UtilService } from 'src/util/util.service';
import { Console } from 'console';

export let RESOURCE_DICTIONARY = ""

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('term')
@Controller('api/term')
export class TermController {
  constructor(
    private utilService: UtilService,
    private readonly termService: TermService,
    private configService: ConfigService) {
    RESOURCE_DICTIONARY = this.configService.get('RESOURCE_DICTIONARY')
  }

  @Post('createbuscador')
  @UseGuards(AuthGuard('jwt'))
  async createbuscador(@Request() req, @Body() createTermDto: CreateTermDto) {
    //Solo ingresa el admin_diccionario
    matchRoles(req.user, 'admin_diccionario');

    //Valido si existe el archivo de la imagen
    if (typeof createTermDto.img !== 'undefined') {
      //Cargo el archivo del campo semantico
      var fs = require("fs");
      var partes_base_64 = createTermDto.img.split(";base64,");
      var cabecera = partes_base_64[0].replace("data:", "").replace("name=", "").split(";");
      var tipo = cabecera[0];
      var nombre = cabecera[1];
      var base64string = partes_base_64[1];
      createTermDto.imgpath = "/rep/diccionario/terminos/" + nombre;
      fs.writeFile("/rep/diccionario/terminos/" + nombre, base64string, { encoding: 'base64' }, function (err) {
        if (err) {
          console.log(err);
        }
      });

    }

    //Cargo el archivo del campo semantico
    /*
    var fs = require("fs");
    var partes_base_64 = createTermDto.audio.split(";base64,");
    var cabecera = partes_base_64[0].replace("data:", "").replace("name=", "").split(";");      
    var tipo = cabecera[0];
    var nombre = cabecera[1];
    var base64string = partes_base_64[1];       
    createTermDto.audiopath="/rep/diccionario/terminos/"+nombre;
    fs.writeFile("/rep/diccionario/terminos/"+nombre,base64string,{encoding:'base64'},function(err){
      if(err){
        console.log(err);
      }
    });
    */

    return await this.termService.create(createTermDto);
  }

  @Put('updatebuscador/:id')
  @UseGuards(AuthGuard('jwt'))
  async updatebuscador(@Request() req, @Param("id") id, @Body() createTermDto: CreateTermDto) {
    //Solo ingresa el admin_diccionario
    matchRoles(req.user, 'admin_diccionario');

    //Valido si existe el archivo de la imagen
    if (typeof createTermDto.img !== 'undefined') {
      //Cargo el archivo del campo semantico
      var fs = require("fs");
      var partes_base_64 = createTermDto.img.split(";base64,");
      var cabecera = partes_base_64[0].replace("data:", "").replace("name=", "").split(";");
      var tipo = cabecera[0];
      var nombre = cabecera[1];
      var base64string = partes_base_64[1];
      createTermDto.imgpath = "/rep/diccionario/terminos/" + nombre;
      fs.writeFile("/rep/diccionario/terminos/" + nombre, base64string, { encoding: 'base64' }, function (err) {
        if (err) {
          console.log(err);
        }
      });
    }

    //Cargo el archivo del campo semantico
    /*
    var fs = require("fs");
    var partes_base_64 = createTermDto.audio.split(";base64,");
    var cabecera = partes_base_64[0].replace("data:", "").replace("name=", "").split(";");      
    var tipo = cabecera[0];
    var nombre = cabecera[1];
    var base64string = partes_base_64[1];       
    createTermDto.audiopath="/rep/diccionario/terminos/"+nombre;
    fs.writeFile("/rep/diccionario/terminos/"+nombre,base64string,{encoding:'base64'},function(err){
      if(err){
        console.log(err);
      }
    });  
    */

    await this.termService.update(id, createTermDto);
  }

  @Put('updateRecord/:id')
  @UseGuards(AuthGuard('jwt'))
  async updateRecord(@Request() req, @Param("id") id, @Body() subtitle) {
    //Solo ingresa el admin_diccionario
    matchRoles(req.user, 'admin_diccionario');    
    await this.termService.updateRecord(id, subtitle.subtitle);
  }  



  @Post('create')
  async create(@Body() createTermDto: CreateTermDto) {
    return await this.termService.create(createTermDto);
  }

  @Put('update/:id')
  update(@Param("id") id, @Body() createTermDto: CreateTermDto) {
    return this.termService.update(id, createTermDto)
  }

  @Get('findAll/:active/:from/:size')
  async getFindAll(@Request() req, @Param('active') active: Boolean, @Param('from') from, @Param('size') size) {
    let returnData = await this.termService.findAll(active, from, size);
    return returnData;
  }

  @Get('findAll/:active/:from/:size/:keyword')
  async getFind(@Request() req, @Param('keyword') keyword, @Param('active') active: Boolean, @Param('from') from, @Param('size') size) {
    let returnData = await this.termService.find(keyword, active, from, size);
    return returnData;
  }

  @Get('findTable')
  @UseGuards(AuthGuard('jwt'))
  async findTable(@Request() req, @Query() query): Promise<Term[]> {
    matchRoles(req.user, 'admin_diccionario') //Validación de roles del usuario
    return this.termService.findTable(query);
  }

  @Get('relations')
  async getRelations(@Request() req, @Param('active') active: Boolean, @Param('from') from, @Param('size') size) {

    return await this.termService.relations();

  }

  @Get('findOne/:id')
  @UseGuards(AuthGuard('jwt'))
  async findOne(@Request() req, @Param('id') id): Promise<Term> {
    //Solo ingresa el admin_diccionario
    matchRoles(req.user, 'admin_diccionario');
    return this.termService.findByid(id);
  }

  @Get('diccionarioTermino/:id')
  @UseGuards(AuthGuard('jwt'))
  async diccionarioTermino(@Request() req, @Param('id') id): Promise<Term> {
    //Solo ingresa el admin_diccionario
    matchRoles(req.user, 'invitado');
    return this.termService.diccionarioTermino(id);
  }

  @Get('diccionarioAbcFind/:id')
  @UseGuards(AuthGuard('jwt'))
  async diccionarioAbcFind(@Request() req, @Param('id') like): Promise<Term> {
    //Solo ingresa el admin_diccionario
    matchRoles(req.user, 'invitado');
    return this.termService.diccionarioAbcFind(like);
  }

  @Get('diccionarioAbc/:id')
  @UseGuards(AuthGuard('jwt'))
  async diccionarioAbc(@Request() req, @Param('id') like): Promise<Term> {
    //Solo ingresa el admin_diccionario
    matchRoles(req.user, 'invitado');
    return this.termService.diccionarioAbc(like);
  }
  
  @Get('diccionarioLike/:id')
  @UseGuards(AuthGuard('jwt'))
  async diccionarioLike(@Request() req, @Param('id') like): Promise<Term> {
    //Solo ingresa el admin_diccionario
    matchRoles(req.user, 'invitado');
    return this.termService.diccionarioLike(like);
  }

  @Get('findByidRecord/:id')
  @UseGuards(AuthGuard('jwt'))
  async findByidRecord(@Request() req, @Param('id') id): Promise<Record> {
    //Solo ingresa el admin_diccionario
    matchRoles(req.user, 'admin_diccionario');
    return this.termService.findByidRecord(id);
  }


  @Post('cargaraudio/:id')
  @UseInterceptors(
    FilesInterceptor('audio', 20, {
      storage: diskStorage({
        destination: '/rep/diccionario/terminos',
        filename: (req, file, cb) => {
          return cb(null, file.originalname)
        }
      })
    }),
  )
  async cargaraudio(@Param("id") id, @Body() createTermDto: CreateTermDto, @UploadedFiles() audio): Promise<Term> {
    //Cargo el archivo del campo semantico
    createTermDto.audiopath = "/rep/diccionario/terminos/" + audio[0].originalname;

    //Consultamos el resource para asociar los audios de loa record del diccionario
    let resource_dic = await this.termService.findByidResource(RESOURCE_DICTIONARY);

    //Consultamos el termino
    let term = await this.termService.findByid(id);

    //Valido si existe el record
    let editar_term = true;
    
    if (typeof term.record === 'undefined') {
        editar_term = false;
        //Configuro el record para asociar el video
        const newRec = {
          ident: this.utilService.uuidv4(),
          identifier: resource_dic.ident,
          identifier_II: resource_dic.identifier,
          filename: "/rep/diccionario/terminos/" + audio[0].originalname,
          resourceId: resource_dic.ident,
          origin: resource_dic.origin,
          type: resource_dic.type,
          subtitle:'',
          extra: {
            originalName: audio[0].originalname,
            mimetype: audio[0].mimetype,
            status: 'created',
            createdBy: resource_dic.type,
          },
          metadata: {
            firstLevel: {
              pages: null,
              resolution: null,
              duration: null,
              fileSize: '',
              title: audio[0].originalname,
              fileFormat: audio[0].mimetype,
              recordDate: new Date(),
              creationDate: '',
              accessLevel: '',
              encoding: audio[0].originalname,
              originalName: audio[0].originalname,
              description: resource_dic.origin,
            }
          },
          content: '',
          textExtraction: false
        }
        const record = await this.termService.createRecord(newRec);
        createTermDto.record = record['_id'];      
        resource_dic.records.push(record['_id']);
        await this.termService.updateResourceDiccionario(resource_dic['_id'],record['_id']);
    }

    //Ajusto el record con el nuevo audio
    if (editar_term === true) {
      const record = await this.termService.updateRecordAudio(term.record, "/rep/diccionario/terminos/" + audio[0].originalname);
      await this.termService.updateResourceDiccionario(resource_dic['_id'],record['_id']);
    }

    await this.termService.update(id, createTermDto)

    return await this.termService.findByid(id);
  }

  @Post('audio/')
  @UseGuards(AuthGuard('jwt'))
  async audio(@Request() req, @Body() audio, @Response() res) {
    matchRoles(req.user, 'admin_diccionario');
    //Donde esta ubicado
    await res.download((await this.termService.findByid(audio.audio)).audiopath);
  }

  @Post('audioRecord/')
  @UseGuards(AuthGuard('jwt'))
  async audioRecord(@Request() req, @Body() id, @Response() res) {
    matchRoles(req.user, 'admin_diccionario');
    //Donde esta ubicado
    await res.download((await this.termService.findByidRecord(id.record)).filename);
  }

}
