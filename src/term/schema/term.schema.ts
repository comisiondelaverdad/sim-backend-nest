import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({collection: 'term'})
export class Term extends Document {

  @Prop({type:"string"})
  ident: string;

  @Prop({type:"string"})
  name: string;

  @Prop({type:"string"})
  thematic_field: string;  

  @Prop({type:"string"})
  equivalent: string;

  @Prop({type:"string"})
  definition: string;
  
  @Prop({type:"string"})
  source_definition: string;

  @Prop({type:"string"})
  context: string;

  @Prop({type:"string"})
  source_context: string;

  @Prop({type:"string"})
  interview: string;

  @Prop({type:"string"})
  interview_date: string;

  @Prop({type:"string"})
  interview_dep: string;

  @Prop({type:"string"})
  interview_mun: string;

  @Prop({type:"object"})
  interview_location: object;

  @Prop({type:"number"})
  year_facts: number;

  @Prop({type:"string"})
  etnia: string;

  @Prop({type:"string"})
  sexo: string;

  @Prop({type:"string"})
  sexual_orientation: string;

  @Prop({type:"string"})
  others: string;

  @Prop({type:"number"})
  age: number;

  @Prop({type:"string"})
  fragment: string;

  @Prop({type:"string"})
  audio: string;

  @Prop({type:"string"})
  audiopath: string;

  @Prop({type:"string"})
  img: string;  

  @Prop({type:"string"})
  imgpath: string;
  
  @Prop({type:"array"})
  terms: string[];

  @Prop({type:"boolean"})
  active:boolean;

  @Prop({type:"string"})
  segundoinicio: string;

  @Prop({type:"string"})
  segundofinal: string;
  
  @Prop({type:"string"})
  record: string;
}


export const TermSchema = SchemaFactory.createForClass(Term);