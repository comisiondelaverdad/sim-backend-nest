export class CreateTermDto {
  _id?: String;
  readonly name: String;
  readonly thematic_field: string;
  readonly equivalent: string;  
  readonly definition: string;  
  readonly source_definition: string;  
  readonly context: string;  
  readonly source_context: string;  
  readonly interview: string;  
  readonly interview_date: string;  
  readonly interview_dep: string;  
  readonly interview_mun: string;  
  readonly interview_location: object;  

  readonly year_facts: string;  
  readonly etnia: string;  
  readonly sexo: string;  
  readonly sexual_orientation: string;  
  readonly others: string;  
  readonly age: string;  
  readonly fragment: string;  
  readonly audio: string;  
  audiopath: string;  
  readonly img: string;    
  imgpath: string;    
  readonly terms: Array<String>;  
  readonly active: Boolean;  
  record: string;    
}

