import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SemanticController } from './semantic.controller';
import { SemanticService } from './semantic.service';
import { SemanticSchema } from '../semantic/schema/semantic.schema';
import { TermSchema } from '../term/schema/term.schema';
import { PassportModule } from '@nestjs/passport';
import { MulterModule } from '@nestjs/platform-express';
import { ConfigModule } from 'src/config/config.module';
import { ConfigService } from 'src/config/config.service';
import { ResourceModule } from '../resource/resource.module';
import { RecordModule } from '../record/record.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Semantic', schema: SemanticSchema }], "database"),
    MongooseModule.forFeature([{ name: 'Term', schema: TermSchema }], "database"),
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        dest: configService.get('DESTINATION_SEMACTIC_FILES'),
      }),
      inject: [ConfigService],
    }),
    ResourceModule,
    RecordModule,
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [SemanticController],
  providers: [SemanticService, ConfigService],
  exports: [SemanticService, MongooseModule]
})
export class SemanticModule { }