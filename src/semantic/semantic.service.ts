import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Semantic } from '../semantic/schema/semantic.schema';
import { Resource } from '../resource/schema/resource.schema';
import { Record } from '../record/schema/record.schema';
import { Term } from '../term/schema/term.schema';
import fs = require("fs");
import filePath = require('path');
import tmp = require('temporary');
import dirTree = require("directory-tree");
import AdmZip = require("adm-zip");
import { CreateSemanticDto } from './dto/create-semantic.dto';
import { json } from 'express';
//import { async } from 'rxjs';
const exec = require('child_process').exec;

@Injectable()
export class SemanticService {
  constructor(
    @InjectModel('Semantic') private SemanticModel: Model<Semantic>,
    @InjectModel('Resource') private ResourceModel: Model<Resource>,
    @InjectModel('Record') private RecordModel: Model<Record>,
    @InjectModel('Term') private TermModel: Model<Term>,
  ) {}

  async findTable(query): Promise<Semantic[]> {
    let skip = parseInt(query['skip']);
    let limit = parseInt(query['limit']);
    delete query['skip'];
    delete query['limit'];
    return await this.SemanticModel.find(query)
      .sort({ name: 1 })
      .collation({ locale: 'en_US', strength: 1 })
      .populate({ path: 'terms', model: this.TermModel })
      .skip(skip)
      .limit(limit)
      .exec();
  }

  async findAll(active, from, size): Promise<Semantic[]> {
    return await this.SemanticModel.find({ active: active })
      .populate({ path: 'terms', model: this.TermModel })
      .sort()
      .skip(Number((from - 1) * size))
      .limit(Number(size))
      .exec();
  }

  async find(keyword, active, from, size): Promise<Semantic[]> {
    let search = new RegExp('.*' + keyword + '.*', 'i');
    return await this.SemanticModel.find({ name: search, active: active })
      .populate({ path: 'terms', model: this.TermModel })
      .sort()
      .skip(Number((from - 1) * size))
      .limit(Number(size))
      .exec();
  }

  async create(createSemanticDto: CreateSemanticDto): Promise<Semantic> {
    const create = new this.SemanticModel(createSemanticDto);
    return await create.save();
  }

  async findByid(id): Promise<Semantic> {
    return await this.SemanticModel.findOne({ _id: id })
      .populate({ path: 'terms', model: this.TermModel })
      .exec();
  }

  async findByidNoPopulate(id): Promise<Semantic> {
    return await this.SemanticModel.findOne({ _id: id }).exec();
  }

  async diccionarioAbcFind(like): Promise<Term> {
    if (like == 'a' || like == 'A') {
      like = 'áa';
    }
    if (like == 'e' || like == 'E') {
      like = 'ée';
    }
    if (like == 'i' || like == 'I') {
      like = 'íi';
    }
    if (like == 'o' || like == 'O') {
      like = 'óo';
    }
    if (like == 'u' || like == 'U') {
      like = 'úu';
    }
    return this.SemanticModel.findOne({
      name: new RegExp('^[' + like + ']', 'i'),
      active: true,
    })
      .sort({ name: -1 })
      .populate({ path: 'terms', model: this.TermModel })
      .exec();
  }

  async diccionarioAbc(like): Promise<Term> {
    if (like == 'a' || like == 'A') {
      like = 'áa';
    }
    if (like == 'e' || like == 'E') {
      like = 'ée';
    }
    if (like == 'i' || like == 'I') {
      like = 'íi';
    }
    if (like == 'o' || like == 'O') {
      like = 'óo';
    }
    if (like == 'u' || like == 'U') {
      like = 'úu';
    }
    return this.SemanticModel.find({
      name: new RegExp('^[' + like + ']', 'i'),
      active: true,
    })
      .sort({ name: -1 })
      .exec();
  }

  async diccionarioLike(like): Promise<Term> {
    console.log(like);
    return this.SemanticModel.find({
      name: { $regex: new RegExp('^' + like, 'i') },
      active: true,
    })
      .sort({ name: -1 })
      .exec();
  }

  async update_thematic_fiel_to_term(id_term: any, id_semantic: any) {
    await this.TermModel.findOneAndUpdate(
      { _id: id_term },
      { thematic_field: id_semantic },
    ).exec();
  }

  async detele_thematic_fiel_to_term(id: any) {
    await this.TermModel.findOneAndUpdate(
      { _id: id },
      { $unset: { thematic_field: 1 } },
    ).exec();

    // const term = await this.TermModel

    // term.save();
  }
  async delete_multiple_terms(terms: any) {
    terms.forEach(async (element) => {
      await this.detele_thematic_fiel_to_term(element);
    });
  }
  async update_multiple_terms(terms: any, id: any) {
    terms.forEach(async (element) => {
      await this.update_thematic_fiel_to_term(element, id);
    });
  }

  async update(id, semactic: CreateSemanticDto): Promise<Semantic> {
    //sacar todos los anteriores términos y a cada uno quitarle la relación de semantico en thematic_field
    const old_semantic = await this.findByidNoPopulate(id);
    if (old_semantic.terms.length) {
      await this.delete_multiple_terms(old_semantic.terms);
    }

    if (semactic.terms.length) {
      await this.update_multiple_terms(semactic.terms, id);
    }

    return await this.SemanticModel.findOneAndUpdate(
      { _id: id },
      semactic,
    ).exec();
  }

  async selectsemantic(): Promise<Semantic[]> {
    return await this.SemanticModel.find({ active: true }, { _id: 1, name: 1 })
      .sort({ name: 1 })
      .exec();
  }

  async relations(like): Promise<Semantic[]> {
    return await this.SemanticModel.find(
      { name: new RegExp(like, 'i'), active: true },
      { _id: 1, name: 1 },
    )
      .sort({ name: 1 })
      .exec();
  }

  async resources(like): Promise<Resource[]> {
    return await this.ResourceModel.find(
      {
        ident: new RegExp(like, 'i'),
        origin: 'ModuloCaptura',
        identifier: '001-EV',
        'metadata.firstLevel.accessLevel': '4',
      },
      { _id: 1, ident: 1 },
    ).exec();
  }

  async records(identifier): Promise<Record[]> {
    return await this.RecordModel.find(
      {
        identifier: identifier,
        type: 'Audio de la entrevista',
        'metadata.firstLevel.accessLevel': 4,
      },
      { _id: 1, filename: 1, extra: 1 },
    ).exec();
  }

  async findByResorce(id): Promise<Resource> {
    return await this.ResourceModel.findOne(
      { _id: id },
      { _id: 1, ident: 1 },
    ).exec();
  }
}
