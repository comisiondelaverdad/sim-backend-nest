import { Body, Controller, Get, Param, Post, Put, Request, UploadedFiles, Query, Res, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { SemanticService } from "./semantic.service"
import { CreateSemanticDto } from './dto/create-semantic.dto';
import { AnyFilesInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { ConfigService } from 'src/config/config.service';
import { Semantic } from './schema/semantic.schema';
import { matchRoles } from 'src/auth/roles.guard';
import { Record } from 'src/record/schema/record.schema';
import { Resource } from 'src/resource/schema/resource.schema';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('semantic')
@Controller('api/semantic')
export class SemanticController {
    constructor(
      private readonly semanticService: SemanticService,
      private configService: ConfigService
    ){}

    @Post('create')
    @UseInterceptors(FilesInterceptor('image',20,{
      storage: diskStorage({
        useFactory: async (configService: ConfigService) => ({
          destination: configService.get('DESTINATION_SEMACTIC_FILES'),
        }),        
        filename: (req, file, cb) => {
          return cb(null,file.originalname)
        }        
      })
    }))  
    async create(@Body() createSemanticDto:CreateSemanticDto,@UploadedFiles() image) {              
      createSemanticDto["image"]=image[0].originalname;
      return await this.semanticService.create(createSemanticDto);
    }    

    @Post('createbuscador')
    @UseGuards(AuthGuard('jwt'))
    async createbuscador(@Request() req, @Body() createSemanticDto: CreateSemanticDto) {
      //Solo ingresa el admin_diccionario
      matchRoles(req.user, 'admin_diccionario');
      
      //Valido si existe el archivo de la imagen
      if (typeof createSemanticDto.img !== 'undefined')
      {
        //Cargo el archivo del campo semantico
        const fs = require("fs");
        const partes_base_64 = createSemanticDto.img.split(";base64,");
        const cabecera = partes_base_64[0].replace("data:", "").replace("name=", "").split(";");      
        const nombre = cabecera[1];
        const base64string = partes_base_64[1];       
        createSemanticDto.imgpath=this.configService.get('DIC_CAMPOS')+nombre;
        fs.writeFile(this.configService.get('DIC_CAMPOS')+nombre,base64string,{encoding:'base64'},function(err){
          if(err){
            console.log(err);
          }
        });
      }
      
      
      return await this.semanticService.create(createSemanticDto);
    }
    
    @Get('findOne/:id')
    @UseGuards(AuthGuard('jwt'))
    async findOne(@Request() req, @Param('id') id): Promise<Semantic> {
      //Solo ingresa el admin_diccionario
      matchRoles(req.user, 'admin_diccionario');
      return this.semanticService.findByid(id);
    }

    @Get('diccionarioAbcFind/:id')
    @UseGuards(AuthGuard('jwt'))
    async diccionarioAbcFind(@Request() req, @Param('id') like): Promise<Semantic> {
      //Solo ingresa el admin_diccionario
      matchRoles(req.user, 'invitado');
      return this.semanticService.diccionarioAbcFind(like);
    }

    @Get('diccionarioAbc/:id')
    @UseGuards(AuthGuard('jwt'))
    async diccionarioAbc(@Request() req, @Param('id') like): Promise<Semantic> {
      //Solo ingresa el admin_diccionario
      matchRoles(req.user, 'invitado');
      return this.semanticService.diccionarioAbc(like);
    }

    @Get('diccionarioLike/:id')
    @UseGuards(AuthGuard('jwt'))
    async diccionarioLike(@Request() req, @Param('id') like): Promise<Semantic> {
      //Solo ingresa el admin_diccionario
      matchRoles(req.user, 'invitado');
      return this.semanticService.diccionarioLike(like);
    }

    @Put('update/:id')
    update(@Param("id") id, @Body() createSemanticDto:CreateSemanticDto){
        return this.semanticService.update(id, createSemanticDto)
    }

    @Put('updatebuscador/:id')
    @UseGuards(AuthGuard('jwt'))
    async updatebuscador(@Request() req, @Param("id") id, @Body() createSemanticDto: CreateSemanticDto) {
      //Solo ingresa el admin_diccionario
      matchRoles(req.user, 'admin_diccionario');

      //Valido si existe el archivo de la imagen
      if (typeof createSemanticDto.img !== 'undefined')
      {
        //Cargo el archivo del campo semantico
        const fs = require("fs");
        const partes_base_64 = createSemanticDto.img.split(";base64,");
        const cabecera = partes_base_64[0].replace("data:", "").replace("name=", "").split(";");      
        const nombre = cabecera[1];
        const base64string = partes_base_64[1];       
        createSemanticDto.imgpath=this.configService.get('DIC_CAMPOS')+nombre;
        fs.writeFile(this.configService.get('DIC_CAMPOS')+nombre,base64string,{encoding:'base64'},function(err){
          if(err){
            console.log(err);
          }
        });
      }
      
      await this.semanticService.update(id, createSemanticDto);
    }

    @Get('findTable')
    @UseGuards(AuthGuard('jwt'))
    async findTable(@Request() req, @Query() query): Promise<Semantic[]> {
      matchRoles(req.user, 'admin_diccionario') //Validación de roles del usuario
      return this.semanticService.findTable(query);
    }

    @Get('findAll/:active/:from/:size')
    async getFindAll(@Request() req, @Param('active') active:Boolean, @Param('from') from, @Param('size') size){
            let returnData = await this.semanticService.findAll(active,from,size);
            return returnData;        
    }
    
    @Get('findAll/:active/:from/:size/:keyword')
    async getFind(@Request() req, @Param('keyword') keyword, @Param('active') active:Boolean, @Param('from') from, @Param('size') size){
            let returnData = await this.semanticService.find(keyword,active,from,size);
            return  returnData;                
    }

    @Get('selectsemantic')
    async getSelectsemantic(@Request() req){
        return await this.semanticService.selectsemantic();
        
    }

    @Get('relations/:like')
    async getRelations(@Request() req, @Param('like') like){

        return await this.semanticService.relations(like);
        
    }

    @Get('resources/:like')
    async resources(@Request() req, @Param('like') like){

      return await this.semanticService.resources(like);

    }

    @Get('records/:id')
    async records(@Request() req, @Param('id') id): Promise<Record[]> {
      return await this.semanticService.records(id);
    }

    @Get('findByResorce/:id')
    @UseGuards(AuthGuard('jwt'))
    async findByResorce(@Request() req, @Param('id') id): Promise<Resource> {
      //Solo ingresa el admin_diccionario
      matchRoles(req.user, 'admin_diccionario');
      return await this.semanticService.findByResorce(id);
    }
    
}
