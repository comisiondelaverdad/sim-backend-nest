import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document , Types} from 'mongoose';

@Schema({collection: 'semantic'})
export class Semantic extends Document {

  @Prop({type:"string"})
  ident: string;

  @Prop({type:"string"})
  name: string;

  @Prop({type:"string"})
  definition: string;  

  @Prop({type:"array"})
  terms: string[];

  @Prop({type:"boolean"})
  active:boolean;

  @Prop({type:"string"})
  img: string;  

  @Prop({type:"string"})
  imgpath: string;  

}


export const SemanticSchema = SchemaFactory.createForClass(Semantic);




