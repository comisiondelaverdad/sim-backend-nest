export class CreateSemanticDto {
  _id?: String;
  readonly name: String;
  readonly active: Boolean;
  readonly definition: string;
  readonly img: string;    
  imgpath: string;    
  readonly terms: Array<String>;  
}

