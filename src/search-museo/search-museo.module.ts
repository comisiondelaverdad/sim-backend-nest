import { Module, HttpModule } from '@nestjs/common';
import { SearchMuseoService } from './search-museo.service';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { SearchMuseoController } from './search-museo.controller';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    ConfigModule,
    HttpModule, 
    PassportModule.register({ defaultStrategy: 'jwt' }),
  ],
  providers: [SearchMuseoService,ConfigService],
  exports:[SearchMuseoService],
  controllers: [SearchMuseoController]
})
export class SearchMuseoModule {}



