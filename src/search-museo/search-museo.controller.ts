
import { SearchMuseoService } from './search-museo.service';
import { Post, Delete, Get, Param, Controller, Body, Query,Request } from '@nestjs/common';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('search-museo')
@Controller('api/search-museo')
export class SearchMuseoController {

    constructor(private readonly searchMuseoService: SearchMuseoService) { }


    @Get('record/content/document/:ident')
    @UseGuards(AuthGuard())
    async getContentDocumentRecord(@Param('ident') ident: String) {
      return await this.searchMuseoService.getContentDocumentByRecordIdent(ident);
    }

    @Post('sites')
    @UseGuards(AuthGuard())   
    async sitesMap(@Body() body: any) {
      return await this.searchMuseoService.getSitesMap(body);
    }

    @Post('geometry/site')
    @UseGuards(AuthGuard())
    async geometrySite(@Body() body: any) {
      return await this.searchMuseoService.geometrySite(body);
    }


    @Get('countries')
    @UseGuards(AuthGuard())
    async getPais() {
      return await this.searchMuseoService.getCountries();
    }
    @Get('departament/:country')
    @UseGuards(AuthGuard())
    async getDepartament(@Param('country') country: String) {
      return await this.searchMuseoService.getDepartament(country);
    }
    
    @Get('municipality/:departament')
    @UseGuards(AuthGuard())
    async getMunicipality(@Param('departament') departament: String) {
        return await this.searchMuseoService.getMunicipality(departament);
    }

}
