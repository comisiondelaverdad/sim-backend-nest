import { Test, TestingModule } from '@nestjs/testing';
import { SearchMuseoService } from './search-museo.service';

describe('SearchMuseoService', () => {
  let service: SearchMuseoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SearchMuseoService],
    }).compile();

    service = module.get<SearchMuseoService>(SearchMuseoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
