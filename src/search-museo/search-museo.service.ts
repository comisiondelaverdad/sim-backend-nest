import { Injectable, HttpService } from '@nestjs/common';
import * as https from 'https'
import * as fs from 'fs';
import { ConfigService } from '../config/config.service';

@Injectable()
export class SearchMuseoService {

  config: any = undefined;
  httpsAgent = null;

  constructor(private configX: ConfigService, private http: HttpService,) {
    let crtFile = fs.readFileSync(configX.get("ELASTIC_AUTH_CERT"))
    let agent = new https.Agent({ ca: crtFile })
    this.httpsAgent = agent
    this.config = {
      elastic: {
        url: configX.get("ELASTIC_URL"),
        index: {
          data: configX.get("ELASTIC_INDEX_DATA"),
          logger: configX.get("ELASTIC_INDEX_LOGGER"),
          env: configX.get("ELASTIC_INDEX_ENV"),
          museo: configX.get("ELASTIC_INDEX_MUSEO"),
          dpto: configX.get("ELASTIC_DPTO"),
          mpio: configX.get("ELASTIC_MPIO"),
          urb: "cascos-urbanos",
          ver: "veredas"
        },
        headers: {
          auth: {
            username: configX.get("ELASTIC_AUTH_USER"),
            password: configX.get("ELASTIC_AUTH_PASS")
          }
        }
      }
    }
  }

  async getContentDocumentByRecordIdent(ident): Promise<object> {
    let query =
    {
      "_source": ["document.records.content"],
      "query": {
        "bool": {
          "must": {
            "nested": {
              "path": "document.records",
              "query": {
                "bool": {
                  "filter": [
                    {
                      "term": {
                        "document.records.ident.keyword": {
                          "value": ident
                        }
                      }
                    }
                  ]
                }
              }
            }
          }
        }
      }
    }

    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }

    let response = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.museo + "-" + this.config.elastic.index.env + "/_search",

      query,
      h
    )
      .toPromise()
    return { ...response.data.hits }
  }

  async geometrySite(body: any): Promise<object> {
    let query =
    {
      "query": {
        "term": {
          "_id": body._id
        }
      },
      "aggs": {
        "centroid": {
          "geo_centroid": {
            "field": "geometry"
          }
        }
      }
    }
    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let response = await this.http.post(
      this.config.elastic.url + body._index + "/_search",
      query,
      h
    )
      .toPromise()
    return { ...response.data.hits }
  }

  async getSitesMap(body: any): Promise<object> {
    let typeMatch = "phrase"
   
    if(body.keyword.length<4){
      typeMatch = "phrase_prefix"
    }

    let query =
    {
      "size": 500,
      "_source": [
        "properties.NOM_CPOB",
        "properties.NOMBRE_VER",
        "properties.NOMB_MPIO",
        "properties.NOM_DEP"
      ],
      "query": {
        "bool": {
          "should": {
            "multi_match": {
              "query": body.keyword,
              "fields": [
                "name"
              ],
              "type": typeMatch
            }
          }
        }
      },
      "track_total_hits": false,    
      "sort": [
        "_index",
        "_score"
      ]
    }
  
    let indexSearch = this.config.elastic.index.urb+","+this.config.elastic.index.ver
    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    
    let response = await this.http.post(
      this.config.elastic.url +indexSearch + "/_search",
      query,
      h
    )
      .toPromise()

    return { ...response.data.hits }

  }


  async getMunicipality(departament: String): Promise<object> {
    let muni = {}

    let query =
    {
      "size": 5000,
      "_source": ["MPIO_CNMBR", "MPIO_CDPMP"],
      "query": {
        "bool": {
          "filter": [
            {
              "term": {
                "DPTO_CCDGO": departament
              }
            }
          ]
        }
      },
      "sort": [
        "MPIO_CNMBR"
      ]
    }
    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let response = await this.http.post(

      this.config.elastic.url + this.config.elastic.index.mpio + "/_search",
      query,
      h
    ).toPromise()
    muni = { ...response.data.hits }

    return await this.getArrayItemCodeValue(muni, "MPIO_CDPMP", "MPIO_CNMBR", null, null, true)
  }


  async getDepartament(country: String): Promise<object> {
    let depto = []
    if (country.toLowerCase() == "co" || country.toLowerCase() == "col") {
      let query =
      {
        "size": 10000,
        "_source": ["DPTO_CNMBR", "DPTO_CCDGO"],
        "query": { "match_all": {} },
        "sort": [
          "DPTO_CNMBR"
        ]

      }
      let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
      let response = await this.http.post(
        this.config.elastic.url + this.config.elastic.index.dpto + "/_search",
        query,
        h
      )
        .toPromise()
      depto = { ...response.data.hits }
    }

    return await this.getArrayItemCodeValue(depto, "DPTO_CCDGO", "DPTO_CNMBR", null, null, true)
  }


  async getCountries(): Promise<object> {
    let query =
    {
      "size": 10000,
      "_source": ["properties.name_long", "properties.adm0_a3", "properties.postal"],
      "query": { "match_all": {} },
      "sort": [
        {
          "_script": {
            "script": "doc['properties.name_long.keyword'].value == 'Colombia' ? 1 : 0",
            "type": "number",
            "order": "desc"
          }
        },
        { "properties.name_long.keyword": "asc" }
      ]
    }
    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let response = await this.http.post(
      this.config.elastic.url + "mundo" + "/_search",
      query,
      h).toPromise()
    let countries = { ...response.data.hits }

    return await this.getArrayItemCodeValue(countries, "adm0_a3", "name_long", "postal", "properties", false)
  }

  async getArrayItemCodeValue(resultHitsElastic, code, value, code2 = null, propertiesContent = null, changeNameValue = null): Promise<object> {
    let result = resultHitsElastic
    if ("hits" in resultHitsElastic) {
      let arrayCodeValue = []
      const arraysHits = resultHitsElastic.hits;
      arraysHits.forEach(element => {
        let data = element["_source"];
        if (propertiesContent != null)
          data = data[propertiesContent]

        let val = data[value]
        if (changeNameValue)
          val = data[value][0].toUpperCase() + data[value].substring(1).toLowerCase()

        if (code2 !== null)
          arrayCodeValue.push({ "code": data[code], "code2": data[code2], "value": val })
        else
          arrayCodeValue.push({ "code": data[code], "value": val })

      });
      result = arrayCodeValue
    }
    return result
  }
}
