import { Test, TestingModule } from '@nestjs/testing';
import { SearchMuseoController } from './search-museo.controller';

describe('SearchMuseoController', () => {
  let controller: SearchMuseoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SearchMuseoController],
    }).compile();

    controller = module.get<SearchMuseoController>(SearchMuseoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
