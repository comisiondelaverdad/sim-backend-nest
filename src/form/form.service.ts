import { Injectable, HttpService } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import { Request } from 'request'
import * as https from 'https'
import * as fs from 'fs';

@Injectable()
export class FormService {

  config :any = undefined;
  httpsAgent = null;

  constructor(
              private http: HttpService,
              private configX: ConfigService) {
    let crtFile = fs.readFileSync(configX.get("ELASTIC_AUTH_CERT"))
    this.httpsAgent = new https.Agent({ca: crtFile})
    this.config = {
      elastic: {
        url: configX.get("ELASTIC_URL"),
        index: {
          data: configX.get("ELASTIC_INDEX_DATA"),
          logger: configX.get("ELASTIC_INDEX_LOGGER"),
          env: configX.get("ELASTIC_INDEX_ENV")
        },
        headers: {
          auth: {
            username: configX.get("ELASTIC_AUTH_USER"),
            password: configX.get("ELASTIC_AUTH_PASS")
          }
        }
      }
    }

  }

  getFormIdent(): any{
    return {
      title: "Identificador",
      name: "ident",
      type: "form",
      schema: {
        type: "object",
        properties: { ident: {title: "Identificador", type: "string" }},
      },
      query: 'resource.ident.keyword:"[ident]"',
      queryAlias: 'identificador:"[ident]"'
    }
  }

  getFormBasic(): any{
    return {
      title: "Texto",
      name: "search",
      type: "form",
      schema:{ "type": "string", title:"Texto libre"},
      query: "",
    }
  }

  getFormDate(): any{
    return {
      title: "Fecha recurso",
      name: "date",
      type: "form",
      schema: {
        type: "object",
        properties: { date: {title: "Fecha", type: "string", format:"date" }},
      },
      query: "metadata.firstLevel.temporalCoverage.start=[date]",
    }
  }

  getFormLanguage(): any{
    return {
      title: "Lenguaje",
      name: "date",
      type: "form",
      schema: {
        type: "object",
        properties: {
          language: {
            title: "Idioma",
            type: "string",
            enum: ["Español", "Inglés"]
          }
        },
      },
      query: 'resource.language.keyword:"[language]"',
    }
  }

  getFormCreator(): any{
    return {
      title: "Creador",
      name: "creator",
      type: "form-select",
      field: "resource.creator.keyword",
      schema: {
        type: "object",
        properties: {
          creator: {
            title: "Creador",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'resource.creator.keyword:"[creator]"',
    }
  }

  getFormDateInterval(): any{
    return {
      title: "Intevalo de fecha recurso",
      name: "dateInterval",
      type: "form",
      schema:{
        type: "object",
        properties: {
          start: {
            type: "string",
            format:"date",
            title: "Fecha inicial",
          },
          end: {
            type: "string",
            format:"date",
            title: "Fecha final"
          },

        },
      },
      query: "(metadata.firstLevel.temporalCoverage.start>[start] AND metadata.firstLevel.temporalCoverage.start<[end])",
    }
  }

  getAutocomplete(): any{
    return {
      title: "Titulo",
      name: "title",
      type: "autocomplete",
      schema:{
        url: "/api/forms/asyncSearch?var=resource.title&query=",
      },
      query: "resource.title=:[title]",
    }
  }

  getFormType(): any{
    return {
      title: "Tipo de Documento",
      name: "type",
      type: "form-select",
      field: "resource.type.keyword",
      schema: {
        type: "object",
        properties: {
          type: {
            title: "Tipo",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'resource.type.keyword:"[type]"',
      queryAlias: 'tipo_recurso:"[type]"'
    }
  }

  getFormTitle(): any{
    return {
      title: "Titulo",
      name: "title",
      type: "form",
      schema: {
        type: "object",
        properties: { title: {title: "Titulo", type: "string" }},
      },
      query: 'resource.title:"[title]"',
      queryAlias: 'titulo:"[title]"'
    }
  }

  getFormDescription(): any{
    return {
      title: "Descripción",
      name: "description",
      type: "form",
      schema: {
        type: "object",
        properties: { description: {title: "Descripción", type: "string" }},
      },
      query: 'resource.descripcion:"[description]"',
      queryAlias: 'descripcion:"[description]"'
    }
  }

  getFormMap(): any{
    return {
      title: "Georeferenciado",
      name: "map",
      type: "map",
      schema: {
      },
      query: 'map:"[map]"',
    }
  }

  getFormPopulation(): any{
    return {
      title: "Población",
      name: "population",
      type: "form-select",
      field: "document.record.metadata.firstLevel.population.keyword",
      schema: {
        type: "object",
        properties: {
          population: {
            title: "Población",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'document.record.metadata.firstLevel.population.keyword:"[population]"',
      queryAlias: 'poblacion:"[population]"'
    }
  }

  getFormTerritorio(): any{
    return {
      title: "Territorio",
      name: "territorio",
      type: "form-select",
      field: "resource.territorio.descripcion.keyword",
      schema: {
        type: "object",
        properties: {
          territorio: {
            title: "Territorio",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'resource.territorio.descripcion.keyword:"[territorio]"',
    }
  }

  getFormTopics(): any{
    return {
      title: "Temas",
      name: "topics",
      type: "form-select",
      field: "document.resource.metadata.firstLevel.topics.keyword",
      schema: {
        type: "object",
        properties: {
          topics: {
            title: "Temas",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'document.resource.metadata.firstLevel.topics.keyword:"[topics]"',
      queryAlias: 'temas:"[topics]"',
    }
  }

  getFromLabelled(): any{
    return {
      title: "Etiquetado",
      name: "labelled",
      type: "form-select",
      field: "record.labelled.annotation.label.keyword",
      schema: {
        type: "object",
        properties: {
          labelled: {
            title: "Etiquetado",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'record.labelled.annotation.label.keyword:"[labelled]"',
    }
  }

  getFormViolenceType(){
    return {
      title: "Tipo de violencia",
      name: "violenceType",
      type: "form-select",
      field: "resource.violenceType.keyword",
      schema: {
        type: "object",
        properties: {
          violenceType: {
            title: "Tipo de violencia",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'resource.violenceType.keyword:"[violenceType]"',
      queryAlias: 'tipo_de_violencia:"[violenceType]"'
    }
  }

  getFormApproaches(){
    return {
      title: "Enfoques",
      name: "approaches",
      type: "form-select",
      field: "resource.approaches.keyword",
      schema: {
        type: "object",
        properties: {
          approaches: {
            title: "Enfoques",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'resource.approaches.keyword:"[approaches]"',
    }
  }

  getFormMacroprocess(){
    return {
      title: "Macroproceso",
      name: "macroprocess",
      type: "form-select",
      field: "resource.macroprocess.keyword",

      schema: {
        type: "object",
        properties: {
          macroprocess: {
            title: "Macroprocesos",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'resource.macroprocess.keyword:"[macroprocess]"',      
    }
  }

  getFormMandate(){
    return {
      title: "Mandato",
      name: "mandate",
      type: "form-select",
      field: "resource.mandate.keyword",
      schema: {
        type: "object",
        properties: {
          mandate: {
            title: "Mandato",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'resource.mandate.keyword:"[mandate]"'
    }
  }

  getFormObjetive(){
    return {
      title: "Objetivos",
      name: "objective",
      type: "form-select",
      field: "resource.objective.keyword",

      schema: {
        type: "object",
        properties: {
          objective: {
            title: "Objetivos",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'resource.objective.keyword:"[objective]"'
    }
  }

  getFormDocumentaryGroup(){
    return {
      title: "Agrupación documental",
      name: "documentaryGroup",
      type: "form-select",
      field: "resource.documentaryGroup.keyword",
      schema: {
        type: "object",
        properties: {
          documentaryGroup: {
            title: "Agrupación documental",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'resource.documentaryGroup.keyword:"[documentaryGroup]"'
    }
  }

  getFormConflictActors(){
    return {
      title: "Actores del conflicto",
      name: "conflictActors",
      type: "form-select",
      field: "resource.conflictActors.keyword",
      schema: {
        type: "object",
        properties: {
          conflictActors: {
            title: "Actores del conflicto",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'resource.conflictActors.keyword:"[conflictActors]"',
      queryAlias: 'actores_del_conflicto:"[conflictActors]"'
    }
  }

  getFormIncidentLocation(){
    return {
      title: "Ubicación del incidente",
      name: "incidentLocation",
      type: "form-select",
      field: "resource.incidentLocation.keyword",
      schema: {
        type: "object",
        properties: {
          incidentLocation: {
            title: "Ubicación del incidente",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'resource.incidentLocation.keyword:"[incidentLocation]"',
    }
  }

  getFormMacroterritorio(){
    return {
      title: "Macroterritorio",
      name: "macroterritorio",
      type: "form-select",
      field: "resource.macroterritorio.descripcion.keyword",
      schema: {
        type: "object",
        properties: {
          macroterritorio: {
            title: "Macroterritorio",
            type: "string",
            enum: [
            ]
          }
        },
      },
      query: 'resource.macroterritorio.descripcion.keyword:"[macroterritorio]"',
      queryAlias: 'macroterritorio:"[macroterritorio]"'
    }
  }

  getFormTemporalCoverage(){
    return {
      title: "Búsqueda por Cobertura Geográfica",
      name: "dateInterval",
      type: "form",
      schema:{
        type: "object",
        properties: {
          start: {
            type: "string",
            format:"date",
            title: "Fecha inicial",
          },
          end: {
            type: "string",
            format:"date",
            title: "Fecha final"
          },

        },
      },
      query: '(resource.temporalCoverage.start: [[start] TO [end]])',
      queryAlias: '(temporal.inicio: [[start] TO [end]])'
    }
  }

  getListForms(): any{
    return [
      //this.getFormBasic(),
      //this.getFormLanguage(),
      //this.getFormDate(),
      //this.getFormDateInterval(),
      //this.getAutocomplete(),
      
      //resource.ident texto abierto
      this.getFormIdent(),
      
      //resource.creator selección
      //this.getFormCreator(),
      
      //resource.type  selección
      this.getFormType(),
      
      //resource.title  texto abierto
      this.getFormTitle(),
      
      //resource.description texto abierto
      this.getFormDescription(),
      
      //this.getFormMap(),

      this.getFormPopulation(),
      
      //this.getFormTerritorio(),

      this.getFormTopics(),
      
      //this.getFromLabelled(),

      this.getFormViolenceType(),
      
      this.getFormApproaches(),

      this.getFormConflictActors(),
      
      //this.getFormIncidentLocation(),

      this.getFormMacroterritorio(),
      this.getFormMacroprocess(),
      this.getFormMandate(),
      this.getFormObjetive(),
      this.getFormDocumentaryGroup(),

    ]
  }

  getAsyncSearch(query): any{
    return {
      items: [
        { login: "pepe" }
      ]
    }

  }

  async getOptions(field: string): Promise<void | any>{
    let json =
    {
      "aggs": {
        "options": {
          "terms": {
            "field": field,
            "size": 100,
          }
        }
      },
      "size": 0,
      "stored_fields": ["*"],
      "script_fields": {},
      "query": {
        "bool": {
          "must": [],
          "filter": [],
          "should": [],
          "must_not": []
        }
      }
    }

    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let response = await this.http.post(
      this.config.elastic.url+this.config.elastic.index.data+"-"+this.config.elastic.index.env+"/_search",
      json,
      h
    ).toPromise().then()

    return response.data
  }
}
