import { Controller, Get, Query, Param, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FormService } from './form.service';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('forms')
@Controller('api/forms')
export class FormController {

  constructor(private readonly formService: FormService) {}

  @Get('advanced-search')
  @UseGuards(AuthGuard())
  advancedSearch()
  {
      return {
        forms: this.formService.getListForms()
      }
  }

  @Get('options/:field')
 
  options(@Param('field') field:string)
  {
      return this.formService.getOptions(field)
  }

  @Get('asyncSearch')
  @UseGuards(AuthGuard())
  asyncSearch(@Query() query){
    console.log("Query: ", query)
    return this.formService.getAsyncSearch(query)
  }


}
