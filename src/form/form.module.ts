import { Module, HttpModule } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { ConfigModule } from '../config/config.module';
import { FormController } from './form.controller';
import { FormService } from './form.service';

@Module({
  imports: [
    HttpModule,
    ConfigModule,
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [FormController],
  providers: [FormService]
})
export class FormModule { }