export class EditLocationDto {
  _id?: String;
  readonly properties: object;
  readonly centroid: object;
  readonly geometry: object;
  readonly admin_level: number;
  readonly pais: string;
  readonly localidad: string;
}

