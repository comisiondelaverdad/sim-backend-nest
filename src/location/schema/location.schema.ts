import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Location extends Document {
  @Prop({type:"object"})
  properties: object;

  @Prop({type:"object"})
  centroid: object;

  @Prop({type:"object"})
  geometry: object;

  @Prop({type:"number"})
  admin_level: number;

  @Prop({type:"string"})
  pais: string;
  @Prop({type:"string"})
  localidad: string;

}

export const LocationSchema = SchemaFactory.createForClass(Location);

