import { Module, HttpModule } from '@nestjs/common';
import { StatsController } from './stats.controller';
import { StatsService } from './stats.service';
import { ConfigModule } from '../config/config.module';
import { SearchModule } from '../search/search.module';
import { SearchService } from '../search/search.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SearchHistorySchema } from '../search/schema/search-history.schema';
import { PassportModule } from '@nestjs/passport';
import { LocationModule } from '../admin/location/location.module';
import { LocationService } from '../admin/location/location.service';
import { Location, LocationSchema } from '../location/schema/location.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'SearchHistory', schema: SearchHistorySchema }], "database"),
    MongooseModule.forFeature([{ name: 'Location', schema: LocationSchema, collection: 'geo-locations' }], "database"),
    LocationModule,
    HttpModule, 
    SearchModule, 
    ConfigModule,
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [StatsController],
  providers: [StatsService, SearchService, LocationService]
})
export class StatsModule {}
