import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { SearchService } from '../search/search.service';
import { AuthGuard } from '@nestjs/passport';


import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('stats')
@Controller('api/stats')
export class StatsController {

  constructor(private readonly searchService: SearchService) { }

  @Post('resource/views')
  @UseGuards(AuthGuard())
  buscar(@Body() body): Promise<any> {
    return this.searchService.statsByViews(body.ids).then((x) => {
      return x.aggregations
    }).catch((err) => {
      console.log(err)
      return []
    })
  }

}
