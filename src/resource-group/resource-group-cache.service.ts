import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { ResourceGroup } from './schema/resource-group.schema';
import { BehaviorSubject, interval } from 'rxjs';
import { ResourceGroupService } from './resource-group.service';

@Injectable()
export class ResourceGroupCacheService {

	private resourcesTreeSubject = new BehaviorSubject([]);
	private resourcesTree$ = this.resourcesTreeSubject.asObservable();
	private treeCache: ResourceGroup[];

	private firstLevelTreeSubject = new BehaviorSubject([]);
	private firstLevelTree$ = this.firstLevelTreeSubject.asObservable();
	private firstLevelTree: ResourceGroup[];

	constructor(private readonly resourceGroupService: ResourceGroupService) {
	}
	init() {
		this.resourceGroupService.firstLevel('').then((response) => {
			this.resourcesTreeSubject.next(response);
		});

		this.resourceGroupService.lazy('all', '0').then((response) => {
			this.firstLevelTreeSubject.next(response);
		});

		const timer = interval(7200000); // cache update 2 hour
		timer.subscribe(async (x) => {

			const treeCache = await this.resourceGroupService.firstLevel('');
			this.resourcesTreeSubject.next(treeCache);

			const firstLevelTree = await this.resourceGroupService.lazy('all', '0');
			this.firstLevelTreeSubject.next(firstLevelTree);
		})

		this.resourcesTree$.subscribe((response: ResourceGroup[]) => {
			this.treeCache = response;
		})

		this.firstLevelTree$.subscribe((response: ResourceGroup[]) => {
			this.firstLevelTree = response;
		})
	}

	firstLevelCache(): any {
		let FirstLevelPromise = new Promise((resolve) => {
			if (this.treeCache) {
				resolve(this.treeCache)
			} else {
				this.resourcesTree$.subscribe((response: any) => {
					if (response.length > 0) {
						resolve(response);
					}
				})
			}
		})
		return FirstLevelPromise;
	}

	firstLevelTreeCache(): any {
		let firstLevelTreePromise = new Promise((resolve) => {
			if (this.firstLevelTree) {
				resolve(this.firstLevelTree)
			} else {
				this.firstLevelTree$.subscribe((response: any) => {
					if (response.length > 0) {
						resolve(response);
					}
				})
			}
		})
		return firstLevelTreePromise;
	}

	async updateCache(): Promise<void> {
		this.treeCache = null;
		this.firstLevelTree = null;
		this.init()
	}
}
