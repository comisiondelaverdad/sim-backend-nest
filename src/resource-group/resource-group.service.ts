import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { ResourceGroup } from './schema/resource-group.schema';
import { UtilService } from 'src/util/util.service';
import { Resource } from 'src/resource/schema/resource.schema';
import * as _ from "lodash"
//import { chdir } from 'process';

@Injectable()
export class ResourceGroupService {
	constructor(
		private readonly utilService: UtilService,
		@InjectModel('ResourceGroup') private resourceGroupModel: Model<ResourceGroup>,
		@InjectModel('Resource') private resourceModel: Model<Resource>,
	) { }

	statmentMeta = [
		{ origin: '_id', destiny: '"_id"', defaultValue: null, type: "string" },
		{ origin: 'ident', destiny: '"ident"', defaultValue: null, type: "string" },
		{ origin: 'documentalId', destiny: '"metadata"."firstLevel"."documentalId"', defaultValue: null, type: "string" },
		{ origin: 'title', destiny: '"metadata"."firstLevel"."title"', defaultValue: null, type: "string" },
		{ origin: 'otherTitles', destiny: '"metadata"."firstLevel"."otherTitles"', defaultValue: null, type: "array" },

		{ origin: 'descriptionLevel', destiny: '"metadata"."firstLevel"."descriptionLevel"', defaultValue: null, type: "string" },
		{ origin: 'accessLevel', destiny: '"metadata"."firstLevel"."accessLevel"', defaultValue: null, type: "string" },
		{ origin: 'description', destiny: '"metadata"."firstLevel"."description"', defaultValue: null, type: "string" },
		{ origin: 'topics', destiny: '"metadata"."firstLevel"."topics"', defaultValue: null, type: "array" },
		{ origin: 'conflictActors', destiny: '"metadata"."missionLevel"."humanRights"."conflictActors"', defaultValue: null, type: "array" },
		{ origin: 'population', destiny: '"metadata"."firstLevel"."population"', defaultValue: null, type: "array" },
		{ origin: 'temporalCoverage', destiny: '"metadata"."firstLevel"."temporalCoverage"', defaultValue: { start: null, end: null }, type: "array" },
		{ origin: 'mandate', destiny: '"metadata"."firstLevel"."mandate"', defaultValue: null, type: "array" },

		{ origin: 'authors', destiny: '"metadata"."firstLevel"."authors"', defaultValue: [], type: "array" },
		{ origin: 'collaborators', destiny: '"metadata"."firstLevel"."collaborators"', defaultValue: [], type: "array" },
		{ origin: 'area', destiny: '"metadata"."firstLevel"."area"', defaultValue: [], type: "array" },
		{ origin: 'custodian', destiny: '"metadata"."firstLevel"."custodian"', defaultValue: [], type: "array" },
		{ origin: 'geographicCoverage', destiny: '"metadata"."firstLevel"."geographicCoverage"', defaultValue: [{ name: 'COLOMBIA', code: 'CO', geoPoint: { lat: 0, lon: 0 } }], type: "array" },
		{ origin: 'documentType', destiny: '"metadata"."firstLevel"."documentType"', defaultValue: null, type: "string" },
		{ origin: 'creationDate', destiny: '"metadata"."firstLevel"."creationDate"', defaultValue: { start: null, end: null }, type: "string" },
		{ origin: 'language', destiny: '"metadata"."firstLevel"."language"', defaultValue: null, type: "string" },
		{ origin: 'notes', destiny: '"metadata"."firstLevel"."notes"', defaultValue: null, type: "string" },
	];



	async firstLevel(params): Promise<ResourceGroup[]> {
		const filter = {
			"ResourceGroupParentId": /^0-/,
			// "extra.status": {
			// 	$ne: "deleted"
			// },
		}
		// const filter = {}
		return this.resourceGroupModel
			.find(filter, `ResourceGroupParentId ResourceGroupId metadata.firstLevel.title origin`)
			.lean()
			.hint({ "_id": 1 })
			.exec()
			.then((resourceGroups) => {
				let tree = [];
				let hashTable = Object.create(null);
				let rawData = resourceGroups.map((x) => {
					let newValue = "0"
					if (x.ResourceGroupParentId !== null) {
						newValue = x.ResourceGroupParentId.split("-")[0];
						if (newValue === "0") {
							x.ResourceGroupParentId = 0;
						}
					}
					else {
						x.ResourceGroupParentId = 0;
					}
					let rawItem = {
						"id": x.ResourceGroupId,
						"parent": x.ResourceGroupParentId,
						"text": x.metadata.firstLevel.title,
						"origin": x.origin,
					}
					return rawItem;
				});

				rawData.forEach(node => hashTable[node["id"]] = { ...node, children: [], resourceGroup: [], resourceGroupAll: [] });
				rawData.forEach(node => {
					if (node["parent"] !== 0 && hashTable[node["parent"]] !== undefined) {
						//hashTable[node["parent"]].icon = "fa fa-folder";
						hashTable[node["parent"]].children.push(hashTable[node["id"]]);
						hashTable[node["parent"]].resourceGroup.push(node["id"]);
						hashTable[node["id"]].resourceGroup.push(node["id"]);
					}
					else {
						hashTable[node["id"]].resourceGroup.push(node["id"]);
						tree.push(hashTable[node["id"]]);
					}
				});

				function compare(a, b) {
					const idA = a.text ? a.text.toUpperCase() : '';
					const idB = b.text ? b.text.toUpperCase() : '';
					let comparison = 0;
					if (idA > idB) {
						comparison = 1;
					}
					else if (idA < idB) {
						comparison = -1;
					}
					return comparison;
				}

				return tree.sort(compare);

			});
	}
	async twoLevels(): Promise<ResourceGroup[]> {
		let rgFathers = await this.resourceGroupModel
			.find({ "ResourceGroupParentId": /^0-/ }, `ResourceGroupParentId ResourceGroupId metadata.firstLevel.title origin`)
			.lean()
			.hint({ "_id": 1 })
			.exec()
		let countChild = await Promise.all(rgFathers.map(async (x) => {
			return this.resourceGroupModel.countDocuments({ "ResourceGroupParentId": x.ResourceGroupId });
		}));

		let childrens: any = await Promise.all(rgFathers.map(async (x) => {
			return this.resourceGroupModel.find({ "ResourceGroupParentId": x.ResourceGroupId })
				.lean()
				.hint({ "_id": 1 })
				.exec();
		}));

		countChild.map((rg, index) => {
			rgFathers[index] = {
				...rgFathers[index], ...{
					"id": rgFathers[index].ResourceGroupId,
					"parent": "#",
					"text": rgFathers[index].metadata.firstLevel.title,
					"metadata": rgFathers[index].ResourceGroupId,
					//"icon": rg > 0 ? "fa fa-folder" : "flaticon2-file-2",
					"children": rg == 0 ? [] : (childrens[index]).map((child) => {
						return {
							...child,
							"id": child.ResourceGroupId,
							"parent": child.ResourceGroupParentId,
							"text": child.metadata.firstLevel.title,
							"metadata": child.ResourceGroupId,
							//"icon": "flaticon2-file-2",
							"children": [],
							"origin": child.origin,
						}
					}),
				}
			}
		})
		function compare(a, b) {
			const idA = a.id.toUpperCase();
			const idB = b.id.toUpperCase();
			let comparison = 0;
			if (idA > idB) {
				comparison = 1;
			}
			else if (idA < idB) {
				comparison = -1;
			}
			return comparison;
		}
		rgFathers = rgFathers.sort(compare)
		return rgFathers;
	}
	async getChildrens(filterData, ishead, path, role = null): Promise<ResourceGroup[]> {
		let filter_pleno = false;
		if (role) {
			const intersection = _.intersection(role, ['comisionado', 'catalogador_gestor'])
			if (intersection.length === 0) {
				filter_pleno = true
			}
		}

		let rgFathers = await this.resourceGroupModel
			.find({
				...filterData, ...{
					"extra.status": {
						$ne: "deleted"
					},
				},
				...filter_pleno ? {
					"ResourceGroupId": {
						$ne: "12-OI"
					},
				} : {}

			}, `ResourceGroupParentId ResourceGroupId metadata.firstLevel.title origin`)
			.lean()
			.hint({ "_id": 1 })
			.exec()
		let countChild = await Promise.all(rgFathers.map(async (x) => {
			return this.resourceGroupModel.countDocuments({ "ResourceGroupParentId": x.ResourceGroupId });
		}));
		countChild.map((rg, index) => {
			rgFathers[index] = {
				...rgFathers[index], ...{
					"id": rgFathers[index].ResourceGroupId,
					"parent": ishead ? "#" : rgFathers[index].ResourceGroupParentId,
					"text": rgFathers[index].metadata.firstLevel.title,
					"metadata": rgFathers[index].ResourceGroupId,
					//"icon": rg > 0 ? "fa fa-folder" : "flaticon2-file-2",
					"children": rg > 0 ? true : [],
					"origin": rgFathers[index].origin,
				}
			}
			if ((rgFathers[index].ResourceGroupId === path[path.length - 1])) {
				rgFathers[index] = { ...rgFathers[index], ...{ "state": { "selected": true } } };
			}
		})
		function compare(a, b) {
			const idA = a.text ? a.text.toUpperCase() : '';
			const idB = b.text ? b.text.toUpperCase() : '';
			let comparison = 0;
			if (idA > idB) {
				comparison = 1;
			}
			else if (idA < idB) {
				comparison = -1;
			}
			return comparison;
		}
		rgFathers = rgFathers.sort(compare)
		return rgFathers
	}

	async getAllChildrens(ident): Promise<ResourceGroup[]> {
		console.log(ident)
		let rgFathers = await this.resourceGroupModel
			.find({
				...{ "ResourceGroupParentId": ident }, ...{
					"extra.status": {
						$ne: "deleted"
					},
				}
			}, `ResourceGroupParentId ResourceGroupId metadata.firstLevel.title origin`)
			.lean()
			.hint({ "_id": 1 })
			.exec()

		if (rgFathers.length > 0) {
			for (var i = 0; i < 5; i++) {
				let c = await recursiveChildren(rgFathers, this.resourceGroupModel)
				if (c) {
					if (c.length > 0)
						rgFathers.push(...c)
					else break
				} else break
			}
		}

		async function recursiveChildren(fathers, model) {
			let childrenIDs = fathers.map((x, i) => {
				return x.ResourceGroupId
			})

			let children = await model
				.find({
					...{ "ResourceGroupParentId": { "$in": childrenIDs } }, ...{
						"extra.status": {
							$ne: "deleted"
						},
					}
				}, `ResourceGroupId metadata.firstLevel.title`)
				.lean()
				.hint({ "_id": 1 })
				.exec()

			if (children.length > 0) {
				// let c_ = await recursiveChildren(children, model)
				return children
			}
		}

		return rgFathers.map(rg => {
			if (rg.metadata) {
				return {
					name: rg.metadata.firstLevel.title,
					id: rg.ResourceGroupId
				}
			}
		})
	}

	async lazy(ident, head, role = null): Promise<ResourceGroup[]> {
		head = head == '0';
		let path = ['all', '0'].includes(ident) ? [] : ident.split('|');
		let filterData = {
			"ResourceGroupParentId": /^0-/,
			// "extra.status": {
			// 	$ne: "deleted"
			// },
		};
		let rgFathers = [];
		if (head) {
			rgFathers = await this.getChildrens(filterData, head, path, role);
			const setChildrens = async (index, tree) => {
				index += 1;
				if (path.length > index) {
					const node = tree.filter((t) => (path[index] === t.id))[0];
					if (typeof node.children !== 'undefined' && node.children != 0) {
						node.children = await this.getChildrens({ "ResourceGroupParentId": node.id }, false, path, role);
						return setChildrens(index, node.children);
					} else {
						return 0;
					}
				} else {
					return 0
				}
			}
			const newTree = await setChildrens(path.length === 0 ? 0 : -1, rgFathers);
		}
		else {
			rgFathers = await this.getChildrens({ "ResourceGroupParentId": path[0] }, false, path, role);
		}
		function compare(a, b) {
			const idA = a.text ? a.text.toUpperCase() : '';
			const idB = b.text ? b.text.toUpperCase() : '';
			let comparison = 0;
			if (idA > idB) {
				comparison = 1;
			}
			else if (idA < idB) {
				comparison = -1;
			}
			return comparison;
		}
		rgFathers = rgFathers.sort(compare)

		if (head) {

			rgFathers = [{
				"text": 'Fondos',
				"state": { "opened": true },
				"children": rgFathers,
				//"icon": "fa fa-folder kt-font-warning"
			}]
		}

		return rgFathers;
	}
	async logicEdit({ id, user, body }) {
		const { metadata } = body;
		let rg = await this.resourceGroupModel.findOne({ "ident": id })
		const history = rg.extra ? rg.extra.history ? rg.extra.history : [] : [];
		const extra = {
			...rg.extra ? rg.extra : {},
			...{ status: 'updated' },
			...{
				history: [...history, ...[{
					id: this.utilService.uuidv4(),
					last: {
						metadata: rg.metadata,
					},
					status: 'updated',
					date: new Date(),
					user: (user._id.toString())
				}]]
			}
		}
		const firstLevel = {
			...rg.metadata.firstLevel ? rg.metadata.firstLevel : null,
			...metadata.firstLevel ? metadata.firstLevel : null
		};
		const humanRights = {
			...rg.metadata.missionLevel ? rg.metadata.missionLevel.humanRights ? rg.metadata.missionLevel.humanRights : null : null,
			...metadata.missionLevel ? metadata.missionLevel.humanRights ? metadata.missionLevel.humanRights : null : null
		};
		const target = {
			...rg.metadata.missionLevel ? rg.metadata.missionLevel.target ? rg.metadata.missionLevel.target : null : null,
			...metadata.missionLevel ? metadata.missionLevel.target ? metadata.missionLevel.target : null : null
		};
		const source = {
			...rg.metadata.missionLevel ? rg.metadata.missionLevel.source ? rg.metadata.missionLevel.source : null : null,
			...metadata.missionLevel ? metadata.missionLevel.source ? metadata.missionLevel.source : null : null
		};
		rg.metadata = {
			firstLevel: firstLevel,
			missionLevel: {
				humanRights: humanRights,
				target: target,
				source: source,
			}
		};
		rg.extra = extra;
		await rg.save()
		return rg;
	}

	async getMetadata(ident) {
		const rg = await this.resourceGroupModel.findOne({ ident: ident }).exec();
		return { metadataHTML: `<div class="meta-container">${this.utilService.getMetadataHtml(rg)}</div>` };
	}

	async logicMove({ user, body }) {
		const { selected, father } = body;
		let rg = await this.resourceGroupModel.findOne({ "ident": selected.id })
		const history = rg.extra ? rg.extra.history ? rg.extra.history : [] : [];
		const extra = {
			...rg.extra ? rg.extra : {},
			...{ status: 'updated' },
			...{
				history: [...history, ...[{
					id: this.utilService.uuidv4(),
					last: {
						ResourceGroupParentId: rg.ResourceGroupParentId,
					},
					status: 'updated',
					date: new Date(),
					user: (user._id.toString())
				}]]
			}
		}
		rg.extra = extra;
		rg.ResourceGroupParentId = father.id;
		await rg.save()
		return rg;
	}

	async logicDelete(id, user) {
		const childrens = await this.resourceGroupModel.find({
			"ResourceGroupParentId": id,
			"extra.status": { $ne: "deleted" }
		}).count();
		const resources = await this.resourceModel.find({
			"ResourceGroupId": id,
			"extra.status": { $ne: "deleted" }
		}).count()

		let messageError = childrens > 0 ? 'El fondo tiene fondos hijos, ' : '' + (resources > 0) ? 'El fondo tiene recursos, ' : '';
		let resourceGroup = await this.resourceGroupModel.findOne({ "ident": id }).exec();

		if ((childrens + resources) === 0) {
			const extra = {
				...resourceGroup.extra ? resourceGroup.extra : {},
				...{ status: 'deleted' },
				...{
					history: [
						...resourceGroup.extra ? resourceGroup.extra.history ? resourceGroup.extra.history : [] : [],
						...[{
							id: this.utilService.uuidv4(),
							last: null,
							status: 'deleted',
							date: new Date(),
							user: (user._id.toString())
						}]]
				}
			}
			resourceGroup.extra = extra;
			await resourceGroup.save()
			return resourceGroup;
		} else {
			return { error: messageError + `No es posible eliminar el fondo: ${resourceGroup.metadata.firstLevel.title}` }
		}
	}

	async new({ resourceGroupParentId, user, body, origin }) {
		console.log({ resourceGroupParentId, user, body, origin });
		const { metadata } = body;
		const extra = {
			createdBy: 'METABUSCADOR',
			history: [{
				id: this.utilService.uuidv4(),
				last: null,
				status: 'created',
				date: new Date(),
				user: (user._id.toString())
			}],
			status: 'created',
		};
		const newRG = {
			ResourceGroupId: '',
			ResourceGroupParentId: resourceGroupParentId,
			resources: [],
			metadata: metadata,
			extra: extra,
			type: null,
			origin: origin,
		}
		const parent = resourceGroupParentId.split('-').pop();
		const newResourceGroup = await this.resourceGroupModel.create(newRG);
		newResourceGroup.identifier = `${newResourceGroup.auto}-${parent}MB`;
		newResourceGroup.ident = `${newResourceGroup.auto}-${parent}MB`;
		newResourceGroup.ResourceGroupId = `${newResourceGroup.auto}-${parent}MB`;
		await newResourceGroup.save()
		return newResourceGroup;
	}

	async findOne(ident): Promise<ResourceGroup[]> {
		return this.resourceGroupModel
			.findOne({
				ident: ident
			})
			.exec();
	}

	async findAll(): Promise<ResourceGroup[]> {
		return this.resourceGroupModel.find().exec();
	}

	async catalogacionFuentesInternas(): Promise<ResourceGroup[]> {
		return this.resourceGroupModel.find({ "origin": "CatalogacionFuentesInternas" }).exec();
	}

	async getDistinctOrigin() {
		return this.resourceGroupModel.distinct('origin');
	}

	async getRecursiveMetadata(ident) {
		const rg = await this.resourceGroupModel.findOne({ ident: ident }).exec();
		let fondos = [];
		let plainRg = this.utilService.getMetadatePlain(rg);
		fondos.push(plainRg);
		// Busca a los hijos de manera recursiva
		let hijos = await this.getRecursiveMetadataFondos(ident);
		fondos = fondos.concat(hijos);
		return { fondos: fondos };
	}

	async getRecursiveMetadataFondos(ident) {
		let rgs = await this.resourceGroupModel.find({ ResourceGroupParentId: ident }).exec();
		let fondos = rgs.map((element) => {
			if (element && element.metadata) {
				return this.utilService.getMetadatePlain(element);
			}
		});
		for (let i = 0 ; i < rgs.length ; i++){
			fondos = fondos.concat(await this.getRecursiveMetadataFondos(rgs[i].ident));
		}
		return fondos;
	}
}
