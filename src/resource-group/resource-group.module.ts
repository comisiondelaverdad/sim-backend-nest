import { forwardRef, Module } from '@nestjs/common';
import { getConnectionToken, MongooseModule } from '@nestjs/mongoose';
import { ResourceGroupController } from './resource-group.controller';
import { ResourceGroupService } from './resource-group.service';
import { ResourceGroupCacheService } from './resource-group-cache.service';
import { ResourceGroupSchema } from './schema/resource-group.schema';
import { PassportModule } from '@nestjs/passport';
import { Connection } from 'mongoose';
import * as AutoIncrementFactory from 'mongoose-sequence';
import { UtilService } from 'src/util/util.service';
import { ResourceModule } from 'src/resource/resource.module';

@Module({
  imports: [
    forwardRef(() => ResourceModule),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    MongooseModule.forFeatureAsync([
      {
        name: 'ResourceGroup',
        useFactory: async (connection: Connection) => {
          const schema = ResourceGroupSchema;
          const AutoIncrement = AutoIncrementFactory(connection);
          // eslint-disable-next-line @typescript-eslint/camelcase
          schema.plugin(AutoIncrement, { id: 'auto_resourcegroups_ident', inc_field: 'auto', start_seq: 1000000});
          return schema;
        },
        inject: [getConnectionToken("database")]
      }
    ],"database"),
  ],
  controllers: [ResourceGroupController],
  providers: [ResourceGroupService, ResourceGroupCacheService, UtilService],
  exports: [ResourceGroupService, ResourceGroupCacheService, MongooseModule]
})
export class ResourceGroupModule { }