import { Test, TestingModule } from '@nestjs/testing';
import { ResourceGroupController } from './resource-group.controller';

describe('ResourceGroup Controller', () => {
  let controller: ResourceGroupController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ResourceGroupController],
    }).compile();

    controller = module.get<ResourceGroupController>(ResourceGroupController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
