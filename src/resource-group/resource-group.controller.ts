import { Controller, Post, Body, Get, Query, Param, Delete, Put, HttpCode, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard, matchRoles, matchRolesList } from '../auth/roles.guard';
import { ResourceGroupService } from './resource-group.service';
import { ResourceGroupCacheService } from './resource-group-cache.service';

import { ApiTags, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('resource-groups')
@Controller('api/resource-groups')
export class ResourceGroupController {

  constructor(private readonly resourceGroupService: ResourceGroupService,
    private readonly resourceGroupCacheService: ResourceGroupCacheService) {
    console.log('inicialize resourceGroup cache ...')
    this.resourceGroupCacheService.init();
  }


  @Get('two-level')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async listTwoLevels() {
    return await this.resourceGroupService.twoLevels();;
  }

  @Get('lazy')
  @HttpCode(200)
  // @UseGuards(AuthGuard())
  async listOnDemandChild(@Request() req, @Query('path') path: string, @Query('head') head: string) {
    if (head == '0' && path == 'all') {
      const firstLevel = await this.resourceGroupCacheService.firstLevelTreeCache();
      //console.log('Desde controlador', JSON.stringify(firstLevel, null, "\t"));
      return firstLevel;
    } else {
      return await this.resourceGroupService.lazy(path, head, req ? req.user ? req.user.roles ? req.user.roles : null : null : null);
    }
  }

  @Get('get-all-children')
  @HttpCode(200)
  async getAllChildren(@Query('ident') ident: string) {
    return await this.resourceGroupService.getAllChildrens(ident);;
  }

  @Post('first-level')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async listFirstLevel(@Body() body) {
    return await this.resourceGroupService.firstLevel(body);
  }

  @Get('first-level-cache')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async listFirstLevelCache(@Body() body) {
    //console.log("algo anda muy mal por acá! ")
    return await this.resourceGroupCacheService.firstLevelCache();
    //return await this.resourceGroupService.firstLevel('')
  }

  @Get('internal')
  @UseGuards(AuthGuard())
  async findInternal() {
    return await this.resourceGroupService.catalogacionFuentesInternas();
  }

  @Get('origins')
  @UseGuards(AuthGuard())
  async findOrigins() {
    return await this.resourceGroupService.getDistinctOrigin();
  }

  @Get('metadata/:id')
  @UseGuards(AuthGuard())
  async getMetadata(@Param('id') ident: string) {
    return await this.resourceGroupService.getMetadata(ident);
  }

  @Get('recursive-metadata/:id')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async getRecursiveMetadata(@Request() req,@Param('id') ident: string) {
    const rolesFunction = matchRolesList(req.user, ['catalogador_gestor']);
    return await this.resourceGroupService.getRecursiveMetadata(ident);
  }

  @Get(':id')
  //@UseGuards(AuthGuard())
  async findOne(@Param('id') ident: string) {
    return await this.resourceGroupService.findOne(ident);
  }

  @Get()
  @UseGuards(AuthGuard())
  async findAll() {
    return await this.resourceGroupService.findAll();
  }

  @Delete(':id')
  @UseGuards(AuthGuard())
  async lock(@Request() req, @Param("id") id) {
    matchRoles(req.user, 'catalogador_gestor') //Validación de roles del usuario
    let deleted = await this.resourceGroupService.logicDelete(id, req.user);
    this.resourceGroupCacheService.updateCache();
    return deleted;
  }

  @Post('update-metadata')
  @UseGuards(AuthGuard())
  async update(@Request() req, @Body() body) {
    const rolesFunction = matchRolesList(req.user, ['catalogador', 'catalogador_gestor'])
    const metadata = {
      ...{ id: body.ident },
      ...{ body: body },
      ...{ user: req.user },
    };
    let returnData = await this.resourceGroupService.logicEdit(metadata);
    return returnData;
  }

  @Post('logic-move')
  @UseGuards(AuthGuard())
  async logicMove(@Request() req, @Body() body) {
    const rolesFunction = matchRoles(req.user, 'catalogador_gestor')
    const data = {
      ...{ body: body },
      ...{ user: req.user },
    };
    let returnData = await this.resourceGroupService.logicMove(data);
    console.log(returnData);
    this.resourceGroupCacheService.updateCache();
    return returnData;
  }

  @Post('')
  @UseGuards(AuthGuard())
  async post(@Request() req, @Body() body) {
    const rolesFunction = matchRolesList(req.user, ['catalogador_gestor'])
    const metadata = {
      ...{ resourceGroupParentId: body.ident },
      ...{ body: body },
      ...{ user: req.user },
      ...{ origin: body.origin }
    };
    let returnData = await this.resourceGroupService.new(metadata);
    console.log(returnData);
    this.resourceGroupCacheService.updateCache();
    return returnData;
  }

  /*
  @Put(':id')
  update(@Param('id') id: string, @Body() updateCatDto: UpdateCatDto) {
    return `This action updates a #${id} cat`;
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return `This action removes a #${id} cat`;
  }
  */
}
