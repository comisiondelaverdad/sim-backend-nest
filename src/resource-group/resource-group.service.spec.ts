import { Test, TestingModule } from '@nestjs/testing';
import { ResourceGroupService } from './resource-group.service';

describe('ResourceGroupService', () => {
  let service: ResourceGroupService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ResourceGroupService],
    }).compile();

    service = module.get<ResourceGroupService>(ResourceGroupService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
