import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({collection:"resourcegroups"})
export class ResourceGroup extends Document {
  @Prop({type:"string"})
  identifier: string;

  @Prop({type:"string"})
  ident: string;

  @Prop({type:"number"})
  auto: number;

  @Prop({type:"array"})
  type: string[];

  //@Prop()
  //metadata: ResourceGroupMetadataSchema;

  @Prop({type:"array"})
  resources: string[];

  @Prop({type:"string"})
  origin: string;

  @Prop({type:"string"})
  ResourceGroupId: string;

  @Prop({type:"string"})
  ResourceGroupParentId: string;

  @Prop({type:"object"})
  extra: Object;

  @Prop({type:"object"})
  metadata: Object;
}


export const ResourceGroupSchema = SchemaFactory.createForClass(ResourceGroup);



