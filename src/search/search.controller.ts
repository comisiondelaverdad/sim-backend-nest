import { Controller, Query, Body, Post, HttpCode, Param, Delete, Get, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { matchRoles } from '../auth/roles.guard';
import { SearchService } from './search.service';
import { CreateSearchHistoryDto } from './dto/create-search-history.dto'

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('search')
@Controller('api/search')
export class SearchController {
  constructor(
    private readonly searchService: SearchService
  ) { }

  @Post()
  @HttpCode(200)
  async search(@Body() body, @Query() query, @Request() req): Promise<any> {
    if (body.source === 'museo') {
      return this.searchService.simpleMuseo({ body, query }).then(async (x) => {
        let log = await this.searchService.loggerSearch(req.user, 'search', { body, query }, x.data);
        return { ...x.data.hits }
      }).catch((err) => {
        console.log(err)
        if (err.response)
          console.log(err.response.data.error)
        else
          console.log(err)
        return []
      })
    } else if(body.source === 'hitos'){
      return this.searchService.simpleHitos({ body, query }).then(async (x) => {
        return { ...x.data.hits }
      }).catch((err) => {
        console.log(err)
        if (err.response)
          console.log(err.response.data.error)
        else
          console.log(err)
        return []
      })
    }else {
      return this.searchService.simple({ body, query }).then(async (x) => {
        let log = await this.searchService.loggerSearch(req.user, 'search', { body, query }, x.data);
        return { ...x.data.hits, buckets: x.data.aggregations.resources_type.buckets }
      }).catch((err) => {
        console.log(err)
        if (err.response)
          console.log(err.response.data.error)
        else
          console.log(err)
        return []
      })
    }

  }

  @UseGuards(AuthGuard())
  @Get('detail/:id')
  async detailMuseo(@Param('id') id: String, @Request() req) {
    matchRoles(req.user, 'invitado')
    const resp = await this.searchService.detailMuseo(id);
    return { ...resp.data.hits }
  }

  @Get('detail/archivo/:slug')
  async detailArchivo(@Param('slug') slug: String, @Request() req) {
    const resp = await this.searchService.detailMuseo(slug);
    return { ...resp.data.hits }
  }
  @Get('detail/simpleident/:simpleident')
  async detailArchivoSimpleident(@Param('simpleident') simpleident: String, @Request() req) {
    const resp = await this.searchService.detailMuseoSimpleIdent(simpleident);
    return { ...resp.data.hits }
  }

  @Get('detail/ident/:id')
  async detailIdent(@Param('id') id: String, @Request() req) {
    const resp = await this.searchService.detailMuseoIdent(id);
    return { ...resp.data.hits }
  }
  

  
  @UseGuards(AuthGuard())
  @Post('resource/ident')
  async getResourceByIdent(@Body() body, @Request() req) {
    const resp = await this.searchService.getResourceByIdent(body.indents);
    return resp
  }

  @UseGuards(AuthGuard())
  @Post('suggestmuseo')
  async suggestMuseo(@Body() body, @Request() req) {
    matchRoles(req.user, 'invitado')
    const resp = await this.searchService.suggestMuseo(body);
    return resp.data.suggest
  }

  @UseGuards(AuthGuard())
  @Get('search-history/:id')
  async read(@Param('id') id: String) {
    return await this.searchService.read(id);
  }

  @UseGuards(AuthGuard())
  @Get('get-history/:username')
  async getHistory(@Param('username') username: String) {
    return await this.searchService.getDataHistorySearch(username);
  }

  @UseGuards(AuthGuard())
  @Get('get-view/:username')
  async getView(@Param('username') username: String) {
    return await this.searchService.getDataViewSearch(username);
  }



  @UseGuards(AuthGuard())
  @Post('search-history')
  async saveHistory(@Body() createSearchHistoryDto: CreateSearchHistoryDto) {
    return await this.searchService.saveHistory(createSearchHistoryDto);
  }

  @UseGuards(AuthGuard())
  @Post('search-history/:id')
  async loadHistory(@Param('id') id: String, @Body() searchHistory: any) {
    let history = await this.searchService.loadHistory(id, searchHistory);
    return history ? history : 0;
  }

  @UseGuards(AuthGuard())
  @Delete('search-history/:id')
  async deleteHistory(@Param('id') id: String) {
    return await this.searchService.deleteHistory(id);
  }

  // @Post('search-museo')
  // @HttpCode(200)
  // @UseGuards(AuthGuard())
  // getHistogramDates(@Body() body): Promise<any> {
  //   return this.searchMuseo.getHistogramDates(body).then(x => {
  //     return x
  //   }).catch(err => {
  //     return {
  //       error: err
  //     }
  //   })
  // }
  

}
