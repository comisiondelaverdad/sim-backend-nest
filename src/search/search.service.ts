import { Injectable, HttpService } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { ConfigService } from '../config/config.service';
import { LocationService } from '../admin/location/location.service';
import { CreateSearchHistoryDto } from './dto/create-search-history.dto'
import { SearchHistory } from './schema/search-history.schema'
import { departamentos } from '../geo-shapes/departamentos'
import { municipios } from '../geo-shapes/municipios'

import * as https from 'https'
import * as fs from 'fs';
import * as request from 'request-promise'
import * as fetch from 'node-fetch';


// var afterLoad=require('after-load');

@Injectable()
export class SearchService {

  config: any = undefined;
  httpsAgent = null;


  constructor(
    private http: HttpService,
    private locationService: LocationService,
    private configX: ConfigService,
    //private usersService: UsersService,
    @InjectModel('SearchHistory') private searchHistoryModel: Model<SearchHistory>,
  ) {
    let crtFile = fs.readFileSync(configX.get("ELASTIC_AUTH_CERT"))
    //console.log("El archivo del certificado es: ", crtFile)
    let agent = new https.Agent({ ca: crtFile })
    this.httpsAgent = agent
    this.config = {
      elastic: {
        url: configX.get("ELASTIC_URL"),
        index: {
          data: configX.get("ELASTIC_INDEX_DATA"),
          logger: configX.get("ELASTIC_INDEX_LOGGER"),
          env: configX.get("ELASTIC_INDEX_ENV"),
          museo: configX.get("ELASTIC_INDEX_MUSEO"),
          dpto: configX.get("ELASTIC_DPTO"),
          mpio: configX.get("ELASTIC_MPIO")
        },
        headers: {
          auth: {
            username: configX.get("ELASTIC_AUTH_USER"),
            password: configX.get("ELASTIC_AUTH_PASS")
          }
        }
      }
    }
  }

  async statsByViews(ids): Promise<void | any> {

    let queryIds =
    {
      "bool": {
        "should": [],
      }
    }
    for (let i = 0; i < ids.length; i++) {
      queryIds.bool.should.push(
        {
          "match_phrase": {
            "result.ident.keyword": ids[i]
          }
        }
      )
    }
    let queryResourceViews =
    {
      "aggs": {
        "resource_views": {
          "terms": {
            "field": "result.ident.keyword",
            "order": {
              "_count": "desc"
            }
          }
        }
      },
      "size": 0,
      "stored_fields": ["*"],
      "script_fields": {},
      "query": {
        "bool": {
          "must": {
            "terms": {
              "result.ident.keyword": ids
            }
          }
        }
      }
    }

    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let response = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.logger + "-view-" + this.config.elastic.index.env + "/_search",
      queryResourceViews,
      h
    )
      .toPromise()
    return response.data

  }

  async count(params): Promise<any> {
    let query = params.query;
    let filters = params.body.filters;



    let json = {
      "query": {
        "bool": {
          "filter": [],
          "must": [],
          "should": [],
          "must_not": [
            {
              "term": {
                "document.record.extra.status.keyword": {
                  "value": "deleted"
                }
              }
            },
            { "term": { "record.type.keyword": "Consentimiento informado" } },
            { "term": { "record.type.keyword": "Ficha corta" } },
            { "term": { "record.type.keyword": "Ficha larga" } },
            { "term": { "record.type.keyword": "Transcripción preliminar" } },
            { "term": { "record.type.keyword": "Archivos de transcripcion (otranscribe)" } },
            { "term": { "document.resource.extra.status.keyword": "deleted" } }
          ]
        }
      }
    }

    if (params.origin !== undefined) {
      json.query.bool.filter.push(
        {
          "match_phrase":
            { "record.origin.keyword": params.origin }
        }
      );
    }

    if (params.ngramFilter !== undefined) {
      if (params.ngramFilter.length > 0) {
        let words = params.ngramFilter

        words.map(w => {
          let path = "record.labelled_for_viz." + w.gram + ".label.keyword"
          let str = '*' + w.nodo + '*'
          let filter = {
            "nested": {
              "path": "record.labelled_for_viz." + w.gram,
              "query": {
                "wildcard": {
                  [path]: str
                }
              }
            }
          }

          json.query.bool.must.push(filter)
        })
      }
    }

    if (params.etiquetasFilter !== undefined) {
      if (params.etiquetasFilter.length > 0) {
        let words = params.etiquetasFilter

        words.map(w => {
          let str = w.name
          let filter = {
            "nested": {
              "path": "record.labelled_for_viz.etiquetas",
              "query": {
                "term": {
                  "record.labelled_for_viz.etiquetas.label.keyword": str
                }
              }
            }
          }

          json.query.bool.must.push(filter)
        })
      }
    }

    if (query.q !== undefined) {
      let keyword = query.q;
      // REEMPLAZAR | por OR y + por AND
      keyword = keyword.replace(" | ", " OR ");
      keyword = keyword.replace(" + ", " AND ");
      json.query.bool.must.push({ "query_string": { "query": keyword, "default_operator": "and" } });
    }

    if (filters.resourceGroup.resourceGroupIDS !== '') {
      json.query.bool.filter.push({ "terms": { "resourceGroup.resourceGroupId.keyword": filters.resourceGroup.resourceGroupIDS.split(",") } })
    }

    if (filters.mapPolygon !== '') {
      let geoQuery = filters.mapPolygon.split("_")
      if (geoQuery[0] === "P") {
        let points = geoQuery[1].split(",")
        let arrayPoints = []
        for (let i = 0; i < points.length / 2; i++) {
          arrayPoints.push(points[2 * i + 1] + "," + points[2 * i])
        }
        json.query.bool.filter.push({ "geo_polygon": { "record.geographicCoverage.location": { "points": arrayPoints } } })
      }
    }

    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    return await this.http.post(this.config.elastic.url + this.config.elastic.index.data + "-" + this.config.elastic.index.env + "/_count", json, h).toPromise()
  }

  async resources_images(query): Promise<any> {
      let json = {
      _source: {
        excludes: [],
      },
      query: {
        "bool": {
          "filter": {
            "bool": {
              "must": [
                {
                  "bool": {
                    "should": [
                     {
                        "term": {
                          "metadata.firstLevel.fileFormat.keyword": {
                            "value": "jpg"
                          }
                        }
                      },
                      {
                        "term": {
                          "metadata.firstLevel.fileFormat.keyword": {
                            "value": "png"
                          }
                        }
                      },
                      {
                        "term": {
                          "support.keyword": {
                            "value":"Galería fotográfica"
                          }
                        }
                      }
                    ]
                  }
                }
              ],
              "must_not": []
            }
            }, 
          "must": [],
          "should": []
        }
      },
      track_total_hits: true,
    };
    json["sort"] = [{"metadata.firstLevel.creationDate":{ "order": "desc"}}]
   
    if (query.from !== undefined) {
      json["from"] = query.from
    } else if (query.from < 0) {
      json['from'] = 0
    }
    if (query.size !== undefined) {
      json["size"] = query.size
    } else {
      json["size"] = 10
    }


      if(query.keyword){
        json.query.bool.should.push({
          "multi_match" : {
                  "query":    query.keyword,                   
                  "fields": [
                    "identifier",
                    "simpleident",
                    "ident",
                    "metadataResource.simpleident",
                    "metadataResource.firstLevel.description",
                    "metadataResource.firstLevel.title"
                  ] ,
                  "type": "phrase"
                }
        } );
       delete json["sort"] 
    }

   
    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let resp: any =  await this.http.post(this.config.elastic.url + "sim-museo-imgs-prod" + "/_search", json, h).toPromise()
    return resp
  }

  async simple(params): Promise<any> {
    let query = params.query;
    let filters = params.body.filters;


    let json = {
      "_source": {
        "excludes": ["resourceGroup.resources", "resource.records"]
      },
      "query": {
        "bool": {
          "filter": [],
          "must": [

          ],
          "should": [],
          "must_not": [
            {
              "term": {
                "document.record.extra.status.keyword": {
                  "value": "deleted"
                }
              }
            },
            { "term": { "record.type.keyword": "Consentimiento informado" } },
            { "term": { "record.type.keyword": "Ficha corta" } },
            { "term": { "record.type.keyword": "Ficha larga" } },
            { "term": { "record.type.keyword": "Transcripción preliminar" } },
            { "term": { "record.type.keyword": "Archivos de transcripcion (otranscribe)" } },
            { "term": { "document.resource.extra.status.keyword": "deleted" } }
          ]
        }
      },
      "track_total_hits": true,
      "aggs": {
        "resources_type": {
          "terms": {
            "field": "resource.type.keyword"
          }
        }
      }
    }

    if (query.showTags === "1") {
      json.query.bool.must.push({ "exists": { "field": "record.labelled" } });
    }

    if (query.q !== undefined) {
      let keyword = query.q;
      // REEMPLAZAR | por OR y + por AND
      keyword = keyword.replace(" | ", " OR ");
      keyword = keyword.replace(" + ", " AND ");
      json.query.bool.must.push({ "query_string": { "query": keyword, "default_operator": "and" } });
    }

    if (filters.resourceGroup !== undefined) {
      if (filters.resourceGroup.resourceGroupIDS !== '') {
        json.query.bool.filter.push({ "terms": { "resourceGroup.resourceGroupId.keyword": filters.resourceGroup.resourceGroupIDS.split(",") } })
      }
    }


    if (filters.mapPolygon !== '' && filters.mapPolygon !== undefined) {
      //console.log("Agregando mapa: ", filters.mapPolygon)
      let geoQuery = filters.mapPolygon.split("_")
      if (geoQuery[0] === "P") {
        let points = geoQuery[1].split(",")
        let arrayPoints = []
        for (let i = 0; i < points.length / 2; i++) {
          arrayPoints.push(points[2 * i + 1] + "," + points[2 * i])
        }
        //console.log("Puntos del mapa", arrayPoints)
        json.query.bool.filter.push({ "geo_polygon": { "record.geographicCoverage.location": { "points": arrayPoints } } })
      }
      if (geoQuery[0] === "M") {
        //console.log(geoQuery)
        let geo = await this.locationService.findByid(geoQuery[1])
        let points = geo.geometry["coordinates"]
        if (geo.geometry["type"] === 'Polygon') {
          json.query.bool.filter.push({ "geo_polygon": { "record.geographicCoverage.location": { "points": points[0] } } })
        } else if (geo.geometry["type"] === 'MultiPolygon') {
          //https://stackoverflow.com/questions/61282260/elasticsearch-query-if-geo-polygon-is-inside-a-multi-polygon
          let poligonList = []
          for (let i = 0; i < points.length; i++) {
            for (let j = 0; j < points[i].length; j++) {
              poligonList.push({ "geo_polygon": { "record.geographicCoverage.location": { "points": points[i][j] } } })
            }
          }
          json.query.bool.filter.push({ "bool": { "should": poligonList } })
        } else {
          //Otras posibles opciones, solo se toma el polígono principal
          json.query.bool.filter.push({ "geo_polygon": { "record.geographicCoverage.location": { "points": points[0][0] } } })
        }
      }
    }

    if (query.from !== undefined) {
      json["from"] = query.from
    } else if (query.from < 0) {
      json['from'] = 0
    }
    if (query.size !== undefined) {
      json["size"] = query.size
    } else {
      json["size"] = 10
    }

    if (params.origin !== undefined) {
      json.query.bool.filter.push(
        {
          "match_phrase":
            { "record.origin.keyword": params.origin }
        }
      );
    }

    if (params.ngramFilter !== undefined) {
      if (params.ngramFilter.length > 0) {
        let words = params.ngramFilter

        words.map(w => {
          let path = "record.labelled_for_viz." + w.gram + ".label.keyword"
          let str = '*' + w.nodo + '*'
          let filter = {
            "nested": {
              "path": "record.labelled_for_viz." + w.gram,
              "query": {
                "wildcard": {
                  [path]: str
                }
              }
            }
          }

          json.query.bool.must.push(filter)
        })
      }
    }

    if (params.etiquetasFilter !== undefined) {
      if (params.etiquetasFilter.length > 0) {
        let words = params.etiquetasFilter

        words.map(w => {
          let str = w.name
          let filter = {
            "nested": {
              "path": "record.labelled_for_viz.etiquetas",
              "query": {
                "term": {
                  "record.labelled_for_viz.etiquetas.label.keyword": str
                }
              }
            }
          }

          json.query.bool.must.push(filter)
        })
      }
    }

    if (params.aggre !== undefined) {
      if (params.aggre) {

      }
    }

    json['highlight'] = {
      "pre_tags": ["<span class='badge badge-warning'><i>"],
      "post_tags": ["</i></span>"],
      "fields": {
        "*": {}
      }
    }

    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    return this.http.post(this.config.elastic.url + this.config.elastic.index.data + "-" + this.config.elastic.index.env + "/_search", json, h).toPromise()
  }

  async suggestMuseo(params): Promise<any> {
    let query = params

    let json = {
      "_source": {
        "excludes": []
      },
      "size": 0,
      "query": {
        "bool": {
          "filter": [],
          "must": [],
          "should": [
            { "term": { "document.metadata.firstLevel.accessLevel.keyword": "4" } },
            { "term": { "document.metadata.firstLevel.accessLevel.keyword": 4 } }
          ],
          "must_not": [
            { "term": { "document.extra.status.keyword": "deleted" } }
          ]
        }
      }
    }

    if (query.q !== undefined && query.q !== '') {
      json['suggest'] = {
        "autocomplete": {
          "text": query.q,
          "term": {
            "field": "document.metadata.firstLevel.title"
          }
        }
      }
    }

    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    return this.http.post(this.config.elastic.url + this.config.elastic.index.museo + "-" + this.config.elastic.index.env + "/_search", json, h).toPromise()
  }

  async simpleHitos(params): Promise<any> {
    let query = params.body;

    let json = {
      "_source": {

      },
      "sort": [
        { "temporalCoverage.start": { "order": "asc", "format": "strict_date_optional_time_nanos" } }
      ],
      "query": {
        "bool": {
          "filter": [],
          "must": [],
          "should": [],
          "must_not": []
        }
      },
      "track_total_hits": true
    }

    if (query.q !== undefined && query.q !== '') {
      json.query.bool.must.push({ "query_string": { "query": query.q, "default_operator": "and" } });
    }

    if (query.from !== undefined) {
      json["from"] = query.from
    } else if (query.from < 0) {
      json['from'] = 0
    }
    if (query.size !== undefined) {
      json["size"] = 10
    } else {
      json["size"] = 10
    }

    if (query.temporalCoverage !== undefined) {
      json.query.bool.must.push({ "range": { "temporalCoverage.start": { "gte": query.temporalCoverage.split('-')[0], "lt": query.temporalCoverage.split('-')[1] } } });
      json.query.bool.must.push({ "range": { "temporalCoverage.end": { "gte": query.temporalCoverage.split('-')[0], "lt": query.temporalCoverage.split('-')[1] } } });
    }

    if (query.dptoExplora !== undefined) {
      const dep = departamentos.find(d => d.codigo === query.dptoExplora.divipola)

      if (dep) {
        json.query.bool.filter.push({
          "geo_shape": {
            "geographicCoverage.geoPoint": {
              "indexed_shape": {
                "index": this.config.elastic.index.dpto,
                "id": dep.id,
                "path": "geometry"
              },
              "relation": "intersects"
            }
          }
        })
      }
    }

    if (query.mpioExplora !== undefined) {
      const mun = municipios.find(d => d.codigo === query.mpioExplora.divipola)

      if (mun) {
        json.query.bool.filter.push({
          "geo_shape": {
            "geographicCoverage.geoPoint": {
              "indexed_shape": {
                "index": this.config.elastic.index.mpio,
                "id": mun.id,
                "path": "geometry"
              },
              "relation": "intersects"
            }
          }
        })
      }
    }


    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let resp: any = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.museo + "-linea-" + this.config.elastic.index.env + "/_search",
      json,
      h
    ).toPromise()

    return resp
  }

  async simpleMuseo(params): Promise<any> {
    let query = params.body;
    if (query.q && query.q === 'gallery') {
      return this.resources_images(query)
    }


    let json = {
      "_source": {
        "includes": [
          "document.ident",
          "document.identifier",
          "document.type",
          "document.metadata.simpleident",
          "document.metadata.slug",
          "document.metadata.firstLevel.url",
          "document.metadata.firstLevel.title",
          "document.metadata.firstLevel.description",
          "document.metadata.firstLevel.creator",
          "document.metadata.firstLevel.geographicCoverage",
          "document.metadata.firstLevel.temporalCoverage",
          "document.metadata.missionLevel",
          "document.records.idmongo",
          "document.records.support",
          "document.records.filename",
          "document.records.ident",
          "document.records.metadata.firstLevel.title",
          "document.metadata.firstLevel.url",
        ]
      },
      "query": {
        "bool": {
          "filter": [],
          "must": [],
          "should": [],
          "must_not": []
        }
      },
      "track_total_hits": true
    }

    json['query']['bool']['must'].push({
      "term": {
        "document.metadata.firstLevel.accessLevel.keyword": {
          "value": "4"
        }
      }
    })

    if (query.q !== undefined && query.q !== '') {
      json.query.bool.must.push({
        "query_string": {
          "query": query.q,
          fields: [
            "document.metadata.firstLevel.title",
            "document.metadata.simpleident",
            "document.metadata.slug",
            "document.ident",
            "document.metadata.firstLevel.description",
            "document.metadata.firstLevel.geographicCoverage",
            "document.records.metadata.firstLevel.title"
          ]
        }
      });
    }

    if (query.tipo) {
      json.query.bool.must.push({
        "nested": {
          "path": "document.records",
          "query": {
            "bool": {
              "filter": [
                {
                  "term": {
                    "document.records.support.keyword": {
                      "value": query.tipo
                    }
                  }
                }
              ]
            }
          }
        }
      })
    }



    if (query.from !== undefined) {
      json["from"] = query.from
    } else if (query.from < 0) {
      json['from'] = 0
    }
    if (query.size !== undefined) {
      json["size"] = 10
    } else {
      json["size"] = 10
    }

    if (query.temporalCoverage !== undefined) {
      json.query.bool.must.push({ "range": { "document.metadata.firstLevel.temporalCoverage.start": { "gte": query.temporalCoverage.split('-')[0], "lt": query.temporalCoverage.split('-')[1] } } });
      json.query.bool.must.push({ "range": { "document.metadata.firstLevel.temporalCoverage.end": { "gte": query.temporalCoverage.split('-')[0], "lt": query.temporalCoverage.split('-')[1] } } });
    }

    if (query.fondo) {
      if(query.fondo.length > 0){
        json['query']['bool']['filter'].push({
          term: {
            'document.identifier.keyword': {
              value: query.fondo.id,
            },
          },
        });
      }
    }

    if (query.idents) {
      json.query.bool.filter.push({ "terms": { "document.ident": query.idents } });
    }


    if (query.dpto !== undefined) {
      const dep = departamentos.find(d => d.codigo === query.dpto.divipola)

      if (dep) {
        json.query.bool.filter.push({
          "geo_shape": {
            "document.metadata.firstLevel.geographicCoverage.geoPoint": {
              "indexed_shape": {
                "index": this.config.elastic.index.dpto,
                "id": dep.id,
                "path": "geometry"
              },
              "relation": "intersects"
            }
          }
        })
      }
    }

    if (query.mpio !== undefined) {
      const mun = municipios.find(d => d.codigo === query.mpio.divipola)

      if (mun) {
        json.query.bool.filter.push({
          "geo_shape": {
            "document.metadata.firstLevel.geographicCoverage.geoPoint": {
              "indexed_shape": {
                "index": this.config.elastic.index.mpio,
                "id": mun.id,
                "path": "geometry"
              },
              "relation": "intersects"
            }
          }
        })
      }
    }

    if (query.idArray !== undefined) {
      query.idArray.map(a => {
        json['query']['bool']['should'].push({
          "term": {
            "extra.identifier.keyword": {
              "value": a
            }
          }
        })
      })
    }


    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let resp: any = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.museo + "-" + this.config.elastic.index.env + "/_search",
      json,
      h
    ).toPromise()

    return resp
  }

  async detailMuseo(params): Promise<any> {
    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let json = {
      "size": 1,
      "query": {
        "bool": {
          "must": [
            {
              "term": {
                "document.metadata.slug.keyword": {
                  "value": params
                }
              }
            }
          ]
        }
      }
    }    
    let resp: any = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.museo + "-" + this.config.elastic.index.env + "/_search",
      json,
      h
    ).toPromise()

    return resp
  }
  async detailMuseoIdent(params): Promise<any> {
    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let json = {
      "size": 1,
      "query": {
        "bool": {
          "must": [
            {
              "term": {
                "document.ident": {
                  "value": params
                }
              }
            }
          ]
        }
      }
    }    
    let resp: any = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.museo + "-" + this.config.elastic.index.env + "/_search",
      json,
      h
    ).toPromise()

    return resp
  }
  async detailMuseoSimpleIdent(params): Promise<any> {
    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let json = {
      "size": 1,
      "query": {
        "bool": {
          "must": [
            {
              "term": {
                "document.metadata.simpleident.keyword": {
                  "value": params
                }
              }
            }
          ]
        }
      }
    }    
    let resp: any = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.museo + "-" + this.config.elastic.index.env + "/_search",
      json,
      h
    ).toPromise()

    return resp
  }
  

  async logger(user, index, resource): Promise<void | any> {
    let logdata = {
      user: user.username,
      date: new Date(),
      result: {
        type: resource.type,
        ident: resource.ident,
      },
      resourceGroup: {}
    }
    if (resource.resourceGroup !== undefined) {
      logdata.resourceGroup = {
        ident: resource.resourceGroup.ident,
        type: resource.resourceGroup.type,
      }
    }
    try {
      let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
      let response = await this.http.post(
        this.config.elastic.url + this.config.elastic.index.logger + "-" + index + "-" + this.config.elastic.index.env + "/_doc",
        logdata,
        h
      ).toPromise()
      return response.data
    } catch (e) {
      console.log(e)
      return e
    }
  }

  async loggerSearch(user=null, index, params, result): Promise<void | any> {
    let logdata = {
      user: user?user.username?user.username: null: null,
      date: new Date(),
      result: {
        total: result.hits ? result.hits.total : 0,
        max_score: result.hits ? result.hits.max_score : 0,
        _shards: result._shards
      },
      params: params,
    }
    try {
      let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
      let response = await this.http.post(
        this.config.elastic.url + this.config.elastic.index.logger + "-" + index + "-" + this.config.elastic.index.env + "/_doc",
        logdata,
        h
      ).toPromise()
      return response.data
    } catch (e) {
      console.log(e)
      return e
    }
  }

  async loggerMuseo(user, index, params, result): Promise<void | any> {
    let logdata = {
      user: user.username,
      date: new Date(),
      params: params
    }
    try {
      let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
      let response = await this.http.post(
        this.config.elastic.url + this.config.elastic.index.logger + "-" + index + "-museo-" + this.config.elastic.index.env + "/_doc",
        logdata,
        h
      ).toPromise()
      return response.data
    } catch (e) {
      console.log(e)
      return e
    }
  }


  async getResourceByIdent(arrIdent: String[]): Promise<object> {

    let query =
    {
      "_source": [
        "document.ident",
        "document.identifier", "document.type",
        "document.metadata.simpleident",
        "document.metadata.slug",
        "document.metadata.firstLevel.description",
        "document.metadata.firstLevel.title",
        "document.metadata.firstLevel.creator",        
        "document.metadata.firstLevel.temporalCoverage",
        "document.metadata.firstLevel.geographicCoverage",
        "document.metadata.missionLevel",
        "document.records.type",
        "document.records.filename",
        "document.records.metadata.firstLevel.title",
        "document.metadata.firstLevel.url",
        "document.records.ident",

      ],
      "query": {
        "bool": {
          "filter": [
            { "terms": { "document.ident": arrIdent } }
          ]
        }
      }
      ,
      "sort": [
        "document.ident"
      ]
    }
    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let response = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.museo + "-" + this.config.elastic.index.env + "/_search",

      query,
      h
    )
      .toPromise()
    return { ...response.data.hits }
  }


  async getResourceByIdentPager(arrIdent: String[], from, size, typeResource): Promise<object> {
    let query =
    {
      "from": from, "size": size,
      "_source": [
        "document.ident",
        "document.identifier",
        "document.type",
        "document.metadata.firstLevel.description",
        "document.metadata.firstLevel.title",
        "document.metadata.firstLevel.creator",
        "document.metadata.firstLevel.temporalCoverage",
        "document.metadata.firstLevel.geographicCoverage",
        "document.metadata.missionLevel",
        "document.records.type",
        "document.records.filename",
        "document.records.metadata.firstLevel.title",
        "document.metadata.firstLevel.url",
        "document.records.ident",
      ],
      "query": {
        "bool": {
          "filter": [
            { "terms": { "document.ident": arrIdent } }
          ]
        }
      }
      ,
      "sort": [
        "document.ident"
      ]
    }

    if (typeResource && typeResource.toLowerCase() != "recurso" && typeResource.toLowerCase() != "documento") {
      let type = ""
      if (typeResource.toLowerCase() === "galeria") {
        type = "Galería fotográfica"
      }
      else if (typeResource.toLowerCase() === "audio") {
        type = "Audio"
      }
      else if (typeResource.toLowerCase() === "video") {
        type = "Producto audiovisual"
      }


      let filterTypeResource = {
        "must": {
          "nested": {
            "path": "document.records",
            "query": {
              "bool": {
                "filter": [
                  {
                    "term": {
                      "document.records.type.keyword": {
                        "value": type
                      }
                    }
                  }
                ]
              }
            }
          }
        }
      }

      query["query"]["bool"] = { ...filterTypeResource, ...query["query"]["bool"] };

    } else if (typeResource.toLowerCase() === "documento") {

      let filterTypeResource = {
        "must": {
          "nested": {
            "path": "document.records",
            "query": {
              "bool": {
                "filter": [
                  {
                    "exists": {
                      "field": "document.records.content"
                    }
                  },
                  {
                    "exists": {
                      "field": "document.records.ident"
                    }
                  }
                ]
              }
            }
          }
        }
      }
      query["query"]["bool"] = { ...filterTypeResource, ...query["query"]["bool"] };
    }
   
    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }

    let response = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.museo + "-" + this.config.elastic.index.env + "/_search",

      query,
      h
    )
      .toPromise()
    return { ...response.data.hits }
  }

  async getResumeResourceByIdent(arrIdent: String[]): Promise<object> {
    let query =
    {
      "_source": ["document.ident",
        "document.ident",
        "document.identifier",
        "document.type",
        "document.metadata.firstLevel.description",
        "document.metadata.firstLevel.title",
        "document.metadata.firstLevel.creator",
        "document.metadata.firstLevel.temporalCoverage",
        "document.metadata.firstLevel.geographicCoverage",
        "document.metadata.missionLevel",
        "document.records.type",
        "document.records.filename",
        "document.records.metadata.firstLevel.title",
        "document.metadata.firstLevel.url",
        "document.records.ident"],
      "query": {
        "bool": {
          "filter": [
            { "terms": { "document.ident": arrIdent } }
          ]
        }
      },
      "sort": [
        "document.ident"
      ]
    }

    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let response = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.museo + "-" + this.config.elastic.index.env + "/_search",

      query,
      h
    )
      .toPromise()
    return { ...response.data.hits }
  }



  async getResumeResourceByIdentPager(arrIdent: String[], from, size): Promise<object> {
    let query =
    {
      "from": from, "size": size,
      "_source": ["document.ident", "document.identifier", "document.type", "document.metadata.firstLevel.description", "document.metadata.firstLevel.title", "document.metadata.firstLevel.creator", "document.metadata.firstLevel.temporalCoverage", "document.metadata.firstLevel.geographicCoverage", "document.metadata.missionLevel"],
      "query": {
        "bool": {
          "filter": [
            { "terms": { "document.ident": arrIdent } }
          ]
        }
      },
      "sort": [
        "document.ident"
      ]
    }
    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }

    let response = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.museo + "-" + this.config.elastic.index.env + "/_search",

      query,
      h
    )
      .toPromise()
    return { ...response.data.hits }
  }





  async saveHistory(createSearchHistoryDto: CreateSearchHistoryDto): Promise<SearchHistory> {
    const createdSearchHistory = new this.searchHistoryModel(createSearchHistoryDto);
    return createdSearchHistory.save();
  }

  async loadHistory(id: String, searchHistory: any): Promise<SearchHistory> {
    return this.searchHistoryModel.findOne({
      user: id, filters: searchHistory.filters
    }).exec()
  }

  async deleteHistory(id: String): Promise<any> {
    return this.searchHistoryModel.deleteOne({
      _id: id
    }).exec()
  }

  async read(id: String): Promise<[]> {
    return this.searchHistoryModel.find({
      user: id
    }).exec()
  }

  async updateTotal(id: String, total: Number): Promise<void | any> {
    return this.searchHistoryModel.findOneAndUpdate({ "_id": id }, { "total": total }).exec()
  }

  async getDataHistorySearch(username: String): Promise<[]> {
    let json =
    {
      "query": {
        "bool": {
          "must": [
            {
              "term": {
                "user.keyword": {
                  "value": username
                }
              }
            }
          ],
          "must_not": [
            { "term": { "params.body.filters.keyword.keyword": "" } }
          ]
        }
      },
      "size": 1000
    }
    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
    let response = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.logger + "-search-" + this.config.elastic.index.env + "/_search",
      json,
      h
    ).toPromise().then()

    return response.data;
  }

  async getDataViewSearch(username: String): Promise<[]> {
    let json =
    {
      "query": {
        "bool": {
          "must": [
            { "match": { "user.keyword": username } },
            {
              "exists": { "field": "result.ident.keyword" }
            }
          ],
          "must_not": [
            { "term": { "result.ident.keyword": "" } }
          ]
        }
      },
      "size": 1000
    }
    let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }

    let response = await this.http.post(
      this.config.elastic.url + this.config.elastic.index.logger + "-view-" + this.config.elastic.index.env + "/_search",
      json,
      h
    ).toPromise().then()
    return response.data;
  }



  




}
