import { Module, HttpModule } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';

import { SearchController } from './search.controller';
import { SearchService } from './search.service';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { SearchHistorySchema } from './schema/search-history.schema'
import { LocationModule } from '../admin/location/location.module';
import { LocationService } from '../admin/location/location.service';
import { Location, LocationSchema } from '../location/schema/location.schema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'SearchHistory', schema: SearchHistorySchema }], "database"),
    MongooseModule.forFeature([{ name: 'Location', schema: LocationSchema, collection: 'geo-locations' }], "database"),
    LocationModule,
    ConfigModule,
    HttpModule, 
    PassportModule.register({ defaultStrategy: 'jwt' }),
  ],
  controllers: [SearchController],
  providers: [SearchService, ConfigService, LocationService],
  exports:[SearchService],
})
export class SearchModule {}
