export class CreateSearchHistoryDto {
  readonly user: String;
  readonly filters: Object;
  readonly total: String;
  readonly extra: Object;
}

