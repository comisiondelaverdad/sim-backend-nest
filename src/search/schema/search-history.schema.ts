import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({collection: 'search-history'})
export class SearchHistory extends Document {
  @Prop({type:"string"})
  user: String;

  @Prop({type:"object"})
  filters: Object;
  
  @Prop({type:"string"})
  total: String;

  @Prop({type:"object"})
  extra: Object;
}

export const SearchHistorySchema = SchemaFactory.createForClass(SearchHistory);