import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Logger extends Document {
  @Prop({type:"boolean"})
  active: boolean;
}


export const LoggerSchema = SchemaFactory.createForClass(Logger);