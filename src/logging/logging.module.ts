import { Module } from '@nestjs/common';
import { LoggingService } from './logging.service';
import { LoggingController } from './logging.controller';
import {LoggerSchema} from './schema/Logger.schema'
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports:[
    MongooseModule.forFeature([{ name: 'Logger', schema: LoggerSchema }], "mongologs")
  ],
  controllers: [LoggingController],
  providers: [LoggingService]
})
export class LoggingModule {}
