import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateLoggerDto } from './dto/create-logger.dto';
import { UpdateLoggerDto } from './dto/update-logger.dto';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import * as winston from 'winston';
import { Logger } from './schema/Logger.schema';

import { WinstonModule, utilities as nestWinstonModuleUtilities } from 'nest-winston';

require('winston-mongodb');

const transports: any = winston.transports;
const config = new ConfigService();

@Injectable()
export class LoggingService {
  private transports: any = winston.transports;
  private config = new ConfigService();

  private errorStackTracerFormatError = winston.format((info) => {
    if (info.stack) {
      info.message = `${info.message} -stack- ${info.stack}`;
    }
    return info;
  });
  private errorStackTracerFormatInfo = winston.format((info) => {
    if (info.stack) {
      info.message = `${info.message} -stack- ${info.stack}`;
    }
     
    if(info.level === 'info' && info.context != 'RoutesResolver' && info.context != 'RouterExplorer'){
      return info
    }
 
  });



  private opciones = {};
  constructor(@InjectModel('Logger') private loggerModel: Model<Logger>,
  ) {}



  async mongoOpts() {
    return {
      silent: !!JSON.parse(
        String(this.config.get('LOGGING_PROCESS_SILENT')).toLowerCase(),
      ),
      level: 'error',
      db: this.config.get('MONGODB_URI_LOGS'),
      collection: 'errors',      
      metaKey: 'meta',
      options: { useUnifiedTopology: true },
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.prettyPrint(),
        this.errorStackTracerFormatError(),
        winston.format.metadata(),
      ),
    };
  }
  async mongoOptsLogin() {

    return {     
      level: 'info',
      db: this.config.get('MONGODB_URI_LOGS'),
      collection: 'login',      
      metaKey: 'meta',
      capped:true,
      options: { useUnifiedTopology: true },
      format: winston.format.combine(
        winston.format.timestamp(),        
        this.errorStackTracerFormatInfo(),
        winston.format.metadata(),
      ),
    };
  }

  async active(): Promise<any> {
    const result = await this.findAll();
    if (result.length) return result[0].active;
    return false;
  }

  async options(): Promise<any> {
    return (this.opciones = {
      meta: true,
      exitOnError: false,
      transports: [
        new this.transports.MongoDB(await this.mongoOpts()),
        new this.transports.MongoDB(await this.mongoOptsLogin()),
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.simple(),
            winston.format.ms(),
            nestWinstonModuleUtilities.format.nestLike(),
          ),
          handleExceptions: true,
        }),
      ],
      exceptionHandlers: [
        new transports.MongoDB({
          db: this.config.get('MONGODB_URI_LOGS'),
          collection: 'exceptions',
          capped: true,
          metaKey: 'meta',
          options: { useUnifiedTopology: true },
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.prettyPrint(),
            winston.format.metadata(),
          ),
        }),
      ],
    });
  }

  async findAll(): Promise<Logger[]> {
    return await this.loggerModel.find();
  }

  async create(): Promise<Logger> {
    let createLoggerDto: CreateLoggerDto;
    let createdLogger = new this.loggerModel(createLoggerDto);
    createdLogger.active = true;

    return await createdLogger.save();
  }

  async update(id: string, updateLoggerDto: UpdateLoggerDto) {
    return await this.loggerModel.findOneAndUpdate(
      { _id: id },
      updateLoggerDto,
      { new: true },
    );
  }

  async creationLogger() {
    return WinstonModule.createLogger(await this.options());
  }
}
