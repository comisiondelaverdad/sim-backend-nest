import {
  Controller,
  Get,
  Put, 
  Body,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { LoggingService } from './logging.service';
import { CreateLoggerDto } from './dto/create-logger.dto';
import { UpdateLoggerDto } from './dto/update-logger.dto';
import { AuthGuard } from '@nestjs/passport';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('logging')
@Controller('api/logging')
export class LoggingController {
  constructor(private readonly loggingService: LoggingService) {}

  @Get()
  // @UseGuards(AuthGuard('jwt'))
  findAll() {
    return this.loggingService.findAll();
  }
  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateLoggerDto: UpdateLoggerDto,
  ) {
    const result = await this.loggingService.findAll();
    if (!result.length) {
      return await this.loggingService.create();
    }
    return await this.loggingService.update(id, updateLoggerDto);
  }
}
