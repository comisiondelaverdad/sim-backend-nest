import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { NotificationService } from "./notificaction.service"

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('notifications')
@Controller('api/notifications')
export class NotificationController {
    constructor(
        private readonly notificationsService: NotificationService
    ){}

    @Get(":id")
    @UseGuards(AuthGuard())
    async getUserNotifications(@Param('id') id): Promise<any> {
        return await this.notificationsService.getUserNotifications(id);
    }

}