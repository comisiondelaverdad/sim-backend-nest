import { Injectable } from '@nestjs/common';
import { SearchService } from '../search/search.service';

@Injectable()
export class NotificationService {

    constructor(
        private searchService : SearchService
    ) {}

    async getUserNotifications(id:string): Promise<any>{
        let userSearchHistory = await this.searchService.read(id);

        let notifications = await Promise.all(userSearchHistory.map(async (userSearch:any)=>{
            let body = {
                filters : userSearch.filters
            };
            let query = {
                q : userSearch.filters.keyword
            };
            let size = 0;

            let searchForChanges = await this.searchService.count({body,query,size})
                .then((x)=>{
                    return x.data ? x.data.count : 0;
                })
                .catch((err)=>{
                    console.log(err.response.data.error);
                    return 0
                })

            let newResults = 0;
            let totalNewResults = userSearch.total;
            if(parseInt(searchForChanges,10) > parseInt(userSearch.total)){
                newResults = parseInt(searchForChanges,10) - parseInt(userSearch.total);
                totalNewResults = parseInt(searchForChanges,10);
                await this.searchService.updateTotal(userSearch._id,totalNewResults);
            }
            
            return {
                id: userSearch._id,
                user: userSearch.user,
                filters: userSearch.filters,
                newResults: newResults,
                oldResults: userSearch.total,
                totalNewResults: totalNewResults 
            }
        }));

        function compare(a:any, b:any) {
            const newA = parseInt(a.newResults,10);
            const newB = parseInt(b.newResults,10);
            let comparison = 0;
            if(newA < newB) {
                comparison = 1;
            } 
            else if(newA > newB) {
                comparison = -1;
            }
            return comparison;
        }

        return notifications.sort(compare);
    }

}