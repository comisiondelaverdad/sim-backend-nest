import { Module, HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NotificationController } from './notificaction.controller';
import { NotificationService } from './notificaction.service';
import { SearchService } from '../search/search.service';
import { SearchHistorySchema } from '../search/schema/search-history.schema';
import { ConfigModule } from '../config/config.module';
import { LocationModule } from '../admin/location/location.module';
import { LocationService } from '../admin/location/location.service';
import { Location, LocationSchema } from '../location/schema/location.schema';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'SearchHistory', schema: SearchHistorySchema }], "database"),
    MongooseModule.forFeature([{ name: 'Location', schema: LocationSchema, collection: 'geo-locations' }], "database"),
    LocationModule,
    HttpModule,
    ConfigModule,
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [NotificationController],
  providers: [NotificationService, SearchService, LocationService],
  exports: [NotificationService, MongooseModule]
})
export class NotificationModule { }
