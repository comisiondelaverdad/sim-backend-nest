import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document , Types} from 'mongoose';
import { Resource } from 'src/resource/schema/resource.schema';
import { User } from 'src/user/schema/user.schema';

@Schema()
export class CollectionBookmark extends Document {
    @Prop( {type: Types.ObjectId, ref: 'User'})
    user: User;

    @Prop( {type: Types.ObjectId, ref: 'Reource'})
    resource_id: Resource;

    @Prop({type:"string"})
    resource_ident: String;
 
    @Prop({ type: Date, default: Date.now(),format: 'date-time' })
    createdAt: Date;
}

export const CollectionBookmarkSchema = SchemaFactory.createForClass(CollectionBookmark);