import { Test, TestingModule } from '@nestjs/testing';
import { CollectionBookmarkService } from './collection-bookmark.service';

describe('CollectionBookmarkService', () => {
  let service: CollectionBookmarkService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CollectionBookmarkService],
    }).compile();

    service = module.get<CollectionBookmarkService>(CollectionBookmarkService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
