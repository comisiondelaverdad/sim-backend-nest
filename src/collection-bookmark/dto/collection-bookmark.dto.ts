
import { IsMongoId, IsString} from 'class-validator';
import { Types } from 'mongoose';
export class CollectionBookmarkDto {
    @IsMongoId()
    readonly user: Types.ObjectId;

    @IsString()
    readonly resource_ident: String;

  }