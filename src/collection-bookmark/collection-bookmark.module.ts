import { Module, HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { CollectionBookmarkController } from './collection-bookmark.controller';
import { CollectionBookmarkService } from './collection-bookmark.service';
import { CollectionBookmarkSchema } from './schema/collection-bookmark';
import { SearchModule } from '../search/search.module';
import { SearchService } from '../search/search.service';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { LocationModule } from '../admin/location/location.module';
import { LocationService } from '../admin/location/location.service';
import { SearchHistorySchema } from '../search/schema/search-history.schema';
import { Location, LocationSchema } from '../location/schema/location.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'SearchHistory', schema: SearchHistorySchema }], "database"),
    MongooseModule.forFeature([{ name: 'Location', schema: LocationSchema, collection: 'geo-locations' }], "database"),
    MongooseModule.forFeature([{ name: 'CollectionBookmark', schema: CollectionBookmarkSchema }], "database"),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    SearchModule,
    ConfigModule,
    LocationModule,
    HttpModule,
  ],
  controllers: [CollectionBookmarkController],
  providers: [CollectionBookmarkService,SearchService, LocationService, ConfigService],
  exports:[CollectionBookmarkService]

})
export class CollectionBookmarkModule {}
