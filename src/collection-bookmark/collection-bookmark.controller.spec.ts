import { Test, TestingModule } from '@nestjs/testing';
import { CollectionBookmarkController } from './collection-bookmark.controller';

describe('CollectionBookmarkController', () => {
  let controller: CollectionBookmarkController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CollectionBookmarkController],
    }).compile();

    controller = module.get<CollectionBookmarkController>(CollectionBookmarkController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
