import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import * as _ from "lodash"
import { CollectionBookmark} from './schema/collection-bookmark';
import { CollectionBookmarkDto } from './dto/collection-bookmark.dto';
import { SearchService } from '../search/search.service';

@Injectable()
export class CollectionBookmarkService {

    constructor(   private searchService: SearchService,
      @InjectModel('CollectionBookmark') private collectionBookmarkModel: Model<CollectionBookmark>) {     
    }
    
     
    async create(collectionBookmarkDto: CollectionBookmarkDto): Promise<CollectionBookmark> { 
      const numDocs  =  await this.collectionBookmarkModel.countDocuments({user: collectionBookmarkDto.user,resource_ident: collectionBookmarkDto.resource_ident}).exec()  
        if (numDocs==0){
            const createdCollectionBookmark = new this.collectionBookmarkModel(collectionBookmarkDto);  
            createdCollectionBookmark.createdAt = new Date(Date.now()); 
            return createdCollectionBookmark.save();
        }else{
            return  this.collectionBookmarkModel.findOne({user: collectionBookmarkDto.user,resource: collectionBookmarkDto.resource_ident}).exec()           
        }    
    }

    async delete(id: String): Promise<CollectionBookmark> {
      return this.collectionBookmarkModel.deleteOne({"_id":id}).exec();
    }

    async deleteUserResource(userId: String,ident: String): Promise<CollectionBookmark> {
      return this.collectionBookmarkModel.deleteOne({"user":userId,"resource_ident":ident}).exec();
    }

    async getbyId(id: String): Promise<CollectionBookmark> {
        return this.collectionBookmarkModel.findOne({"_id":id}).exec();
      }
  
    async idsByUser(id): Promise<String[]> {
      return this.collectionBookmarkModel.find({user:  id}).exec()
          .then((collectionBookmarks)=>{
            let collectionBookmarksId = _.flatMap(collectionBookmarks, "id") 
            return _.uniq(collectionBookmarksId).sort()
          })
    }


    async resourceIdentByUser(id): Promise<String[]> {
      return this.collectionBookmarkModel.find({user:  id}).exec()
          .then((collectionBookmarks)=>{
            let collectionBookmarksId = _.flatMap(collectionBookmarks, "resource_ident") 
            return _.uniq(collectionBookmarksId).sort()
          })
    }


    async resourceByUser(id): Promise<Object> {
        const arrIdent =  await this.resourceIdentByUser(id);        
        const resources = await this.searchService.getResourceByIdent(arrIdent); 
        return resources 
    }

    
    async resourceByUserPage(body): Promise<Object> {
    
      const arrIdent =  await this.resourceIdentByUser(body.user);        
      const resources = await this.searchService.getResourceByIdentPager(arrIdent,body.from,body.size,body.typeResource); //{"user":body.user,"from":body.from, "size":body.size, "filters":body.filters}// 
      return resources 
  }



}
