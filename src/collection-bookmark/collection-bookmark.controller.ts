

import { Post, Delete, Get, Param, Controller, Body, Query,Request } from '@nestjs/common';

import { CollectionBookmarkDto } from './dto/collection-bookmark.dto';
import { CollectionBookmarkService } from './collection-bookmark.service';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('collection-bookmark')
@Controller('api/collection-bookmark')
export class CollectionBookmarkController {

  constructor(private readonly collectionBookmarkService: CollectionBookmarkService) { }
  @Post()
  async create(@Body() CollectionBookmarkDto: CollectionBookmarkDto) {
    return await this.collectionBookmarkService.create(CollectionBookmarkDto);
  }

  @Delete(':id')
  async delete(@Param('id') id) {
    return await this.collectionBookmarkService.delete(id);
  }


  @Delete('user/:userId/resource-ident/:ident')
  async deleteUserResource(@Param('userId') userId, @Param('ident') ident) {
    return await this.collectionBookmarkService.deleteUserResource(userId, ident);
  }


  @Get(':id')
  async get(@Param('id') id) {
    return await this.collectionBookmarkService.getbyId(id);
  }


  @Get('resource-ident/byuser/:id')
  async resourceIdentByUser(@Param('id') id) {
    return await this.collectionBookmarkService.resourceIdentByUser(id);
  }


  @Get('resource/byuser/:id')
  async resourceByUser(@Param('id') id) {
    return await this.collectionBookmarkService.resourceByUser(id);
  }

  @Post('resource/byuser/filter')
  async search(@Body() body, @Query() query, @Request() req): Promise<any> {
    return await this.collectionBookmarkService.resourceByUserPage(body);
  }


  @Get('byuser/:id')
  async idsByUser(@Param('id') id) {
    return await this.collectionBookmarkService.idsByUser(id);
  }




}
