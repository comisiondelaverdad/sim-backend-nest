import { Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AccessRequest } from './schema/access-request.schema';
import { AccessRequestHistory } from '../access-request-history/schema/access-request-history.schema';
import { AccessRequestExtension } from '../access-request-extension/schema/access-request-extension.schema';
import { User } from '../user/schema/user.schema';
import { Resource } from '../resource/schema/resource.schema';
import { ResourceGroup } from './../resource-group/schema/resource-group.schema';
import _ from 'lodash';
import { RolesGuard } from 'src/auth/roles.guard';
@Injectable()
export class AccessRequestService {

  constructor(
    @InjectModel('AccessRequest') private accessRequest: Model<AccessRequest>,
    @InjectModel('AccessRequestHistory') private accessRequestHistoryModel: Model<AccessRequestHistory[]>,
    @InjectModel('AccessRequestExtension') private accessRequestExtensionModel: Model<AccessRequestExtension[]>,
    @InjectModel('User') private userModel: Model<User>,
    @InjectModel('Resource') private resourceModel: Model<Resource>,
    @InjectModel('ResourceGroup') private resourceGroupModel: Model<ResourceGroup>,
  ) { }

  async findByUser(id): Promise<void | AccessRequest[]> {
    return this.accessRequest.find({
      user: id
    })
      .populate(
        {
          path: 'user',
          model: this.userModel
        }
      )
      .populate(
        {
          path: 'history',
          model: this.accessRequestHistoryModel
        }
      )
      .populate(
        {
          path: 'extensions',
          model: this.accessRequestExtensionModel
        }
      )
      .populate(
        {
          path: 'resource',
          model: this.resourceModel
        }
      )
      .exec()
  }

  async get(ident): Promise<AccessRequest> {
    return this.accessRequest.findOne({
      ident: ident
    })
      .populate(
        {
          path: 'user',
          model: this.userModel
        }
      )
      .populate(
        {
          path: 'history',
          model: this.accessRequestHistoryModel
        }
      )
      .populate(
        {
          path: 'extensions',
          model: this.accessRequestExtensionModel
        }
      )
      .populate(
        {
          path: 'resource',
          model: this.resourceModel
        }
      )
      .exec()
  }

  async put(ident, ar): Promise<AccessRequest[]> {
    delete ar._id
    return this.accessRequest.findOneAndUpdate({ ident: ident }, ar, { new: true })
      .exec()
  }

  async delete(ident): Promise<any> {
    return this.accessRequest.deleteOne({ ident: ident })
      .exec()
  }

  async list(user: any): Promise<AccessRequest[]> {
    const query = user ? { user: user._id } : {};
    let result = await this.accessRequest.find(query)
      .populate(
        {
          path: 'user',
          model: this.userModel,
          select: 'name username _id',
        }
      )
      .populate(
        {
          path: 'history',
          model: this.accessRequestHistoryModel,
          select: 'ident date status',
        }
      )
      .populate(
        {
          path: 'extensions',
          model: this.accessRequestExtensionModel
        }
      )
      .populate(
        {
          path: 'resource',
          model: this.resourceModel,
          select: 'metadata.firstLevel.title type identifier ident metadata.firstLevel.accessLevel metadata.firstLevel.creator',
        }
      )
      .exec()
    result = result.sort(function (a, b) {
      if (a.status === 'pending' && b.status !== 'pending') {
        return -1;
      }
      if (a.status !== 'pending' && b.status === 'pending') {
        return 1;
      }
      return a.ident - b.ident;
    });

    // const resourcegroups = result.map((resourcegroups) => {
    //   return resourcegroups ? resourcegroups.resource ? resourcegroups.resource.identifier ? resourcegroups.resource.identifier : null : null : null;
    // })
    // const uniqueResourcegroups = resourcegroups.filter((value, index, self) => {
    //   return (self.indexOf(value) === index && value);
    // })
    // let resourceg = await Promise.all(uniqueResourcegroups.map((rg) => {
    //   return this.resourceGroupModel.findOne({ 'ResourceGroupId': rg }, '_id metadata.firstLevel.title ident').exec()
    // }))

    // //console.log(JSON.stringify(resourceg, null, "\t"));

    // result.forEach((element) => {
    //   if (element.resource) {
    //     if (element.resource.identifier) {
    //       element.resource['extra'] = resourceg.filter((rg: any) => {
    //         if (rg) {
    //           return rg.ident === element.resource.identifier
    //         } else {
    //           return false
    //         }
    //       })[0]
    //     }
    //   }
    // });

    return result;
  }

  async create(post): Promise<AccessRequest> {
    return this.accessRequest.create(post)
  }
}

