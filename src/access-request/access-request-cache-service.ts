import { Injectable } from '@nestjs/common';
import { BehaviorSubject, interval } from 'rxjs';
import { UtilService } from 'src/util/util.service';
import { AccessRequestService } from './access-request.service';
import { AccessRequest } from './schema/access-request.schema';

@Injectable()
export class AccessRequestCacheService {

	private listAccessRequestSubject = new BehaviorSubject([]);
	private listAccessRequest$ = this.listAccessRequestSubject.asObservable();
	private listAccessRequestCache: AccessRequest[];

	constructor(
		private readonly utilsService: UtilService,
		private readonly accessRequestService: AccessRequestService
	){  
	}

	async init() {
		this.accessRequestService.list(false).then((response)=>{
		this.listAccessRequestSubject.next(response);
		});
		const timer = interval(3600000); // cache update 2 hour
		timer.subscribe(async (x)=> {
			const listAccessRequestCache = await this.accessRequestService.list(false);
			this.listAccessRequestSubject.next(listAccessRequestCache);
		})

		this.listAccessRequest$.subscribe((response: AccessRequest[]) => {
			this.listAccessRequestCache = response;
		})
	}

	listCache(): any {
		let listAccessRequestPromise = new Promise((resolve) => {
            if (this.listAccessRequestCache) {
                resolve(this.listAccessRequestCache)
            } else {
                this.listAccessRequest$.subscribe((response: any) => {
					if (response.length > 0) {
						resolve(response);
					}
				})
            }
        })
        return listAccessRequestPromise;
	}
}
