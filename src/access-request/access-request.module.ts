import { forwardRef, Module, HttpModule } from '@nestjs/common';
import { getConnectionToken, MongooseModule } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import * as AutoIncrementFactory from 'mongoose-sequence';
import { AccessRequestController } from './access-request.controller';
import { AccessRequestSchema } from './schema/access-request.schema';
import { AccessRequestService } from './access-request.service';
import { AccessRequestCacheService } from './access-request-cache-service';
import { AccessRequestHistoryModule } from '../access-request-history/access-request-history.module';
import { AccessRequestExtensionModule } from '../access-request-extension/access-request-extension.module';
import { UserModule } from '../user/user.module';
import { UsersService } from '../user/user.service';
import { UtilModule } from '../util/util.module';
import { UtilService } from '../util/util.service';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { RecordModule } from '../record/record.module';
import { ResourceGroupModule } from '../resource-group/resource-group.module';
import { ResourceGroupService } from '../resource-group/resource-group.service';
import { ResourceModule } from '../resource/resource.module';
import { ResourceService } from '../resource/resource.service';
import { PassportModule } from '@nestjs/passport';
import { LogsService } from 'src/logs/logs.service';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'AccessRequest',
        useFactory: async (connection: Connection) => {
          const schema = AccessRequestSchema;
          const AutoIncrement = AutoIncrementFactory(connection);
          // eslint-disable-next-line @typescript-eslint/camelcase
          schema.plugin(AutoIncrement, { id: 'ident_access-request', inc_field: 'ident' });
          return schema;
        },
        inject: [getConnectionToken("database")]
      }
    ], "database"),
    HttpModule,
    UtilModule,
    ConfigModule,
    AccessRequestHistoryModule,
    AccessRequestExtensionModule,
    ResourceModule,
    RecordModule,
    ResourceGroupModule,
    forwardRef(() => UserModule),
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [AccessRequestController],
  providers: [AccessRequestService, AccessRequestCacheService, UtilService, UsersService, ConfigService, ResourceGroupService, LogsService],
  exports: [AccessRequestService, MongooseModule]
})
export class AccessRequestModule {}