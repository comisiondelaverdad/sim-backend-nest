import { Controller, Get, Param, Body, Post, Put, Delete, Request, UseGuards, Query } from '@nestjs/common';
import { Types } from 'mongoose';
import _ from 'lodash';

import { AccessRequest } from './schema/access-request.schema';
import { AccessRequestDto } from './schema/access-request.dto'

import { AccessRequestService } from './access-request.service';
import { UtilService } from '../util/util.service';
import { UsersService } from '../user/user.service';
import { ResourceService } from '../resource/resource.service';
import { ResourceGroupService } from '../resource-group/resource-group.service';

import { ConfigService } from '../config/config.service';
import { AuthGuard } from '@nestjs/passport';
import { AccessRequestCacheService } from './access-request-cache-service';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('access-requests')
@Controller('api/access-requests')
export class AccessRequestController {

  constructor(
    private readonly accessRequestService: AccessRequestService,
    private readonly accessRequestCacheService: AccessRequestCacheService,
    private readonly resourceService: ResourceService,
    private readonly resourceGroupService: ResourceGroupService,
    private readonly utilService: UtilService,
    private readonly usersService: UsersService,
    private config: ConfigService,
  ) {
    //this.accessRequestCacheService.init();
    console.log('Inicializando accessRequest caché ...')

  }
  /*
  @Get(':id/user')
  async accessRequestByUser(@Param() params): Promise<AccessRequest[]> {
    return this.accessRequestService.findByUser(params.id);
  }
  */

  @Get('/all')
  @UseGuards(AuthGuard())
  async list(@Request() req): Promise<AccessRequest[]> {
    return await this.accessRequestService.list(false)
  }

  @Get('/my')
  @UseGuards(AuthGuard())
  async mylist(@Request() req, @Query("ident") ident): Promise<any> {
    let list = await this.accessRequestService.list(req.user)
    let actual = await this.resourceService.getByIdSimple(req, ident)
    return { "accessRequest": list, "actual": actual }
  }

  @Get('/users')
  async users() {
    let users = await this.usersService.find({ roles: { $in: ["reviewer"] } }).then()
    let emails = users.map(u => { return u.username })
    console.log(users);
    return emails

  }

  // Create AccessRequestHistory
  @Post()
  @UseGuards(AuthGuard())
  async create(@Body() ar: AccessRequestDto): Promise<AccessRequest> {
    let accessR = JSON.parse(JSON.stringify(ar));
    if (typeof accessR.user === "string") {
      accessR.user = new Types.ObjectId(accessR.user)
    }

    let resource = await this.resourceService.getByResource(ar.resource).then()
    let resourceGroup = await this.resourceGroupService.findOne(resource["ResourceGroupId"]).then()

    let users = await this.usersService.find({ roles: { $in: ["reviewer"] } }).then()
    let emails = users.map(u => { return u.username })

    let ar_new = await this.accessRequestService.create(accessR)
    let requestUsers = await this.usersService.find({ _id: ar_new.user }).then()

    let to = emails
    let subject = "SIM [" + resource["ident"] + "] Nueva solicitud de acceso " + ar_new["ident"] + "."
    let body = "Se creó una nueva solicitud de acceso al recurso " + resource["ident"] + ".<br/><br/>" +
      "<b>Recurso:</b> " + resource["ident"] + ", " + resource["metadata"]["firstLevel"]["title"] + ".<br/>" +
      "<b>Nivel de acceso:</b> " + resource["metadata"]["firstLevel"]["accessLevel"] + ".<br/>" +
      "<b>Fondo:</b> " + resource["ResourceGroupId"] + ", " + resourceGroup["metadata"]["firstLevel"]["title"] + ".<br/>" +
      "<b>Solicitante:</b> " + requestUsers[0].name + " - " + requestUsers[0].username + ".<br/>" +
      "<b>Justificación:</b> " + ar_new.description + ".<br/>" +
      "<b>Fechas sugerida por el usuario:</b> " + ("" + ar.requestedFrom).substring(0, 10) + " a " + ("" + ar.requestedTo).substring(0, 10) + "<br/>" +
      "<a href='" + this.config.get("URL_BASE") + "/access-requests/" + ar_new["ident"] + "'>Ver solicitud de acceso</a>." +
      "<br/>"
    let email = await this.utilService.sendEmail(to, subject, body).then()
      .catch((err) => {
        console.log("Error enviando el email", err)
      })
    //console.log("email: ", email)


    return ar_new
  }

  @Get(":ident")
  @UseGuards(AuthGuard())
  async get(@Param("ident") ident): Promise<AccessRequest> {
    return this.accessRequestService.get(ident)
  }

  @Put(":ident")
  @UseGuards(AuthGuard())
  async put(@Param("ident") ident, @Body() ar: AccessRequestDto, @Request() req): Promise<AccessRequest[]> {

    let ar_org = await this.accessRequestService.get(ident).then()

    //console.log("Original: ", ar_org)
    //console.log("Nuevo: ", ar)

    let to = []
    let subject = ""
    let body = ""

    let resource = await this.resourceService.getByResource(ar.resource).then()
    let resourceGroup = await this.resourceGroupService.findOne(resource["ResourceGroupId"]).then()

    if (ar_org.status === "pending") {


      let users = await this.usersService.find({ roles: { $in: ["approver"] } }).then()
      let emails = users.map(u => { return u.username })
      let requestUsers = await this.usersService.find({ _id: ar.user }).then()

      //console.log("Enviando respuesta de correo")
      to = emails
      subject = "SIM [" + ar_org.resource["ident"] + "] Nueva solicitud de acceso pre-aprobada " + ar.ident + "."
      body = "Una solicitud de acceso " + ar.ident + " al recurso " +
        ar_org.resource["ident"] +
        " está pendiente para ser aprobada o rechazada.<br/><br/>" +
        "<b>Recurso:</b> " + resource["ident"] + ", " + resource["metadata"]["firstLevel"]["title"] + ".<br/>" +
        "<b>Nivel de acceso:</b> " + resource["metadata"]["firstLevel"]["accessLevel"] + ".<br/>" +
        "<b>Fondo:</b> " + resource["ResourceGroupId"] + ", " + resourceGroup["metadata"]["firstLevel"]["title"] + ".<br/>" +
        "<b>Solicitante:</b> " + requestUsers[0].name + " - " + requestUsers[0].username + ".<br/>" +
        "<b>Justificación:</b> " + ar.description + ".<br/>" +
        "<b>Fechas sugerida por el usuario:</b> " + ("" + ar.requestedFrom).substring(0, 10) + " a " + ("" + ar.requestedTo).substring(0, 10) + "<br/>" +
        "<a href='" + this.config.get("URL_BASE") + "/access-requests/" + ar["ident"] + "'>Ver solicitud de acceso</a>." +
        "<br/>"
      let email = await this.utilService.sendEmail(to, subject, body).then()
        .catch((err) => {
          console.log("Error enviando el email", err)
        })


    }


    to = []
    subject = "SIM [" + ar_org.resource["ident"] + "] Actualización solicitud de acceso " + ar.ident + "."
    body = ""


    if (ar_org.status === "validated") {
      //console.log("Enviando respuesta de correo")
      to = [ar_org.user["username"]]
      body = "Su solicitud de acceso " + ar.ident + " al recurso " +
        ar_org.resource["ident"] +
        " cambió su estado a <b>" +
        (ar.status === "approved" ? "Aprobado" : "Rechazado") +
        "</b>.<br/>" +

        "<b>Recurso:</b> " + resource["ident"] + ", " + resource["metadata"]["firstLevel"]["title"] + ".<br/>" +
        "<b>Fondo:</b> " + resource["ResourceGroupId"] + ", " + resourceGroup["metadata"]["firstLevel"]["title"] + ".<br/>" +
        "<b>Rango de fechas:</b> " + ("" + ar.requestedFrom).substring(0, 10) + " a " + ("" + ar.requestedTo).substring(0, 10) + "<br/>" +

        "<br/>" +
        "<a href='" + this.config.get("URL_BASE") + "/access-requests/" + ar["ident"] + "'>Ver solicitud de acceso</a>." +
        "<br/>"
      let email = await this.utilService.sendEmail(to, subject, body).then()
        .catch((err) => {
          console.log("Error enviando el email", err)
        })


      let sendDiffConcept = false

      if (ar.status === "approved") {
        if (ar_org.history[1]["description"].startsWith("No se pre-aprueba")) {
          subject = "SIM [" + ar_org.resource["ident"] + "] Diferencia concepto solicitud de acceso " + ar.ident + "."
          body = "La solicitud de acceso " + ar.ident + " al recurso " +
            ar_org.resource["ident"] +
            " tiene una diferencia de concepto. Fue <b>no pre-aprobada</b> y posteriormente fue <b>Aprobada</b>."

          sendDiffConcept = true
        }
      }
      if (ar.status !== "approved") {
        if (!ar_org.history[1]["description"].startsWith("No se pre-aprueba")) {
          subject = "SIM [" + ar_org.resource["ident"] + "] Diferencia concepto solicitud de acceso " + ar.ident + "."
          body = "La solicitud de acceso " + ar.ident + " al recurso " +
            ar_org.resource["ident"] +
            " tiene una diferencia de concepto. Fue <b>pre-aprobada</b> y posteriormente fue <b>Rechazada</b>."

          sendDiffConcept = true
        }
      }

      if (sendDiffConcept) {
        let user = await this.usersService.getUser(ar_org.history[1]["user"]).then()
        //console.log("El usuario preaprobador es: ", user)
        to = [user.username]
        let email = await this.utilService.sendEmail(to, subject, body).then()
          .catch((err) => {
            console.log("Error enviando el email", err)
          })
      }


    }


    return this.accessRequestService.put(ident, ar)
  }

  @Delete(":ident")
  @UseGuards(AuthGuard())
  async delete(@Param("ident") ident): Promise<AccessRequest[]> {
    return this.accessRequestService.delete(ident)
  }

}
