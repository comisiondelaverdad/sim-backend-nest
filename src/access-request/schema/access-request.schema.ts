import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

@Schema({collection:"access-requests"})
export class AccessRequest extends Document {

  @Prop({type:"string"})
  user: {type: Types.ObjectId, ref: 'User'};
  
  @Prop({type:"array"})
  extensions: Array<{type: Types.ObjectId, ref: 'AccessRequestExtension'}>;

  @Prop({type:"array"})
  history: Array<{type: Types.ObjectId, ref: 'AccessRequestHistory'}>;

  @Prop({type:"string"})
  description: string;

  @Prop({type:"boolean"})
  userAgreement: boolean;

  @Prop({type:"Date"})
  requestDate: Date;

  @Prop({type:"Date"})
  requestedFrom: Date;

  @Prop({type:"Date"})
  requestedTo: Date;

  @Prop({type:"Date"})
  grantedDate: Date;

  @Prop({type:"string"})
  grantedBy: {type: Types.ObjectId, ref: 'User'};

  @Prop({type:"Date"})
  grantedFrom: Date;

  @Prop({type:"Date"})
  grantedTo: Date;

  @Prop({type:"Date"})
  rejectionDate: Date;

  @Prop({type:"Date"})
  preApproveDate: Date;

  @Prop({type:"boolean"})
  preApproveStatus: boolean;

  @Prop({type:"string"})
  preApproveComment: string;

  @Prop({type:"string"})
  status: string;

  @Prop({type:"string"})
  resource: {type: Types.ObjectId, ref: 'Resource'}
  //resource: Types.ObjectId; //{type: 'ObjectId', ref: 'Resource'}
   
    
}


export const AccessRequestSchema = SchemaFactory.createForClass(AccessRequest);


