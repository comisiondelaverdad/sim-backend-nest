import { IsInt, IsString, IsObject, IsBoolean, IsDate, IsMongoId, IsArray } from 'class-validator';
import { Document, Types } from 'mongoose';

export class AccessRequestDto {
  @IsMongoId()
  readonly user: Types.ObjectId;

  @IsArray()
  @IsMongoId()
  readonly extensions: [Types.ObjectId];

  @IsArray()
  @IsMongoId()
  readonly history: [Types.ObjectId];
  
  @IsString()
  readonly description: string;

  @IsBoolean()
  readonly useAgreement: boolean;

  @IsDate()
  readonly requestDate: Date;

  @IsDate()
  readonly requestedFrom: Date;

  @IsDate()
  readonly requestedTo: Date;
  
  @IsString()
  readonly status: string;
  
  @IsMongoId()
  readonly resource: Types.ObjectId;
  
  @IsBoolean()
  readonly formValidated: boolean;

  @IsString()
  readonly accessLevel: string;
  
  @IsBoolean()
  readonly showTermsOfService: boolean;

  @IsString()
  readonly ident: string;

}

