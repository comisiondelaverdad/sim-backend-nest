import { Controller, Get, Query} from '@nestjs/common';
import { TestingService } from './testing.service';
import { Resource } from '../resource/schema/resource.schema';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('testing')
@Controller('api/testing')
export class TestingController {

  constructor(private readonly TestingService: TestingService) {}

  @Get("resources/getLimitedResources")
  async getLimitedListResources(@Query('limit') limit): Promise<Resource[]> {
    return this.TestingService.getLimitedListResources(limit);
  }

  @Get("resources/getLimitedbyTypeResources")
  async getLimitedbyTypeResources(@Query() query): Promise<Resource[]> {
    return this.TestingService.getLimitedbyTypeResources(query.limit,query.type);
  }

}
