import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TestingController } from './testing.controller';
import { TestingService } from './testing.service';
import { ResourceModule } from '../resource/resource.module';
import { RecordModule } from '../record/record.module';


@Module({
  imports: [
  	ResourceModule,
  	RecordModule
  	],
  controllers: [TestingController],
  providers: [TestingService],
})
export class TestingModule {}
