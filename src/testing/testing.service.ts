import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Resource } from '../resource/schema/resource.schema';
import { Record } from '../record/schema/record.schema';

@Injectable()
export class TestingService {
  constructor(
  	@InjectModel('Resource') private resourceModel: Model<Resource>,
  	@InjectModel('Record') private recordModel: Model<Record>	
  	){}

  async getLimitedListResources(limit): Promise<Resource[]> {
    return this.resourceModel.find().limit(Number(limit)).exec()
   
  }

  async getLimitedbyTypeResources(limit,type): Promise<Resource[]> {
    return this.resourceModel.find({type: type})
    .populate({
       path: 'records',
       model: this.recordModel})
     
    .limit(Number(limit)).exec()
   
  }

}
