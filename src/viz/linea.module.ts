import { Module, HttpModule } from '@nestjs/common';
import { LineaController } from './linea.controller';
import { LineaService } from './linea.service';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    ConfigModule,
    HttpModule, 
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [LineaController],
  providers: [LineaService, ConfigService],
  exports: [LineaService]
})
export class LineaModule { }