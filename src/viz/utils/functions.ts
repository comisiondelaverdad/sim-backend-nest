export function agregarFiltrosTexto(filtros, json): any {

    // FILTROS por nodo de ngram
    if (filtros.ngramFilter !== undefined) {
        if (filtros.ngramFilter.length > 0) {
            let words = filtros.ngramFilter

            words.map(w => {
                let path = "record.labelled_for_viz." + w.gram + ".label.keyword"
                let str = '*' + w.nodo + '*'
                let filter = {
                    "nested": {
                        "path": "record.labelled_for_viz." + w.gram,
                        "query": {
                            "wildcard": {
                                [path]: str
                            }
                        }
                    }
                }

                json.query.bool.must.push(filter)
            })
        }
    }

    // FILTROS por etiquetas
    if (filtros.etiquetasFilter !== undefined) {
        if (filtros.etiquetasFilter.length > 0) {
            let words = filtros.etiquetasFilter

            words.map(w => {
                let str = w.name
                let filter = {
                    "nested": {
                        "path": "record.labelled_for_viz.etiquetas",
                        "query": {
                            "term": {
                                "record.labelled_for_viz.etiquetas.label.keyword": str
                            }
                        }
                    }
                }

                json.query.bool.must.push(filter)
            })
        }
    }

    // FILTROS por entidades
    if (filtros.entidadesFilter !== undefined) {
        if (filtros.entidadesFilter.length > 0) {
            let words = filtros.entidadesFilter

            words.map(w => {
                let str = w
                let filter = {
                    "nested": {
                        "path": "record.labelled_for_viz.entidades",
                        "query": {
                            "term": {
                                "record.labelled_for_viz.entidades.text.keyword": str
                            }
                        }
                    }
                }

                json.query.bool.must.push(filter)
            })
        }
    }

    // Bookmarks
    if (filtros.idArray !== undefined) {
        filtros.idArray.map(a => {
            json['query']['bool']['should'].push({
                "term": {
                    "extra.identifier.keyword": {
                        "value": a
                    }
                }
            })
        })
    }

    // Resource Group
    if (filtros.rg !== undefined) {
        json['query']['bool']['must'].push({
            "term": {
                "document.record.identifier_II.keyword": {
                    "value": filtros.rg
                }
            }
        })
    }

    json['query']['bool']['must_not'].push({
        "term": {
          "document.record.extra.status.keyword": {
            "value": "deleted"
          }
        }
      })

    return json
}