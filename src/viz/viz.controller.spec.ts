import { Test, TestingModule } from '@nestjs/testing';
import { VizController } from './viz.controller';

describe('Viz Controller', () => {
  let controller: VizController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VizController],
    }).compile();

    controller = module.get<VizController>(VizController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
