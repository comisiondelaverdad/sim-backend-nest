import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document , Types } from 'mongoose';

@Schema()
export class Linea extends Document {
  @Prop( {type: Types.ObjectId})
  _id: string;
  
  @Prop({type:"string"})
  base: string;
  
  @Prop({type:"string"})
  titulo: string;
  
  @Prop({type:"string"})
  descripcion: string;
  
  @Prop({type:"string"})
  tema_principal: string;
  
  @Prop({type:"string"})
  fecha_inicio: string;
  
  @Prop({type:"string"})
  fecha_fin: string;
  
  @Prop({type:"string"})
  fecha_inicio_cl: string;
  
  @Prop({type:"string"})
  fecha_fin_cl: string;
  
  @Prop({type:"string"})
  frecuencia: string;
  
  @Prop({type:"string"})
  pais: string;
  
  @Prop({type:"string"})
  departamento: string;
  
  @Prop({type:"string"})
  subregion: string;
  
  @Prop({type:"string"})
  MacroT: string;
  
  @Prop({type:"string"})
  Topónimo: string;
  
  @Prop({type:"string"})
  territorio_colectivo: string;
  
  @Prop({type:"string"})
  barrio: string;
  
  @Prop({type:"string"})
  direccion_o_nombre_del_lugar: string;

  @Prop({type:"string"})
  municipio: string;
  
  @Prop({type:"string"})
  fuente_de_informacion: string;
  
  @Prop({type:"string"})
  presunto_responsable_directo: string;
  
  @Prop({type:"string"})
  detalle_responsable_directo: string;
  
  @Prop({type:"string"})
  detalle_responsable_directo_2: string;
  
  @Prop({type:"string"})
  presunto_responsable_directo_2: string;
  
  @Prop({type:"string"})
  x: string;
  
  @Prop({type:"string"})
  y: string;
  
  @Prop({type:"string"})
  yy_fecha_inicio_cl: string;
  
  @Prop({type:"string"})
  mm_fecha_inicio_cl: string;
  
  @Prop({type:"string"})
  dd_fecha_inicio_cl: string;
  
  @Prop({type:"string"})
  yy_fecha_fin_cl: string;
  
  @Prop({type:"string"})
  mm_fecha_fin_cl: string;
  
  @Prop({type:"string"})
  dd_fecha_fin_cl: string;

}

export const LineaSchema = SchemaFactory.createForClass(Linea);


