import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Localidad extends Document {
  @Prop()
    properties: Object

    @Prop()
    centroid: Object

    @Prop()
    geometry: Object

    @Prop()
    admin_level: Number
}


export const LocalidadSchema = SchemaFactory.createForClass(Localidad);

