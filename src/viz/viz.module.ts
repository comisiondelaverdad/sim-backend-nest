import { Module, HttpModule } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { VizController } from './viz.controller';
import { VizService } from './viz.service';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SearchHistorySchema } from '../search/schema/search-history.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'SearchHistory', schema: SearchHistorySchema }], "database"),
    ConfigModule,
    HttpModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
  ],
  controllers: [VizController],
  providers: [VizService, ConfigService],
})
export class VizModule { }
