export class LineaDto {
    _id?: string;
    readonly num: number;
    readonly base: string;
    readonly titulo: string;
    readonly descripcion: string;
    readonly tema_principal: string;
    readonly fecha_inicio: Date;
    readonly fecha_fin: Date;
    readonly frecuencia: string;
    readonly pais: string;
    readonly departamento: string;
    readonly municipio: string;
    readonly fuente_de_informacion: string;
  }
  
  