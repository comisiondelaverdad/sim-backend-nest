import { HttpService } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import { InjectModel } from '@nestjs/mongoose';
import { SearchHistory } from '../search/schema/search-history.schema';
import { Model } from 'mongoose';
import { agregarFiltrosTexto } from './utils/functions';
import { departamentos } from '../geo-shapes/departamentos';
import { municipios } from '../geo-shapes/municipios';
import * as https from 'https';
import * as fs from 'fs';

export class VizService {
  config: any = undefined;
  httpsAgent = null;

  constructor(
    private http: HttpService,
    private configX: ConfigService,
    @InjectModel('SearchHistory')
    private searchHistoryModel: Model<SearchHistory>,
  ) {
    let crtFile = fs.readFileSync(configX.get('ELASTIC_AUTH_CERT'));
    let agent = new https.Agent({ ca: crtFile });
    this.httpsAgent = agent;
    this.config = {
      elastic: {
        url: configX.get('ELASTIC_URL'),
        index: {
          data: configX.get('ELASTIC_INDEX_DATA'),
          logger: configX.get('ELASTIC_INDEX_LOGGER'),
          env: configX.get('ELASTIC_INDEX_ENV'),
          analyzer: configX.get('ELASTIC_ANALYZER'),
          museo: configX.get('ELASTIC_INDEX_MUSEO'),
          dpto: configX.get('ELASTIC_DPTO'),
          mpio: configX.get('ELASTIC_MPIO'),
        },
        headers: {
          auth: {
            username: configX.get('ELASTIC_AUTH_USER'),
            password: configX.get('ELASTIC_AUTH_PASS'),
          },
        },
      },
    };
  }

  /**
   * Devuelve el máximo de resultados para una query busqueda
   * @param body
   */
  async totalSearchHits(body): Promise<any> {
    let params = body;

    let json = {
      _source: {
        excludes: ['resourceGroup.resources', 'resource.records'],
      },
      track_total_hits: true,
      query: {
        bool: {
          filter: [],
          must: [],
          should: [],
          must_not: [
            {
              term: {
                'document.record.extra.status.keyword': {
                  value: 'deleted',
                },
              },
            },
            { term: { 'record.type.keyword': 'Audio de la entrevista' } },
            { term: { 'record.type.keyword': 'Consentimiento informado' } },
            { term: { 'record.type.keyword': 'Ficha corta' } },
            { term: { 'record.type.keyword': 'Ficha larga' } },
            { term: { 'record.type.keyword': 'Transcripción preliminar' } },
            {
              term: {
                'record.type.keyword':
                  'Archivos de transcripcion (otranscribe)',
              },
            },
          ],
        },
      },
    };

    json['size'] = 10;

    if (params.page !== undefined) {
      json['from'] = (params.page - 1) * 10;
    }

    if (params.filters !== undefined) {
      if (
        params.filters.keyword !== undefined &&
        params.filters.keyword !== ''
      ) {
        let keyword = params.filters.keyword;
        if (params.filters.keyword?.indexOf(':') !== -1) {
          // REEMPLAZAR | por OR y + por AND
          keyword = keyword.replace(' | ', ' OR ');
          keyword = keyword.replace(' + ', ' AND ');
          json.query.bool.must.push({
            query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        } else {
          // REEMPLAZAR OR por | y AND por +
          keyword = keyword.replace(' OR ', ' | ');
          keyword = keyword.replace(' AND ', ' + ');
          json.query.bool.must.push({
            simple_query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        }
      }

      if (params.filters.resourceGroup.resourceGroupIDS !== '') {
        json.query.bool.filter.push({
          terms: {
            'resourceGroup.resourceGroupId.keyword':
              params.filters.resourceGroup.resourceGroupIDS.split(','),
          },
        });
      }
    }

    if (params.origin !== undefined) {
      json.query.bool.filter.push({
        match_phrase: { 'record.origin.keyword': params.origin },
      });
    }

    json = agregarFiltrosTexto(params, json);

    json['aggs'] = {
      conflictActors: {
        terms: {
          field: 'record.conflictActors.keyword',
          size: 20,
        },
      },
      aproaches: {
        terms: {
          field: 'record.approaches.keyword',
          size: 51,
        },
      },
      macroterritorio: {
        terms: {
          field: 'record.macroterritorio.keyword',
          size: 16,
        },
      },
      violencia: {
        terms: {
          field: 'record.violenceType.keyword',
          size: 20,
        },
      },
      territorio: {
        terms: {
          field: 'record.territorio.keyword',
          size: 100,
        },
      },
    };

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };

    return this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.data +
        '-' +
        this.config.elastic.index.env +
        '/_search',
        json,
        h,
      )
      .toPromise();
  }

  /**
   * Devuelve el mapping del indice
   */
  async mapping(): Promise<any> {
    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    return this.http
      .get(
        this.config.elastic.url +
        this.config.elastic.index.data +
        '-' +
        this.config.elastic.index.env +
        '/_mapping',
        h,
      )
      .toPromise();
  }

  /**
   * Servicio que devuelve la agregación de las entidades
   * al usarse en los bookmarks se le pasa un array idArray con los path de los documentos
   * @param body
   */
  async grafoEntidades(body): Promise<any> {
    let params = body;

    let json = {
      _source: {
        excludes: ['resourceGroup.resources', 'resource.records'],
      },
      query: {
        bool: {
          filter: [],
          must: [],
          should: [],
          must_not: [
            {
              term: {
                'document.record.extra.status.keyword': {
                  value: 'deleted',
                },
              },
            },
            { term: { 'record.type.keyword': 'Audio de la entrevista' } },
            { term: { 'record.type.keyword': 'Consentimiento informado' } },
            { term: { 'record.type.keyword': 'Ficha corta' } },
            { term: { 'record.type.keyword': 'Ficha larga' } },
            { term: { 'record.type.keyword': 'Transcripción preliminar' } },
            {
              term: {
                'record.type.keyword':
                  'Archivos de transcripcion (otranscribe)',
              },
            },
          ],
        },
      },
    };

    json['size'] = 1;

    if (params.filters !== undefined) {
      if (
        params.filters.keyword !== undefined &&
        params.filters.keyword !== ''
      ) {
        let keyword = params.filters.keyword;
        if (params.filters.keyword?.indexOf(':') !== -1) {
          // REEMPLAZAR | por OR y + por AND
          keyword = keyword.replace(' | ', ' OR ');
          keyword = keyword.replace(' + ', ' AND ');
          json.query.bool.must.push({
            query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        } else {
          // REEMPLAZAR OR por | y AND por +
          keyword = keyword.replace(' OR ', ' | ');
          keyword = keyword.replace(' AND ', ' + ');
          json.query.bool.must.push({
            simple_query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        }
      }

      if (params.filters.resourceGroup.resourceGroupIDS !== '') {
        json.query.bool.filter.push({
          terms: {
            'resourceGroup.resourceGroupId.keyword':
              params.filters.resourceGroup.resourceGroupIDS.split(','),
          },
        });
      }
    }

    // json.query.bool.must.push({"exists": {"field": "record.labelled_for_viz"}});

    json['aggs'] = {
      tipo: {
        nested: {
          path: 'record.labelled_for_viz.connections',
        },
        aggs: {
          connections: {
            filter: {
              nested: {
                path: 'record.labelled_for_viz.connections',
                query: {
                  bool: {
                    must: [],
                  },
                },
              },
            },
            aggs: {
              total: {
                terms: {
                  size: 500,
                  field: 'record.labelled_for_viz.connections.label.keyword',
                },
              },
            },
          },
        },
      },
    };

    if (params.gruposEntidades !== undefined) {
      params.gruposEntidades.map((g) => {
        json['aggs']['tipo']['aggs']['connections']['filter']['nested'][
          'query'
        ]['bool']['should'].push({
          match: {
            'record.labelled_for_viz.connections.tipo1.keyword': g,
          },
        });
        json['aggs']['tipo']['aggs']['connections']['filter']['nested'][
          'query'
        ]['bool']['should'].push({
          match: {
            'record.labelled_for_viz.connections.tipo2.keyword': g,
          },
        });
      });
    }

    if (params.origin !== undefined) {
      json.query.bool.filter.push({
        match_phrase: { 'record.origin.keyword': params.origin },
      });
    }

    // FILTRO en la agregación por conexiones de entidades
    if (params.entidadFilter !== undefined) {
      let str =
        '*' +
        params.entidadFilter.group +
        '||' +
        params.entidadFilter.name +
        '*';
      let filter = {
        wildcard: {
          'record.labelled_for_viz.connections.label.keyword': str,
        },
      };

      json['aggs']['tipo']['aggs']['connections']['filter']['nested']['query'][
        'bool'
      ]['must'].push(filter);
    }

    json = agregarFiltrosTexto(params, json);

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    let resp: any = await this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.data +
        '-' +
        this.config.elastic.index.env +
        '/_search?scroll=1m',
        json,
        h,
      )
      .toPromise();

    return resp;
  }

  /**
   * Servicio que devuelve la agregación de las etiquetas
   * al usarse en los bookmarks se le pasa un array idArray con los path de los documentos
   * @param body
   */
  async getEtiquetas(body): Promise<any> {
    let params = body;

    let json = {
      _source: {
        excludes: ['resourceGroup.resources', 'resource.records'],
      },
      query: {
        bool: {
          filter: [],
          must: [],
          should: [],
          must_not: [
            {
              term: {
                'document.record.extra.status.keyword': {
                  value: 'deleted',
                },
              },
            },
            { term: { 'record.type.keyword': 'Audio de la entrevista' } },
            { term: { 'record.type.keyword': 'Consentimiento informado' } },
            { term: { 'record.type.keyword': 'Ficha corta' } },
            { term: { 'record.type.keyword': 'Ficha larga' } },
            { term: { 'record.type.keyword': 'Transcripción preliminar' } },
            {
              term: {
                'record.type.keyword':
                  'Archivos de transcripcion (otranscribe)',
              },
            },
          ],
        },
      },
    };

    json['size'] = 1;

    if (params.filters !== undefined) {
      if (
        params.filters.keyword !== undefined &&
        params.filters.keyword !== ''
      ) {
        let keyword = params.filters.keyword;
        if (params.filters.keyword?.indexOf(':') !== -1) {
          // REEMPLAZAR | por OR y + por AND
          keyword = keyword.replace(' | ', ' OR ');
          keyword = keyword.replace(' + ', ' AND ');
          json.query.bool.must.push({
            query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        } else {
          // REEMPLAZAR OR por | y AND por +
          keyword = keyword.replace(' OR ', ' | ');
          keyword = keyword.replace(' AND ', ' + ');
          json.query.bool.must.push({
            simple_query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        }
      }

      if (params.filters.resourceGroup.resourceGroupIDS !== '') {
        json.query.bool.filter.push({
          terms: {
            'resourceGroup.resourceGroupId.keyword':
              params.filters.resourceGroup.resourceGroupIDS.split(','),
          },
        });
      }
    }

    // json.query.push({"exists": {"field": "record.labelled_for_viz"}});

    json['aggs'] = {
      tipo: {
        nested: {
          path: 'record.labelled_for_viz.etiquetas',
        },
        aggs: {
          etiquetas: {
            terms: {
              size: 500,
              field: 'record.labelled_for_viz.etiquetas.label.keyword',
            },
            aggs: {
              value: {
                avg: {
                  field: 'record.labelled_for_viz.etiquetas.value',
                },
              },
            },
          },
        },
      },
    };

    if (params.origin !== undefined) {
      json.query.bool.filter.push({
        match_phrase: { 'record.origin.keyword': params.origin },
      });
    }

    json = agregarFiltrosTexto(params, json);

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    let resp: any = await this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.data +
        '-' +
        this.config.elastic.index.env +
        '/_search?scroll=1m',
        json,
        h,
      )
      .toPromise();

    return resp;
  }

  /**
   * Servicio que devuelve la agregación para la nube de entidades y la visualizacion de metadatos
   * Al pasarle el type como true hace la agregación en las entidades si type!==undefined entonces hace la agregacion en el campo de metadatos especificado en type
   * al usarse en los bookmarks se le pasa un array idArray con los path de los documentos
   * @param body
   */
  async tagCloud(body): Promise<any> {
    let params = body;

    let json = {
      _source: {
        excludes: ['resourceGroup.resources', 'resource.records'],
      },
      query: {
        bool: {
          filter: [],
          must: [],
          should: [],
          must_not: [
            {
              term: {
                'document.record.extra.status.keyword': {
                  value: 'deleted',
                },
              },
            },
            { term: { 'record.type.keyword': 'Audio de la entrevista' } },
            { term: { 'record.type.keyword': 'Consentimiento informado' } },
            { term: { 'record.type.keyword': 'Ficha corta' } },
            { term: { 'record.type.keyword': 'Ficha larga' } },
            { term: { 'record.type.keyword': 'Transcripción preliminar' } },
            {
              term: {
                'record.type.keyword':
                  'Archivos de transcripcion (otranscribe)',
              },
            },
          ],
        },
      },
    };

    json['size'] = 1;

    if (params.origin !== undefined && params.origin != '') {
      json.query.bool.filter.push({
        match_phrase: { 'record.origin.keyword': params.origin },
      });
    }

    if (params.filters !== undefined) {
      if (
        params.filters.keyword !== undefined &&
        params.filters.keyword !== ''
      ) {
        let keyword = params.filters.keyword;
        if (params.filters.keyword?.indexOf(':') !== -1) {
          // REEMPLAZAR | por OR y + por AND
          keyword = keyword.replace(' | ', ' OR ');
          keyword = keyword.replace(' + ', ' AND ');
          json.query.bool.must.push({
            query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        } else {
          // REEMPLAZAR OR por | y AND por +
          keyword = keyword.replace(' OR ', ' | ');
          keyword = keyword.replace(' AND ', ' + ');
          json.query.bool.must.push({
            simple_query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        }
      }

      if (params.filters.resourceGroup.resourceGroupIDS !== '') {
        json.query.bool.filter.push({
          terms: {
            'resourceGroup.resourceGroupId.keyword':
              params.filters.resourceGroup.resourceGroupIDS.split(','),
          },
        });
      }
    }

    if (params.type) {
      if (params.type === true) {
        json['aggs'] = {
          tipo: {
            nested: {
              path: 'record.labelled_for_viz.entidades',
            },
            aggs: {
              cloud: {
                filter: {
                  nested: {
                    path: 'record.labelled_for_viz.entidades',
                    query: {
                      bool: {
                        must: [],
                      },
                    },
                  },
                },
                aggs: {
                  total: {
                    terms: {
                      size: 100,
                      field: 'record.labelled_for_viz.entidades.text.keyword',
                    },
                  },
                },
              },
            },
          },
        };
      } else {
        json['aggs'] = {
          tipo: {
            terms: {
              field: 'record.' + params.type + '.keyword',
              size: 100,
            },
          },
        };
      }
    }
    if (params.min && params.max) {
      json.query.bool.must.push({
        range: {
          'record.temporalCoverage.start': { gte: params.min, lte: params.max },
        },
      });
    }

    if (params.gruposEntidades !== undefined) {
      params.gruposEntidades.map((g) => {
        json['aggs']['tipo']['aggs']['cloud']['filter']['nested']['query'][
          'bool'
        ]['must'].push({
          match: {
            'record.labelled_for_viz.entidades.label.keyword': g,
          },
        });
      });
    }

    json = agregarFiltrosTexto(params, json);

    console.log(JSON.stringify(json));

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    let resp: any = await this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.data +
        '-' +
        this.config.elastic.index.env +
        '/_search',
        json,
        h,
      )
      .toPromise();

    return resp;
  }

  /**
   * Servicio que devuelve la agregación de las conexiones que tengan uno de los parametros de entidad que se le pasan
   * al usarse en los bookmarks se le pasa un array idArray con los path de los documentos
   */
  async getLineaTiempo(body): Promise<any> {
    let params = body;
    let json = {
      _source: {
        excludes: ['resourceGroup.resources', 'resource.records'],
      },
      query: {
        bool: {
          filter: [],
          must: [],
          should: [],
          must_not: [
            {
              term: {
                'document.record.extra.status.keyword': {
                  value: 'deleted',
                },
              },
            },
            { term: { 'record.type.keyword': 'Audio de la entrevista' } },
            { term: { 'record.type.keyword': 'Consentimiento informado' } },
            { term: { 'record.type.keyword': 'Ficha corta' } },
            { term: { 'record.type.keyword': 'Ficha larga' } },
            { term: { 'record.type.keyword': 'Transcripción preliminar' } },
            {
              term: {
                'record.type.keyword':
                  'Archivos de transcripcion (otranscribe)',
              },
            },
          ],
        },
      },
    };

    json['size'] = 1;

    if (params.filters !== undefined) {
      if (
        params.filters.keyword !== undefined &&
        params.filters.keyword !== ''
      ) {
        let keyword = params.filters.keyword;
        if (params.filters.keyword?.indexOf(':') !== -1) {
          // REEMPLAZAR | por OR y + por AND
          keyword = keyword.replace(' | ', ' OR ');
          keyword = keyword.replace(' + ', ' AND ');
          json.query.bool.must.push({
            query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        } else {
          // REEMPLAZAR OR por | y AND por +
          keyword = keyword.replace(' OR ', ' | ');
          keyword = keyword.replace(' AND ', ' + ');
          json.query.bool.must.push({
            simple_query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        }
      }

      if (params.filters.resourceGroup.resourceGroupIDS !== '') {
        json.query.bool.filter.push({
          terms: {
            'resourceGroup.resourceGroupId.keyword':
              params.filters.resourceGroup.resourceGroupIDS.split(','),
          },
        });
      }
    }

    json.query.bool.must.push({ exists: { field: 'record.labelled_for_viz' } });

    json['aggs'] = {
      tipo: {
        nested: {
          path: 'record.labelled_for_viz.connections',
        },
        aggs: {
          connections: {
            filter: {
              nested: {
                path: 'record.labelled_for_viz.connections',
                query: {
                  bool: {
                    should: [
                      {
                        match: {
                          'record.labelled_for_viz.connections.tipo1.keyword':
                            'Fecha',
                        },
                      },
                      {
                        match: {
                          'record.labelled_for_viz.connections.tipo2.keyword':
                            'Fecha',
                        },
                      },
                    ],
                  },
                },
              },
            },
            aggs: {
              total: {
                terms: {
                  size: 100,
                  field: 'record.labelled_for_viz.connections.label.keyword',
                },
              },
            },
          },
        },
      },
    };

    if (params.ngramFilter !== undefined) {
      if (params.ngramFilter.length > 0) {
        let words = params.ngramFilter;

        words.map((w) => {
          let path = 'record.labelled_for_viz.' + w.gram + '.label.keyword';
          let str = '*' + w.nodo + '*';
          let filter = {
            nested: {
              path: 'record.labelled_for_viz.' + w.gram,
              query: {
                wildcard: {
                  [path]: str,
                },
              },
            },
          };

          json.query.bool.must.push(filter);
        });
      }
    }

    json = agregarFiltrosTexto(params, json);

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    let resp: any = await this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.data +
        '-' +
        this.config.elastic.index.env +
        '/_search',
        json,
        h,
      )
      .toPromise();

    return resp;
  }

  /**
   * Servicio que devuelve los lugares de entidades con sus coordenadas
   */

  async mapaVizEntidades(body): Promise<any> {
    let params = body;

    let json = {
      _source: {
        excludes: ['resourceGroup.resources', 'resource.records'],
      },
      query: {
        bool: {
          filter: [],
          must: [],
          should: [],
          must_not: [
            {
              term: {
                'document.record.extra.status.keyword': {
                  value: 'deleted',
                },
              },
            },
            { term: { 'record.type.keyword': 'Audio de la entrevista' } },
            { term: { 'record.type.keyword': 'Consentimiento informado' } },
            { term: { 'record.type.keyword': 'Ficha corta' } },
            { term: { 'record.type.keyword': 'Ficha larga' } },
            { term: { 'record.type.keyword': 'Transcripción preliminar' } },
            {
              term: {
                'record.type.keyword':
                  'Archivos de transcripcion (otranscribe)',
              },
            },
          ],
        },
      },
    };

    json['size'] = 1;

    if (params.origin) {
      json.query.bool.filter.push({
        match_phrase: { 'record.origin.keyword': params.origin },
      });
    }

    if (params.filters !== undefined) {
      if (
        params.filters.keyword !== undefined &&
        params.filters.keyword !== ''
      ) {
        let keyword = params.filters.keyword;
        if (params.filters.keyword?.indexOf(':') !== -1) {
          // REEMPLAZAR | por OR y + por AND
          keyword = keyword.replace(' | ', ' OR ');
          keyword = keyword.replace(' + ', ' AND ');
          json.query.bool.must.push({
            query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        } else {
          // REEMPLAZAR OR por | y AND por +
          keyword = keyword.replace(' OR ', ' | ');
          keyword = keyword.replace(' AND ', ' + ');
          json.query.bool.must.push({
            simple_query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        }
      }

      if (params.filters.resourceGroup.resourceGroupIDS !== '') {
        json.query.bool.filter.push({
          terms: {
            'resourceGroup.resourceGroupId.keyword':
              params.filters.resourceGroup.resourceGroupIDS.split(','),
          },
        });
      }
    }

    json['aggs'] = {
      tipo: {
        nested: {
          path: 'record.labelled_for_viz.entidades',
        },
        aggs: {
          cloud: {
            filter: {
              nested: {
                path: 'record.labelled_for_viz.entidades',
                query: {
                  bool: {
                    must: [
                      {
                        match: {
                          'record.labelled_for_viz.entidades.label.keyword':
                            'Divipola',
                        },
                      },
                      {
                        exists: {
                          field: 'record.labelled_for_viz.entidades.location',
                        },
                      },
                    ],
                  },
                },
              },
            },
            aggs: {
              total: {
                terms: {
                  size: 100,
                  field: 'record.labelled_for_viz.entidades.text.keyword',
                },
                aggs: {
                  centroid: {
                    geo_centroid: {
                      field: 'record.labelled_for_viz.entidades.location',
                    },
                  },
                },
              },
            },
          },
        },
      },
    };

    json = agregarFiltrosTexto(params, json);

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    let resp: any = await this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.data +
        '-' +
        this.config.elastic.index.env +
        '/_search',
        json,
        h,
      )
      .toPromise();

    return resp;
  }

  /**
   * Servicio que devuelve la agregación de las conexiones que tengan uno de los parametros de entidad que se le pasan
   * al usarse en los bookmarks se le pasa un array idArray con los path de los documentos
   * TODO: sim-data-processing agregar la coordenada de las entidades conectadas
   */
  async getEntidadesMapa(body): Promise<any> {
    let params = body;

    let json = {
      _source: {
        excludes: ['resourceGroup.resources', 'resource.records'],
      },
      query: {
        bool: {
          filter: [],
          must: [],
          should: [],
          must_not: [
            {
              term: {
                'document.record.extra.status.keyword': {
                  value: 'deleted',
                },
              },
            },
            { term: { 'record.type.keyword': 'Audio de la entrevista' } },
            { term: { 'record.type.keyword': 'Consentimiento informado' } },
            { term: { 'record.type.keyword': 'Ficha corta' } },
            { term: { 'record.type.keyword': 'Ficha larga' } },
            { term: { 'record.type.keyword': 'Transcripción preliminar' } },
            {
              term: {
                'record.type.keyword':
                  'Archivos de transcripcion (otranscribe)',
              },
            },
          ],
        },
      },
    };

    json['size'] = 1;
    if (params.filters !== undefined) {
      if (
        params.filters.keyword !== undefined &&
        params.filters.keyword !== ''
      ) {
        let keyword = params.filters.keyword;
        if (params.filters.keyword?.indexOf(':') !== -1) {
          // REEMPLAZAR | por OR y + por AND
          keyword = keyword.replace(' | ', ' OR ');
          keyword = keyword.replace(' + ', ' AND ');
          json.query.bool.must.push({
            query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        } else {
          // REEMPLAZAR OR por | y AND por +
          keyword = keyword.replace(' OR ', ' | ');
          keyword = keyword.replace(' AND ', ' + ');
          json.query.bool.must.push({
            simple_query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        }
      }

      if (params.filters.resourceGroup.resourceGroupIDS !== '') {
        json.query.bool.filter.push({
          terms: {
            'resourceGroup.resourceGroupId.keyword':
              params.filters.resourceGroup.resourceGroupIDS.split(','),
          },
        });
      }
    }

    // json.query.bool.must.push({"exists": {"field": "record.labelled_for_viz"}});

    json['aggs'] = {
      tipo: {
        nested: {
          path: 'record.labelled_for_viz.connections',
        },
        aggs: {
          connections: {
            filter: {
              nested: {
                path: 'record.labelled_for_viz.connections',
                query: {
                  bool: {
                    should: [
                      {
                        match: {
                          'record.labelled_for_viz.connections.tipo1.keyword':
                            'Divipola',
                        },
                      },
                      {
                        match: {
                          'record.labelled_for_viz.connections.tipo2.keyword':
                            'Divipola',
                        },
                      },
                    ],
                  },
                },
              },
            },
            aggs: {
              total: {
                terms: {
                  size: 100,
                  field: 'record.labelled_for_viz.connections.label.keyword',
                },
                aggs: {
                  centroid1: {
                    geo_centroid: {
                      field: 'record.labelled_for_viz.connections.location1',
                    },
                  },
                  centroid2: {
                    geo_centroid: {
                      field: 'record.labelled_for_viz.connections.location2',
                    },
                  },
                },
              },
            },
          },
        },
      },
    };

    json = agregarFiltrosTexto(params, json);

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    let resp: any = await this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.data +
        '-' +
        this.config.elastic.index.env +
        '/_search',
        json,
        h,
      )
      .toPromise();

    return resp;
  }

  /**
   * Servicio que devuelve la agregación de las conexiones que tengan uno de los parametros de entidad que se le pasan
   * al usarse en los bookmarks se le pasa un array idArray con los path de los documentos
   */
  async getNGram(body): Promise<any> {
    let params = body;

    let json = {
      _source: {
        excludes: ['resourceGroup.resources', 'resource.records'],
      },
      query: {
        bool: {
          filter: [],
          must: [],
          should: [],
          must_not: [
            {
              term: {
                'document.record.extra.status.keyword': {
                  value: 'deleted',
                },
              },
            },
            { term: { 'record.type.keyword': 'Audio de la entrevista' } },
            { term: { 'record.type.keyword': 'Consentimiento informado' } },
            { term: { 'record.type.keyword': 'Ficha corta' } },
            { term: { 'record.type.keyword': 'Ficha larga' } },
            { term: { 'record.type.keyword': 'Transcripción preliminar' } },
            {
              term: {
                'record.type.keyword':
                  'Archivos de transcripcion (otranscribe)',
              },
            },
          ],
        },
      },
    };

    json['size'] = 0;

    if (params.origin !== undefined) {
      json.query.bool.filter.push({
        match_phrase: { 'record.origin.keyword': params.origin },
      });
    }

    if (params.filters !== undefined) {
      if (
        params.filters.keyword !== undefined &&
        params.filters.keyword !== ''
      ) {
        let keyword = params.filters.keyword;
        if (params.filters.keyword?.indexOf(':') !== -1) {
          // REEMPLAZAR | por OR y + por AND
          keyword = keyword.replace(' | ', ' OR ');
          keyword = keyword.replace(' + ', ' AND ');
          json.query.bool.must.push({
            query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        } else {
          // REEMPLAZAR OR por | y AND por +
          keyword = keyword.replace(' OR ', ' | ');
          keyword = keyword.replace(' AND ', ' + ');
          json.query.bool.must.push({
            simple_query_string: {
              query: keyword,
              default_operator: 'and',
              analyzer: this.config.elastic.index.analyzer,
            },
          });
        }
      }

      if (params.filters.resourceGroup.resourceGroupIDS !== '') {
        json.query.bool.filter.push({
          terms: {
            'resourceGroup.resourceGroupId.keyword':
              params.filters.resourceGroup.resourceGroupIDS.split(','),
          },
        });
      }
    }

    // json.query.bool.must.push({"exists": {"field": "record.labelled_for_viz"}});
    if (params.ngram !== undefined) {
      json['aggs'] = {
        tipo: {
          nested: {
            path: 'record.labelled_for_viz.' + params.ngram,
          },
          aggs: {
            ngram: {
              filter: {
                nested: {
                  path: 'record.labelled_for_viz.' + params.ngram,
                  query: {
                    bool: {},
                  },
                },
              },
              aggs: {
                total: {
                  terms: {
                    size: 100,
                    field:
                      'record.labelled_for_viz.' +
                      params.ngram +
                      '.label.keyword',
                  },
                },

                // TODO: average con la frecuencia normalizada
              },
            },
          },
        },
      };
    }

    json = agregarFiltrosTexto(params, json);

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    let resp: any = await this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.data +
        '-' +
        this.config.elastic.index.env +
        '/_search',
        json,
        h,
      )
      .toPromise();

    return resp.data;
  }

  async getHistogramDates(body): Promise<any> {
    let params = body;
    //ToDo: Agregar body como parametros en la consulta si en el futuro se llega a necesitar
    let json = {
      size: 0,
      aggs: {
        resource_by_month: {
          date_histogram: {
            //"field": "record.temporalCoverage.start",
            //"field": "temporal.inicio",
            field: 'record.creationDate',
            calendar_interval: '1y',
          },
        } /*
        "query": {
            "bool":{
                "filter":[
                  {
                    "range":{
                      "record.temporalCoverage.start":{
                        "gte":"2000-01-01T00:00:00.000Z",
                        "lte":"2021-12-31T00:00:00.000Z",
                        "format":"strict_date_optional_time"
                        }
                      }
                    }
                  ],
                "must":[],
                "should":[],
                "must_not":[
                ]
            }
        }*/,
      },
    };
    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    let resp: any = await this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.data +
        '-' +
        this.config.elastic.index.env +
        '/_search',
        json,
        h,
      )
      .toPromise();
    return resp.data;
  }

  /**
   * Servicio que devuelve la agregación con la lista de las etiquetas
   */
  async getListaEtiquetas(): Promise<any> {
    let json = {
      _source: {
        excludes: ['resourceGroup.resources', 'resource.records'],
      },
      size: 0,
      query: {
        bool: {
          filter: [],
          must: [],
          should: [],
          must_not: [
            {
              term: {
                'document.record.extra.status.keyword': {
                  value: 'deleted',
                },
              },
            },
            { term: { 'record.type.keyword': 'Audio de la entrevista' } },
            { term: { 'record.type.keyword': 'Consentimiento informado' } },
            { term: { 'record.type.keyword': 'Ficha corta' } },
            { term: { 'record.type.keyword': 'Ficha larga' } },
            { term: { 'record.type.keyword': 'Transcripción preliminar' } },
            {
              term: {
                'record.type.keyword':
                  'Archivos de transcripcion (otranscribe)',
              },
            },
          ],
        },
      },
      aggs: {
        tipo: {
          nested: {
            path: 'record.labelled_for_viz.etiquetas',
          },
          aggs: {
            etiquetas: {
              terms: {
                size: 999,
                field: 'record.labelled_for_viz.etiquetas.label.keyword',
              },
              aggs: {
                value: {
                  avg: {
                    field: 'record.labelled_for_viz.etiquetas.value',
                  },
                },
              },
            },
          },
        },
      },
    };

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    let resp: any = await this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.data +
        '-' +
        this.config.elastic.index.env +
        '/_search',
        json,
        h,
      )
      .toPromise();

    return resp;
  }

  /**
   * Servicio que devuelve la agregación con la lista de las entidades
   */
  async getListaEntidades(): Promise<any> {
    let json = {
      _source: {
        excludes: ['resourceGroup.resources', 'resource.records'],
      },
      size: 1,
      query: {
        bool: {
          filter: [],
          must: [],
          should: [],
          must_not: [
            {
              term: {
                'document.record.extra.status.keyword': {
                  value: 'deleted',
                },
              },
            },
            { term: { 'record.type.keyword': 'Audio de la entrevista' } },
            { term: { 'record.type.keyword': 'Consentimiento informado' } },
            { term: { 'record.type.keyword': 'Ficha corta' } },
            { term: { 'record.type.keyword': 'Ficha larga' } },
            { term: { 'record.type.keyword': 'Transcripción preliminar' } },
            {
              term: {
                'record.type.keyword':
                  'Archivos de transcripcion (otranscribe)',
              },
            },
          ],
        },
      },
      aggs: {
        tipo: {
          nested: {
            path: 'record.labelled_for_viz.entidades',
          },
          aggs: {
            entidades: {
              terms: {
                size: 10,
                field: 'record.labelled_for_viz.entidades.label.keyword',
              },
              aggs: {
                valores: {
                  terms: {
                    size: 100,
                    field: 'record.labelled_for_viz.entidades.text.keyword',
                    min_doc_count: 10,
                  },
                },
              },
            },
          },
        },
      },
    };

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    let resp: any = await this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.data +
        '-' +
        this.config.elastic.index.env +
        '/_search',
        json,
        h,
      )
      .toPromise();

    return resp;
  }

  /**
   * Servicio que devuelve la agregacion por departamentos para el explora del museo
   */

  async getMuseoDptoMapa(params): Promise<any> {
    const query = params;

    let json = {
      _source: {
        excludes: [],
      },
      size: 0,
      query: {
        bool: {
          filter: [],
          must: [],
          should: [],
          must_not: [],
        },
      },
      track_total_hits: true,
    };

    if (query.q !== undefined && query.q !== '') {
      json.query.bool.must.push({
        query_string: {
          query: query.q,
          fields: [
            'document.metadata.firstLevel.title',
            'document.ident',
            'document.metadata.firstLevel.description',
            'document.metadata.firstLevel.geographicCoverage',
            'document.records.metadata.firstLevel.title',
          ],
        },
      });
    }

    if (query.tipo) {
      json.query.bool.must.push({
        nested: {
          path: 'document.records',
          query: {
            bool: {
              filter: [
                {
                  term: {
                    'document.records.support.keyword': {
                      value: query.tipo,
                    },
                  },
                },
              ],
            },
          },
        },
      });
    }

    if (query.temporalCoverage !== undefined) {
      json.query.bool.must.push({
        range: {
          'document.metadata.firstLevel.temporalCoverage.start': {
            gte: query.temporalCoverage.split('-')[0],
            lt: query.temporalCoverage.split('-')[1],
          },
        },
      });
      json.query.bool.must.push({
        range: {
          'document.metadata.firstLevel.temporalCoverage.end': {
            gte: query.temporalCoverage.split('-')[0],
            lt: query.temporalCoverage.split('-')[1],
          },
        },
      });
    }

    if (query.fondo) {
      
      if(query.fondo.length > 0){
        json.query.bool.filter.push({
          "terms": {
            "document.identifier.keyword": query.fondo
          }
        })
      }
    }

    if (query.idents) {
      json.query.bool.filter.push({
        terms: { 'document.ident': query.idents },
      });
    }

    json['aggs'] = {
      departamentos: {
        filters: {
          filters: {},
        },
      },
    };

    departamentos.forEach((element) => {
      json['aggs']['departamentos']['filters']['filters'][element.codigo] = {
        geo_shape: {
          'document.metadata.firstLevel.geographicCoverage.geoPoint': {
            indexed_shape: {
              index: this.config.elastic.index.dpto,
              id: element.id,
              path: 'geometry',
            },
            relation: 'intersects',
          },
        },
      };
    });

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    let resp: any = await this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.museo +
        '-' +
        this.config.elastic.index.env +
        '/_search',
        json,
        h,
      )
      .toPromise();

    return resp;
  }

  /**
   * Servicio que devuelve la agregacion por municipios por departamento para el explora del museo
   */

  async getMuseoMpioMapa(query): Promise<any> {
    let json = {
      _source: {
        excludes: [],
      },
      size: 0,
      query: {
        bool: {
          filter: [],
          must: [],
          should: [],
          must_not: [],
        },
      },
      track_total_hits: true,
    };

    if (query.q !== undefined && query.q !== '') {
      json.query.bool.must.push({
        query_string: {
          query: query.q,
          fields: [
            'document.metadata.firstLevel.title',
            'document.ident',
            'document.metadata.firstLevel.description',
            'document.metadata.firstLevel.geographicCoverage',
            'document.records.metadata.firstLevel.title',
          ],
        },
      });
    }

    if (query.tipo) {
      json.query.bool.must.push({
        nested: {
          path: 'document.records',
          query: {
            bool: {
              filter: [
                {
                  term: {
                    'document.records.support.keyword': {
                      value: query.tipo,
                    },
                  },
                },
              ],
            },
          },
        },
      });
    }

    if (query.temporalCoverage !== undefined) {
      json.query.bool.must.push({
        range: {
          'document.metadata.firstLevel.temporalCoverage.start': {
            gte: query.temporalCoverage.split('-')[0],
            lt: query.temporalCoverage.split('-')[1],
          },
        },
      });
      json.query.bool.must.push({
        range: {
          'document.metadata.firstLevel.temporalCoverage.end': {
            gte: query.temporalCoverage.split('-')[0],
            lt: query.temporalCoverage.split('-')[1],
          },
        },
      });
    }

    if (query.fondo) {
      
      if(query.fondo.length > 0){
        json.query.bool.filter.push({
          "terms": {
            "document.identifier.keyword": query.fondo
          }
        })
      }
    }

    if (query.idents) {
      json.query.bool.filter.push({
        terms: { 'document.ident': query.idents },
      });
    }

    if (query.dpto !== undefined) {
      json['aggs'] = {
        municipios: {
          filters: {
            filters: {},
          },
        },
      };

      municipios
        .filter((d) => d.dpto === query.dpto.divipola)
        .forEach((element) => {
          json['aggs']['municipios']['filters']['filters'][element.codigo] = {
            geo_shape: {
              'document.metadata.firstLevel.geographicCoverage.geoPoint': {
                indexed_shape: {
                  index: this.config.elastic.index.mpio,
                  id: element.id,
                  path: 'geometry',
                },
                relation: 'intersects',
              },
            },
          };
        });
    }

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    let resp: any = await this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.museo +
        '-' +
        this.config.elastic.index.env +
        '/_search',
        json,
        h,
      )
      .toPromise();

    return resp;
  }

  /**
   * Servicio que devuelve la agregacion por departamentos para el explora del museo
   */

  async getMuseoHistogram(params): Promise<any> {
    const query = params;

    console.log(JSON.stringify(params))

    let json = {
      _source: {
        excludes: [],
      },
      size: 0,
      query: {
        bool: {
          filter: [],
          must: [],
          should: [],
          must_not: [],
        },
      },
      track_total_hits: true,
    };

    if (query.q !== undefined && query.q !== '') {
      json.query.bool.must.push({
        query_string: {
          query: query.q,
          fields: [
            'document.metadata.firstLevel.title',
            'document.ident',
            'document.metadata.firstLevel.description',
            'document.metadata.firstLevel.geographicCoverage',
            'document.records.metadata.firstLevel.title',
          ],
        },
      });
    }

    if (query.tipo) {
      json.query.bool.must.push({
        nested: {
          path: 'document.records',
          query: {
            bool: {
              filter: [
                {
                  term: {
                    'document.records.support.keyword': {
                      value: query.tipo,
                    },
                  },
                },
              ],
            },
          },
        },
      });
    }

    if (query.temporalCoverage !== undefined) {
      json.query.bool.must.push({
        range: {
          'document.metadata.firstLevel.temporalCoverage.start': {
            gte: query.temporalCoverage.split('-')[0],
            lt: query.temporalCoverage.split('-')[1],
          },
        },
      });
      json.query.bool.must.push({
        range: {
          'document.metadata.firstLevel.temporalCoverage.end': {
            gte: query.temporalCoverage.split('-')[0],
            lt: query.temporalCoverage.split('-')[1],
          },
        },
      });
    }

    if (query.fondo) {
      
      if(query.fondo.length > 0){
        json.query.bool.filter.push({
          "terms": {
            "document.identifier.keyword": query.fondo
          }
        })
      }
    }

    if (query.idents) {
      json.query.bool.filter.push({
        terms: { 'document.ident': query.idents },
      });
    }

    if (query.dpto !== undefined) {
      const dep = departamentos.find(d => d.codigo === query.dpto.divipola)

      if (dep) {
        json.query.bool.filter.push({
          "geo_shape": {
            "document.metadata.firstLevel.geographicCoverage.geoPoint": {
              "indexed_shape": {
                "index": this.config.elastic.index.dpto,
                "id": dep.id,
                "path": "geometry"
              },
              "relation": "intersects"
            }
          }
        })
      }
    }

    json['aggs'] = {
      "years": {
        "date_histogram": {
          "field": "document.metadata.firstLevel.temporalCoverage.start",
          "calendar_interval": "1y"
        }
      }
    }

    let h = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };



    let resp: any = await this.http
      .post(
        this.config.elastic.url +
        this.config.elastic.index.museo +
        '-' +
        this.config.elastic.index.env +
        '/_search',
        json,
        h,
      )
      .toPromise();


    return resp;
  }

  /**
   * Servicio que devuelve la agregacion por un metadato específico para la dataviz de Metadatos
   * @param body
   */

  async aggregateMetadataBy(body) {
    const { fieldId, filters } = body;
    const dpto = departamentos.find((d) => d.codigo === filters.dpto?.divipola);

    const dptoQuery = [
      {
        geo_shape: {
          'document.metadata.firstLevel.geographicCoverage.geoPoint': {
            indexed_shape: {
              index: this.config.elastic.index.dpto,
              id: dpto?.id,
              path: 'geometry',
            },
            relation: 'intersects',
          },
        },
      },
    ];
    const fondoQuery = [
      {
        term: {
          'document.resourceGroup.metadata.firstLevel.title.keyword':
            filters.fondo,
        },
      },
    ];
    const identsQuery = [
      {
        terms: { 'document.ident': filters.idents },
      },
    ];
    const keywordQuery = [
      {
        query_string: {
          query: filters.keyword,
          fields: [
            'document.metadata.firstLevel.title',
            'document.ident',
            'document.metadata.firstLevel.description',
            'document.metadata.firstLevel.geographicCoverage',
            'document.records.metadata.firstLevel.title',
          ],
        },
      },
    ];
    const periodoQuery = [
      {
        range: {
          'document.metadata.firstLevel.temporalCoverage.start': {
            gte: filters.periodo?.start,
          },
        },
      },
      {
        range: {
          'document.metadata.firstLevel.temporalCoverage.end': {
            lte: filters.periodo?.end,
          },
        },
      },
    ];
    const tipoQuery = [
      {
        nested: {
          path: 'document.records',
          query: {
            bool: {
              filter: [
                {
                  term: {
                    'document.records.support.keyword': {
                      value: filters.tipo,
                    },
                  },
                },
              ],
            },
          },
        },
      },
    ];

    const jsonQuery = {
      track_total_hits: true,
      ...(filters && {
        query: {
          bool: {
            filter: [
              ...(filters.fondo ? fondoQuery : []),
              ...(filters.idents ? identsQuery : []),
              ...(filters.periodo ? periodoQuery : []),
              ...(filters.tipo ? tipoQuery : []),
              ...(filters.dpto ? dptoQuery : []),
            ],
            must: [...(filters.keyword ? keywordQuery : [])],
          },
        },
      }),
      aggs: {
        tipo: {
          terms: {
            field: `document.metadata.${fieldId}.keyword`,
            size: 10,
          },
        },
      },
      size: 0,
    };

    let headers = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    const { url, index } = this.config.elastic;
    const completeUrl = `${url}${index.museo}-${index.env}/_search`;
    let response: any = await this.http
      .post(completeUrl, jsonQuery, headers)
      .toPromise();

    return response;
  }

  /**
   * Servicio que devuelve la lista de Fondos (origin)
   * @param body
   */
  async getOriginsList(): Promise<any> {
    let jsonQuery = {
      aggs: {
        tipo: {
          terms: {
            field: 'document.resourceGroup.metadata.firstLevel.title.keyword',
            size: 10,
          },
        },
      },
      size: 0,
    };

    let headers = {
      auth: this.config.elastic.headers.auth,
      httpsAgent: this.httpsAgent,
    };
    const { url, index } = this.config.elastic;
    const completeUrl = `${url}${index.museo}-${index.env}/_search`;
    let response: any = await this.http
      .post(completeUrl, jsonQuery, headers)
      .toPromise();

    return response;
  }
}
