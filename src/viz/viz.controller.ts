import {
  Controller,
  Get,
  Post,
  Body,
  HttpCode,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { VizService } from './viz.service';

import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('viz')
@Controller('api/viz')
export class VizController {
  cacheEtiquetas = undefined;
  cacheEntidades = undefined;

  constructor(private readonly vizService: VizService) {
    this.cacheEtiquetas = undefined;
    this.cacheEntidades = undefined;
  }

  @Post('tagcloud')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  nubeTags(@Body() body): Promise<any> {
    return this.vizService
      .tagCloud(body)
      .then((x) => {
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Get('mapping')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  mapping(): Promise<any> {
    return this.vizService
      .mapping()
      .then((x) => {
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Post('grafoEntidades')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  grafoEntidades(@Body() body): Promise<any> {
    return this.vizService
      .grafoEntidades(body)
      .then((x) => {
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Post('totalsearchhits')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  totalsearchhits(@Body() body): Promise<any> {
    return this.vizService
      .totalSearchHits(body)
      .then((x) => {
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Post('getetiquetas')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  getetiquetas(@Body() body): Promise<any> {
    return this.vizService
      .getEtiquetas(body)
      .then((x) => {
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Post('getlineatiempo')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  getlineatiempo(@Body() body): Promise<any> {
    return this.vizService
      .getLineaTiempo(body)
      .then((x) => {
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Post('getmapaviz')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  getmapaviz(@Body() body): Promise<any> {
    return this.vizService
      .mapaVizEntidades(body)
      .then((x) => {
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Post('getentidadesmapa')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  getentidadesmapa(@Body() body): Promise<any> {
    return this.vizService
      .getEntidadesMapa(body)
      .then((x) => {
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Post('getngram')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  getngram(@Body() body): Promise<any> {
    return this.vizService
      .getNGram(body)
      .then((x) => {
        return x;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Post('getHistogramDates')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  getHistogramDates(@Body() body): Promise<any> {
    return this.vizService
      .getHistogramDates(body)
      .then((x) => {
        return x;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Get('getEtiquetas')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  getListaEtiquetas(): Promise<any> {
    // if (this.cacheEtiquetas !== undefined) {
    //   // console.log(this.cacheEtiquetas)
    //   return this.cacheEtiquetas
    // }
    return this.vizService
      .getListaEtiquetas()
      .then((x) => {
        this.cacheEtiquetas = x.data;
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Get('etiquetas')
  @HttpCode(200)
  listaEtiquetas(): Promise<any> {
    // if (this.cacheEtiquetas !== undefined) {
    //   return this.cacheEtiquetas
    // }
    return this.vizService
      .getListaEtiquetas()
      .then((x) => {
        this.cacheEtiquetas = x.data;
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Get('getEntidades')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  getListaEntidades(): Promise<any> {
    // if (this.cacheEntidades !== undefined) {
    //   return this.cacheEntidades
    // }
    return this.vizService
      .getListaEntidades()
      .then((x) => {
        this.cacheEntidades = x.data;
        console.log(JSON.stringify(x.data));
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Post('getMuseoDptoMapa')
  @HttpCode(200)
  getMuseoDptoMapa(@Body() body, @Request() req): Promise<any> {
    return this.vizService
      .getMuseoDptoMapa(body)
      .then((x) => {
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Post('getMuseoMpioMapa')
  @HttpCode(200)
  getMuseoMpioMapa(@Body() body, @Request() req): Promise<any> {
    return this.vizService
      .getMuseoMpioMapa(body)
      .then((x) => {
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Post('getMuseoHistogram')
  @HttpCode(200)
  getMuseoHistogram(@Body() body, @Request() req): Promise<any> {
    return this.vizService
      .getMuseoHistogram(body)
      .then((x) => {
        return x.data;
      })
      .catch((err) => {
        return {
          error: err,
        };
      });
  }

  @Post('aggregateMetadataBy')
  @HttpCode(200)
  aggregateMetadataBy(@Body() body): Promise<any> {
    return this.vizService
      .aggregateMetadataBy(body)
      .then((result) => result.data)
      .catch((error) => {
        console.log('Error calling aggregateMetadataBy', error);
        return { error };
      });
  }

  @Post('getOriginsList')
  @HttpCode(200)
  getOriginsList(): Promise<any> {
    return this.vizService
      .getOriginsList()
      .then((result) => result.data)
      .catch((error) => {
        console.log('Error calling getOriginsList', error);
        return { error };
      });
  }
}
