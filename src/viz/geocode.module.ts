import { Module, forwardRef} from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { VizController } from './viz.controller';
import { VizService } from './viz.service';
import { AccessRequestModule } from '../access-request/access-request.module';
import { AccessRequestHistoryModule } from '../access-request-history/access-request-history.module';
import { LocalidadSchema } from './schema/localidad.schema';


@Module({
  imports: [MongooseModule.forFeature([{ name: 'Localidad', schema: LocalidadSchema }]),
   forwardRef(() => AccessRequestModule)
  ],
  controllers: [VizController],
  providers: [VizService],
  exports: [VizService,MongooseModule]
})
export class UserModule {}

