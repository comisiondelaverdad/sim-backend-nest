import { Controller, Get, Param, Query } from '@nestjs/common';
import { LineaService } from './linea.service';
import { Linea } from './schema/linea.schema';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('linea')
@Controller('api/linea')
export class LineaController {

  constructor(
    private readonly lineaService: LineaService,
  ) { }
  
  @Get('')
  async findAll(@Query() query): Promise<Linea[]> {
    return await this.lineaService.findAll(query.size, query.from);
  }

  @Get(':tema/tema')
  async findAllByLineaTema(@Param() params): Promise<Linea[]> {
    return this.lineaService.findAllByLineaTema(params.tema);
  }
  
  @Get(':titulo/titulo')
  async findByTitulo(@Param() params, @Query() query): Promise<Linea> {
    return this.lineaService.findByTitulo(params.titulo, query.size, query.from);
  }
  
  @Get(':id/id')
  async findById(@Param() params): Promise<Linea> {
    return this.lineaService.findById(params.id);
  }
  
  @Get(':from/from/:to/to')
  async findAllBetweenDates(@Param() params, @Query() query): Promise<Linea> {
    return this.lineaService.findAllBetweenDates(params.from, params.to, query.size, query.from);
  }
  
  @Get(':month/month/:day/day')
  async findAllInMonth(@Param() params): Promise<Linea> {
    return this.lineaService.findAllInMonth(parseInt(params.month, 10), parseInt(params.day, 10));
  }

}
