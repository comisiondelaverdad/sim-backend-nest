import { Injectable, HttpService } from '@nestjs/common';
import * as https from 'https'
import * as fs from 'fs';
import { Linea } from './schema/linea.schema';
import { ConfigService } from '../config/config.service';

@Injectable()
export class LineaService {

  config: any = undefined;
  httpsAgent = null;
  h = null;

  constructor(
    private configX: ConfigService,
    private http: HttpService
    ) {
      const crtFile = fs.readFileSync(configX.get("ELASTIC_AUTH_CERT"));
      const httpsAgent = new https.Agent({ ca: crtFile });
      this.config = {
        elastic: {
          url: configX.get("ELASTIC_URL"),
          index: {
            data: configX.get("ELASTIC_LINEA") || 'sim-museo-linea-qa',
          }
        }
      };
      this.h = { auth: {
        username: configX.get("ELASTIC_AUTH_USER"),
        password: configX.get("ELASTIC_AUTH_PASS")
      }, httpsAgent }
    }

  async findAll(size = 100, from = 0): Promise<Linea[]> {
    const query = {
      size,
      from,
      "query": {          
        "match_all" : {}
      }
    };
    const response = await this.http.post(`${this.config.elastic.url}${this.config.elastic.index.data}/_search`, query, this.h).toPromise()
    return { ...response.data.hits }
  }
  
  async findByTitulo(titulo: string, size = 100, from = 0): Promise<Linea>{
    const query = {
      size,
      from,
      "query":{
        "query_string":{  
             "fields":[
               "document.titulo",
               "document.descripcion",
               "document.presunto_responsable_directo",
             ],
             "query":`*${titulo}*`
          }
      }
    };
    const response = await this.http.post(`${this.config.elastic.url}${this.config.elastic.index.data}/_search`, query, this.h).toPromise()
    return { ...response.data.hits }
  }
  
  async findById(id: string): Promise<Linea>{
    const query = {
      "query":{
        "match": {
          "_id": id
        }
      }
    }
    const response = await this.http.post(`${this.config.elastic.url}${this.config.elastic.index.data}/_search/`, query, this.h).toPromise()
    return { ...response.data.hits }
  }
  
  async findAllBetweenDates(desde: string, hasta: string, size = 100, from = 0): Promise<Linea>{
    const query = {
      size,
      from,
      "query":{
        "range": {
          "document.temporalCoverage.start": {
            "gte": desde,
            "lte": hasta
          }
        }
      }
    }
    const response = await this.http.post(`${this.config.elastic.url}${this.config.elastic.index.data}/_search`, query, this.h).toPromise()
    return { ...response.data.hits }
  }
  
  async findAllInMonth(m: number, d: number): Promise<Linea>{
    const query = {
      "size": 100,
      "sort" : [
        { "document.temporalCoverage.start" : "asc" },
        "_score"
      ],
      "query":{
        "bool": {
          "filter": {
            "script": {
              "script": {
                "lang": "expression",
                "source": "doc['document.temporalCoverage.start'].date.monthOfYear == month && doc['document.temporalCoverage.start'].date.dayOfMonth == day",
                "params": {
                  "month": m,
                  "day": d
                }
              }
            }
          }  
        }
      }
    };
    const response = await this.http.post(`${this.config.elastic.url}${this.config.elastic.index.data}/_search`, query, this.h).toPromise()
    return { ...response.data.hits }
  }

  async findAllByLineaTema(tema: string, size = 100, from = 0): Promise<Linea[]>{
    const query = {
      size,
      from,
      "query":{
        "match": {
          "document.tema_principal": tema
        }
      },
    }
    const response = await this.http.post(`${this.config.elastic.url}${this.config.elastic.index.data}/_search`, query, this.h).toPromise()
    return { ...response.data.hits }
  }
}
