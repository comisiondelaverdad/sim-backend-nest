import { HttpService, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateLogDto } from './dto/create-log.dto';
import { Logs } from './schema/logs.schema';
import { ConfigService } from 'src/config/config.service';
import * as https from 'https'
import * as fs from 'fs';

@Injectable()
export class LogsService {
  private config:any = undefined;
  private httpsAgent:any = undefined;

  constructor(
    @InjectModel('Logs') private logModel: Model<Logs>,
    private configX: ConfigService,
    private http: HttpService,
  ) {
    let crtFile = fs.readFileSync(configX.get("ELASTIC_AUTH_CERT"))
    //console.log("El archivo del certificado es: ", crtFile)
    let agent = new https.Agent({ ca: crtFile })
    this.httpsAgent = agent
    //console.log("El agente https es: ", agent)
    this.config = {
      elastic: {
        url: configX.get("ELASTIC_URL"),
        index: {
          logger: configX.get("ELASTIC_INDEX_LOGGER"),
          env: configX.get("ELASTIC_INDEX_ENV"),
        },
        headers: {
          auth: {
            username: configX.get("ELASTIC_AUTH_USER"),
            password: configX.get("ELASTIC_AUTH_PASS")
          }
        }
      }
    }
  }

  async create(createLogDto:CreateLogDto){
    return (new this.logModel(createLogDto)).save();
  }

  async logger(index, logData:CreateLogDto): Promise<void | any> {
    try {
      let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
      let response = await this.http.post(
        this.config.elastic.url + this.config.elastic.index.logger + "-" + index + "-" + this.config.elastic.index.env + "/_doc",
        logData,
        h
      ).toPromise()
      return response.data
    } catch (e) {
      console.log(e)
      return e
    }
  }

  async loggerRecord(user, params): Promise<void | any> {
    let logdata = {
      user: user.username,
      date: new Date(),
      params: params
    }
    try {
      let h = { auth: this.config.elastic.headers.auth, httpsAgent: this.httpsAgent }
      let response = await this.http.post(
        this.config.elastic.url + "logger-museo-records-" + this.config.elastic.index.env + "/_doc",
        logdata,
        h
      ).toPromise()
      return response.data
    } catch (e) {
      console.log(e)
      return e
    }
  }

  async logDescarga(res){
    return this.logModel.find({"action": "Download File"}).exec()
      .then( (logDescarga:any) => {
        let logs = [] 
        logs.push(["usuario","fecha","ident","archivo","extension","origen","ip"]);
        logDescarga.forEach((log) => {
          logs.push([
            log.user,
            log.createdAt,
            log.metadata.ident,
            log.metadata.file.filename,
            log.metadata.file.extension,
            log.metadata.origin,
            log.metadata.ip
          ])
        });
        try{
          let file = "/rep/logs/logDescarga.csv"
          fs.writeFileSync(file, logs.join('\n'));
          return res.download(file);
        }
        catch(e){
          console.log(e)
        }
      });
  }

  async logDescargaByUser(username){
    return this.logModel.find({"action": "Download File", "$or": [{username: username},{user:username}]} ).exec();
  }
}
