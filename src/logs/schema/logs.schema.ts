import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({collection:"app", timestamps: true})
export class Logs extends Document {
  @Prop({type:"string"})
  user: string;

  @Prop({type:"string"})
  username: string;

  @Prop({type:"string"})
  from: string;

  @Prop({type:"string"})
  action: string;

  @Prop({type:"string"})
  message: string;

  @Prop({type:"object"})
  metadata: Object;
}

export const LogsSchema = SchemaFactory.createForClass(Logs);