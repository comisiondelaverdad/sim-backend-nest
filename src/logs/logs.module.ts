import { HttpModule, Module } from '@nestjs/common';
import { LogsService } from './logs.service';
import { LogsController } from './logs.controller';
import { Logs, LogsSchema } from './schema/logs.schema'
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigService } from 'src/config/config.service';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports:[
    MongooseModule.forFeature([{ name: 'Logs', schema: LogsSchema }], "mongologs"),
    HttpModule,
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [LogsController],
  providers: [LogsService, ConfigService],
  exports:[LogsService]
})
export class LogsModule {}