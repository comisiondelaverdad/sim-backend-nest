export class CreateLogDto {
    user: String;
    username: String;
    from: String;
    action: String;
    message: String;
    metadata: Object;
    createdAt?: String;
    updatedAt?: String;
}
