import { Controller, Body, UseGuards, Post, Req, Get, Res } from '@nestjs/common';
import { LogsService } from './logs.service';
import { CreateLogDto } from './dto/create-log.dto';
import { AuthGuard } from '@nestjs/passport';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('logs')
@Controller('api/logs')
export class LogsController {

  constructor(
    private readonly logs: LogsService
  ) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  create(@Body() createLogDto: CreateLogDto, @Req() req?) {
    let date = new Date().toISOString().split('T').join(" ");
    createLogDto.user = req.user._id;
    createLogDto.username = req.user.username;
    createLogDto.metadata['ip'] =  req.headers['x-forwarded-for'] ? req.headers['x-forwarded-for'] : (req.ip ? req.ip : (req.connection.remoteAddress ? req.connection.remoteAddress : "" ));
    createLogDto.createdAt = date;
    createLogDto.updatedAt = date;

    this.logs.logger("app", createLogDto);
    return this.logs.create(createLogDto);
  }

  @Get("/descarga")
  @UseGuards(AuthGuard('jwt'))
  async logDescarga(@Res() res){
    return await this.logs.logDescarga(res);
  }
}
