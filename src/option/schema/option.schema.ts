import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as SchemaM} from 'mongoose';
//import * as mongoosePaginate from 'mongoose-paginate';

@Schema()
export class Option extends Document {
  @Prop({type:"string"})
  term: string;

  @Prop({type:"array"})
  requestUpdate: Object[];

  @Prop({type: "object"})
  lastRequest: Object;

  @Prop({type:"string"})
  status: string;

  @Prop({ type: Date, format: 'date-time' })
  createdAt: Date;

  @Prop({ type: Date, format: 'date-time' })
  updateAt: Date;

  @Prop()
  lists: SchemaM.ObjectId;

  @Prop({type:"string"})
  description: string;

}


export const OptionSchema = SchemaFactory.createForClass(Option);
//ResourceSchema.plugin(mongoosePaginate);