import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';

import { ConfigModule } from '../config/config.module';
import { OptionController } from './option.controller';
import { OptionService } from './option.service';
import { SearchModule } from '../search/search.module';
import { OptionSchema } from './schema/option.schema';
import { LocationModule } from '../admin/location/location.module';
import { UtilService } from 'src/util/util.service';
import { ListModule } from 'src/list/list.module';
import { ResourceModule } from 'src/resource/resource.module';
import { FormsModule } from 'src/forms/forms.module';


@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Option', schema: OptionSchema }], "database"),
    LocationModule,
    SearchModule,
    ConfigModule,
    forwardRef(() => ListModule),
    forwardRef(() => ResourceModule),
    forwardRef(() => FormsModule),
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [OptionController],
  providers: [ OptionService,UtilService],
  exports: [OptionService, MongooseModule]
})
export class OptionModule { }
