import { IsString} from 'class-validator';
import { IsObjectId } from 'class-validator-mongo-object-id';

export class RequestObject {
  @IsString()
  user: string;

  @IsString()
  date: string;

  @IsString()
  term: string;
}

export class OptionDto {
  @IsObjectId()
  _id?: String;

  @IsString()
  term: String;

  @IsString()
  description: String;

  requestUpdate: Array<RequestObject>;

  @IsString()
  lastRequest: RequestObject

  @IsString()
  status: string;

  @IsString()
  createdAt: String;

  @IsString()
  updatedAt: String;

  @IsString()
  lists:String;
}