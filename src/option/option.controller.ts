import {
  Controller, HttpCode, Query, Request, Post, Body, UseGuards, Delete, Get, Param, Put
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { matchRolesList } from 'src/auth/roles.guard';
import { OptionDto } from './dto/option.dto';
import { OptionService } from './option.service';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Option } from './schema/option.schema';


@ApiBearerAuth()
@ApiBearerAuth()
@ApiTags('Options')
@Controller('api/option')
export class OptionController {

  constructor(
    private OptionService: OptionService) {
  }

  @ApiOperation({summary: "Option by _id"})
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: Option,
  })
  @HttpCode(200)
  @UseGuards(AuthGuard())
  @Get('byId/:id')
  async getByName(@Request() req, @Param('id') id:string, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor', 'tesauro']) //Validación de roles del usuario
    return this.OptionService.findById(id);
  }


  @ApiOperation({summary: "Delete option {idOption} from list {idList}"})
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: Option,
  })
  @UseGuards(AuthGuard())
  @Delete(':idOption/:idList')
  async delete(@Param('idOption') idOption:string, @Param('idList') idList:string ) {
    return await this.OptionService.delete(idOption, idList);
  }

  @HttpCode(200)
  @UseGuards(AuthGuard())
  @Post('alert')
  async getRegisterByList(@Request() req, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor', 'tesauro']) //Validación de roles del usuario
    return this.OptionService.findRegisterByList(body);
  }

  @HttpCode(200)
  @UseGuards(AuthGuard())
  @Post('')
  async newOption(@Request() req, @Query() query, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor', 'tesauro']) //Validación de roles del usuario
    const dtoOption: OptionDto = body;
    let newList = await this.OptionService.create(dtoOption);
    return newList;
  }

  @HttpCode(200)
  @UseGuards(AuthGuard())
  @Put('')
  async updateOption(@Request() req, @Query() query, @Body() body): Promise<any> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor', 'tesauro']) //Validación de roles del usuario
    const dtoOption: OptionDto = body;
    let newList = await this.OptionService.update(dtoOption);
    return newList;
  }

}
