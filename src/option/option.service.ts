import { Injectable, HttpService, HttpException, HttpStatus } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Option } from './schema/option.schema';
import * as _ from "lodash";
import { OptionDto } from './dto/option.dto';
import { UtilService } from 'src/util/util.service';
import { List } from 'src/list/schema/list.schema';
import { ListDto } from 'src/list/dto/list.dto';
import { ResourceService } from 'src/resource/resource.service';
import { FormsService } from 'src/forms/forms.service';

@Injectable()
export class OptionService {
  constructor(
    @InjectModel('Option') private optionModel: Model<Option>,
    @InjectModel('List') private listModel: Model<List>,
    private readonly resourceService: ResourceService,
    private readonly utilService: UtilService,
    private formsService: FormsService,
  ) { }

  async create(optionDto: OptionDto): Promise<Option> {
    const createdUser = new this.optionModel(optionDto);
    createdUser.createdAt = new Date();
    createdUser.updatedAt = null;
    const created = await createdUser.save();
    return created;
  }

  async update(optionDto: OptionDto,): Promise<Option> {
    return await this.optionModel.findOneAndUpdate(
      { _id: optionDto._id }, { ...optionDto, ...{ updateAt: new Date() } }, { new: true }
    )
  }

  async getPathByid(optionId = null, path = null, listHead = null) {
    if (optionId) {
      const option = await this.optionModel.findOne({ _id: optionId }).exec();
      path = !path ? option.term : `${option.term} - ${path}`;
      const list = await this.listModel.find({ options: { $in: [option._id] } }).exec();
      if (this.utilService.is_array(list) && list.length > 0) {
        const optionFather = await this.optionModel.findOne({ lists: list[0]._id }).exec();
        if (optionFather) {
          return this.getPathByid(optionFather._id, path, list[0])
        } else {
          return { listHead: list[0], term: path }
        }
      } else {
        return { listHead: listHead, term: path }
      }
    } else {
      return { listHead: listHead, term: path }
    }
  }


  async delete(idOption, idList) {
    let registersFounded = 0;

    const { term, listHead } = await this.getPathByid(idOption);
    const statmentFAI = await this.formsService.getIdFormStatment("RESOURCES_FAI");
    const statmentFAE = await this.formsService.getIdFormStatment("RESOURCES_FAE");
    const statmentFondos = await this.formsService.getIdFormStatment("RESOURCEGROUPS_FAE");
    const statmentColaborativeForm = await this.formsService.getIdFormStatment("COLABORATIVE_FORM");

    let resourcesFlat;
    let resourcegroupsFlat;
    const { resources, resourcegroups } : any = await this.resourceService.getResourcesAndResourcegroups(listHead.path, term);
    
    if(typeof resources !== 'undefined') {
      resourcesFlat = resources.map((resource) => { return this.utilService.getMetadatePlain(resource)});
      if(resources) registersFounded += resources.length;
    }
    if(typeof resourcegroups !== 'undefined') {
      resourcegroupsFlat = resourcegroups.map((resourcegroup) => { return this.utilService.getMetadatePlain(resourcegroup)});
      if(resourcegroups) registersFounded += resourcegroups.length;
    }
     
    if (registersFounded === 0) {
      const list = await this.listModel.findOne({ "_id": idList }).exec();
      list.options = list.options.filter(opt => (opt.toString() !== idOption))
      await list.save();
      return this.optionModel.deleteOne({ "_id": idOption });
    } else {
      return {
        message: `No es posible eliminar, Se encontraron ${registersFounded} registros con esta coincidencia <b>${listHead.name}: ${term}</b>`,
        resourcesFlat,
        resourcegroupsFlat,
        statmentFAI,
        statmentFAE,
        statmentFondos,
        statmentColaborativeForm
      }
    }
  }

  async getRegister(idOption, idList) {
    let registersFounded = 0;
    const { term, listHead } = await this.getPathByid(idOption, null);
    console.log({ term, listHead })
    const { resources, resourcegroups } = await <any>this.resourceService.getResourcesAndResourcegroups(listHead.path, term);
    const resourcesFlat = resources.map((resource) => { return this.utilService.getMetadatePlain(resource)});
    const resourcegroupsFlat = resourcegroups.map((resourcegroup) => { return this.utilService.getMetadatePlain(resourcegroup)});
    const statmentFAI = await this.formsService.getIdFormStatment("RESOURCES_FAI");
    const statmentFAE = await this.formsService.getIdFormStatment("RESOURCES_FAE");
    const statmentFondos = await this.formsService.getIdFormStatment("RESOURCEGROUPS_FAE");

    if (resources) {
      //console.log(resources);
      registersFounded += resources.length
    }
    if (resourcegroups) {
      registersFounded += resourcegroups.length
    }
    if (registersFounded === 0) {
      const list = await this.listModel.findOne({ "_id": idList }).exec();
      list.options = list.options.filter((o) => (o.toString() !== idOption))
      //console.log(list);
      await list.save();
      return this.optionModel.deleteOne({ "_id": idOption });

      //console.log({ message: `No es posible eliminar, Se encontraron ${registersFounded} registros con esta coincidencia <b>${listHead.name}: ${term}</b>` })
    } else {
        
      return {
        message: `No es posible eliminar, Se encontraron ${registersFounded} registros con esta coincidencia <b>${listHead.name}: ${term}</b>`,
        resourcesFlat: resourcesFlat,
        resourcegroupsFlat: resourcegroupsFlat,
        statmentFAI: statmentFAI,
        statmentFAE: statmentFAE,
        statmentFondos: statmentFondos
      }
    }
  }

  async findRegisterByList(body) {

    console.log("Body descarga:", body);
    const { resources, resourcegroups } = await <any>this.resourceService.getRegistersWithListId(body);
    const resourcesFlat = resources.map((resource) => { return this.utilService.getMetadatePlain(resource) });
    const resourcegroupsFlat = resourcegroups.map((resourcegroup) => { return this.utilService.getMetadatePlain(resourcegroup) });
    const statmentFAI = await this.formsService.getIdFormStatment("RESOURCES_FAI");
    const statmentFAE = await this.formsService.getIdFormStatment("RESOURCES_FAE");
    const statmentFondos = await this.formsService.getIdFormStatment("RESOURCEGROUPS_FAE");
    return {
      resourcesFlat: resourcesFlat,
      resourcegroupsFlat: resourcegroupsFlat,
      statmentFAI: statmentFAI,
      statmentFAE: statmentFAE,
      statmentFondos: statmentFondos
    }
  }



  async ifEdit(idOption, idList) {
    let registersFounded = 0;
    const option = await this.optionModel.findOne({ _id: idOption }).exec();
    const lists = await this.listModel.findOne({ _id: idList }).exec();
    const { resources, resourcegroups } = await <any>this.resourceService.getResourcesAndResourcegroups(lists.path, option.term);

    if (resources) {
      console.log(resources);
      registersFounded += resources.length
    }
    if (resourcegroups) {
      registersFounded += resourcegroups.length
    }
    if (registersFounded === 0) {
      return { resources: null, resourcegroups: null };
    } else {
      return { resources, resourcegroups }
    }
  }

  async findAll(): Promise<Option[]> {
    return this.optionModel.find().populate({ path: 'lists', model: this.listModel }).exec();
  }

  async findById(id): Promise<Option> {
    return this.optionModel.findOne({ '_id': id }).populate({ path: 'lists', model: this.listModel }).exec();
  }



}