import { Module, HttpModule, Logger } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from './auth/roles.guard';
import { DatabaseModule } from './database/database.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SearchModule } from './search/search.module';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { ResourceGroupModule } from './resource-group/resource-group.module';
import { ResourceModule } from './resource/resource.module';
import { LineaModule } from './viz/linea.module';
import { RecordModule } from './record/record.module';
import { SemanticModule } from './semantic/semantic.module';
import { TermModule } from './term/term.module';
import { BookmarkModule } from './bookmark/bookmark.module';
import { NotificationModule } from './notification/notificaction.module';
import { AccessRequestModule } from './access-request/access-request.module';
import { StatsModule } from './stats/stats.module';
import { FormModule } from './form/form.module';
import { AccessRequestHistoryModule } from './access-request-history/access-request-history.module';
import { AccessRequestExtensionModule } from './access-request-extension/access-request-extension.module';
import { AdminModule } from './admin/admin.module';
import { TestingModule } from './testing/testing.module';
import { VizModule } from './viz/viz.module';
import { LoggingModule } from './logging/logging.module';
import { LogsModule } from './logs/logs.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { CollectionModule } from './collection/collection.module';
import { CollectionBookmarkModule } from './collection-bookmark/collection-bookmark.module';
import {MenuModule } from './admin/menus/menu.module'
import {MicrositeModule } from './admin/microsites/microsite.module'

//import { MetadataStandarizationController } from './metadata-standarization/metadata-standarization.controller';
import { MetadataStandarizationService } from './metadata-standarization/metadata-standarization.service';
import { MetadataStandarizationModule } from './metadata-standarization/metadata-standarization.module';
import { SearchMuseoModule } from './search-museo/search-museo.module';
import { ListModule } from './list/list.module';
import { OptionModule } from './option/option.module';
import { FormsModule } from './forms/forms.module';


@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'client'),
    }),
    DatabaseModule,
    HttpModule,
    AuthModule,
    SearchModule,
    VizModule,
    UserModule,
    ResourceGroupModule,
    ResourceModule,
    LineaModule,
    RecordModule,
    SemanticModule,
    TermModule,
    NotificationModule,
    BookmarkModule,
    AccessRequestModule,
    StatsModule,
    FormModule,
    AccessRequestHistoryModule,
    AccessRequestExtensionModule,
    TestingModule,
    AdminModule,
    CollectionModule,
    CollectionBookmarkModule,
    LoggingModule,    
    MenuModule,
    MicrositeModule,
    LogsModule, 
    MetadataStandarizationModule,
    ListModule,
    FormsModule,
    OptionModule,
    EventEmitterModule.forRoot(),
    SearchMuseoModule
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_GUARD,
      useClass: RolesGuard
    },
    AppService,
    Logger,
  ],
})
export class AppModule { }