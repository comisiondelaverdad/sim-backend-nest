import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class User extends Document {
  @Prop({type:"string"})
  username: string;
  
  @Prop({type:"string"})
  password: string;
  
  @Prop({type:"string"})
  name: string;
  
  @Prop({type:"string"})
  lastname: string;
  
  @Prop({type:"string"})
  email: string;
  
  @Prop({type:"string"})
  photo: string;
  
  @Prop({type:"string"})
  provider: string;
  
  @Prop({type:"array"})
  roles: Array<string>;
  
  @Prop({type:"number"})
  status: number;
  
  @Prop({type:"date"})
  date: Date;
  
  @Prop({type:"date"})
  lastLogin: Date;
  
  @Prop({type:"object"})
  social: Object;

  @Prop({type:"number"})
  accessLevel: number;

  @Prop({type:"boolean"})
  compromise: boolean;

  @Prop({type:"number"})
  downloadQuota: string;

}


export const UserSchema = SchemaFactory.createForClass(User);


