import { Body, Controller, Request, UseGuards, Get, Post, Param, Req, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Reflector } from '@nestjs/core';
import { Roles } from '../auth/roles.decorator';
import { UsersService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './schema/user.schema';
import { AccessRequest } from '../access-request/schema/access-request.schema';
import { matchRoles } from '../auth/roles.guard';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('users')
@Controller('api/users')
export class UserController {

  constructor(
    private readonly usersService: UsersService,
  ) { }

  
  
  @Get('acceptcompromise')
  @UseGuards(AuthGuard())//, RolesGuard)
  async acceptcompromise( @Request() req) {   
    const res = await this.usersService.updateCompromise(req.user._id);
    console.log(res)
    return res
  }

  @Get('compromise')
  @UseGuards(AuthGuard())//, RolesGuard)
  async compromise( @Request() req) {   
    const res = await this.usersService.getUser(req.user._id);
    console.log(res)
    return res
  }

  @Get('me')
  @UseGuards(AuthGuard())//, RolesGuard)
  me(@Request() req) {
    console.log(req)
    return req.user;
  }

  @Get('test')
  @UseGuards(AuthGuard())
  test(@Request() req): object | string {
    matchRoles(req.user, 'user') //Validación de roles del usuario
    console.log("El usuario dentro del controller es: ", req.user)
    return req.user;
  }

  @Get('catalogador')
  @UseGuards(AuthGuard())
  getCatalogador(@Request() req): object | string {
    matchRoles(req.user, 'catalogador_gestor') //Validación de roles del usuario
    return this.usersService.getByRol(['catalogador', 'catalogador_gestor'])
  }
  
  @Get('byroles')
  getUsers(@Query() query): object | string {
    const roles = query.roles ? query.roles.split(',') : ['catalogador', 'catalogador_gestor'];
    return this.usersService.getByRol(roles).then(users => {
      return users.map((user: any) => {
        return {
          _id: user._id,
          username: user.username,
          name: user.name,
          photo: user.photo,
        };
      })
    });
  }

  @Get(':id/access-request')
  @UseGuards(AuthGuard())
  accessRequestByUser(@Param() params): Promise<void | AccessRequest[]> {
    return this.usersService.accessRequestByUser(params.id);
  }

  @Get('quota')
  @UseGuards(AuthGuard())
  downloadQuotaByUser(@Req() req): object | string {
    return this.usersService.downloadQuota(req.user.username);
  }

}
