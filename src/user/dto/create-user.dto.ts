export class CreateUserDto {
  _id?: String;
  readonly username: String;
  readonly password: String;
  readonly name: string;
  readonly lastname: String;
  readonly email: String;
  readonly photo: String;
  readonly provider: String;
  readonly roles: Array<String>;
  readonly status: Number;
  readonly date: Date;
  readonly lastLogin: Date;
  readonly social: Object;
  accessLevel: Number;
}

