import { Module, forwardRef, HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserController } from './user.controller';
import { UsersService } from './user.service';
import { AccessRequestModule } from '../access-request/access-request.module';
import { AccessRequestHistoryModule } from '../access-request-history/access-request-history.module';
import { UserSchema } from './schema/user.schema';
import { PassportModule } from '@nestjs/passport';
import { LogsService } from 'src/logs/logs.service';
import { UtilService } from 'src/util/util.service';
import { LogsSchema } from 'src/logs/schema/logs.schema';
import { ConfigService } from 'src/config/config.service';
import { LogsModule } from 'src/logs/logs.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }], "database"),
    forwardRef(() => AccessRequestModule),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    MongooseModule.forFeature([{ name: 'Logs', schema: LogsSchema }], "mongologs"),
    HttpModule,
    LogsModule,
  ],
  controllers: [UserController],
  providers: [UsersService, LogsService, UtilService, ConfigService],
  exports: [UsersService, MongooseModule]
})
export class UserModule { }