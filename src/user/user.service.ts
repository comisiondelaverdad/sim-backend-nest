import { Injectable} from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './schema/user.schema';
import { CreateUserDto } from './dto/create-user.dto';
import { EditUserDto } from './dto/edit-user.dto';
import { AccessRequestService } from '../access-request/access-request.service'
import { AccessRequest } from '../access-request/schema/access-request.schema';
import { CreateLogDto } from 'src/logs/dto/create-log.dto';
import { UtilService } from 'src/util/util.service';
import { LogsService } from 'src/logs/logs.service';

enum Provider
{
    GOOGLE = 'google'
}


@Injectable()
export class UsersService {
  constructor(
    @InjectModel('User') private userModel: Model<User>,
    private accessRequestService: AccessRequestService,
    private readonly logs: LogsService,
    private utils: UtilService,
    ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    createUserDto["accesslevel"] = 4
    const createdUser = new this.userModel(createUserDto);
    const user = await  createdUser.save();
    let logInfo: CreateLogDto = {
      user: user._id,
      username:user.username,
      from: "user",
      action: "created",
      message: "",
      metadata: {
        ident: user._id,
        roles: user.roles,
        accessLevel: user.accessLevel,
        status: user.status
      },
      createdAt: this.utils.dateToAAAAMMDD(Date.now()),
    }
    this.logs.create(logInfo);
    this.logs.logger("app", logInfo);
    return new Promise((resolve) => {
      resolve(createdUser)
    })
  }

  async find(query): Promise<User[]> {
    return this.userModel.find(query).exec();
  }

  async findAll(): Promise<User[]> {
    return this.userModel.find().exec();
  }
  
  async findOneByThirdPartyId(thirdPartyId: string, provider: Provider): Promise<User>{
    return this.userModel.findOne({"social.id":thirdPartyId,"provider":provider}).exec();
  }
  
  async registerOAuthUser(u: object, thirdPartyId: string, provider: Provider): Promise<User> {
    const createdUser = new this.userModel(u);
    let logInfo: CreateLogDto = {
      user: createdUser._id,
      username:createdUser.username,
      from: "user",
      action: "created",
      message: "",
      metadata: {
        ident: createdUser._id,
        roles: createdUser.roles,
        accessLevel: createdUser.accessLevel,
        status: createdUser.status
      },
      createdAt: this.utils.dateToAAAAMMDD(Date.now()),
    }
    this.logs.create(logInfo);
    this.logs.logger("app", logInfo);
    await createdUser.save();  
    return new Promise((resolve) => {
      resolve(createdUser)
    })
  }

  async getUser(id): Promise<User> {
    return this.userModel.findOne({"_id":id}).exec();
  }

  getByRol(rol: Array<string>) {
    return this.userModel.find({"roles": { $in: rol}}).exec()
  }

  async accessRequestByUser(id): Promise<void | AccessRequest[]> {
    return this.accessRequestService.findByUser(id);
  }

  async update(id, user: EditUserDto): Promise<User> {
    return this.userModel.findOneAndUpdate({"_id":id}, user).exec();
  }
  async updateCompromise(id): Promise<User> {
    return this.userModel.updateOne({"_id":id},{"$set":{"compromise": true}}).exec();
  }
  

  async findOneByUserName(username: string): Promise<User>{
    return this.userModel.findOne({"username":username}).exec();
  }

  async downloadQuota(username: string){
    let quota = await this.userModel.findOne({"username":username}).exec();
    let downloaded = await this.logs.logDescargaByUser(username);
    let quotaActual = quota.downloadQuota ? quota.downloadQuota : 0
    return {
      quota: quotaActual,
      downloaded: downloaded.length,
      download: quotaActual > downloaded.length
    }
    
  }
}
