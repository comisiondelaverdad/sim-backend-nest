import { Injectable, HttpService, HttpException, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Resource } from './schema/resource.schema';
import { Record } from '../record/schema/record.schema';
import { ResourceGroup } from './../resource-group/schema/resource-group.schema';
import { ResourceGroupCacheService } from '../resource-group/resource-group-cache.service';
import { UtilService } from './../util/util.service';
import * as _ from "lodash";
import { Types } from 'mongoose';
import { FormsService } from 'src/forms/forms.service';
import { List } from 'src/list/schema/list.schema';
import { Option } from 'src/option/schema/option.schema';
import { SlugIdentService } from '../slug-ident/slug-ident.service';
import { User } from '../user/schema/user.schema'
import { ListService } from 'src/list/list.service';
import { retry } from 'rxjs/operators';

function objectIdWithTimestamp(timestamp) {
  /* Convert string date to Date object (otherwise assume timestamp is a date) */
  if (typeof (timestamp) === 'string') {
    timestamp = new Date(timestamp);
  }
  console.log({ timestamp });
  /* Convert date object to hex seconds since Unix epoch */
  const hexSeconds = Math.floor(timestamp / 1000).toString(16);
  /* Create an ObjectId with that hex timestamp */
  const constructedObjectId = Types.ObjectId(hexSeconds + "0000000000000000");
  return constructedObjectId;
}

@Injectable()
export class ResourceService {

  constructor(
    private httpService: HttpService,
    private readonly utilService: UtilService,
    private readonly listService: ListService,
    private formsService: FormsService,
    private resourceGroupCacheService: ResourceGroupCacheService,
    private readonly slugIdentService: SlugIdentService,
    @InjectModel('Resource') private resourceModel: Model<Resource>,
    @InjectModel('Record') private recordModel: Model<Record>,
    @InjectModel('ResourceGroup') private resourceGroupModel: Model<ResourceGroup>,
    @InjectModel('List') private listModel: Model<List>,
    @InjectModel('Option') private optionModel: Model<Option>,
    @InjectModel('User') private userModel: Model<User>,
  ) {
    console.log('inicialize resourceGroup cache from resources ...')
    this.resourceGroupCacheService.init();
  }

  async count(): Promise<number> {
    return this.resourceModel.countDocuments({
      "extra.status": {
        "$ne": "deleted"
      }
    }).exec();
  }

  // cuenta resources hasta una fecha filtrados por origen default="CatalogacionFuentesInternas" o cuenta la diferencia entre dos fechas
  // sumando siempre todos los resources que no tienen fecha en absoluto
  async countBetweenDates(to: Date, origin: string, from?: Date): Promise<number> {
    if (!from) {
      return this.resourceModel.countDocuments({ $or: [{ 'extra': { $exists: false } }, { 'extra.history.date': { $lte: to } }] }).where({ 'origin': origin }).exec();
    } else {
      const existed = await this.resourceModel.countDocuments({ 'extra': { $exists: false } }).where({ 'origin': origin }).exec();
      const datesAdjusted = await this.resourceModel.countDocuments({ $and: [{ 'extra.history.date': { $lte: to } }, { 'extra.history.date': { $gte: from } }], 'origin': origin }).exec();
      return existed + datesAdjusted;
    }
  }

  // 'extra.history': {$elemMatch : {$and: [{"user":"60525ed406c3872f09a2136d", "status":"created"}] } } }
  async catalogoInternasPorTiempoEnId(users: Array<string>, _from?: string, _to?: string): Promise<[]> {
    const query = {
      _id: { $gte: objectIdWithTimestamp(_from || '05-01-2021'), $lt: objectIdWithTimestamp(_to || new Date()) },
      "origin": {
        $eq: "CatalogacionFuentesInternas"
      },
      "extra.status": {
        $ne: 'deleted'
      },
    };
    if (users.length > 0) {
      const whichUsers = users.map(user => {
        const obj = { $and: [{ "user": user, "status": "created" }] };
        return obj;
      })
      query['extra.history'] = {
        $elemMatch: { $or: whichUsers }
      };
    }

    return await this.resourceModel.find(query).exec();
  }

  async resourceConteoTemporalCatalogacionFuentesInternas(to: Date, users: Array<string>): Promise<Resource[]> {
    const query = {
      _id: { $gt: objectIdWithTimestamp(to) },
      "extra.status": {
        $ne: 'deleted'
      },
      "origin": {
        $eq: "CatalogacionFuentesInternas"
      },
    };

    if (users.length > 0) {
      const whichUsers = users.map(user => {
        const obj = { $and: [{ "user": user, "status": "created" }] };
        return obj;
      })
      query['extra.history'] = {
        $elemMatch: { $or: whichUsers }
      };
    }
    const resource = await this.resourceModel.find(query).exec();
    if (!resource) {
      throw new HttpException({
        status: HttpStatus.FORBIDDEN,
        error: `El recurso para ${to} no trajo resultados`,
      }, HttpStatus.FORBIDDEN);
    } else {
      return resource;
    }
  }

  async conteoTemporalMetaBuscador(to: Date): Promise<Resource[]> {
    return this.resourceModel.find(
      {
        "extra.history.date": { $gte: to },
        "extra.createdBy": {
          $eq: "METABUSCADOR"
        }
      }).exec();
  }

  // lo anterior es mas facil resolver con agregaciones pero tiene que estar la fecha maximo dentro de un objeto no a 3 niveles de distancia
  // return this.resourceModel.aggregate([
  //   {
  //     $match: {
  //       "extra.history.date": {$gte: starting},
  //       "origin": {
  //         $eq: "CatalogacionFuentesInternas"
  //       },
  //     },
  //   },
  //   {
  //     $group: {
  //         _id: "$extra.status",
  //         documentCount: {$sum: 1}
  //     }
  //   }
  // ]).exec();

  async conteoRecursosPorOrigen(origin: string): Promise<number> {
    return this.resourceModel.countDocuments({ "origin": origin }).exec();
  }

  async getById(req, id): Promise<Resource[]> {
    let resource = await this.resourceModel
      .findOne({
        "ident": id,
        "extra.status": {
          $ne: "deleted"
        },
      })
      .populate({
        path: 'records',
        model: this.recordModel
      })
      .exec();
    if (!resource) {
      throw new HttpException({
        status: HttpStatus.FORBIDDEN,
        error: `El recurso con indent ${id} ha sido eliminado o no existe, por favor valide con el area de catalogación`,
      }, HttpStatus.FORBIDDEN);
    }
    return resource;
  }

  async getByIdSimple(req, id): Promise<Resource> {
    let resource = await this.resourceModel
      .findOne({ "ident": id })
      .exec();
    return resource;
  }

  async getByResource(id): Promise<Resource[]> {
    let resource = await this.resourceModel
      .findOne({ "_id": id })
      .populate({
        path: 'records',
        model: this.recordModel
      })
      .exec();

    return resource;
  }


  // admin-catalogin module
  async getMyResources(user) {
    let resources = await this.resourceModel
      .aggregate([
        {
          $match: {
            "extra.history": {
              $elemMatch: {
                $and: [
                  { user: user._id.toString() }
                ]
              }
            },
            "extra.status": {
              $ne: "deleted"
            },
          },
        }
      ]);
    let resourceg = await Promise.all(resources.map((rg) => {
      if (rg.identifier) {
        return this.resourceGroupModel.find({ 'ident': rg.identifier }, '_id metadata.firstLevel.title ident').exec()
      }
    }))
    resourceg.map((rg, index) => {
      if (rg) {
        resources[index].extra = { ...resources[index].extra, ...{ resourcegroup: rg[0] } }
      }
    })

    resources = resources.map((resource) => {
      const firstLevel = {
        ...resource.metadata.firstLevel,
      }
      resource.metadata.firstLevel = firstLevel;
      return resource;
    })
    return resources;
  }

  async getRegistersWithListId(body) {
    let { listResources, listRG, destinyName, listName } = body;
    var filterResources = { 'ident': { $in: listResources } };
    var filterRG = { 'ident': { $in: listRG } };
    //const nameResourcegroups = ['RESOURCEGROUPS_FAE'];
    //const nameResources = ['RESOURCES_FAE', 'RESOURCES_FAI'];
    let dataReturn = {};
    const resourcesfounded = await this.resourceModel.find(filterResources, `${destinyName} metatadata.firstLevel.title ident ResourceGroupId origin`);
    if (JSON.stringify(resourcesfounded) !== '[[]]') {
      dataReturn = {
        ...dataReturn,
        ...{ resources: resourcesfounded }
      }
    }
    const resourceGroupsfounded = await this.resourceGroupModel.find(filterRG, `${destinyName} metatadata.firstLevel.title ident ResourceGroupParentId origin`);
    if (JSON.stringify(resourceGroupsfounded) !== '[[]]') {
      dataReturn = {
        ...dataReturn,
        ...{ resourcegroups: resourceGroupsfounded }
      }
    }
    return dataReturn;
  }

  async getResources(filter) {

    let { ResourceGroupParentId, user, title, from, size, ident, filename } = filter.body;
    size = size > 0 ? size : 1;

    let idRecord = null;
    if (filename) {
      let record = await this.recordModel.findOne({ 'filename': filename }).exec();
      if (record) {
        idRecord = record._id;
      }
    }

    let resources = await this.resourceModel
      .aggregate([
        {
          $match: {
            ...user ? {
              "extra.history": {
                $elemMatch: {
                  $and: [
                    { user: user.toString() }
                  ]
                }
              }
            } : {},
            ...ResourceGroupParentId ? { "ResourceGroupId": ResourceGroupParentId ? ResourceGroupParentId.id ? ResourceGroupParentId.id : '' : '' } : {},
            ...title ? { "metadata.firstLevel.title": RegExp(`${title.trim()}`) } : {},
            ...ident ? { "ident": RegExp(`${ident.trim()}`) } : {},
            "extra.status": {
              $ne: "deleted"
            },
          },
        },
        {
          $match: {
            ...filename ? { "records": (`${idRecord.toString()}`) } : {}
          }
        },
        {
          $project: {
            "_id": 1,
            "ident": 1,
            "identifier": 1,
            "type": 1,
            "metadata": 1,
            "records": 1,
            "origin": 1,
            "ResourceGroupId": 1,
            "extra": 1,
            "datarecords.filename": 1,
          }
        },
      ])
      .skip(Number((from - 1) * size))
      .limit(Number(size));

    const count = await this.resourceModel
      .aggregate([
        {
          $match: {
            ...user ? {
              "extra.history": {
                $elemMatch: {
                  $and: [
                    { user: user.toString() }
                  ]
                }
              }
            } : {},
            ...ResourceGroupParentId ? { "ResourceGroupId": ResourceGroupParentId ? ResourceGroupParentId.id ? ResourceGroupParentId.id : '' : '' } : {},
            ...title ? { "metadata.firstLevel.title": RegExp(`${title.trim()}`) } : {},
            ...ident ? { "ident": RegExp(`${ident.trim()}`) } : {},
            "extra.status": {
              $ne: "deleted"
            },
          },
        },
        {
          $match: {
            ...filename ? { "records": (`${idRecord.toString()}`) } : {}
          }
        },
        {
          $project: {
            "_id": 1,
          }
        }
      ])
      .count("total");

    let userInfo = await Promise.all(resources.map(async (r) => {
      if (r?.extra?.history) {
        return await Promise.all(r.extra.history.map(async (h) => {
          if (h?.user) {
            const user = await this.userModel.findOne({ '_id': h.user }, 'name').exec();
            return {
              username: user.name,
              date: h.date,
              status: h.status
            }
          } else {
            return h
            // const dateFilter = h.date.split('-');
            // let filterValid = '';
            // if (dateFilter.length === 4) {
            //   filterValid = `${dateFilter[0]}-${dateFilter[1]}-${dateFilter[2]} ${dateFilter[3]}`;
            // }
            // return {
            //   ...h,
            //   ...{ date: new Date(filterValid), username: 'Sistema' }
            // }
          }
        }))
      }
    }))

    let resourceg = await Promise.all(resources.map((rg) => {
      if (rg.ResourceGroupId) {
        return this.resourceGroupModel.find({ 'ident': rg.ResourceGroupId }, '_id metadata.firstLevel.title ident').exec()
      }
    }))

    resourceg.map((rg, index) => {
      if (rg) {
        const extra = { ...resources[index].extra ? resources[index].extra : {}, ...{ "resourcegroup": rg[0] } }
        resources[index] = {
          ...resources[index],
          ...{ extra: extra }
        }
      }
    })

    let resourcesCompleted = await Promise.all(resources.map((rgCompleted) => {
      if (rgCompleted.records) {
        let id = []
        id = [... new Set(rgCompleted.records)];
        return this.recordModel.find({ '_id': { $in: id } }).exec();
      }
    }))

    resourcesCompleted.map(async (rg, index) => {
      if (rg) {
        resources[index].datarecords = rg
      }
    })

    const statmentFAI = await this.formsService.getFormWithOriginStatment('RESOURCES_FAI');
    const statmentFAE = await this.formsService.getFormWithOriginStatment('RESOURCES_FAE');
    const statmentOBJ = await this.formsService.getFormWithOriginStatment('RESOURCES_OBJ');

    let resourcesReturn = resources.map((item, index) => {

      if (item?.extra?.history) item.extra.history = userInfo[index];

      let auth = item.datarecords.find((record) => {
        return record.type === 'attachedRightsAuthorization';
      });
      let hashAuth = auth ? 'Sí' : 'No';

      return {
        ...item,
        ...{
          metadataHTML: `<div class="meta-container">${this.utilService.getMetadataHtmlFromStatment(
            item,
            statmentFAI.origin.includes(item.origin) ? statmentFAI.statment : statmentFAE.origin.includes(item.origin) ? statmentFAE.statment : statmentOBJ.statment
          )}</div>`,
        },
        ...{
          metadataExcel: this.utilService.getMetadatePlain(item, hashAuth)
        }
      };
    });
    return { "resources": resourcesReturn, "total": JSON.stringify(count) === '[]' ? 0 : count[0].total, "currentPage": from };
  }

  async postResource(body) {
    const { metadata } = body;
    metadata.firstLevel.accessLevel = parseInt(metadata.firstLevel.accessLevel, 10);
    const resourceGroup = await this.resourceGroupModel.findOne({ "ident": body.resourceGroup })
    const identify = this.utilService.uuidv4();
    const type = body.metadata.firstLevel.documentType ? this.utilService.is_array(body.metadata.firstLevel.documentType) ?
      body.metadata.firstLevel.documentType[0] : body.metadata.firstLevel.documentType :
      body.metadata.firstLevel.descriptionLevel ? body.metadata.firstLevel.descriptionLevel : null;

    const newResource = {
      ResourceGroupId: resourceGroup.ident,
      extra: {
        createdBy: 'METABUSCADOR',
        history: [{
          id: this.utilService.uuidv4(),
          last: null,
          status: 'created',
          date: new Date(),
          user: (body.user._id.toString())
        }],
        createdAt: new Date(),
        status: 'created',
      },
      type: type,
      ident: `${resourceGroup.ident}-${identify}`,
      identifier: resourceGroup.ident,
      origin: resourceGroup.origin,
      records: [],
      metadata: metadata,
      indexed: false,
    }
    //Agregar campos slug, simpleident a metadata
    if (metadata.firstLevel.accessLevel === 4 && resourceGroup.origin === 'CatalogacionFuentesInternas') {
      const myResources = await this.slugIdentService.getAllResources();
      Promise.all([
        this.slugIdentService.getSlug(myResources, metadata.firstLevel.title),
        this.slugIdentService.getSimpleIdent(myResources)
      ])
        .then(([slug, simpleident]) => {
          metadata.slug = slug;
          metadata.simpleident = simpleident;
        })
        .catch(err => console.error(err))
    }

    const resource = await this.resourceModel.create(newResource);
    if (resourceGroup.resources !== null && typeof resourceGroup.resources !== 'undefined') {
      resourceGroup.resources.push(resource._id.toString());
    } else {
      resourceGroup.resources = [resource._id.toString()];
    }
    resource.ident = `${resourceGroup.ident}-${resource._id}`
    const recordsResults = await Promise.all(body.records.map(async (rec) => {
      const typeRecord = (rec.fieldname.split('-'))[0] === 'attachedRightsAuthorization' ? 'attachedRightsAuthorization' :
        body.metadata.firstLevel.documentType ? this.utilService.is_array(body.metadata.firstLevel.documentType) ?
          body.metadata.firstLevel.documentType[0] : body.metadata.firstLevel.documentType :
          body.metadata.firstLevel.descriptionLevel ? body.metadata.firstLevel.descriptionLevel : null;
      const nameSplit = rec.originalname.split(".");
      const newRec = {
        ident: this.utilService.uuidv4(),
        identifier: resource.ident,
        identifier_II: `${resourceGroup.ident}`,
        filename: rec.path,
        resourceId: resource._id.toString(),
        origin: resourceGroup.origin,
        type: typeRecord,
        extra: {
          originalName: rec.originalname,
          mimetype: rec.mimetype,
          status: 'created',
          createdAt: new Date(),
          createdBy: 'METABUSCADOR',
          ...{
            history: [{
              id: this.utilService.uuidv4(),
              last: null,
              status: 'created',
              date: new Date(),
              user: (body.user._id.toString())
            }]
          }
        },
        metadata: {
          firstLevel: {
            pages: null,
            resolution: null,
            duration: null,
            fileSize: rec.size,
            title: (nameSplit.splice(0, (nameSplit.length - 1))).join("."),
            fileFormat: nameSplit.pop().toLowerCase(),
            recordDate: new Date(),
            accessLevel: parseInt(body.metadata.firstLevel.accessLevel, 10),
            creationDate: body.metadata.firstLevel.creationDate,
            encoding: rec.encoding,
            originalName: rec.originalName,
            geographicCoverage: []
          }
        },
        content: rec.content ? rec.content.length > 10 ? rec.content : '' : '',
        textExtraction: (rec.content ? rec.content.length > 10 ? 'Extracted' : 'Not extracted' : 'Not extracted')
      }
      const record = await this.recordModel.create(newRec);
      record.ident = `${resource.ident}-${record._id}`;
      await record.save()
      resource.records.push(record._id.toString());
      return record;
    }));

    await resourceGroup.save();
    await resource.save();

    //Generar las versiones de soporte para catalogación fuentes internas
    if (metadata.firstLevel.accessLevel === 4 && resource.origin === 'CatalogacionFuentesInternas') {
      resource.records.map(recordID => {
      })
    }

    return resource;
  }

  async logicDelete(ident, user) {
    let resource = await this.resourceModel.findOne({ "ident": ident })
    const extra = {
      ...resource.extra,
      ...{ status: 'deleted', deletedAt: new Date() },
      ...{
        history: [...resource.extra ? resource.extra.history ? resource.extra.history : [] : [], ...[{
          id: this.utilService.uuidv4(),
          last: null,
          status: 'deleted',
          date: new Date(),
          user: (user._id.toString())
        }]]
      },
    }
    const recordsAll = resource.records;
    recordsAll.map(async (record) => {
      let recordToDelete = await this.recordModel.findOne({ '_id': record }).exec();
      const extraRecord = {
        ...recordToDelete.extra ? recordToDelete.extra : {},
        ...{ status: 'deleted', deletedAt: new Date() },
        ...{
          history: [...recordToDelete.extra ? recordToDelete.extra.history ? recordToDelete.extra.history : [] : [], ...[{
            id: this.utilService.uuidv4(),
            last: null,
            status: 'deleted',
            date: new Date(),
            user: (user._id.toString())
          }]]
        }
      }
      recordToDelete.extra = extraRecord;
      recordToDelete.save();
    })
    resource.indexed = false;
    resource.type = this.utilService.is_array(resource.type) ? resource.type[0] : resource.type;
    resource.extra = extra;
    await resource.save()
    return resource;
  }

  async logicEdit({ ident, user, body, filesToDelete, records }) {
    const { metadata } = body;
    metadata.firstLevel.accessLevel = parseInt(metadata.firstLevel.accessLevel, 10);
    let resource = await this.resourceModel.findOne({ "ident": ident });
    const extra = {
      ...resource.extra ? resource.extra : {},
      ...{ status: 'updated', updatedAt: new Date() },
      ...{
        history: [...resource.extra ? resource.extra.history ? resource.extra.history : [] : [], ...[{
          id: this.utilService.uuidv4(),
          last: {
            records: resource.records,
            metadata: resource.metadata,
            type: resource.type ? resource.type : null,
          },
          status: 'updated',
          date: new Date(),
          user: (user._id.toString())
        }]]
      }
    }
    if (filesToDelete.length > 0) {
      filesToDelete.map(async (record) => {
        let recordToDelete = await this.recordModel.findOne({ '_id': record }).exec();
        const extraRecord = {
          ...recordToDelete.extra ? recordToDelete.extra : {},
          ...{ status: 'deleted', deletedAt: new Date() },
          ...{
            history: [...recordToDelete.extra ? recordToDelete.extra.history ? recordToDelete.extra.history : [] : [], ...[{
              id: this.utilService.uuidv4(),
              last: null,
              status: 'deleted',
              date: new Date(),
              user: (user._id.toString())
            }]]
          }
        }
        recordToDelete.extra = extraRecord;
        recordToDelete.save();
      })
      const r = _.xor(filesToDelete, resource.records)
      resource.records = r;
    }

    resource.extra = extra;
    resource.indexed = false;
    resource.type = this.utilService.is_array(body.metadata.firstLevel.documentType) ? body.metadata.firstLevel.documentType[0] : body.metadata.firstLevel.documentType;
    const firstLevel = {
      ...resource.metadata.firstLevel ? resource.metadata.firstLevel : null,
      ...metadata.firstLevel ? metadata.firstLevel : null
    };
    const humanRights = {
      ...resource.metadata.missionLevel ? resource.metadata.missionLevel.humanRights ? resource.metadata.missionLevel.humanRights : null : null,
      ...metadata.missionLevel ? metadata.missionLevel.humanRights ? metadata.missionLevel.humanRights : null : null
    };
    const target = {
      ...resource.metadata.missionLevel ? resource.metadata.missionLevel.target ? resource.metadata.missionLevel.target : null : null,
      ...metadata.missionLevel ? metadata.missionLevel.target ? metadata.missionLevel.target : null : null
    };
    const source = {
      ...resource.metadata.missionLevel ? resource.metadata.missionLevel.source ? resource.metadata.missionLevel.source : null : null,
      ...metadata.missionLevel ? metadata.missionLevel.source ? metadata.missionLevel.source : null : null
    };
    resource.metadata = {
      firstLevel: firstLevel,
      missionLevel: {
        humanRights: humanRights,
        target: target,
        source: source,
      },
      slug: resource.metadata.slug,
      simpleident: resource.metadata.simpleident
    };
    const recordsResults = await Promise.all(records.map(async (rec) => {
      const typeRecord = (rec.fieldname.split('-'))[0] === 'attachedRightsAuthorization' ? 'attachedRightsAuthorization' :
        this.utilService.is_array(body.metadata.firstLevel.documentType) ? body.metadata.firstLevel.documentType[0] : `${body.metadata.firstLevel.documentType}`;
      const nameSplit = rec.originalname.split(".");
      const newRec = {
        ident: this.utilService.uuidv4(),
        identifier: resource.ident,
        identifier_II: resource.identifier,
        filename: rec.path,
        resourceId: resource._id.toString(),
        origin: resource.origin,
        type: typeRecord,
        extra: {
          originalName: rec.originalname,
          mimetype: rec.mimetype,
          status: 'created',
          createdAt: new Date(),
          createdBy: 'METABUSCADOR',
          ...{
            history: [...resource.extra.history, ...[{
              id: this.utilService.uuidv4(),
              last: null,
              status: 'created',
              date: new Date(),
              user: (user._id.toString())
            }]]
          }
        },
        metadata: {
          firstLevel: {
            pages: null,
            resolution: null,
            duration: null,
            fileSize: rec.size,
            title: (nameSplit.splice(0, (nameSplit.length - 1))).join("."),
            fileFormat: nameSplit.pop().toLowerCase(),
            recordDate: new Date(),
            creationDate: body.metadata.firstLevel.creationDate,
            accessLevel: parseInt(body.metadata.firstLevel.accessLevel, 10),
            encoding: rec.encoding,
            originalName: rec.originalName,
            geographicCoverage: []
          }
        },
        content: rec.content ? rec.content.length > 10 ? rec.content : '' : '',
        textExtraction: (rec.content ? rec.content.length > 10 ? 'Extracted' : 'Not extracted' : 'Not extracted')
      }
      const record = await this.recordModel.create(newRec);
      record.ident = `${resource.ident}-${record._id}`;
      await record.save()
      resource.records.push(record._id.toString());
      return record;
    }));
    await resource.save()
    //Generar las versiones de soporte para catalogación fuentes internas
    if (metadata.firstLevel.accessLevel === 4 && resource.origin === 'CatalogacionFuentesInternas') {
      resource.records.map(recordID => {
      })
    }

    return resource;
  }

  getCountries(): Promise<any> {
    const path = 'https://services.arcgis.com/P3ePLMYs2RVChkJx/ArcGIS/rest/services/World_Countries_(Generalized)/FeatureServer/0/query?where=1%3D1&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&resultType=none&distance=0.0&units=esriSRUnit_Meter&returnGeodetic=false&outFields=FID%2CCOUNTRY%2CISO&returnGeometry=false&returnCentroid=true&featureEncoding=esriDefault&multipatchOption=xyFootprint&maxAllowableOffset=&geometryPrecision=&outSR=4326&datumTransformation=&applyVCSProjection=false&returnIdsOnly=false&returnUniqueIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&returnQueryGeometry=false&returnDistinctValues=false&cacheHint=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&having=&resultOffset=&resultRecordCount=&returnZ=false&returnM=false&returnExceededLimitFeatures=true&quantizationParameters=&sqlFormat=none&f=json&token=';
    return this.httpService.get(path).toPromise()
  }

  getDepartaments(): Promise<any> {
    const path = 'https://mapas.comisiondelaverdad.co/server/rest/services/Departamentos_cache/MapServer/0/query?where=1%3D1&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=dpto_cnmbr%2Cdpto_ccdgo&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=4326&having=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&historicMoment=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentOnly=false&datumTransformation=&parameterValues=&rangeValues=&quantizationParameters=&featureEncoding=esriDefault&f=pjson';
    return this.httpService.get(path).toPromise()
  }

  municipality(id): Promise<any> {
    const path = `https://mapas.comisiondelaverdad.co/server/rest/services/Despliegue_Territorial/MapServer/1/query?where=dpto_ccdgo%3D%27${id}%27&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&having=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&historicMoment=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentOnly=false&datumTransformation=&parameterValues=&rangeValues=&quantizationParameters=&featureEncoding=esriDefault&f=pjson`;
    return this.httpService.get(path).toPromise()
  }

  getSuggestions(text, maxSuggestions = 1) {
    const path = `https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/suggest?f=json&text=${encodeURIComponent(text)}&maxSuggestions=${maxSuggestions}`;
    return this.httpService.get(path).pipe(retry(5))
  }

  getLocation(text, magicKey, maxLocation = 1, outFields = '*') {
    const path = `https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?SingleLine=${encodeURIComponent(text)}&f=json&outSR=%7B%22wkid%22%3A102100%7D&outFields=${outFields}&magicKey=${magicKey}&maxLocations=${maxLocation}`
    return this.httpService.get(path)
  }

  getLocation2(text){
   const headersRequest = { 
    'cache-control': 'no-cache' 
    }
    const promisePoint = new Promise(async (resolve, reject) => {
    const path = `https://nominatim.openstreetmap.org/search?q=${encodeURIComponent(text)}&format=json`;
      this.httpService.get(path, { headers: headersRequest }).subscribe((response)=>{
        const datos = response.data;
        if(datos.length > 0){
          const {lat, lon} = datos[0];
          if(lat && lon){
            resolve({lat, lon});
          }
        }
      }, (error)=>{
        console.log(error);
        reject(error)
      })
    })
    return promisePoint;
  }

  async getLocationByText(text, maxLocation = 1, outFields = '*') {
    const promisePoint = new Promise(async (resolve, reject) => {
      this.getSuggestions(text).subscribe((data: any)=>{
        const { suggestions } = data.data;
        if (suggestions.length > 0) {
          let { magicKey, text} = suggestions[0];
          if(magicKey) {
            //const query = `https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?SingleLine=${encodeURIComponent(text)}&f=json&outSR=%7B%22wkid%22%3A102100%7D&outFields=*&magicKey=${magicKey}&maxLocations=1`
            this.getLocation(text, magicKey, maxLocation, outFields).subscribe((geoData: any)=> {
              const { X, Y, Xmin, Xmax, Ymin, Ymax } = geoData.data.candidates[0].attributes;
              console.log({ X, Y, Xmin, Xmax, Ymin, Ymax });
              resolve({ geoPoint: { lat: Y, lon: X }, geoRectangle: {Xmin, Xmax, Ymin, Ymax } })
            }, (error) =>  resolve({ geoPoint: { lat: null, lon: null } }))
          } else {
            resolve({ geoPoint: { lat: null, lon: null } })
          }
        } else {
          resolve({ geoPoint: { lat: null, lon: null } })
        }
      }, (error) => {
        resolve({ geoPoint: { lat: null, lon: null } })
      })
    }
    );
    return promisePoint;
  }

  async deleteRecords() {

  }

  async getParents(id): Promise<Resource[]> {
    return this.resourceModel
      .aggregate()
      .match({
        'ident': id
      })
      .graphLookup({
        from: 'resourcegroups',
        startWith: '$ResourceGroupId',
        connectFromField: 'ResourceGroupParentId',
        connectToField: 'ResourceGroupId',
        as: 'parents',
      })
      .project({
        "ident": 1,
        "ResourceGroupId": 1,
        "parents.ResourceGroupId": 1,
        "parents.ResourceGroupParentId": 1,
        "parents.metadata.firstLevel.title": 1,
      })
      .exec()
  }

  async listByParents(req, resourceGroup, from, size): Promise<{}> {

    const firstLevel = await this.resourceGroupCacheService.firstLevelCache();
    console.log(firstLevel);
    // console.log(resourceGroup)
    // // Busco el nodo 
    // function nodeSearch(resourceGroup) {
    //   // esta funcion no es async me preocupa que corra sin que firstLevel haya sido primero resuelto
    //   return firstLevel.reduce(function f(acc, child) {
    //     return (child.id === resourceGroup) ? child :
    //       (child.children && child.children.length) ? child.children.reduce(f, acc) : acc;
    //   });
    // }
    // const nodeSelected = nodeSearch(resourceGroup);
    const nodeSelected = resourceGroup;

    // Busco todos los ID
    // let id = []
    // let getAll = function (node) {
    //   if (node.children.length > 0) {
    //     id.push(...node.resourceGroup);
    //     node.children.forEach((c) => {
    //       getAll(c);
    //     });
    //   }
    //   else {
    //     id.push(...node.resourceGroup);
    //   }
    // }

    // getAll(nodeSelected);

    // console.log(resourceGroup)

    // id = [... new Set(id)];

    // se jala los rg hijos del selected

    var rg_hijos = await this.resourceGroupModel
      .find({ "ResourceGroupParentId": resourceGroup }, { "ident": 1 }).exec()

    var ids = rg_hijos.map(d => d.ident)
    ids.push(resourceGroup)

    var populate =
      [
        {
          path: "records",
          select: "-content -labelled",
          model: this.recordModel
        }
      ];

    var query = this.resourceModel
      .find({
        'ResourceGroupId': { $in: ids },
        "extra.status": {
          $ne: "deleted"
        },
      })
      .sort()
      .skip(Number((from - 1) * size))
      .limit(Number(size));

    [].concat(populate).forEach(function (item) {
      query.populate(item);
    });

    var result = {
      docs: await query.exec(),
      total: await this.resourceModel.countDocuments({ 'ResourceGroupId': { $in: ids } }).exec()
    }

    result.docs.map((r: any) => {
      if (parseInt(r.metadata.firstLevel.accessLevel, 10) >= parseInt(req.user.accessLevel, 10)) {
        r.metadata.firstLevel.originalAccessLevel = r.metadata.firstLevel.accessLevel;
        r.metadata.firstLevel.accessLevel = "4";
      }
    })
    // result.docs = result.docs.filter((r: any) => {
    //   return (r ? r.extra ? r.extra.status ? r.extra.status !== 'deleted' : true : true : true)
    // })
    console.log(result)
    return result;
  }

  async findListByParam(file, param, collectionType) {
    const statment = await this.formsService.getFormStatment(collectionType)
    const compileData = await this.utilService.xlsxCsvToJson(file, statment);
    //console.log(compileData);
    // -- validamos las listas de la data a actualizar
    let failedValueLists = [];
    let failedValues = await Promise.all(compileData.map(async (meta) => {
      let valuesInOptions = await Promise.all(statment.map(async (element: any) => {
        if ([
          "select",
          "select-multiple",
          "select-recursive",
          "select-multiple2"
        ]
          .includes(element.type) && meta[element.origin] && meta[element.origin].length > 0 &&
          !meta[element.origin].includes("Sin especificar")) {
          try {
            if(meta[element.origin] !== 'null'){              
              const dataList = await this.listModel.findOne({ "path": element.origin });
  
              if (dataList) {
                const options = await this.listService.getFlatList(dataList._id);
                //console.log("options", options);
                const optionsFromExcel = this.utilService.is_array(meta[element.origin]) ? meta[element.origin] : [meta[element.origin]];
                  // var options = [];
                  // for (let i = 0; i < dataList.options.length; i++) {
                  //   const option = dataList.options[i];
                  //   const dataOption = await this.optionModel.findOne({ _id: option });
                  //   if (dataOption) {
                  //     options.push(dataOption.term);
                  //   }
                  // }
                  //console.log(optionsFromExcel);
                  let diff = _.difference(optionsFromExcel, options);
                  //console.log( JSON.stringify(optionsFromExcel), diff)
                if ((diff.length === 0)) {
                  return { list: element.origin, contained: true, difference: null };
                } else {
                  meta[element.origin] = null;
                  return { list: element.origin, contained: false, difference: diff };
                }
              }
            }
          } catch (error) {
            console.log('error: ', error);
          }
        }
      }));
      return { ident: meta.ident, title: meta.title, valuesInOptions: valuesInOptions };
    }));
    failedValues.forEach((res: any) => {
      res.valuesInOptions.forEach(element => {
        if (element && !element.contained) {
          failedValueLists.push({ ident: res.ident, title: res.title, list: element.list, value: element.difference, observacion: "La lista contiene opciones no válidas, por lo tanto, no será actualizada." })
        }
      });
    });

    //console.log(compileData);
    // if(compileData.lenght > 0) {
    //   this.utilService.removeFile(file);
    // }
    //console.log(compileData)

    const resourcesFound = await Promise.all(compileData.map(async (meta) => {
      if (meta[param]) {
        const { destiny } = <any>(statment.filter((statmentEl) => (statmentEl["origin"] === param)))[0]
        const filter = { [destiny]: meta[param] };
        //console.log(filter);
        const element = (collectionType === 'RESOURCEGROUPS_FAE') ?
          await this.resourceGroupModel.findOne(filter) :
          await this.resourceModel.findOne(filter);
        //console.log(element)
        return element ? element : {};
      } else {
        return {};
      }
    }));
    const resourcesTransform = await this.utilService.objectTransformationInverse(resourcesFound, statment);
    const mergeData = compileData.map((compile, index) => {
      let newCompile = {};
      for (let c in compile) {
        if (compile.hasOwnProperty(c)) {
          if (compile[c] !== null) {
            if (c !== 'ident') {
              const field = <any>statment.filter((field: any) => (field.origin === c));
              newCompile = { ...newCompile, ...{ [c]: compile[c] === 'null' ? field[0].defaultValue : compile[c] } }
            }
          }
        }
      }
      // console.log(resourcesTransform);
      const merge = { ...resourcesTransform[index], ...newCompile };
      return merge;
    }).filter((mergeObject) => (mergeObject.ident !== 'REGISTRO NO ENCONTRADO'));
    //console.log(mergeData);
    const updateData = mergeData.map((data) => (this.utilService.objectTransformation(data, statment)));
    return { compileData: compileData, resourcesFound: resourcesTransform, mergeData: mergeData, updateData: updateData, failedValueLists: failedValueLists }
  }

  async getResourcesAndResourcegroups(path, term) {
    const nameResourcegroups = ['RESOURCEGROUPS_FAE'];
    const nameResources = ['RESOURCES_FAE', 'RESOURCES_FAI', 'RESOURCES_OBJ', 'COLABORATIVE_FORM'];
    let dataReturn = {};
    const allForms = await this.formsService.findAll();

    const fields = allForms.map((data) => {
      return {
        name: data.name,
        fields: data.fields.filter((f: any) => (typeof f.list !== 'undefined' && f.list == path)).map((f: any) => (f.destiny))
      }
    })

    const resourcesDestiny = (fields.filter((f) => (nameResources.includes(f.name) && f.fields.length > 0))
      .map(f => f.fields))
    const resourcegroupDestiny = fields.filter((f) => (nameResourcegroups.includes(f.name) && f.fields.length > 0))
      .map(f => f.fields)

    if (resourcesDestiny.length > 0) {
      const pathsr = resourcesDestiny
        .reduce((prev, acum) => ([...prev, ...acum]))

      const resourcesfounded = await Promise.all(pathsr.map(async (p) => {
        const filter = { [p]: term };
        return this.resourceModel.find(filter, `${p} metatadata.firstLevel.title ident ResourceGroupId origin`)
      }))
      if (JSON.stringify(resourcesfounded) !== '[[]]') {
        dataReturn = {
          ...dataReturn,
          ...{ resources: resourcesfounded.reduce((prev: any, acum: any) => ([...prev, ...acum])) }
        }
      }
      if (resourcegroupDestiny.length > 0) {
        const pathsrg = resourcegroupDestiny
          .reduce((prev, acum) => ([...prev, ...acum]))
        const resourceGroupsfounded = await Promise.all(pathsrg.map(async (p) => {
          const filter = { [p]: term };
          return this.resourceGroupModel.find(filter, `${p} metatadata.firstLevel.title ident ResourceGroupParentId origin`)
        }))
        if (JSON.stringify(resourceGroupsfounded) !== '[[]]') {
          dataReturn = {
            ...dataReturn,
            ...{ resourcegroups: resourceGroupsfounded.reduce((prev: any, acum: any) => ([...prev, ...acum])) }
          }
        }
      }
    }
    return dataReturn;
  }



  async moveResourceToResourcegroup(resource, rgDestiny, rgOrigin, user) {
    const resourceGroupDestiny = await this.resourceGroupModel.findOne({ "ident": rgDestiny });
    const resource1 = await this.resourceModel.findOne({ "ident": resource });
    const resourceGroupOrigin = await this.resourceGroupModel.findOne({ "ident": rgOrigin });

    const extra = {
      ...resource1.extra ? resource.extra : {},
      ...{ status: 'updated', updatedAt: new Date() },
      ...{
        history: [...resource1.extra ? resource1.extra.history ? resource1.extra.history : [] : [], ...[{
          id: this.utilService.uuidv4(),
          last: {
            identifier: resource1.identifier,
            ResourceGroupId: resource1.ResourceGroupId
          },
          status: 'updated',
          date: new Date(),
          user: (user._id.toString())
        }]]
      }
    }

    const recordsAll = resource1.records;
    recordsAll.map(async (record) => {
      let recordToMove = await this.recordModel.findOne({ '_id': record }).exec();
      if (recordToMove) {
        const extraRecord = {
          ...recordToMove.extra ? recordToMove.extra : {},
          ...{ status: 'updated', updatedAt: new Date() },
          ...{
            history: [...recordToMove.extra ? recordToMove.extra.history ? recordToMove.extra.history : [] : [], ...[{
              id: this.utilService.uuidv4(),
              last: {
                identifier_II: recordToMove.identifier_II,
              },
              status: 'updated',
              date: new Date(),
              user: (user._id.toString())
            }]]
          }
        }
        recordToMove.extra = extraRecord;
        recordToMove.identifier_II = resourceGroupDestiny.ident;
        recordToMove.save();
      }
    })

    const newResourcesOrigin = _.xor([resource1._id.toString()], resourceGroupOrigin.resources);
    const newResourcesDestiny = _.union([resource1._id.toString()], resourceGroupDestiny.resources);

    const extraResourceGroupDestiny = {
      ...resourceGroupDestiny.extra,
      ...{ status: 'updated', updatedAt: new Date() },
      ...{
        history: [...resourceGroupDestiny.extra ? resourceGroupDestiny.extra.history ? resourceGroupDestiny.extra.history : [] : [], ...[{
          id: this.utilService.uuidv4(),
          last: {
            resources: resourceGroupDestiny.resources,
          },
          status: 'updated',
          date: new Date(),
          user: (user._id.toString())
        }]]
      }
    }
    const extraResourceGroupOrigin = {
      ...resourceGroupOrigin.extra,
      ...{ status: 'updated', updatedAt: new Date() },
      ...{
        history: [...resourceGroupOrigin.extra ? resourceGroupOrigin.extra.history ? resourceGroupOrigin.extra.history : [] : [], ...[{
          id: this.utilService.uuidv4(),
          last: {
            resources: resourceGroupOrigin.resources,
          },
          status: 'updated',
          date: new Date(),
          user: (user._id.toString())
        }]]
      }
    }

    resource1.extra = extra;
    resource1.identifier = resourceGroupDestiny.ident;
    resource1.ResourceGroupId = resourceGroupDestiny.ident;
    resource1.type = this.utilService.is_array(resource1.type) ? resource1.type[0] : resource1.type;

    resourceGroupDestiny.resources = newResourcesDestiny;
    resourceGroupDestiny.extra = extraResourceGroupDestiny;
    resourceGroupOrigin.resources = newResourcesOrigin;
    resourceGroupOrigin.extra = extraResourceGroupOrigin;

    await resourceGroupDestiny.save()
    await resourceGroupOrigin.save()
    await resource1.save()
    return resource1;

  }

}