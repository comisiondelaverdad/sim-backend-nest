import { Injectable } from '@nestjs/common';
import { BehaviorSubject, interval } from 'rxjs';
import { ResourceService } from './resource.service';

@Injectable()
export class ResourceCacheService {

    private countriesSubject = new BehaviorSubject([]);
    private countries$ = this.countriesSubject.asObservable();
    private countries: any[];

    private departamentsSubjects = new BehaviorSubject([]);
    private departaments$ = this.departamentsSubjects.asObservable();
    private departaments: any[];

    private countSubjects = new BehaviorSubject(0);
    private count$ = this.countSubjects.asObservable();
    private count: number = 0;

    constructor(private readonly resourceService: ResourceService) { }

    init() {
        this.resourceService.getCountries().then((response) => {
            this.countriesSubject.next(response);
        });

        this.resourceService.getDepartaments().then((response) => {
            this.departamentsSubjects.next(response);
        });

        this.resourceService.count().then((response) => {
            this.countSubjects.next(response);
        });

        this.countries$.subscribe((response) => {
            this.countries = response;
        })

        this.departaments$.subscribe((response) => {
            this.departaments = response;
        })

        this.count$.subscribe((response) => {
            this.count = response;
        })

        const timer = interval(300000); // cache update 5 min
        timer.subscribe(async (x) => {
            this.count = await this.resourceService.count();
            this.countSubjects.next(this.count);
        })
    }

    getDepartamentsCache() {
        let departamentsPromise = new Promise((resolve) => {
            if (this.departaments) {
                resolve(this.departaments)
            } else {
                this.departaments$.subscribe((response: any) => {
                    if (response.length > 0) {
                        resolve(response);
                    }
                })
            }
        })
        return departamentsPromise;
    }

    getCountriesCache() {
        let countriesPromise = new Promise((resolve) => {
            if (this.countries) {
                resolve(this.countries);
            } else {
                return this.countries$.subscribe((response: any) => {
                    if (response.length > 0) {
                        resolve(response);
                    }
                })
            }
        })
        return countriesPromise;
    }

    getCount() {
        let countPromise = new Promise((resolve) => {
            if (this.count) {
                resolve(this.count);
            } else {
                return this.count$.subscribe((response: any) => {
                    if (response) {
                        resolve(response);
                    }
                })
            }
        })
        return countPromise;
    }
}
