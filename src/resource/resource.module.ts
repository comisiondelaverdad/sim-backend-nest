import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';

import { ConfigModule } from '../config/config.module';
import { ResourceController } from './resource.controller';
import { ResourceService } from './resource.service';
import { SearchModule } from '../search/search.module';
import { SearchService } from '../search/search.service';
import { ResourceSchema } from './schema/resource.schema';
import { RecordModule } from '../record/record.module';
import { ResourceGroupModule } from 'src/resource-group/resource-group.module';
import { SearchHistorySchema } from '../search/schema/search-history.schema';
import { MulterModule } from '@nestjs/platform-express';
import { ConfigService } from '../config/config.service';
import { ResourceCacheService } from './resource-cache.service';
import { ResourceGroupCacheService } from '../resource-group/resource-group-cache.service';
import { UtilService } from './../util/util.service'
import { LocationModule } from '../admin/location/location.module';
import { LocationService } from '../admin/location/location.service';
import { Location, LocationSchema } from '../location/schema/location.schema';
import { FormsService } from 'src/forms/forms.service';
import { FormsModule } from 'src/forms/forms.module';
import { ListSchema } from 'src/list/schema/list.schema';
import { OptionSchema } from 'src/option/schema/option.schema';
import { SlugIdentModule } from '../slug-ident/slug-ident.module';
import { UserModule } from 'src/user/user.module';
import { ListModule } from 'src/list/list.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Resource', schema: ResourceSchema }], "database"),
    MongooseModule.forFeature([{ name: 'List', schema: ListSchema }], "database"),
    MongooseModule.forFeature([{ name: 'Option', schema: OptionSchema }], "database"),
    MongooseModule.forFeature([{ name: 'SearchHistory', schema: SearchHistorySchema }], "database"),
    MongooseModule.forFeature([{ name: 'Location', schema: LocationSchema, collection: 'geo-locations' }], "database"),
    LocationModule,
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        dest: configService.get('DESTINATION_INTERNAL_FILES'),
      }),
      inject: [ConfigService],
    }),
    HttpModule,
    SearchModule,
    ConfigModule,
    RecordModule,
    forwardRef(() => ResourceGroupModule),
    forwardRef(() => UserModule),
    forwardRef(() => ListModule),
    FormsModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    SlugIdentModule
  ],
  controllers: [ResourceController],
  providers: [ResourceService, SearchService, ResourceCacheService, ResourceGroupCacheService, UtilService, LocationService],
  exports: [ResourceService, ResourceCacheService, MongooseModule]
})
export class ResourceModule { }
