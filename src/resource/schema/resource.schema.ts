import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
//import * as mongoosePaginate from 'mongoose-paginate';

@Schema()
export class Resource extends Document {
  @Prop({type:"string"})
  identifier: string;

  @Prop({type:"string"})
  ident: string;

  @Prop({type:"string"})
  type: string;

  @Prop({type:"object"})
  metadata: Object;

  @Prop({type:"array"})
  records: string[];

  @Prop({type:"string"})
  origin: string;

  @Prop({type:"string"})
  ResourceGroupId: string;

  @Prop({type:"boolean"})
  indexed:boolean;

  //@Prop()
  //ResourceGroupParentId: string;

  @Prop({type:"object"})
  extra: Object;
}


export const ResourceSchema = SchemaFactory.createForClass(Resource);
//ResourceSchema.plugin(mongoosePaginate);
