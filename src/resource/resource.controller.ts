import {
  Controller,
  UseInterceptors,
  UploadedFiles,
  HttpCode,
  Get,
  Param,
  Query,
  Request,
  Post,
  Body,
  UseGuards,
  Delete,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { matchRolesList } from '../auth/roles.guard';
import { ResourceService } from './resource.service';
import { Resource } from './schema/resource.schema';
import { SearchService } from '../search/search.service';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { ConfigService } from './../config/config.service';
import { User } from 'src/user/schema/user.schema';
import { ResourceCacheService } from './resource-cache.service';
import { UtilService } from 'src/util/util.service';
import * as _ from 'lodash';
import { departaments, countries } from "./../geo-shapes/departamentos-municipios";
export let DESTINATION_INTERNAL_FILES = '';

export class ResponseCount {
  count: number;
  to: Date;
  from?: Date;
}
import { ApiTags, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('resources')
@Controller('api/resources')
export class ResourceController {
  constructor(
    private readonly resourceService: ResourceService,
    private readonly resourceCacheService: ResourceCacheService,
    private readonly searchService: SearchService,
    private utils: UtilService,

    private configService: ConfigService,
  ) {
    console.log('inicialize resources cache ...');
    this.resourceCacheService.init();
    DESTINATION_INTERNAL_FILES = this.configService.get(
      'DESTINATION_INTERNAL_FILES',
    );
  }

  @Get('count')
  @UseGuards(AuthGuard())
  count() {
    return this.resourceCacheService.getCount().then((c) => {
      return { count: c, name: 'Cantidad de Recursos' };
    });
  }

  @Get('count/:to/to')
  async countBetweenDates(
    @Param() params,
    @Query() query,
  ): Promise<ResponseCount> {
    const to = new Date(params.to);
    const from = query.from ? new Date(query.from) : undefined;
    return this.resourceService
      .countBetweenDates(
        to,
        query.origin ? query.origin : 'CatalogacionFuentesInternas',
        from,
      )
      .then((c) => {
        return { count: c, to: to, from: from };
      });
  }

  @Get('catalogacionInternasPorTiempo/:to/to')
  async resourceConteoTemporalCatalogacionFuentesInternas(
    @Param() params,
    @Query() query,
  ): Promise<any> {
    const to = new Date(params.to);
    const dataRangeArray = [];
    const currentDate = new Date();
    for (
      let start = new Date(params.to);
      start <= currentDate;
      start.setDate(start.getDate() + 31)
    ) {
      dataRangeArray.push(new Date(start));
    }
    const obj = [];
    const users = query.users ? query.users.split(',') : [];
    return await this.resourceService
      .resourceConteoTemporalCatalogacionFuentesInternas(to, users)
      .then((c) => {
        c.forEach((element: any) => {
          const historyDate = new Date(element.extra.history[0].date);
          dataRangeArray.forEach((date) => {
            const thisDate = { date, count: 0 };
            const yaEsta = obj.findIndex((el) => el.date === date);
            if (historyDate.getTime() <= date.getTime()) {
              if (yaEsta !== -1) {
                obj[yaEsta].count++;
              } else {
                obj.push(thisDate);
              }
            }
          });
        });
        const obk = obj.map((el, index) => {
          return {
            date: el.date,
            count: el.count,
            currentMonth: index > 1 ? el.count - obj[index - 1].count : 0,
          };
        });
        return obk.sort(
          (a, b) => new Date(a.date).getTime() - new Date(b.date).getTime(),
        );
      });
  }

  @Get('catalogoInternasPorTiempoEnId')
  async catalogoInternasPorTiempoEnId(@Query() query): Promise<any> {
    const users = query.users ? query.users.split(',') : [];
    return await this.resourceService
      .catalogoInternasPorTiempoEnId(users, query.from, query.to)
      .then((c) => {
        const response = c.map((element: any) => {
          return {
            title: element.metadata.firstLevel.title,
            users: element.extra.history.map((history) => ({
              user: history.user,
              date: history.date,
              status: history.status,
            })),
            records: element.records.length,
            date: element.extra.history[0].date,
            clear: element.extra.history.every(
              (history) => history.user === element.extra.history[0].user,
            ),
          };
        });
        return { count: response.length, response };
      });
  }

  @Get('conteoTemporalMetaBuscador/:to/to')
  async conteoTemporalMetaBuscador(@Param() params): Promise<any> {
    const to = new Date(params.to);

    const dataRangeArray = this.utils.dateDatesRange(to, new Date(), 15);
    const obj = [];

    return await this.resourceService
      .conteoTemporalMetaBuscador(to)
      .then((c) => {
        c.forEach((element: any) => {
          const historyDate = new Date(element.extra.history[0].date);
          dataRangeArray.forEach((date) => {
            const thisDate = { date, count: 0 };
            const yaEsta = obj.findIndex((el) => el.date === date);
            if (historyDate.getTime() <= date.getTime()) {
              if (yaEsta !== -1) {
                obj[yaEsta].count++;
              } else {
                obj.push(thisDate);
              }
            }
          });
        });
        return obj.sort(
          (a, b) => new Date(a.date).getTime() - new Date(b.date).getTime(),
        );
      });
  }

  @Get('conteoRecursosPorOrigen')
  async conteoRecursosPorOrigen(@Query() query): Promise<ResponseCount> {
    return this.resourceService
      .conteoRecursosPorOrigen(
        query.origin ? query.origin : 'CatalogacionFuentesInternas',
      )
      .then((c) => {
        return { count: c, to: new Date() };
      });
  }

  @Get('countries')
  @UseGuards(AuthGuard())
  async countries() {
    return countries;
  }

  @Get('departaments')
  @UseGuards(AuthGuard())
  async departaments() {
    const dep =  departaments.map((d: any) => {
      const { name, value, geoPoint, geoRectangle } = d;
      return { name, value, geoPoint, geoRectangle };
    });
    return _.orderBy(dep, [(d) => d.name.toLowerCase()], ['asc']);
  }


  @Get('getInfoDepartamentMunicipality')
  @UseGuards(AuthGuard())
  async infoDepartamentMunicipality() {
     let dataDepartaments = [];
    // let municipalityNull = {count: 0, all: 0,list:[]}
    // let departamentsNull = {count: 0, all: 0, list:[]}
    // const location: any =  departaments;
    // if (location.length > 0) {
    //   for (let i = 0; i < location.length; i++) {
    //     if (location[i].geoPoint.lat === null && location[i].geoPoint.lon === null) {
    //       departamentsNull.count += 1;
    //       departamentsNull.list.push({code: location[i].code, text: location[i].text.split(',').pop()});
    //       location[i].geoPoint = <any> await this.resourceService.getLocation2(location[i].text);
    //       console.log(location[i].text, location[i].geoPoint );
    //     }
    //     departamentsNull.all += 1;
    //     const { municipality } = location[i];
    //     if (municipality.length > 0) {
    //       for (let j = 0; j < municipality.length; j++) {
    //         if (municipality[j].geoPoint.lat === null && municipality[j].geoPoint.lon === null) {
    //           municipalityNull.count += 1;
    //           municipalityNull.list.push({code: municipality[j].code, text: municipality[j].text.split(',').pop()});

    //           // const { geoPoint } = <any> await this.resourceService.getLocationByText(municipality[j].text);
    //           // municipality[j].geoPoint = geoPoint;
    //           // console.log(municipality[j].text, geoPoint );
    //         }
    //         municipalityNull.all += 1;
    //       }
    //     }
    //   }
    //   dataDepartaments = location;
    // }
    //} else {
      let dep: any = await this.resourceCacheService.getDepartamentsCache();
      for(let i = 0; i< dep.data.features.length; i ++ ){
        const d = dep.data.features[i];
        //let sleep = await this.utils.sleep(this.getRandomArbitrary(500, 2000));
        console.log(`COLOMBIA, ${d.attributes.dpto_cnmbr}` );
        const { geoPoint, geoRectangle } = <any> await this.resourceService.getLocationByText(`COLOMBIA, ${d.attributes.dpto_cnmbr}`);
        console.log(`point`, { geoPoint, geoRectangle } );
        let departamentElement = {
          name: d.attributes.dpto_cnmbr,
          value: d.attributes.dpto_ccdgo,
          text: `COLOMBIA, ${d.attributes.dpto_cnmbr}`,
          code: `CO-${d.attributes.dpto_ccdgo}`,
          geoPoint: geoPoint,
          geoRectangle: geoRectangle
        }
        let mun = await this.resourceService.municipality(d.attributes.dpto_ccdgo);
        const municipalityArray = []
        for(let j = 0; j < mun.data.features.length; j ++ ){
          const m = mun.data.features[j];
          console.log(`COLOMBIA, ${d.attributes.dpto_cnmbr}, ${m.attributes.mpio_cnmbr}`);
          const { geoPoint, geoRectangle } = <any> await this.resourceService.getLocationByText(`COLOMBIA, ${d.attributes.dpto_cnmbr}, ${m.attributes.mpio_cnmbr}`)
          console.log(`point`, { geoPoint, geoRectangle });
          let municipalityElement = {
            name: m.attributes.mpio_cnmbr,
            value: m.attributes.mpio_ccdgo,
            text: `COLOMBIA, ${d.attributes.dpto_cnmbr}, ${m.attributes.mpio_cnmbr}`,
            code: `CO-${d.attributes.dpto_ccdgo}-${m.attributes.mpio_ccdgo}`,
            geoPoint: geoPoint,
            geoRectangle: geoRectangle
          }
          municipalityArray.push(municipalityElement);
        }
        departamentElement = {...departamentElement, ...{municipality: municipalityArray}}
        dataDepartaments.push(departamentElement);
        this.utils.writeJson("location.json", dataDepartaments);
      }
    // //}
    return { dataDepartaments };
  }

  @Get('getMyResources')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async getMyResources(@Request() req) {
    let returnData = await this.resourceService.getMyResources(req.user);
    return returnData;
  }

  @Get('municipality/:id')
  @UseGuards(AuthGuard())
  async municipality(@Param('id') id) {
    let { municipality } = departaments.find((d) => d.value == id);
    const mun = municipality.map((mun) => {
      const { name, value, geoPoint, geoRectangle } = mun;
      return { name, value, geoPoint, geoRectangle };
    });
    return _.orderBy(mun, [(m) => m.name.toLowerCase()], ['asc']);
  }

  @Get(':id')
  @UseGuards(AuthGuard())
  async getById(@Request() req, @Param('id') id): Promise<Resource[]> {
    let resource = this.resourceService.getById(req, id);
    return resource;
  }

  @Get('/byident/:ident')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async getByIdent(@Request() req, @Param('ident') ident): Promise<Resource> {
    let resource = this.resourceService.getByIdSimple(req, ident);
    return resource;
  }

  @Get('/checkIn/:id')
  @UseGuards(AuthGuard())
  async getCheckInById(@Request() req, @Param('id') id): Promise<any> {
    let resource = await this.resourceService.getById(req, id);
    this.searchService.logger(req.user, 'view', resource);
    return { result: 'ok' };
  }

  @Get(':id/resources-parents')
  async getParents(@Param('id') id): Promise<Resource> {
    let parents = await this.resourceService.getParents(id);
    return parents.shift();
  }

  @Delete(':id')
  @UseGuards(AuthGuard())
  async delete(@Request() req, @Param('id') ident): Promise<User> {
    matchRolesList(req.user, ['admin', 'catalogador_gestor']); //Validación de roles del usuario
    return this.resourceService.logicDelete(ident, req.user);
  }

  @Post('listByParents')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async listByParents(
    @Request() req,
    @Query() query,
    @Body() body,
  ): Promise<any> {
    console.log(body);
    let resources = this.resourceService.listByParents(
      req,
      body.resourceGroups,
      query.from,
      query.size,
    );
    return resources;
  }

  @Post('moveResource')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async moveResource(
    @Request() req,
    @Query() query,
    @Body() body,
  ): Promise<any> {
    const rolesFunction = matchRolesList(req.user, ['catalogador_gestor']);
    let resourceUpdate = await this.resourceService.moveResourceToResourcegroup(
      body.resource,
      body.resourceGroupDestiny,
      body.resourceGroupOrigin,
      req.user,
    );
    return resourceUpdate;
  }

  @Post('getAllResources')
  @HttpCode(200)
  @UseGuards(AuthGuard())
  async getResources(
    @Request() req,
    @Query() query,
    @Body() body,
  ): Promise<any> {
    return await this.resourceService.getResources({ body });
  }

  @Post('ilustration')
  @UseGuards(AuthGuard())
  @UseInterceptors(
    AnyFilesInterceptor({
      storage: diskStorage({
        destination: (req, file, cb) => {
          cb(null, DESTINATION_INTERNAL_FILES);
        },
        filename: (req, file, cb) => {
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          return cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  async uploadedIlustration(@UploadedFiles() files, @Request() req) {
    if (files[0]) {
      const data = JSON.stringify(files[0])


    }
    return files;
  }

  @Post('files')
  @UseGuards(AuthGuard())
  @UseInterceptors(
    AnyFilesInterceptor({
      storage: diskStorage({
        destination: (req, file, cb) => {
          cb(null, DESTINATION_INTERNAL_FILES);
        },
        filename: (req, file, cb) => {
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          return cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  async uploadedFile(
    @UploadedFiles() files,
    @Request() req,
    @Query() query,
    @Body() body,
  ) {
    const filesInfo = JSON.parse(JSON.stringify(files));
    const contents = await filesInfo.map(async (f) => {
      const fileFormat = f.originalname.split('.').pop().toLowerCase();
      return this.utils.getText(f.path, fileFormat);
    });

    const dataFiles = await Promise.all(contents);

    dataFiles.map(async (textContent, index) => {
      filesInfo[index] = { ...filesInfo[index], ...{ content: textContent } };
      return filesInfo[index];
    });

    const rolesFunction = matchRolesList(req.user, [
      'catalogador',
      'catalogador_gestor',
    ]);
    const newResource = {
      ...JSON.parse(body.body),
      ...{ records: filesInfo },
      ...{ user: req.user },
      ...{ resourceGroup: body.resourcegroup },
    };
    let returnData = await this.resourceService.postResource(newResource);
    return returnData;
  }

  @Post('masive')
  @UseGuards(AuthGuard())
  @UseInterceptors(
    AnyFilesInterceptor({
      storage: diskStorage({
        destination: (req, file, cb) => {
          cb(null, DESTINATION_INTERNAL_FILES);
        },
        filename: (req, file, cb) => {
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          return cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  async uploadTemplate(
    @UploadedFiles() files,
    @Request() req,
    @Query() query,
    @Body() body,
  ) {
    const filesInfo = JSON.parse(JSON.stringify(files));
    const findBy = JSON.parse(body.body);
    return await this.resourceService.findListByParam(
      filesInfo[0],
      findBy.fieldBase,
      findBy.collectionType,
    );
  }

  @Post('editFiles')
  @UseGuards(AuthGuard())
  @UseInterceptors(
    AnyFilesInterceptor({
      storage: diskStorage({
        destination: (req, file, cb) => {
          cb(null, DESTINATION_INTERNAL_FILES);
        },
        filename: (req, file, cb) => {
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          return cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  async editFiles(
    @UploadedFiles() files,
    @Request() req,
    @Query() query,
    @Body() body,
  ) {
    const filesInfo = JSON.parse(JSON.stringify(files));

    const contents = await filesInfo.map(async (f) => {
      const fileFormat = f.originalname.split('.').pop().toLowerCase();
      return this.utils.getText(f.path, fileFormat);
    });

    const dataFiles = await Promise.all(contents);

    dataFiles.map(async (textContent, index) => {
      filesInfo[index] = { ...filesInfo[index], ...{ content: textContent } };
      return filesInfo[index];
    });

    const rolesFunction = matchRolesList(req.user, [
      'catalogador',
      'catalogador_gestor',
      'user',
    ]);

    const newResource = {
      ...{ body: body.body ? JSON.parse(body.body) : {} },
      ...{
        filesToDelete: body.filesToDelete ? JSON.parse(body.filesToDelete) : [],
      },
      ...{ records: filesInfo },
      ...{ user: req.user },
      ...{ ident: JSON.parse(body.ident) },
    };
    let returnData = await this.resourceService.logicEdit(newResource);
    return returnData;
  }

  @Post('')
  @UseGuards(AuthGuard())
  async createResource(@Request() req, @Body() body) {
    let returnData = await this.resourceService.postResource(body);
    return returnData;
  }
}
