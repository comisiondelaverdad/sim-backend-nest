import { Injectable } from '@nestjs/common';
import * as _ from "lodash"
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Bookmark } from './schema/bookmark.schema';
import { CreateBookmarkDto } from './dto/create-bookmark.dto';


@Injectable()
export class BookmarkService {
  constructor(@InjectModel('Bookmark') private bookmarkModel: Model<Bookmark>) {}

  /**
   * BOOKMARKS
   */

  async create(createBookmarkDto: CreateBookmarkDto): Promise<Bookmark> {
    const createdBookmark = new this.bookmarkModel(createBookmarkDto);
    return createdBookmark.save();
  }

  async read(): Promise<Bookmark[]> {
    return this.bookmarkModel.find({}).exec();
  }  

  async update(id: string, updateBookmarkDto: CreateBookmarkDto): Promise<Bookmark> {
    return this.bookmarkModel.findOneAndUpdate({"_id":id}, updateBookmarkDto).exec();
  }

  async delete(id: string): Promise<Bookmark> {
    return this.bookmarkModel.deleteOne({"_id":id}).exec();
  }

  async getByUser(id: string): Promise<Bookmark[]> {
    return this.bookmarkModel.find({user:  id}).exec();
  }

  /**
   * LABELS
   */

  async getLabelsByUser(id): Promise<string[]> {
    return this.bookmarkModel.find({user:  id}).exec()
        .then((bookmarks)=>{
          let labels = _.flatMap(bookmarks, "labels")
          let lowerCase = _.map(labels, (v)=>{ return v.toLowerCase() })
          return _.uniq(lowerCase).sort()
        })
  }

  async updateLabels(id: string, newLabel, oldLabel): Promise<Bookmark> {
    return this.bookmarkModel.updateMany({
          "user": id
      },{
          $set : {"labels.$[labels]" : newLabel }
      }, {
          arrayFilters : [{ "labels" : new RegExp(oldLabel,"i") }]
      }).exec()
  }

}
