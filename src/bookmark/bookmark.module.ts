import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { BookmarkController } from './bookmark.controller';
import { BookmarkService } from './bookmark.service';
import { Bookmark, BookmarkSchema } from './schema/bookmark.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Bookmark', schema: BookmarkSchema }], "database"),
    PassportModule.register({ defaultStrategy: 'jwt' })
  ],
  controllers: [BookmarkController],
  providers: [BookmarkService]
})
export class BookmarkModule { }