import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Bookmark extends Document {
  @Prop({type:"string"})
  user: string;

  @Prop({type:"string"})
  path: string;

  @Prop({type:"string"})
  title: string;
  @Prop({type:"object"})
  description: Object;

  @Prop({type:"array"})
  labels: Array<string>;

  @Prop({type:"object"})
  extra: object;
}


export const BookmarkSchema = SchemaFactory.createForClass(Bookmark);