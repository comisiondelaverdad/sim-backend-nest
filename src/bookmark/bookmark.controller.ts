import { Controller, Get, Post, Put, Body, Param, Delete, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/roles.guard';
import { BookmarkService } from './bookmark.service';
import { Bookmark } from './schema/bookmark.schema';
import { CreateBookmarkDto } from './dto/create-bookmark.dto';

import { ApiTags, ApiOperation , ApiBearerAuth} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('bookmarks')
@Controller('api/bookmarks')
export class BookmarkController {

  constructor(private readonly bookmarkService: BookmarkService) { }

  /**
   * BOOKMARKS
   */

  @Post()
  @UseGuards(AuthGuard())
  async create(@Body() createBookmarkDto: CreateBookmarkDto) {
    return await this.bookmarkService.create(createBookmarkDto);
  }

  @Get()
  @UseGuards(AuthGuard())
  async read(): Promise<Bookmark[]> {
    return this.bookmarkService.read();
  }

  @Put(':id')
  @UseGuards(AuthGuard())
  async update(@Param('id') id, @Body() createBookmarkDto: CreateBookmarkDto) {
    return await this.bookmarkService.update(id, createBookmarkDto);
  }

  @Delete(':id')
  @UseGuards(AuthGuard())
  async delete(@Param('id') id) {
    return await this.bookmarkService.delete(id);
  }

  @Get(':id')
  @UseGuards(AuthGuard())
  async getByUser(@Param('id') id): Promise<Bookmark[]> {
    return this.bookmarkService.getByUser(id);
  }

  /**
   * LABELS
   */

  @Get("labelsbyuser/:id")
  @UseGuards(AuthGuard())
  async getLabelsByUser(@Param('id') id): Promise<string[]> {
    return this.bookmarkService.getLabelsByUser(id);
  }

  @Put('labelsbyuser/:id')
  @UseGuards(AuthGuard())
  async updateLabels(@Param('id') id, @Body() body) {
    return await this.bookmarkService.updateLabels(id, body.newLabel, body.oldLabel);
  }

}
