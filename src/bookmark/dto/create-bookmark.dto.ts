export class CreateBookmarkDto {
  readonly user:String;
  readonly path:String;
  readonly title:String;
  readonly labels:Array<String>;
  readonly description:Object;
  readonly geographicCoverage:Object;
}


