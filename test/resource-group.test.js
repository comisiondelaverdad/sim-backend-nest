/* global describe, it */
import chai from 'chai';
import chaiHttp from 'chai-http';
import rp from 'request-promise';

const expect = require('chai').expect;
chai.use(chaiHttp);

const host = `http://localhost:4000`;
var resourcegroups;

describe('Resource-Group Controller Test', function() {
    before(function(done) {
        getAllResourceGroups(done)
      });

    describe('Get One', function() {

        describe('Ident', function() {
               it('should exists', (done) => {
                   
                   chai.request(host)
                   .get('/api/resource-groups/011-VZ')             
                   .end( function(err,res){                                 
                       expect(res.body).hasOwnProperty('ident');
                       done();
                   });
                  
              });
               it('should be string', (done) => {

                     chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(typeof res.body.ident).to.eql('string');
                           done();
                       });
                     
                });
                
                it('should not be empty', (done) => {
                    
                    chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(res.body.ident.length).to.above(0);
                           done();
                       });
                                         
                });
    
             });

        describe('Identifier', function() {
               it('should exists', (done) => {
                   
                   chai.request(host)
                   .get('/api/resource-groups/011-VZ')             
                   .end( function(err,res){                                 
                       expect(res.body).hasOwnProperty('identifier');
                       done();
                   });
                  
              });
               it('should be string', (done) => {

                     chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(typeof res.body.identifier).to.eql('string');
                           done();
                       });
                     
                });
                
                it('should not be empty', (done) => {
                    
                    chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(res.body.identifier.length).to.above(0);
                           done();
                       });
                                         
                });
    
             });

      describe('type', function() {
               it('should exists', (done) => {
                   
                   chai.request(host)
                   .get('/api/resource-groups/011-VZ')             
                   .end( function(err,res){                                 
                       expect(res.body).hasOwnProperty('type');
                       done();
                   });
                  
              });
               it('should be string', (done) => {

                     chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(typeof res.body.type).to.eql('string');
                           done();
                       });
                     
                });
                
                it('should not be empty', (done) => {
                    
                    chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(res.body.type.length).to.above(0);
                           done();
                       });
                                         
                });
    
             });

      describe('ResourceGroupId', function() {
               it('should exists', (done) => {
                   
                   chai.request(host)
                   .get('/api/resource-groups/011-VZ')             
                   .end( function(err,res){                                 
                       expect(res.body).hasOwnProperty('ResourceGroupId');
                       done();
                   });
                  
              });
               it('should be string', (done) => {

                     chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(typeof res.body.ResourceGroupId).to.eql('string');
                           done();
                       });
                     
                });
                
                it('should not be empty', (done) => {
                    
                    chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(res.body.ResourceGroupId.length).to.above(0);
                           done();
                       });
                                         
                });
    
             });

      describe('ResourceGroupParentId', function() {
               it('should exists', (done) => {
                   
                   chai.request(host)
                   .get('/api/resource-groups/011-VZ')             
                   .end( function(err,res){                                 
                       expect(res.body).hasOwnProperty('ResourceGroupParentId');
                       done();
                   });
                  
              });
               it('should be string', (done) => {

                     chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(typeof res.body.ResourceGroupParentId).to.eql('string');
                           done();
                       });
                     
                });
                
                it('should not be empty', (done) => {
                    
                    chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(res.body.ResourceGroupParentId.length).to.above(0);
                           done();
                       });
                                         
                });
    
             });

      describe('origin', function() {
               it('should exists', (done) => {
                   
                   chai.request(host)
                   .get('/api/resource-groups/011-VZ')             
                   .end( function(err,res){                                 
                       expect(res.body).hasOwnProperty('origin');
                       done();
                   });
                  
              });
               it('should be string', (done) => {

                     chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(typeof res.body.origin).to.eql('string');
                           done();
                       });
                     
                });
                
                it('should not be empty', (done) => {
                    
                    chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(res.body.origin.length).to.above(0);
                           done();
                       });
                                         
                });
    
             });

      describe('resources', function() {
               it('should exists', (done) => {
                   
                   chai.request(host)
                   .get('/api/resource-groups/011-VZ')             
                   .end( function(err,res){                                 
                       expect(res.body).hasOwnProperty('resources');
                       done();
                   });
                  
              });
               it('should be object', (done) => {

                     chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(typeof res.body.resources).to.eql('object');
                           done();
                       });
                     
                });
                
                it('should not be empty', (done) => {
                    
                    chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(res.body.resources.length).to.above(0);
                           done();
                       });
                                         
                });
    
             });

       describe('metadata', function() {
               it('should exists', (done) => {
                   
                   chai.request(host)
                   .get('/api/resource-groups/011-VZ')             
                   .end( function(err,res){                                 
                       expect(res.body).hasOwnProperty('metadata');
                       done();
                   });
                  
              });
               it('should be object', (done) => {

                     chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(typeof res.body.metadata).to.eql('object');
                           done();
                       });
                     
                });
                
                it('should not be empty', (done) => {
                    
                    chai.request(host)
                       .get('/api/resource-groups/011-VZ')             
                       .end( function(err,res){                                 
                           expect(res.body.metadata.length).to.above(0);
                           done();
                       });
                                         
                });
    
             });

    });

    describe('Get All', function() {

       it('Results should not be repeated', (done) => {
                
            var gr;
            gr = getRepeated(resourcegroups)
                       
            expect(gr).to.have.length(0);
            done();
            
        
        })


        describe('Checking fields for Resource Groups', function() {

            describe('Ident', function() {
               it('should exists', (done) => {
                      
                  const x = resourcegroups.filter(pos => pos.hasOwnProperty('ident') === false);
                  expect(x).to.have.length(0);
                  done();
              });
               it('should be string', (done) => {
                      
                    const x = resourcegroups.filter(pos => typeof pos.ident !== "string");
                    expect(x).to.have.length(0);
                    done();
                });
                
                it('should not be empty', (done) => {
                         
                    const x = resourcegroups.filter(pos => pos.ident.length === 0);
                    expect(x).to.have.length(0);
                    done();
                });
    
             });
    
             describe('Identifier', function() {
               
                it('should exists', (done) => {
                        
                    const x = resourcegroups.filter(pos => pos.hasOwnProperty('identifier') === false);
                    expect(x).to.have.length(0);
                    done();
                });
                it('should be string', (done) => {
                        
                      const x = resourcegroups.filter(pos => typeof pos.identifier !== "string");
                      expect(x).to.have.length(0);
                      done();
                  });
                  
                  it('should not be empty', (done) => {
                          
                      const x = resourcegroups.filter(pos => pos.identifier.length === 0);
                      expect(x).to.have.length(0);
                      done();
                  });
    
           });
            
            describe('Type', function() {
    
    
              it('should exists', (done) => {
                     
                  const x = resourcegroups.filter(pos => pos.hasOwnProperty('type') === false);
                  expect(x).to.have.length(0);
                  done();
              });
    
              it('should be string', (done) => {
                     
                   const x = resourcegroups.filter(pos => typeof pos.type !== "string");
                   expect(x).to.have.length(0);
                   done();
               });
               
               it('should not be empty', (done) => {
                        
                   const x = resourcegroups.filter(pos => pos.type.length === 0);
                   expect(x).to.have.length(0);
                   done();
               });
    
            });
    
            describe('ResourceGroupId', function() {
    
    
                it('should exists', (done) => {
                       
                    const x = resourcegroups.filter(pos => pos.hasOwnProperty('ResourceGroupId') === false);
                    expect(x).to.have.length(0);
                    done();
                });
      
                it('should be string', (done) => {
                       
                     const x = resourcegroups.filter(pos => typeof pos.ResourceGroupId !== "string");
                     expect(x).to.have.length(0);
                     done();
                 });
                 
                 it('should not be empty', (done) => {
                          
                     const x = resourcegroups.filter(pos => pos.ResourceGroupId.length === 0);
                     expect(x).to.have.length(0);
                     done();
                 });
      
              });

            describe('ResourceGroupParentId', function() {
    
    
                it('should exists', (done) => {
                       
                    const x = resourcegroups.filter(pos => pos.hasOwnProperty('ResourceGroupParentId') === false);
                    expect(x).to.have.length(0);
                    done();
                });
      
                it('should be string', (done) => {
                       
                     const x = resourcegroups.filter(pos => typeof pos.ResourceGroupParentId !== "string");
                     expect(x).to.have.length(0);
                     done();
                 });
                 
                 it('should not be empty', (done) => {
                          
                     const x = resourcegroups.filter(pos => pos.ResourceGroupParentId.length === 0);
                     expect(x).to.have.length(0);
                     done();
                 });
      
              });

             describe('origin', function() {
    
    
                it('should exists', (done) => {
                       
                    const x = resourcegroups.filter(pos => pos.hasOwnProperty('origin') === false);
                    expect(x).to.have.length(0);
                    done();
                });
      
                it('should be string', (done) => {
                       
                     const x = resourcegroups.filter(pos => typeof pos.origin !== "string");
                     expect(x).to.have.length(0);
                     done();
                 });
                 
                 it('should not be empty', (done) => {
                          
                     const x = resourcegroups.filter(pos => pos.origin.length === 0);
                     expect(x).to.have.length(0);
                     done();
                 });
      
              });
             
             describe('resources', function() {
    
    
                it('should exists', (done) => {
                       
                    const x = resourcegroups.filter(pos => pos.hasOwnProperty('resources') === false);
                    expect(x).to.have.length(0);
                    done();
                });
      
                it('should be object', (done) => {
                       
                     const x = resourcegroups.filter(pos => typeof pos.resources !== "object");
                     expect(x).to.have.length(0);
                     done();
                 });
                 
                 it('should not be empty', (done) => {
                     const x = resourcegroups.filter(pos => pos.resources.length === 0);
                     expect(x).to.have.length(0);
                     done();
                 });
      
              });

             describe('metadata', function() {
    
    
                it('should exists', (done) => {
                       
                    const x = resourcegroups.filter(pos => pos.hasOwnProperty('metadata') === false);
                    expect(x).to.have.length(0);
                    done();
                });
      
                it('should be object', (done) => {
                       
                     const x = resourcegroups.filter(pos => typeof pos.metadata !== "object");
                     expect(x).to.have.length(0);
                     done();
                 });
                 
                 it('should not be empty', (done) => {
                          
                     const x = resourcegroups.filter(pos => pos.metadata.length === 0);
                     expect(x).to.have.length(0);
                     done();
                 });
      
              });
        });

    });

    describe('first-level', function() {
        
         

         })
        


});

function inspectChildrenResourceGroupId(pos){

    if (pos.children.length != 0){
        pos.children.filter(function(x) {
           if (x.ResourceGroupId.length == 0){
               return x
            }     
         });  
    }
    
}

function inspectChildrenResourceGroupParentId(pos){

    if (pos.children.length != 0){
        pos.children.filter(function(x) {
           if (x.ResourceGroupParentId.length == 0){
               return x
            }     
         });  
    }
    
}

function getAllResourceGroups(done) {
        
         let options = {
           method: 'GET',
           url: `${host}/api/resource-groups`,
           
       };
       
       rp(options)
           .then(function (a) {
               resourcegroups = JSON.parse(a);
               
               done()                  
           })
           .catch(function (err) {
             console.log(err)    
             done()  
           });
      
       

}

function getRepeated(arr){
    var uniq = arr
    .map((x) => {
        return {
        key: x.ident,
        value: 1        
        }
    }).reduce((a,b) =>{
      
        a[b.key] = (a[b.key] || 0) + b.value
        return a
    }, {})
   
   var duplicates = Object.keys(uniq).filter((a) => uniq[a] > 1)
   return duplicates
}