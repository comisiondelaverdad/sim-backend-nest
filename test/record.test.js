/* global describe, it */
import chai from 'chai';
import chaiHttp from 'chai-http';
import rp from 'request-promise';

const expect = require('chai').expect;
chai.use(chaiHttp);

const host = `http://localhost:4000`;

var records;
var resourcesbyrecord = []; 
var miMapa = new Map();

describe('Record Controller Test', function() {
    before(function(done) {
       // login(done)
        getAllRecords(done)
        getAllResources(done)
      });

    describe('Get One', function() {

        
        it('should return 200 status', (done) => {
            
                chai.request(host)
                .get('/api/record/001-VI-00001_3401')
                
                .end( function(err,res){
                    expect(res).to.have.status(200);
                    done();
                });
            });
        
        it('should return 404 status for invalid recodr', (done) => {
            
                chai.request(host)
                .get('/api/record/001-VI-')
                
                .end( function(err,res){
                   
                    expect(res).to.have.status(404);
                    done();
                });
            });
    });

    describe('Get All', function() {
        it('Results should not be repeated', (done) => {
                  
          var gr;
          gr = getRepeated(records)
                    
          expect(gr).to.have.length(0);
          done();
          
      
      })
   });

    describe('Checking fields for GetAll', function() {
        
        describe('Ident', function() {
           
            it('should exists', (done) => {
                  
              const x = records.filter(pos => pos.hasOwnProperty('ident') === false);
              expect(x).to.have.length(0);
              done();
          });
           it('should be string', (done) => {
                  
                const x = records.filter(pos => typeof pos.ident !== "string");
                expect(x).to.have.length(0);
                done();
            });
            
            it('should not be empty', (done) => {
                     
                const x = records.filter(pos => pos.ident.length === 0);
                expect(x).to.have.length(0);
                done();
            });

         });

         describe('Identifier (filed refering to resource)', function() {
           
            it('should exists', (done) => {
                    
                const x = records.filter(pos => pos.hasOwnProperty('identifier') === false);
                expect(x).to.have.length(0);
                done();
            });
            it('should be string', (done) => {
                    
                  const x = records.filter(pos => typeof pos.identifier !== "string");
                  expect(x).to.have.length(0);
                  done();
              });
              
              it('should not be empty', (done) => {
                      
                  const x = records.filter(pos => pos.identifier.length === 0);
                  expect(x).to.have.length(0);
                  done();
              });

              it('Resource (got by identifier) must exists', (done) => {
                    
                    /* Implementing when we can get all resources at once
                    Searching with identifier, per record, if resource exists (on map)

                    const y = records.map(function(pos){
                        return miMapa.get(pos.identifier)
                    })
                    const x = y.filter(pos => pos === undefined);

                    */
                    const x = resourcesbyrecord.filter(pos => pos.ident === undefined);
                    expect(x).to.have.length(0);
                    done();  
                        
                
            });

              it('Record must exists on array in Resource', (done) => {
                      
                /* Implementing when we can get all resources at once
                Searching with identifier, per record, the resources (on map)
                getting the records array in resource and searching for the wanted record (pos)

                var y;
                records.map(function(pos){
                    var records = miMapa.get(pos.identifier)
                    result.push(records.find(element => element._id == pos._id));
                })
                            

                const x = y.filter(pos => pos === undefined);

                */
                var searchingresource;
                var recordarrayinresource = [];
                records.map(function(pos){
                  //console.log("Identifier del resource al que pertenece el record:",pos.identifier)
                  //console.log("Id del record:",pos._id)
                  searchingresource = resourcesbyrecord.find(element => element.ident == pos.identifier);   //Find on resources array the resources for this record
                  recordarrayinresource.push(searchingresource.records.find(element => element._id == pos._id)); //find on records array if the one in pos exists.
                  /*ar2.map(function(e){
                    console.log(e._id)
                  })Printing the _id in record array*/
                });

                const x = recordarrayinresource.filter(pos => pos === undefined);
                expect(x).to.have.length(0);
                done();  
                    
            
           });

       });
        
        describe('AccessLevel', function() {


          it('should exists', (done) => {
                 
              const x = records.filter(pos => pos.metadata.firstLevel.hasOwnProperty('accessLevel') === false);
              expect(x).to.have.length(0);
              done();
          });

          it('should be string', (done) => {
                 
               const x = records.filter(pos => typeof pos.metadata.firstLevel.accessLevel !== "string");
               expect(x).to.have.length(0);
               done();
           });
           
           it('should not be empty', (done) => {
                   
               const x = records.filter(pos => pos.metadata.firstLevel.accessLevel.length === 0);
               expect(x).to.have.length(0);
               done();
           });

        });

        describe('Labelled records', function() {
             it('Checking points on label ', (done) => {
            
                chai.request(host)
                .get('/api/testing/records/getTagsByRecord?identifier=001-VI-00004')
              
                .end( function(err,res){
                    var ar = getByPoint(res.body[0].labelled.annotation,"Goyes")
                    var l = ar.length
                    /* Print tags and points
                    ar.map(function(e){
                      console.log(e.label, " - ", e.points)
                    })
                    */
                    expect(l).to.be.greaterThan(0)
                    done();
                });
             });
          
            it('Checking tags by name', (done) => {
            
                chai.request(host)
                .get('/api/testing/records/getTagsByRecord?identifier=001-VI-00004')
               
                .end( function(err,res){
                    var ar = getByLabel(res.body[0].labelled.annotation,"Entidades - Personas")
                    /* Print tags and points
                    ar.map(function(e){
                      console.log(e.label, " - ", e.points)
                    })*/
                    var l = ar.length
                    expect(l).to.be.greaterThan(0)
                    done();
                });
             });

        });


      
         
    });
    
});

/*Function to search record by origin
*/
function getRecordByOrigin(o) {
    console.log("Origin",o)
    var query = Record.find({origin: o});
    console.log(query)
        query.exec(function (err) {
            if (err) {
                console.log("err")
                return;
            }
            console.log("succ")
        });
    
  
    //return Record.findOne({ origin: origin }).exec().then((r)=>{ return r })
}

/*Function to get all the records at once
  For now is limited to 10.
*/

function getAllRecords(done) {

         
      //GETTING RECORDS BY LIMIT
        let options = {
          method: 'GET',
          url: `${host}/api/testing/records/getLimitedRecords?limit=10`,
          }
      
      rp(options)
          .then(function (a) {
              records = JSON.parse(a);
              //GETTING RESOURCES BY RECORD - For now I'm just getting ident field and its records.
              ///The idea is to delete this when the map could be used (getting all)
              records.map(function(x) {
                 
                  let options = {
                      method: 'GET',
                      url: `${host}/api/resource/${x.identifier}`,
                      
                      
                  };
  
                  rp(options)
                      .then(function (a) {
                          var json = JSON.parse(a);
                          resourcesbyrecord.push({ident: json.ident, records: json.records})
                                                  
                      })
                      .catch(function (err) {
                          resourcesbyrecord.push({ident: undefined, records: undefined})
                          done();
                      });
                                 
              });
                      
          })
          .catch(function (err) {
            console.log(err)    
            done()
          });
     
      
  
}

/*Function to get all the resources and save them to a map. 
  The idea is not to make a request per record and search on map
  Not using it yet because I can't get all the resources at once

*/
function getAllResources(done) {

//GETTING RECORDS BY LIMIT
        let options = {
          method: 'GET',
          url: `${host}/api/testing/resources/getLimitedResources?limit=10`,
          
      };
      
      rp(options)
          .then(function (a) {
              var x = JSON.parse(a);
              x.map(function(pos){
                miMapa.set(pos.ident,pos.records)
              })
             
              done()            
          })
          .catch(function (err) {
            console.log(err)    
            done()  
          });
     
     
  
}

function getRepeated(arr){
  var uniq = arr
  .map((x) => {
      return {
      key: x.ident,
      value: 1        
      }
  }).reduce((a,b) =>{
    
      a[b.key] = (a[b.key] || 0) + b.value
      return a
  }, {})
 
 var duplicates = Object.keys(uniq).filter((a) => uniq[a] > 1)
 return duplicates
}
  
function getByLabel(arr,label){ 

 var x = [];
 arr.map(function(a){

   a.label.map(function(ep){
   
    if (ep === label){
       x.push(a)
     }
   })
 })

 return x
}

function getByPoint(arr,st){ 

 var x = [];
 arr.map(function(a){

   a.points.map(function(ep){
    var o = ep.text.search(st)
    if (o != -1){
       x.push(a)
     }
   })
 })

 return x
}