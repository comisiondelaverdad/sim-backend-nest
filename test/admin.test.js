/* global describe, it */

import chai from 'chai';
import chaiHttp from 'chai-http';
import rp from 'request-promise';

const expect = require('chai').expect;
chai.use(chaiHttp);

const host = `http://localhost:4000`;


describe('Admin  Controller Test', function() {

	describe('Users Test', function() {

		describe('GetAll', function() {
			chai.request(host)
            .get('api/admin/users')
            .end( function(err,res){
               expect(res).to.have.status(200);
               done();
             });  
          
		});

	});

});