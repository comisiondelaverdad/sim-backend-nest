import chai from 'chai';
import chaiHttp from 'chai-http';
import rp from 'request-promise';

const expect = require('chai').expect;
chai.use(chaiHttp);

const host = `http://localhost:4000`;


describe('AccessRequestExtension Controller Test', function() {

	describe('Post', function() {
  
      it('Should return 201 status', (done) => {
         chai.request(host)
            .post('/api/access-request-extension')
             .send(
              	{
				  "user": "5e8231f982746369b83ed2ac",
				  "date": "2020-07-14T16:32:44.001Z",
				  "from": "2020-07-14T16:32:44.001Z",
				  "to": "2020-07-14T16:32:44.001Z",
				  "status": "string"
				}
              )
            .end( function(err,res){
               expect(res).to.have.status(201);
               done();
             });  
            
           })
      

    });

    describe('Put', function() {
  
      it('Should return 201 status', (done) => {
         chai.request(host)
             .put('/api/access-request-extension/1')
             .send(
              	{
        				  "user": "5e8231f982746369b83ed2ac",
        				  "date": "2020-07-14T16:32:44.001Z",
        				  "from": "2020-07-14T16:32:44.001Z",
        				  "to": "2020-07-14T16:32:44.001Z",
        				  "status": "pending"
        				}
              )
            .end( function(err,res){
               expect(res.body.status).to.eql("pending");
               done();
             });  
            
           })
      

    });


});
