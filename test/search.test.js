/* global describe, it */
import chai from 'chai';
import chaiHttp from 'chai-http';
import request from 'request';
import rp from 'request-promise';
import e from 'express';

const expect = require('chai').expect;
chai.use(chaiHttp);

const host = `http://localhost:4000`;

var token = '';


function getRepeated(arr){
    var uniq = arr
    .map((x) => {
        return {
        key: x._id,
        value: 1        
        }
    }).reduce((a,b) =>{
      
        a[b.key] = (a[b.key] || 0) + b.value
        return a
    }, {})
   
   var duplicates = Object.keys(uniq).filter((a) => uniq[a] > 1)
   return duplicates
}

describe('Search Controller Test', function() {

    describe('Search keywords', function() {
       

        it('should return 200 status with a valid request', (done) => {

            chai.request(host)
            .post('/api/search?q=bogota&from=0&size=10')
            .send({"filters":{
                    "keyword": "bogota",
                    "resourceGroup": {
                      "resourceGroupText": "", 
                      "resourceGroupIDS":  ""
                    },
                    "mapPolygon": ""
                 }
            })
            .end( function(err,res){
              
                expect(res).to.have.status(200);
                done();
                });
            });
        
        it('should return 200 status with no matching keywords', (done) => {
            

                chai.request(host)
                .post('/api/search?q=asdf&from=1')
                .send({"filters":{
                    "keyword": "",
                    "resourceGroup": {
                      "resourceGroupText": "", 
                      "resourceGroupIDS":  ""
                    },
                    "mapPolygon": ""
                     }
                })
                .end( function(err,res){
                
                    expect(res).to.have.status(200);
                    done();
                    });
            });
            
        it('should return empty hits when theres no results', (done) => {
            

                    chai.request(host)
                    .post('/api/search?q=asdf&from=1')
                    .send({"filters":{
                            "keyword": "",
                            "resourceGroup": {
                              "resourceGroupText": "", 
                              "resourceGroupIDS":  ""
                            },
                            "mapPolygon": ""
                         }
                    })
                    .end( function(err,res){
                    
                        expect(res.body).to.have.property('hits').to.have.length(0);
                        done();
                        });
            });
        
        it('should 10 hits per page', (done) => {
            

                chai.request(host)
                .post('/api/search?q=bogota&from=0&size=10')
                .send({"filters":{
                        "keyword": "bogota",
                        "resourceGroup": {
                          "resourceGroupText": "", 
                          "resourceGroupIDS":  ""
                        },
                        "mapPolygon": ""
                     }
                })
                .end( function(err,res){
                    
                    expect(res.body).to.have.property('hits').to.have.length(10);
                    done();
                });
         });
         
          it('should return total.value', (done) => {
            

                chai.request(host)
                .post('/api/search?q=bogota&from=0&size=10')
                .send({"filters":{
                        "keyword": "bogota",
                        "resourceGroup": {
                          "resourceGroupText": "", 
                          "resourceGroupIDS":  ""
                        },
                        "mapPolygon": ""
                     }
                })
                .end( function(err,res){
                    
                    expect(res.body.total.value).to.be.greaterThan(0)
                    done();
                });
         });

          it('should return max_score', (done) => {
            

                chai.request(host)
                .post('/api/search?q=bogota&from=0&size=10')
                .send({"filters":{
                        "keyword": "bogota",
                        "resourceGroup": {
                          "resourceGroupText": "", 
                          "resourceGroupIDS":  ""
                        },
                        "mapPolygon": ""
                     }
                })
                .end( function(err,res){
                    
                    expect(res.body).to.have.property('max_score');
                    done();
                });
         });

         it('Results should not be repeated', (done) => {
                
                var gr;
                gr = getRepeated(totalHits)
                           
                expect(gr).to.have.length(0);
                done();
                
            
         });


        });



    describe('DATA CHECKING: Verifying fields from response (Resource field). If throws erros, could be data, dont worry', function() {

            it('Description must be a string, not an object', (done) => {
                
                chai.request(host)
                .post('/api/search?q=bogota&size=100')
                .send({"filters":{
                        "keyword": "bogota",
                        "resourceGroup": {
                          "resourceGroupText": "", 
                          "resourceGroupIDS":  ""
                        },
                        "mapPolygon": ""
                     }
                })
                .end( function(err,res){
                    
                    const x = res.body.hits.filter(pos =>  typeof pos._source.resource.descripcion !== "string");
                    expect(x).to.have.length(0)
                    done();
                });

            });

            it('conflictActors must be a string, not an object', (done) => {
                
                chai.request(host)
                .post('/api/search?q=bogota&size=100')
                .send({"filters":{
                        "keyword": "bogota",
                        "resourceGroup": {
                          "resourceGroupText": "", 
                          "resourceGroupIDS":  ""
                        },
                        "mapPolygon": ""
                     }
                })
                 .end( function(err,res){
                    
                    const x = res.body.hits.filter(pos =>  typeof pos._source.resource.conflictActors !== "string");
                    expect(x).to.have.length(0)
                    done();
                });


            });

            it('notes must be a string, not an object', (done) => {
                
                chai.request(host)
                .post('/api/search?q=bogota&size=100')
                .send({"filters":{
                        "keyword": "bogota",
                        "resourceGroup": {
                          "resourceGroupText": "", 
                          "resourceGroupIDS":  ""
                        },
                        "mapPolygon": ""
                     }
                })
                 .end( function(err,res){
                    
                    const x = res.body.hits.filter(pos =>  typeof  typeof pos._source.resource.notes !== "string");
                    expect(x).to.have.length(0)
                    done();
                });


            });

             it('Access Level must be a string, not an object', (done) => {
                
                chai.request(host)
                .post('/api/search?q=bogota&size=100')
                .send({"filters":{
                        "keyword": "bogota",
                        "resourceGroup": {
                          "resourceGroupText": "", 
                          "resourceGroupIDS":  ""
                        },
                        "mapPolygon": ""
                     }
                })
                 .end( function(err,res){
                    
                    const x = res.body.hits.filter(pos =>  typeof pos._source.resource.accessLevel !== "string");
                    expect(x).to.have.length(0)
                    done();
                });

            
            });

            it('Ident must be a string, not an object', (done) => {
                
                chai.request(host)
                .post('/api/search?q=bogota&size=100')
                .send({"filters":{
                        "keyword": "bogota",
                        "resourceGroup": {
                          "resourceGroupText": "", 
                          "resourceGroupIDS":  ""
                        },
                        "mapPolygon": ""
                     }
                })
                 .end( function(err,res){
                    
                    const x = res.body.hits.filter(pos => typeof pos._source.resource.ident !== "string");
                    expect(x).to.have.length(0)
                    done();
                });
             

            });

            it('availabilityDate must be a string, not an object', (done) => {
                
                chai.request(host)
                .post('/api/search?q=bogota&size=100')
                .send({"filters":{
                        "keyword": "bogota",
                        "resourceGroup": {
                          "resourceGroupText": "", 
                          "resourceGroupIDS":  ""
                        },
                        "mapPolygon": ""
                     }
                })
                 .end( function(err,res){
                    
                    const x = res.body.hits.filter(pos =>  typeof pos._source.resource.availabilityDate !== "string");
                    expect(x).to.have.length(0)
                    done();
                });

            });

            it('language must be a string, not an object', (done) => {
                
                chai.request(host)
                .post('/api/search?q=bogota&size=100')
                .send({"filters":{
                        "keyword": "bogota",
                        "resourceGroup": {
                          "resourceGroupText": "", 
                          "resourceGroupIDS":  ""
                        },
                        "mapPolygon": ""
                     }
                })
                 .end( function(err,res){
                    
                    const x = res.body.hits.filter(pos =>  typeof pos._source.resource.language !== "string");
                    expect(x).to.have.length(0)
                    done();
                });

            });

            it('title must be a string, not an object', (done) => {
                
                chai.request(host)
                .post('/api/search?q=bogota&size=100')
                .send({"filters":{
                        "keyword": "bogota",
                        "resourceGroup": {
                          "resourceGroupText": "", 
                          "resourceGroupIDS":  ""
                        },
                        "mapPolygon": ""
                     }
                })
                 .end( function(err,res){
                    
                    const x = res.body.hits.filter(pos =>  typeof pos._source.resource.title !== "string");
                    expect(x).to.have.length(0)
                    done();
                });


            });

            it('type must be a string, not an object', (done) => {
                
                chai.request(host)
                .post('/api/search?q=bogota&size=100')
                .send({"filters":{
                        "keyword": "bogota",
                        "resourceGroup": {
                          "resourceGroupText": "", 
                          "resourceGroupIDS":  ""
                        },
                        "mapPolygon": ""
                     }
                })
                 .end( function(err,res){
                    
                    const x = res.body.hits.filter(pos =>  typeof  pos._source.resource.type !== "string");
                    expect(x).to.have.length(0)
                    done();
                });

            
            });
        });

    
    describe('Cobertura temporal searching', function() {

         it('Between the given range - page 1', (done) => {
            
            
                chai.request(host)
                .post('/api/search?q=(resource.temporalCoverage.start%3A%20%5B2019-06-01%20TO%202020-09-30%5D)&from=0&size=10')
                .send({"filters":{"keyword":"(resource.temporalCoverage.start: [2019-06-01 TO 2020-09-30])","resourceGroup":{"resourceGroupText":"","resourceGroupIDS":""},"mapPolygon":""}})
                .end( function(err,res){     

                    var start_test =  new Date("2019-06-01")     
                    var end_test =  new Date("2020-09-30")  
                  
                    var hits = res.body.hits
                    console.log(hits)

                       
                    var results = hits.map(function(pos){
                        var d1 = new Date(pos._source.resource.temporalCoverage.start);
                        var d2 = new Date(pos._source.resource.temporalCoverage.end);
                        if( d1 >= start_test &&  d2 <= end_test) {
                            return 0               
                        }else{
                            return 1
                        }

                       
                    })
                
                    expect(results.reduce((a, b) => a + b)).to.eql(0);
                    done();
                });

        });

         it('Not valid range', (done) => {
            
            
                chai.request(host)
                .post('/api/search?q=(resource.temporalCoverage.start%3A%20%5B2019-06-01%20TO%2019-06-02%5D)&from=0&size=10')
                .send({"filters":{"keyword":"(resource.temporalCoverage.start: [2019-06-01 TO 2019-06-02])","resourceGroup":{"resourceGroupText":"","resourceGroupIDS":""},"mapPolygon":""}})
                .end( function(err,res){     

                    expect(res.body.hits).to.have.length(0);
                    done();
                });

        });

         it('Between the given range - page 5', (done) => {
            
            
                chai.request(host)
                .post('/api/search?q=(resource.temporalCoverage.start%3A%20%5B2019-06-01%20TO%202020-09-30%5D)&from=50&size=10')
                .send({"filters":{"keyword":"(resource.temporalCoverage.start: [2019-06-01 TO 2020-09-30])","resourceGroup":{"resourceGroupText":"","resourceGroupIDS":""},"mapPolygon":""}})
                .end( function(err,res){     

                    var start_test =  new Date("2019-06-01")     
                    var end_test =  new Date("2020-09-30")  
                  
                    var hits = res.body.hits

                       
                    var results = hits.map(function(pos){
                        var d1 = new Date(pos._source.resource.temporalCoverage.start);
                        var d2 = new Date(pos._source.resource.temporalCoverage.end);
                        if( d1 >= start_test &&  d2 <= end_test) {
                            return 0               
                        }else{
                            return 1
                        }

                       
                    })
                
                    expect(results.reduce((a, b) => a + b)).to.eql(0);
                    done();
                });

        });


         it('Smaller range - page 1', (done) => {
            
            
                chai.request(host)

                .post('/api/search?q=(resource.temporalCoverage.start%3A%20%5B2019-06-01%20TO%202019-06-30%5D)&from=0&size=10')
                .send({"filters":{"keyword":"(resource.temporalCoverage.start: [2019-06-01 TO 2019-06-30])","resourceGroup":{"resourceGroupText":"","resourceGroupIDS":""},"mapPolygon":""}})
                .end( function(err,res){     

                    var start_test =  new Date("2019-06-01")     
                    var end_test =  new Date("2019-06-30")  
                  
                    var hits = res.body.hits

                       
                    var results = hits.map(function(pos){
                        var d1 = new Date(pos._source.resource.temporalCoverage.start);
                        var d2 = new Date(pos._source.resource.temporalCoverage.end);
                        if( d1 >= start_test &&  d2 <= end_test) {
                            console.log(d1,"-",d2)
                            return 0               
                        }else{
                            console.log("NO",d1,"-",d2)
                            return 1
                        }

                       
                    })
                    console.log(results)
                    expect(results.reduce((a, b) => a + b)).to.eql(0);
                    done();
                });

        });
    });

  });
