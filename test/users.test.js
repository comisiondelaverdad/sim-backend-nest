/* global describe, it */

import chai from 'chai';
import chaiHttp from 'chai-http';
import rp from 'request-promise';

const expect = require('chai').expect;
chai.use(chaiHttp);

const host = `http://localhost:4000`;


describe('Users Controller Test', function() {
  
    describe('access-requests by user', function() {
  
      it('Should return 200 status', (done) => {
         chai.request(host)
            .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
            .end( function(err,res){
               expect(res).to.have.status(200);
               done();
             });  
            
           })
      
       it('Should return object for given user', (done) => {
         chai.request(host)
            .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
            .end( function(err,res){
                const x = typeof res.body
                expect(x).to.eql("object");
                done();
             });  
            
           })
     
    describe('Id field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('_id') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos._id !== "string");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos._id.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });
      });
      
       describe('extensions field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('extensions') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be object', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.extensions !== "object");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
         
      });
      
      describe('history field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('history') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be object', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.history !== "object");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
         
      });

      describe('User field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    

                        const x = res.body.filter(pos => pos.hasOwnProperty('user') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be object', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.user !== "object");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.user.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });

      });


      describe('Description field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('description') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.description !== "string");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.description.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });

      });

      describe('userAgreement field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('userAgreement') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be boolean', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    

                        const x = res.body.filter(pos => typeof pos.userAgreement !== "boolean");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.userAgreement.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });

      });
      
       describe('RequestDate field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('requestDate') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.requestDate !== "string");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.requestDate.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });

      });

        describe('RequestedFrom field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('requestedFrom') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.requestedFrom !== "string");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.requestedFrom.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });

      });

         describe('requestedTo field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('requestedTo') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.requestedTo !== "string");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.requestedTo.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });

      });


      describe('Status field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('status') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.status !== "string");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.status.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });

      });

        describe('Resouces field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('resource') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be object', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.resource !== "object");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
          
      });


      describe('Ident field', function() {
        it('should exists', (done) => {
              chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('ident') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be number', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.ident !== "number");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/users/5e8231f982746369b83ed2ac/access-requests')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.ident.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });

      });

    });

         
   
    
           
});

