/* global describe, it */

import chai from 'chai';
import chaiHttp from 'chai-http';
import rp from 'request-promise';

const expect = require('chai').expect;
chai.use(chaiHttp);

const host = `http://localhost:4000`;


describe('AccessRequestHistory Controller Test', function() {
  
    describe('Get All', function() {
  
      it('Should return 200 status', (done) => {
         chai.request(host)
            .get('/api/access-requests-history')
            .end( function(err,res){
               expect(res).to.have.status(200);
               done();
             });  
            
           })
      
      describe('Id field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('_id') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos._id !== "string");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos._id.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });
      });

      describe('User field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    

                        const x = res.body.filter(pos => pos.hasOwnProperty('user') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be object', (done) => {
                chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.user !== "object");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.user.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });

      });

      describe('Date field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('date') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.date !== "string");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.date.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });

      });

      describe('Status field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('status') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.status !== "string");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.status.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });

      });

      describe('Description field', function() {
         it('should exists', (done) => {
              chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('description') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.description !== "string");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.description.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });

      });

      describe('Ident field', function() {
        it('should exists', (done) => {
              chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.hasOwnProperty('ident') === false);
                        expect(x).to.have.length(0);
                        done();
               });
          });

          it('should be number', (done) => {
                chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        
                        const x = res.body.filter(pos => typeof pos.ident !== "number");
                        expect(x).to.have.length(0);
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .get('/api/access-requests-history')
                    .end( function(err,res){    
                        const x = res.body.filter(pos => pos.ident.length === 0);
                        expect(x).to.have.length(0);
                        done();
                });

            });

      });

      
    

     

  });
   
     describe('Post', function() {
         it('should return 201, created', (done) => {
                chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "string"
                      })  
                    .end( function(err,res){    
                        
                         expect(res).to.have.status(201);
                         done();
                });

            });

       describe('_id field', function() {
        it('should exists', (done) => {
              chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "_id field test"
                      }) 
                    .end( function(err,res){    
                        const x = res.body.hasOwnProperty('_id');
                        expect(x).to.equal(true);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "_id field test"
                      }) 
                    .end( function(err,res){    
                        
                        const x = typeof res.body._id;
                        expect(x).to.equal("string");
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "_id field test"
                      }) 
                    .end( function(err,res){    
                        const x = res.body._id;
                        expect(x).not.to.have.length(0);
                        done();
                });

            });

      });

       describe('user field', function() {
        it('should exists', (done) => {
              chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "user field test"
                      }) 
                    .end( function(err,res){    
                        const x = res.body.hasOwnProperty('user');
                        expect(x).to.equal(true);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "user field test"
                      }) 
                    .end( function(err,res){    
                        
                        const x = typeof res.body.user;
                        expect(x).to.equal("string");
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "user field test"
                      }) 
                    .end( function(err,res){    
                        const x = res.body.user;
                        expect(x).not.to.have.length(0);
                        done();
                });

            });

      });

    
    describe('date field', function() {
        it('should exists', (done) => {
              chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "date field test"
                      }) 
                    .end( function(err,res){    
                        const x = res.body.hasOwnProperty('date');
                        expect(x).to.equal(true);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "date field test"
                      }) 
                    .end( function(err,res){    
                        
                        const x = typeof res.body.date;
                        expect(x).to.equal("string");
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "date field test"
                      }) 
                    .end( function(err,res){    
                        const x = res.body.date;
                        expect(x).not.to.have.length(0);
                        done();
                });

            });

      });

    describe('status field', function() {
        it('should exists', (done) => {
              chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "status field test"
                      }) 
                    .end( function(err,res){    
                        const x = res.body.hasOwnProperty('status');
                        expect(x).to.equal(true);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "status field test"
                      }) 
                    .end( function(err,res){    
                        
                        const x = typeof res.body.status;
                        expect(x).to.equal("string");
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "status field test"
                      }) 
                    .end( function(err,res){    
                        const x = res.body.status;
                        expect(x).not.to.have.length(0);
                        done();
                });

            });

      });

    describe('description field', function() {
        it('should exists', (done) => {
              chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "description field test"
                      }) 
                    .end( function(err,res){    
                        const x = res.body.hasOwnProperty('description');
                        expect(x).to.equal(true);
                        done();
               });
          });

          it('should be string', (done) => {
                chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "description field test"
                      }) 
                    .end( function(err,res){    
                        
                        const x = typeof res.body.description;
                        expect(x).to.equal("string");
                        done();
                });
            });
            
           it('should not be empty', (done) => {
                chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "description field test"
                      }) 
                    .end( function(err,res){    
                        const x = res.body.description;
                        expect(x).not.to.have.length(0);
                        done();
                });

            });

      });

    describe('ident field', function() {
        it('should exists', (done) => {
              chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "ident field test"
                      }) 
                    .end( function(err,res){    
                        const x = res.body.hasOwnProperty('ident');
                        expect(x).to.equal(true);
                        done();
               });
          });

          it('should be number', (done) => {
                chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "ident field test"
                      }) 
                    .end( function(err,res){    
                        
                        const x = typeof res.body.ident;
                        expect(x).to.equal("number");
                        done();
                });
            });
            
           it('should not be 0 - sequence working', (done) => {
                chai.request(host)
                    .post('/api/access-requests-history')
                    .send(      
                      {
                         
                          "user": "5e8231f982746369b83ed2ac",
                          "date": "2020-07-09T17:47:51.603Z",
                          "status": "created",
                          "description": "ident field test"
                      }) 
                    .end( function(err,res){    
                        const x = res.body.ident;
                        expect(x).to.be.above(0);
                        done();
                });

            });

      });

 });
    
           
});

