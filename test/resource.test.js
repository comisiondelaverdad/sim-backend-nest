/* global describe, it */

import chai from 'chai';
import chaiHttp from 'chai-http';
import rp from 'request-promise';

const expect = require('chai').expect;
chai.use(chaiHttp);

const host = `http://localhost:4000`;
var resources;
var miMapa = new Map();


describe('Resource Controller Test', function() {

    before(function(done) {
        getAllResources(done)
        //getAllResourceGroups(done)
      });

 
 
    describe('Get One', function() {
        
        it('should return 200 status', (done) => {
                
                    chai.request(host)
                    .get('/api/resources/045-VI-00023')
                   
                    .end( function(err,res){
                        
                        expect(res).to.have.status(200);
                        done();
                    });
                });
            
          });

    describe('Get All', function() {
  
      it('Results should not be repeated', (done) => {
                
                var gr;
                gr = getRepeated(resources)
                          
                expect(gr).to.have.length(0);
                done();
                
            
            })
          
        });

    describe('Cases for Interviews ', function() {
  
           it('should return a informedConsent (one example)', (done) => {
                     
               chai.request(host)
               .get('/api/resources/046-VI-00019')
             
               .end( function(err,res){
                   var ci =  res.body.records.filter(function(x) {
                     return x.type == "Consentimiento informado";
                   });       
                               
                   expect(ci).to.have.length(1);
                   done();
               });
           });
           
           it('an interview should return a informedConsent (with limit of resources)', (done) => {
             
             chai.request(host)
             .get('/api/testing/resources/getLimitedbyTypeResources?limit=30&type=Entrevista%20individual%20a%20v%C3%ADctimas%2C%20familiares%20y%20testigos')
            
             .end( function(err,res){
               
                 var r = res.body.map(function(num) {
                     return inspectInformedConsent(num.records);
                 });   
                 expect(r).to.have.length(30);
                 done();
             });
           });
        });

    describe('ListByParents service', function() {
        it('Should bring 25 resources by given resourceGroupId', (done) => {
              
            chai.request(host)
            .post('/api/resources/listByParents?from=1&size=25')
            .send({
            'resourceGroup':'011-VZ'
            })         
            .end( function(err,res){        
                var l = res.body.docs.length  
                expect(l).not.greaterThan(25);
                done();
            });
       });

       it('Should bring total resources by given resourceGroupId', (done) => {
              
            chai.request(host)
            .post('/api/resources/listByParents?from=1&size=25')
            .send({
            'resourceGroup':'011-VZ'
            })
            .end( function(err,res){          
                expect(res.body).to.have.property("total");
                done();
            });
       });
      
       it('Should bring empty docs when no existing resourceGroupId', (done) => {
              
            chai.request(host)
            .post('/api/resources/listByParents?from=1&size=25')
            .send({
            'resourceGroup':'14-OE'
            })
            .end( function(err,res){          
                expect(res.body.docs).to.have.length(0);
                done();
            });
       });

       it('Should bring total = 0 when no existing resourceGroupId ', (done) => {
              
              chai.request(host)
              .post('/api/resources/listByParents?resourceGroup=14-OE&from=1&size=25')
              .send({
                'resourceGroup':'14-OE'
                })
              .end( function(err,res){          
                  var t = res.body.total
                  expect(t).to.be.equal(0);
                  done();
              });
        })

        it('Should bring empty docs when theres no resources for existing resourceGroupId', (done) => {
              
              chai.request(host)
              .post('/api/resources/listByParents?from=1&size=25')
              .send({
              'resourceGroup':'11-OI'
              })
              .end( function(err,res){          
                  expect(res.body.docs).to.have.length(0);
                  done();
              });
       });

       it('Should bring total = 0 when theres no resources for existing resourceGroupId ', (done) => {
              
              chai.request(host)
              .post('/api/resources/listByParents?resourceGroup=11-OI&from=1&size=25')
              .send({
              'resourceGroup':'11-OI'
              })
              .end( function(err,res){          
                  var t = res.body.total
                  expect(t).to.be.equal(0);
                  done();
              });
       });

    });

    describe('Checking fields for Resources', function() {
        describe('Ident', function() {
           
            it('should exists', (done) => {
                  
              const x = resources.filter(pos => pos.hasOwnProperty('ident') === false);
              expect(x).to.have.length(0);
              done();
          });
           it('should be string', (done) => {
                  
                const x = resources.filter(pos => typeof pos.ident !== "string");
                expect(x).to.have.length(0);
                done();
            });
            
            it('should not be empty', (done) => {
                     
                const x = resources.filter(pos => pos.ident.length === 0);
                expect(x).to.have.length(0);
                done();
            });

         });

         describe('Identifier', function() {
           
            it('should exists', (done) => {
                    
                const x = resources.filter(pos => pos.hasOwnProperty('identifier') === false);
                expect(x).to.have.length(0);
                done();
            });
            it('should be string', (done) => {
                    
                  const x = resources.filter(pos => typeof pos.identifier !== "string");
                  expect(x).to.have.length(0);
                  done();
              });
              
              it('should not be empty', (done) => {
                      
                  const x = resources.filter(pos => pos.identifier.length === 0);
                  expect(x).to.have.length(0);
                  done();
              });

       });
        
        describe('AccessLevel', function() {


          it('should exists', (done) => {
                 
              const x = resources.filter(pos => pos.metadata.firstLevel.hasOwnProperty('accessLevel') === false);
              expect(x).to.have.length(0);
              done();
          });

          it('should be string', (done) => {
                 
               const x = resources.filter(pos => typeof pos.metadata.firstLevel.accessLevel !== "string");
               expect(x).to.have.length(0);
               done();
           });
           
           it('should not be empty', (done) => {
                    
               const x = resources.filter(pos => pos.metadata.firstLevel.accessLevel.length === 0);
               expect(x).to.have.length(0);
               done();
           });

        });

        describe('ResourceGroupId (filed refering to parent)', function() {


            it('should exists', (done) => {
                   
                const x = resources.filter(pos => pos.hasOwnProperty('ResourceGroupId') === false);
                expect(x).to.have.length(0);
                done();
            });
  
            it('should be string', (done) => {
                   
                 const x = resources.filter(pos => typeof pos.ResourceGroupId !== "string");
                 expect(x).to.have.length(0);
                 done();
             });
             
             it('should not be empty', (done) => {
                      
                 const x = resources.filter(pos => pos.ResourceGroupId === 0);
                 expect(x).to.have.length(0);
                 done();
             });

             it('ResourceGroup (got by ResourceGroupId) must exists', (done) => {
               
                //Searching with resourcesGroupId, per resources, if Resource Group exists (on map)
                const y = resources.map(function(pos){
                   return miMapa.get(pos.ResourceGroupId)
                })
                const x = y.filter(pos => pos === undefined);
                expect(x).to.have.length(0);
                done();  
                    
            
              });

              it('ResourceGroup must have on its resources array the named resource ', (done) => {
                
                //Searching with resourcesGroupId, per resources, if Resource Group exists (on map)
                var found = [];
                resources.map(function(pos){
                   var a = miMapa.get(pos.ResourceGroupId)
                   found.push(a.find(element => element === pos._id))
                  
                })
                
                expect(found).to.have.length(resources.length);
                done();  
                    
            
              });
  
          });
         
    });

    describe('Count service', function() {
         it('should return count as a number', (done) => {
                     
               chai.request(host)
               .get('/api/resources/count')             
               .end( function(err,res){
                                                      
                   expect(typeof res.body.count).to.eql("number");
                   done();
               });
           });

         it('should return count greater than 0', (done) => {
                     
               chai.request(host)
               .get('/api/resources/count')             
               .end( function(err,res){
                                                      
                   expect(res.body.count).to.above(0);
                   done();
               });
           });
    })

    describe('Resources-parents per Resource ', function() {

         describe('_id', function() {

          it('should exists', (done) => {

               chai.request(host)
               .get('/api/resources/045-VI-00023/resources-parents')             
               .end( function(err,res){
                                                      
                   expect(res.body).to.hasOwnProperty('_id');
                   done();
               });
              
          });

          it('should be string', (done) => {
                 
                chai.request(host)
               .get('/api/resources/045-VI-00023/resources-parents')             
               .end( function(err,res){
                             
                   expect(typeof res.body._id).to.eql('string');
                   done();
               });
           });
           
           it('should not be empty', (done) => {
                    
                chai.request(host)
               .get('/api/resources/045-VI-00023/resources-parents')             
               .end( function(err,res){
                                                      
                   expect(res.body._id.length).to.above(0);
                   done();
               });
           });

        });

          describe('ident', function() {

          it('should exists', (done) => {

               chai.request(host)
               .get('/api/resources/045-VI-00023/resources-parents')             
               .end( function(err,res){
                                                      
                   expect(res.body).to.hasOwnProperty('ident');
                   done();
               });
              
          });

          it('should be string', (done) => {
                 
                chai.request(host)
               .get('/api/resources/045-VI-00023/resources-parents')             
               .end( function(err,res){
                             
                   expect(typeof res.body.ident).to.eql('string');
                   done();
               });
           });
           
           it('should not be empty', (done) => {
                    
                chai.request(host)
               .get('/api/resources/045-VI-00023/resources-parents')             
               .end( function(err,res){
                                                      
                   expect(res.body.ident.length).to.above(0);
                   done();
               });
           });

        });

        describe('ResourceGroupId', function() {

          it('should exists', (done) => {

               chai.request(host)
               .get('/api/resources/045-VI-00023/resources-parents')             
               .end( function(err,res){
                                                      
                   expect(res.body).to.hasOwnProperty('ResourceGroupId');
                   done();
               });
              
          });

          it('should be string', (done) => {
                 
                chai.request(host)
               .get('/api/resources/045-VI-00023/resources-parents')             
               .end( function(err,res){
                             
                   expect(typeof res.body.ResourceGroupId).to.eql('string');
                   done();
               });
           });
           
           it('should not be empty', (done) => {
                    
                chai.request(host)
               .get('/api/resources/045-VI-00023/resources-parents')             
               .end( function(err,res){
                                                      
                   expect(res.body.ResourceGroupId.length).to.above(0);
                   done();
               });
           });

        });

        describe('Parents', function() {

          it('should exists', (done) => {

               chai.request(host)
               .get('/api/resources/045-VI-00023/resources-parents')             
               .end( function(err,res){
                                                      
                   expect(res.body).to.hasOwnProperty('parents');
                   done();
               });
              
          });

          it('should be array', (done) => {
                 
                chai.request(host)
               .get('/api/resources/045-VI-00023/resources-parents')             
               .end( function(err,res){
                             
                   expect(typeof res.body.parents).to.eql('object');
                   done();
               });
           });
           
           it('should not be empty', (done) => {
                    
                chai.request(host)
               .get('/api/resources/045-VI-00023/resources-parents')             
               .end( function(err,res){
                                                      
                   expect(res.body.parents.length).to.above(0);
                   done();
               });
           });

        });

    });

           
});


function inspectInformedConsent(pos){

  pos.filter(function(x) {
     if (x.type == "Consentimiento informado"){
       return x
     }     
  });  
}

function getAllResources(done) {
    
   let options = {
           method: 'GET',
           url: `${host}/api/testing/resources/getLimitedResources?limit=10`,
           
       };
       
       rp(options)
           .then(function (a) {
               resources = JSON.parse(a);
                                        
           })
           .catch(function (err) {
             console.log(err)    
             done()  
           });
      
      done()   

}

function getAllResourceGroups(done) {

  let options = {
              method: 'GET',
              url: `${host}/api/resources-groups`,
              
          };
          
          rp(options)
              .then(function (a) {
                  var x = JSON.parse(a);                 
                  x.map(function(pos){                    
                    miMapa.set(pos.ident,pos.resources)
                  })
                 
                           
              })
              .catch(function (err) {
                console.log(err)    
                done()  
              });
         
         done()    
      
    }


function getRepeated(arr){
  var uniq = arr.map((x) => {
      return {
      key: x.ident,
      value: 1        
      }
  }).reduce((a,b) =>{
    
      a[b.key] = (a[b.key] || 0) + b.value
      return a
  }, {})
 
 var duplicates = Object.keys(uniq).filter((a) => uniq[a] > 1)
 return duplicates
}