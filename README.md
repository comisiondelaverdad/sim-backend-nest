# Backend SIM

Implementación de servicios para el Sistema de Información Misional de la
Comisión de la Verdad.


<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[travis-image]: https://api.travis-ci.org/nestjs/nest.svg?branch=master
[travis-url]: https://travis-ci.org/nestjs/nest
[linux-image]: https://img.shields.io/travis/nestjs/nest/master.svg?label=linux
[linux-url]: https://travis-ci.org/nestjs/nest
  
<p align="center">A progressive <a href="http://nodejs.org" target="blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>

  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description



## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Original Nest project is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers.
If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## License

Original Nest project is [MIT licensed](https://github.com/nestjs/nest/blob/master/LICENSE).

## Agregar campos en archivo .env para funcionamiento de proceso Logging (Logs de errores y excepciones)

#MONGODB_URI_LOGS='mongodb://sim_prod_user:POUiMeiiXtYQmgH357UMW0wTYzQ4uyR0x4HyUOS5qUgDrYPakd56@192.168.1.23:27017/sim-logging'
MONGODB_URI_LOGS='mongodb://sim_qa_user:699cNmxsFS5CS2WuaSphsDENMFbcuCvauF8b1TWCxRE8yPVfN6@192.168.1.23:27017/sim-logging-qa'
LOGGING_PROCESS_SILENT = false