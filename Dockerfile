FROM ubuntu:20.04

ARG PORT

ENV DEBIAN_FRONTEND noninteractive

WORKDIR /app

RUN apt-get update \
    && apt-get -y install curl \
    && apt-get -y install git \
    && curl -sL https://deb.nodesource.com/setup_14.x | bash - \
    && apt-get -y install nodejs\
    && apt-get -y install pdftk\
    && apt-get -y install poppler-utils\
    && apt-get -y install ghostscript\
    && apt-get -y install tesseract-ocr\
    && apt-get -y install libreoffice
#    && apt-get -y install ffmpeg
#   && apt-get install python3.8

COPY . .

RUN cd /app\
    && ls -la\
    && echo "" > index.html

RUN  cd /app && npm install

RUN mkdir /rep

EXPOSE ${PORT}
CMD ["npm","run-script", "start"]
